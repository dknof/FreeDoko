DEPTH = .

include $(DEPTH)/Makefile.version
include $(DEPTH)/Makefile.os

include $(DEPTH)/Makefile.install.directories

ifeq ($(OPERATING_SYSTEM), Windows)
	PROGRAM ?= FreeDoko.exe
endif
ifeq ($(OPERATING_SYSTEM), MSYS)
	PROGRAM ?= FreeDoko.exe
endif
PROGRAM ?= FreeDoko

export PROGRAM


# Edited for Debian GNU/Linux.
DESTDIR = 


.PHONY: all
all: short_help

# short help
.PHONY: short_help
short_help : short_help-de

short_help-de:
# short help
.PHONY: short_help-de
short_help-de:
	@echo "Kurzhilfe"
	@echo ""
	@echo "make compile"
	@echo "   Erzeugt das Programm für make install"
	@echo ""
	@echo "make documentation"
	@echo "   Erzeugt die Dokumentation."
	@echo ""
	@echo "make install"
	@echo "   Erzeugt das Programm und die Dokumentation und installiert FreeDoko mit den zugehörigen Dateien."
	@echo "   Die Installationspfade können in Makefile.install.directories angepasst werden."
	@echo ""
	@echo "make uninstall"
	@echo "   Löscht die Dateien, die mit „make install“ installiert wurden."
	@echo ""
	@echo "make help"
	@echo "   Zeigt die Hilfe für alle make-Ziele."
	@echo ""
	@echo "Wenn Sie in das Verzeichnis src wechseln und dort make aufrufen, können Sie FreeDoko direkt aus dem src-Verzeichnis starten."

# short help
.PHONY: short_help-en
short_help-en:
	@echo "short help"
	@echo ""
	@echo "make compile"
	@echo "   Create the binary in the src-directory."
	@echo "   You can start FreeDoko directly from that directory."
	@echo ""
	@echo "make documentation"
	@echo "   Create the documentation."
	@echo ""
	@echo "make install"
	@echo "   Create the binary and documentation and install FreeDoko."
	@echo "   The paths can be changed in Makefile.directories."
	@echo ""
	@echo "make uninstall"
	@echo "   Remove the files installed by 'make install'."
	@echo ""
	@echo "make help"
	@echo "   Show help for all make targets."


# print the targets of this makefile
.PHONY: help
help : help-de

.PHONY: help-de
help-de :
	@echo "make-Ziele"
	@echo "  help             diese Hilfe"
	@echo "  help-en          diese Hilfe in englisch"
	@echo "  compile          Kompiliert FreeDoko"
	@echo "  documentation    erzeugt die Dokumentation"
	@echo "  Version          aktualisiert die Datei „Version“"
	@echo "  clean            löscht alle automatisch erzeugten Dateien"
	@echo "  release          erzeugt alle Dateien für eine Veröffentlichung"
	@echo "  release_debian           erzeugt die debian-Pakete für eine Veröffentlichung (aktuell nicht gepflegt)"
	@echo "  release_Linux            erzeugt das Linux-Archiv für eine Veröffentlichung"
	@echo "  release_Windows          erzeugt den Windows-Installer für eine Veröffentlichung"
	@echo "  release_directory        erzeugt das Verzeichnis für die Veröffentlichungs-Dateien"
	@echo "  release_data             kopiert die Datendateien in das Veröffentlichungs-Verzeichnis"
	@echo "  release_linux_binary     erzeugt das Linux-Programm und kopiert es in das Veröffentlichungs-Verzeichnis"
	@echo "  release_windows_binary   kopiert das Windows-Programm in das Veröffentlichungs-Verzeichnis"
	@echo "  release_windows_libs     kopiert die Windows-Bibliotheken in das Veröffentlichungs-Verzeichnis"
	@echo "  release_source   erzeugt die src.zip-Datei für die Veröffentlichung"
	@echo "  install          compiliert das Programm und installiert es im System (Pfade siehe Makefile.directories)"
	@echo "  uninstall        löscht die Dateien, die mit „make install“ installiert wurden"
	@echo "  CD               erstellt eine FreeDoko-CD (aktuell nicht gepflegt)"
	@echo "  flip             transformiert die text-Dateien in DOS-Format"


# print the targets of this makefile
.PHONY: help-en
help-en :
	@echo "make targets"
	@echo "  help             this help"
	@echo "  compile          compile FreeDoko"
	@echo "  documentation    create the documentation"
	@echo "  Version          update the Version file"
	@echo "  clean            clean the directory"
	@echo "  release          create a release"
	@echo "  release_debian           create the debian packages for a release"
	@echo "  release_Linux            create the Linux archive for a release"
	@echo "  release_Windows          create the Windows installer for a release"
	@echo "  release_directory        create the directory for the release files"
	@echo "  release_data             copies the data files in the release directory"
	@echo "  release_linux_binary     creates the linux binary and copies it into the release directory"
	@echo "  release_windows_binary   copies the windows binary into the release directory"
	@echo "  release_windows_libs     copies the windows libs into the release directory"
	@echo "  release_source   copies all data for the release source zip in the release diretory"
	@echo "  install          creates the program and installs it into the system"
	@echo "  uninstall        delete the by 'make install' installed files"
	@echo "  CD               create the FreeDoko CD"
	@echo "  flip             transforms the text files in DOS-format"

# update the 'Version' file
Version : src/versions.cpp
ifeq ($(SHELLTYPE), sh)
	@echo "Version: "${VERSION} >Version
	@echo "Date: "`date +%Y-%m-%d` >>Version
endif

.PHONY: compile
compile:
# compile FreeDoko
	$(MAKE) -C src release_bin
	@echo "---"
	@echo "* Program 'FreeDoko' created"

.PHONY: documentation
documentation:
# create the documentation"
	$(MAKE) -C doc/ all
	@echo "---"
	@echo "Dokumentation erzeugt"

# clean the directory
ifeq ($(SHELLTYPE), sh)
.PHONY: clean
clean :
	rm -f *~
	-@for d in *; \
	do	if test -d $$d; \
		then	if test -e $$d/Makefile; \
			then	$(MAKE) -C $$d clean; \
			fi \
		fi \
	done
	-find . -name "core" -exec rm -f \{\} \;
#	-find . -name ".thumbnails" -exec rm -rf \{\} \;
endif

# create a release
# destination is ../FreeDoko_$(VERSION).release
# * source zip
# * linux tgz archive
# * windows zip archive
# * windows setup exe (see setup/Windows/Makefile)
# * debian package (needs debian programs) (see setup/debian/)
# * rpm package (from debian archive)
ifeq ($(OPERATING_SYSTEM), Linux)
.PHONY: release
release : clean
	-chmod -R +w $(RELEASE_DEST)
	-$(RM) -r $(RELEASE_DEST)/*
	-$(RM) -r obj
	$(MAKE) Version
	$(MAKE) release_source
	$(MAKE) release_Linux
	$(MAKE) release_Windows
#	packages
#	$(MAKE) release_debian

# create the debian packages
.PHONY: release_debian
release_debian:
	$(MAKE) debian_packages
	cd $(RELEASE_DEST) \
	  && rm -rf debian/ \
	  && mkdir -p debian/pool/main/FreeDoko/ \
	  && cp -a /home/install/mirrors/FreeDoko/pool/main/FreeDoko/*$(VERSION)*.deb debian/pool/main/FreeDoko/ \
	  && mkdir -p debian/pool/non-free/FreeDoko/ \
	  && cp -a /home/install/mirrors/FreeDoko/pool/non-free/FreeDoko/*$(VERSION)*.deb debian/pool/non-free/FreeDoko/
	cd $(RELEASE_DEST) \
	  && cp debian/pool/non-free/FreeDoko/freedoko-nonfree_$(VERSION)*.deb .
# Probleme mit dem Setzen der Dateizugehörigkeit auf root -- manuell aufrufen
#	cd $(RELEASE_DEST) \
	  && fakeroot alien --to-rpm freedoko-nonfree_$(VERSION)*.deb
	$(RM) -r $(RELEASE_TMP)


# create the windows release
.PHONY: release_Windows
release_Windows:
	-test -f $(RELEASE_DEST)/FreeDoko_$(VERSION).Windows.zip && rm $(RELEASE_DEST)/FreeDoko_$(VERSION).Windows.zip 
	$(MAKE) release_directory
	mkdir $(RELEASE_TMP)/FreeDoko_$(VERSION)
	$(MAKE) release_data
	$(MAKE) release_windows_binary
	$(MAKE) release_windows_libs
	cd $(RELEASE_TMP); \
	  mv FreeDoko_$(VERSION) FreeDoko_$(VERSION)-symlinks; \
	  mkdir FreeDoko_$(VERSION); \
	  cd FreeDoko_$(VERSION)-symlinks; \
	  tar -hcf - . | tar -xf - -C ../FreeDoko_$(VERSION); \
	  cd ..;\
	  $(RM) -r FreeDoko_$(VERSION)-symlinks; \
	  zip -r $(RELEASE_DEST)/FreeDoko_$(VERSION).Windows.zip FreeDoko_$(VERSION)
	$(MAKE) -C setup/InnoSetup setup
	$(RM) -r $(RELEASE_TMP)/FreeDoko_$(VERSION)
	mv setup/InnoSetup/FreeDoko_$(VERSION).Setup.exe $(RELEASE_DEST)/
	chmod +x $(RELEASE_DEST)/*.exe

# create the directory for the release files
# called by target 'release'
.PHONY: release_directory
release_directory :
	-test -d $(RELEASE_TMP) \
	  && ( chmod -R +w $(RELEASE_TMP); \
	  rm -r $(RELEASE_TMP) )
	mkdir -p $(RELEASE_TMP)

# copies the data files in the release directory
# 'release_directory' should be called sometime before
# called by target 'release'
.PHONY: release_data
release_data :
	$(MAKE) Version
	cp AUTHORS COPYING README Version src/options $(RELEASE_TMP)/FreeDoko_$(VERSION)/
	$(MAKE) -C data release
	$(MAKE) -C po release
	$(MAKE) -C doc release
	$(MAKE) -C manual release

# create the Linux release
.PHONY: release_Linux
release_Linux:
	$(MAKE) release_directory
	$(RM) $(RELEASE_DEST)/FreeDoko_$(VERSION).Linux.zip
	#echo "UEsFBgAAAAAAAAAAAAAAAAAAAAAAAA==" | base64 -d > $(RELEASE_DEST)/FreeDoko_$(VERSION).Linux.zip 
	mkdir $(RELEASE_TMP)/FreeDoko_$(VERSION)
	$(MAKE) release_data
	$(MAKE) release_linux_binary
	cd $(RELEASE_TMP); zip -r $(RELEASE_DEST)/FreeDoko_$(VERSION).Linux.zip FreeDoko_$(VERSION)
	rm -r $(RELEASE_TMP)

# creates the linux binary and copies it into the release directory
# 'release_directory' should be called sometime before
# called by target 'release'
.PHONY: release_linux_binary
release_linux_binary :
	$(MAKE) -C src release_bin
	cp src/$(PROGRAM) $(RELEASE_TMP)/FreeDoko_$(VERSION)/

# copies the windows binary into the release directory
# 'release_directory' should be called sometime before
# called by target 'release'
# the windows binary is not created, it is only being copied
.PHONY: release_windows_binary
release_windows_binary :
	cp ~/mnt/qemu/FreeDoko/src/FreeDoko.exe $(RELEASE_TMP)/FreeDoko_$(VERSION)/

# copy the libraries for the release files
# called by target 'release'
.PHONY: release_windows_libs
release_windows_libs :
	for d in setup/dll/gtkmm-3.dll; do \
	  cd "$$d"; \
	  git ls-files . \
	    | while read f; do \
	        cp --parents "$$f" $(RELEASE_TMP)/FreeDoko_$(VERSION)/; \
	      done; \
	  cd -; \
	done

# copies all data for the release source zip in the release diretory
# called by target 'release'
# this depends on having the source in a git repository
.PHONY: release_source
release_source : release_directory
	-test -f $(RELEASE_DEST)/FreeDoko_$(VERSION).src.zip  && rm $(RELEASE_DEST)/FreeDoko_$(VERSION).src.zip 
	git archive --prefix FreeDoko_$(VERSION)/ -o $(RELEASE_DEST)/FreeDoko_$(VERSION).src.zip HEAD .
	fuse-zip $(RELEASE_DEST)/FreeDoko_$(VERSION).src.zip $(RELEASE_TMP)
	cd "$(RELEASE_TMP)/FreeDoko_$(VERSION)/" \
	  && rm -r homepage bugreports references setup/debian
	cd "$(RELEASE_TMP)/FreeDoko_$(VERSION)/data/" \
	  && rm -r "cardsets/InnoCard" "cardsets/gnome-games" "cardsets/kdecarddecks" "iconsets/Georg"
	rm -r "$(RELEASE_TMP)/FreeDoko_$(VERSION)/setup/dll"
	git ls-files manual \
	  | while read f; do \
	      cp --parents "$$f" $(RELEASE_TMP)/FreeDoko_$(VERSION)/; \
	    done
	fusermount -u $(RELEASE_TMP)
endif

# compiles the program and installs it into the system
# The directories can be changed in 'Makefile.install.directories'
ifeq ($(OPERATING_SYSTEM), Linux)
.PHONY: install
install : check_install
#	Copying the program
	echo "Kopiere das Programm"
	mkdir -p $(DESTDIR)$(BIN_DIRECTORY)
	cp src/FreeDoko $(DESTDIR)$(BIN_DIRECTORY)
	cd $(DESTDIR)$(BIN_DIRECTORY) && test -e freedoko || ln -sf FreeDoko freedoko
	-restorecon -F $(DESTDIR)$(BIN_DIRECTORY)/FreeDoko $(DESTDIR)$(BIN_DIRECTORY)/freedoko
#	Copying the data files
	echo "Kopiere die Daten"
	$(MAKE) -C data install
#	copy the locale files
	$(MAKE) -C po install
#	Copying the documentation
#	SuSE does want the documentation in the directory
#	'/usr/share/doc/packages/freedoko' so adjust it here
	echo "Kopiere die Dokumentation"
	mkdir -p $(DESTDIR)$(HELP_DIRECTORY)
	cp AUTHORS COPYING $(DESTDIR)$(HELP_DIRECTORY)/
	$(MAKE) -C doc install
	$(MAKE) -C manual install
#	update the manual page
	echo "Aktualisiere die man-Page"
	a2x -f manpage doc/freedoko.adoc
	mkdir -p $(DESTDIR)$(MAN_DIRECTORY)
	cp doc/freedoko.6 $(DESTDIR)$(MAN_DIRECTORY)/
	cd $(DESTDIR)$(MAN_DIRECTORY)/ && test -e FreeDoko.6 || ln -sf freedoko.6 FreeDoko.6
	-restorecon -F $(DESTDIR)$(MAN_DIRECTORY)/FreeDoko.6 $(DESTDIR)$(MAN_DIRECTORY)/freedoko.6
#	copy the icon
	echo "Kopiere das Icon"
	mkdir -p $(DESTDIR)$(ICON_DIRECTORY)
	cp src/FreeDoko.png $(DESTDIR)$(ICON_DIRECTORY)/
	-restorecon -F $(DESTDIR)$(ICON_DIRECTORY)/FreeDoko.png
#	copy the desktop file
	echo "Kopiere den Desktop-Starter"
	mkdir -p "$(DESTDIR)$(DESKTOP_FILE_DIRECTORY)"
	sed "s|Exec=.*$$|Exec=$(BIN_DIRECTORY)/FreeDoko|" bin/FreeDoko.desktop > "$(DESTDIR)$(DESKTOP_FILE_DIRECTORY)/FreeDoko.desktop"
	-restorecon -F "$(DESTDIR)$(DESKTOP_FILE_DIRECTORY)/FreeDoko.desktop"
#	all finished
	echo "--\nInstallation erfolgreich"

.PHONY: check_install
check_install :
	@test -f src/FreeDoko || echo "Programmdatei FreeDoko nicht gefunden. Wurde „make compile“ vorher erfolgreich ausgeführt?"
	@which asciidoc >/dev/null \
	  || echo "Programm asciidoc nicht gefunden."
	@which a2x >/dev/null \
	  || echo "Programm a2x nicht gefunden."
	@(which flip || which unix2dos) >/dev/null \
	  || echo "Weder Programm flip noch unix2dos gefunden."
	@which pdflatex >/dev/null \
	  || echo "Programm pdflatex nicht gefunden."
	@which rsync >/dev/null \
	  || echo "Programm rsync nicht gefunden."
	@test -f src/FreeDoko 
	@which asciidoc >/dev/null
	@which a2x >/dev/null
	@(which flip || which unix2dos) >/dev/null
	@which pdflatex >/dev/null
	@which rsync >/dev/null

.PHONY: uninstall
uninstall :
	@test -f $(DESTDIR)$(BIN_DIRECTORY)/FreeDoko \
	  || (( echo "FreeDoko does not seems to be installed." && false ))
	@echo "Deleting the following files and directories:"
	@ls "$(DESTDIR)$(DESKTOP_FILE_DIRECTORY)/FreeDoko.desktop"
	@ls "$(DESTDIR)$(BIN_DIRECTORY)/FreeDoko"
	@ls "$(DESTDIR)$(BIN_DIRECTORY)/freedoko"
	@ls "$(DESTDIR)$(ICON_DIRECTORY)/FreeDoko.png"
	@ls "$(DESTDIR)$(MAN_DIRECTORY)/freedoko.6"
	@ls "$(DESTDIR)$(MAN_DIRECTORY)/FreeDoko.6"
	@ls "$(DESTDIR)$(LOCALE_DIRECTORY)/"*"/LC_MESSAGES/FreeDoko.mo"
	@ls -d "$(DESTDIR)$(HELP_DIRECTORY)/../freedoko"
	@ls -d "$(DESTDIR)$(DATA_DIRECTORY)"
	@ls -d "$(DESTDIR)$(HELP_DIRECTORY)"
	@echo "Waiting 10 seconds"
	@sleep 10
	@echo "Deleting files.."
	$(RM) "$(DESTDIR)$(DESKTOP_FILE_DIRECTORY)/FreeDoko.desktop"
	$(RM) "$(DESTDIR)$(BIN_DIRECTORY)/FreeDoko"
	$(RM) "$(DESTDIR)$(BIN_DIRECTORY)/freedoko"
	$(RM) "$(DESTDIR)$(ICON_DIRECTORY)/FreeDoko.png"
	$(RM) "$(DESTDIR)$(MAN_DIRECTORY)/freedoko.6"
	$(RM) "$(DESTDIR)$(MAN_DIRECTORY)/FreeDoko.6"
	$(RM) "$(DESTDIR)$(LOCALE_DIRECTORY)/"*"/LC_MESSAGES/FreeDoko.mo"
	$(RM) "$(DESTDIR)$(HELP_DIRECTORY)/../freedoko"
	$(RM) -r "$(DESTDIR)$(DATA_DIRECTORY)"
	$(RM) -r "$(DESTDIR)$(HELP_DIRECTORY)"
	@echo "...finished uninstall."
endif

# create the debian packages
# see 'setup/debian/Makefile'
ifeq ($(OPERATING_SYSTEM), Linux)
.PHONY: debian_packages
debian_packages :
	$(MAKE) -C setup/debian packages
endif

# create the FreeDoko CD
# uses a script
# needs the release files in ../FreeDoko_$(VERSION).release
ifeq ($(OPERATING_SYSTEM), Linux)
.PHONY: CD
CD :
	./bin/create_cd ${VERSION}
endif

ifeq ($(OPERATING_SYSTEM), Linux)
# transforms the text files in DOS-format
.PHONY: flip
flip :
	flip -mb AUTHORS COPYING README* Version \
	 || unix2dos AUTHORS COPYING README* Version
	$(MAKE) -C src/ flip
endif
