(short english text is below)

Heute veröffentlichen wir die Version 0.7.13 von FreeDoko.

Die InnoCard International GmbH hat unsere Lizenz für den Kartensatz um ein Jahr verlängert. Wir haben die Karten nun in besserer Qualität und in drei verschiedenen Größen vorliegen. Die Symbolsätze gibt es auch entsprechend in den drei Größen.

Einige Turniereinstellungen, die für das Nachspielen von Turnieren verwendet werden, sind standardmäßig ausgeblendet. Sie können über die Einstellung „Zusätzliche Turniereinstellungen” wieder eingeblendet werden.

Und natürlich haben wir weiter an der KI gearbeitet, insbesondere an den Ansagen, und einige Fehler korrigiert.

Viel Spaß beim Spielen vom Doppelkopf
Das FreeDoko Team

----

Today we release the version 0.7.13 of FreeDoko.

Changes:
* expanded license of the InnoCard cardsets to 31. Dezember 2015
* bigger and better card pictures
* some tournament settings are hidden as default
* ai improvements
* bug fixes

Have fun playing Doppelkopf
Your FreeDoko team
