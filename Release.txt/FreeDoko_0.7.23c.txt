Heute veröffentliche ich die Version 0.7.23c von FreeDoko.
Download: http://free-doko.sourceforge.net/

Die InnoCard International GmbH hat nachträglich die Kartensatzlizenz verlängert. Die Kartensätze stehen in den Einstellungen in der Rubrik Erscheinungsbild zur Verfügung.

Viel Spaß beim Spielen vom Doppelkopf
Diether Knof
FreeDoko-Entwickler
