Änderungen:
* Die Lizenz vom InnoCard-Kartensatz wurde bis Ende 2022 verlängert.
* Optionen können in der Datei „options“ angegeben werden. Damit lässt sich verhindern, dass FreeDoko Dateien außerhalb vom Programmverzeichnis ablegt.
* Einstellungen umstrukturiert und Icons ergänzt
* Neuer Symbole mit Schachfiguren
* Die Wiederholung eines Spiels kann optional mit gleicher Kartenverteilung für alle Spieler durchgeführt werden (in den Einstellungen Rubrik „Kartenverteilung“).
* Neue Einstellung „Mischen ohne Startwert“. Die ist für Personen, die der Meinung sind, dass von FreeDoko verwendete Zufallsgenerator schlecht ist. Damit werden die Karten mit dem Standardzufallsgenerator von C++ vorgemischt.
* Standardmäßig werden die offiziellen Kartengrenzen für die Ansagen verwendet. Individuelle Angaben müssen explizit aktiviert werden.
* Regeln entfernt: „Genscher“, „Gegenansage“ und „strikte Grenzen“
