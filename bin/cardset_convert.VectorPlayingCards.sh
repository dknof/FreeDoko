#!/bin/bash


# script to convert the SVG-Cards cardsets into the FreeDoko one
# uses imagemagick

# height of the destination cards
if [ $# -eq 0 ]; then
  echo "Argumente: Höhe der Karten (Zahl)"
  exit 1;
fi

# first argument:  source
# second argument: destination
function convert_card {
  convert -resize x${HEIGHT} \
          -background none \
	  $1 \
	  $2
}

function convert_back {
  convert -resize ${WIDTH}x${HEIGHT}! \
          -background none \
	  $1 \
	  $2
}

function create_cards {
convert_card cards/club_queen.svg    ${HEIGHT}/cards/club_queen.png &
convert_card cards/club_jack.svg     ${HEIGHT}/cards/club_jack.png &
convert_card cards/club_king.svg     ${HEIGHT}/cards/club_king.png &
convert_card cards/club_ace.svg      ${HEIGHT}/cards/club_ace.png &
convert_card cards/club_nine.svg     ${HEIGHT}/cards/club_nine.png &
convert_card cards/club_ten.svg      ${HEIGHT}/cards/club_ten.png &
convert_card cards/spade_queen.svg   ${HEIGHT}/cards/spade_queen.png &
convert_card cards/spade_jack.svg    ${HEIGHT}/cards/spade_jack.png &
convert_card cards/spade_king.svg    ${HEIGHT}/cards/spade_king.png &
convert_card cards/spade_ace.svg     ${HEIGHT}/cards/spade_ace.png &
convert_card cards/spade_nine.svg    ${HEIGHT}/cards/spade_nine.png &
convert_card cards/spade_ten.svg     ${HEIGHT}/cards/spade_ten.png &
convert_card cards/heart_queen.svg   ${HEIGHT}/cards/heart_queen.png &
convert_card cards/heart_jack.svg    ${HEIGHT}/cards/heart_jack.png &
convert_card cards/heart_king.svg    ${HEIGHT}/cards/heart_king.png &
convert_card cards/heart_ace.svg     ${HEIGHT}/cards/heart_ace.png &
convert_card cards/heart_nine.svg    ${HEIGHT}/cards/heart_nine.png &
convert_card cards/heart_ten.svg     ${HEIGHT}/cards/heart_ten.png &
convert_card cards/diamond_queen.svg ${HEIGHT}/cards/diamond_queen.png &
convert_card cards/diamond_jack.svg  ${HEIGHT}/cards/diamond_jack.png &
convert_card cards/diamond_king.svg  ${HEIGHT}/cards/diamond_king.png &
convert_card cards/diamond_ace.svg   ${HEIGHT}/cards/diamond_ace.png &
convert_card cards/diamond_nine.svg  ${HEIGHT}/cards/diamond_nine.png &
convert_card cards/diamond_ten.svg   ${HEIGHT}/cards/diamond_ten.png &

wait
}

function create_backs {
  cd ~/Daten/FreeDoko/orig/backs/xaos
  ./create_xaos.sh $((${WIDTH})) $((${HEIGHT}))
  cd -
  cp ~/Daten/FreeDoko/orig/backs/xaos/xaos.png ${HEIGHT}/backs/xaos.png

  cd ~/Daten/FreeDoko/orig/backs/tux
  ./create_tux.sh $((${WIDTH})) $((${HEIGHT}))
  cd -
  cp ~/Daten/FreeDoko/orig/backs/tux/tux.png ${HEIGHT}/backs/penguin.png

  add_black_border

  for f in backs/*.svg; do
    convert_back $f ${HEIGHT}/backs/$(basename $f .svg).png &
  done
  wait

}

function add_black_border
{
  cd ${HEIGHT}/backs/
  echo ${HEIGHT}x${WIDTH}
  CLUB_QUEEN=../cards/club_queen.png
  for f in *.png; do 
    convert $f -crop $((${WIDTH} - 2))x$((${HEIGHT} - 2))+1+1 +repage ${CLUB_QUEEN} -compose CopyOpacity -composite tmp.png
    convert tmp.png -bordercolor none -border 1 -background black -alpha background -channel A -blur 1x1 -level 0,7% $f
  done
  rm tmp.png
  cd -
}

function convert_vector_playing_cards {
  mkdir ${HEIGHT}/cards
  create_cards

  WIDTH=$(identify -format "%w" ${HEIGHT}/cards/club_queen.png)
  mkdir ${HEIGHT}/backs
  create_backs

  cd ../..
}

while [ $# -gt 0 ]; do
  HEIGHT=$1
  echo "Konvertiere Kartensatz VectorPlayingCards: Höhe $HEIGHT"

  rm -rf ${HEIGHT}
  mkdir ${HEIGHT}
  convert_vector_playing_cards
  pngcrushmin ${HEIGHT}/*/*.png
  shift
done
