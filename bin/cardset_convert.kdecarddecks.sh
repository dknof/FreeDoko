#!/bin/bash
#
# converts kdecarddecks to FreeDoko-cardsets
# uses imagemagick
#
# ./kdecarddecks-to-FreeDoko-cardsets  /usr/share/apps/carddecks/cards-*
#
# 18. June 2002
# Diether Knof

# copying the copyright
mkdir -p kdecarddecks
cp /usr/share/doc/kdegames-card-data/copyright kdecarddecks/

while [ $# -gt 0 ]; do
  if [ ! -d $1 ]; then
    echo \"$1\" is not a directory
    shift
    continue
  fi

  style=$(basename $1 | sed "s/^cards-//")

  if [ ! -f $1/1.png ]; then
    echo \"$1\" is not a kdecarddecks directory
    shift
    continue
  fi

  width=$(identify -format "%w" "$1/1.png")
  height=$(identify -format "%h" "$1/1.png")

  dir=kdecarddecks/${style}

  if [ -d ${dir} ]; then
    echo \"${dir}\" already exists
    shift
    continue
  fi



  echo "converting $1 to $dir"

  # copying the copyright

  mkdir -p ${dir}  

  cp $1/COPYRIGHT ${dir}/

  # converting the cards

  cp $1/1.png  ${dir}/cards/club_ace.png
  cp $1/17.png ${dir}/cards/club_ten.png
  cp $1/5.png  ${dir}/cards/club_king.png
  cp $1/9.png  ${dir}/cards/club_queen.png
  cp $1/13.png ${dir}/cards/club_jack.png
  cp $1/21.png ${dir}/cards/club_nine.png

  cp $1/2.png  ${dir}/cards/spade_ace.png
  cp $1/18.png ${dir}/cards/spade_ten.png
  cp $1/6.png  ${dir}/cards/spade_king.png
  cp $1/10.png ${dir}/cards/spade_queen.png
  cp $1/14.png ${dir}/cards/spade_jack.png
  cp $1/22.png ${dir}/cards/spade_nine.png

  cp $1/3.png  ${dir}/cards/heart_ace.png
  cp $1/19.png ${dir}/cards/heart_ten.png
  cp $1/7.png  ${dir}/cards/heart_king.png
  cp $1/11.png ${dir}/cards/heart_queen.png
  cp $1/15.png ${dir}/cards/heart_jack.png
  cp $1/23.png ${dir}/cards/heart_nine.png

  cp $1/4.png  ${dir}/cards/diamond_ace.png
  cp $1/20.png ${dir}/cards/diamond_ten.png
  cp $1/8.png  ${dir}/cards/diamond_king.png
  cp $1/12.png ${dir}/cards/diamond_queen.png
  cp $1/16.png ${dir}/cards/diamond_jack.png
  cp $1/24.png ${dir}/cards/diamond_nine.png

  # converting the decks

  mkdir -p ${dir}/backs

  for f in $(dirname $1)/decks/*.png; do
    if [ ${width} = $(identify -format "%w" $f) \
      -a ${height} = $(identify -format "%h" $f) ]; then
      cp $f ${dir}/backs/$(basename $f)
    fi
  done

  shift
done


exit 0
