#!/bin/bash

# script to convert the SVG-Cards cardsets into the FreeDoko one
# uses imagemagick

# height of the destination cards
if [ $# = 1 ]; then
  HEIGHT=$1
else
  for i in 140 185 240; do
    echo "== height: $i"
    "$0" $i
  done
  exit
fi

rm -rf png.${HEIGHT}

mkdir png.${HEIGHT}



# first argument:  source
# second argument: destination
function convert_card {
  convert -scale x${HEIGHT} \
          -background none \
	  $1 \
	  $2
} # function convert_card

function create_cards {
convert_card cards/club_queen.svg    png.${HEIGHT}/cards/club_queen.png &
convert_card cards/club_jack.svg     png.${HEIGHT}/cards/club_jack.png &
convert_card cards/club_king.svg     png.${HEIGHT}/cards/club_king.png &
convert_card cards/club_ace.svg      png.${HEIGHT}/cards/club_ace.png &
convert_card cards/club_nine.svg     png.${HEIGHT}/cards/club_nine.png &
convert_card cards/club_ten.svg      png.${HEIGHT}/cards/club_ten.png &
convert_card cards/spade_queen.svg   png.${HEIGHT}/cards/spade_queen.png &
convert_card cards/spade_jack.svg    png.${HEIGHT}/cards/spade_jack.png &
convert_card cards/spade_king.svg    png.${HEIGHT}/cards/spade_king.png &
convert_card cards/spade_ace.svg     png.${HEIGHT}/cards/spade_ace.png &
convert_card cards/spade_nine.svg    png.${HEIGHT}/cards/spade_nine.png &
convert_card cards/spade_ten.svg     png.${HEIGHT}/cards/spade_ten.png &
convert_card cards/heart_queen.svg   png.${HEIGHT}/cards/heart_queen.png &
convert_card cards/heart_jack.svg    png.${HEIGHT}/cards/heart_jack.png &
convert_card cards/heart_king.svg    png.${HEIGHT}/cards/heart_king.png &
convert_card cards/heart_ace.svg     png.${HEIGHT}/cards/heart_ace.png &
convert_card cards/heart_nine.svg    png.${HEIGHT}/cards/heart_nine.png &
convert_card cards/heart_ten.svg     png.${HEIGHT}/cards/heart_ten.png &
convert_card cards/diamond_queen.svg png.${HEIGHT}/cards/diamond_queen.png &
convert_card cards/diamond_jack.svg  png.${HEIGHT}/cards/diamond_jack.png &
convert_card cards/diamond_king.svg  png.${HEIGHT}/cards/diamond_king.png &
convert_card cards/diamond_ace.svg   png.${HEIGHT}/cards/diamond_ace.png &
convert_card cards/diamond_nine.svg  png.${HEIGHT}/cards/diamond_nine.png &
convert_card cards/diamond_ten.svg   png.${HEIGHT}/cards/diamond_ten.png &

wait
}

function create_backs {
  cd ~/FreeDoko/Kartensätze/backs/xaos
  ./create_xaos.sh ${WIDTH} ${HEIGHT}
  cd -
  cp ~/FreeDoko/Kartensätze/backs/xaos/xaos.png png.${HEIGHT}/backs/xaos.png

  cd ~/FreeDoko/Kartensätze/backs/tux
  ./create_tux.sh ${WIDTH} ${HEIGHT}
  cd -
  cp ~/FreeDoko/Kartensätze/backs/tux/tux.png png.${HEIGHT}/backs/penguin.png
}

function add_black_border
{
  cd png.${HEIGHT}/backs/
  CLUB_QUEEN=../cards/club_queen.png
  for f in *.png; do 
    convert $f ${CLUB_QUEEN} -compose CopyOpacity -composite tmp.png
    convert tmp.png -bordercolor none -border 1 -background black -alpha background -channel A -blur 1x1 -level 0,7% $f
  done
  rm tmp.png
  cd -

  cd png.${HEIGHT}/cards
  for f in *.png; do 
    mogrify -bordercolor none -border 1 -background black -alpha background -channel A -blur 1x1 -level 0,7% $f
  done
  cd -
}

function convert_svg_cards {
  echo "converting svg-cards cardset"

  mkdir png.${HEIGHT}/cards
  create_cards

  WIDTH=$(identify -format "%w" png.${HEIGHT}/cards/club_queen.png)
  mkdir png.${HEIGHT}/backs
  create_backs

  add_black_border

  exit

  mkdir png.${HEIGHT}/icons
  cd png.${HEIGHT}/icons
  ~/FreeDoko/bin/create_icons

  cd ../..
}

convert_svg_cards

pngcrushmin */*/*.png
