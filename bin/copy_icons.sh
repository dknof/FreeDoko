#!/bin/zsh

mkdir -p ../icons_filtered
cp index.theme ../icons_filtered
grep -o -h 'set_\(image_\)\?from_icon_name("[-a-z]\+"' ~/FreeDoko/src/**/*.cpp \
  | sed 's/set_\(image_\)\?from_icon_name("\([-a-z]\+\)"/\2/' \
| sort -u \
| while read f; do
  echo $f
  ls **/$f.* \
  | while read i; do
    mkdir -p $(dirname "../icons_filtered/$i")
    cp --dereference "$i" "../icons_filtered/$i"
  done
done
for i in **/gtk-*; do
  mkdir -p $(dirname "../icons_filtered/$i")
  cp --dereference "$i" "../icons_filtered/$i"
done
