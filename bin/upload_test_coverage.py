#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import glob
import datetime

from teamscale_client import TeamscaleClient
from teamscale_client.constants import CoverageFormats

TEAMSCALE_URL = "http://localhost:8080"

USERNAME = "build"
PASSWORD = "1bmX1Ti1KamBjbFBHoMx7AoDvMNFxDrY"

PROJECT_NAME = "freedoko"

if __name__ == '__main__':
    client = TeamscaleClient(TEAMSCALE_URL, USERNAME, PASSWORD, PROJECT_NAME)

    files = [ file for file in glob.glob("/home/dknof/FreeDoko/src/FreeDoko_coverage.xml")]

    client.upload_coverage_data(files, CoverageFormats.COBERTURA , datetime.datetime.now(), "Upload coverage", "test-partition")
