#!/bin/zsh

# Überprüfe die Referenzen

function print_help
{
  echo "Aufruf:"
  echo "  check_references"
  echo "  check_references --help"
  echo "  check_references [Verzeichnis]"
  echo "  check_references [Unterverzeichnis von references]"
}

function wrong_arguments
{
  echo "Falscher Aufruf" >&2
  print_help >&2
  exit -1
}

function parse_arguments
{
  if [ $# -eq 0 ]; then
    DIR="$PWD"
  elif [ $# -eq 1 ]; then
    if [ "$1" = "--help" ]; then
      print_help
      exit 0
    elif [ -d "$1" ]; then
      DIR="$PWD/$1"
    elif [ -d "$1" ]; then
      DIR="$PWD/$1"
    elif [ -d "heuristics/$1" ]; then
      DIR="$PWD/heuristics/$1"
    else
      wrong_arguments
    fi
  else
    wrong_arguments
  fi
}

function invalid_reference_files
{
  find -name "*.Reference.FreeDoko" \
    | sed "s,^\./,," \
    | while read f; do \
      < $f head -n 1 | grep -vq "FreeDoko Reference" && echo "$f"; \
    done
  }

function check_references_dir
{
  if [ ! -d "$DIR" ]; then
    echo "$DIR ist kein Verzeichnis" >&2
    print_help >&2
    exit -1;
  fi
  pushd "$DIR"
  # nach Bugreport.FreeDoko suchen
  x=$(find -name "*.BugReport.FreeDoko" | sed "s,^\./,,")
  if [ -n "$x" ]; then
    echo "Im Referenzverzeichnis existieren Dateien *.BugReport.FreeDoko:"
    echo "$x"
    exit -1
  fi
  x=$(grep -l "[C]HECK" . -r)
  if [ -n "$x" ]; then
    echo "Im Referenzverzeichnis existieren Dateien mit „CHE""CK“:"
    echo "$x"
    exit -1
  fi
  x=$(invalid_reference_files)
  if [ -n "$x" ]; then
    echo "Im Referenzverzeichnis existieren Dateien ohne „FreeDoko Reference“:"
    echo "$x"
    exit -1
  fi
  popd >/dev/null
}

function compile_FreeDoko
{
  test -f FreeDoko && rm FreeDoko

  #OBJDIR=/tmp/${LOGNAME}/FreeDoko.check_reference/src
  OBJDIR=.
  mkdir -p "$OBJDIR"
  rm "$OBJDIR/**/*.gcda"

export CXXFLAGS="-O3 -std=c++17 -pipe -Werror -Wall -Wextra -Wno-empty-body -Wno-unused-parameter -Wno-unused-command-line-argument -Wno-return-std-move"
#export CXXFLAGS="-O0 -ggdb -std=c++17 -pipe -Werror -Wall -Wextra -Wno-empty-body -Wno-unused-parameter -Wno-unused-command-line-argument -Wno-return-std-move"
make -C ../src \
  USE_UI_GTKMM=false \
  USE_UI_TEXT=false \
  USE_SOUND=false \
  OBJDIR="$OBJDIR" \
  FreeDoko

test -f ../src/FreeDoko || exit -1
}

function run_references
{
  pushd ../data
  test -f core && rm core
  ../src/FreeDoko -r "$DIR" --ui=none
  #valgrind --gen-suppressions=yes ../src/FreeDoko -r "$DIR" --ui=none

  test -f core && exit -1
  popd >/dev/null
}

function print_check_summary
{
  grep -h -v OK References.report.csv \
    | grep "^[^;]*[0-9]\+\(_[0-9]\+\)\?.Reference.FreeDoko" \
    | sort -n

  tail -n 5 References.report.csv \
    | sed "s/\;/\t/g";
  }

function count_references
{
  find . -name "*.Reference.FreeDoko" \
    | wc -l
  }

function get_unchecked_referencs
{
  find . -name "*.Reference.FreeDoko" \
    | sed "s,^\./,," \
    | while read f; do
      grep -q "^$f;" References.report.csv || echo "$f";
    done
  }

function count_unchecked_references
{
  get_unchecked_referencs | wc -l
}

function count_checks
{
  grep "^[ \t]*check" **/*.Reference.FreeDoko \
    | wc -l
  }

function count_broken
{
  <References.report.csv grep "Reference.FreeDoko" | grep -v "_broken" | grep -v "OK" | wc -l
}

function get_new_broken
{
  sed "$DIR"/References.report.csv \
    -e "1d" \
    -e "/^\([^;]*;\)\{3\}OK;/d" \
    -e "/^$/d" \
    -e "/^files;/d" \
    -e "/^checks;/d" \
    -e "/^success;/d" \
    -e "/^failed;/d" \
    -e "s/;.*$//" \
    | grep -v "_broken/"
}

function get_new_broken_files
{
  sed "$DIR"/References.report.csv \
    -e "1d" \
    -e "/^\([^;]*;\)\{3\}OK;/d" \
    -e "/^$/d" \
    -e "/^files;/d" \
    -e "/^checks;/d" \
    -e "/^success;/d" \
    -e "/^failed;/d" \
    -e "s/;.*$//" \
    | grep -v "_broken/" \
    | sort -u
}

function count_new_broken
{
  get_new_broken | wc -l
}

function print_broken_count
{
  x=$(count_broken)
  if [ ! "$x" -eq 0 ]; then
    printf "Kaputt (alt):       %4d\n" $x
  else
    printf "\e[01;32m"
    printf "Kaputt (alt):       %4d\n" 0
    printf "\e[;0m"
  fi
}

function print_new_broken_count
{
  x=$(count_new_broken)
  if [ ! "$x" -eq 0 ]; then
    printf "\e[01;31m"
    printf "Kaputt (neu):       %4d\n" $x
    echo "$(get_new_broken)" | sed "s|^|  $DIR/|"
    printf "\e[;0m"
  else
    printf "\e[01;32m"
    printf "Kaputt (neu):       %4d\n" 0
    printf "\e[;0m"
  fi
}

function get_fixed
{
  find . -name "*.Reference.FreeDoko" | sed -e "/_broken/!d" -e "s,^\./,," \
    | while read f; do
      #grep -q "$f" /tmp/FreeDoko.References.failed/References.failed || echo "$f"
      <References.report.csv grep "$f" | grep -qv "OK" || echo "$f"
    done
  }

function print_fixed_count
{
  # Die wieder funktionierenden kaputten Referenzen anzeigen
  x=$(get_fixed)
  if [ -n "$x" ]; then
    printf "\e[01;32m"
    printf "Wieder heil:        %4d\n" $(echo $x | wc -l)
    echo "$x" | sed "s|^|  $DIR/|"
    printf "\e[;0m"
  fi
}

function copy_failed_references
{
  pushd "$DIR"
  rm -rf /tmp/FreeDoko.References.failed
  mkdir /tmp/FreeDoko.References.failed
  sed References.report.csv \
    -e "1d" \
    -e "/^\([^;]*;\)\{3\}OK;/d" \
    -e "/^$/d" \
    -e "/^files;/d" \
    -e "/^checks;/d" \
    -e "/^success;/d" \
    -e "/^failed;/d" \
    -e "s/;.*$//" \
    >/tmp/FreeDoko.References.failed/References.failed

  for f in References.report.csv $(cat /tmp/FreeDoko.References.failed/References.failed); do
    mkdir -p /tmp/FreeDoko.References.failed/"$(dirname $f)"
    cp "$f" "/tmp/FreeDoko.References.failed/$f"
  done
  popd >/dev/null
}

function parse_references {
  pushd "$DIR"
  #print_check_summary
  #echo
  if [ ! $(count_unchecked_references) -eq 0 ]; then
    printf "Dateien gesamt:        %4d\n" $(count_references)
    printf "Dateien nicht geprüft: %4d\n" $(count_unchecked_references)
    exit -1
  fi
  printf "Dateien gesamt:     %4d\n" $(count_references)
  printf "Einträge überprüft: %4d\n" $(count_checks)
  print_broken_count
  print_new_broken_count
  print_fixed_count
  popd >/dev/null
}

function print_coverage
{
  gcovr --print-summary --root . "$OBJDIR" | grep "^TOTAL" | sed -E "s/^TOTAL +([0-9]+) +([0-9]+) +([0-9]+%)$/Coverage: \2\/\1 = \3/"
}


parse_arguments "$@"
check_references_dir
compile_FreeDoko
SECONDS=0
echo "run references"
run_references
#echo "copy failed references"
#copy_failed_references
#echo "parse references"
parse_references
echo
echo "Laufzeit: ${SECONDS} Sekunden"

#print_coverage

exit $(count_new_broken)
