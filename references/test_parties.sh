#!/bin/zsh

# Startet FreeDoko mit den einzelnen Turnieren

function compile_FreeDoko
{
  test -f FreeDoko && rm FreeDoko

  #OBJDIR=/tmp/${LOGNAME}/FreeDoko.check_reference/src
  OBJDIR=.
  mkdir -p "$OBJDIR"
  rm "$OBJDIR/**/*.gcda"

export CXXFLAGS="-O3 -std=c++17 -pipe -march=native -Werror -Wall -Wextra -Wno-empty-body -Wno-unused-parameter -Wno-unused-command-line-argument -Wno-return-std-move"
#export CXXFLAGS="-O0 -ggdb -std=c++17 -pipe -march=native -Werror -Wall -Wextra -Wno-empty-body -Wno-unused-parameter -Wno-unused-command-line-argument -Wno-return-std-move"
make -C ../src \
  USE_UI_GTKMM=true \
  USE_UI_TEXT=false \
  USE_SOUND=false \
  OBJDIR="$OBJDIR" \
  FreeDoko

test -f ../src/FreeDoko || exit -1
}

function run_references
{
  pushd ../data
  test -f core && rm core
  for f in ../references/parties/*; do
    echo -n "* "
    basename "$f"
    ../src/FreeDoko -F $((4+8+16+32)) "$f"
  done

  test -f core && exit -1
  popd >/dev/null
}

compile_FreeDoko
echo "Rufe Referenzturniere auf"
run_references
echo "Fertig"
