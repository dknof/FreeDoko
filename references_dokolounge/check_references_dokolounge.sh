#!/bin/zsh

REFERENCES_REPOSITORY="."
print_diff=false

CXXFLAGS="-Wall -Werror -Wno-parentheses -pipe -std=c++17 -O0 -ggdb -fprofile-arcs -ftest-coverage"

while [ $# -gt 0 ]; do
  if [ "$1" = "--print-diff" ]; then
    print_diff=true
    shift
  else
    echo "Wrong usage: ./check_references [--print-diff]" >&2
    exit -1
  fi
done

#make WEBSOCKETS_DOKOLOUNGE_SERVICE=true -C src FreeDoko
trap "test -f core && rm core" EXIT

count_ref=0
count_ref_good=0
count_ref_failed=0

find . -name "*.dokolounge" \
  | sort \
  | while read reference; do
  echo "checking reference ${reference} ..."
  count_ref=$((${count_ref} + 1))
  reference_output_ref="$(dirname $reference)/$(basename $reference .dokolounge)".ref
  reference_output="$(dirname $reference)/$(basename $reference .dokolounge)".out

  ../src/FreeDoko --debug write --debug gameplay --debug -codeline file "$reference" 2>&1 \
    | grep -v "tischchat" \
    | grep -v "Fehlerbericht" \
    | sed "s/^[^ ]*.cpp#[0-9]*  //" \
    > "${reference_output}"

  if [ ! -f "${reference_output_ref}" ]; then
    count_ref_failed=$((${count_ref_failed} + 1))
    echo -n -e "\e[01;31m"
    echo "... ref missing"
    echo -n -e "\e[;0m"
  elif cmp --silent <(< "${reference_output}" tr -d "'") <(< "${reference_output_ref}" tr -d "'"); then
    count_ref_good=$((${count_ref_good} + 1))
    echo -n -e "\e[01;32m"
    echo "... good"
    echo -n -e "\e[;0m"
  else
    #diff ${reference_output} ${reference_output_ref}
    if [ "${print_diff}" = "true" ]; then
      diff <(< "${reference_output}" tr -d "'") <(< "${reference_output_ref}" tr -d "'");
    fi
    count_ref_failed=$((${count_ref_failed} + 1))
    echo -n -e "\e[01;31m"
    echo "... failed"
    gvimdiff "${reference_output}" "${reference_output_ref}"
    echo -n -e "\e[;0m"
  fi
done

echo
echo -n -e "\e[01;1m"
echo "Summary"
echo -n -e "\e[;0m"
echo "total:  ${count_ref}"
if [ ${count_ref_failed} -eq 0 ]; then
  echo -n -e "\e[01;32m"
  echo "good:   ${count_ref_good}"
  echo -n -e "\e[;0m"
  echo "failed: ${count_ref_failed}"
else
  echo "good:   ${count_ref_good}"
  echo -n -e "\e[01;31m"
  echo "failed: ${count_ref_failed}"
fi
echo -n -e "\e[;0m"

#gcovr --print-summary src | grep "^TOTAL" | sed -E "s/^TOTAL +([0-9]+) +([0-9]+) +([0-9]+%)$/Coverage: \2\/\1 = \3/"

exit ${count_ref_failed}
