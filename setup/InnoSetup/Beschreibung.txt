FreeDoko ist eine freie Implementierung des Kartenspiels 'Doppelkopf', hauptsächlich programmiert von Diether Knof.
Das Programm steht unter der GNU General Public License Version 3 (https://www.gnu.org/licenses/gpl-3.0) oder später. Die Benutzung erfolgt auf eigene Verantwortung.

Die Internetseite von FreeDoko lautet http://free-doko.sourceforge.net.
