#%s du bist falsch programmiert ;)

lern doko,? %s|%s lern doko
|%s,? (du |der |die )?(Pfeife|Null|Pappnase|Vollpfosten|blöder?|(blöder )?[Ss]ack|Looser|Depp|Flasche|(blöder|doofer) ? Vogel)
|%s (ist )?(doo+f|bö+se|bescheuert|ne Pfeife)\.?
|(doo+fe?r?|dämlicher|Blödmann,?|Idiot,?|[Bb]löder?|böser?|Sack|Depp|Flasche) %s
|Du bist doo+f,? %s\.?
|%s,? du (bist|spielst) (doo+f|bekloppt)
|%s,? du bist ein depp
|%s spielte? wie ein (Anfänger|Vollidiot)\.?
|%s (Du kannst|kann) (nix|nichts)\.?
|(Der |Die )?%s ist (doo+f|blöd|dämlich|bescheuert)
|%s,? bist Du (doo+f|blöd|dämlich|bescheuert)(\.?|\?*)
|%s,? [Dd]u (bist )?(echt )?(so )?(doo+f|blöd|dämlich|bescheuert|bekloppt)\.?
|(So ein )?(doo+fer|(sau ?)?blöder|dämlicher|bescheuerter) %s!?
|%s,? spinnst Du\??
|Der Pinguin spinnt doch!?
|Der Pinguin spielt (völlig )?bescheuert\.?
|%s spinnt[.!]?
|%s ist (wohl )?falsch programmiert.?
|%s,? du spielst (unter aller sau|Mist|gruselig)!?
|%s,? du kannst (ja )?nix
|%s,? schlechter kann man (kaum|nicht) spielen
|(Trottel|Idioten|Schei(ss|ß)) %s
|%s Du (Trottel|Knallkopp|Idiot)
|Dämlich,? der %s
|%s,? die Schwachsinnsnelke
|%s,? spielt Müll+
|%s,? Du bist eine (Flasche|Pissnelke)(\.|!)*
|%s (sind|war|ist) (ja )?(alle )?(so )?(doo+f|dumm)\.?!?
|%s Du dummer Pinguin
|%s = doof
|Was spielen die blöden Vögel dämlich
|%s ist (schon )?ein blöder Vogel
|%s spielt Banane
|%s (Du )?Schnepfe
<Beschimpfung>
<Beschimpfung>
<Beschimpfung>
<Beschimpfung>
<Beschimpfung>
<Beschimpfung>
<Beschimpfung>
<Beschimpfung>
<Beschimpfung>
<Beschimpfung>
<Beschimpfung>
<Beschimpfung>
<Beschimpfung>
<Eingeschnappt>

Schnauze %s
|%s,? Schnauze
Nö
Bla Bla Bla

%s fliegt gleich\.*
|%s wird gekündigt
|Du spielst schei(ss|ß|xx)(e|dumm),? %s
|%s (ist|spielt) (ober)?schei(ss|ß|xx)(e|dumm)
|%s,? du (spielst|bist( so)?) (ober)?schei(ss|ß|xx)(e|dumm)!?
|%s = (ober)?schei(ss|ß|xx)(e|dumm)!?
|%s,? leck mich
|(So ein )?(sau ?blöder|bescheuerter) %s!?
|(Der |Die )?%s ist (sau ?blöd|bescheuert)
|%s,? bist Du (sau ?blöd|bescheuert)(\.?|\?*)
|%s,? [Dd]u (bist )?(sau ?blöd|bescheuert)\.?
|%s,? [Dd]u (bist )?(eine? )?(Pissnelke|Schlampe)\.?
|%s,? [Dd]u (bist )?(eine? )?(Arsch|Pissnelke|Schlampe|Hurenbock)\.?
|(So ein )?(doo+fer|(sau ?)?blöder|bescheuerter) %s!?
|Kack %s
|%s das war Kacke
|%s (Du )?[Kk]ackvogel
|(Der Pinguin|%s) stinkt!?
<Beschimpfung>
<Eingeschnappt>
<Eingeschnappt>

%s,? (gehe? Hände waschen|wasch Dir (mal )?die Hände)
Dann solltest du mal abheben, %s.
Ich habe sie sogar anschließend desinfiziert. Oder war das zuviel des Guten?
Mit oder ohne Seife?
Die Hände? Ich dachte, die Füße sollte ich waschen.
Das habe ich doch schon letzte Woche.
Immer dieser Sauberkeitsfimmel...
Das sagt mir meine Mutter auch immer.
Dreckig ist meine natürliche Hautfarbe, die kenne ich nicht anders.
Aber ich habe doch gar nicht gegeben.
Hast Du schonmal einen Pinguin gesehen, der Hände wäscht?


<Beschimpfung>
Ich habe den Eindruck, Du bist nicht so erfreut über meine Spielweise.
Statt mich zu beschimpfen schreibe mir lieber einen konstruktiven Vorschlag.
Wie kann ich denn besser spielen? 
Was hätte ich denn sonst machen sollen?
Immer diese Meckerer 🐐🐐🐐 
:brokenheart:


%s[,:]? [Ff]ick dich
|[Ff]ick dich,? %s
|[Ff]ick die %s( :\))?
|[Ff]ick dir %s
|Bück dich,? %s
|%s,? bück dich
Aber nicht hier am Tisch!
<Beschimpfung>
<Eingeschnappt>
<Eingeschnappt>


%s wird erschossen
|kill %s
|%s ich kill dich
<Beschimpfung>
<Eingeschnappt>
<Eingeschnappt>


<Eingeschnappt>
Ich habe jetzt keine Lust mehr weiterzuspielen.
Diesen Umgang lasse ich mir nicht länger gefallen.
Ich hoffe, das nächste Mal kannst Du Dich besser benehmen.
Pinguine sind mehr für einen freundlichen Umgangston.
Ich spiele lieber an einem freundlichen Tisch weiter.

+<Eingeschnappt>
!TischVerlassen
