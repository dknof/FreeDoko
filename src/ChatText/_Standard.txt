# Syntax
# Raute beginnt einen Kommentar
# Blöcke werden mit Leerzeilen getrennt
# Jeder Block beginnt mit einem Kennwort.
# Ein Kennwort ist
# * Ein regulärer Ausdruck (extended POSIX regular expression)
# * Ein "*" für alle Tage
# * Ein Datum (mit oder ohne Jahr) oder ein Tag (wie Nikolaus) oder ein Wochentag
# * Steht im Kennwort ein "%s", dann wird das als der Spielernamen, der im Chat eingeht, angesehen.
# * Ein "+" vor dem Kennwort steht für eine zusätzliche Zeile für die Ausgabe. Also aus "Hallo" und "+Hallo" ergeben sich zwei Zeilen.
# * Weiter Zeilen, die mit | anfangen sind Varianten vom Kennwort (also eins\n|zwei entspricht als regulärer Ausdruck (eins|zwei))
# * Noch vorher kann ein Prozentsatz stehen für die Wahrscheinlichkeit, dass dieser Block berücksichtigt wird.
# Beispiele:
#    *
#    Hallo
#    06.12.2022
#    24.12.
#    Nikolaus
#    Sonntag
#    Hallo %s
#    +Hallo
#    50% Hallo
#    50% +Hallo
# Danach folgt im Block optional "> Name" für Begrüßungen für bestimmte Personen oder "> *" nur für Personen.
# Anschließend werden die Textzeilen angegeben.
# %s in einer Textzeile steht für den Namen der Person, die angesprochen wird.
# Vor dem Text kann ein Zeitraum stehen:
#   -12:00      bis 12 Uhr
#   +12:00      ab 12 Uhr
#   12:00-14:00 zwischen 12 und 14 Uhr
# Kombinationen werden mit ~1, ~2, ... vorangestellt. Es werden dann jeweils eines aus ~1, eines aus ~2, ... verwendet
# "~" werden durch Leerzeichen ersetzt
# Eine Zeile nur mit "$" wird durch leeren Text ersetzt
# "!clear" als Eintrag löscht alle vorigen passenden Textbausteine. Damit kann auf Besonderheiten eingegangen werden.
# "<Kennwort>" als Eintrag wird durch einen Eintrag zum Kennwort "<Kennwort>" ersetzt (rekursives Ersetzen, Achtung wegen Endlosschleife). Beispiel siehe Geburtstag.txt
#

# ToDo
# * Leerzeichen anders darstellen
# * Leere Einträge
# * Freunde

# Smileys:
#
# 😀 :)
# 😊 :freuen:
# 🙁 :(
# 😉 ;)
# 😃 :D
# 🤕 :krank:
# 🙄 :rollingeyes:
# 😅 ^^
# 😍 :augenherz:
# :1f633:
# 😷 :1f637:
# :1f928:
# 🤔 :1f914:
# 🤗 :1f917:
# 😠 V:-I
# 😡 :@
# 😘 ;*
# 😎 :cool:
# 🤢 :übel:
# 😢 :,(
# 😴 (-.-)
# <3
# 💘 :herz2:
# 😜 :P
# :1f937-1f3fb-2640:
# :1f937-1f3fb-2642:
# :frauwinken:
# :mannwinken:
# 👍 :daumenhoch:
# 👏 :beifall:
# 👌 :prima:
# 🖖 :spock:
# :daumen:
# :victory:
# 🙏 :danke:
# 🎷 :sax:
# :pokal:
# 💪 :muskel:
# 🧀 :käse:
# :mond:
# :sonne:
# :sekt:
# 🥂 :sekt1:
# :blumen:
# 🐌 :schnecke:
# 🐧 :tux:
# :male:
# :female:
# :m+w:
# 🙈 :blind:
# 🙉 :taub:
# 🤡 :clown: 

Danke,? %s|%s,? Danke
Bitte, %s
Gern geschehen, %s
Gerne, %s
Gerne, immer wieder
Das mache ich doch gerne

Gute Nacht!?(,? %s)?
~1 Gute Nacht
~1 Schlaf schön
~2 , %s
~3
~3  🛌
~3  (-.-)
~3  :mond:


%s (ruhe|schweige|still|sei still|leise)
| (ruhe|schweige|still|sei still|leise),? %s
| %s chat (aus|stumm)
| chat (aus|stumm),? %s
<still>

# Reden ist hardkodiert, da der Chatter sonst nicht darauf reagieren kann.
#%s (sprich|reden|laut)
#!ChatOn
#<reden>

<still>
Ich bin ab jetzt leise.
Wie Du willst, %s.
Zu Befehl, %s.
Mmmmmmmmmmhhhh...

+<still>
!ChatOff

<reden>
Danke, %s, dass ich wieder reden darf.
:)
Bla, bla, bla, ...

+<reden>
!ChatOn


!include Hallo.txt
!include Feiertage.txt
!include Geburtstage.txt
!include Spielaktionen.txt
!include Fehlerbericht.txt
!include Beschimpfung.txt
!include Lob.txt
!include Sonstiges.txt
!include Witz.txt
