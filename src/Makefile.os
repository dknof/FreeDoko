include $(DEPTH)/../Makefile.os

include $(DEPTH)/Makefile.local
include $(DEPTH)/Makefile.modules

ifeq ($(REST_SERVICE), true)
  override WEBSOCKETS_DOKOLOUNGE_SERVICE = false
  override USE_UI_GTKMM = false
  override USE_SOUND    = false
endif
ifeq ($(WEBSOCKETS_DOKOLOUNGE_SERVICE), true)
  override REST_SERVICE = false
  override USE_UI_GTKMM = false
  override USE_SOUND    = false
endif

ifeq ($(OPERATING_SYSTEM), Linux)
	# program name
	PROGRAM ?= FreeDoko
	# compiler
	CXX ?= g++
	# strip program
	STRIP ?= strip
	# flags for the compiler (warnings and optimizations)
	#CXXFLAGS ?= -Wall -Wno-unused-value -Wno-parentheses -pipe -std=c++17 -O0 -ggdb
	CXXFLAGS ?= -std=c++17 -O3
	# flags for automatic generating of the dependencies
	#DEPGEN_FLAGS ?= -MMD -MP
	DEPGEN_FLAGS ?= -MMD -MP -MT $@ -MF $(@:.o=.d)
	override CPPFLAGS_FREEDOKO += -DLINUX
	ifeq ($(USE_UI_GTKMM), true)
		INCLUDE_GTKMM ?= $(shell pkg-config --cflags gtkmm-3.0)
		LIBS_GTKMM ?= $(shell pkg-config --libs gtkmm-3.0)
	endif
	ifeq ($(USE_SOUND_ALUT), true)
		INCLUDE_SOUND_ALUT ?= $(shell pkg-config --cflags freealut)
		LIBS_SOUND_ALUT ?= $(shell pkg-config --libs freealut)
	endif
	ifeq ($(USE_THREADS), true)
		override LIBS += -lpthread
	endif
	ifeq ($(REST_SERVICE), true)
		override LIBS += $(shell pkg-config --libs libmicrohttpd)
	endif
	ifeq ($(WEBSOCKETS_DOKOLOUNGE_SERVICE), true)
		override LIBS += $(shell pkg-config --libs libwebsockets)
	endif
endif

ifeq ($(OPERATING_SYSTEM), Linux_to_Windows)
	MINGW_DIR ?= /usr/x86_64-w64-mingw32
	PROGRAM ?= FreeDoko.exe
	CXX ?= i686-w64-mingw32-g++
	STRIP ?= strip
	CXXFLAGS ?= -Wall -std=c++17 -march=pentium -O2
	DEPGEN_FLAGS = -MMD -MP
	override CPPFLAGS_FREEDOKO += -DWINDOWS
	#C_INCLUDE_PATH ?= $(MINGW_DIR)/include
	#LIBRARY_PATH ?= $(MINGW_DIR)/lib
	#INCLUDE ?= -I$(MINGW_DIR)/include
	#LIBS ?= -I$(MINGW_DIR)/lib
	ifeq ($(USE_UI_GTKMM), true)
#		INCLUDE_GTKMM ?= -I$(MINGW_DIR)/include/gtkmm-2.0 -I$(MINGW_DIR)/lib/gtkmm-2.0/include -I$(MINGW_DIR)/include/gtk-2.0 -I$(MINGW_DIR)/lib/sigc++-1.2/include -I$(MINGW_DIR)/include/sigc++-1.2 -I$(MINGW_DIR)/include/glib-2.0 -I$(MINGW_DIR)/lib/glib-2.0/include -I$(MINGW_DIR)/lib/gtk-2.0/include -I$(MINGW_DIR)/include/pango-1.0 -I$(MINGW_DIR)/include/atk-1.0
#		LIBS_GTKMM ?= -L$(MINGW_DIR)/lib -lgtkmm-2.0 -lgdkmm-2.0 -latkmm-1.0 -lgtk-win32-2.0 -lpangomm-1.0 -lglibmm-2.0 -lsigc-1.2 -lgdk-win32-2.0 -latk-1.0 -lgdk_pixbuf-2.0 -lpangowin32-1.0 -lgdi32 -lpango-1.0 -lgobject-2.0 -lgmodule-2.0 -lglib-2.0
		#INCLUDE_GTKMM ?= $(shell pkg-config --cflags gtkmm-3.0)
		#LIBS_GTKMM ?= $(shell pkg-config --libs gtkmm-3.0)
	endif
	ifeq ($(USE_THREADS), true)
		override LIBS += -lpthread
	endif
	override LDFLAGS += -rdynamic
endif

ifeq ($(OPERATING_SYSTEM), FreeBSD)
	PROGRAM ?= FreeDoko
	CXX ?= c++
	STRIP ?= strip
	CXXFLAGS ?= -Wall -O2 -std=c++17
	DEPGEN_FLAGS ?= -MMD -MP
	override CPPFLAGS_FREEDOKO += -DLINUX
	ifeq ($(USE_UI_GTKMM), true)
		INCLUDE_GTKMM ?= $(shell pkg-config --cflags gtkmm-3.0)
		LIBS_GTKMM ?= $(shell pkg-config --libs gtkmm-3.0)
	endif
	ifeq ($(USE_THREADS), true)
		override LIBS += -lpthread
	endif
endif

ifeq ($(OPERATING_SYSTEM), HPUX)
	PROGRAM ?= FreeDoko
	CXX ?= g++
	STRIP ?= strip
	CXXFLAGS ?= -Wall -O2 -std=c++17
	DEPGEN_FLAGS ?= -MMD -MP
	LIBS ?= -Wl,+s
	override CPPFLAGS_FREEDOKO += -DHPUX
	ifeq ($(USE_UI_GTKMM), true)
		INCLUDE_GTKMM ?= $(shell pkg-config --cflags gtkmm-3.0)
		LIBS_GTKMM ?= $(shell pkg-config --libs gtkmm-3.0)
	endif
	ifeq ($(USE_THREADS), true)
		override LIBS += -lpthread
	endif
endif

ifeq ($(OPERATING_SYSTEM), MACOSX)
	PROGRAM ?= FreeDoko
	CXX ?= g++
	STRIP ?= strip
	CXXFLAGS ?= -Wall -O2 -std=c++17
	DEPGEN_FLAGS ?= -MMD -MP
	override CPPFLAGS_FREEDOKO += -DLINUX
	override CPPFLAGS_FREEDOKO += -DOSX
	ifeq ($(USE_UI_GTKMM), true)
		INCLUDE_GTKMM ?= $(shell pkg-config --cflags gtkmm-3.0)
		LIBS_GTKMM ?= $(shell pkg-config --libs gtkmm-3.0)
	endif
	ifeq ($(USE_THREADS), true)
		override LIBS += -lpthread
	endif
endif

ifeq ($(OPERATING_SYSTEM), MSYS)
	# program name
	PROGRAM ?= FreeDoko.exe
	# compiler
	CXX ?= g++
	# strip program
	STRIP ?= strip
	# flags for the compiler (warnings and optimizations)
	CXXFLAGS ?= -Wall -Wno-unused-command-line-argument -Wno-unused-value -pipe -O2 -std=c++17
	# flags for automatic generating of the dependencies
	DEPGEN_FLAGS ?= -MMD -MP -MT $@ -MF $(@:.o=.d)
	ifeq ($(USE_UI_GTKMM), true)
		INCLUDE_GTKMM ?= $(shell pkg-config --cflags gtkmm-3.0)
		LIBS_GTKMM ?= $(shell pkg-config --libs gtkmm-3.0)
	endif
	ifeq ($(USE_SOUND_ALUT), true)
	# does not exists
		#INCLUDE_SOUND_ALUT ?= $(shell pkg-config --cflags freealut)
		#LIBS_SOUND_ALUT ?= $(shell pkg-config --libs freealut)
	endif
	ifeq ($(USE_THREADS), true)
		override LIBS += -lpthread
	endif
	ifeq ($(REST_SERVICE), true)
		override LIBS += -lmicrohttpd
		override LIBS += -lpthread
		override LIBS += -lintl
	endif
	ifeq ($(WEBSOCKETS_DOKOLOUNGE_SERVICE), true)
		override LIBS += -lwebsockets
		override LIBS += -lpthread
		override LIBS += -lintl
	endif
endif

ifeq ($(OPERATING_SYSTEM), MSYS)
	MINGW_DIR ?= C:/MinGW32
	PROGRAM ?= FreeDoko.exe
	CXX ?= $(MINGW_DIR)/bin/g++
	STRIP ?= $(MINGW_DIR)/bin/strip
	CXXFLAGS ?= -Wall -O2 -march=pentium -mno-cygwin -mms-bitfields -std=c++17
	DEPGEN_FLAGS ?= -MMD -MP
	override CPPFLAGS_FREEDOKO += -DWINDOWS
	C_INCLUDE_PATH ?= D:/mingw32/include
	LIBRARY_PATH ?= D:/mingw32/lib
	ifeq ($(USE_UI_GTKMM), true)
		INCLUDE_GTKMM ?= $(shell pkg-config --cflags gtkmm-3.0)
		LIBS_GTKMM ?= $(shell pkg-config --libs gtkmm-3.0)
		# because of error with auto-import (see 'ld'-documentation for 'enable-auto-import'
		LIBS_GTKMM += -Wl,-enable-runtime-pseudo-reloc
	endif
	ifeq ($(USE_THREADS), true)
		override LIBS += -lpthread
	endif
endif

ifeq ($(OPERATING_SYSTEM), Windows)
	MINGW_DIR ?= C:\MinGW32
	PROGRAM ?= FreeDoko.exe
	CXX ?= $(MINGW_DIR)\bin\g++
	STRIP ?= $(MINGW_DIR)\bin\strip
#	# the '-w' is used instead of '-Wall' because else I get warnings like
#	#     choosing ... over ...
#	#     because conversion sequence for the argument jis better
	CXXFLAGS ?= -w -O2 -march=pentium -mno-cygwin -mms-bitfields -std=c++17
#	#CXXFLAGS ?= -Wall -O2 -march=pentium -mno-cygwin -mms-bitfields -std=c++17
	DEPGEN_FLAGS ?= -MMD -MP
	override CPPFLAGS_FREEDOKO += -DWINDOWS
	C_INCLUDE_PATH ?= $(MINGW_DIR)/include
	LIBRARY_PATH ?= $(MINGW_DIR)/lib
#	# for a windows application (without DOS-prompt)
	#override LDFLAGS += -mwindows
	ifeq ($(USE_UI_GTKMM), true)
#		# version 2.8
#		# adjusted output of 'pkg-config --cflags gtkmm-2.4'
		INCLUDE_GTKMM ?= $(shell pkg-config --cflags gtkmm-2.4)
		LIBS_GTKMM ?= $(shell pkg-config --libs gtkmm-2.4)
#		# because of error with auto-import (see 'ld'-documentation for 'enable-auto-import'
		LIBS_GTKMM += -Wl,-enable-runtime-pseudo-reloc
	endif
	ifeq ($(USE_SOUND_ALUT), true)
		INCLUDE_SOUND_ALUT ?= -I$(MINGW_DIR)/include/alut
		LIBS_SOUND_ALUT ?= -L$(MINGW_DIR)/lib \
				   -lalut -lopenal32
	endif
	ifeq ($(USE_SOUND_PLAYSOUND), true)
		LIBS_SOUND_PLAYSOUND ?= -L$(MINGW_DIR)/lib \
				        -lwinmm
	endif
	ifeq ($(USE_THREADS), true)
		override LIBS += -lpthreadGCE2
	endif
endif
ifeq ($(REST_SERVICE), true)
	override CPPFLAGS_FREEDOKO += -DREST_SERVICE
endif
ifeq ($(WEBSOCKETS_DOKOLOUNGE_SERVICE), true)
	override CPPFLAGS_FREEDOKO += -DWEBSOCKETS_SERVICE
endif
ifeq ($(USE_UI_GTKMM), true)
	override CPPFLAGS_FREEDOKO += -DUSE_UI_GTKMM
endif
ifeq ($(USE_SOUND), true)
	override CPPFLAGS_FREEDOKO += -DUSE_SOUND
ifeq ($(USE_SOUND_ALUT), true)
	override CPPFLAGS_FREEDOKO += -DUSE_SOUND_ALUT
endif
ifeq ($(USE_SOUND_COMMAND), true)
	override CPPFLAGS_FREEDOKO += -DUSE_SOUND_COMMAND
endif
ifeq ($(USE_SOUND_PLAYSOUND), true)
	override CPPFLAGS_FREEDOKO += -DUSE_SOUND_PLAYSOUND
endif
endif
ifeq ($(USE_THREADS), true)
	override CPPFLAGS_FREEDOKO += -DUSE_THREADS
endif
ifeq ($(RELEASE), true)
	override CPPFLAGS_FREEDOKO += -DRELEASE
	VERSION_DESCRIPTION ?= '"$(OPERATING_SYSTEM_NAME)"'
  ifeq ($(USE_UI_GTKMM), true)
    ifeq ($(OPERATING_SYSTEM), Windows)
	override LDFLAGS += -mwindows
    endif
    ifeq ($(OPERATING_SYSTEM), MSYS)
	override LDFLAGS += -mwindows
    endif
  endif
else
	VERSION_DESCRIPTION ?= '"self compiled"'
endif
ifneq ($(VERSION_DESCRIPTION), )
	override CPPFLAGS_FREEDOKO += -DVERSION_DESCRIPTION=$(VERSION_DESCRIPTION)
endif

# path to the generated .o and .d files
# can be set in 'Makefile.local'
#OBJDIR ?= $(DEPTH)
