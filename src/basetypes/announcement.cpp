/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "announcement.h"

auto to_string(Announcement const announcement) noexcept -> string
{
  switch (announcement) {
  case Announcement::noannouncement:
    (void)_("Announcement::no announcement");
    return "no announcement";
  case Announcement::no120:
    (void)_("Announcement::no 120");
    return "no 120";
  case Announcement::no90:
    (void)_("Announcement::no 90");
    return "no 90";
  case Announcement::no60:
    (void)_("Announcement::no 60");
    return "no 60";
  case Announcement::no30:
    (void)_("Announcement::no 30");
    return "no 30";
  case Announcement::no0:
    (void)_("Announcement::no 0");
    return "no 0";
  case Announcement::reply:
    (void)_("Announcement::reply");
    return "reply";
  } // switch(announcement)

  DEBUG_ASSERTION(false,
                  "::to_string(Announcement):\n"
                  "  announcement not found: " << static_cast<int>(announcement)
                 );

  return {};
}

auto gettext(Announcement const announcement) -> string
{
  return gettext("Announcement::" + to_string(announcement));
}

auto operator<<(ostream& ostr, Announcement const announcement) -> ostream&
{
  ostr << to_string(announcement);
  return ostr;
}

auto announcement_from_string(string const& name) -> Announcement
{
  return element_with_name(announcement_list, name);
}

auto operator!(Announcement announcement) noexcept -> bool
{
  return (announcement == Announcement::noannouncement);
}

auto operator&&(bool const lhs, Announcement const rhs) noexcept -> bool
{
  return (lhs && (rhs != Announcement::noannouncement));
}

auto operator&&(Announcement const lhs, bool const rhs) noexcept -> bool
{
  return ((lhs != Announcement::noannouncement) && rhs);
}

auto operator||(bool const lhs, Announcement const rhs) noexcept -> bool
{
  return (lhs || (rhs != Announcement::noannouncement));
}

auto operator||(Announcement const lhs, bool const rhs) noexcept -> bool
{
  return ((lhs != Announcement::noannouncement) || rhs);
}

auto operator<(Announcement const lhs, Announcement const rhs) noexcept -> bool
{
  if (lhs == rhs)
    return false;
  return (lhs <= rhs);
}

auto operator<=(Announcement const lhs, Announcement const rhs) noexcept -> bool
{
  switch (lhs) {
  case Announcement::noannouncement:
    return true;
  case Announcement::no120:
    if (rhs == Announcement::no120)
      return true;
    [[fallthrough]];
  case Announcement::no90:
    if (rhs == Announcement::no90)
      return true;
    [[fallthrough]];
  case Announcement::no60:
    if (rhs == Announcement::no60)
      return true;
    [[fallthrough]];
  case Announcement::no30:
    if (rhs == Announcement::no30)
      return true;
    [[fallthrough]];
  case Announcement::no0:
    if (rhs == Announcement::no0)
      return true;
    return false;
  case Announcement::reply:
    return false;
  } // switch(announcement)

  return false;
}

auto operator>(Announcement const lhs, Announcement const rhs) noexcept -> bool
{
  if (lhs == rhs)
    return false;
  switch (lhs) {
  case Announcement::noannouncement:
    return false;
  case Announcement::no0:
    if (rhs == Announcement::no30)
      return true;
    [[fallthrough]];
  case Announcement::no30:
    if (rhs == Announcement::no60)
      return true;
    [[fallthrough]];
  case Announcement::no60:
    if (rhs == Announcement::no90)
      return true;
    [[fallthrough]];
  case Announcement::no90:
    if (rhs == Announcement::no120)
      return true;
    [[fallthrough]];
  case Announcement::no120:
    if (rhs == Announcement::noannouncement)
      return true;
    return false;
  case Announcement::reply:
    return (rhs == Announcement::noannouncement);
  } // switch(announcement)

  return false;
}

auto operator>=(Announcement const lhs, Announcement const rhs) noexcept -> bool
{
  return (   lhs == rhs
          || lhs >  rhs);
}

auto previous(Announcement const announcement) -> Announcement
{
  switch (announcement) {
  case Announcement::no120:
    return Announcement::noannouncement;
  case Announcement::no90:
    return Announcement::no120;
  case Announcement::no60:
    return Announcement::no90;
  case Announcement::no30:
    return Announcement::no60;
  case Announcement::no0:
    return Announcement::no30;
  default:
    DEBUG_ASSERTION(false,
                    "previous(announcement)\n"
                    "  there exists no previous announcement of "
                    << announcement);
  } // switch (announcement)

  return Announcement::noannouncement;
}

auto next(Announcement const announcement) -> Announcement
{
  switch (announcement) {
  case Announcement::noannouncement:
    return Announcement::no120;
  case Announcement::no120:
    return Announcement::no90;
  case Announcement::no90:
    return Announcement::no60;
  case Announcement::no60:
    return Announcement::no30;
  case Announcement::no30:
    return Announcement::no0;
  default:
    DEBUG_ASSERTION(false,
                    "next(announcement)\n"
                    "  there exists no next announcement of "
                    << announcement);
  } // switch (announcement)

  return Announcement::noannouncement;
}

auto needed_points(Announcement const announcement) noexcept -> unsigned
{
  switch (announcement) {
  case Announcement::noannouncement:
    return 120; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  case Announcement::no120:
    return 240 - 120 + 1; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  case Announcement::no90:
    return 240 -  90 + 1; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  case Announcement::no60:
    return 240 -  60 + 1; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  case Announcement::no30:
    return 240 -  30 + 1; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  case Announcement::no0:
    return 240 -   0 + 1; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  case Announcement::reply:
    return 120; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  } // switch (announcement)

  return 0;
}

auto point_limit_for_opposite_team(Announcement const announcement) noexcept -> unsigned
{
  return 240 - needed_points(announcement) + 1; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
}
