/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "exception.h"

enum class Announcement {
  noannouncement,
  no120, // means re or contra
  no90,
  no60,
  no30,
  no0,
  reply,
}; // enum class Announcement

constexpr auto announcement_list = array<Announcement, 7>{
  Announcement::noannouncement,
  Announcement::no120, // Re/Kontra bzw. beim Klopfen keine 120
  Announcement::no90,
  Announcement::no60,
  Announcement::no30,
  Announcement::no0,
  Announcement::reply,
};
constexpr auto real_announcement_list = array<Announcement, 5>{
  Announcement::no120,
  Announcement::no90,
  Announcement::no60,
  Announcement::no30,
  Announcement::no0,
};

auto to_string(Announcement announcement) noexcept -> string;
auto gettext(Announcement announcement) -> string;
auto operator<<(ostream& ostr, Announcement announcement) -> ostream&;
auto announcement_from_string(string const& name) -> Announcement;

auto operator! (Announcement announcement)  noexcept -> bool;
auto operator&&(bool lhs, Announcement rhs) noexcept -> bool;
auto operator&&(Announcement lhs, bool rhs) noexcept -> bool;
auto operator||(bool lhs, Announcement rhs) noexcept -> bool;
auto operator||(Announcement lhs, bool rhs) noexcept -> bool;
auto operator< (Announcement lhs, Announcement rhs) noexcept -> bool;
auto operator<=(Announcement lhs, Announcement rhs) noexcept -> bool;
auto operator> (Announcement lhs, Announcement rhs) noexcept -> bool;
auto operator>=(Announcement lhs, Announcement rhs) noexcept -> bool;

auto previous(Announcement announcement) -> Announcement;
auto next(Announcement announcement)     -> Announcement;

auto needed_points(Announcement announcement)                 noexcept -> unsigned;
auto point_limit_for_opposite_team(Announcement announcement) noexcept -> unsigned;

struct AnnouncementWithTrickno {
    AnnouncementWithTrickno() noexcept = default;
    AnnouncementWithTrickno(Announcement const announcement,
                            unsigned const trickno) noexcept :
      announcement(announcement), trickno(trickno)
  { }
    operator Announcement const&() const noexcept
    { return announcement; }
    Announcement announcement = Announcement::noannouncement;
    unsigned trickno = UINT_MAX;
}; // struct AnnouncementWithTrickno
