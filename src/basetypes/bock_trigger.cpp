/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "bock_trigger.h"

auto to_string(BockTrigger const bock_trigger) -> string
{
  switch (bock_trigger) {
  case BockTrigger::equal_points:
    (void)_("BockTrigger::equal points");
    return "equal points";
  case BockTrigger::solo_lost:
    (void)_("BockTrigger::solo lost");
    return "solo lost";
  case BockTrigger::re_lost:
    (void)_("BockTrigger::re lost");
    return "re lost";
  case BockTrigger::contra_lost:
    (void)_("BockTrigger::contra lost");
    return "contra lost";
  case BockTrigger::heart_trick:
    (void)_("BockTrigger::heart trick");
    return "heart trick";
  case BockTrigger::black:
    (void)_("BockTrigger::black");
    return "black";
  } // switch (bock_trigger)

  DEBUG_ASSERTION(false,
                  "to_string(BockTrigger):\n"
                  "  no name found for " << static_cast<int>(bock_trigger)
                 );

  return {};
}

auto gettext(BockTrigger const bock_trigger) -> string
{
  return gettext("BockTrigger::" + to_string(bock_trigger));
}

auto bock_trigger_from_string(string const& name) -> BockTrigger
{
  return element_with_name(bock_trigger_list, name);
}

auto operator<<(ostream& ostr, BockTrigger const bock_trigger) -> ostream&
{
  ostr << to_string(bock_trigger);
  return ostr;
}
