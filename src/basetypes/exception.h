/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include <exception>

class StdException : public std::exception {
public:
  StdException() = default;
  explicit StdException(string const message) :
    message_(message)
  { }
  StdException(StdException const&)   = default;
  StdException(StdException&&)        = default;
  auto operator=(StdException const&) = delete;
  auto operator=(StdException&&)      = delete;
  ~StdException() override = default;

  auto what() const noexcept -> char const* override
  { return message_.c_str(); }

  virtual auto message() const noexcept -> string const&
  { return message_; }

private:
  string const message_;
};

/**
 ** An exception thrown when a reading (of data from an istream) failed
 **/
class ReadException : public StdException {
public:
  ReadException() = default;
  explicit ReadException(string const message) :
    StdException(message)
  { }
  ReadException(ReadException const&)  = default;
  ReadException(ReadException&&)       = default;
  auto operator=(ReadException const&) = delete;
  auto operator=(ReadException&&)      = delete;

  ~ReadException() override = default;
};
