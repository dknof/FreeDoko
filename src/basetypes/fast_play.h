/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

class FastPlay;
extern FastPlay fast_play; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

enum class FastPlayBit {
    party_start            = 1,                           //    1 automatical start party
    player                 = party_start            << 1, //    2 change human players to ais
    dummy_ai               = player                 << 1, //    4 set the players to the dummy ai
    pause                  = dummy_ai               << 1, //    8 skip pauses
    gameplay_information   = pause                  << 1, //   16 skip gameplay information dialogs
    game_finished          = gameplay_information   << 1, //   32 skip game finished dialog
    party_finished         = game_finished          << 1, //   64 skip party finished dialog
    show_all_hands         = party_finished         << 1, //  128 show all hands
    hide_bug_report_window = show_all_hands         << 1, //  256 do not show the bug report replay window
    quit_when_finished     = hide_bug_report_window << 1, //  512 quit when finished
    seed_info              = quit_when_finished     << 1  // 1024 only write the seed on stdout
}; // enum class FastPlayBit

class FastPlay {
  friend auto operator|(FastPlayBit lhs, FastPlayBit rhs) noexcept -> FastPlay;
  public:
    static FastPlayBit constexpr party_start            = FastPlayBit::party_start;
    static FastPlayBit constexpr player                 = FastPlayBit::player;
    static FastPlayBit constexpr dummy_ai               = FastPlayBit::dummy_ai;
    static FastPlayBit constexpr pause                  = FastPlayBit::pause;
    static FastPlayBit constexpr gameplay_information   = FastPlayBit::gameplay_information;
    static FastPlayBit constexpr game_finished          = FastPlayBit::game_finished;
    static FastPlayBit constexpr party_finished         = FastPlayBit::party_finished;
    static FastPlayBit constexpr show_all_hands         = FastPlayBit::show_all_hands;
    static FastPlayBit constexpr hide_bug_report_window = FastPlayBit::hide_bug_report_window;
    static FastPlayBit constexpr quit_when_finished     = FastPlayBit::quit_when_finished;
    static FastPlayBit constexpr seed_info              = FastPlayBit::seed_info;

  public:
    FastPlay()              noexcept;
    FastPlay(FastPlayBit v) noexcept;

    operator bool() const noexcept;

    auto operator&(FastPlayBit rhs) const noexcept -> FastPlay;

    auto operator|=(FastPlayBit rhs) noexcept -> FastPlay&;

    void add(FastPlayBit rhs) noexcept;
    void remove(FastPlayBit rhs) noexcept;

  private:
    int value = 0;
};

auto operator|(FastPlayBit lhs, FastPlayBit rhs) noexcept -> FastPlay;
