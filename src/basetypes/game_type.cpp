/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "game_type.h"

auto to_string(GameType const game_type) noexcept -> string
{
  switch(game_type) {
  case GameType::normal:
    (void)_("GameType::normal");
    return "normal";
  case GameType::thrown_nines:
    (void)_("GameType::thrown nines");
    return "thrown nines";
  case GameType::thrown_kings:
    (void)_("GameType::thrown kings");
    return "thrown kings";
  case GameType::thrown_nines_and_kings:
    (void)_("GameType::thrown nines and kings");
    return "thrown nines and kings";
  case GameType::thrown_richness:
    (void)_("GameType::thrown richness");
    return "thrown richness";
  case GameType::fox_highest_trump:
    (void)_("GameType::fox highest trump");
    return "fox highest trump";
  case GameType::redistribute:
    (void)_("GameType::redistribute");
    return "redistribute";
  case GameType::poverty:
    (void)_("GameType::poverty");
    return "poverty";
  case GameType::marriage:
    (void)_("GameType::marriage");
    return "marriage";
  case GameType::marriage_solo:
    (void)_("GameType::marriage solo");
    return "marriage solo";
  case GameType::marriage_silent:
    (void)_("GameType::marriage silent");
    return "marriage silent";
  case GameType::solo_jack:
    (void)_("GameType::solo jack");
    return "solo jack";
  case GameType::solo_queen:
    (void)_("GameType::solo queen");
    return "solo queen";
  case GameType::solo_king:
    (void)_("GameType::solo king");
    return "solo king";
  case GameType::solo_queen_jack:
    (void)_("GameType::solo queen-jack");
    return "solo queen-jack";
  case GameType::solo_king_jack:
    (void)_("GameType::solo king-jack");
    return "solo king-jack";
  case GameType::solo_king_queen:
    (void)_("GameType::solo king-queen");
    return "solo king-queen";
  case GameType::solo_koehler:
    (void)_("GameType::solo koehler");
    return "solo koehler";
  case GameType::solo_club:
    (void)_("GameType::solo club");
    return "solo club";
  case GameType::solo_spade:
    (void)_("GameType::solo spade");
    return "solo spade";
  case GameType::solo_heart:
    (void)_("GameType::solo heart");
    return "solo heart";
  case GameType::solo_diamond:
    (void)_("GameType::solo diamond");
    return "solo diamond";
  case GameType::solo_meatless:
    (void)_("GameType::solo meatless");
    return "solo meatless";
  } // switch(gametype)

  DEBUG_ASSERTION(false,
                  "to_string(GameType):\n"
                  "  game type not found: " << static_cast<int>(game_type));

  return {};
}

auto gettext(GameType const game_type) -> string
{
  return gettext("GameType::" + to_string(game_type));
}

auto game_type_from_string(string const& name) -> GameType
{
#ifdef DEPRECATED
  if (name == "genscher")
    return GameType::normal;
#endif
  return element_with_name(game_type_list, name);
}

auto operator<<(ostream& ostr, GameType const game_type) -> ostream&
{
  ostr << to_string(game_type);
  return ostr;
}

auto is_with_unknown_teams(GameType const game_type) noexcept -> bool
{
  switch (game_type) {
  case GameType::normal:
  case GameType::marriage_silent:
    return true;
  default:
    return false;
  }
}

auto is_with_trump_color(GameType const game_type) noexcept -> bool
{
  switch (game_type) {
  case GameType::normal:
  case GameType::poverty:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
    return true;
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
  case GameType::fox_highest_trump:
  case GameType::redistribute:
  case GameType::solo_meatless:
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
    return false;
  }
  return false;
}

auto is_normal(GameType const game_type) noexcept -> bool
{
  switch (game_type) {
  case GameType::normal:
  case GameType::poverty:
  case GameType::marriage:
    return true;
  default:
    return false;
  }
}

auto is_marriage(GameType const game_type) noexcept -> bool
{
  switch (game_type) {
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
    return true;
  default:
    return false;
  }
}

auto is_poverty(GameType const game_type) noexcept -> bool
{
  switch (game_type) {
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::fox_highest_trump:
  case GameType::redistribute:
  case GameType::poverty:
    return true;
  default:
    return false;
  }
}

auto is_real_solo(GameType const game_type) noexcept -> bool
{
  return (   is_color_solo(game_type)
          || is_picture_solo(game_type)
          || game_type == GameType::solo_meatless);
}

auto is_solo(GameType const game_type) noexcept -> bool
{
  return (   is_real_solo(game_type)
          || game_type == GameType::marriage_solo
          || game_type == GameType::marriage_silent);
}

auto is_color_solo(GameType const game_type) noexcept -> bool
{
  switch (game_type) {
  case GameType::solo_club:
  case GameType::solo_spade:
  case GameType::solo_heart:
  case GameType::solo_diamond:
    return true;
  default:
    return false;
  }
}

auto is_picture_solo(GameType const game_type) noexcept -> bool
{
  return (   is_single_picture_solo(game_type)
          || is_double_picture_solo(game_type)
          || game_type == GameType::solo_koehler);
}

auto is_picture_solo_or_meatless(GameType const game_type) noexcept -> bool
{
  return (   is_picture_solo(game_type)
          || game_type == GameType::solo_meatless);
}

auto is_single_picture_solo(GameType const game_type) noexcept -> bool
{
  switch (game_type) {
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
    return true;
  default:
    return false;
  }
}

auto is_double_picture_solo(GameType const game_type) noexcept -> bool
{
  switch (game_type) {
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
    return true;
  default:
    return false;
  }
}
