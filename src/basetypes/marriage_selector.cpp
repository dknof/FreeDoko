/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "marriage_selector.h"

auto to_string(MarriageSelector const marriage_selector) noexcept -> string
{
  switch(marriage_selector) {
  case MarriageSelector::silent:
    (void)_("MarriageSelector::silent");
    return "silent";
  case MarriageSelector::team_set:
    (void)_("MarriageSelector::team set");
    return "team set";
  case MarriageSelector::first_foreign:
    (void)_("MarriageSelector::first foreign");
    return "first foreign";
  case MarriageSelector::first_trump:
    (void)_("MarriageSelector::first trump");
    return "first trump";
  case MarriageSelector::first_color:
    (void)_("MarriageSelector::first color");
    return "first color";
  case MarriageSelector::first_club:
    (void)_("MarriageSelector::first club");
    return "first club";
  case MarriageSelector::first_spade:
    (void)_("MarriageSelector::first spade");
    return "first spade";
  case MarriageSelector::first_heart:
    (void)_("MarriageSelector::first heart");
    return "first heart";
  }

  DEBUG_ASSERTION(false,
                  "to_string(MarriageSelector):\n"
                  "  no name found for " << static_cast<int>(marriage_selector)
                 );

  return {};
}

auto gettext(MarriageSelector const marriage_selector) -> string
{
  return gettext("MarriageSelector::" + to_string(marriage_selector));
}

auto marriage_selector_from_string(string const& name) -> MarriageSelector
{
  return element_with_name(marriage_selector_list, name);
}

auto operator<<(ostream& ostr, MarriageSelector const marriage_selector) -> ostream&
{
  ostr << to_string(marriage_selector);
  return ostr;
}

auto operator!(MarriageSelector const marriage_selector) noexcept -> bool
{
  return (marriage_selector == MarriageSelector::team_set);
}
