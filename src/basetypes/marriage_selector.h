/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "exception.h"

enum class MarriageSelector {
  team_set,
  first_foreign,
  first_trump,
  first_color,
  first_club,
  first_spade,
  first_heart,
  silent
}; // enum class MarriageSelector

constexpr auto marriage_selector_list = array<MarriageSelector, 8>{
  MarriageSelector::team_set,
  MarriageSelector::first_foreign,
  MarriageSelector::first_trump,
  MarriageSelector::first_color,
  MarriageSelector::first_club,
  MarriageSelector::first_spade,
  MarriageSelector::first_heart,
  MarriageSelector::silent
};

auto to_string(MarriageSelector marriage_selector) noexcept        -> string;
auto gettext(MarriageSelector marriage_selector)                   -> string;
auto operator<<(ostream& ostr, MarriageSelector marriage_selector) -> ostream&;
auto marriage_selector_from_string(string const& name)             -> MarriageSelector;
auto operator!(MarriageSelector marriage_selector) noexcept        -> bool;
