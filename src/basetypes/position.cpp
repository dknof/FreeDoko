/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "position.h"

auto to_string(Position const position) noexcept -> string
{
  switch (position) {
  case Position::north:
    return "north";
  case Position::south:
    return "south";
  case Position::east:
    return "east";
  case Position::west:
    return "west";
  case Position::center:
    return "center";
  }
  return {};
}

auto gettext(Position const position) -> string
{
  return gettext("Position::" + to_string(position));
}

auto operator<<(ostream& ostr, Position const position) -> ostream&
{
  ostr << to_string(position);
  return ostr;
}
