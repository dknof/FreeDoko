/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "program_flow_exception.h"

namespace ProgramFlowException {


PartyLoaded::PartyLoaded()
  = default;


auto PartyLoaded::what() const noexcept -> char const*
{ return "party loaded"; }


Quit::Quit()
  = default;


auto Quit::what() const noexcept -> char const*
{ return "quit"; }


CardPlayed::CardPlayed(Card const& card)
  : card(card)
{ }


auto CardPlayed::what() const noexcept -> char const*
{
  static string text;
  text = "card played: " + to_string(card);
  return text.c_str();
}

} // namespace ProgramFlowException
