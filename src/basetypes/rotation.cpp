/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "rotation.h"

auto clockwise(Rotation const rotation) noexcept -> Rotation
{
  switch (rotation) {
  case Rotation::up:
    return Rotation::left;
  case Rotation::down:
    return Rotation::right;
  case Rotation::right:
    return Rotation::up;
  case Rotation::left:
    return Rotation::down;
  }

  DEBUG_ASSERTION(false,
                  "ROTATION::conterclockwise(rotation):\n"
                  "  behind 'switch(rotation)'");

  return Rotation::up;
}

auto counterclockwise(Rotation const rotation) noexcept -> Rotation
{
  switch (rotation) {
  case Rotation::up:
    return Rotation::right;
  case Rotation::down:
    return Rotation::left;
  case Rotation::right:
    return Rotation::down;
  case Rotation::left:
    return Rotation::up;
  }

  DEBUG_ASSERTION(false,
                  "ROTATION::conterclockwise(rotation):\n"
                  "  behind 'switch(rotation)'");

  return Rotation::up;
}

auto to_string(Rotation const rotation) noexcept -> string
{
  switch (rotation) {
  case Rotation::up:
    return "up";
  case Rotation::down:
    return "down";
  case Rotation::right:
    return "right";
  case Rotation::left:
    return "left";
  }
  return {};
}

auto operator<<(ostream& ostr, Rotation const rotation) -> ostream&
{
  ostr << to_string(rotation);
  return ostr;
}
