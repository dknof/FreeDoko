/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "team.h"

auto to_string(Team const team) noexcept -> string
{
  switch(team) {
  case Team::re:
    (void)_("Team::re");
    return "re";
  case Team::unknown:
    (void)_("Team::unknown team");
    return "unknown team";
  case Team::contra:
    (void)_("Team::contra");
    return "contra";
  case Team::noteam:
    (void)_("Team::no team");
    return "no team";
  }

  DEBUG_ASSERTION(false,
                  "to_string(Team):\n"
                  "  team not found: " << static_cast<int>(team));
  return {};
}

auto gettext(Team const team) -> string
{
  return gettext("Team::" + to_string(team));
}

auto operator<<(ostream& ostr, Team const team) -> ostream&
{
  ostr << to_string(team);
  return ostr;
}

auto short_string(Team const team) noexcept -> string
{
  switch(team) {
  case Team::re:
    (void)_("Team::re");
    return "re";
  case Team::contra:
    (void)_("Team::contra");
    return "contra";
  case Team::unknown:
    (void)_("Team::?");
    return "?";
  case Team::noteam:
    (void)_("Team::-");
    return "-";
  }

  DEBUG_ASSERTION(false,
                  "short_string(Team):\n"
                  "  team not found: " << static_cast<int>(team));

  return {};
}

auto team_from_string(string const& name) -> Team
{
  return element_with_name(team_list, name);
}

auto opposite(Team const team) noexcept -> Team
{
  switch(team) {
  case Team::re:
    return Team::contra;
  case Team::contra:
    return Team::re;
  default:
    return Team::unknown;
  }
}

auto is_real(Team const team) noexcept -> bool
{
  return (   team == Team::re
          || team == Team::contra);
}
