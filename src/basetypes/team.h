/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "exception.h"

enum class Team {
  noteam,
  unknown,
  re,
  contra, 
}; // enum class Team

constexpr auto team_list = array<Team, 4>{
  Team::noteam,
  Team::unknown,
  Team::re,
  Team::contra, 
};

auto to_string(Team team)    noexcept -> string;
auto short_string(Team team) noexcept -> string;
auto gettext(Team team)               -> string;
auto operator<<(ostream& ostr, Team team) -> ostream&;
auto team_from_string(string const& name) -> Team;

auto opposite(Team team) noexcept -> Team;
auto is_real(Team team)  noexcept -> bool;
