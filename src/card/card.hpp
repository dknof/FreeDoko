/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

constexpr Card::Card() noexcept = default;
constexpr Card::Card(Color const c, Value const v) noexcept :
color_(c), value_(v)
{ }

constexpr auto Card::color() const noexcept -> Color
{ return color_; }
constexpr auto Card::value() const noexcept -> Value 
{ return value_; }

constexpr auto Card::is_empty() const noexcept -> bool
{ return color_ == nocardcolor  || value_ == nocardvalue; }
constexpr Card::operator bool() const noexcept
{ return !is_empty(); }
constexpr auto Card::is_unknown() const noexcept -> bool
{ return color_ == unknowncardcolor  || value_ == unknowncardvalue; }

constexpr auto Card::points() const noexcept -> unsigned
{ return static_cast<unsigned>(value_); }

constexpr auto Card::operator==(Card const& rhs) const noexcept -> bool
{ return color_ == rhs.color_ && value_ == rhs.value_; }
constexpr auto Card::operator!=(Card const& rhs) const noexcept -> bool
{ return color_ != rhs.color_ || value_ != rhs.value_; }

constexpr Card Card::empty{};
constexpr Card Card::unknown       = Card(unknowncardcolor, unknowncardvalue);
constexpr Card Card::dulle         = Card(heart,   ten);
constexpr Card Card::charlie       = Card(club,    jack);
constexpr Card Card::club_ace      = Card(club,    ace);
constexpr Card Card::spade_ace     = Card(spade,   ace);
constexpr Card Card::heart_ace     = Card(heart,   ace);
constexpr Card Card::diamond_ace   = Card(diamond, ace);
constexpr Card Card::club_ten      = Card(club,    ten);
constexpr Card Card::spade_ten     = Card(spade,   ten);
constexpr Card Card::heart_ten     = Card(heart,   ten);
constexpr Card Card::diamond_ten   = Card(diamond, ten);
constexpr Card Card::club_king     = Card(club,    king);
constexpr Card Card::spade_king    = Card(spade,   king);
constexpr Card Card::heart_king    = Card(heart,   king);
constexpr Card Card::diamond_king  = Card(diamond, king);
constexpr Card Card::club_queen    = Card(club,    queen);
constexpr Card Card::spade_queen   = Card(spade,   queen);
constexpr Card Card::heart_queen   = Card(heart,   queen);
constexpr Card Card::diamond_queen = Card(diamond, queen);
constexpr Card Card::club_jack     = Card(club,    jack);
constexpr Card Card::spade_jack    = Card(spade,   jack);
constexpr Card Card::heart_jack    = Card(heart,   jack);
constexpr Card Card::diamond_jack  = Card(diamond, jack);
constexpr Card Card::club_nine     = Card(club,    nine);
constexpr Card Card::spade_nine    = Card(spade,   nine);
constexpr Card Card::heart_nine    = Card(heart,   nine);
constexpr Card Card::diamond_nine  = Card(diamond, nine);

