/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "card_counter.h"

#include "../game/game.h"

namespace {
auto to_size_t(Card card) noexcept -> size_t;
}; // namespace

void CardCounter::clear()
{
  cards_.clear();
  cards_no_ = 0;
  map<Card, unsigned>::clear();

  no_ = vector<unsigned>(Card::number_of_cards + 1, 0);
}

void CardCounter::write(ostream& ostr) const
{
  ostr << "cardno = " << cards_no() << '\n';
  for (auto const& x : *this) {
    if (x.second > 0)
      ostr << setw(14) << x.first << " = " << x.second << '\n';
  }
}

auto CardCounter::begin() const -> const_iterator
{
  return Base::begin();
}

auto CardCounter::end() const -> const_iterator
{
  return Base::end();
}

auto CardCounter::cards() const -> vector<Card> const&
{
  return cards_;
}

auto CardCounter::cards_no() const -> unsigned
{
#ifdef DEBUG
  DEBUG_ASSERTION((cards_no_ == cards_.size()),
                  "CardCounter::cards_no():\n"
                  "  'cards_no_ = " << cards_no_
                  << " != " << cards_.size()
                  << " = cards_.size()");

  auto const n = sum(*this, [](auto const& card_count) {
                     return card_count.second;
                     });

  DEBUG_ASSERTION((cards_no_ == n),
                  "CardCounter::cards_no():\n"
                  "  'cards_no_ = " << cards_no_
                  << " != " << n
                  << " = Sum");
#endif

  return cards_no_;
}

auto CardCounter::operator[](Card const card) const -> unsigned
{
  return no_[to_size_t(card)];
}

auto CardCounter::operator()(Card::TColor const tcolor, Game const& game) const -> unsigned
{
  unsigned no = 0;

  for (auto const& x : *this)
    if (x.first.tcolor(game) == tcolor)
      no += x.second;

  return no;
}

auto CardCounter::set(Card const card, unsigned const n) -> bool
{
  unsigned const m = (*this)[card];
  if (n > m)
    return min_set(card, n);
  else if (n < m)
    return max_set(card, n);

  return false;
}

auto CardCounter::min_set(Card const card, unsigned const n) -> bool
{
  if (n == 0)
    return false;

  unsigned const t = (*this)[card];
  if (t >= n)
    return false;

  // add the card to the vector
  cards_.insert(cards_.end(), (n - t), card);

  // set the number
  map<Card, unsigned>::operator[](card) = n;
  no_[to_size_t(card)] = n;

  // adjust the total cards number
  cards_no_ += (n - t);

  return true;
}

auto CardCounter::max_set(Card const card, unsigned const n) -> bool
{
  unsigned const t = (*this)[card];
  if (t <= n)
    return false;

  { // remove the cards from the list
    auto i = container_algorithm::find(cards_, card);
    for (unsigned a = 0; a < t - n; ++a) {
      i = cards_.erase(i);
      i = std::find(i, std::end(cards_), card);
    } // for (a < t - n)
  } // remove the cards from the list

  // set the number
  if (n == 0) {
    if (count(card) > 0)
      map<Card, unsigned>::erase(card);
  } else { // if !(n == 0)
    map<Card, unsigned>::operator[](card) = n;
  } // if !(n == 0)
  no_[to_size_t(card)] = n;

  // adjust the total cards number
  cards_no_ -= t - n;

  return true;
}

auto CardCounter::erase(Card const card) -> bool
{
  return max_set(card, 0);
}

void CardCounter::inc(Card const card)
{
  map<Card, unsigned>::operator[](card) += 1;
  cards_.push_back(card);
  cards_no_ += 1;

  no_[to_size_t(card)] += 1;
}

void CardCounter::add(Card const card, unsigned const n)
{
  map<Card, unsigned>::operator[](card) += n;
  for (unsigned i = 0; i < n; ++i)
    cards_.push_back(card);
  cards_no_ += n;

  no_[to_size_t(card)] += n;
}

void CardCounter::dec(Card const card)
{
  auto x = find(card);
  DEBUG_ASSERTION(   (x != end())
                  && x->second,
                  "CardCounter::dec(" << card << "):\n"
                  "  the counter is '0'");
  x->second -= 1;
  if (x->second == 0)
    map<Card, unsigned>::erase(x);

  no_[to_size_t(card)] -= 1;

  cards_.erase(container_algorithm::find(cards_, card));
  cards_no_ -= 1;
}

auto CardCounter::contains(vector<Card> const& cards) const -> bool
{
  map<Card, unsigned> counter;
  for (auto const& c : cards)
    counter[c] += 1;

  return contains(counter);
}

auto CardCounter::contains(map<Card, unsigned> const& cards) const -> bool
{
  for (auto const c : cards) {
    if (c.first == Card::unknown)
      continue;
    if ((*this)[c.first] < c.second) {
      cerr << "CardCounter::contains() "
        << c.first << ": " << (*this)[c.first] << " < " << c.second << endl;
      return false;
    }
  }

  return true;
}

auto CardCounter::is_contained(vector<Card> const& cards) const -> bool
{
  map<Card, unsigned> counter;
  for (auto const c : cards)
    counter[c] += 1;

  return is_contained(counter);
}

auto CardCounter::is_contained(map<Card, unsigned> const& cards) const -> bool
{
  for (auto const& c : cards) {
    if ((*this)[c.first] > c.second) {
      cerr << "CardCounter::is_contained() "
        << c.first << ": " << (*this)[c.first] << " > " << c.second << endl;
      return false;
    }
  }

  return true;
}

auto operator<<(ostream& ostr, CardCounter const& card_counter) -> ostream&
{
  card_counter.write(ostr);
  return ostr;
}

auto operator==(CardCounter const& lhs, CardCounter const& rhs) -> bool
{
  return (   static_cast<map<Card, unsigned> const&>(lhs)
          == static_cast<map<Card, unsigned> const&>(rhs));
}

auto operator!=(CardCounter const& lhs, CardCounter const& rhs) -> bool
{
  return (   static_cast<map<Card, unsigned> const&>(lhs)
          != static_cast<map<Card, unsigned> const&>(rhs));
}

namespace {
auto to_size_t(Card const card) noexcept -> size_t
{
  /** This value is used to use 'vector<.>' instead of 'map<Card, .>'
   ** because of performance reasons (~15% better when using in 'CardCounter')
   **/
  if (card.color() == Card::nocardcolor)
    return 0;
  switch (card.value()) {
  case Card::nocardvalue:
  case Card::unknowncardvalue:
    return 0;
  case Card::nine:
    return (card.color() * 6 + 1);
  case Card::jack:
    return (card.color() * 6 + 2);
  case Card::queen:
    return (card.color() * 6 + 3);
  case Card::king:
    return (card.color() * 6 + 4);
  case Card::ten:
    return (card.color() * 6 + 5);
  case Card::ace:
    return (card.color() * 6 + 6);
  }; // switch (card.value())

  return 0;
}

}; // namespace
