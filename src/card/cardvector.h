/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "card.h"

#include <map>

struct Card_map_less
{
  auto operator()(Card const a, Card const b) const -> bool
  {
    return ((a.color() < b.color())
	    || ((a.color() == b.color())
		&& (a.value() < b.value())));
  }
}; // struct map_less

#ifdef POSTPONED

template<typename T>
using CardVector = std::map<Card, T, Card_map_less>;

#else

template<typename T>
class CardVector : public std::map<Card, T, Card_map_less> {
  public:
    CardVector() = default;
    CardVector(CardVector const& cv) = default;
    ~CardVector() = default;

    CardVector& operator=(CardVector const& cv) = default;

  private:
}; // class CardVector : public std::map<Card, T, map_less>

#endif
