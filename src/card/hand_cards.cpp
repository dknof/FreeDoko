/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "hand_cards.h"

#include "../player/player.h"
#include "../game/game.h"
#include "../party/rule.h"

HandCards::HandCards(Hand const& hand) :
  hand_(&hand)
{ }


HandCards::HandCards(vector<HandCard> const& cards) :
  cards_(cards)
{
  if (empty())
    return ;
  hand_ = &cards[0].hand();
#ifndef DEBUG_NO_ASSERTS
  // test whether all cards are from the same player
  for (auto const& card : *this) {
    DEBUG_ASSERTION(hand_ == &card.hand(),
                    "HandCards::HandCards(cards):\n"
                    "  the cards belong to different hand: Player "
                    << cards[0].player().no() << " and " << card.player().no());
  }
#endif
}


HandCards::HandCards(Hand const& hand, vector<Card> const& cards) :
  hand_(&hand)
{
  for (auto c : cards)
    push_back(c);
}


HandCards::HandCards(Hand const& hand, unsigned const n) :
  hand_(&hand),
  cards_(n)
{ }


HandCards::HandCards(Hand const& hand, istream& istr) :
  hand_(&hand)
{
  while (istr.good()) {
    string line;
    std::getline(istr, line);
    if (!line.empty() && (*line.rbegin() == '\r'))
      line.erase(line.end() - 1);
    if (line.empty())
      break;

    push_back(Card(line));
  }
}


auto HandCards::hand() const -> Hand const&
{
  return *hand_;
}


HandCards::operator vector<Card>() const
{
  vector<Card> cards;
  cards.reserve(size());
  for (auto const& c : *this)
    cards.emplace_back(c);
  return cards;
}


auto HandCards::empty() const -> bool
{
  return cards_.empty();
}


auto HandCards::size() const -> size_t
{
  return cards_.size();
}


void HandCards::clear()
{
  cards_.clear();
}


auto HandCards::begin() -> iterator
{
  return cards_.begin();
}


auto HandCards::begin() const -> const_iterator
{
  return cards_.begin();
}


auto HandCards::end() -> iterator
{
  return cards_.end();
}


auto HandCards::end() const -> const_iterator
{
  return cards_.end();
}


auto HandCards::back() const -> HandCard const&
{
  if (cards_.empty())
    return HandCard::empty;
  return cards_.back();
}


auto HandCards::operator[](size_type pos) const -> HandCard const&
{
  return cards_.at(pos);
}



auto HandCards::operator[](size_type pos) -> HandCard&
{
  return cards_.at(pos);
}


void HandCards::push_back(Card const card)
{
  if (hand_) {
    cards_.emplace_back(*hand_, card);
  } else {
    cards_.emplace_back(card);
  }
}


void HandCards::push_back(HandCard&& card)
{
  cards_.push_back(std::move(card));
}


auto HandCards::insert(const_iterator pos, Card const& card) -> iterator
{
  return cards_.insert(pos, HandCard(*hand_, card));
}


auto HandCards::insert(const_iterator pos, size_type count, Card const& card) -> iterator
{
  return cards_.insert(pos, count, HandCard(*hand_, card));
}


auto HandCards::insert(const_iterator pos, const_iterator first, const_iterator last) -> iterator
{
  // ToDo: set the right hand
  return cards_.insert(pos, first, last);
}


auto HandCards::erase(const_iterator pos) -> iterator
{
  return cards_.erase(pos);
}


auto HandCards::erase(const_iterator first, const_iterator last) -> iterator
{
  return cards_.erase(first, last);
}


auto HandCards::card(unsigned const i) const -> HandCard
{
  return (*this)[i];
}



auto HandCards::player() const -> Player const&
{
  DEBUG_ASSERTION(!empty(),
                  "HandCards::player()\n"
                  "  no card");
  return (*this)[0].player();
}


auto HandCards::game() const -> Game const&
{
  return player().game();
}


void HandCards::add(HandCard const card)
{
  emplace_back(card);
}


void HandCards::add(HandCards const& cards)
{
  insert(end(), cards.begin(), cards.end());
}


void HandCards::remove_all(Card const card)
{
  auto card_selector = [card](auto const& d) { return (d == card); };
  remove_if(*this, card_selector);
}


void HandCards::remove(Card const card)
{
  auto d = find_if(*this, [card](auto const& d) { return (d == card); });
  DEBUG_ASSERTION((d != end()),
                  "HandCards::remove(card):\n"
                  "  card '" << card << "' not found in the cards.\n"
                  << *this);
  erase(d);
}


void HandCards::remove(HandCards const& cards)
{
  for (auto const& c : cards) {
    auto d = find_if(*this, [&c](auto const& d) { return (d == c); });
    DEBUG_ASSERTION((d != end()),
                    "HandCards::remove(cards):\n"
                    "  card '" << c << "' not found in the cards.\n"
                    << *this);
    erase(d);
  } // for (c \in cards)
}


auto HandCards::highest_value() const -> Card::Value
{
  if (empty())
    return Card::nocardvalue;

  Card::Value value = begin()->value();
  for (auto c : *this) {
    if (c.value() > value)
      value = c.value();
  } // for (c)

  return value;
}


auto HandCards::highest_value(Card::TColor const tcolor) const -> Card::Value
{
  if (empty())
    return Card::nocardvalue;

  Card::Value value = Card::nine;
  for (auto c : *this) {
    if (   (c.value() > value)
        && (c.tcolor() == tcolor) )
      value = c.value();
  } // for (c)

  return value;
}


auto HandCards::lowest_value() const -> Card::Value
{
  if (empty())
    return Card::nocardvalue;

  return min_element(*this, [](auto a, auto b) {
                     return (a.value() < b.value());
                     })->value();
}


auto HandCards::lowest_value(Card::TColor const tcolor) const -> Card::Value
{
  if (empty())
    return Card::nocardvalue;

  auto max = min_element(*this, [tcolor](auto a, auto b) {
                         return (   (a.tcolor() == tcolor)
                                 && (a.value() < b.value()));
                         });
  if (max == end())
    return Card::nocardvalue;
  return max->value();
}


auto HandCards::highest_card() const -> HandCard
{
  if (empty())
    return HandCard::empty;

  auto highest_card = begin();

  for (auto c = begin() + 1;
       c != end();
       ++c) {
    if (highest_card->is_jabbed_by(*c))
      highest_card = c;
  } // for (c)

  return *highest_card;
}


auto HandCards::highest_trump() const -> HandCard
{
  return highest_card(Card::trump);
}


auto HandCards::highest_trump_till(Card const card_limit) const -> HandCard
{
  return highest_card_till(Card::trump, card_limit);
}


auto HandCards::highest_card(Card::TColor const tcolor) const -> HandCard
{
  if (count(tcolor) == 0)
    return HandCard::empty;

  auto highest_card = begin();
  for (;
       highest_card != end();
       ++highest_card)
    if (highest_card->tcolor() == tcolor)
      break;

  if (highest_card == end())
    return HandCard::empty;

  for (auto c = highest_card + 1;
       c != end();
       ++c) {
    if (   c->tcolor() == tcolor
        && highest_card->is_jabbed_by(*c))
      highest_card = c;
  } // for (c)

  return *highest_card;
}


auto HandCards::second_highest_trump() const -> HandCard
{
  return second_highest_card(Card::trump);
}


auto HandCards::second_highest_card(Card::TColor const tcolor) const -> HandCard
{
  if (count(tcolor) < 2)
    return HandCard::empty;

  auto p = get_second_max_element_if(*this,
                                     [tcolor](HandCard card) {
                                     return card.tcolor() == tcolor;
                                     },
                                     [](HandCard const& lhs, HandCard const& rhs) {
                                     return lhs.is_jabbed_by(rhs);
                                     });
  if (p == nullptr)
    return {};
  return *p;
}


auto HandCards::highest_card(Card::Value const value) const -> HandCard
{
  DEBUG_ASSERTION(   value == Card::jack
                  || value == Card::queen,
                  "HandCards::highest_card(value = " << value << "):\n"
                  "  value must be jack or queen");
  for (auto const card : {
       Card{Card::club, value},
       Card{Card::spade, value},
       Card{Card::heart, value},
       Card{Card::diamond, value},
       }) {
    if (contains(card))
      return *find(*this, card);
  }
  return {};
}


auto HandCards::highest_card_till(Card::TColor const tcolor,
                                  Card const card_limit_) const -> HandCard
{
  if (count(tcolor) == 0)
    return HandCard::empty;

  auto const card_limit = HandCard(hand(), card_limit_);
  auto highest_card = begin();
  for (;
       highest_card != end();
       ++highest_card)
    if (   highest_card->tcolor() == tcolor
        && (   *highest_card == card_limit
            || highest_card->is_jabbed_by(card_limit)) )
      break;

  if (highest_card == end())
    return HandCard::empty;

  for (auto c = highest_card + 1;
       c != end();
       ++c) {
    if (   c->tcolor() == tcolor
        && highest_card->is_jabbed_by(*c)
        && (   *c == card_limit
            || c->is_jabbed_by(card_limit)) )
      highest_card = c;
  } // for (c)

  return *highest_card;
}


auto HandCards::lowest_trump() const -> HandCard
{
  return lowest_card(Card::trump);
}


auto HandCards::lowest_card(Card::TColor const tcolor) const -> HandCard
{
  if (count(tcolor) == 0)
    return HandCard::empty;

  auto lowest_card = begin();
  for (;
       lowest_card != end();
       ++lowest_card)
    if (lowest_card->tcolor() == tcolor)
      break;

  if (lowest_card == end())
    return HandCard::empty;

  for (auto c = lowest_card + 1;
       c != end();
       ++c) {
    if (   c->tcolor() == tcolor
        && c->is_jabbed_by(*lowest_card) )
      lowest_card = c;
  } // for (c)

  return *lowest_card;
}


auto HandCards::lowest_card(Card::Value const value) const -> HandCard
{
  DEBUG_ASSERTION(   value == Card::jack
                  || value == Card::queen,
                  "HandCards::lowest_card(value = " << value << "):\n"
                  "  value must be jack or queen");
  for (auto const card : {
       Card{Card::diamond, value},
       Card{Card::heart, value},
       Card{Card::spade, value},
       Card{Card::club, value},
       }) {
    if (contains(card))
      return *find(*this, card);
  }
  return {};
}


auto HandCards::higher_card_exists(Card const card) const -> bool
{
  for (auto const& c : *this) {
    if (HandCard(hand(), card).is_jabbed_by(c)) {
      return true;
    }
  }

  return false;
}



auto HandCards::lower_card_exists(Card const card_) const -> bool
{
  auto const card = HandCard(hand(), card_);
  for (auto& c : *this) {
    if (   c.tcolor() == card.tcolor()
        && c.is_jabbed_by(card))
      return true;
  }

  return false;
}


auto HandCards::higher_cards_no(Card const card_) const -> unsigned
{
  auto const card = HandCard(hand(), card_);
  return count_if(*this, [&card](auto const& c) {
                  return card.is_jabbed_by(c);
                  });
}


auto HandCards::next_jabbing_card(Card const card_) const -> HandCard
{
  auto const card = HandCard(hand(), card_);
  if (card.jabs(card))
    return same_or_higher_card(card);
  else
    return next_higher_card(card);
}


auto HandCards::next_higher_card(Card const card_) const -> HandCard
{
  if (empty())
    return {};
  auto const card = HandCard(hand(), card_);
  HandCard const* higher_card = nullptr;
  for (auto const& c : *this) {
    if (   card.is_jabbed_by(c)
        && c != card
        && (   higher_card == nullptr
            || c.is_jabbed_by(*higher_card)) )
      higher_card = &c;
  } // for (c)

  if (!higher_card)
    return {};
  return *higher_card;
}


auto HandCards::same_or_higher_card(Card const card) const -> HandCard
{
  for (auto const& c : *this) {
    if (c == card)
      return c;
  }

  return next_higher_card(card);
}


auto HandCards::next_lower_card(Card const card_) const -> HandCard
{
  auto const card = HandCard(hand(), card_);
  HandCard const* lower_card = nullptr;
  auto const tcolor = card.tcolor();
  for (auto const& c : *this) {
    if (   c.is_jabbed_by(card)
        && c != card
        && c.tcolor() == tcolor
        && (   lower_card == nullptr
            || lower_card->is_jabbed_by(c)) )
      lower_card = &c;
  } // for (c)

  if (lower_card == nullptr)
    return HandCard::empty;
  return *lower_card;
}


auto HandCards::same_or_lower_card(Card const card) const -> HandCard
{
  auto const is_card = [card](HandCard const& c) {
    return c == card;
  };
  auto const c = find_if(*this, is_card);
  if (c != end())
    return *c;

  return next_lower_card(card);
}


auto HandCards::hastrump() const -> bool
{
  auto is_trump = [](HandCard const& card) {
    return card.istrump();
  };
  return any_of(*this, is_trump);
}


auto HandCards::hascolor() const -> bool
{
  auto is_color = [](HandCard const& card) {
    return !card.istrump();
  };
  return any_of(*this, is_color);
}


auto HandCards::count(Card::Value const value) const -> unsigned
{
  return count_if(*this, [value](auto const& c) {
                  return (c.value() == value);
                  });
}


auto HandCards::count_rich_cards() const -> unsigned
{
  return count_if(*this, [](auto const& c) {
                  return (c.value() >= 10);
                  });
}


auto HandCards::count(Card::TColor const tcolor) const -> unsigned
{
  return count_if(*this, [tcolor](auto const& c) {
                  return (c.tcolor() == tcolor);
                  });
}

auto HandCards::count(Card const card) const -> unsigned
{
  return count_if(*this, [&card](auto const& c) {
                  return (c == card);
                  });
}


auto HandCards::count(Card::TColor const tcolor,
                      Card::Value const value) const -> unsigned
{
  if (empty())
    return 0;

  if (tcolor == Card::trump)
    return count_if(*this, [value](auto const& c) {
                    return (   c.value() == value
                            && c.istrump());
                    });

  return count(Card(tcolor, value));
}


auto HandCards::count(Card::Color const color, Game const& game) const -> unsigned
{
  return count(color, game.type(), game.rule(Rule::Type::dullen));
}


auto HandCards::contains(Card const card) const -> bool
{
  auto const is_card = [&card](HandCard const& c) {
    return c == card;
  };
  return any_of(*this, is_card);
}


auto HandCards::contains(Card::TColor const tcolor,
                         Card::Value const value) const -> bool
{
  if (   tcolor == Card::nocardcolor
      || value == Card::nocardvalue)
    return false;
  if (tcolor == Card::trump) {
    auto const is_card = [&value](HandCard const& c) {
      return (c.value() == value && c.istrump());
    };
    return any_of(*this, is_card);
  }
  return contains({tcolor, value});
}


auto HandCards::contains(Card::TColor const tcolor) const -> bool
{
  auto const is_tcolor = [tcolor](HandCard const& card) {
    return card.tcolor() == tcolor;
  };
  return any_of(*this, is_tcolor);
}


auto HandCards::contains(Card::Value const value) const -> bool
{
  auto const is_value = [value](HandCard const& card) {
    return card.value() == value;
  };
  return any_of(*this, is_value);
}


auto HandCards::count(Card::Color const color,
                      GameType const gametype, bool const dullen) const -> unsigned
{
  return count_if(*this, [color, gametype, dullen](auto const& c) {
                  return (   (c.color() == color)
                          && !c.Card::istrump(gametype, dullen));
                  });
}


auto HandCards::count_trumps(GameType const gametype) const -> unsigned
{
  if (empty())
    return 0;
  auto const dullen = game().rule(Rule::Type::dullen);
  return count_if(*this, [gametype, dullen](auto const& c) {
                  return (c.Card::istrump(gametype, dullen));
                  });
}


auto HandCards::count_trump_aces(GameType const gametype) const -> unsigned
{
  if (empty())
    return 0;
  return count_if(*this, [gametype](auto const& c) {
                  return (   c.value() == Card::ace
                          && c.Card::istrump(gametype, false));
                  });
}


auto HandCards::equal(HandCards const& cards) const -> bool
{
  for (auto const& c : cards)
    if (count(c) != cards.count(c))
      return false;

  return true;
}


auto HandCards::count_ge(Card const card_) const -> unsigned
{
  auto const card = HandCard(hand(), card_);
  return count_if(*this,
                  [&card](auto const& c) {
                  return (c == card) || c.jabs(card);
                  });
}


auto HandCards::cards_not_ge(Card const card_) const -> HandCards
{
  auto const card = HandCard(hand(), card_);
  auto cards = HandCards(hand());
  for (auto const& c : *this) {
    if (c != card && !c.jabs(card)) {
      cards.add(c);
    }
  }
  return cards;
}


auto operator<<(ostream& ostr, HandCards const& cards) -> ostream&
{
  for (auto const& c : cards)
    ostr << c << '\n';

  return ostr;
}
