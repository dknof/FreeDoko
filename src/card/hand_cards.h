/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "hand_card.h"
class Trick;
class Player;

/**
 ** Cards of a hand
 **
 ** @author	Borg Enders
 ** @author	Diether Knof
 **/
class HandCards {
public:
  using iterator       = vector<HandCard>::iterator;
  using const_iterator = vector<HandCard>::const_iterator;
  using reference      = HandCard&;
  using size_type      = vector<HandCard>::size_type;

public:
  HandCards() = delete;
  HandCards(Hand const& hand);
  HandCards(vector<HandCard> const& cards);
  HandCards(Hand const& hand, vector<Card> const& cards);
  HandCards(Hand const& hand, unsigned n);
  HandCards(Hand const& hand, istream& istr);
  HandCards(HandCards const&) = default;
  auto operator=(HandCards const&) -> HandCards& = default;
  ~HandCards() = default;

  auto hand() const -> Hand const&;
  operator vector<Card>() const;

  auto empty() const -> bool;
  auto size()  const -> size_t;

  void clear();

  auto begin()       -> iterator;
  auto begin() const -> const_iterator;
  auto end()         -> iterator;
  auto end()   const -> const_iterator;

  auto back() const -> HandCard const&;

  auto operator[](size_type pos) const -> HandCard const&;
  auto operator[](size_type pos)       -> HandCard&;

  void push_back(Card card);
  void push_back(HandCard&& card);
  template<class... Args>
    auto emplace_back(Args&&... args) -> reference
    { return cards_.emplace_back(std::forward<Args>(args)...); }

  auto insert(const_iterator pos, Card const& card)                  -> iterator;
  auto insert(const_iterator pos, size_type count, Card const& card) -> iterator;
  auto insert(const_iterator pos, const_iterator first, const_iterator last) -> iterator;

  auto erase(const_iterator pos) -> iterator;
  auto erase(const_iterator first, const_iterator last) -> iterator;

  auto card(unsigned i) const -> HandCard;

  auto player() const -> Player const&;
  auto game() const -> Game const&;

  void add(HandCard card);
  void add(HandCards const& cards);

  void remove(Card card);
  void remove_all(Card card);
  void remove(HandCards const& cards);

  auto highest_value()                    const -> Card::Value;
  auto highest_value(Card::TColor tcolor) const -> Card::Value;
  auto lowest_value()                     const -> Card::Value;
  auto lowest_value(Card::TColor tcolor)  const -> Card::Value;

  auto highest_card()                                          const -> HandCard;
  auto highest_trump()                                         const -> HandCard;
  auto highest_trump_till(Card card_limit)                     const -> HandCard;
  auto highest_card(Card::TColor tcolor)                       const -> HandCard;
  auto second_highest_trump()                                  const -> HandCard;
  auto second_highest_card(Card::TColor tcolor)                const -> HandCard;
  auto highest_card(Card::Value value)                         const -> HandCard;
  auto highest_card_till(Card::TColor tcolor, Card card_limit) const -> HandCard;
  auto lowest_card(Card::TColor tcolor)                        const -> HandCard;
  auto lowest_card(Card::Value value)                          const -> HandCard;
  auto lowest_trump()                                          const -> HandCard;
  auto higher_card_exists(Card card)                           const -> bool;
  auto lower_card_exists(Card card)                            const -> bool;
  auto higher_cards_no(Card card)                              const -> unsigned;
  auto next_jabbing_card(Card card)                            const -> HandCard;
  auto next_higher_card(Card card)                             const -> HandCard;
  auto same_or_higher_card(Card card)                          const -> HandCard;
  auto next_lower_card(Card card)                              const -> HandCard;
  auto same_or_lower_card(Card card)                           const -> HandCard;

  auto hastrump() const -> bool;
  auto hascolor() const -> bool;

  auto count(Card::Value value)                      const -> unsigned;
  // the number of cards with 10 or 11 points
  auto count_rich_cards()                            const -> unsigned;
  auto count(Card::TColor tcolor)                    const -> unsigned;
  auto count(Card card)                              const -> unsigned;
  auto count(Card::TColor tcolor, Card::Value value) const -> unsigned;

  auto contains(Card card)                              const -> bool;
  auto contains(Card::TColor tcolor, Card::Value value) const -> bool;
  auto contains(Card::TColor tcolor)                    const -> bool;
  auto contains(Card::Value value)                      const -> bool;

  // the number of cards 'Card(color)' without counting trump
  auto count(Card::Color color, Game const& game)               const -> unsigned;
  auto count(Card::Color color, GameType gametype, bool dullen) const -> unsigned;

  auto count_trumps(GameType gametype)     const -> unsigned;
  auto count_trump_aces(GameType gametype) const -> unsigned;

  // whether 'cards' is equal to 'this' (without order)
  auto equal(HandCards const& cards) const -> bool;

  auto count_ge(Card card)     const -> unsigned;
  auto cards_not_ge(Card card) const -> HandCards;

private:
  Hand const* hand_ = nullptr;
  vector<HandCard> cards_;
};

auto operator<<(ostream& ostr, HandCards const& cards) -> ostream&;
