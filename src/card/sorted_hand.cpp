/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "sorted_hand.h"

#include "hand.h"

#include "../misc/preferences.h"
#include "../utils/random.h"

SortedHand::SortedHand(Hand& hand) :
  hand_(&hand),
  to_sorted_pos_(hand.cardsnumber_all()),
  from_sorted_pos_(hand.cardsnumber_all()),
  sorted(::preferences(Preferences::Type::cards_order).sorted())
{
  for (unsigned i = 0; i < cardsnumber_all(); ++i)
    to_sorted_pos_.push_back(i);

  set_sorting();
}


SortedHand::SortedHand(SortedHand const&) = default;


auto SortedHand::operator=(SortedHand const&) -> SortedHand& = default;


SortedHand::~SortedHand() = default;


auto SortedHand::hand() const -> Hand const&
{ return *hand_; }


SortedHand::operator Hand const&() const
{ return *hand_; }


auto SortedHand::cardsnumber() const -> unsigned
{
  return hand_->cardsnumber();
}


auto SortedHand::cardsnumber_all() const -> unsigned
{
  return hand_->cardsnumber_all();
}


auto SortedHand::card(unsigned const i) const -> HandCard
{
  return hand_->card(from_sorted_pos(i));
}


auto SortedHand::card_all(unsigned const i) const -> HandCard
{
  DEBUG_ASSERTION((i < cardsnumber_all()),
                  "SortedHand::card_all(i):\n"
                  "  illegal value " << i
                  << " (maximal value is: "
                  << cardsnumber_all() - 1 << ")");

  return hand_->card_all(from_sorted_pos_all(i));
}


auto SortedHand::played(unsigned const i) const -> bool
{
  DEBUG_ASSERTION((i < cardsnumber_all()),
                  "SortedHand::played(i):\n"
                  "  illegal value " << i
                  << " (maximal value is: "
                  << cardsnumber_all() - 1 << ")");

  return hand_->played(from_sorted_pos_all(i));
}


auto SortedHand::played_trick(unsigned const i) const -> unsigned
{
  DEBUG_ASSERTION((i < cardsnumber_all()),
                  "SortedHand::played_trick(i):\n"
                  "  illegal value " << i
                  << " (maximal value is: "
                  << cardsnumber_all() - 1 << ")");

  return hand_->played_trick(from_sorted_pos_all(i));
}


auto SortedHand::from_sorted_pos(unsigned const i) const -> unsigned
{
  DEBUG_ASSERTION((i < cardsnumber()),
                  "SortedHand::from_sorted_pos(i):\n"
                  "  illegal value " << i
                  << " (maximal value is: "
                  << cardsnumber() - 1 << ")");

  return hand_->pos_all_to_pos(from_sorted_pos_all(pos_to_pos_all(i)));
}


auto SortedHand::from_sorted_pos_all(unsigned const i) const -> unsigned
{
  DEBUG_ASSERTION((i < cardsnumber_all()),
                  "SortedHand::from_sorted_pos_all(i):\n"
                  "  illegal value " << i
                  << " (maximal value is: "
                  << cardsnumber_all() - 1 << ")");

  return from_sorted_pos_[i];
}


auto SortedHand::to_sorted_pos(unsigned const i) const -> unsigned
{
  if (i == UINT_MAX)
    return UINT_MAX;

  DEBUG_ASSERTION((i < cardsnumber()),
                  "SortedHand::to_sorted_pos(i):\n"
                  "  illegal value " << i
                  << " (maximal value is: "
                  << cardsnumber() - 1 << ")");

  return hand_->pos_all_to_pos(to_sorted_pos_all(pos_to_pos_all(i)));
}


auto SortedHand::to_sorted_pos_all(unsigned const i) const -> unsigned
{
  if (i == UINT_MAX)
    return UINT_MAX;

  DEBUG_ASSERTION((i < cardsnumber_all()),
                  "SortedHand::to_sorted_pos_all(i):\n"
                  "  illegal value " << i
                  << " (maximal value is: "
                  << cardsnumber_all() - 1 << ")");

  return to_sorted_pos_[i];
}

auto SortedHand::pos_to_pos_all(unsigned pos) const -> unsigned
{
 // The pos of the argument is the pos in the unplayed cards.
 // It is searched, what position in all cards 'pos' stands for.
  if (pos == UINT_MAX)
    return UINT_MAX;

  // translate the position
  unsigned n = 0;
  for (pos += 1; pos > 0; ++n)
    if (!hand_->played(from_sorted_pos_all(n)))
      pos -= 1;

  return (n - 1);
}


auto SortedHand::pos_all_to_pos(unsigned pos) const -> unsigned
{
 // The pos of the argument is the pos in all cards.
 // It is searched, what position in the remaining cards 'pos' stands for.
  if (pos == UINT_MAX)
    return UINT_MAX;

  // translate the position
  unsigned n = 0;
  for (pos += 1; pos > 0; pos--)
    if (!hand_->played(from_sorted_pos_all(pos - 1)))
      n++;

  return (n - 1);
}


auto SortedHand::contains_unknown_or_played() const -> bool
{
  return hand_->contains_unknown_or_played();
}


auto SortedHand::requested_card() const -> HandCard
{
  return hand_->requested_card();
}


auto SortedHand::requested_position() const -> unsigned
{
  return to_sorted_pos(hand_->requested_position());
}


auto SortedHand::requested_position_all() const -> unsigned
{
  return to_sorted_pos_all(hand_->requested_position_all());
}


void SortedHand::request_position_all(unsigned const pos_all)
{
  hand_->request_position_all(from_sorted_pos_all(pos_all));
}


void SortedHand::playcard(Card const card)
{
  hand_->playcard(card);
}


void SortedHand::playcard(unsigned const pos)
{
  hand_->playcard(from_sorted_pos(pos));
}


void SortedHand::playcard_all(unsigned const pos)
{
  auto const p = pos_all_to_pos(pos);
  hand_->playcard(from_sorted_pos(p));
}


void SortedHand::unplaycard(Card const card)
{
  hand_->unplaycard(card);
}


void SortedHand::replace_unknown_or_played_with(Card const card)
{
  auto& hand = *hand_;
  if (hand.count_played(card) > 0) {
    hand.unplaycard(card);
    return ;
  }
  if (hand.contains(Card::unknown)) {
    hand.set_known(card);
    return ;
  }

  for (auto const c : from_sorted_pos_) {
    if (hand.played(c)) {
      hand.remove(c);
      hand.add(card);
      return;
    }
  }
}


void SortedHand::add(vector<Card> const& cards)
{
  // here we know, that the cards are added at the end of the hand
  for (unsigned c = 0; c < cards.size(); c++) {
    to_sorted_pos_.push_back(c + cardsnumber());
    from_sorted_pos_.push_back(c + cardsnumber());
  }
  hand_->add(cards);

  if (sorted)
    sort();
}


void SortedHand::add(HandCards const& cards)
{
  add(static_cast<vector<Card> >(cards));
}


void SortedHand::remove(HandCard const card)
{
  // the position in the hand of the removed card
  auto const pos_all = hand_->getpos_all(card);
  // the position in the sorted hand of the removed card
  auto const sorted_pos_all = to_sorted_pos_all(pos_all);

  hand_->remove(card);

  // update the sorting vectors
  //vector<unsigned> to_sorted_pos_;
  //vector<unsigned> from_sorted_pos_;

  from_sorted_pos_.erase(from_sorted_pos_.begin()
                               + sorted_pos_all);
  for_each(from_sorted_pos_,
           [pos_all](auto& p) {
           if (p > pos_all)
           p -= 1;
           });

  to_sorted_pos_.erase(to_sorted_pos_.begin() + pos_all);
  for_each(to_sorted_pos_,
           [sorted_pos_all](auto& p) {
           if (p > sorted_pos_all)
           p -= 1;
           });
}


void SortedHand::set_known(Card const card)
{
  hand_->set_known(card);
}


void SortedHand::remove(HandCards const& cards)
{
  for (auto const& c : cards)
    remove(c);
}


void SortedHand::set_sorting()
{
  if (sorted)
    sort();
  else
    unsort();
}


auto SortedHand::sort() -> bool
{
  from_sorted_pos_.clear();
  for (unsigned i = 0; i < cardsnumber_all(); ++i)
    from_sorted_pos_.push_back(i);

  bool changed = false;
  if (::preferences(Preferences::Type::cards_order).sorted()) {
    // a bad bubble sort, but we have at max 12 cards

    for (unsigned c1 = 0; c1 < cardsnumber_all(); ++c1)
      for (unsigned c2 = c1 + 1; c2 < cardsnumber_all(); ++c2)
        if (HandCard::relative_position(hand_->card_all(from_sorted_pos_[c1]),
                                        hand_->card_all(from_sorted_pos_[c2]))
            > 0) {
          std::swap(from_sorted_pos_[c1], from_sorted_pos_[c2]);
          changed = true;
        }

    set_positions();
  }

  sorted = true;

  return changed;
}


void SortedHand::unsort()
{
  from_sorted_pos_.clear();
  for (unsigned i = 0; i < cardsnumber_all(); ++i)
    from_sorted_pos_.push_back(i);

  set_positions();

  sorted = false;
}


void SortedHand::set_positions()
{
  to_sorted_pos_.resize(from_sorted_pos_.size());

  for (unsigned c = 0; c < cardsnumber_all(); c++)
    to_sorted_pos_[from_sorted_pos_[c]] = c;
}


auto operator==(SortedHand const& lhs, SortedHand const& rhs) -> bool
{
  return (   (lhs.hand_ == rhs.hand_)
          && (lhs.sorted == rhs.sorted)
          && (lhs.to_sorted_pos_ == rhs.to_sorted_pos_));
}


auto operator!=(SortedHand const& lhs, SortedHand const& rhs) -> bool
{
  return !(lhs == rhs);
}


auto operator<<(ostream& ostr, SortedHand const& sorted_hand) -> ostream&
{
  for (unsigned i = 0; i < sorted_hand.cardsnumber_all(); i++) {
    ostr << std::hex << i << std::dec << "  " << sorted_hand.card_all(i);
    if (sorted_hand.played(i))
      ostr << "\t(played " << setw(2) << sorted_hand.played_trick(i) << ")";
    ostr << "\n";
  }

  return ostr;
}


auto gettext(SortedHand const& hand) -> string
{
  ostringstream ostr;
  for (unsigned i = 0; i < hand.cardsnumber(); ++i) {
    ostr << gettext(hand.card(i)) << '\n';
  }

  return ostr.str();
}
