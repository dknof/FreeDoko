/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

class Hand;
class HandCard;
class HandCards;
class Card;

#include "sorted_hand_const.h"

/**
 ** A sorted hand.
 ** This is a wrap around ::Hand
 **/
class SortedHand {
  friend auto operator==(SortedHand const& lhs, SortedHand const& rhs) -> bool;
public:
  SortedHand() = delete;
  explicit SortedHand(Hand& hand);
  SortedHand(SortedHand const& hand);
  auto operator=(SortedHand const& hand) -> SortedHand&;

  ~SortedHand();

  operator SortedHandConst() const;

  auto hand()            const -> Hand const&;
  operator Hand const&() const;

  auto cardsnumber()     const -> unsigned;
  auto cardsnumber_all() const -> unsigned;

  auto card(unsigned i)         const -> HandCard;
  auto card_all(unsigned i)     const -> HandCard;
  auto played(unsigned i)       const -> bool;
  auto played_trick(unsigned i) const -> unsigned;

  auto from_sorted_pos(unsigned i)     const -> unsigned;
  auto from_sorted_pos_all(unsigned i) const -> unsigned;
  auto to_sorted_pos(unsigned i)       const -> unsigned;
  auto to_sorted_pos_all(unsigned i)   const -> unsigned;

  auto pos_to_pos_all(unsigned pos) const -> unsigned;
  auto pos_all_to_pos(unsigned pos) const -> unsigned;

  auto contains_unknown_or_played() const -> bool;

  auto requested_card()         const -> HandCard;
  auto requested_position()     const -> unsigned;
  auto requested_position_all() const -> unsigned;
  //void request_position(unsigned const position);
  void request_position_all(unsigned position_all);
  //void request_card(Card card);
  //void forget_request();

  void playcard(Card c);
  void playcard(unsigned pos);
  void playcard_all(unsigned pos);
  void unplaycard(Card c);
  void replace_unknown_or_played_with(Card card);

  void add(Card card);
  void add(vector<Card> const& cards);
  void add(HandCards const& cards);
  void remove(Card card);
  void remove(HandCard card);
  void remove(HandCards const& cards);
  void set_known(Card card);

  void set_sorting();

  auto sort() -> bool;
  void unsort();

private:
  void set_positions();

private:
  Hand* hand_ = nullptr;
  vector<unsigned> to_sorted_pos_; // position in the sorted hand
  vector<unsigned> from_sorted_pos_; // original position (SortedHand -> Hand)

  bool sorted = false;
}; // class SortedHand

auto operator==(SortedHand const& lhs, SortedHand const& rhs) -> bool;
auto operator!=(SortedHand const& lhs, SortedHand const& rhs) -> bool;

auto operator<<(ostream& ostr, SortedHand const& hand) -> ostream&;

auto gettext(SortedHand const& hand) -> string;
