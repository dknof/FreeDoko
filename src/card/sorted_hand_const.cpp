/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "sorted_hand_const.h"

#include "hand.h"

#include "../misc/preferences.h"
#include "../utils/random.h"

SortedHandConst::SortedHandConst(Hand const& hand) :
  hand_(&hand),
  to_sorted_pos_(hand.cardsnumber_all()),
  from_sorted_pos_(hand.cardsnumber_all()),
  sorted(::preferences(Preferences::Type::cards_order).sorted())
{
  for (unsigned i = 0; i < cardsnumber_all(); ++i)
    to_sorted_pos_.push_back(i);

  set_sorting();
}


SortedHandConst::SortedHandConst(SortedHandConst const&) = default;


auto SortedHandConst::operator=(SortedHandConst const&) -> SortedHandConst& = default;


SortedHandConst::~SortedHandConst() = default;


auto SortedHandConst::hand() const -> Hand const&
{ return *hand_; }


SortedHandConst::operator Hand const&() const
{ return *hand_; }


auto SortedHandConst::cardsnumber() const -> unsigned
{
  return hand_->cardsnumber();
}


auto SortedHandConst::cardsnumber_all() const -> unsigned
{
  return hand_->cardsnumber_all();
}


auto SortedHandConst::card(unsigned const i) const -> HandCard
{
  return hand_->card(from_sorted_pos(i));
}


auto SortedHandConst::card_all(unsigned const i) const -> HandCard
{
  DEBUG_ASSERTION((i < cardsnumber_all()),
		  "SortedHandConst::card_all(i):\n"
		  "  illegal value " << i
		  << " (maximal value is: "
		  << cardsnumber_all() - 1 << ")");

  return hand_->card_all(from_sorted_pos_all(i));
}


auto SortedHandConst::played(unsigned const i) const -> bool
{
  DEBUG_ASSERTION((i < cardsnumber_all()),
		  "SortedHandConst::played(i):\n"
		  "  illegal value " << i
		  << " (maximal value is: "
		  << cardsnumber_all() - 1 << ")");

  return hand_->played(from_sorted_pos_all(i));
}


auto SortedHandConst::played_trick(unsigned const i) const -> unsigned
{
  DEBUG_ASSERTION((i < cardsnumber_all()),
		  "SortedHandConst::played_trick(i):\n"
		  "  illegal value " << i
		  << " (maximal value is: "
		  << cardsnumber_all() - 1 << ")");

  return hand_->played_trick(from_sorted_pos_all(i));
}


auto SortedHandConst::from_sorted_pos(unsigned const i) const -> unsigned
{
  DEBUG_ASSERTION((i < cardsnumber()),
		  "SortedHandConst::from_sorted_pos(i):\n"
		  "  illegal value " << i
		  << " (maximal value is: "
		  << cardsnumber() - 1 << ")");

  return hand_->pos_all_to_pos(from_sorted_pos_all(pos_to_pos_all(i)));
}


auto SortedHandConst::from_sorted_pos_all(unsigned const i) const -> unsigned
{
  DEBUG_ASSERTION((i < cardsnumber_all()),
		  "SortedHandConst::from_sorted_pos_all(i):\n"
		  "  illegal value " << i
		  << " (maximal value is: "
		  << cardsnumber_all() - 1 << ")");

  return from_sorted_pos_[i];
}


auto SortedHandConst::to_sorted_pos(unsigned const i) const -> unsigned
{
  if (i == UINT_MAX)
    return UINT_MAX;

  DEBUG_ASSERTION((i < cardsnumber()),
		  "SortedHandConst::to_sorted_pos(i):\n"
		  "  illegal value " << i
		  << " (maximal value is: "
		  << cardsnumber() - 1 << ")");

  return hand_->pos_all_to_pos(to_sorted_pos_all(pos_to_pos_all(i)));
}


auto SortedHandConst::to_sorted_pos_all(unsigned const i) const -> unsigned
{
  if (i == UINT_MAX)
    return UINT_MAX;

  DEBUG_ASSERTION((i < cardsnumber_all()),
		  "SortedHandConst::to_sorted_pos_all(i):\n"
		  "  illegal value " << i
		  << " (maximal value is: "
		  << cardsnumber_all() - 1 << ")");

  return to_sorted_pos_[i];
}


auto SortedHandConst::pos_to_pos_all(unsigned pos) const -> unsigned
{
 // The pos of the argument is the pos in the unplayed cards.
 // It is searched, what position in all cards 'pos' stands for.
  if (pos == UINT_MAX)
    return UINT_MAX;

  // translate the position
  unsigned n = 0;
  for (pos += 1; pos > 0; ++n)
    if (!hand_->played(from_sorted_pos_all(n)))
      pos -= 1;

  return (n - 1);
}


auto SortedHandConst::pos_all_to_pos(unsigned pos) const -> unsigned
{
 // The pos of the argument is the pos in all cards.
 // It is searched, what position in the remaining cards 'pos' stands for.
  if (pos == UINT_MAX)
    return UINT_MAX;

  // translate the position
  unsigned n = 0;
  for (pos += 1; pos > 0; pos--)
    if (!hand_->played(from_sorted_pos_all(pos - 1)))
      n++;

  return (n - 1);
}


auto SortedHandConst::contains_unknown_or_played() const -> bool
{
  return hand_->contains_unknown_or_played();
}


auto SortedHandConst::requested_card() const -> HandCard
{
  return hand_->requested_card();
}


auto SortedHandConst::requested_position() const -> unsigned
{
  return to_sorted_pos(hand_->requested_position());
}


auto SortedHandConst::requested_position_all() const -> unsigned
{
  return to_sorted_pos_all(hand_->requested_position_all());
}


void SortedHandConst::set_sorting()
{
  if (sorted)
    sort();
  else
    unsort();
}


auto SortedHandConst::sort() -> bool
{
 // @todo       overview, especially the return value
 // @todo       use std::sort
  from_sorted_pos_.clear();
  for (unsigned i = 0; i < cardsnumber_all(); ++i)
    from_sorted_pos_.push_back(i);

  bool changed = false;
  if (::preferences(Preferences::Type::cards_order).sorted()) {
    // a bad bubble sort, but we have at max 12 cards

    for (unsigned c1 = 0; c1 < cardsnumber_all(); ++c1)
      for (unsigned c2 = c1 + 1; c2 < cardsnumber_all(); ++c2)
        if (HandCard::relative_position(hand_->card_all(from_sorted_pos_[c1]),
                                        hand_->card_all(from_sorted_pos_[c2]))
            > 0) {
          std::swap(from_sorted_pos_[c1], from_sorted_pos_[c2]);
          changed = true;
        }

    set_positions();
  }

  sorted = true;

  return changed;
}


void SortedHandConst::unsort()
{
  from_sorted_pos_.clear();
  for (unsigned i = 0; i < cardsnumber_all(); ++i)
    from_sorted_pos_.push_back(i);

  set_positions();

  sorted = false;
}


void SortedHandConst::set_positions()
{
  to_sorted_pos_.resize(from_sorted_pos_.size());

  for (unsigned c = 0; c < cardsnumber_all(); c++)
    to_sorted_pos_[from_sorted_pos_[c]] = c;
}


auto operator==(SortedHandConst const& lhs, SortedHandConst const& rhs) -> bool
{
  return (   (lhs.hand_ == rhs.hand_)
          && (lhs.sorted == rhs.sorted)
          && (lhs.to_sorted_pos_ == rhs.to_sorted_pos_));
}


auto operator!=(SortedHandConst const& lhs, SortedHandConst const& rhs) -> bool
{
  return !(lhs == rhs);
}


auto operator<<(ostream& ostr, SortedHandConst const& sorted_hand) -> ostream&
{
  for (unsigned i = 0; i < sorted_hand.cardsnumber_all(); i++) {
    ostr << std::hex << i << std::dec << "  " << sorted_hand.card_all(i);
    if (sorted_hand.played(i))
      ostr << "\t(played " << setw(2) << sorted_hand.played_trick(i) << ")";
    ostr << "\n";
  }

  return ostr;
}
