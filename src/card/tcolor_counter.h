/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "card.h"

/** counts the number of the tcolors
 **/
class TColorCounter : private vector<unsigned> {
  friend auto operator==(TColorCounter const& lhs, TColorCounter const& rhs) -> bool;
  friend auto operator!=(TColorCounter const& lhs, TColorCounter const& rhs) -> bool;

  public:
  TColorCounter();
  TColorCounter(TColorCounter const& tcolor_counter) = default;
  auto operator=(TColorCounter const& tcolor_counter) -> TColorCounter& = default;
  ~TColorCounter() = default;

  // clears all data
  void clear();

  // writes the data in 'ostr'
  void write(ostream& ostr) const;

  using vector<unsigned>::const_iterator;
  using vector<unsigned>::begin;
  using vector<unsigned>::end;


  // returns the total number of cards
  auto cards_no() const -> unsigned;
  // returns the number for 'tcolor'
  auto operator[](Card::TColor tcolor) const -> unsigned const&;
  private:
  // returns the number for 'tcolor'
  auto operator[](Card::TColor tcolor) -> unsigned&;
  public:

  // set the counter of 'tcolor' to exactly 'n'
  auto set(Card::TColor tcolor, unsigned n) -> bool;
  // set the counter of 'tcolor' to minimal 'n'
  auto min_set(Card::TColor tcolor, unsigned n) -> bool;
  // set the counter of 'tcolor' to maximal 'n'
  auto max_set(Card::TColor tcolor, unsigned n) -> bool;

  // increment the counter of 'tcolor'
  void inc(Card::TColor tcolor);
  // decrement the counter of 'tcolor'
  void dec(Card::TColor tcolor);

  private:
}; // class TColorCounter : private vector<unsigned>

auto operator<<(ostream& ostr, TColorCounter const& tcolor_counter) -> ostream&;

auto operator==(TColorCounter const& lhs, TColorCounter const& rhs) -> bool;
auto operator!=(TColorCounter const& lhs, TColorCounter const& rhs) -> bool;
