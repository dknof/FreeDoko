/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */


#include "trick.h"

#include "hand.h"
#include "../game/game.h"
#include "../player/player.h"
#include "../party/party.h"
#include "../party/rule.h"
#include "../ui/ui.h"
#include "../utils/string.h"

Trick const Trick::empty = Trick();

Trick::Trick(Player const& startplayer) :
  game_(&startplayer.game()),
  no_(startplayer.game().tricks().current_no()),
  startplayer_(startplayer.no())
{ }

Trick::Trick(unsigned const startplayer,
             vector<Card> const& cards,
             unsigned const winnerplayer) :
  startplayer_(startplayer),
  winnerplayer_(winnerplayer)
{ 
  for (auto const card : cards) {
    cards_.emplace_back(card);
  }
}

Trick::Trick(istream& istr)
{
  auto getline = [](istream& istr, string& line) {
    std::getline(istr, line);
    if (*(line.end() - 1) == '\r')
      line.erase(line.end() - 1);
  };

  string line;
  getline(istr, line);
  if (String::word_first(line) != "startplayer:")
    return ;

  String::word_first_remove(line);
  startplayer_ = static_cast<unsigned>(stoi(line));

  while (istr.good()) {
    getline(istr, line);
    if (line.empty())
      break;
    if (String::word_first(line) == "winner:")
      break;

    // the line is like: 1: club ace
    String::word_first_remove(line);
    String::remove_blanks(line);
    cards_.push_back(HandCard(Card(line)));
  } // while (istr.good())
  if (line.empty()
      || !istr.good())
    return ;

  if (String::word_first(line) == "winner:") {
    String::word_first_remove(line);
    winnerplayer_ = static_cast<unsigned>(stoi(line));
  } // if (String::word_first(line) == "winner:")
}

Trick::Trick() = default;

Trick::Trick(Trick const& trick) = default;

auto Trick::operator=(Trick const& trick) -> Trick& = default;

auto Trick::begin() const -> const_iterator
{
  return cards_.begin();
}

auto Trick::end() const -> const_iterator
{
  return cards_.end();
}

void Trick::set_game(Game const& game)
{
  game_ = &game;
  for (auto& c : cards_)
    c.set_hand(game.player(c.player().no()).hand());
}

auto Trick::game() const -> Game const&
{
  DEBUG_ASSERTION(game_,
                  "Trick::game()\n"
                  "  game is not set");
  return *game_;
}

auto Trick::cards() const -> Cards const&
{
  return cards_;
}


auto Trick::no() const -> unsigned
{
  return no_;
}

auto Trick::intrickpile() const -> bool
{
  return intrickpile_;
}

auto Trick::self_check() const -> bool
{
  DEBUG_ASSERTION(game_ != nullptr,
                  "Trick::self_check()\n"
                  "  no game");

  DEBUG_ASSERTION((no_ <= game_->tricks().current_no()),
                  "Trick::self_check()\n"
                  "  number not valid: " << no_);

  DEBUG_ASSERTION((this == &game_->tricks().trick(no_)),
                  "Trick::self_check()\n"
                  "  Not the trick the number says to be");

  DEBUG_ASSERTION(startplayerno() != UINT_MAX,
                  "Trick::self_check()\n"
                  "  no startplayer");

  DEBUG_ASSERTION((!intrickpile_
                   || (winnerplayerno() != UINT_MAX)),
                  "Trick::self_check()\n"
                  "  in trickpile but no winnerplayer");

  return true;
}

auto operator<<(ostream& ostr, Trick const& trick) -> ostream&
{
  ostr << "startplayer: " << trick.startplayerno() << "\n";
  for(unsigned i = 0; i < trick.actcardno(); i++) {
    ostr
      << (trick.game_ != nullptr
          ? trick.playerno_of_card(i)
          : (trick.startplayerno() + i))
      << ": "
      << trick.card(i) << "\n";
  }

  if (trick.isfull())
    ostr << "winner: " << trick.winnerplayerno() << "\n";

  return ostr;
}

void Trick::add(HandCard const card)
{
  DEBUG_ASSERTION(!card.is_empty(),
                  "Trick::add(HandCard):\n"
                  "  shall add empty card to the trick");

  DEBUG_ASSERTION(   !game_
                  || !isfull(),
                  "Trick::add(HandCard):\n"
                  "  trick is already full");

  DEBUG_ASSERTION((   !game_
                   || (&card.player() == &actplayer())),
                  "Trick::add(HandCard):\n"
                  "  Card is from the wrong player "
                  << card.player().no()
                  << " (should be " << actplayer().no() << ")\n"
                  "  Pointer: " << &card.player() << " != "
                  << &actplayer());

  if (game_ != nullptr) {
    // update the winnerplayer
    if (isstartcard()) {
      winnerplayer_ = startplayer_;
    } else {
      if (winnercard().is_jabbed_by(card)) {
        winnerplayer_ = actplayer().no();
      }
    } // if !(isstartcard())
  } // if (game_)

  // add the card to the trick
  cards_.push_back(card);
}

auto Trick::operator+(HandCard card) const -> Trick
{
  Trick trick2 = *this;
  trick2.add(card);
  return trick2;
}

auto Trick::till_card(unsigned const c) const -> Trick
{
  DEBUG_ASSERTION((c <= actcardno()),
                  "Trick::till_card(" << c << ")\n"
                  "  the trick contains only " << actcardno() << " cards");

  Trick t(startplayer());
  for (unsigned i = 0; i <= c; ++i)
    t.add(card(i));

  return t;
}

auto Trick::till_player(Player const& player) const -> Trick
{
  DEBUG_ASSERTION(has_played(player),
                  "Trick::till_player(" << player.no() << ")\n"
                  "  the player has not played so far");

  Trick t(startplayer());
  auto const n = cardno_of_player(player);
  for (unsigned i = 0; i <= n; ++i)
    t.add(card(i));

  return t;
}

auto Trick::before_player(Player const& player) const -> Trick
{
  DEBUG_ASSERTION(has_played(player),
                  "Trick::before_player(" << player.no() << ")\n"
                  "  the player has not played so far");

  Trick t(startplayer());
  auto const n = cardno_of_player(player);
  for (unsigned i = 0; i < n; ++i)
    t.add(card(i));

  return t;
}

auto Trick::before_last_played_card() const -> Trick
{
  DEBUG_ASSERTION(!isempty(),
                  "Trick::before_before_last_played_card()\n"
                  "  the trick is empty");

  Trick t(startplayer());
  for (unsigned i = 0; i + 1 < actcardno(); ++i)
    t.add(card(i));

  return t;
}

auto Trick::card(unsigned const c) const -> HandCard
{
  DEBUG_ASSERTION((c < actcardno()),
                  "Trick::card(c):\n"
                  "  'c' is to great (" << c << ">=" << actcardno()
                  << ")");

  return cards_[c];
}

auto Trick::card_of_player(Player const& player) const -> HandCard
{
  return ((player.no() >= startplayerno())
          ? card(player.no()
                 - startplayerno())
          : card(player.no()
                 + game().playerno()
                 - startplayerno())
         );
}

auto Trick::player_of_card(unsigned const c) const -> Player const&
{
  return game().player(playerno_of_card(c));
}

auto Trick::playerno_of_card(unsigned const c) const -> unsigned
{
  return ((startplayerno() + c
           < game().playerno())
          ? (c + startplayerno())
          : (c + startplayerno()
             - game().playerno())
         );
}

auto Trick::cardno_of_player(Player const& player) const -> unsigned
{
  return ((startplayerno()
           <= player.no())
          ? (player.no() - startplayerno())
          : (player.no()
             + game().playerno()
             - startplayerno())
         );
}

auto Trick::has_played(Player const& player) const -> bool
{
  return (cardno_of_player(player) < actcardno());
}


auto Trick::playes_before(Player const& lhs, Player const& rhs) const -> bool
{
  return cardno_of_player(lhs) < cardno_of_player(rhs);
}

auto Trick::contains(Card const card) const -> bool
{
  return (find(cards_, card) != cards_.end());
}

auto Trick::contains_trump() const -> bool
{
  return (find_if(cards_,
                  [](auto const& card)
                  { return card.istrump();
                  })
          != cards_.end());
}

auto Trick::contains_fox() const -> bool
{
  return (find_if(cards_,
                  [](auto const& card)
                  { return card.isfox();
                  })
          != cards_.end());
}

auto Trick::contains_possible_extrapoint(Game const& game) const -> bool
{
  if (  game.rule()(Rule::Type::extrapoints_in_solo)
      ? !is_with_trump_color(game.type())
      : game.is_solo())
    return false;

  auto const& rule = game.rule();
  auto const is_last_trick = game.tricks().is_last_trick();

  if (   rule(Rule::Type::extrapoint_catch_fox)
      && contains(game.cards().fox())
      && !game.swines().swines_announced())
    return true;
  if (   rule(Rule::Type::extrapoint_fox_last_trick)
      && is_last_trick
      && winnercard().isfox())
    return true;
  if (   rule(Rule::Type::extrapoint_catch_fox_last_trick)
      && is_last_trick
      && contains(game.cards().fox()))
    return true;
  if (   rule(Rule::Type::extrapoint_charlie)
      && is_last_trick
      && (winnercard() == Card::charlie) )
    return true;
  if (   rule(Rule::Type::extrapoint_catch_charlie)
      && is_last_trick
      && contains(Card::charlie) )
    return true;
  if (   rule(Rule::Type::extrapoint_dulle_jabs_dulle)
      && (winnercard() == Card::dulle) )
    return true;
  if (rule(Rule::Type::extrapoint_heart_trick)) {
    unsigned c = 0;
    for (c = 0; c < actcardno(); c++)
      if (card(c).tcolor() != Card::heart)
        break;
    if (c == actcardno())
      return true;
  }

  return false;
}

auto Trick::count(Card::TColor const color) const -> unsigned
{
  return std::count_if(begin(), end(), [color](auto const card) { return card.tcolor() == color; });
}

auto Trick::startplayerno() const -> unsigned
{
  return startplayer_;
}

auto Trick::startplayer() const -> Player const&
{
  DEBUG_ASSERTION(startplayerno() != UINT_MAX,
                  "Trick::startplayer():\n"
                  "  startplayerno() == NULL != UINT_MAX");

  return game().player(startplayerno());
}

auto Trick::lastplayer() const -> Player const&
{
  DEBUG_ASSERTION(startplayerno() != UINT_MAX,
                  "Trick::lastplayer():\n"
                  "  startplayerno() == N != UINT_MAX");

  return game().player((startplayerno()
                        + game().playerno() - 1)
                       % game().playerno());
}

auto Trick::secondlastplayer() const -> Player const&
{
  DEBUG_ASSERTION(startplayerno() != UINT_MAX,
                  "Trick::secondlastplayer():\n"
                  "  startplayerno() == N != UINT_MAX");

  return game().player((startplayerno()
                        + game().playerno() - 2)
                       % game().playerno());
}

auto Trick::remaining_players() const -> vector<std::reference_wrapper<Player const>>
{
  auto const playerno = game().playerno();
  vector<std::reference_wrapper<Player const>> players;
  for (auto i = actcardno(); i < playerno; ++i) {
    players.emplace_back(std::ref(player_of_card(i)));
  }
  return players;
}

auto Trick::remaining_following_players() const -> vector<std::reference_wrapper<Player const>>
{
  auto const playerno = game().playerno();
  vector<std::reference_wrapper<Player const>> players;
  for (auto i = actcardno() + 1; i < playerno; ++i) {
    players.emplace_back(std::ref(player_of_card(i)));
  }
  return players;
}

void Trick::set_startplayer(Player const& startplayer)
{
#ifndef WITH_DOKOLOUNGE
  DEBUG_ASSERTION(startplayerno() == UINT_MAX,
                  "Trick::set_startplayer(startplayer):\n"
                  "  startplayer already set");
#endif

  startplayer_ = startplayer.no();
}

void Trick::set_startplayer(unsigned const startplayerno)
{
#ifndef WITH_DOKOLOUNGE
  DEBUG_ASSERTION(startplayer_ == UINT_MAX,
                  "Trick::set_startplayer(startplayer):\n"
                  "  startplayer already set");
#endif

  startplayer_ = startplayerno;
}

auto Trick::startcard() const -> HandCard
{
  DEBUG_ASSERTION(!isstartcard(),
                  "Trick::startcard():\n"
                  "  trick is empty");

  return cards_[0];
}

auto Trick::startcolor() const -> Card::TColor
{
  return startcard().tcolor();
}

auto Trick::isstartcard() const -> bool
{
  return cards_.empty();
}

auto Trick::isempty() const -> bool
{
  return cards_.empty();
}

auto Trick::isfull() const -> bool
{
  if (game_ == nullptr)
    return true;
  return (remainingcardno() == 0);
}

auto Trick::islastcard() const -> bool
{
  return (remainingcardno() == 1);
}

auto Trick::issecondlastcard() const -> bool
{
  return (remainingcardno() == 2);
}

void Trick::move_in_trickpile()
{
  // Bug: const cast
  const_cast<Player&>(winnerplayer()).move_in_trickpile(*this);

  intrickpile_ = true;

  if (!game().isvirtual())
    ::ui->trick_move_in_pile();
}

auto Trick::actcardno() const -> unsigned
{
  return cards_.size();
}

auto Trick::remainingcardno() const -> unsigned
{
  return (game().playerno() - actcardno());
}

auto Trick::actplayer() const -> Player const&
{
  return game().player(actplayerno());
}

auto Trick::actplayerno() const -> unsigned
{
  return ((startplayerno() + actcardno())
          % game().playerno());
}

auto Trick::nextplayer() const -> Player const&
{
  DEBUG_ASSERTION(!islastcard(),
                  "Trick::nextplayer()\n"
                  "  This is already the last card");
  return game().players().following(actplayer());
}

auto Trick::points() const -> Points
{
  auto const card_value = [](Card const& card) { return card.is_unknown() ? 0 : card.points(); };
  return sum(cards_, card_value);
}

auto Trick::isvalid(HandCard card) const -> bool
{
  return card.isvalid(*this);
}

auto Trick::isvalid(Card const card, Hand const& hand) const -> bool
{
  return isvalid(HandCard(hand, card));
}

auto Trick::winnerplayerno() const -> unsigned
{
  return winnerplayer_;
}

auto Trick::winnerplayer() const -> Player const&
{
  // for Game::numberoftricks_of_player()
  if (winnerplayer_ == UINT_MAX)
    return startplayer();

  DEBUG_ASSERTION(winnerplayer_ != UINT_MAX,
                  "Trick::winnerplayer():\n"
                  "  winnerplayerno() == UINT_MAX");

  return game().player(winnerplayer_);
}

auto Trick::winnerteam() const -> Team
{
  return winnerplayer().team();
}

auto Trick::winnerplayer_before() const -> Player const&
{
  DEBUG_ASSERTION(!isempty(),
                  "Trick::winnerplayer_before():\n"
                  "  trick is empty");
  DEBUG_ASSERTION(!isstartcard(),
                  "Trick::winnerplayer_before():\n"
                  "  trick has only one card");

  auto winnercard = card(0);
  auto const* winnerplayer = &startplayer();
  for (unsigned c = 1; c < actcardno() - 1; ++c) {
    if (winnercard.is_jabbed_by(card(c))) {
      winnercard = card(c);
      winnerplayer = &player_of_card(c);
    }
  }

  return *winnerplayer;
}


auto Trick::winnercard() const -> HandCard
{
  if (isempty())
    return {};

  return card_of_player(winnerplayer());
}

auto Trick::winnercardno() const -> unsigned
{
  return cardno_of_player(winnerplayer());
}

auto Trick::winnercard_before() const -> HandCard
{
  if (actcardno() <= 1)
    return {};

  auto winnercard = card(0);
  for (unsigned c = 1; c < actcardno() - 1; ++c)
    if (winnercard.is_jabbed_by(card(c)))
      winnercard = card(c);

  return winnercard;
}

auto Trick::jabs_cards_before(unsigned const cardno) const -> bool
{
  DEBUG_ASSERTION(cardno < actcardno(),
                  "Trick::jabs_cards_before(cardno = " << cardno << ")\n"
                  "  Card is not played, yet.\n"
                  "  Trick:\n" << *this);

  auto const& card = this->card(cardno);
  for (unsigned c = 0; c < cardno; ++c)
    if (!this->card(c).is_jabbed_by(card))
      return false;

  return true;
}

auto Trick::jabs_cards_before(Player const& player) const -> bool
{
  DEBUG_ASSERTION(has_played(player),
                  "Trick::jabs_cards_before(player = " << player.no() << ")\n"
                  "  Player has not played, yet.\n"
                  "  Trick:\n" << *this);

  return jabs_cards_before(cardno_of_player(player));
}

auto Trick::islast() const -> bool
{
  return (no() == game().tricks().max_size() - 1);
}

auto Trick::isjabbed(HandCard const card) const -> bool
{
  if (isempty())
    return true;
  else {
    if (card.possible_hyperswine())
      return true;
    else if (card.possible_swine())
      return !winnercard().ishyperswine();
    else
      return winnercard().is_jabbed_by(card);
  }
}

auto Trick::isjabbed(Card const card, Hand const& hand) const -> bool
{
  return isjabbed(HandCard(hand, card));
}

auto Trick::less(unsigned const lhs, unsigned rhs) const -> bool
{
  DEBUG_ASSERTION(   lhs < actcardno()
                  && rhs < actcardno(),
                  "Trick::less(" << lhs << ", " << rhs << ")\n"
                  "  one cardno is less than the actcardno " << actcardno()
                 );
  if (lhs == rhs)
    return false;
  else if (lhs < rhs)
    return card(lhs).is_jabbed_by(card(rhs));
  else
    return !card(rhs).is_jabbed_by(card(lhs));
}

auto operator==(Trick const& lhs, Trick const& rhs) -> bool
{
  if ((lhs.startplayerno() != rhs.startplayerno())
      || (lhs.actcardno() != rhs.actcardno()))
    return false;

  for (unsigned c = 0; c < lhs.actcardno(); c++)
    if (lhs.card(c) != rhs.card(c))
      return false;

  return (lhs.winnerplayerno() == rhs.winnerplayerno());
}

auto operator!=(Trick const& lhs, Trick const& rhs) -> bool
{
  return !(lhs == rhs);
}

auto Trick::specialpoints() const -> Specialpoints
{
  vector<Team> teams;

  for (auto const& p : game().players())
    teams.push_back(game().teaminfo().get(p));

  return specialpoints(teams);
}

auto Trick::specialpoints(vector<Team> const& teams) const -> Specialpoints
{
  Specialpoints sp;

  Team const local_winnerteam
    = ( (  (teams[winnerplayerno()] != Team::unknown)
         ? teams[winnerplayerno()]
         : Team::noteam) );

  auto const& rule = game().rule();

  // if this game is a solo there are no specialpoints
  auto const in_solo = (   is_solo(game().type())
                        && !(   game().type() == GameType::marriage_solo
                             && no() < game().marriage().determination_trickno()) );

  if (in_solo && !rule(Rule::Type::extrapoints_in_solo))
    return sp;

  // doppelkopf
  if (points() >= rule(Rule::Type::points_for_doppelkopf)) {
    Specialpoint s(Specialpoint::Type::doppelkopf, local_winnerteam);
    s.player_get_no = winnerplayerno();
    sp.push_back(s);
  } // if (points() >= rule(Rule::Type::points_for_doppelkopf))

  if (is_with_trump_color(game().type())) {
    bool dulle_caught = false;

    bool fl_caught_charlie = false;
    for (unsigned i = 0; i < actcardno(); i++) {
      // the card belongs to the winner team
      if (   ::is_real(local_winnerteam)
          && local_winnerteam == teams[playerno_of_card(i)])
        continue;

      // fox caught
      if (   card(i).isfox()
          && rule(Rule::Type::extrapoint_catch_fox)
          && (winnerplayerno() != playerno_of_card(i))) {
        {
          auto s = Specialpoint(Specialpoint::Type::caught_fox, local_winnerteam);
          s.player_get_no = winnerplayerno();
          s.player_of_no = playerno_of_card(i);

          sp.push_back(s);
        }

        if (islast()
            && rule(Rule::Type::extrapoint_catch_fox_last_trick)) {
          auto s = Specialpoint(Specialpoint::Type::caught_fox_last_trick, local_winnerteam);
          s.player_get_no = winnerplayerno();
          s.player_of_no = playerno_of_card(i);

          sp.push_back(s);
        }
      }

      // Charlie caught
      if (   islast()
          && card(i) == Card::charlie
          && !fl_caught_charlie
          && rule(Rule::Type::extrapoint_catch_charlie)
          && (!rule(Rule::Type::extrapoint_catch_charlie_only_with_diamond_queen)
              || winnercard() == Card::diamond_queen)
          && local_winnerteam != teams[playerno_of_card(i)]) {

        fl_caught_charlie=true;

        auto s = Specialpoint(Specialpoint::Type::caught_charlie, local_winnerteam);
        s.player_get_no = winnerplayerno();
        s.player_of_no = playerno_of_card(i);
        sp.push_back(s);
        // check for double charlies caught
        if (rule(Rule::Type::extrapoint_catch_double_charlie))
          for (unsigned n = i + 1; n < actcardno(); ++n) {
            if (   (card(n) == Card::charlie)
                && (teams[playerno_of_card(n)] != s.team) ) {
              auto s2 = Specialpoint(Specialpoint::Type::caught_charlie, local_winnerteam);
              s2.player_get_no = winnerplayerno();
              s2.player_of_no = playerno_of_card(n);
              sp.push_back(s2);
            }
          }
      } // if (charlie caught)

      // dulle caught
      if (is_with_trump_color(game().type()))
        dulle_caught |= card(i).isdulle();

    } // for (i < actcardno())

    if (rule(Rule::Type::extrapoint_dulle_jabs_dulle)
        && dulle_caught
        && (winnercard() == Card::dulle)) {
      for (unsigned c = 0; c < actcardno(); c++) {
        if (   (c != winnercardno())
            && (card(c) == Card::dulle)
            && (local_winnerteam != teams[playerno_of_card(c)]) ) {
          Specialpoint s(Specialpoint::Type::dulle_caught_dulle,
                         local_winnerteam);
          s.player_get_no = winnerplayerno();
          s.player_of_no = playerno_of_card(c);

          sp.push_back(s);
        } // if (dulle caught of opposite team)
      } // for (c)
    } // if (dulle caught)

    if (rule(Rule::Type::extrapoint_heart_trick)
        && (is_normal(game().type()))
        && (winnercard().tcolor() == Card::heart)) {
      unsigned c = 0;
      for (; c < actcardno(); ++c)
        if (card(c).tcolor() != Card::heart)
          break;
      if (c == actcardno()) {
        Specialpoint s(Specialpoint::Type::heart_trick,
                       local_winnerteam);
        s.player_get_no = winnerplayerno();

        sp.push_back(s);
      }
    } // if (heart trick)

    // check for foxes who are winning the last trick
    if (   islast()
        && winnercard().isfox()
        && rule(Rule::Type::extrapoint_fox_last_trick)) {

      auto s = Specialpoint(Specialpoint::Type::fox_last_trick, local_winnerteam);
      s.player_get_no = winnerplayerno();
      //s.player_of_no = winnerplayerno();
      sp.push_back(s);

      // check for double fox winning
      if (rule(Rule::Type::extrapoint_double_fox_last_trick))
        for (unsigned n = winnercardno()+1; n < actcardno(); ++n) {
          if (   card(n).isfox()
              && teams[playerno_of_card(n)]==s.team) {
            auto s2 = Specialpoint(Specialpoint::Type::fox_last_trick, local_winnerteam);
            s2.player_get_no = winnerplayerno();
            //s.player_of_no = playerno_of_card(n);
            sp.push_back(s2);
          }
        }
    }

    // check for charlies in lasttrick
    if (islast()) {
      if (   winnercard()==Card::charlie
          && rule(Rule::Type::extrapoint_charlie)) {
        auto s = Specialpoint(Specialpoint::Type::charlie,
                              local_winnerteam);

        s.player_get_no = winnerplayerno();
        //s.player_of_no = winnerplayerno();
        sp.push_back(s);

        // check for double charlies
        if (rule(Rule::Type::extrapoint_double_charlie)) {
          for (unsigned n = winnercardno()+1; n < actcardno(); ++n) {
            if (   card(n)==Card::charlie
                && teams[playerno_of_card(n)] == s.team) {
              auto s2 = Specialpoint(Specialpoint::Type::charlie,
                                     local_winnerteam);
              s2.player_get_no = winnerplayerno();
              //s2.player_of_no = playerno_of_card(n);

              sp.push_back(s2);
            }
          }
        }
      } // if (charlie)
    }
  }

  return sp;
}

auto Trick::specialpoints_cards() const -> vector<HandCard>
{
  auto const spv = specialpoints();

  vector<HandCard> cards;

  for (auto sp : specialpoints()) {
    switch (sp.type) {
    case Specialpoint::Type::nospecialpoint:
      break;
    case Specialpoint::Type::caught_fox:
    case Specialpoint::Type::caught_fox_last_trick: {
      auto const trump_ace = find(cards_, game().cards().fox());
      if (trump_ace != cards_.end())
        cards.push_back(*trump_ace);
      break;
    }
    case Specialpoint::Type::fox_last_trick:
      cards.push_back(winnercard());
      break;
    case Specialpoint::Type::charlie:
      cards.push_back(winnercard());
      break;
    case Specialpoint::Type::caught_charlie: {
      auto const charlie = find(cards_, Card::charlie);
      if (charlie != cards_.end())
        cards.push_back(*charlie);
      break;
    }
    case Specialpoint::Type::dulle_caught_dulle: {
      auto const dulle = find(rbegin(cards_), rend(cards_), Card::dulle);
      if (dulle != cards_.rend())
        cards.push_back(*dulle);
      break;
    }
    case Specialpoint::Type::heart_trick:
      cards.push_back(winnercard());
      break;
    case Specialpoint::Type::doppelkopf:
      // check for confusing winnercard
      if (   winnercard() != game().cards().fox()
          && winnercard() != Card::dulle) {
        cards.push_back(winnercard());
        break;
      }
      { // search better card
        HandCards::const_iterator c;
        for (c = cards_.begin();
             c != cards_.end();
             ++c) {
          if (   *c != game().cards().fox()
              && *c != Card::dulle) {
            cards.push_back(*c);
            break;
          }
        } // for (c \in cards_)
        if (c != cards_.end())
          break;
      } // search better card

      // found no better card -- taking winnercard
      cards.push_back(winnercard());
      break;
    case Specialpoint::Type::won:
    case Specialpoint::Type::no90:
    case Specialpoint::Type::no60:
    case Specialpoint::Type::no30:
    case Specialpoint::Type::no0:
    case Specialpoint::Type::no120_said:
    case Specialpoint::Type::no90_said:
    case Specialpoint::Type::no60_said:
    case Specialpoint::Type::no30_said:
    case Specialpoint::Type::no0_said:
    case Specialpoint::Type::no90_said_120_got:
    case Specialpoint::Type::no60_said_90_got:
    case Specialpoint::Type::no30_said_60_got:
    case Specialpoint::Type::no0_said_30_got:
    case Specialpoint::Type::announcement_reply:
    case Specialpoint::Type::contra_won:
    case Specialpoint::Type::solo:
    case Specialpoint::Type::bock:
      break;
    } // switch (sp.type)
  } // for (sp : spv)

  return cards;
}

auto Trick::is_specialpoints_card(unsigned const c) const -> bool
{
  DEBUG_ASSERTION((c < actcardno()),
                  "Trick::is_specialpoints_card(c):\n"
                  "  'c' is to great (" << c << ">=" << actcardno()
                  << ")");

  auto const playerno = playerno_of_card(c);
  auto const is_of_player = [playerno](HandCard const& card) {
    return card.player().no() == playerno;
  };
  return any_of(specialpoints_cards(), is_of_player);
}

auto gettext(Trick const& trick) -> string
{
  string text = trick.startplayer().name() + ":";
  for (auto const& card : trick) {
    text += " " + _(card) + ",";
  }
  if (text.back() == ',')
    text.pop_back();
  if (trick.isfull())
    text += " -> " + trick.winnerplayer().name();
  return text;
}
