/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "weighted_card_list.h"

/** standard constructor
 **
 ** @param    game   corresponding game
 **/
WeightedCardList::WeightedCardList(Game const& game) :
  game_(&game),
  tcolor_ptr(Card::number_of_tcolors, WeightedCardList::end()),
  tcolor_pos(Card::number_of_tcolors, -1)
{ }

/** copy constructor
 **
 ** @param    card_list   card list to copy
 **/
WeightedCardList::WeightedCardList(WeightedCardList const& card_list) :
  List(card_list),
  game_(card_list.game_),
  tcolor_ptr(Card::number_of_tcolors, WeightedCardList::end()),
  tcolor_pos(Card::number_of_tcolors, -1)
{ recreate_tcolor_ptrs(); }

/** copy operator
 **
 ** @param    card_list   card list to copy
 **
 ** @return   the card list
 **/
WeightedCardList&
WeightedCardList::operator=(WeightedCardList const& card_list)
{
  if (this == &card_list)
    return *this;

  static_cast<List&>(*this) = card_list;
  recreate_tcolor_ptrs();

  return *this;
} // WeightedCardList& WeightedCardList::operator=(WeightedCardList card_list)

/** @return   the corresponding game
 **/
Game const&
WeightedCardList::game() const
{ return *game_; }


/** clear the list
 **/
void
WeightedCardList::clear()
{
  List::clear();
  tcolor_ptr
    = vector<WeightedCardList::iterator>(Card::number_of_tcolors,
                                         end());
  tcolor_pos = vector<int>(Card::number_of_tcolors, -1);
}

/** write the list in the stream
 **
 ** @param    ostr   output stream
 **/
void
WeightedCardList::write(ostream& ostr) const
{
  for (auto wc = begin(); wc != end(); ++wc) {
    ostr << std::setw(5) << wc->first << ' ';
    char c = ' ';
    for (unsigned tcolor = 0; tcolor < Card::number_of_tcolors; ++tcolor) {
      if (wc == tcolor_ptr[tcolor]) {
        c = to_string(static_cast<Card::TColor>(tcolor))[0];
        break;
      }
    }
    ostr << c << ' ' << wc->second << '\n';
  }
  ostr << '\n';
  for (unsigned tcolor = 0; tcolor < Card::number_of_tcolors; ++tcolor) {
    ostr << setw(7) << static_cast<Card::TColor>(tcolor) << "  "
      << setw(2) << tcolor_pos[tcolor] << ' ';

    auto const& highest_card = highest(static_cast<Card::TColor>(tcolor));
    if (highest_card.is_empty())
      ostr << "--\n";
    else
      ostr << highest_card << '\n';
  }
} // void WeightedCardList::write(ostream& ostr) const

/** @return   the number of cards
 **/
unsigned
WeightedCardList::cards_no() const
{
  return size();
} // unsigned WeightedCardList::cards_no() const

/** add the card
 **
 ** @param    card        card to add
 ** @param    weighting   the weighting of the card
 **/
void
WeightedCardList::add(Card const card, int const weighting)
{
  // pointer to the position to insert the card
  auto wc = begin();
  // position of the card
  int pos = 0;
  // search the position to insert the card
  for (; wc != end(); ++wc, ++pos)
    if (weighting >= wc->first)
      break;
  wc = insert(wc, pair<int, Card>(weighting, card));

  { // update the pos of the highest tcolors
    for (unsigned tcolor = 0; tcolor < Card::number_of_tcolors; ++tcolor)
      if (tcolor_pos[tcolor] >= pos)
        tcolor_pos[tcolor]  += 1;
  } // update the pos of the highest tcolors

  { // update the highest pointer of the tcolor of the card
    auto const tcolor = card.tcolor(game());
    if (   (tcolor_pos[tcolor] == -1)
        || (tcolor_pos[tcolor] >= pos) ) {
      tcolor_pos[tcolor] = pos;
      tcolor_ptr[tcolor] = wc;
    } // if (tcolor_pos[tcolor] >= pos)
  } // update the highest pointer of the tcolor of the card
} // void WeightedCardList::add(Card card, int weighting)

/** @return   the card with the highest weighting
 **/
Card
WeightedCardList::highest() const
{
  if (empty())
    return Card::empty;

  return front().second;
} // Card WeightedCardList::highest() const

/** -> result
 **
 ** @param    tcolor   tcolor to check
 **
 ** @return   the card with the highest weighting of 'tcolor'
 **/
Card
WeightedCardList::highest(Card::TColor const tcolor) const
{
  if (tcolor_ptr[tcolor] == end())
    return Card::empty;

  return tcolor_ptr[tcolor]->second;
} // Card WeightedCardList::highest(Card::TColor tcolor) const

/** -> result
 **
 ** @param    tcolor   tcolor not to check
 **
 ** @return   the card with the highest weighting not of 'tcolor'
 **/
Card
WeightedCardList::highest_not_of(Card::TColor const tcolor) const
{
  auto const& ptr = highest_ptr_not_of(tcolor);
  if (ptr == end())
    return Card::empty;

  return ptr->second;
} // Card WeightedCardList::highest_not_of(Card::TColor tcolor) const

/** -> result
 **
 ** @param    tcolor_a   first tcolor to check
 ** @param    tcolor_b   second tcolor to check
 **
 ** @return   the card with the highest weighting of 'tcolor_a' or 'tcolor_b'
 **/
Card
WeightedCardList::highest(Card::TColor const tcolor_a,
                          Card::TColor const tcolor_b) const
{
  auto const& ptr = highest_ptr(tcolor_a, tcolor_b);
  if (ptr == end())
    return Card::empty;

  return ptr->second;
} // Card WeightedCardList::highest(Card::TColor tcolor_a, Card::TColor tcolor_b) const

/** -> result
 **
 ** @param    tcolor_a   first tcolor to check
 ** @param    tcolor_b   second tcolor to check
 ** @param    tcolor_c   third tcolor to check
 **
 ** @return   the card with the highest weighting of 'tcolor_'.
 **/
Card
WeightedCardList::highest(Card::TColor const tcolor_a,
                          Card::TColor const tcolor_b,
                          Card::TColor const tcolor_c) const
{
  auto const& ptr = highest_ptr(tcolor_a, tcolor_b, tcolor_c);
  if (ptr == end())
    return Card::empty;

  return ptr->second;
} // Card WeightedCardList::highest(Card::TColor tcolor_a, Card::TColor tcolor_b, Card::TColor tcolor_c) const

/** -> result
 **
 ** @param    tcolor_a   first tcolor to check
 ** @param    tcolor_b   second tcolor to check
 ** @param    tcolor_c   third tcolor to check
 ** @param    tcolor_d   fourth tcolor to check
 **
 ** @return   the card with the highest weighting of 'tcolor_'.
 **/
Card
WeightedCardList::highest(Card::TColor const tcolor_a,
                          Card::TColor const tcolor_b,
                          Card::TColor const tcolor_c,
                          Card::TColor const tcolor_d) const
{
  auto const& ptr = highest_ptr(tcolor_a, tcolor_b, tcolor_c, tcolor_d);
  if (ptr == end())
    return Card::empty;

  return ptr->second;
} // Card WeightedCardList::highest(Card::TColor tcolor_a, Card::TColor tcolor_b, Card::TColor tcolor_c, Card::TColor tcolor_d) const

/** @return   the highest weighting
 **            INT_MIN, if none exists
 **/
int
WeightedCardList::highest_weighting() const
{
  if (empty())
    return INT_MIN;

  return front().first;
} // int WeightedCardList::highest_weighting() const

/** -> result
 **
 ** @param    tcolor   tcolor
 **
 ** @return   the highest weighting of tcolor
 **            INT_MIN, if none exists
 **/
int
WeightedCardList::highest_weighting(Card::TColor const tcolor) const
{
  if (tcolor_ptr[tcolor] == end())
    return INT_MIN;

  return tcolor_ptr[tcolor]->first;
} // int WeightedCardList::highest_weighting(Card::TColor tcolor) const

/** -> result
 **
 ** @param    tcolor   tcolor
 **
 ** @return   the highest weighting not of tcolor
 **            INT_MIN, if none exists
 **/
int
WeightedCardList::highest_weighting_not_of(Card::TColor const tcolor) const
{
  auto const& ptr = highest_ptr_not_of(tcolor);
  if (ptr == end())
    return INT_MIN;

  return ptr->first;
} // int WeightedCardList::highest_weighting_not_of(Card::TColor tcolor) const

/** -> result
 **
 ** @param    tcolor_a   first tcolor to check
 ** @param    tcolor_b   second tcolor to check
 **
 ** @return   the highest weighting of 'tcolor_a' or 'tcolor_b'
 **/
int
WeightedCardList::highest_weighting(Card::TColor const tcolor_a,
                                    Card::TColor const tcolor_b) const
{
  auto const& ptr = highest_ptr(tcolor_a, tcolor_b);
  if (ptr == end())
    return INT_MIN;

  return ptr->first;
} // int WeightedCardList::highest_weighting(Card::TColor tcolor_a, Card::TColor tcolor_b) const

/** -> result
 **
 ** @param    tcolor_a   first tcolor to check
 ** @param    tcolor_b   second tcolor to check
 ** @param    tcolor_c   third tcolor to check
 **
 ** @return   the highest weighting of 'tcolor_'.
 **/
int
WeightedCardList::highest_weighting(Card::TColor const tcolor_a,
                                    Card::TColor const tcolor_b,
                                    Card::TColor const tcolor_c) const
{
  auto const& ptr = highest_ptr(tcolor_a, tcolor_b, tcolor_c);
  if (ptr == end())
    return INT_MIN;

  return ptr->first;
} // int WeightedCardList::highest_weighting(Card::TColor tcolor_a, Card::TColor tcolor_b, Card::TColor tcolor_c) const

/** -> result
 **
 ** @param    tcolor_a   first tcolor to check
 ** @param    tcolor_b   second tcolor to check
 ** @param    tcolor_c   third tcolor to check
 ** @param    tcolor_d   fourth tcolor to check
 **
 ** @return   the card with the highest weighting of 'tcolor_'.
 **/
int
WeightedCardList::highest_weighting(Card::TColor const tcolor_a,
                                    Card::TColor const tcolor_b,
                                    Card::TColor const tcolor_c,
                                    Card::TColor const tcolor_d) const
{
  auto const& ptr = highest_ptr(tcolor_a, tcolor_b, tcolor_c, tcolor_d);
  if (ptr == end()) {
    return INT_MIN;
  }

  return ptr->first;
} // int WeightedCardList::highest_weighting(Card::TColor tcolor_a, Card::TColor tcolor_b, Card::TColor tcolor_c, Card::TColor tcolor_d) const

/** @return   the pointer to the highest weighting
 **/
WeightedCardList::const_iterator
WeightedCardList::highest_ptr() const
{
  return begin();
} // WeightedCardList::const_iterator WeightedCardList::highest_ptr() const

/** -> result
 **
 ** @param    tcolor   tcolor to check
 **
 ** @return   the pointer to the highest weighting of 'tcolor'
 **/
WeightedCardList::iterator const&
WeightedCardList::highest_ptr(Card::TColor const tcolor) const
{
  return tcolor_ptr[tcolor];
} // WeightedCardList::iterator WeightedCardList::highest_ptr(Card::TColor const tcolor) const

/** -> result
 **
 ** @param    tcolor_a   first tcolor to check
 ** @param    tcolor_b   second tcolor to check
 **
 ** @return   the ptr with the highest weighting of 'tcolor_a' or 'tcolor_b'
 **/
WeightedCardList::iterator const&
WeightedCardList::highest_ptr(Card::TColor const tcolor_a,
                              Card::TColor const tcolor_b) const
{
  if (tcolor_ptr[tcolor_a] == end())
    return tcolor_ptr[tcolor_b];

  if (tcolor_ptr[tcolor_b] == end())
    return tcolor_ptr[tcolor_a];

  if (tcolor_pos[tcolor_a] < tcolor_pos[tcolor_b])
    return tcolor_ptr[tcolor_a];
  else
    return tcolor_ptr[tcolor_b];
} // WeightedCardList::iterator WeightedCardList::highest_ptr(Card::TColor tcolor_a, Card::TColor tcolor_b) const

/** -> result
 **
 ** @param    tcolor_a   first tcolor to check
 ** @param    tcolor_b   second tcolor to check
 ** @param    tcolor_c   third tcolor to check
 **
 ** @return   the ptr with the highest weighting of 'tcolor_'.
 **/
WeightedCardList::iterator const&
WeightedCardList::highest_ptr(Card::TColor const tcolor_a,
                              Card::TColor const tcolor_b,
                              Card::TColor const tcolor_c) const
{
  if (tcolor_ptr[tcolor_a] == end())
    return highest_ptr(tcolor_b, tcolor_c);

  if (tcolor_ptr[tcolor_b] == end())
    return highest_ptr(tcolor_a, tcolor_c);

  if (tcolor_pos[tcolor_a] < tcolor_pos[tcolor_b])
    return highest_ptr(tcolor_a, tcolor_c);
  else
    return highest_ptr(tcolor_b, tcolor_c);
} // WeightedCardList::iterator WeightedCardList::highest_ptr(Card::TColor tcolor_a, Card::TColor tcolor_b, Card::TColor tcolor_c) const

/** -> result
 **
 ** @param    tcolor_a   first tcolor to check
 ** @param    tcolor_b   second tcolor to check
 ** @param    tcolor_c   third tcolor to check
 ** @param    tcolor_d   fourth tcolor to check
 **
 ** @return   the ptr with the highest weighting of 'tcolor_'.
 **/
WeightedCardList::iterator const&
WeightedCardList::highest_ptr(Card::TColor const tcolor_a,
                              Card::TColor const tcolor_b,
                              Card::TColor const tcolor_c,
                              Card::TColor const tcolor_d) const
{
  if (tcolor_ptr[tcolor_a] == end())
    return highest_ptr(tcolor_b, tcolor_c, tcolor_d);

  if (tcolor_ptr[tcolor_b] == end())
    return highest_ptr(tcolor_a, tcolor_c, tcolor_d);

  if (tcolor_pos[tcolor_a] < tcolor_pos[tcolor_b])
    return highest_ptr(tcolor_a, tcolor_c, tcolor_d);
  else
    return highest_ptr(tcolor_b, tcolor_c, tcolor_d);
} // WeightedCardList::iterator WeightedCardList::highest_ptr(Card::TColor tcolor_a, Card::TColor tcolor_b, Card::TColor tcolor_c, Card::TColor tcolor_d) const

/** -> result
 **
 ** @param    tcolor   tcolor not to check
 **
 ** @return   the pointer to the highest weighting not of 'tcolor'
 **/
WeightedCardList::iterator const&
WeightedCardList::highest_ptr_not_of(Card::TColor const tcolor) const
{
  Card::TColor const tc = highest().tcolor(game());
  if (tc != tcolor)
    return highest_ptr(tc);

  return highest_ptr(( (tcolor == Card::diamond)
                            ? Card::heart
                            : Card::diamond),
                           (  (tcolor <= Card::heart)
                            ? Card::spade
                            : Card::heart),
                           (  (tcolor <= Card::spade)
                            ? Card::club
                            : Card::spade),
                           (  (tcolor <= Card::club)
                            ? Card::trump
                            : Card::club)
                          );
} // WeightedCardList::iterator WeightedCardList::highest_ptr_not_of(Card::TColor const tcolor) const

/** -> result
 **
 ** @param    card   card to return the (heighest) weighting of
 **
 ** @return   the (highest) weighting weighting of 'card'
 **            INT_MIN, if none exists
 **/
int
WeightedCardList::weighting(Card const card) const
{
  // search the card in the pointers
  for (auto ptr
       = tcolor_ptr[card.tcolor(game())];
       ptr != end();
       ++ptr)
    if (ptr->second == card)
      return ptr->first;

  return INT_MIN;
} // int WeightedCardList::weighting(Card card) const

/** remove the highest card
 **/
void
WeightedCardList::pop_highest()
{
  if (empty())
    return ;

  // the tcolor of the card to remove
  auto const tcolor = front().second.tcolor(game());

  // remove the card
  pop_front();

  // adjust the positions
  for (unsigned tc = 0; tc < Card::number_of_tcolors; ++tc)
    if (tcolor_pos[tc] >= 0)
      tcolor_pos[tc] -= 1;

  // search the first of 'tcolor' in the list
  recreate_tcolor_ptr(tcolor);
} // void WeightedCardList::pop_highest()

/** remove the highest card of the tcolor
 **
 ** @param    tcolor   tcolor to remove the highest card from
 **/
void
WeightedCardList::pop_highest(Card::TColor const tcolor)
{
  // search the card in the pointers
  auto& ptr = tcolor_ptr[tcolor];
  if (ptr == end())
    return ;

  int& pos = tcolor_pos[tcolor];
  // update the pos of the further tcolors
  for (unsigned tc = 0; tc < Card::number_of_tcolors; ++tc)
    if (tcolor_pos[tc] > pos)
      tcolor_pos[tc]  += 1;
  // remove the pointer
  ptr = erase(ptr);
  for (; ptr != end(); ++ptr, ++pos)
    if (ptr->second.tcolor(game()) == tcolor)
      break;
  if (ptr == end())
    pos = -1;
} // void WeightedCardList::pop_highest(Card::TColor tcolor)

/** remove the highest card not of the tcolor
 **
 ** @param    tcolor   tcolor to remove the highest card from
 **
 ** @todo      own code (for better performance)
 **/
void
WeightedCardList::pop_highest_not_of(Card::TColor const tcolor)
{
  pop(highest_not_of(tcolor));
} // void WeightedCardList::pop_highest_not_of(Card::TColor tcolor)

/** remove the given card
 **
 ** @param    card   card to remove
 **/
void
WeightedCardList::pop(Card const card)
{
  // search the card in the pointers
  auto const tcolor = card.tcolor(game());
  auto& ptr = tcolor_ptr[tcolor];
  if (  (ptr != end())
      && (ptr->second == card) ) {
    int& pos = tcolor_pos[tcolor];
    // update the pos of the further tcolors
    for (unsigned tc = 0; tc < Card::number_of_tcolors; ++tc)
      if (tcolor_pos[tc] > pos)
        tcolor_pos[tc]  += 1;
    // remove the pointer
    ptr = erase(ptr);
    for (; ptr != end(); ++ptr, ++pos)
      if (ptr->second.tcolor(game()) == tcolor)
        break;
    if (ptr == end())
      pos = -1;
  } else { // if !(tcolor_ptr[tcolor].second == card)
    int pos = tcolor_pos[tcolor];
    for (; ptr != end(); ++ptr, ++pos)
      if (ptr->second == card)
        break;
    DEBUG_ASSERTION(ptr != end(),
                    "WeightedCardList::pop(card)\n"
                    "  card '" << card << "' not found in the list:\n"
                    << *this);
    ptr = erase(ptr);
    // update the pos of the further tcolors
    for (unsigned tc = 0; tc < Card::number_of_tcolors; ++tc)
      if (tcolor_pos[tc] > pos)
        tcolor_pos[tc] -= 1;
  } // if !(tcolor_ptr[tcolor].second == card)
} // void WeightedCardList::pop(Card card)

/** recreate the pointers to the highest card of each tcolor
 **/
void
WeightedCardList::recreate_tcolor_ptrs()
{
  // search the first of 'tcolor' in the list
  for (unsigned tcolor = 0; tcolor < Card::number_of_tcolors; ++tcolor)
    recreate_tcolor_ptr(static_cast<Card::TColor>(tcolor));
} // void WeightedCardList::recreate_tcolor_ptrs()

/** recreate the pointer to the highest card of the tcolor
 **
 ** @param    tcolor   tcolor to recreate the pointer of
 **/
void
WeightedCardList::recreate_tcolor_ptr(Card::TColor const tcolor)
{
  // search the first of 'tcolor' in the list
  int& pos = tcolor_pos[tcolor];
  auto& ptr = tcolor_ptr[tcolor];
  for (pos = 0, ptr = begin(); ptr != end(); ++ptr, ++pos)
    if (ptr->second.tcolor(game()) == tcolor)
      break;
  if (ptr == end())
    pos = -1;
} // void WeightedCardList::recreate_tcolor_ptr(Card::TColor tcolor)

/** write 'card_list' in 'ostr'
 **
 ** @param    ostr        output stream
 ** @param    card_list   card list to write
 **/
ostream&
operator<<(ostream& ostr, WeightedCardList const& card_list)
{
  card_list.write(ostr);
  return ostr;
} // ostream& operator<<(ostream& ostr, WeightedCardList card_list)
