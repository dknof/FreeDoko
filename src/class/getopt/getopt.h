/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include <string>
#include <vector>

namespace GetOpt {

struct Syntax {
  enum Type {
    BOOL,
    INT,
    UNSIGNED,
    DOUBLE,
    CHAR,
    BSTRING
  }; // enum Type

  std::string name; // the (long) name of the option
  char short_name; // the short form of the option
  Type type; // the type of the option
}; // struct Syntax

struct Option {
  friend Option getopt(int& argc, char* argv[],
                       std::vector<Syntax> const& syntax);
  // the error code
  enum Error {
    OK,
    NO_OPTION,
    UNKNOWN_OPTION,
    FALSE_ARGUMENT,
    NO_ARGUMENT,
    UNKNOWN_ERROR
  }; // enum Error
  // different option types
  enum TypeBool     { BOOL };
  enum TypeInt      { INT      = BOOL     + 1 };
  enum TypeUnsigned { UNSIGNED = INT      + 1 };
  enum TypeDouble   { DOUBLE   = UNSIGNED + 1 };
  enum TypeChar     { CHAR     = DOUBLE   + 1 };
  // could not use 'STRING', 'B' stands for 'basic'
  enum TypeString   { BSTRING  = CHAR     + 1 };
  public:
  // constructor
  Option();
  // constructor
  Option(Option const& option);
  // setting equal
  Option& operator=(Option const& option);

  // desctuctor
  ~Option();

  // the error code
  Error error() const;
  // wether the parsing has failed
  bool fail() const;
  // whether an option could be parsed (with or without an error)
  operator bool() const;

  // the name of the option
  std::string const& name() const;
  // the Type of the option
  Syntax::Type type() const;

  // the boolean value
  bool value(TypeBool type) const;
  // the int value
  int value(TypeInt type) const;
  // the unsigned value
  unsigned value(TypeUnsigned type) const;
  // the double value
  double value(TypeDouble type) const;
  // the char value
  char value(TypeChar type) const;
  // the string value
  std::string const& value(TypeString type) const;
  // the string value
  std::string const& value_string() const;

  // set the value
  Error value_set(char const* argv);

  // ***  protected:
  public:
  Error error_v = OK;
  std::string name_v;
  Syntax::Type type_v = Syntax::BOOL;
  union {
    bool b;
    int i;
    unsigned u;
    double d;
    char c;
  } value_v; // NOLINT(modernize-use-default-member-init)
  std::string value_string_v;
}; // struct Option

// get the next option
auto getopt(int& argc, char* argv[], std::vector<Syntax> const& syntax)-> Option;
auto getopt(std::istream& istr, std::vector<Syntax> const& syntax)     -> Option;

} // namespace GetOpt

auto to_string(GetOpt::Option::Error error) -> std::string;
auto operator<<(std::ostream& ostr, GetOpt::Option::Error error) -> std::ostream&;
auto operator<<(std::ostream& ostr, GetOpt::Option option)       -> std::ostream&;
