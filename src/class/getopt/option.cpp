/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "getopt.h"
using std::string;

#include <iostream>
#include <cstdlib>
#include <climits>

using namespace GetOpt;

Option::Option() = default; // NOLINT(cppcoreguidelines-pro-type-member-init,hicpp-member-init)


Option::Option(Option const& option) = default; // NOLINT(cppcoreguidelines-pro-type-member-init,hicpp-member-init)


auto Option::operator=(Option const& option) -> Option& = default;


Option::~Option() = default;


auto Option::error() const -> Option::Error
{
  return this->error_v;
}


auto Option::fail() const -> bool
{
  return (   this->error() != OK
	  && this->error() != NO_OPTION);
}


Option::operator bool() const
{
  return (this->error() != NO_OPTION);
}


auto Option::name() const -> std::string const&
{
  return this->name_v;
}


auto Option::type() const -> Syntax::Type
{
  return this->type_v;
}


auto Option::value(TypeBool type) const -> bool
{
  return this->value_v.b; // NOLINT(cppcoreguidelines-pro-type-union-access)
}


auto Option::value(TypeInt type) const -> int
{
  return this->value_v.i; // NOLINT(cppcoreguidelines-pro-type-union-access)
}


auto Option::value(TypeUnsigned type) const -> unsigned
{
  return this->value_v.u; // NOLINT(cppcoreguidelines-pro-type-union-access)
}


auto Option::value(TypeDouble type) const -> double
{
  return this->value_v.d; // NOLINT(cppcoreguidelines-pro-type-union-access)
}


auto Option::value(TypeChar type) const -> char
{
  return this->value_v.b; // NOLINT(cppcoreguidelines-pro-type-union-access)
}


auto Option::value(TypeString type) const -> std::string const&
{
  return this->value_string_v;
}


auto Option::value_string() const -> std::string const&
{
  return this->value_string_v;
}


auto Option::value_set(char const* argv) -> Option::Error
{
  this->error_v = OK;

  switch(this->type()) {
  case Syntax::BOOL:
    // cannot be
    this->error_v = UNKNOWN_ERROR;
    break;
  case Syntax::INT:
    {
      char* endptr = nullptr;
      this->value_v.i = strtol(argv, &endptr, 0); // NOLINT(cppcoreguidelines-pro-type-union-access)
      if ((endptr == argv)
          || (*endptr != '\0'))
        this->error_v = FALSE_ARGUMENT;
      break;
    }
  case Syntax::UNSIGNED:
    {
      char* endptr = nullptr;
      this->value_v.u = strtoul(argv, &endptr, 0); // NOLINT(cppcoreguidelines-pro-type-union-access)
      if ((endptr == argv)
          || (*endptr != '\0')
          || (this->value_v.u == UINT_MAX) // NOLINT(cppcoreguidelines-pro-type-union-access)
          || (argv[0] == '-')) // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        this->error_v = FALSE_ARGUMENT;
      break;
    }
  case Syntax::DOUBLE:
    {
      char* endptr = nullptr;
      this->value_v.d = strtod(argv, &endptr); // NOLINT(cppcoreguidelines-pro-type-union-access)
      if ((endptr == argv)
          || (*endptr != '\0'))
        this->error_v = FALSE_ARGUMENT;
      break;
    }
  case Syntax::CHAR:
    this->value_v.c = *argv; // NOLINT(cppcoreguidelines-pro-type-union-access)
    if (argv[1] != '\0') // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
      this->error_v = FALSE_ARGUMENT;
    break;
  case Syntax::BSTRING:
    this->value_string_v = argv;
    this->error_v = OK;
    break;
  }; // switch(this->type())

  return this->error();
}

auto to_string(Option::Error const error) -> string
{
  switch(error) {
  case Option::OK:
    return "ok";
  case Option::NO_OPTION:
    return "no option";
  case Option::UNKNOWN_OPTION:
    return "unknown option";
  case Option::FALSE_ARGUMENT:
    return "false argument";
  case Option::NO_ARGUMENT:
    return "no argument";
  case Option::UNKNOWN_ERROR:
    return "unknown error";
  }

  return "";
}

auto operator<<(std::ostream& ostr, Option::Error const error) -> std::ostream&
{
  ostr << to_string(error);
  return ostr;
}


auto operator<<(std::ostream& ostr, Option option) -> std::ostream&
{
  if (option.name().empty()) {
    ostr << option.value_string();
    return ostr;
  }
  ostr << option.name();
  switch (option.type()) {
  case Syntax::BOOL:
    break;
  case Syntax::INT:
    ostr << ": " << option.value(Option::TypeInt::INT);
    break;
  case Syntax::UNSIGNED:
    ostr << ": " << option.value(Option::TypeUnsigned::UNSIGNED);
    break;
  case Syntax::DOUBLE:
    ostr << ": " << option.value(Option::TypeDouble::DOUBLE);
    break;
  case Syntax::CHAR:
    ostr << ": " << option.value(Option::TypeChar::CHAR);
    break;
  case Syntax::BSTRING:
    ostr << ": " << option.value(Option::TypeString::BSTRING);
    break;
  }
  return ostr;
}
