/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "readconfig.h"
#include <istream>
#include <iostream>
#include <sstream>
#include <cctype>
#include "../../utils/string.h"
using std::string;


// NOLINTNEXTLINE(misc-no-recursion)
auto operator>>(std::istream& istr, Config& config) -> std::istream&
{
  string line;

  std::getline(istr, line);
  if (!istr.good() && (line.length() == 0)) {
    config.separator = false;
    config.name = "";
    config.value = "";
    return istr;
  }

  // look, if the line is empty or a remark
  String::remove_blanks(line);
  if (line.empty())
    return (istr >> config);
  if (static_cast<string const&>(line)[0] == '#') // remark line
    return (istr >> config);

  // look, if there is an '=' in the line
  string::size_type pos = line.find('=');
  if (pos == string::npos) {
    // no '='
    config.separator = false;
    if (isalpha(static_cast<string const&>(line)[0])) { // NOLINT readability-implicit-bool-conversion
      // the whole line is the name
      config.name = line;
      config.value = "";
    } else { // if !(isalpha(static_cast<string const&>(line)[0]))
      // the first word is the name, the rest of the line the value
      for (pos = 0; pos < line.length(); pos++)
	if (isspace(static_cast<string const&>(line)[pos])) // NOLINT readability-implicit-bool-conversion
	  break;
      config.name = string(line, 0, pos);
      if (pos < line.length()) {
	config.value = string(line, pos + 1, std::string::npos);
      } else {
	config.value = "";
      }
    } // if !(isalpha(static_cast<string const&>(line)[0]))
  } else { // if !('=' in 'line')
    config.separator = true;
    config.name = string(line, 0, pos);
    config.value = string(line, pos + 1, std::string::npos);
  } // if !('=' in 'line')

  String::remove_blanks(config.value);
  String::remove_blanks(config.name);

  return istr;
}


Config::Config(std::string const& line)
{
  std::istringstream istr(line);
  istr >> *this;
}


auto Config::empty() const -> bool
{
  return name.empty() && value.empty();
}


auto operator<<(std::ostream& ostr, Config const& config) -> std::ostream&
{
  if (config.separator)
    ostr << config.name << " = " << config.value;
  else
    ostr << config.name << " " << config.value;

  return ostr;
}
