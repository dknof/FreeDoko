/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include <string>
#include <iosfwd>

// a function to read entries out of a configuration file

struct Config {
  Config() = default;
  explicit Config(std::string const& line);
  auto empty() const -> bool;
  bool separator = false; // whether there was a separator
  std::string name;       // the name of the entry
  std::string value;      // the value of the entry
}; // struct Config

// reads the next config from 'istr'
// * blank lines and lines beginning with '#' are ignored
// * the separator is '=':
//     the left is the name, the right is the value
// * if no '=' is in the line the first nonblank character is looked at:
//   - if it is a character, the whole line is the value
//   - if it is not a character, the first word is the name, the rest the value
auto operator>>(std::istream& istr, Config& config) -> std::istream&;

auto operator<<(std::ostream& ostr, Config const& config) -> std::ostream&;
