/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include <functional>
#include <vector>
#include <algorithm>
#include <memory>

//#define DEBUG_SIGNAL

/**
 ** Signal for void functions
 **/
template<typename>
class Signal;

/**
 ** A connection (pointer to the function)
 **/
using Connection = void const*;

/**
 ** Automatically disconnects all connections when being destructed
 **/
class AutoDisconnector {
  template<typename>
    friend class Signal;
  public:
  AutoDisconnector() = default;
  AutoDisconnector(AutoDisconnector const&) = delete;
  AutoDisconnector(AutoDisconnector&&)      = delete;
  auto operator=(AutoDisconnector const&)   = delete;
  auto operator=(AutoDisconnector&&)        = delete;

  virtual ~AutoDisconnector();
  private:
  template<typename... Args>
    void add(Connection connection, Signal<void(Args...)>& signal);
  template<typename... Args>
    void remove(Signal<void(Args...)> const& signal);

  using SignalPtr = void*; // only needed for comparison; there is not one signal type
  using Disconnector = std::function<void()>;
  std::vector<std::pair<SignalPtr, Disconnector>> disconnector_;
}; // class AutoDisconnector

/**
 ** Signal for void functions
 **/
template<typename... Args>
class Signal<void(Args...)> final {
  public:
    using Callback = std::function<void(Args...)>;
  public:

    Signal();
    Signal(Signal const&)                               = delete;
    Signal(Signal&& signal)         noexcept;
    auto operator=(Signal const&)            -> Signal& = delete;
    auto operator=(Signal&& signal) noexcept -> Signal&;

    ~Signal();

    // connect to a function
    template<typename U>
      auto connect(U fn)      -> Connection;
    template<typename U>
      auto connect_back(U fn) -> Connection;
    template<typename U>
      auto connect(U fn, AutoDisconnector& disconnector)      -> Connection;
    template<typename U>
      auto connect_back(U fn, AutoDisconnector& disconnector) -> Connection;
    // connect to a member
    template<typename T, typename U>
      auto connect(T& p, U fn)      -> Connection;
    template<typename T, typename U>
      auto connect_back(T& p, U fn) -> Connection;
    template<typename T, typename U>
      auto connect(T& p, U fn, AutoDisconnector& disconnector)      -> Connection;
    template<typename T, typename U>
      auto connect_back(T& p, U fn, AutoDisconnector& disconnector) -> Connection;

    void disconnect(Connection connection);
    void remove(AutoDisconnector const& auto_disconnector);

    void disconnect_all();

    void operator()(Args... args) const;

  private:
    std::vector<std::unique_ptr<Callback>> callbacks_;
    std::vector<std::unique_ptr<Callback>> callbacks_back_;
    std::vector<AutoDisconnector*> auto_disconnector_;
}; // class Signal<void(Args...)> final

// implementation


inline AutoDisconnector::~AutoDisconnector()
{
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") ~AutoDisconnector" << std::endl;
#endif
  std::for_each(disconnector_.begin(), disconnector_.end(), 
                [](auto& d) { d.second(); });
  // std::invoke(d) // C++17
}

template<typename... Args>
void AutoDisconnector::add(Connection const connection, Signal<void(Args...)>& signal)
{
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") AutoDisconnector::add Connection " << connection << ", Signal " << &signal << std::endl;
#endif
  disconnector_.emplace_back(std::make_pair(&signal,
                                            [this, connection, &signal]()
                                            {
                                            signal.disconnect(connection);
                                            signal.remove(*this);
                                            }));
}

template<typename... Args>
void AutoDisconnector::remove(Signal<void(Args...)> const& signal)
{
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") AutoDisconnector::remove(signal = " << &signal << ")" << std::endl;
#endif
  for (auto d = disconnector_.begin(); d != disconnector_.end(); ) {
    if (d->first == &signal) {
#ifdef DEBUG_SIGNAL
      cout << "#" << __LINE__ << '(' << this << ") AutoDisconnector::erase Connection " << d->first << std::endl;
#endif
      d = disconnector_.erase(d);
    } else {
      ++d;
    }
  }
}

template<typename... Args>
Signal<void(Args...)>::Signal() = default;

template<typename... Args>
Signal<void(Args...)>::Signal(Signal&&) noexcept = default;

template<typename... Args>
auto Signal<void(Args...)>::operator=(Signal&&) noexcept -> Signal<void(Args...)>& = default;

template<typename... Args>
Signal<void(Args...)>::~Signal()
{
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") ~Signal" << std::endl;
#endif
  disconnect_all();
}

// connect to a function
template<typename... Args>
template<typename U>
auto Signal<void(Args...)>::connect(U fn) -> Connection
{
  callbacks_.emplace_back(std::make_unique<Callback>([fn](auto&&... args)
                                                     { fn(std::forward<decltype(args)>(args)...); }
                                                    ));
  return callbacks_.back().get();
}

template<typename... Args>
template<typename U>
auto Signal<void(Args...)>::connect_back(U fn) -> Connection
{
  callbacks_back_.emplace_back(std::make_unique<Callback>([fn](auto&&... args)
                                                          { fn(std::forward<decltype(args)>(args)...); }
                                                         ));
  return callbacks_back_.back().get();
}

// connect to a function
template<typename... Args>
template<typename U>
auto Signal<void(Args...)>::connect(U fn, AutoDisconnector& disconnector) -> Connection
{
  auto const connection = connect(fn);
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") Signal::add Connection " << connection << std::endl;
#endif
  disconnector.add(connection, *this);
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") Signal::add AutoDisconnector " << &disconnector << std::endl;
#endif
  auto_disconnector_.push_back(&disconnector);
  return connection;
}

// connect to a function
template<typename... Args>
template<typename U>
auto Signal<void(Args...)>::connect_back(U fn, AutoDisconnector& disconnector) -> Connection
{
  auto const connection = connect_back(fn);
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") Signal::add Connection " << connection << std::endl;
#endif
  disconnector.add(connection, *this);
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") Signal::add AutoDisconnector " << &disconnector << std::endl;
#endif
  auto_disconnector_.push_back(&disconnector);
  return connection;
}

// connect to a member
template<typename... Args>
template<typename T, typename U>
auto Signal<void(Args...)>::connect(T& p, U fn) -> Connection
{
  callbacks_.emplace_back(std::make_unique<Callback>([&p, fn](auto&&... args)
                                                     { std::mem_fn(fn)(&p, std::forward<decltype(args)>(args)...); }
                                                    ));
  return callbacks_.back().get();
}

// connect to a member
template<typename... Args>
template<typename T, typename U>
auto Signal<void(Args...)>::connect_back(T& p, U fn) -> Connection
{
  callbacks_back_.emplace_back(std::make_unique<Callback>([&p, fn](auto&&... args)
                                                          { std::mem_fn(fn)(&p, std::forward<decltype(args)>(args)...); }
                                                         ));
  return callbacks_back_.back().get();
}

// connect to a member
template<typename... Args>
template<typename T, typename U>
auto Signal<void(Args...)>::connect(T& p, U fn, AutoDisconnector& disconnector) -> Connection
{
  auto const connection = connect(p, fn);
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") Signal::add Connection " << connection << std::endl;
#endif
  disconnector.add(connection, *this);
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") Signal::add AutoDisconnector " << &disconnector << std::endl;
#endif
  auto_disconnector_.push_back(&disconnector);
  return connection;
}

// connect to a member
template<typename... Args>
template<typename T, typename U>
auto Signal<void(Args...)>::connect_back(T& p, U fn, AutoDisconnector& disconnector) -> Connection
{
  auto const connection = connect_back(p, fn);
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") Signal::add Connection " << connection << std::endl;
#endif
  disconnector.add(connection, *this);
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") Signal::add AutoDisconnector " << &disconnector << std::endl;
#endif
  auto_disconnector_.push_back(&disconnector);
  return connection;
}

template<typename... Args>
void Signal<void(Args...)>::disconnect(Connection const connection)
{
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") Signal::erase Connection " << connection << std::endl;
#endif
  {
    auto p = std::remove_if(callbacks_.begin(), callbacks_.end(),
                            [connection](auto const& c)
                            { return (connection == c.get()); }
                           );
    callbacks_.erase(p, callbacks_.end());
  }
  {
    auto p = std::remove_if(callbacks_back_.begin(), callbacks_back_.end(),
                            [connection](auto const& c)
                            { return (connection == c.get()); }
                           );
    callbacks_back_.erase(p, callbacks_back_.end());
  }
  // ToDo: remove connection from AutoDisconnectors
}

template<typename... Args>
void Signal<void(Args...)>::remove(AutoDisconnector const& auto_disconnector)
{
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") Signal::remove AutoDisconnector " << &auto_disconnector << std::endl;
#endif
  auto p = std::remove_if(auto_disconnector_.begin(), auto_disconnector_.end(),
                          [&auto_disconnector](auto const& d)
                          { return (d == &auto_disconnector); }
                         );
  auto_disconnector_.erase(p, auto_disconnector_.end());
}

template<typename... Args>
void Signal<void(Args...)>::disconnect_all()
{
#ifdef DEBUG_SIGNAL
  cout << "#" << __LINE__ << '(' << this << ") Signal::disconnect_all" << std::endl;
#endif
  std::for_each(auto_disconnector_.begin(), auto_disconnector_.end(),
                [this](auto& d) { d->remove(*this); });
  auto_disconnector_.clear();
  callbacks_.clear();
  callbacks_back_.clear();
}

template<typename... Args>
void Signal<void(Args...)>::operator()(Args... args) const
{
#if 0
  // does not work
  for (auto const& c : callbacks_) {
    (*c)(std::forward<Args>(args)...);
  }
  for (auto const& c : callbacks_back_) {
    (*c)(std::forward<Args>(args)...);
  }
#else
  std::for_each(callbacks_.begin(), callbacks_.end(),
                [&](auto const& c)
                { (*c)(std::forward<Args>(args)...); }
                // { std::invoke(c, std::forward<Args>(args)...); } // C++17
               );
  std::for_each(callbacks_back_.begin(), callbacks_back_.end(),
                [&](auto const& c)
                { (*c)(std::forward<Args>(args)...); }
                // { std::invoke(c, std::forward<Args>(args)...); } // C++17
               );
#endif
}
