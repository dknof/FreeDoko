/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once // ./constants.h

// using the microsoft visual C++ compiler
#ifdef _MSC_VER
#define MS_VISUAL_C
#define WINDOWS
#endif

// C++ include

#include <typeinfo>
#include <iostream>
using std::ostream;
using std::istream;
using std::cout;
using std::clog;
using std::cerr;
using std::endl;
using std::flush;
#include <iomanip>
using std::setw;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include <sstream>
using std::stringstream;
using std::istringstream;
using std::ostringstream;
#include <algorithm>
#include <numeric>
using std::min;
using std::max;
using std::begin;
using std::end;

// for temporary output
#define CLOG cerr << __FILE__ << '#' << __LINE__ << "  "          // NOLINT(cppcoreguidelines-macro-usage)

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage,cppcoreguidelines-avoid-c-arrays,hicpp-avoid-c-arrays,modernize-avoid-c-arrays)
#define ENUM_WITH_LIST(EnumName, enum_name_list, first_value, type1, ...) \
  enum EnumName { type1 = first_value, __VA_ARGS__ }; \
  constexpr EnumName enum_name_list[] = { type1, __VA_ARGS__ }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage,cppcoreguidelines-avoid-c-arrays,hicpp-avoid-c-arrays,modernize-avoid-c-arrays)
#define ENUM_WITH_STATIC_LIST(EnumName, enum_name_list, first_value, type1, ...) \
  enum EnumName { type1 = first_value, __VA_ARGS__ }; \
  static constexpr EnumName enum_name_list[] = { type1, __VA_ARGS__ }


using std::pair;
#include <vector>
using std::vector;
#include <array>
using std::array;
#include <queue>
using std::queue;
#include <map>
using std::map;
#include <set>
using std::set;
#include <string>
using std::string;
using namespace std::literals::string_literals;
#include <memory>
using std::unique_ptr;
using std::make_unique;
#include <optional>
using std::optional;
#include <functional>
using std::function;

#include <filesystem>
using Path = std::filesystem::path;
using Directory = std::filesystem::path;

#include "utils/container_algorithm.h"
using namespace container_algorithm;


#include "class/signal.h"
#include "signals.h"


using Seed = std::uint_fast32_t; // same as in utils/random.h

// C functions
#include <cmath>
#include <climits>

#ifdef WINDOWS
#define USE_REGISTRY
#endif

#ifdef WINDOWS
#include "utils/windows.h"
#endif


#ifndef MS_VISUAL_C
#include <dirent.h>
#endif


// Whether GTKMM can be used.
// When not defined, FreeDoko should compile and run without any gtkmm library
// this makro shall be defined through the compiler (in the makefile)
//#define USE_UI_GTKMM

#define WITH_UI_INTERFACE
#define BUG_REPORT
#define BUG_REPORT_REPLAY
#define REFERENCES_REPLAY

#ifdef REST_SERVICE
#undef WITH_UI_INTERFACE
#undef WEBSOCKETS_SERVICE
#undef BUG_REPORT
#undef BUG_REPORT_REPLAY
#undef REFERENCES_REPLAY
#endif

#ifdef WEBSOCKETS_SERVICE
#undef WITH_UI_INTERFACE
#undef REST_SERVICE
#define BUG_REPORT
#undef BUG_REPORT_REPLAY
#undef REFERENCES_REPLAY
#define WITH_DOKOLOUNGE
#endif



// code that is outdated -- leave this macro undefined
#undef OUTDATED
// code, that is postponed -- leave this macro undefined
#undef POSTPONED

// code that is deprecated -- leave this macro defined
#define DEPRECATED

// code that is workaround -- leave this macro defined
#define WORKAROUND

// code that is for backward compability -- leave this macro defined
#define BACKWARD_COMPABILITY


#include "text/gpl.txt.h" // defines gpl_txt
auto constexpr contact = "freedoko@users.sourceforge.net";
auto constexpr program_name = "FreeDoko";


// In the release-version there are some elements hidden
//#define RELEASE
//#define VERSION_DESCRIPTION "preversion"


// whether to check the runtime
#if !defined(RELEASE) && !defined(USE_THREADS)
//#define CHECK_RUNTIME
#endif
#ifdef CHECK_RUNTIME
#include "runtime.h"
#endif


// some utils
#include "utils.h"
#include "misc/translation.h"


#ifndef WINDOWS
// PlaySound exists only under MS Windows
#undef USE_SOUND_PLAYSOUND
#endif


// whether an assertion shall create a segmentation fault (for a core dump)
// (define it in 'Makefile.local')
//#define ASSERTION_GENERATES_SEGFAULT
#ifdef RELEASE
#undef ASSERTION_GENERATES_SEGFAULT
#endif

// creates a segmentation fault
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define SEGFAULT if (true) { cerr << "Created segmentation fault:\n" \
  << __FILE__ << " # " << __LINE__ << endl; \
  std::abort(); } else (void)0
//(*reinterpret_cast<volatile int*>(NULL) = 0); } else (void)0

// debugging utils
#include "debug.h"
