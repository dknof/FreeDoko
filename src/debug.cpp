/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "debug.h"

#include <ctime>
#include "utils.h"

#include "ui/ui.h"

Debug debug; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
// Filter
// table: Zeichnen des Tisches
// gameplay: Spielverlauf mit ausgeben
// heuristics

// initialize the pointer to the debug function
DebugFunction debug_function = nullptr; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)


void report_error(string const& message)
{
#ifndef WITH_UI_INTERFACE
  throw message;
#endif
  if (::ui) {
    static bool recursion = false;
    if (recursion) {
      cerr << message << std::flush;
    } else {
      recursion = true;
      auto const backtrace = backtrace_string(2);
      ::ui->error(message, backtrace);
      recursion = false;
    }
  } else {
    cerr << message << std::flush;
  }
#ifdef ASSERTION_GENERATES_SEGFAULT
  SEGFAULT;
#endif
  std::exit(EXIT_FAILURE);
}

Debug::Debug() :
  ostr_(&cerr)
{
  add("");
  add("codeline");
}

Debug::~Debug() = default;

auto Debug::ostr() -> ostream&
{
  return *ostr_;
}

void Debug::set_ostr(ostream& ostr)
{
  ostr_ = &ostr;
}

void Debug::set_file(string const file)
{
  if (   file.empty()
      || file == "-"
      || file == "cerr")
    ostr_ = &cerr;
  else
    ostr_ = new ofstream(file);
}

void Debug::add(string const filter)
{
  filter_.insert(filter);
}

void Debug::remove(string const filter)
{
  filter_.erase(filter);
}

auto Debug::filter(string const filter) -> bool
{
  return filter_.count(filter);
}

auto Debug::operator()(string const filter) const -> bool
{
  return (filter.empty() || filter_.count(filter));
}

auto Debug::ostr(string const filter) -> Ostr
{
  if (filter.empty() || filter_.count(filter)) {
    if (ostr_)
      return Ostr(*ostr_);
    else
      return Ostr(cerr);
  } else {
    return Ostr(::null_ostr);
  }
}

auto Debug::ostr(string const filter_, string const file, long const line, string const function_name) -> Ostr
{
  auto ostr = this->ostr(filter_);
  if (&ostr.ostr_ == &::null_ostr)
    return ostr;
  if (filter("time"))
    ostr << Time::current() << ' ';
  if (filter("clock")) {
    auto const c = std::clock();
    ostr << Time(c / CLOCKS_PER_SEC, (c / (CLOCKS_PER_SEC / 1000) % 1000)) << ' ';
  }
  if (filter("codeline"))
    ostr << file << '#' << line << ' ';
  if (filter("function"))
    ostr << function_name << "() ";
  return ostr;
}

Debug::Ostr::Ostr(ostream& ostr) :
  ostr_(ostr)
{ }

Debug::Ostr::~Ostr()
{
  auto const text = str();
  if (text.empty())
    return ;
  ostr_ << text;
  if (text.back() != '\n' && text.back() != '\r')
    ostr_ << '\n';
}
