/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
#include <sstream>
using std::ostringstream;
#include <string>
using std::string;
#include <set>
using std::set;
#include <memory>
using std::unique_ptr;
using std::make_unique;

#include <cstdlib>
using std::set_terminate;

#include "utils/time.h"

void report_error(string const& message);
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define DEBUG_ASSERTION(predicate, text) \
  do {                                   \
    if (!(predicate)) {                  \
      ostringstream ostr;                \
      ostr << "ASSERT(file " << __FILE__ \
           << ", line " << __LINE__      \
           << "): " << text << endl;     \
      report_error(ostr.str());          \
      std::exit(EXIT_FAILURE);           \
    } \
  } while(false)

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define DEBUG_ASSERT(predicate) \
  DEBUG_ASSERTION(predicate, "failed")

using DebugFunction = void (*)(string const&);
extern DebugFunction debug_function; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

// Ausgabe der History einer ungefangenen Exception auf fs::error
struct UncaughtException
{
  UncaughtException(string const file,
                    int const line,
                    string const what) :
    file_(file), line_(line), what_(what)
  { }
  string const file_;
  int const line_;
  string const what_;
}; // struct UncaughtException

// class for writing debug informations
// Usage:
//   debug.add("filter name");
//   DEBUG("filter name") << "debug text\n";
// special filters:
//   "codeline": prepend the file and code line in front of the debug text (default: on)
//   "time": prepend the time (of the day) in front of the debug text (default: off)
//   "clock": prepend the processor time in front of the debug text (default: off)
class Debug {
public:
  class Ostr;
public:
  Debug();
  Debug(Debug const&)          = delete;
  Debug(Debug&&)               = delete;
  auto operator=(Debug const&) = delete;
  auto operator=(Debug&&)      = delete;
  ~Debug();

  auto ostr() -> ostream&;
  void set_ostr(ostream& ostr);
  void set_file(string file);
  void add(string filter);
  void remove(string filter);
  auto filter(string filter) -> bool;

  auto operator()(string filter) const -> bool;
  auto ostr(string filter) -> Ostr;
  auto ostr(string filter, string file, long line, string function_name) -> Ostr;

private:
  set<string> filter_;
  ostream* ostr_ = nullptr;
}; // class Debug

class Debug::Ostr final : public std::ostringstream {
public:
  explicit Ostr(ostream& ostr);
  Ostr(Ostr const&);
  ~Ostr() final;
private:
  ostream& ostr_;
  friend class Debug;
};

extern Debug debug; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
#define DEBUG(f) if (!::debug(f)) ; else ::debug.ostr(f, __FILE__, __LINE__, __func__) // NOLINT(hicpp-no-array-decay)
