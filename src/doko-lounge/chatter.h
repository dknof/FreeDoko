/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "reaktion.h"
class Player;

namespace DokoLounge {

class Spieltisch;

class Chatter {
public:
  Chatter(Spieltisch& spieltisch, string spielername);
  Chatter() = delete;
  Chatter(Chatter const&)        = delete;
  auto operator=(Chatter const&) = delete;

  void setze_geburtstagskinder(std::set<string> const& geburtstagskinder);

  void lade_reaktionen(Path const& datei, string const& spielername);

  void sage_mit_prefix(string const& prefix, string const& kennwort);
  void sage(string const& kennwort);
  void sage(string const& kennwort, string const& spieler);
  auto text(string const& kennwort)                        const -> string;
  auto text(string const& kennwort, string const& spieler) const -> string;
  auto kenne_kennwort(string const& kennwort) const -> bool;

  void sage_eintrag(string const& kennwort, string const& spieler);
  void sage_zufaellig(vector<string> const& text);
  void sage_zufaellig(vector<string> const& text1, vector<string> const& text2);
  void sage_zufaellig(vector<string> const& text1, vector<string> const& text2, vector<string> const& text3);

  void reagiere_auf_chat(string const& name, string text);
  void reagiere_auf_spielerliste(vector<string> const& namen);

  void spiel(Player const& ai);
  void blatt(Player const& ai);
  void teaminfo(Player const& ai);
  void karteninfo(Player const& ai);

private:
  auto hallo_ergaenzung() const -> vector<string>;

private:
  Spieltisch* spieltisch;
  set<string> spieler_begruest;

  vector<Reaktion> reaktionen;
};

} // namespace DokoLounge

#endif
