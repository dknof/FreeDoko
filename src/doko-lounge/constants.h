/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once // doko-lounge/constants.h

#ifdef WINDOWS
#include <winsock2.h>
#endif

#include "../constants.h"

#ifdef WEBSOCKETS_SERVICE

#include <set>
using std::set;

using std::string_view;
#include "string_lc.h"

#include <chrono>
using Zeitpunkt = std::chrono::time_point<std::chrono::system_clock>;

using URI = string;
struct Offline { };

extern set<string> auto_bug_report_keys;
extern set<string> chat_keys;

namespace DokoLounge {
using Spielername   = string_lc;
using Passwort      = string;
using Punkte        = int;
using Knecht        = string;
using Trumpf        = string;
using Vorbehalt     = string;
using Kartennummer  = int;
using Tischnummer   = unsigned;
using Spielernummer = unsigned;
using Blatt         = vector<Kartennummer>;
}

struct lws_protocols;
struct lws;
struct lws_context;

namespace DokoLounge {
namespace WebSockets {
using Protocol = struct lws_protocols;
using Protocols = vector<Protocol>;
using Instance = struct lws*;
using Context = struct lws_context*;

extern Protocols const protocols;

extern URI const testserver;
}
}
#endif
