/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "datum.h"

Zeit::Zeit(int const stunde_, int const minute_) :
  stunde(stunde_), minute(minute_)
{}


auto gestern() -> Datum const&
{
  auto jetzt_utc = time(nullptr) - 24 * 60 * 60;
  static Datum* jetzt; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
  jetzt = localtime(&jetzt_utc);
  return *jetzt;
}


auto heute() -> Datum const&
{
  auto jetzt_utc = time(nullptr);
  static Datum* jetzt; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
  jetzt = localtime(&jetzt_utc);
  return *jetzt;
}


auto morgen() -> Datum const&
{
  auto jetzt_utc = time(nullptr) + 24 * 60 * 60;
  static Datum* jetzt; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
  jetzt = localtime(&jetzt_utc);
  return *jetzt;
}


auto aktuelle_stunde() -> unsigned
{
  auto jetzt_utc = time(nullptr);
  return localtime(&jetzt_utc)->tm_hour;
}


auto ist_weihnachtszeit(Datum const& datum) -> bool
{
  return (   (datum.tm_mon == 10 && datum.tm_mday >= 27)
          || (datum.tm_mon == 11 && datum.tm_mday <= 23));
}

auto ist_advent_1(Datum const& datum) -> bool
{
  return (   datum.tm_wday == 0
          && (   (datum.tm_mon == 10 && datum.tm_mday >= 27)
              || (datum.tm_mon == 11 && datum.tm_mday <=  3)));
}


auto ist_advent_2(Datum const& datum) -> bool
{
  return (   datum.tm_wday ==  0
          && datum.tm_mon  == 11
          && datum.tm_mday >=  4
          && datum.tm_mday <= 10);
}


auto ist_advent_3(Datum const& datum) -> bool
{
  return (   datum.tm_wday ==  0
          && datum.tm_mon  == 11
          && datum.tm_mday >= 11
          && datum.tm_mday <= 17);
}


auto ist_advent_4(Datum const& datum) -> bool
{
  return (   datum.tm_wday ==  0
          && datum.tm_mon  == 11
          && datum.tm_mday >= 18
          && datum.tm_mday <= 24);
}


auto ist_nikolaus(Datum const& datum) -> bool
{
  return (   datum.tm_mon  == 11
          && datum.tm_mday ==  6);
}


auto ist_heiligabend(Datum const& datum) -> bool
{
  return (   datum.tm_mon  == 11
          && datum.tm_mday == 24);
}


auto ist_weihnachten(Datum const& datum) -> bool
{
  return (   datum.tm_mon  == 11
          && (   datum.tm_mday == 25
              || datum.tm_mday == 26));
}


auto ist_silvester(Datum const& datum) -> bool
{
  return (   datum.tm_mon  == 11
          && datum.tm_mday == 31);
}


auto ist_neujahr(Datum const& datum) -> bool
{
  return (   datum.tm_mon  == 0
          && datum.tm_mday == 1);
}


auto jetzt() -> Zeit
{
  auto jetzt_utc = time(nullptr);
  auto const jetzt = localtime(&jetzt_utc);
  return {jetzt->tm_hour, jetzt->tm_min};
}


auto operator==(Zeit const& lhs, Zeit const& rhs) -> bool
{
  return (   lhs.stunde == rhs.stunde
          && lhs.minute == rhs.minute);
}


auto operator!=(Zeit const& lhs, Zeit const& rhs) -> bool
{
  return (   lhs.stunde != rhs.stunde
          || lhs.minute != rhs.minute);
}


auto operator< (Zeit const& lhs, Zeit const& rhs) -> bool
{
  return (   lhs.stunde < rhs.stunde
          || (   lhs.stunde == rhs.stunde
              && lhs.minute <  rhs.minute) );
}


auto operator<=(Zeit const& lhs, Zeit const& rhs) -> bool
{
  return (   lhs.stunde < rhs.stunde
          || (   lhs.stunde == rhs.stunde
              && lhs.minute <= rhs.minute) );
}


auto operator> (Zeit const& lhs, Zeit const& rhs) -> bool
{
  return (   lhs.stunde > rhs.stunde
          || (   lhs.stunde == rhs.stunde
              && lhs.minute >  rhs.minute) );
}


auto operator>=(Zeit const& lhs, Zeit const& rhs) -> bool
{
  return (   lhs.stunde > rhs.stunde
          || (   lhs.stunde == rhs.stunde
              && lhs.minute >= rhs.minute) );
}


auto to_string(Wochentag const wochentag) -> string
{
  switch (wochentag) {
  case Wochentag::sonntag:    return "Sonntag";
  case Wochentag::montag:     return "Montag";
  case Wochentag::dienstag:   return "Dienstag";
  case Wochentag::mittwoch:   return "Mittwoch";
  case Wochentag::donnerstag: return "Donnerstag";
  case Wochentag::freitag:    return "Freitag";
  case Wochentag::samstag:    return "Samstag";
  }
  return {};
}

auto ist_wochentag(string const& text) -> bool
{
  for (auto const w : wochentage) {
    if (to_string(w) == text)
      return true;
  }
  return false;
}


auto to_wochentag(string const& text) -> Wochentag
{
  for (auto const w : wochentage) {
    if (to_string(w) == text)
      return w;
  }
  return Wochentag::sonntag;
}


auto to_wochentag(Datum const& datum) -> Wochentag
{
  switch (datum.tm_wday) {
  case 0: return Wochentag::sonntag;
  case 1: return Wochentag::montag;
  case 2: return Wochentag::dienstag;
  case 3: return Wochentag::mittwoch;
  case 4: return Wochentag::donnerstag;
  case 5: return Wochentag::freitag;
  case 6: return Wochentag::samstag;
  }
  return Wochentag::sonntag;
}


auto gestern_string() -> string
{
  auto const datum = gestern();
  return (  (datum.tm_mday < 10 ? "0"s : ""s) + std::to_string(datum.tm_mday)  + "."
          + (datum.tm_mon  <  9 ? "0"s : ""s) + std::to_string(datum.tm_mon+1) + "."
          + std::to_string(datum.tm_year + 1900));
}

auto heute_string() -> string
{
  auto const datum = heute();
  return (  (datum.tm_mday < 10 ? "0"s : ""s) + std::to_string(datum.tm_mday)  + "."
          + (datum.tm_mon  <  9 ? "0"s : ""s) + std::to_string(datum.tm_mon+1) + "."
          + std::to_string(datum.tm_year + 1900));
}

auto morgen_string() -> string
{
  auto const datum = morgen();
  return (  (datum.tm_mday < 10 ? "0"s : ""s) + std::to_string(datum.tm_mday)  + "."
          + (datum.tm_mon  <  9 ? "0"s : ""s) + std::to_string(datum.tm_mon+1) + "."
          + std::to_string(datum.tm_year + 1900));
}

auto operator<<(ostream& ostr, Datum const& datum) -> ostream&
{
  if (datum.tm_mday < 10)
    ostr << 0;
  ostr << datum.tm_mday;
  if (datum.tm_mon < 10)
    ostr << 0;
  ostr << datum.tm_mon;
  ostr << (datum.tm_year + 1900);
  return ostr;
}

auto operator<<(ostream& ostr, Zeit const& zeit)   -> ostream&
{
  ostr << zeit.stunde << ':';
  if (zeit.minute < 10)
    ostr << 0;
  ostr << zeit.minute;
  return ostr;
}
