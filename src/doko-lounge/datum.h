/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include <chrono>

// ToDo: auf C++20 date-Klassen umstellen
//
using Datum = struct tm;

enum class Wochentag {
  sonntag, montag, dienstag, mittwoch, donnerstag, freitag, samstag
};
constexpr array<Wochentag, 7> wochentage = {Wochentag::sonntag, Wochentag::montag, Wochentag::dienstag, Wochentag::mittwoch, Wochentag::donnerstag, Wochentag::freitag, Wochentag::samstag};

struct Zeit {
  Zeit(int stunde, int minute);
  Zeit() = delete;
  Zeit(Zeit const&) = default;
  auto operator=(Zeit const&) -> Zeit& = default;

  int stunde;
  int minute;
};

auto gestern() -> Datum const&;
auto heute()   -> Datum const&;
auto morgen()  -> Datum const&;
auto aktuelle_stunde() -> unsigned;

auto ist_weihnachtszeit(Datum const& datum) -> bool;
auto ist_advent_1(Datum const& datum) -> bool;
auto ist_advent_2(Datum const& datum) -> bool;
auto ist_advent_3(Datum const& datum) -> bool;
auto ist_advent_4(Datum const& datum) -> bool;
auto ist_nikolaus(Datum const& datum) -> bool;
auto ist_heiligabend(Datum const& datum) -> bool;
auto ist_weihnachten(Datum const& datum) -> bool;
auto ist_silvester(Datum const& datum)   -> bool;
auto ist_neujahr(Datum const& datum)     -> bool;
auto ist(Wochentag wochentag)     -> bool;

auto jetzt() -> Zeit;

auto operator==(Zeit const& lhs, Zeit const& rhs) -> bool;
auto operator!=(Zeit const& lhs, Zeit const& rhs) -> bool;
auto operator< (Zeit const& lhs, Zeit const& rhs) -> bool;
auto operator<=(Zeit const& lhs, Zeit const& rhs) -> bool;
auto operator> (Zeit const& lhs, Zeit const& rhs) -> bool;
auto operator>=(Zeit const& lhs, Zeit const& rhs) -> bool;

auto to_string(Wochentag wochentag) -> string;
auto ist_wochentag(string const& text) -> bool;
auto to_wochentag(string const& text) -> Wochentag;
auto to_wochentag(Datum const& datum) -> Wochentag;
auto gestern_string() -> string;
auto heute_string()   -> string;
auto morgen_string()  -> string;

auto operator<<(ostream& ostr, Datum const& datum) -> ostream&;
auto operator<<(ostream& ostr, Zeit const& zeit)   -> ostream&;
