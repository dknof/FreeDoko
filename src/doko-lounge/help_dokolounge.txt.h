static auto const help_dokolounge = "\
FreeDoko -- Version " + to_string(*::version) + "\n\
Dummy für die Doko-Lounge\n\
\n\
== Argumente\n\
-h -?\n\
--help\n\
	Gibt diese Hilfe aus, beendet sich anschließend\n\
\n\
-v\n\
--version\n\
	Gibt die Version aus, beendet sich anschließend\n\
\n\
-L\n\
--license\n\
	Gibt Informationen über die Lizenz (GPL) aus, beendet sich anschließend\n\
\n\
--defines\n\
	Gibt aus, mit welchen Flags kompiliert wurde, beendet sich anschließend\n\
\n\
--po\n\
	Gibt Informationen zur Übersetzung aus, beendet sich anschließend\n\
\n\
--debug [string]\n\
	Schaltet Debug-Informationen an, z.B.\n\
	\"heuristics\": Entscheidungslogik für alle Heuristiken\n\
	Name einer Heuristik: Entscheidungslogik für die spezielle Heuristik\n\
	\"solo decision rationale\": Entscheidungslogik für die Soloentscheidung\n\
	\"Karte\":    Gewählte Karte\n\
	\"Meldung\":  Gewählte Meldung\n\
	\"Ansage\":   Gewählte Ansage\n\
	\"gameplay\": Spielverlauf (Kartenspiel, Ansage, Stiche, Zusammenfassung)\n\
	\"write\":    Ausgabe der Nachrichten an den Server\n\
	\"refresh\":  Ausgabe der Gründe für eine Refresh-Anfrage\n\
	\n\
	\"codeline\": Zusätzliche Informationen zur Code-Postition (Vorgabe: an)\n\
	\"time\":     Zusätzliche Informationen zum Zeitpunkt (Vorgabe: aus)\n\
	\"clock\":    Zusätzliche Informationen zur Zeitinformation (Vorgabe: aus)\n\
\n\
--card-play-delay [ms]\n\
	Gibt an, wie viele Millisekunden vor dem Kartenausspielen gewartet werden soll.\n\
	Vorgabe: 500 ms\n\
\n\
--speech-delay [ms]\n\
	Gibt an, wie viele Millisekunden nach einer Sprachausgabe gewartet werden soll.\n\
        Dieser Wert greift nur, wenn die Dauer der Sprachausgabe nicht aus der Datei ermittelt werden kann.\n\
	Vorgabe: 2000 ms\n\
\n\
--tisch [Tischnummer]\n\
	Gibt den Tisch in der Doko-Lounge an, auf dem gespielt werden soll.\n\
	Pflichtargument\n\
	Vorgabe: 1\n\
\n\
[uri]\n\
	Angabe der URI der Lounge\n\
	Beispiel: wss://doko-lounge.selfhost.eu:443/lounge/\n\
\n\
\n\
Anschließend folgt weiteres Argument das angibt, wie die Verbindung erfolgt:\n\
\n\
Login:Passwort\n\
	Verbindet sich mit der Doko-Lounge unter dem angegebenen Login \n\
	und Passwort\n\
\n\
stdin\n\
	Verbindet sich nicht mit der Doko-Lounge sondern liest den Netzverkehr \n\
        aus der Standardeingabe. Hilfreich für Debugging.\n\
\n\
file:Datei\n\
	Verbindet sich nicht mit der Doko-Lounge sondern liest den Netzverkehr \n\
        aus der Datei „Datei“. Hilfreich für Debugging.\n\
\n\
\n\
== Debug-Dateien\n\
\n\
	Für jeden Spieler (z.B. Kai) wird eine Log-Datei (Name in_Kai.log \n\
	entsprechend dem Spielernamen) erstellt. Diese enthält eine gefiltete \n\
	Auswahl der Nachrichten vom Server. \n\
	Die Datei kann zu Debugging-Zwecken mit FreeDoko file:in_Kai.log \n\
	eingelesen werden.\n\
\n\
\n\
== Beispiele\n\
\n\
FreeDoko --tisch 1 Kai:ganzgeheim\n\
	Verbindet FreeDoko mit doko-lounge.selfhost.eu:1502.\n\
	Der Login „Kai“ muss mit dem angegebenen Passwort existierten.\n\
	Kai setzt sich an Tisch 1 und spielt bis er rausgeworfen oder \n\
	Zuschauer wird.\n\
\n\
FreeDoko --host localhost --port 1234 --tisch 4 Kai:ganzgeheim\n\
	Verbindet FreeDoko mit localhost:1234.\n\
	Der Login „Kai“ muss mit dem angegebenen Passwort existierten.\n\
	Kai setzt sich an Tisch 4 und spielt bis er rausgeworfen oder \n\
	Zuschauer wird.\n\
\n\
\n\
== Tischchat\n\
\n\
Im Tischchat kann man mit „dummies raus“ alle FreeDoko-Dummies und mit „FreeDoko_1-1 raus”\n\
einen einzelner FreeDoko-Dummie „FreeDoko_1-1“ entfernen.\n\
\n\
Sitzt man an einem Tisch nur mit FreeDoko-Dummies, können weitere Informationen von der KI erfragt werden:\n\
\n\
Status\n\
	Die Dummies schreiben „ihren“ Spielstatus in den Chat.\n\
\n\
?\n\
	Die Dummies schreiben die Entscheidungslogik zur letzten von ihnen gespielten Karte in den Chat.\n\
\n\
\n\
== Sonstiges\n\
\n\
Sendet der Server <<offline>><</offline>> beendet sich FreeDoko.\n\
\n\
\n\
Fehler bitte an freedoko@users.sourceforge.net melden.\n\
";
