/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "client.h"

#include "../versions.h"

#include "../options.h"
#include "../utils/string.h"
#include "../utils/file.h"
#include "../utils/random.h"

#include "../misc/preferences.h"

#include <future>
#include <thread>
using namespace std::literals::chrono_literals;

#include <stdlib.h>

namespace DokoLounge::WebSockets {
Protocols const protocols = Protocols({
                                      {"Doko-Lounge", connection_callback, sizeof(Connection*), 0, 0, nullptr, 0},
                                      {nullptr, nullptr, 0, 0, 0, nullptr, 0} // terminator
                                      });
} // namespace DokoLounge::WebSockets

namespace {
void load_dokolounge_options_file();
void parse_dokolounge_option(GetOpt::Option option);
auto verarbeite_stdin_fuer_client()                   -> int;
auto verarbeite_datei_fuer_client(string const& pfad) -> int;
void beende_nach_inaktivitaet(Zeitpunkt* const zeitpunkt, std::chrono::seconds dauer);

const vector<GetOpt::Syntax> option_syntax = {
  {"help",             '?',  GetOpt::Syntax::BOOL},
  {"hilfe",            'h',  GetOpt::Syntax::BOOL},
  {"version",          'v',  GetOpt::Syntax::BOOL},
  {"license",          'L',  GetOpt::Syntax::BOOL},
  {"defines",          '\0', GetOpt::Syntax::BOOL},
  {"directories",      '\0', GetOpt::Syntax::BOOL},
  {"po",               '\0', GetOpt::Syntax::BOOL},
  {"debug",            '\0', GetOpt::Syntax::BSTRING},
  {"tisch",            '\0', GetOpt::Syntax::UNSIGNED},
  {"timeout",          '\0', GetOpt::Syntax::UNSIGNED},
  {"speech-delay",     '\0', GetOpt::Syntax::UNSIGNED},
  {"card-play-delay",  '\0', GetOpt::Syntax::UNSIGNED},
  {"auto-bug-report",  '\0', GetOpt::Syntax::BSTRING},
  {"chat",             '\0', GetOpt::Syntax::BSTRING},
};

//URI uri = "wss://doko-lounge.selfhost.eu:443/lounge/"; // offizielle Lounge
URI uri = "ws://localhost:1401"; // lokal
string login;
string passwort;
unsigned tischnummer = 0;
unsigned timeout_sekunden = 60;
} // namespace

set<string> auto_bug_report_keys;
set<string> chat_keys;

int
main(int argc, char* argv[])
{
  argv0 = argv[0];
  File::initialize_executable_directory(argv0);

  init_before_option_parsing();
  load_dokolounge_options_file();
  ::preferences.set(Preferences::Type::automatic_savings,         false);
  ::preferences.set(Preferences::Type::additional_party_settings, true);
  ::preferences.set(Preferences::Type::announcement_window_close_delay, 2 * 1000);
  ::preferences.set(Preferences::Type::card_play_delay, 500);

  // parse the options
  GetOpt::Option option;
  while ((option = GetOpt::getopt(argc, argv, option_syntax))) {
    parse_dokolounge_option(option);
  }

  if (login == "stdin")
    return verarbeite_stdin_fuer_client();
  if (login == "file")
    return verarbeite_datei_fuer_client(passwort);

  struct lws_context_creation_info info;
  memset(&info, 0, sizeof(info));

  info.port = CONTEXT_PORT_NO_LISTEN;
  info.protocols = DokoLounge::WebSockets::protocols.data();
  info.options = LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;
  info.gid = -1;
  info.uid = -1;

  //lws_set_log_level(1023, 0);
  auto context = lws_create_context(&info);

  unique_ptr<DokoLounge::WebSockets::Client> client;
  client = make_unique<DokoLounge::WebSockets::Client>(context, uri, login, passwort, tischnummer);

  if (!client) {
    cerr << "Kein Client erstellt.\n";
    return EXIT_FAILURE;
  }

  if (client && tischnummer == 0) {
    cerr << "Client, aber keine Tischnummer angegeben.\n";
    return EXIT_FAILURE;
  }

  Zeitpunkt letzte_aktivitaet = std::chrono::system_clock::now();
  auto const f = std::async(std::launch::async, beende_nach_inaktivitaet, &letzte_aktivitaet, std::chrono::seconds(timeout_sekunden));

  try {
    auto zuletzt_menschen_gesehen = std::chrono::system_clock::now();
    auto const zeitverschiebung = ::random_value(1000) * 10ms;
    while (client->wsi_ != nullptr) {
      letzte_aktivitaet = std::chrono::system_clock::now();
      DEBUG("main") << "Rufe lws_service auf\n";
      lws_service(context, /* timeout_ms = */ 250);
      DEBUG("main") << "Verarbeite Daten\n";
      if (client->letzte_aktivitaet + std::chrono::seconds(timeout_sekunden) <  std::chrono::system_clock::now()) {
          auto tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        client->write_log("Letzte Netzwerkaktivität vor ", timeout_sekunden, " Sekunden, um ", std::put_time(std::localtime(&tt), "%X"), ".\n");
        client->write_log_current_time();
        cout << client->login << ": " << timeout_sekunden << " Sekunden ohne Aktivität aus dem Netzwerk, beende FreeDoko-Spieler.\n";
        throw Offline();
      }
      if (client) {
        if (!client->parse_input())
          break;
        if (client->tisch && client->tisch->zaehle_menschen()) {
          zuletzt_menschen_gesehen = std::chrono::system_clock::now();
        } else if (zuletzt_menschen_gesehen < std::chrono::system_clock::now() - std::chrono::seconds(timeout_sekunden) - zeitverschiebung) {
          auto tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
          cout << std::put_time(std::localtime(&tt), "%X") << " Uhr: ";
          tt = std::chrono::system_clock::to_time_t(zuletzt_menschen_gesehen);

          if (client->tisch) {
            client->write_log("Zuletzt einen Menschen um ", std::put_time(std::localtime(&tt), "%X"), " Uhr gesehen.");
            client->write_log("Verlasse den Tisch ", client->tischnummer, ".");
          } else {
            client->write_log("Warte seit ", std::put_time(std::localtime(&tt), "%X"), " erfolglos auf Beitritt zum Tisch ", client->tischnummer, ".");
          }
          client->write_log_current_time();
          cout << client->login << ": Zuletzt einen Menschen um " << std::put_time(std::localtime(&tt), "%X") << " Uhr gesehen. ";
          if (client->tisch)
            cout << client->login << ": " << "Verlasse den Tisch " << client->tischnummer << ".\n";
          else
            cout << client->login << ": " << "Bin gar nicht zum Tisch " << client->tischnummer << " gekommen.\n";
          throw Offline();
        }
      }
      DEBUG("main") << "Warte 10 msec\n";
      std::this_thread::sleep_for(10ms);
    }
  } catch (Offline const&) {
    if (!client->read_buffer().empty()) {
      client->write_log("== read buffer ==");
      client->write_log(client->read_buffer());
    }
  } catch (string const& text) {
    cerr << "Fehler: " << text << '\n';
    cerr << "Beende Programm\n";
  }

  cout << "Beende WebSocket-Service für " << uri << "." << endl;
  lws_context_destroy(context);

  std::exit(EXIT_SUCCESS); // Damit werden alle Threads beendet
}

namespace {

void load_dokolounge_options_file()
{
  auto istr = std::ifstream(File::executable_directory / "options.Doko-Lounge");
  GetOpt::Option option;

  while (   istr.good()
         && (option = GetOpt::getopt(istr, option_syntax))) {
    if (option)
      parse_dokolounge_option(option);
  }
}


void parse_dokolounge_option(GetOpt::Option option) {
  if (option.fail()) {
    cout << usage_string(argv0, option);
    std::exit(EXIT_FAILURE);
  } // if (option.fail())

  if (   option.name() == "help"
      || option.name() == "hilfe") {
#include "help_dokolounge.txt.h"
    cout << help_dokolounge;
    std::exit(EXIT_SUCCESS);
  } else if (option.name() == "version") {
    cout << version_string();
    std::exit(EXIT_SUCCESS);
  } else if (option.name() == "license") {
    cout << license_string();
    std::exit(EXIT_SUCCESS);
  } else if (option.name() == "defines") {
    cout << defines_string();
    std::exit(EXIT_SUCCESS);
  } else if (option.name() == "po") {
    cout << _("");
    std::exit(EXIT_SUCCESS);
  } else if (option.name() == "debug") {
    add_debug(option.value_string());
  } else if (option.name() == "tisch") {
    tischnummer = option.value(GetOpt::Option::UNSIGNED);
  } else if (option.name() == "timeout") {
    timeout_sekunden = option.value(GetOpt::Option::UNSIGNED);
  } else if (option.name() == "speech-delay") {
    ::preferences.set(Preferences::Type::announcement_window_close_delay, option.value(GetOpt::Option::UNSIGNED));
  } else if (option.name() == "card-play-delay") {
    ::preferences.set(Preferences::Type::card_play_delay, option.value(GetOpt::Option::UNSIGNED));
  } else if (option.name() == "auto-bug-report") {
    auto_bug_report_keys.insert(option.value_string());
  } else if (option.name() == "chat") {
    if (option.value_string() == "*" || option.value_string() == "+" || option.value_string() == "++") {
      chat_keys.insert("Hallo");
      chat_keys.insert("gw");         // Glückwunsch
      chat_keys.insert("dp");         // Dankeschön
      chat_keys.insert("Fuchs");      // Fuchs gefangen
      chat_keys.insert("Doppelkopf"); // 40+ Punkte erhalten
      chat_keys.insert("Offline");    // Dankeschön
      chat_keys.insert("Witz");       // Dankeschön
    } else {
      chat_keys.insert(option.value_string());
    }
  } else if (option.name().empty()) {
    if (option.value_string().substr(0, 6) == "wss://") {
      uri = option.value_string();
    } else if (option.value_string().substr(0, 5) == "ws://") {
      uri = option.value_string();
    } else if (login == "file") {
      passwort = option.value_string();
    } else {
      login = option.value_string();
      //login = "FreeDoko#Jessika";
      auto const p = login.find(":");
      if (p != string::npos) {
        passwort = login.substr(p + 1);
        login.erase(p);
      }
      if (p == string::npos
          && (   std::filesystem::is_regular_file(login)
              || login.find('/')  != string::npos
              || login.find('\\') != string::npos)) {
        passwort = login;
        login = "file";
      }
    }
  }

}


auto verarbeite_stdin_fuer_client() -> int
{
  using std::cin;
  auto client = DokoLounge::WebSockets::Client("stdin", "", 0);
  string text;
  while (cin.good() && !cin.eof()) {
    getline(cin, text);
    client.receive(text + (text.empty() || text.back() != '>' ? "\n" : ""));
    client.parse_input();
  }
  if (cin.eof())
    cout << "Ende des Eingabestroms erreicht\n";
  return cin.fail() ? EXIT_FAILURE : EXIT_SUCCESS;
}


auto verarbeite_datei_fuer_client(string const& pfad) -> int
{
  auto istr = ifstream(pfad);
  if (istr.fail()) {
    cerr << "Fehler beim Öffnen der Datei „" << pfad << "“.\n";
    return EXIT_FAILURE;
  }
  auto client = DokoLounge::WebSockets::Client("file", pfad, 0);
  ::debug.add("ohne Pause");
  string text;
  while (istr.good() && !istr.eof()) {
    getline(istr, text);
    client.receive(text + (text.empty() || text.back() != '>' ? "\n" : ""));
    if (!client.parse_input()) {
      cout << "Client vorzeitig beendet\n";
      break;
    }
  }
  if (istr.eof())
    cout << "Ende der Datei erreicht\n";
  return istr.fail() ? EXIT_FAILURE : EXIT_SUCCESS;
}


void beende_nach_inaktivitaet(Zeitpunkt* const zeitpunkt, std::chrono::seconds const dauer)
  // zeitpunkt muss ein Zeiger sein: const& wird wohl zu const optimiert, also wird nicht der aktuelle Wert genommen. & funktioniert nicht.
{
  while (*zeitpunkt + dauer >  std::chrono::system_clock::now()) {
    std::this_thread::sleep_for(1s);
  }
  cout << dauer.count() << " Sekunden ohne Aktivität, beende FreeDoko.\n";
  std::abort();
}

} // namespace

#endif
