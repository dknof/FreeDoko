/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "random_text.h"

#include <random>

RandomText::RandomText() = default;

RandomText::~RandomText() = default;

void RandomText::clear()
{
  text1.clear();
  text2.clear();
  propability_sum = 0;
}


auto RandomText::all_text() -> string
{
  string text;
  for (auto const& t : text1) {
    text += t.first + "\n";
  }
  for (auto const& t : text2) {
    text += t + "\n";
  }
  return text;
}


void RandomText::add(string const& text, Propability const p)
{
  if (p <= 0) {
    cerr << "RandomText::add(" << text << ", " << p << "): Propability is less then 0.\n";
    return ;
  }
  text1.emplace_back(text, p);
  propability_sum += p;
}


void RandomText::add(string const& text)
{
  text2.emplace_back(text);
}


void RandomText::add_combinations(vector<string> const& text1, vector<string> const& text2)
{
  for (auto const& t1 : text1)
    for (auto const& t2 : text2)
      add(t1 + t2);
}


void RandomText::add_combinations(vector<string> const& text1, vector<string> const& text2, vector<string> const& text3)
{
  for (auto const& t1 : text1)
    for (auto const& t2 : text2)
      for (auto const& t3 : text3)
        add(t1 + t2 + t3);
}


auto RandomText::get() const -> string
{
  if (text1.empty() && text2.empty())
    return {};
  static std::random_device rd;
  static auto gen = std::mt19937(rd());
  auto dis = std::uniform_real_distribution<>(0, max(propability_sum, Propability(1)));
  auto p = dis(gen);

  if (p < propability_sum) {
    for (auto const& t : text1) {
      if (t.second >= p)
        return t.first;
      p -= t.second;
    }
  }
  return text2.at(std::uniform_int_distribution<>(0, text2.size() - 1)(gen));
}


auto operator<<(ostream& ostr, RandomText const& random_text) -> ostream&
{
  for (auto const& t : random_text.text1)
    ostr << '[' << t.second << "] " << t.first << '\n';
  for (auto const& t : random_text.text2)
    ostr << t << '\n';
  return ostr;
}

#endif
