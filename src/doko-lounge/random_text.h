/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

class RandomText {
public:
  using Propability = double;
  friend auto operator<<(ostream& ostr, RandomText const& text) -> ostream&;

public:
  RandomText();
  RandomText(RandomText const&)     = delete;
  auto operator=(RandomText const&) = delete;

  ~RandomText();

  void clear();
  auto all_text() -> string;

  void add(string const& text, Propability p);
  void add(string const& text);
  void add_combinations(vector<vector<string>> const& text);
  void add_combinations(vector<string> const& text1, vector<string> const& text2);
  void add_combinations(vector<string> const& text1, vector<string> const& text2, vector<string> const& text3);

  auto get() const -> string;

private:
  vector<std::pair<string, Propability>> text1;
  vector<string> text2; // Alle Elemente hierdrin werden mit gleicher Wahrscheinlichkeit genommen
  Propability propability_sum = 0;
};

auto operator<<(ostream& ostr, RandomText const& random_text) -> ostream&;

#endif
