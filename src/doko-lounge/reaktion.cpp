/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "reaktion.h"
#include "random_text.h"
#include "../utils/string.h"

#include <random>

namespace {
  std::random_device rd;
  auto random_generator = std::mt19937(rd());
} // namespace

namespace DokoLounge {

Reaktion::Eintrag::Eintrag(string zeile)
{
  if (regex_match(zeile, "^\\+[0-9]:[0-9][0-9] .*")) {
    zeit_von = Zeit(zeile[1] - '0', (zeile[3] - '0') * 10 + zeile[4] - '0');
    zeile.erase(0, 6);
  } else if (regex_match(zeile, "^\\+[0-2][0-9]:[0-9][0-9] .*")) {
    zeit_von = Zeit((zeile[1] - '0') * 10 + zeile[2] - '0', (zeile[4] - '0') * 10 + zeile[5] - '0');
    zeile.erase(0, 7);
  } else if (regex_match(zeile, "^-[0-9]:[0-9][0-9] .*")) {
    zeit_bis = Zeit(zeile[1] - '0', (zeile[3] - '0') * 10 + zeile[4] - '0');
    zeile.erase(0, 6);
  } else if (regex_match(zeile, "^-[0-2][0-9]:[0-9][0-9] .*")) {
    zeit_bis = Zeit((zeile[1] - '0') * 10 + (zeile[2] - '0'), (zeile[4] - '0') * 10 + zeile[5] - '0');
    zeile.erase(0, 7);
  } else if (regex_match(zeile, "^[0-9]:[0-9][0-9]-[0-9]:[0-9][0-9] .*")) {
    zeit_von = Zeit(zeile[0] - '0', (zeile[2] - '0') * 10 + zeile[3] - '0');
    zeit_bis = Zeit(zeile[5] - '0', (zeile[7] - '0') * 10 + zeile[8] - '0');
    zeile.erase(0, 10);
  } else if (regex_match(zeile, "^[0-2][0-9]:[0-9][0-9]-[0-9]:[0-9][0-9] .*")) {
    zeit_von = Zeit((zeile[0] - '0') * 10 + zeile[1] - '0', (zeile[3] - '0') * 10 + zeile[4] - '0');
    zeit_bis = Zeit(zeile[6] - '0', (zeile[8] - '0') * 10 + zeile[9] - '0');
    zeile.erase(0, 11);
  } else if (regex_match(zeile, "^[0-9]:[0-9][0-9]-[0-2][0-9]:[0-9][0-9] .*")) {
    zeit_von = Zeit(zeile[0] - '0', (zeile[2] - '0') * 10 + zeile[3] - '0');
    zeit_bis = Zeit((zeile[5] - '0') * 10 + zeile[6] - '0', (zeile[8] - '0') * 10 + zeile[9] - '0');
    zeile.erase(0, 11);
  } else if (regex_match(zeile, "^[0-2][0-9]:[0-9][0-9]-[0-2][0-9]:[0-9][0-9] .*")) {
    zeit_von = Zeit((zeile[0] - '0') * 10 + zeile[1] - '0', (zeile[3] - '0') * 10 + zeile[4] - '0');
    zeit_bis = Zeit((zeile[6] - '0') * 10 + zeile[7] - '0', (zeile[9] - '0') * 10 + zeile[10] - '0');
    zeile.erase(0, 12);
  }
  if (zeile == "$")
    zeile.clear();
  String::replace_all(zeile, "~", " ");
  text_ = {{zeile}};
}


Reaktion::Eintrag::Eintrag(vector<vector<string>> text) :
  text_(text)
{
  for (auto& text : text_) {
    for (auto& t : text) {
      if (t == "$")
        t.clear();
      String::replace_all(t, "~", " ");
    }
  }
}


void Reaktion::Eintrag::ergaenze(RandomText& random_text, string const& spieler) const
{
  auto const j = jetzt();
  if (j >= zeit_von && j <= zeit_bis) {
    auto const text = this->text();
    if (text == "!clear")
      random_text.clear();
    else
      random_text.add(String::replaced_all(text, "%s", spieler));
  }
}


void Reaktion::Eintrag::ergaenze_immer(RandomText& random_text, string const& spieler) const
{
  random_text.add(String::replaced_all(text(), "%s", spieler));
}


auto Reaktion::Eintrag::text() const -> string
{
  string eintrag;
  for (auto const& t : text_) {
    if (!t.empty()) {
      auto dis = std::uniform_int_distribution<>(0, t.size() - 1);
      eintrag += t.at(dis(random_generator));
    }
  }
  return eintrag;
}


Reaktion::Reaktion(string const& kennwort, istream& istr) :
  kennwort_(kennwort)
{
  if (regex_match(kennwort_, "^[0-9]%.*")) {
    wahrscheinlichkeit_ = kennwort_[0] - '0';
    kennwort_.erase(0, 2);
  } else if (regex_match(kennwort_, "^[0-9][0-9]%.*")) {
    wahrscheinlichkeit_ = (kennwort_[0] - '0') * 10 + (kennwort_[1] - '0');
    kennwort_.erase(0, 3);
  }
  while (!kennwort_.empty() && isspace(kennwort_[0]))
    kennwort_.erase(0, 1);
  using std::getline;
  string zeile;
  // Wenn die nächste Zeile mit | beginnt, diese in die regexp integrieren
  if (istr.good() && istr.peek() == '|') {
    kennwort_.insert(0, 1, '(');
    while (istr.good() && istr.peek() == '|') {
      getline(istr, zeile);
      kennwort_.append(zeile);
    }
    kennwort_.append(1, ')');
  }
  // Empfänger einlesen
  while (istr.good()) {
    getline(istr, zeile);
    while (!zeile.empty() && isspace(zeile[0])) {
      zeile.erase(zeile.begin());
    }
    if (zeile.empty())
      break;
    if (zeile[0] == '#')
      continue;
    if (zeile[0] != '>') {
      break;
    }
    zeile.erase(zeile.begin());

    while (!zeile.empty() && isspace(zeile[0])) {
      zeile.erase(zeile.begin());
    }
    empfaenger_.emplace_back(zeile);
  }
  while (istr.good()) {
    while (!zeile.empty() && isspace(zeile[0])) {
      zeile.erase(zeile.begin());
    }
    if (zeile.empty())
      break;
    if (zeile[0] == '#') {
      getline(istr, zeile);
      continue;
    }
    if (zeile.size() >= 2 && zeile[0] == '~' && zeile[1] == '1') {
      // Kombinationen über mehrere Zeilen aufgespalten
      // Die Zeilen beginnen mit ~1, ~2, ~3, ...
      vector<vector<string>> text;
      int ebene = 0;
      while (istr.good()) {
        if (zeile.size() < 2)
          break;
        if (zeile[0] != '~')
          break;
        if (zeile[1] == '0' + ebene + 1) {
          ebene += 1;
          text.emplace_back();
        }
        if (zeile[1] != '0' + ebene)
          break;
        if (zeile.size() == 2)
          text.back().emplace_back();
        else if (zeile[2] != ' ')
          break;
        else
          text.back().emplace_back(zeile.substr(3));
        getline(istr, zeile);
      }

      eintrag_.emplace_back(text);
    } else {
      eintrag_.emplace_back(zeile);
      getline(istr, zeile);
    }
  }
}


Reaktion::Reaktion(Reaktion const&) = default;


void Reaktion::write(ostream& ostr) const
{
  if (wahrscheinlichkeit_ < 100)
    ostr << wahrscheinlichkeit_ << "% ";
  ostr << kennwort_ << '\n';
  for (auto const& e : empfaenger_)
    ostr << "> " << e << '\n';
  for (auto const& e : eintrag_)
    ostr << e << '\n';
}


auto Reaktion::kennwort_match(string const& kennwort) const -> bool
{
  if (!kennwort_.empty() && kennwort_.front() == '+')
    return regex_matchi(kennwort, "\\" + kennwort_);
  return regex_matchi(kennwort, kennwort_);
}


void Reaktion::ergaenze(RandomText& text, string const& kennwort, string const& spieler) const
{
  if (!kennwort_match(kennwort))
    return ;
  if (!empfaenger_.empty() && !contains(empfaenger_, spieler) && !contains(empfaenger_, "*"))
    return ;
  static auto dis = std::uniform_int_distribution<>(0, 99);
  if (dis(random_generator) >= wahrscheinlichkeit_)
    return ;
  for (auto const& e : eintrag_)
    e.ergaenze(text, spieler);
}


void Reaktion::ergaenze_immer(RandomText& text, string const& kennwort, string const& spieler) const
{
  if (!kennwort_match(kennwort))
    return ;
  if (!empfaenger_.empty() && !contains(empfaenger_, spieler) && !contains(empfaenger_, "*"))
    return ;
  for (auto const& e : eintrag_)
    e.ergaenze_immer(text, spieler);
}


auto operator<<(ostream& ostr, Reaktion::Eintrag const& eintrag) -> ostream&
{
  if (eintrag.zeit_von == Zeit(0, 0) && eintrag.zeit_bis == Zeit(24, 0))
    ;
  else if (eintrag.zeit_von == Zeit(0, 0) && eintrag.zeit_bis < Zeit(24, 0))
    ostr << '-' << eintrag.zeit_bis << ' ';
  else if (eintrag.zeit_von > Zeit(0, 0) && eintrag.zeit_bis == Zeit(24, 0))
    ostr << '+' << eintrag.zeit_von << ' ';
  else
    ostr << eintrag.zeit_von << " - " << eintrag.zeit_bis << ' ';
  if (eintrag.text_.size() == 1 && eintrag.text_[0].size() == 1) {
    ostr << eintrag.text_[0][0];
  } else {
    for (size_t i = 0; i < eintrag.text_.size(); ++i) {
      for (auto const& t : eintrag.text_[i]) {
        ostr << '~' << i << ' ' << t << '\n';
      }
    }
  }
  return ostr;
}


auto operator<<(ostream& ostr, Reaktion const& reaktion) -> ostream&
{
  reaktion.write(ostr);
  return ostr;
}

} // namespace DokoLounge

#endif
