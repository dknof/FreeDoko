/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "spieltisch.h"
#include "conversion.h"
#include "client.h"

#include "../options.h"
#include "../party/party.h"
#include "../party/rule.h"
#include "../game/game.h"
#include "../game/gameplay.h"
#include "../game/game_summary.h"
#include "../game/gameplay_actions.h"
#include "../player/ai/ai.h"
#include "../player/cards_information.of_player.h"
#include "../player/team_information.h"
#include "../misc/bug_report.h"
#include "../misc/preferences.h"
#include "../utils/random.h"

#include <thread>
#include <chrono>
using namespace std::literals::chrono_literals;
#include <cctype>

namespace {
auto schweine_vor_ansage(Game const& game) -> bool;
auto endabrechnung_text(Game const& game) -> string;
}

namespace DokoLounge {

Spieltisch::Spieltisch(WebSockets::Client& socket_, Spielername const spieler_, Tischnummer const nummer_) :
  socket(&socket_),
  chatter(*this, spieler_),
  nummer(nummer_),
  spieler(spieler_)
{
  chatter.setze_geburtstagskinder(socket->geburtstagskinder);
  if (!socket->login_is_special()) {
    // Die Standardinitialisierung vom Zufallswert ist sekundengenau.
    // Damit erhalten drei gleichzeitig aufgerufene Dummies der gleichen Startwert.
    // Also wird der Startwert hier genauer initialisiert.
    auto const seed = std::chrono::system_clock::now().time_since_epoch().count();
    random_value.set_seed(seed);
    switch (random_value(5)) {
    case 0:
      difficulty = Aiconfig::Difficulty::cautious;
      break;
    case 1:
      difficulty = Aiconfig::Difficulty::chancy;
      break;
    default:
      difficulty = Aiconfig::Difficulty::standard;
      break;
    }
  }
  party = make_unique<Party>();
  party->open();
  clean();
  socket->sende_refresh();
}


Spieltisch::~Spieltisch() = default;


Spieltisch::operator bool() const
{
  return nummer != 0;
}


void Spieltisch::clean()
{
  if (game)
    game->close();
  game = nullptr;
  vector<string> spielernamen;

  trumpf                 .clear();
  solospieler            = 0;
  partner_hochzeit_armut = 0;
  spieler_mit_sau        = 0;
  spieler_mit_supersau   = 0;
  blatt                  .clear();
  team                   = Team::noteam;
  gelegte_karten         .clear();
  karten                 .clear();
  karten_auf_tisch       .clear();
  buffer_gewaehlte_karte = BufferGewaehlteKarte();

  kenne_startspieler = false;
  kenne_team         = false;
  springe_in_laufendes_spiel = false;
  meldung_gesendet   = false;
  spiel_gestartet    = false;
  armut_mit_trumpf   = -1;

  poverty_shifted_cards.clear();
  poverty_returned_cards.clear();

  in_invalid_game = false;

  party->set_status(Party::Status::init);

  if (socket)
    socket->clear_write_buffer();
}


void Spieltisch::chat(string text)
{
  if (socket->login_is_special()) {
    DEBUG("chat") << "chat: " << text << '\n';
  }
  if (text.size() >= 7 + 8
      && text.substr(0, 7) == "<<col>>") {
    auto const p = text.find("<</col>>");
    if (p != string::npos) {
      text.erase(0, p + 8);
    }
  }
  string name;
  if (text.size() >= 8 + 9
      && text.substr(0, 8) == "<<name>>") {
    auto const p = text.find("<</name>>");
    if (p != string::npos) {
      name = text.substr(8, p - 8);
      text.erase(0, p + 9);
    }
  }
  auto const wird_gefluestert = [&text](string const& frage) -> bool {
    if (frage.empty())
      return false;
    auto const fluestern = " flüstert an dich: " + (frage.back() == '*'
                                                    ? frage.substr(0, frage.length() - 1)
                                                    : frage + "</b>");

    if (frage.back() == '*') {
      return (text.find(fluestern) != string::npos);
    } else {
      return (   text.size() > fluestern.size()
              && text.substr(text.size() - fluestern.size()) == string_lc(fluestern));
    }

  };
  auto const fluesterer = [&text]() -> string {
    if (text.substr(0, 3) != "<b>")
      return {};
    auto const p = text.find(" flüstert an dich");
    if (p == string::npos)
      return {};
    return text.substr(3, p - 3);
  };
  auto const spieler = (this->spieler.substr(0, 9) == "FreeDoko#"
                        ? this->spieler.substr(8) // Die Raute stehen lassen
                        : this->spieler);
  auto const spielerx = (this->spieler.substr(0, 9) == "FreeDoko#"
                         ? "(#?" + this->spieler.substr(9) + ")"
                         : this->spieler);

  if (chat_keys.count("cout") && !name.empty()) {
    cout << name + ": " << text << '\n';
  }

  if (   text == spieler + " muss noch eine Meldung machen."
      || text == this->spieler + " muss noch eine Meldung machen.") {
    auto const vorbehalt = waehle_vorbehalt();
    if (!vorbehalt.empty()) {
      socket->sende_meldung(vorbehalt);
    }
  } else if (text == "<b>Niemand möchte die Armut mitnehmen.</b>") {
    setze_armut_abgelehnt();
  } else if (text == "Die Armut hat Trumpf bekommen.") {
    armut_mit_trumpf = 1;
  } else if (text == "Die Armut hat keinen Trumpf bekommen.") {
    armut_mit_trumpf = 0;
  } else if (text == "Diese Karte darfst du nicht legen. Du musst erst bedienen."
             || text == "Hallo?<br>Du musst erst bedienen."
             || text == "Heute spielen wir MIT Bedienen."
             || text == "Heute halten wir uns einmal an die Dokoregeln. Erst bedienen.") {
    socket->write_log("Unzulässiges Spiel: Die ausgespielte Karte ist nicht zulässig.");
    in_invalid_game = true;
    auto const karte = waehle_karte();
    socket->sende_karte(karte);
  } else if (text.size() >= 30 && text.substr(20) == "<b>" + spieler + "kommt raus</b>") {
    auto const karte = waehle_karte();
    socket->sende_karte(karte);
  } else if (text.size() >= 30 && text.substr(20) == "<b>" + this->spieler + "kommt raus</b>") {
    auto const karte = waehle_karte();
    socket->sende_karte(karte);
  } else if (wird_gefluestert("version")) {
    string const compile_date =
#include "../compile_date.txt"
      ;
    socket->fluestere(fluesterer(), "Version vom "
                      + to_long_string_de(Date(std::stoi(compile_date.substr(0, 4)),
                                               std::stoi(compile_date.substr(6, 2)),
                                               std::stoi(compile_date.substr(9, 2)))));
  } else if (wird_gefluestert("status")) {
    socket->fluestere(fluesterer(), "Status: " + status_string());
  } else if (wird_gefluestert("KI")) {
    socket->fluestere(fluesterer(), "Schwierigkeit: " + _(ai().difficulty()));
  } else if (wird_gefluestert("offline")) {
    socket->write_log("Wurde mit offline angeflüstert");
    socket->write_log_current_time();
    DEBUG("offline") << "Wurde mit offline angeflüstert\n";
    throw Offline();
  } else if (wird_gefluestert("?")) {
    if (game) {
      auto rationale = ai().last_heuristic_rationale();
      if (!rationale.empty())
        socket->send(Element("tischchat", Element("name", this->spieler) + "/f " + fluesterer() + " "
                             + _(ai().last_heuristic()) + ":\n" + _(rationale)));
    }
  } else if (   wird_gefluestert("Fehlerbericht*")
             || wird_gefluestert("Vorschlag*")) {
    erstelle_fehlerbericht(text, fluesterer());
    socket->fluestere(fluesterer(), "Ich habe den Verbesserungsvorschlag erstellt, vielen Dank dafür, " + name + ".");
  } else if (   regex_matchi(text, spielerx + " fehlerbericht.*")
             || regex_matchi(text, spielerx + " vorschlag.*")) {
    erstelle_fehlerbericht(text, name);
    socket->sage("Ich habe den Verbesserungsvorschlag erstellt, vielen Dank dafür, " + name + ".");
  } else if (text == "log help" || text == "log h" || text == "log hilfe" || text == "log Hilfe") {
    cout
      << "log Version:    Gibt die Version aus\n"
      << "log Lizenz:     Gibt die Lizenz aus\n"
      << "log Kontakt:    Gibt Kontaktinformationen aus\n"
      << "log Status:     Gibt den internen Spielstatus aus\n"
      << "log Spieltisch: Gibt den gesamten Spieltisch aus\n"
      << "log Spiel:      Gibt das gesamte Spiel aus\n"
      << "log Blatt:      Gibt das eigene Blatt aus\n"
      << "log Teams:      Gibt die bekannte Teamzuordnung aus\n"
      << "log Karten:     Gibt die Blattinformationen aus\n"
      << "log ?:          Gibt eine Erläuterung für die letzte gespielte Karte aus\n"
      ;
  } else if (text == "log version" || text == "log Version") {
    cout << spieler << ": Version = " << version_string() << '\n';
  } else if (text == "log status" || text == "log Status") {
    cout << spieler << ": Spielstatus = " << (game ? _(game->status()) : _(party->status())) << '\n';
  } else if (text == "log spieltisch") {
    cout << spieler << ": Spieltisch:\n" << *this << '\n';
  } else if (text == "log game" || text == "log spiel" || text == "log Spiel") {
    if (game)
      cout << spieler << ": Spiel:\n" << *game << '\n';
  } else if (text == "log teams") {
    if (game)
      cout << spieler << ": Teaminformationen:\n" << ai().team_information() << '\n';
  } else if (text == "log cards" || text == "log karten" || text == "log Karten") {
    if (game)
      cout << spieler << ": Karteninformationen:\n" << ai().cards_information() << '\n';
  } else if (text == "log hand" || text == "log blatt" || text == "log Blatt") {
    if (game)
      cout << spieler << ": Blatt:\n" << hand() << '\n';

  } else if (text == "log ?") {
    if (game) {
      auto rationale = ai().last_heuristic_rationale();
      if (!rationale.empty())
        cout << _(ai().last_heuristic()) << ":\n" << rationale << '\n';
    }
  } else if (text.substr(0, 7) == "debug +") {
    cout << "Debug: +" + text.substr(7) << '\n';
    ::debug.add(text.substr(7));
  } else if (text.substr(0, 7) == "debug -") {
    cout << "Debug: -" + text.substr(7) << '\n';
    ::debug.remove(text.substr(7));
  } else if (regex_matchi(text, spielerx + " Hilfe")) {
    string text = "Folgende Befehle verstehe ich (meinen Namen vorsetzen):\n"
      "* Hilfe: Ausgabe dieser Hilfe\n"
      "* Version: Version (Datum)\n"
      "* Kontakt: Kontaktmöglichkeiten bezüglich FreeDoko\n"
      "* Lizenz: Lizenz von FreeDoko\n"
      "* Status: Ausgabe des Status\n"
      "* KI: Ausgabe der Schwierigkeit meiner KI\n"
      "* standard: Stelle meiner KI-Schwierigkeit auf „standard“\n"
      "* vorsichtig: Stelle meiner KI-Schwierigkeit auf „vorsichtig“\n"
      "* riskant: Stelle meiner KI-Schwierigkeit auf „riskant“\n"
      "* +Absagen: Aktiviere Absagen\n"
      "* ?: Ausgabe der Entscheidung für meiner letzte gespielte Karte (nur wenn alleine mit FreeDokos)\n"
      "* still: Nichts mehr sagen\n"
      "* reden: Wieder etwas sagen\n"
      "* chat: Ausgabe, welcher Chat aktiviert ist\n"
      "* chat ++: Aktiviert allen automatischen Chat\n"
      "* chat +xxx: Aktiviert automatischen Chat zum angegebenen Kennwort xxx (dp, gw, …)\n"
      "* chat --: Deaktiviert allen automatischen Chat\n"
      "* chat -xxx: Deaktiviert automatischen Chat zum angegebenen Kennwort xxx\n"
      "* Vorschlag Text: Erstellt einen Verbesserungsvorschlag mit dem angebenen Text\n"
      "* offline: Melde mich ab\n";
    if (offenes_blatt()) {
      text += "Für Dich sind noch die folgenden Befehle freigeschaltet:\n"
        "* Team\n"
        "* Blatt\n"
        "* Spiel\n"
        "* Teaminformation\n"
        "* Karteninformation\n";
    }
    socket->sage(text);
  } else if (   text == string_lc("version")
             || regex_matchi(text, spielerx + " version")) {
    socket->sage("Version vom " + to_long_string_de(Date(__DATE__)));
  } else if (regex_matchi(text, spielerx + " lizenz")) {
    socket->sage("FreeDoko ist Freie Software: Du kannst es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Deiner Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <a href=\"https://www.gnu.org/licenses/\" target=\"_blank\">https://www.gnu.org/licenses/</a>).");
  } else if (regex_matchi(text, spielerx + " kontakt")) {
    socket->sage("Webseite: <a href=\"http://free-doko.sourceforge.net/\" target=\"_blank\">http://free-doko.sourceforge.net/</a>\n"
                 "E-Mail: <a href=\"mailto:dknof@posteo.de?subject=FreeDoko\">dknof@posteo.de</a>");
  } else if (   zaehle_menschen() == 1
             && text == string_lc("Hallo")) {
    chatter.sage("Hallo", name);
  } else if (   text == string_lc("Status")
             || regex_matchi(text, spielerx + " status")) {
    socket->sage("Status: " + status_string());
  } else if (   text == string_lc("KI")
             || regex_matchi(text, spielerx + " ki")) {
    if (game)
      socket->sage("Schwierigkeit: " + _(ai().difficulty()));
    else
      socket->sage("Schwierigkeit: " + _(difficulty));
  } else if (regex_matchi(text, spielerx + " standard")) {
    setze_ki_schwierigkeit("standard");
    socket->sage("Schwierigkeit: " + _(difficulty));
  } else if (   regex_matchi(text, spielerx + " cautious")
             || regex_matchi(text, spielerx + " vorsichtig")) {
    setze_ki_schwierigkeit("vorsichtig");
    socket->sage("Schwierigkeit: " + _(difficulty));
  } else if (   regex_matchi(text, spielerx + " chancy")
             || regex_matchi(text, spielerx + " riskant")) {
    setze_ki_schwierigkeit("riskant");
    socket->sage("Schwierigkeit: " + _(difficulty));
  } else if (regex_matchi(text, spielerx + ",? (offline|hopp ins Bett)")) {
    socket->write_log("Wurde mit offline angesprochen");
    socket->write_log_current_time();
    DEBUG("offline") << "Wurde mit offline angesprohcen\n";
    throw Offline();
  } else if (regex_matchi(text, spielerx + " (sprich|reden|laut)")) {
    ::chat_keys.erase("still");
    chatter.sage("<reden>", name);
  } else if (   text == "chat"
             || regex_matchi(text, spielerx + " chat")
             || regex_matchi(text, "chat " + spielerx)) {
    if (::chat_keys.empty()) {
      socket->sage("Es ist kein Chat aktiviert.");
    } else if (::chat_keys.count("still")) {
      socket->sage("Ich bin still.");
    } else {
      string text = "Folgende Chats sind aktiviert: ";
      for (auto const& t : ::chat_keys) {
        text.append(t).append(", ");
      }
      text.pop_back(); // Leerzeichen
      text.pop_back(); // Komma
      socket->sage(text);
    }
  } else if (   regex_matchi(text, "chat ?([+][+]|[+][*]|[*])")
             || regex_matchi(text, spielerx + " chat ?([+][+]|[+][*]|[*])")) {
    ::chat_keys.insert("Hallo");
    ::chat_keys.insert("gw"); // Glückwunsch
    ::chat_keys.insert("dp"); // Dankeschön
    ::chat_keys.insert("Fuchs"); // Fuchs gefangen
    ::chat_keys.insert("Doppelkopf"); // 40+ Punkte erhalten
    ::chat_keys.insert("Offline");
    ::chat_keys.insert("Witz");
  } else if (   regex_matchi(text, "chat ?[-]([-]|[*])")
             || regex_matchi(text, spielerx + " chat ?[-]([-]|[*])")) {
    ::chat_keys.clear();
  } else if (text.substr(0, 6) == "chat +") {
    ::chat_keys.insert(text.substr(6));
  } else if (text.substr(0, 6) == "chat -") {
    ::chat_keys.erase(text.substr(6));
  } else if (text.substr(0, (spieler + " chat +").size()) == spieler + " chat +") {
    ::chat_keys.insert(text.substr((spieler + " chat +").size()));
  } else if (text.substr(0, ("#" + spieler + " chat +").size()) == "#" + spieler + " chat +") {
    ::chat_keys.insert(text.substr(("#" + spieler + " chat +").size()));
  } else if (text.substr(0, (spieler + " chat -").size()) == spieler + " chat -") {
    ::chat_keys.erase(text.substr((spieler + " chat -").size()));
  } else if (text.substr(0, ("#" + spieler + " chat -").size()) == "#" + spieler + " chat -") {
    ::chat_keys.erase(text.substr(("#" + spieler + " chat -").size()));
  } else if (text.substr(0, (spieler + " chat ").size()) == spieler + " chat ") {
    ::chat_keys.insert(text.substr((spieler + " chat ").size()));
  } else if (text.substr(0, ("#" + spieler + " chat ").size()) == "#" + spieler + " chat ") {
    ::chat_keys.insert(text.substr(("#" + spieler + " chat ").size()));
  } else if (   text == "?"
             || regex_matchi(text, spielerx + " [?]")) {
    if (!offene_ki_informationen()) {
      socket->sage("Die Spielinformation gibt es nur, wenn Du alleine gegen FreeDokos spielst");
    } else {
      if (game) {
        auto rationale = ai().last_heuristic_rationale();
        if (!rationale.empty())
          socket->sage(_(ai().last_heuristic()) + ":\n" + _(rationale));
      }
    }
#if 0
  } else if (   text == "Info") {
    // ToDo: Im Chat wird nur die erste Nachricht angezeigt. Eventuell verschluckt der Server die folgenden.
    socket->sage("Menschen: " + std::to_string(zaehle_menschen()));
    if (offenes_blatt())
      socket->sage("Offenes Blatt");
    if (offene_ki_informationen())
      socket->sage("Offene KI-Informationen");
#endif
  } else if (   regex_matchi(text, spielerx + " Absagen")
             || (   zaehle_menschen() == 1
                 && text == "Absagen")) {
    if (mit_absagen)
      socket->sage("Ich sage ab.");
    else
      socket->sage("Ich sage nicht ab.");
  } else if (   regex_matchi(text, spielerx + " +Absagen")
             || (   zaehle_menschen() == 1
                 && text == "+Absagen")) {
    mit_absagen = true;
    socket->sage("Ich sage ab sofort ab.");
  } else if (   regex_matchi(text, spielerx + " -Absagen")
             || (   zaehle_menschen() == 1
                 && text == "-Absagen")) {
    mit_absagen = false;
    socket->sage("Ich sage ab sofort nicht ab.");
  } else if (   regex_matchi(text, spielerx + " Team")
             || (   offene_ki_informationen()
                 && text == "Team")) {
    if (offenes_blatt())
      socket->sage(_(ai().team()));
  } else if (   regex_matchi(text, spielerx + " Blatt")
             || regex_matchi(text, "Blatt " + spielerx)
             || (   offene_ki_informationen()
                 && text == "Blatt")) {
    if (offenes_blatt())
      chatter.blatt(ai());
  } else if (   regex_matchi(text, spielerx + " Spiel")
             || regex_matchi(text, spielerx + " Spielinfo")
             || regex_matchi(text, "Spiel " + spielerx)
             || regex_matchi(text, "Spielinfo " + spielerx)
             || (   offene_ki_informationen()
                 && text == "Spiel"
                 && text == "Spielinfo")) {
    if (offenes_blatt() && game)
      chatter.spiel(ai());
  } else if (   regex_matchi(text, spielerx + " Teaminformation")
             || regex_matchi(text, spielerx + " Teaminfo")
             || regex_matchi(text, spielerx + " Teams")
             || regex_matchi(text, "Teaminformation " + spielerx)
             || regex_matchi(text, "Teams " + spielerx)
             || (   offene_ki_informationen()
                 && (   text == "Teaminformation"
                     || text == "Teaminfo"
                     || text == "Teams"))) {
    if (offenes_blatt() && game)
      chatter.teaminfo(ai());
  } else if (   regex_matchi(text, spielerx + " Karteninformation")
             || regex_matchi(text, spielerx + " Karteninfo")
             || regex_matchi(text, spielerx + " Karten")
             || regex_matchi(text, "Karteninformation " + spielerx)
             || regex_matchi(text, "Karteninfo " + spielerx)
             || regex_matchi(text, "Karten " + spielerx)
             || (   offene_ki_informationen()
                 && (   text == "Karteninformation"
                     || text == "Karteninfo"
                     || text == "Karten"))) {
    if (offenes_blatt() && game)
      chatter.karteninfo(ai());
  } else {
    chatter.reagiere_auf_chat(name, text);
  }
}

void Spieltisch::karten_legen(string const& text)
{
  // Wenn eine Karte vom FreeDoko gelegt wurde und er in einem unzulässigen Spiel ist, wird der interne Status zurückgesetzt, damit das Spiel wieder zulässig wird.
  if (!in_invalid_game)
    return ;
  auto const elements = Element::split_into_elements(text);
  for (auto const& element : elements) {
    if (element.tag == "tisch") {
      if (element.content != std::to_string(nummer))
        return ;
    } else if (element.tag == "name") {
      if (element.content != spieler)
        return ;
    }
  }
  cerr << spieler << ": setze Spielstatus zurück.\n";
  clean();
  socket->sende_refresh();
}

void Spieltisch::setze_ki_schwierigkeit(string const& text)
{
  if (text == "standard")
    difficulty = Aiconfig::Difficulty::standard;
  else if (text == "cautious")
    difficulty = Aiconfig::Difficulty::cautious;
  else if (text == "vorsichtig")
    difficulty = Aiconfig::Difficulty::cautious;
  else if (text == "chancy")
    difficulty = Aiconfig::Difficulty::chancy;
  else if (text == "riskant")
    difficulty = Aiconfig::Difficulty::chancy;
  else
    difficulty = Aiconfig::Difficulty::standard;
  if (game) {
    ai().set_to_difficulty(difficulty);
  }
}

void Spieltisch::setze_regeln(string const& text)
{
  Rule rule;
  rule.set(Rule::Type::swines_announcement_begin,      false);
  rule.set(Rule::Type::hyperswines_announcement_begin, false);
  auto const elements = Element::split_into_elements(text);
  string tischinfo = ""; // DDV oder Sonderregen
  for (auto const& element : elements) {
    auto const value = (element.content == "1");
    if (element.tag == "tischinfo") {
      tischinfo = element.content;
    } else if (element.tag == "tisch") {
      if (element.content != std::to_string(nummer)) {
        return ;
      }
    } else if (element.tag == "oNeunen") {
      if (rule(Rule::Type::with_nines) != !value) {
        rule.set(Rule::Type::with_nines, !value);
      }
    } else if (element.tag == "skir") {
      rule.set(Rule::Type::lustsolo_player_leads, value);
    } else if (element.tag == "armut") {
      // rule.set(Rule::Type::poverty, value); // Hier steht immer 0 drin, dass aber ignorieren
    } else if (element.tag == "ErsteDulleImmerHoch") {
      rule.set(Rule::Type::dullen_second_over_first, !value);
    } else if (element.tag == "ersted") {
      rule.set(Rule::Type::dullen_contrary_in_last_trick, value);
    } else if (element.tag == "ohneSchweinchen") {
      rule.set(Rule::Type::swines, !value);
    } else if (element.tag == "ohneSupersau") {
      if (rule(Rule::Type::with_nines)) { // Hyperschweine/Supersau ist Trumpf Neun
        rule.set(Rule::Type::hyperswines, !value);
      }
    } else if (element.tag == "SauErstBeimAusspielen") {
      // Hier nicht relevant
    } else if (element.tag == "Karlchenfangen") {
      rule.set(Rule::Type::extrapoint_catch_charlie, value);
    } else if (element.tag == "SchmeissenErlauben") {
      rule.set(Rule::Type::throw_with_nines, value);
      rule.set(Rule::Type::throw_with_kings, value);
      rule.set(Rule::Type::min_number_of_throwing_nines, 5);
      rule.set(Rule::Type::min_number_of_throwing_kings, 5);
    } else if (element.tag == "Feigheitsregel") {
      rule.set(Rule::Type::cowardliness_dokolounge, value);
    } else if (element.tag == "Kartenzeigenflg") {
      // Hier nicht relevant
    } else if (element.tag == "TischBesitzerIstBoss") {
      // Hier nicht relevant
    } else {
      cerr << spieler << ": Regel „" << element.tag << "“ unbekannt.\n";
    }
  }
  if (tischinfo == "DDV") {
    rule.set(Rule::Type::with_nines,                    true);
    rule.set(Rule::Type::poverty,                       false);
    rule.set(Rule::Type::swines,                        false);
    rule.set(Rule::Type::hyperswines,                   false);
    rule.set(Rule::Type::lustsolo_player_leads,         false);
    rule.set(Rule::Type::dullen_second_over_first,      false);
    rule.set(Rule::Type::dullen_contrary_in_last_trick, false);
    rule.set(Rule::Type::cowardliness_dokolounge,       false);
    rule.set(Rule::Type::extrapoints_in_solo,           false);
  } else {
    rule.set(Rule::Type::poverty,                       true);
    rule.set(Rule::Type::extrapoints_in_solo,           true);
  }
  if (!rule(Rule::Type::with_nines)) {
    rule.set(Rule::Type::hyperswines, false); // Hyperschweine/Supersau ist Trumpf Neun
  }
  if (party->rule() != rule) {
    if (party->rule()(Rule::Type::with_nines) != rule(Rule::Type::with_nines)) {
      clean();
    }
    party->rule() = rule;
    setze_kenne_regeln();
  }
}


void Spieltisch::setze_spieler(string const& text)
{
  auto const elements = Element::split_into_elements(text);
  auto namen = vector<string>(4);
  for (auto const& element : elements) {
    auto const& name = element.content;
    if (   element.tag == "spieler1"
        || element.tag == "spieler2"
        || element.tag == "spieler3"
        || element.tag == "spieler4") {
      auto const n = std::stoi(element.tag.substr(7)) - 1;
      namen[n] = name;
    } else if (element.tag == "tisch") {
      if (element.content != std::to_string(nummer)) {
        //cerr << "Tisch: Tischnummer stimmt nicht. Server sagt „" << element.content << "”, aber erwarte „" << nummer << "“\n";
        return ;
      }
    } else if (   element.tag == "punkte1"
               || element.tag == "punkte2"
               || element.tag == "punkte3"
               || element.tag == "punkte4") {
    } else if (   element.tag == "geschlecht1"
               || element.tag == "geschlecht2"
               || element.tag == "geschlecht3"
               || element.tag == "geschlecht4") {
    } else if (element.tag == "Sterne") {
    } else {
      cerr << spieler << ": Tisch: unbekannter Tag „" << element.tag << "“\n";
    }
  }
  if (!contains(namen, spieler)) {
    cerr << "Die Spielerliste enthält nicht " << spieler << ".\n";
  }
  chatter.reagiere_auf_spielerliste(namen);
  for (size_t p = 0; p < 4; ++p) {
    party->players().player(p).person().set_name(namen[p]);
    party->players().set_type(p, (namen[p] == spieler
                                  ? Player::Type::ai
                                  : Player::Type::ai_dummy));
  }
  advance_game_if_possible();
  if (   !socket->login_is_special()
      && alle_spieler_freedoko()
      && spieler == party->players().player(1).name()
      && knecht != "an"
      && knecht != "gesendet") {
    socket->send("knechtgeben", spieler);
    knecht = "gesendet";
  }
}


void Spieltisch::setze_knecht(string const& text)
{
  knecht = text;
  if (   !socket->login_is_special()
      && alle_spieler_freedoko()
      && spieler == party->players().player(1).name()
      && knecht == "aus") {
    socket->send("knechtgeben", spieler);
    knecht = "gesendet";
  }
}


void Spieltisch::setze_karten(string const& text)
{
  if (   !kenne_regeln
      && !kenne_startspieler
      && (   text == "00-00-00-00-00-00-00-00-00-00-00-00-"
          || text == "00-00-00-00-00-00-00-00-00-00-")) {
    // Wird teilweise am Anfang gesendet
    // Wenn keine Karten da sind gibt es auch nichts zu entscheiden
    return ;
  }
  if (karten == text)
    return ;

  karten = text;

  blatt = to_blatt(karten, rule()(Rule::Type::number_of_cards_on_hand));
  if (blatt.size() != rule()(Rule::Type::number_of_cards_on_hand)) {
    cerr << spieler << ": Anzahl der Karten im Blatt " << blatt.size() << " entspricht nicht der erwarteten Anzahl " << rule()(Rule::Type::number_of_cards_on_hand) << ".\n";
    return;
  }

  if (   game
      && game->type()   == GameType::poverty
      && game->status() == Game::Status::start) {
    set_hand();
    start_game();
  }
}


void Spieltisch::setze_trumpf(string const& text)
{
  if (!trumpf.empty() && text == "Karo")
    return ;
  trumpf = text;
}


void Spieltisch::setze_team(string const& text)
{
  if (text == string_lc("Re")) {
    team = Team::re;
  } else if (text == string_lc("Kontra")) {
    team = Team::contra;
  } else {
    cerr << spieler << ": Spieltisch: unbekanntes Team „" << text << "“\n";
  }
  if (!game)
    return ;
  if (game->status() < Game::Status::play)
    return ;
  if (game->marriage().is_undetermined())
    return ;
  setze_kenne_team();
}


void Spieltisch::setze_ist_re(string const& text)
{
  if (!game)
    return ;
  if (game->status() < Game::Status::play)
    return ;
  if (text == spieler && team == Team::unknown) {
    team = Team::re;
    setze_kenne_team();
  }
  for (auto& player : game->players()) {
    if (player.name() != text)
      continue;
    if (player.team() != Team::re) {
      DEBUG("gameplay") << spieler << ": Team von Spieler " << player.no() << " " << player.name() << ": " << team << '\n';
      player.set_team(Team::re);
    }
    game->teaminfo().set(player, Team::re);
  }
  ai().team_information().update_teams();
}


void Spieltisch::setze_ansage(string const& text)
{
  if (!game)
    return ;

  auto tail = [&text](size_t const size) -> string_lc {
    if (text.size() < size)
      return {};
    return string_lc(text.substr(text.size() - size));
  };
  struct {
    string const text;
    Team const team;
    Announcement const announcement;
  } ansagen[] = {
    {": RE"s,               Team::re,     Announcement::reply},
    {": RE keine 120"s,     Team::re,     Announcement::no120},
    {": RE keine 90"s,      Team::re,     Announcement::no90},
    {": RE keine 60"s,      Team::re,     Announcement::no60},
    {": RE keine 30"s,      Team::re,     Announcement::no30},
    {": RE schwarz"s,       Team::re,     Announcement::no0},
    {": KONTRA"s,           Team::contra, Announcement::reply},
    {": KONTRA keine 120"s, Team::contra, Announcement::no120},
    {": KONTRA keine 90"s,  Team::contra, Announcement::no90},
    {": KONTRA keine 60"s,  Team::contra, Announcement::no60},
    {": KONTRA keine 30"s,  Team::contra, Announcement::no30},
    {": KONTRA schwarz"s,   Team::contra, Announcement::no0},
  };
  for (auto const& ansage : ansagen) {
    if (tail(ansage.text.size()) == ansage.text) {
      auto const n = spielernummer(text.substr(0, text.size() - ansage.text.size()));
      if (n > 0) {
        set_announcement(n - 1, ansage.team, ansage.announcement);
        auto& player = game->player(n - 1);
        player.set_team(ansage.team);
        game->teaminfo().set(player, ansage.team);
      }
      break;
    }
  }
}


void Spieltisch::setze_solo(string const& text)
{
  if (!game)
    return ;
  auto const elements = Element::split_into_elements(text);
  string solo;
  for (auto const& element : elements) {
    if (element.tag == "Name") {
      solospieler = spielernummer(element.content);
      if (solospieler != 0)
        set_soloplayer(solospieler - 1);
    } else if (element.tag == "Solo") {
      solo = element.content;
    } else {
      cerr << spieler << ": Spieltisch: Soloinfo: unbekannter Tag „" << element << "“.\n";
    }
  }
  if (!solo.empty() && solo != trumpf) {
    trumpf = solo;
    if (game->status() > Game::Status::reservation) {
      // throw "Solo " + solo + ", aber das Spiel läuft schon.";
      if (solospieler != 0) {
        game->force_set_solo(to_game_type(solo), game->player(solospieler - 1));
      }
    }
  }
  setze_spiel_laeuft();
}


void Spieltisch::setze_hochzeit(Spielername const& braut)
{
  if (!game)
    return ;
  solospieler = spielernummer(braut);
  trumpf = "Hochzeit";
  if (game->type() != GameType::marriage) {
    game->set_type(GameType::marriage);
    auto const spielera = spielernummer(braut);
    for (Player& player : game->players()) {
      if (player.no() == spielera - 1) {
        game->players().set_soloplayer(player);
        player.set_team(Team::re);
      } else {
        player.set_team(Team::noteam);
      }
    }
    game->marriage().set_selector(MarriageSelector::first_foreign);
  }
  setze_spiel_laeuft();
}


void Spieltisch::setze_hochzeitspartner(Spielername const& braeutigam)
{
  if (!game)
    return ;
  partner_hochzeit_armut = spielernummer(braeutigam);
  trumpf = "Hochzeit";
  auto const spielera = spielernummer(braeutigam);
  auto& player = game->players().player(spielera - 1);
  player.set_team(Team::re);
  for (Player& player : game->players()) {
    if (player.team() == Team::unknown) {
      player.set_team(Team::contra);
    }
  }
  game->marriage().set_selector(MarriageSelector::team_set);
}


void Spieltisch::setze_armut_abgelehnt()
{
  trumpf = "Karo";
  advance_game_if_possible();
}


void Spieltisch::setze_armut_team(string const& text)
{
  if (!game)
    return ;
  if (game->status() < Game::Status::reservation)
    return ;

  auto const elements = Element::split_into_elements(text);
  if (!(   elements.size() == 2
        || elements[0].tag == "a"
        || elements[1].tag == "b")) {
    return ;
  }
  auto const spielera = spielernummer(elements[0].content);
  auto const spielerb = spielernummer(elements[1].content);
  if (spielera == 0 || spielerb == 0)
    return ;

  if (kenne_team && game->type() == GameType::poverty)
    return ;

  trumpf = "Armut";
  setze_kenne_team();
  game->players().set_soloplayer(game->players().player(spielera - 1));
  for (Player& player : game->players()) {
    if (player.no() == spielera - 1) {
      game->players().set_soloplayer(player);
      player.set_team(Team::re);
    } else if (player.no() == spielerb - 1) {
      player.set_team(Team::re);
    } else {
      player.set_team(Team::contra);
    }
    game->teaminfo().set(player, player.team());
  }

  if (   (springe_in_laufendes_spiel || game->status() == Game::Status::play)
      && game->type() == GameType::normal) {
    game->set_type(GameType::poverty);
    return ;
    // setze die Teams
  }
  game->set_type(GameType::poverty);
  game->set_status(Game::Status::poverty_shift);
  if (ai().team() == Team::re) {
    karten.clear();
    blatt.clear();
    game->set_status(Game::Status::start);
    // Die Methode start_game() wird aufgerufen, wenn die Karten das nächste Mal gesetzt werden.
  } else {
    start_game();
  }
}


void Spieltisch::setze_sau(Spielername const& spieler)
{
  auto const n = spielernummer(spieler);
  if (n == 0)
    return ;
  spieler_mit_sau = n;
  if (!game || game->status() < Game::Status::reservation)
    return ;
  set_swines(spieler_mit_sau - 1);
}


void Spieltisch::setze_supersau(Spielername const& spieler)
{
  auto const n = spielernummer(spieler);
  if (n == 0)
    return ;
  spieler_mit_supersau = n;
  if (!game || game->status() < Game::Status::reservation)
    return ;
  set_hyperswines(spieler_mit_supersau - 1);
}


void Spieltisch::setze_spiel_laeuft()
{
  if (!game || game->status() >= Game::Status::play)
    return ;
  if (   karten.empty()
      || trumpf.empty())
    return;
  spiel_gestartet = true;
  setze_kenne_team();
  advance_game_if_possible();
}


void Spieltisch::setze_karten_auf_tisch(string const& text)
{
  if (!game || game->status() < Game::Status::play) {
    karten_auf_tisch = text;
    setze_startspieler_aus_karten_auf_tisch(text);
    springe_in_laufendes_spiel = true;
    DEBUG("refresh") << spieler << ": Springe in laufendes Spiel. Karten auf dem Tisch: " << text << '\n';
    socket->sende_refresh();
    return ;
  }
  if (text == karten_auf_tisch) {
    // einen vollen Stich nicht doppelt berücksichtigen
    return ;
  }
  karten_auf_tisch = text;
  auto const elements = Element::split_into_elements(text);
  for (size_t i = 0; i < elements.size(); ++i) {
    auto const& element = elements[i];
    if (element.tag != "karte" + std::to_string(i + 1)) {
      cerr << spieler << ": kartenauftisch: es kommt nicht der erwartete Tag „karte" << i << "“: " << text << '\n';
      return;
    }
    auto const subelements = Element::split_into_elements(element.content);
    GelegteKarte karte;
    for (auto const& element : subelements) {
      if (element.tag == "index") {
        karte.spielernummer = std::stou(element.content);
      } else if (element.tag == "kartennr") {
        karte.nummer = std::stoi(element.content);
      } else {
        cerr << spieler << ": Spieltisch: Karte gelegt: unbekannter Tag „" << element.tag << "“.\n";
      }
    }
    if (karte.spielernummer == 0 || karte.nummer == 0) {
      cerr << spieler << ": Spieltisch: Karte gelegt: index oder kartennr fehlt: " << element.content;
      return ;
    }
    if (gelegte_karten.size() % 4 == i) {
      gelegte_karten.push_back(karte);
      if (game->tricks().current().isfull()) {
        game->play(GameplayActions::TrickOpen());
      }
      adjust_trick_startplayer_if_required(karte.spielernummer - 1);
      auto const action = GameplayActions::CardPlayed(karte.spielernummer - 1, to_card(karte.nummer));
      DEBUG("gameplay") << spieler << ": " << action << '\n';
      game->play(action);
      if (karte.spielernummer == spielernummer()) {
        entferne_aus_blatt(karte.nummer);
      }
    }
  }
  if (!game->tricks().current().isfull()) {
    game->set_status(Game::Status::play);
  }
}


void Spieltisch::setze_startspieler_aus_karten_auf_tisch(string const& text)
{
  if (game && game->status() >= Game::Status::play)
    return ;

  auto const elements = Element::split_into_elements(text);
  if (elements.empty())
    return ;

  auto const& element = elements[0];
  if (element.tag != "karte1") {
    cerr << spieler << ": kartenauftisch: es kommt nicht der erwartete Tag „karte1“: " << text << '\n';
    return;
  }
  auto const subelements = Element::split_into_elements(element.content);
  for (auto const& element : subelements) {
    if (element.tag == "index") {
      setze_startspieler(std::stou(element.content));
      return ;
    }
  }
}


void Spieltisch::ersetze_unbekannte_karte_aus_karten_auf_tisch()
{
  if (!game)
    return ;
  if (game->status() < Game::Status::play)
    return ;

  auto const elements = Element::split_into_elements(karten_auf_tisch);
  for (auto const& element : elements) {
    auto const subelements = Element::split_into_elements(element.content);
    unsigned index = 0;
    Kartennummer karte = 0;
    Card card;
    for (auto const& element : subelements) {
      if (element.tag == "index") {
        index = stou(element.content);
      } else if (element.tag == "kartennr") {
        karte = std::stoi(element.content);;
        card = to_card(karte);
      } else {
        cerr << spieler << ": Spieltisch: Karte gelegt: unbekannter Tag „" << element.tag << "“.\n";
      }
    }
    if (index == spielernummer() && card) {
      //hand().unplaycard(Card::unknown);
      hand().set_known(card);
      auto const p = container_algorithm::find(blatt, 0);
      if (p != blatt.end())
        blatt.erase(p);
      blatt.push_back(karte);
    }
  }
}


void Spieltisch::spieler_ist_dran(Spielername const& name)
{
  if (name.empty())
    return ;
  if (!kenne_team && !kenne_startspieler) {
    auto const nummer = spielernummer(name);
    if (nummer == 0) {
      cerr << spieler << ": Spieler „" << name << "“ nicht gefunden.\n";
      cerr << "Kenne nur:\n";
      for (auto const& player : party->players())
        cerr << "  " << player.name() << '\n';
      return ;
    }
    setze_startspieler(spielernummer(name));
  }

  if (name != spieler)
    return ;

  if (!game)
    return ;

  if (game->status() < Game::Status::reservation) {
    DEBUG("refresh") << spieler << ": Spieltisch: Spieler ist dran: bin noch nicht beim Vorbehalt\n";
    socket->sende_refresh();
    return ;
  }

  if (game->status() >= Game::Status::finished) {
    return ;
  }

  if (karten.empty())
    return ;
  if (gelegte_karten.size() == rule()(Rule::Type::number_of_cards))
    return ;

  if (game->status() < Game::Status::play) {
    return ;
  }

  auto const karte = waehle_karte();
  socket->optional_sende_ansage();
  socket->sende_karte(karte);
}


void Spieltisch::setze_stich_vom_tisch(string const& text)
{
  if (!game)
    return ;
  if (game->status() != Game::Status::play)
    return;
  karten_auf_tisch.clear();
  auto const& trick = game->tricks().current();
  if (trick.isempty())
    return ;
  if (!trick.isfull()) {
    if (trick.contains(Card::unknown))
      return ;
    throw "Stich soll voll sein, ist es aber nicht\n";
  }
  auto const action = GameplayActions::TrickFull(trick);
  DEBUG("gameplay") << spieler << ": " << action << '\n';
  game->play(action);
  if (game->tricks().size() == game->tricks().max_size()) {
    game->set_status(Game::Status::finished);
  }
  if (trick.winnerplayer().name() != text) {
    cerr << spieler << ": Gewinner vom Stich passt nicht. Erwarte " << trick.winnerplayer().name() << ", es ist aber " << text << ":\n"
      << "  " << _(trick.card(0)) << "  (" << trick.player_of_card(0).name() << ")\n"
      << "  " << _(trick.card(1)) << "  (" << trick.player_of_card(1).name() << ")\n"
      << "  " << _(trick.card(2)) << "  (" << trick.player_of_card(2).name() << ")\n"
      << "  " << _(trick.card(3)) << "  (" << trick.player_of_card(3).name() << ")\n";
    socket->write_log("Unzulässiges Spiel: Gewinner vom Stich passt nicht. Erwarte " + trick.winnerplayer().name() + ", es ist aber " + text + ".");
    return ;
  }
  // Chat für den Stich
  if (trick.winnerplayer() == ai()) {
    if (::chat_keys.count("Fuchs")) {
      for (unsigned i = 0; i < trick.actcardno(); ++i) {
        auto const& p = trick.player_of_card(i);
        if (   trick.card(i).isfox()
            && p != ai()
            && is_real(game->teaminfo().get(ai()))
            && is_real(game->teaminfo().get(p))
            && ai().team() != p.team()
           ) {
          chatter.sage("Fuchs erhalten", p.name());
        }
      }
    }
    if (   ::chat_keys.count("Doppelkopf")
        && trick.points() >= 40) {
      chatter.sage("Doppelkopf");
    }
  }
}


void Spieltisch::endabrechnung(string text)
{
  DEBUG("Endabrechnung") << "Endabrechnung:\n" << text << '\n';
  if (!game)
    return ;
  if (text == "Niemand möchte die Armut mitnehmen.") {
    game->close();
    return ;
  }
  if (game->type() == GameType::normal) {
    for (Player& player : game->players()) {
      if (player.team() == Team::unknown) {
        player.set_team(Team::contra);
      }
    }
  }

  erstelle_automatische_fehlerberichte(GameSummary(*game));

  if (!auto_bug_report_keys.count("Abweichung in der Endabrechnung"))
    return ;

  String::replace_all(text, " \n", "\n");
  String::replace_all(text, "\n\n", "\n");
  String::replace_all(text, "\n\n", "\n");
  String::replace_all(text, "\n\n", "\n");
  // Korrekturen
  String::replace_all(text, "Punkt.\n", "Punkt\n");
  String::replace_all(text, "ist SCHWARZ gespielt worden=", "wurde SCHWARZ =");
  String::replace_all(text, "hat  keine 60", "hat keine 60");
  String::replace_all(text, "_ki ", " ");
  String::replace_all(text, "_ki:", ":");

  auto const text_freedoko = endabrechnung_text(*game);

  if (   text_freedoko != text
      && !springe_in_laufendes_spiel) {
    erstelle_fehlerbericht("Abweichung in der Endabrechnung\n"
                           "--- Doko-Lounge ---\n"
                           + text
                           + "\n"
                           "--- FreeDoko ---\n"
                           + text_freedoko);
#if 0
    CLOG << "Abweichung in der Endabrechnung\n"
      << GameSummary(*game)
      << "--- Doko-Lounge ---\n"
      << text
      << '\n'
      << "--- FreeDoko ---\n"
      << text_freedoko
      << '\n'
      << "---\n";
    exit(0);
#endif

  }
}

void Spieltisch::erstelle_automatische_fehlerberichte(GameSummary const& game_summary)
{

  if (   ::chat_keys.count("gw")
      && game_summary.winnerteam() != player().team()) {
    if (is_solo(game->type()) && player().team() == Team::contra)
      chatter.sage("Glückwunsch", game->players().soloplayer().name());
    else
      chatter.sage("Glückwunsch");
  } else if (   ::chat_keys.count("dp")
             && game_summary.winnerteam() == player().team()
             && !(   is_solo(game->type())
                  && player().team() == Team::re)) {
    auto const partnerno = game_summary.partner(player());
    if (partnerno == UINT_MAX)
      chatter.sage("Danke Partner");
    else
      chatter.sage("Danke Partner", game->players().player(partnerno).name());
  }

  if (::chat_keys.count("Witz")) {
    chatter.sage_mit_prefix("/witz", "<Witz bei Spielende>");
  }

  DEBUG("gameplay") << spieler << ": Spielergebnis:\n" << game_summary;
  socket->write_log_current_time();

  if (   auto_bug_report_keys.count("Verloren")
      && game_summary.winnerteam() != player().team()) {
    erstelle_fehlerbericht("Verloren");
    chatter.sage("Fehlerbericht: Verloren");
  } else if (   auto_bug_report_keys.count("Ansage verloren")
             && player().announcement()
             && game_summary.winnerteam() != player().team()) {
    erstelle_fehlerbericht("Ansage verloren");
    chatter.sage("Fehlerbericht: Ansage verloren");
  } else if (   auto_bug_report_keys.count("Ansage verloren mit < 90 Punkten")
             && player().announcement()
             && game_summary.winnerteam() != player().team()
             && game_summary.trick_points(player().team()) < 90
            ) {
    erstelle_fehlerbericht("Ansage verloren mit < 90 Punkten");
    chatter.sage("Fehlerbericht: Ansage verloren mit < 90 Punkten");
  } else if (   auto_bug_report_keys.count("Ansage verloren mit < 60 Punkten")
             && player().announcement()
             && game_summary.winnerteam() != player().team()
             && game_summary.trick_points(player().team()) < 60
            ) {
    erstelle_fehlerbericht("Ansage verloren mit < 60 Punkten");
    chatter.sage("Fehlerbericht: Ansage verloren mit < 60 Punkten");
  } else if (   auto_bug_report_keys.count("Ansage gegen Schweine verloren")
             && player().announcement()
             && game->swines().swines_announced()
             && game->swines().swines_owner().team() != player().team()
             && game_summary.winnerteam() != player().team()
             && schweine_vor_ansage(*game)) {
    erstelle_fehlerbericht("Ansage gegen Schweine verloren");
    chatter.sage("Fehlerbericht: Ansage gegen Schweine verloren");
  } else if (   auto_bug_report_keys.count("Hochzeit verloren")
             && game->type() == GameType::marriage
             && game->players().is_soloplayer(player())
             && game_summary.winnerteam() != Team::re) {
    erstelle_fehlerbericht("Hochzeit verloren");
    chatter.sage("Fehlerbericht: Hochzeit verloren");
  } else if (   auto_bug_report_keys.count("mitgenommene Armut verloren")
             && game->type() == GameType::poverty
             && player().team() == Team::re
             && !game->players().is_soloplayer(player())
             && game_summary.winnerteam() != Team::re) {
    erstelle_fehlerbericht("mitgenommene Armut verloren");
    chatter.sage("Fehlerbericht: mitgenommene Armut verloren");
  } else if (   auto_bug_report_keys.count("Solo verloren")
             && !springe_in_laufendes_spiel
             && is_solo(game->type())
             && game->players().is_soloplayer(player())
             && game_summary.winnerteam() != Team::re) {
    erstelle_fehlerbericht("Solo verloren");
    chatter.sage("Fehlerbericht: Solo verloren");
  } else if (   auto_bug_report_keys.count("Solo verloren mit < 90 Punkten")
             && !springe_in_laufendes_spiel
             && is_solo(game->type())
             && game->players().is_soloplayer(player())
             && game_summary.winnerteam() != Team::re
             && game_summary.trick_points(Team::re) < 90) {
    erstelle_fehlerbericht("Solo verloren mit < 90 Punkten");
    chatter.sage("Fehlerbericht: Solo verloren mit < 90 Punkten");
  } else if (   auto_bug_report_keys.count("Solo verloren mit < 60 Punkten")
             && !springe_in_laufendes_spiel
             && is_solo(game->type())
             && game->players().is_soloplayer(player())
             && game_summary.winnerteam() != Team::re
             && game_summary.trick_points(Team::re) < 60) {
    erstelle_fehlerbericht("Solo verloren mit < 60 Punkten");
    chatter.sage("Fehlerbericht: Solo verloren mit < 60 Punkten");
  }
}


void Spieltisch::debug(string const& text)
{
  if (text == "game status") {
    cout << "game status: " << (game ? _(game->status()) : _(party->status())) << '\n';
  } else if (text == "spieltisch") {
    cout << *this << '\n';
  } else if (text == "regeln" || text == "rules") {
    cout << _(Rule::Type::with_nines)                 << ": " << party->rule()(Rule::Type::with_nines)                    << '\n'
      << _(Rule::Type::poverty)                       << ": " << party->rule()(Rule::Type::poverty)                       << '\n'
      << _(Rule::Type::swines)                        << ": " << party->rule()(Rule::Type::swines)                        << '\n'
      << _(Rule::Type::hyperswines)                   << ": " << party->rule()(Rule::Type::hyperswines)                   << '\n'
      << _(Rule::Type::lustsolo_player_leads)         << ": " << party->rule()(Rule::Type::lustsolo_player_leads)         << '\n'
      << _(Rule::Type::dullen_second_over_first)      << ": " << party->rule()(Rule::Type::dullen_second_over_first)      << '\n'
      << _(Rule::Type::dullen_contrary_in_last_trick) << ": " << party->rule()(Rule::Type::dullen_contrary_in_last_trick) << '\n'
      << _(Rule::Type::throw_with_nines)              << ": " << party->rule()(Rule::Type::throw_with_nines)              << '\n'
      << _(Rule::Type::cowardliness_dokolounge)       << ": " << party->rule()(Rule::Type::cowardliness_dokolounge)       << '\n'
      ;
  } else if (text == "rule") {
    if (game)
      cout << game->rule();
  } else if (text == "status") {
    cout << "game status: " << (game ? _(game->status()) : _(party->status())) << '\n';
  } else if (text == "spiel" || text == "game") {
    if (game)
      cout << *game;
    else
      cout << "Kein Spiel\n";
  } else if (text == "player") {
    cout << static_cast<Player const&>(ai());
  } else if (text == "ai") {
    cout << static_cast<Player const&>(ai());
  } else if (text == "cards" || text == "cards information") {
    cout << ai().cards_information();
  } else if (text == "teams") {
    cout << ai().team_information();
  } else if (text == "team") {
    cout << "Team von " << ai().name() << " = " << ai().team() << "   (teaminfo: " << ai().teaminfo(ai()) << ")\n";
  } else if (text == "hand" || text == "Blatt") {
    cout << ai().hand();
  } else if (text == "stich" || text == "trick") {
    if (game)
      cout << game->tricks().current();
    else
      cout << "Kein Spiel\n";
  } else if (text == "playerno") {
    cout << "playerno: " << ai().no() << '\n';
  } else if (text == "heuristic" || text == "?") {
    cout << _(ai().last_heuristic()) << '\n'
      << ai().last_heuristic_rationale() << '\n';
  } else if (text == "announcement") {
    cout << _(ai().announcement()) << '\n'
      << ai().last_announcement_rationale() << '\n';
  }
}


void Spieltisch::warte_wegen_sprachausgabe(string const& text)
{
  if (::debug("ohne Pause"))
    return ;
  if (::preferences(Preferences::Type::announcement_window_close_delay) == 0)
    return ;

  auto const elements = Element::split_into_elements(text);
  if (!(elements.size() == 3
        && elements[0].tag == "SprechStimme"
        && elements[1].tag == "Spieler"
        && elements[2].tag == "Text")) {
    // Warte einfach die Standardzeit
    std::this_thread::sleep_for(::preferences(Preferences::Type::announcement_window_close_delay) * 1ms);
    return ;
  }

  string stimme = elements[0].content;
  if (stimme.empty())
    return ;
  stimme.front() = std::tolower(stimme.front());
  string const& sprachtext = elements[2].content;
  string const file = "sound/" + stimme + "/" + sprachtext + ".wav";

  static std::map<std::string, unsigned> durations;
  auto& duration_ms = durations[file];
  if (duration_ms == 0) {
    if (std::filesystem::is_regular_file(file)) {
      // Ermittle die Länge der wav-Datei mit soxi (im Paket sox).
      char buffer[16];
      string const cmd = "echo $(( $(soxi -s \"" + file + "\") * 1000 / $(soxi -r \"" + file + "\") ))";
      FILE* pipe = popen(cmd.c_str(), "r");
      if (pipe) {
        fgets(buffer, sizeof(buffer), pipe);
        pclose(pipe);
        buffer[sizeof(buffer)-1] = 0;
        try {
          duration_ms = std::stou(buffer);
        } catch (...) {
        }
      }
    }
  }
  duration_ms = 0;
  if (duration_ms == 0
      && std::filesystem::is_regular_file(file)) {
    // Schätze die Abspieldauer anhand der Dateigröße
    duration_ms = std::filesystem::file_size(file) / 44.100 / 2; // 44.100 Hz, 16 Bit
  }
  if (duration_ms == 0) {
    // Standardwert, kann als Option vorgegeben werden
    duration_ms = ::preferences(Preferences::Type::announcement_window_close_delay);
  }

  // Warte die Zeit plus noch kurze Pause.
  std::this_thread::sleep_for(duration_ms * 1ms + 200ms);
}


void Spieltisch::setze_startspieler(Spielernummer const nummer)
{
  if (game) {
    if (game->status() < Game::Status::init) {
      game->set_status(Game::Status::init);
    }
    DEBUG("gameplay") << spieler << ": Startspieler: " << nummer - 1 << '\n';
    game->set_startplayer(nummer - 1);
  } else {
    if (party->startplayer() != nummer - 1) {
      party->set_startplayer(nummer - 1);
    }
  }
  setze_kenne_startspieler();
}


void Spieltisch::setze_kenne_regeln()
{
  kenne_regeln = true;
}


void Spieltisch::setze_kenne_startspieler()
{
  kenne_startspieler = true;
  advance_game_if_possible();
}


void Spieltisch::setze_kenne_team()
{
  kenne_team = true;
  if (!game)
    return ;
  if (game->status() >= Game::Status::play) {
    if (team != ai().team()) {
      DEBUG("gameplay") << spieler << ": Team von Spieler " << ai().no() << ": " << team << '\n';
      ai().set_team(team);
    }
  }
}


auto Spieltisch::waehle_vorbehalt(bool pflichtsolo) -> Vorbehalt
{
  advance_game_if_possible();
  if (springe_in_laufendes_spiel)
    return {};
  if (!game || game->status() != Game::Status::reservation) {
    DEBUG("refresh") << spieler << ": Spieltisch: wähle Vorbehalt: kenne nicht alle Informationen.\n" << *this << '\n';
    socket->sende_refresh();
    return {};
  }

  if (pflichtsolo) {
    // Einmal ausgewählt kann es nicht abgewählt werden.
    // Grund ist, dass diese Funktion auch vom Chat getriggert wird, dann aber die Information fehlt, dass dies ein Pflichtsolo ist.
    game->set_is_duty_solo(pflichtsolo);
  }
  auto const reservation = get_reservation();

  if (reservation.game_type == GameType::poverty) {
    poverty_shifted_cards.clear();
    for (auto const& card : hand()) {
      if (card.istrump())
        poverty_shifted_cards.push_back(card);
    }
  }
  DEBUG("Meldung") << spieler << ": Meldung = " << to_dokolounge_name(reservation) << " -- " << reservation << '\n';
  return to_dokolounge_name(reservation);
}


auto Spieltisch::waehle_karte() -> Kartennummer
{
  if (in_invalid_game) {
    if (blatt.empty())
      return 0;
    if (blatt.size() > 1) {
      // Alternative: einfach alle Werte übergeben
      for (size_t i = 0; i < blatt.size(); ++i) {
        std::rotate(blatt.begin(), blatt.begin() + 1, blatt.end());
        if (blatt[0])
          break;
      }
    }
    ai().set_last_heuristic_to_manual();
    if (::chat_keys.count("KI") && offene_ki_informationen())
      socket->sage("Unzulässiges Spiel, wähle erste Karte");
    return blatt[0];
  }

  advance_game_if_possible();
  if (!game)
    return 0;
  if (game->status() < Game::Status::play) {
    DEBUG("refresh") << spieler << ": Spieltisch: wähle Karte: bin nicht im Spiel.\n" << *this << '\n';
    socket->sende_refresh();
    return 0;
  }

  if (   !karten_auf_tisch.empty()
      && buffer_gewaehlte_karte.karten_auf_tisch == karten_auf_tisch
      && buffer_gewaehlte_karte.karte != 0) {
    return buffer_gewaehlte_karte.karte;
  }

  auto const start_zeitpunkt = std::chrono::system_clock::now();
  auto const card = get_card();
  if (!card)
    return 0;
  auto const karte = karte_aus_blatt(card);
  DEBUG("Karte") << spieler << ": Karte = " << karte << "  " << card << '\n';
  if (::chat_keys.count("KI") && offene_ki_informationen())
    socket->sage(_(ai().last_heuristic()) + ":\n" + _(ai().last_heuristic_rationale()));
  buffer_gewaehlte_karte.karten_auf_tisch = karten_auf_tisch;
  buffer_gewaehlte_karte.karte = karte;
  if (!::debug("ohne Pause")) {
    std::this_thread::sleep_until(start_zeitpunkt + ::preferences(Preferences::Type::card_play_delay) * 1ms);
  }
  return karte;
}


auto Spieltisch::waehle_ansage() -> string
{
  if (in_invalid_game)
    return {};

  advance_game_if_possible();
  if (!game)
    return {};
  if (game->status() < Game::Status::play) {
    DEBUG("refresh") << spieler << ": Spieltisch: wähle Ansage: bin nicht im Spiel.\n" << *this << '\n';
    socket->sende_refresh();
    return {};
  }
  auto const announcement = get_announcement();
  if (announcement == Announcement::noannouncement)
    return {};
  auto const ansage = (ai().team() == Team::re ? "Re" : "Kontra") + " "s + to_dokolounge_name(announcement);
  DEBUG("Ansage") << spieler << ": Ansage = " << ansage << '\n';
  if (::chat_keys.count("KI") && offene_ki_informationen()) {
    socket->sage(ansage + ":\n" + _(ai().last_announcement_rationale()));
  }
  return ansage;
}


auto Spieltisch::waehle_armut_karten() -> string
{
  if (!game)
    return {};
  if (game->status() < Game::Status::reservation) {
    return {};
  }
  if (game->status() > Game::Status::poverty_shift) {
    return {};
  }
  game->set_status(Game::Status::poverty_shift);
  game->set_type(GameType::poverty);
  trumpf = "Armut";
  game->set_type(GameType::poverty);
  game->players().set_soloplayer(ai());

  poverty_shifted_cards = ai().poverty_shift();
  auto const action = GameplayActions::PovertyShift(ai(), poverty_shifted_cards);
  DEBUG("gameplay") << spieler << ": " << action << '\n';
  string karten;
  for (auto const card : poverty_shifted_cards) {
    auto const k = karte_aus_blatt(card);
    if (k < 10)
      karten += "0";
    karten += std::to_string(k);
    entferne_aus_blatt(k);
  }
  return karten;
}


auto Spieltisch::waehle_armut_annahme(string const text) -> string
{
  if (!game)
    return {};
  if (game->status() < Game::Status::reservation) {
    return {};
  }
  if (game->status() > Game::Status::poverty_shift) {
    return {};
  }
  game->set_status(Game::Status::poverty_shift);
  trumpf = "Armut";
  game->set_type(GameType::poverty);
  auto const p = text.find(" hat eine Armut.");
  if (p == string::npos)
    return {};
  auto const name = text.substr(0, p);
  auto const solospieler = spielernummer(name);
  if (solospieler == 0)
    return {};
  game->players().set_soloplayer(game->players().player(solospieler - 1));

  auto const annahme = get_poverty_take_accept();
  if (annahme) {
    auto const action = GameplayActions::PovertyAccepted(ai());
    DEBUG("gameplay") << spieler << ": " << action << '\n';
    return "Mitnehmen";
  } else {
    auto const action = GameplayActions::PovertyDenied(ai());
    DEBUG("gameplay") << spieler << ": " << action << '\n';
    return "Ablehnen";
  }
}


auto Spieltisch::wechsel_armut_karten(string text) -> string
{
  if (!game)
    return {};
  if (game->status() < Game::Status::poverty_shift) {
    return {};
  }

  game->set_type(GameType::poverty);
  game->set_status(Game::Status::poverty_shift);
  if (text.size() != 3 * 2)
    return {};
  poverty_shifted_cards.clear();
  poverty_shifted_cards.push_back(to_card(text.substr(0, 2)));
  poverty_shifted_cards.push_back(to_card(text.substr(2, 2)));
  poverty_shifted_cards.push_back(to_card(text.substr(4, 2)));
  blatt.push_back(to_kartennummer(text.substr(0, 2)));
  blatt.push_back(to_kartennummer(text.substr(2, 2)));
  blatt.push_back(to_kartennummer(text.substr(4, 2)));

  poverty_returned_cards = ai().poverty_cards_change(poverty_shifted_cards);
  auto const action = GameplayActions::PovertyReturned(ai(), poverty_returned_cards);
  DEBUG("gameplay") << spieler << ": " << action << '\n';
  string karten;
  for (auto const card : poverty_returned_cards) {
    auto const k = karte_aus_blatt(card);
    if (k < 10)
      karten += "0";
    karten += std::to_string(k);
    entferne_aus_blatt(k);
  }
  return karten;
}


auto Spieltisch::karte_aus_blatt(Card const card) -> Kartennummer
{
  for (auto const karte : blatt) {
    if (to_card(karte) == card)
      return karte;
  }
  cerr << spieler << ": Karte " << _(card) << " nicht im Blatt gefunden\n";
  for (auto const karte : blatt) {
    cerr << "  " << to_card(karte) << '\n';
  }
  cerr << "Blatt:\n" << hand();
  throw "Karte " + _(card) + " nicht im Blatt gefunden";
}


void Spieltisch::entferne_aus_blatt(Kartennummer karte)
{
  auto const p = find(blatt, karte);
  if (p == blatt.end()) {
    cerr << spieler << ": Karte „" << karte << "“ " << _(to_card(karte)) << " nicht im Blatt gefunden.\n";
    for (auto const c : blatt)
      cerr << "  " << setw(2) << c << "  " << _(to_card(c)) << '\n';
    throw "Karte „" + std::to_string(karte) + "“ " + _(to_card(karte)) + " nicht im Blatt gefunden.";
  }
  *p = 0;
}


auto Spieltisch::spielernummer(string name) const -> Spielernummer
{
  if (name.empty())
    name = "frei";
  for (unsigned i = 0; i < party->players().size(); ++i) {
    if (party->players().player(i).name() == name) {
      return i + 1;
    }
  }
  // Wenn der Spieler nicht bekannt ist ist vermutlich dieser FreeDoko für ihn eingesprungen.
  if (   name.substr(0, 8) == "FreeDoko"
      || name.front() == '#')
  {
    return 0;
  }
  for (unsigned i = 0; i < party->players().size(); ++i) {
    if (party->players().player(i).name() == spieler) {
      return i + 1;
    }
  }
  return 0;
}


auto Spieltisch::spielernummer() const -> Spielernummer
{
  return spielernummer(spieler);
}


auto Spieltisch::alle_spieler_freedoko() const -> bool
{
  for (Player const& player : party->players()) {
    auto const name = string_lc(player.name());
    if (!(   name.substr(0, 8) == "FreeDoko"
          || name.front() == '#'
          || name == "fd1"
          || name == "fd2"
          || name == "fd3"
          || name == "fd4")) {
      return false;
    }
  }
  return true;
}


auto Spieltisch::zaehle_menschen() const -> unsigned
{
  unsigned n = 0;
  for (Player const& player : party->players()) {
    auto const name = string_lc(player.name());
    if (!(   name.substr(0, 8) == "FreeDoko"
          || name.substr(0, 6) == "Dummy "
          || name.front() == '#'
          || name == "frei"
          || name == "empty")) {
      n += 1;
    }
  }
  return n;
}


auto Spieltisch::offenes_blatt() const -> bool
{
  for (Player const& player : party->players()) {
    auto const name = string_lc(player.name());
    if (!(   name.substr(0, 8) == "FreeDoko"
          || name.substr(0, 6) == "Dummy "
          || name.front() == '#'
          || name == "fd1"
          || name == "fd2"
          || name == "fd3"
          || name == "fd4"
          || name == "frei"
          || name == "empty"
          || name == "Diether"
          || name == "Stevie"
          || name == "Schnuppi911"
         )) {
      return false;
    }
  }

  return true;
}


auto Spieltisch::offene_ki_informationen() const -> bool
{
  return (zaehle_menschen() <= 1) || offenes_blatt();
}


auto Spieltisch::status_string() const -> string
{
  if (!game) {
    return "Warte auf Spielstart";
  }
  string text = _(game->status());
  if (   game->status() >= Game::Status::play
      && game->status() <= Game::Status::trick_taken) {
    text += "\n";
    text += "Stich " + std::to_string(game->tricks().current_no()) + "\n";
    text += _(game->tricks().current());
  }
  return text;
}

void Spieltisch::erstelle_fehlerbericht(string const text, string const ersteller) const
{
  auto const spieler = (this->spieler.substr(0, 9) == "FreeDoko#" ? this->spieler.substr(9) : this->spieler);
  if (!game)
    BugReport::create_from_dokolounge(ersteller, nummer, spieler,
                                      "Dummy: " + spieler + "\n"
                                      + version_string() + "\n"
                                      + "\n" + text);
  else
    BugReport::create_from_dokolounge(ersteller, *game, nummer, spieler,
                                      "Dummy: " + spieler + "\n"
                                      + version_string() + "\n"
                                      + "\n" + text);
}


auto Spieltisch::playerno() const -> unsigned
{
  return spielernummer(spieler) - 1;
}


auto Spieltisch::get_reservation() -> Reservation
{
  if (!game) {
    throw "Spieltisch::get_reservation(): kein Spiel-Objekt vorhanden"s;
  }

  if (game->status() > Game::Status::reservation) {
    if (gelegte_karten.empty())
      return {};
    cerr << spieler << ":\n";
    cerr << *game << '\n';
    throw "Spieltisch::get_reservation(): Spielstatus ist nach dem Vorbehalt: " + _(game->status());
  }

  game->set_status(Game::Status::reservation);

  auto const reservation = ai().get_reservation();
  return reservation;
}


auto Spieltisch::get_card() -> Card
{
  if (!game) {
    throw "Spieltisch::get_card(): kein Spiel-Objekt vorhanden"s;
  }

  if (game->status() >= Game::Status::finished) {
    cerr << spieler << ":\n";
    cerr << *game << '\n';
    throw "card requested, but game status = " + to_string(game->status()) + " >= finished";
  }

  auto& ai = this->ai();

  if (game->tricks().current().isfull()) {
    game->play(GameplayActions::TrickOpen());
    adjust_trick_startplayer_if_required(spielernummer() - 1);
  }
  auto const& trick = game->tricks().current();
  if (trick.actplayer() != ai) {
    cerr << spieler << ":\n";
    cerr << *game << '\n';
    cerr << "card requested, but the current player " << trick.actplayer().name() << " = " << trick.actplayer().no() << " is not " << ai.no() << '\n';
    throw "card requested, but the current player " + trick.actplayer().name() + " = " + std::to_string(trick.actplayer().no()) + " is not " + std::to_string(ai.no());
  }
  if (hand().empty()) {
    throw "card requested, but the hand is empty"s;
  }

  auto card = ai.card_get();

  if (!trick.isvalid(card)) {
    card = hand().validcards(trick)[0];
  }

  return card;
}


auto Spieltisch::get_announcement() -> Announcement
{
  if (!game)
    return {};

  if (game->status() == Game::Status::cards_distribution) {
    game->set_status(Game::Status::play);
  }
  if (game->status() < Game::Status::play) {
    throw "Spieltisch::get_announcement(): Spielstatus ist nicht im Spiel: " + _(game->status());
  }

  if (game->status() >= Game::Status::finished) {
    return {};
  }

  auto const announcement = ai().announcement_request();
  // Die FreeDokos sagen zu viel an, daher nur Re/Kontra ansagen, keine weiteren Absagen (keine 90, ...)
  if (!mit_absagen && announcement != Announcement::no120)
    return Announcement::noannouncement;
  if (game->type() == GameType::poverty && team == Team::contra)
    return Announcement::noannouncement;

  return announcement;
}


auto Spieltisch::get_poverty_take_accept() -> bool
{
  if (!game)
    return false;

  if (   game->status() == Game::Status::cards_distribution
      || game->status() == Game::Status::reservation) {
    game->set_status(Game::Status::poverty_shift);
  }
  if (spieler.substr(0, 2) == string_lc("fd"))
    return true;
  return ai().poverty_take_accept(3);
}


void Spieltisch::advance_game_if_possible()
{
  if (game && game->status() >= Game::Status::play)
    return ;

  if (!game && kenne_startspieler) {
    create_game();
  }
  if (!game)
    return ;

  if (   game->status() == Game::Status::cards_distribution
      && !karten.empty()) {
    set_hand();
  }
  if (game->status() <= Game::Status::cards_distribution)
    return ;

  if (   game->type() == GameType::poverty
      && !springe_in_laufendes_spiel) {
  }

  if (!spiel_gestartet)
    return ;

  if (trumpf.empty())
    return ;

  set_game_type();
  start_game();
}


void Spieltisch::create_game()
{
  party->set_status(Party::Status::init);
  game = &party->create_game();
  game->set_seed(0);
  game->set_status(Game::Status::init);
  game->init();
  game->set_status(Game::Status::cards_distribution);
  game->announcements().signal_announcement_made.connect(*this, &Spieltisch::announcement_made);

  ai().set_to_difficulty(difficulty);
}


void Spieltisch::set_hand()
{
  if (!game)
    return ;
  auto hands = Hands(rule()(Rule::Type::number_of_players_in_game));
  auto& hand = hands[playerno()];
  blatt = to_blatt(karten, rule()(Rule::Type::number_of_cards_on_hand));
  hand = to_hand(game->players().player(playerno()), blatt);
  DEBUG("gameplay") << spieler << ": Hand:\n" << hand;
  if (spieler_mit_sau != 0)
    set_swines(spieler_mit_sau - 1);
  if (spieler_mit_supersau != 0)
    set_hyperswines(spieler_mit_supersau - 1);
  game->players().set_current_player(game->startplayer());
  ai().cards_information().reset();
  game->distribute_cards(hands);
  if (game->type() == GameType::poverty) {
    set_cards_information_for_poverty();
  }
  if (hand.contains_unknown_or_played()) {
    springe_in_laufendes_spiel = true;
  }
  if (   game->type() == GameType::poverty
      && game->status() == Game::Status::start)
    return ;
  game->set_status(Game::Status::reservation);
}


void Spieltisch::set_cards_information_for_poverty()
{
  if (!game)
    return ;
  if (armut_mit_trumpf != -1) {
    auto& cards_information = ai().cards_information().of_player(game->players().soloplayer());
    if (armut_mit_trumpf == 0)
      cards_information.add_cannot_have(Card::trump);
    if (armut_mit_trumpf == 1)
      cards_information.add_must_have(Card::trump, 1);
  }
  if (ai() == game->players().soloplayer()) {
#ifdef POSTPONED
    // Gegen die Karten im eigenen Blatt abgleichen.
    // Dazu brauche ich am besten geschobene_karten mit den DokoLounge-Nummern
    // poverty_returned_cards ist nur gesetzt, wenn der Spieler der Aufnehmer ist.
    auto cards = poverty_shifted_cards;
    for (auto const card : poverty_returned_cards) {
      container_algorithm::remove(cards, card);
    }
    auto& cards_information = ai().cards_information().of_player(game->players().poverty_partner());
    cards_information.add_must_have(cards);
#endif
  } else if (   ai().team() == Team::re
             && !poverty_returned_cards.empty()) {
    auto& cards_information = ai().cards_information().of_player(game->players().soloplayer());
    cards_information.add_must_have(poverty_returned_cards);
    size_t trumpno = 0;
    for (auto const& card : poverty_returned_cards) {
      if (HandCard(ai().hand(), card).istrump())
        trumpno += 1;
    }
    cards_information.add_must_exactly_have(Card::trump, trumpno);
  } else if (ai().team() == Team::contra) {
    auto& cards_information = ai().cards_information().of_player(game->players().soloplayer());
    cards_information.add_can_have(Card::trump, 3);
  }
}


void Spieltisch::set_game_type()
{
  if (!game)
    return ;
  if (game->status() < Game::Status::reservation) {
    if (!springe_in_laufendes_spiel)
      return ;
    game->set_status(Game::Status::reservation);
  }
  if (   game->status() != Game::Status::reservation
      && game->status() != Game::Status::start
      && game->status() != Game::Status::poverty_shift) {
    throw "Game type given, but game status = " + to_string(game->status()) + " != game reservation";
  }

  auto const game_type = to_game_type(trumpf);
  DEBUG("gameplay") << spieler << ": Spieltyp: " << game_type << '\n';
  game->set_type(game_type);
  if (game_type == GameType::marriage) {
    game->marriage().set_selector(MarriageSelector::first_foreign);
  }
  if (   game_type == GameType::marriage
      || is_solo(game_type)) {
    DEBUG("gameplay") << spieler << ": Solospieler: " << solospieler - 1 << '\n';
    set_soloplayer(solospieler == 0      // Hoffentlich wird später der Solospieler richtig gesetzt.
                   ? (ai().no() + 1) % 4 // Eine Nummer, die nicht die KI ist.
                   : solospieler - 1);
  }
  game->players().set_current_player(game->startplayer());
  Player const* player = &game->startplayer();
  for (unsigned p = 0; p < game->players().size(); ++p) {
    Reservation reservation;
    if (game->players().is_soloplayer(*player)) {
      reservation.game_type = game_type;
      reservation.marriage_selector = MarriageSelector::first_foreign;
    }
    game->add(GameplayActions::Reservation(player->no(), reservation));
    player = &game->players().following(*player);
  }
}


void Spieltisch::start_game()
{
  if (!game)
    return ;
  game->set_status(Game::Status::start);
  game->teaminfo().set_at_gamestart();
  for (auto& player : game->players()) {
    player.game_start();
  }
  game->set_status(Game::Status::play);
  game->players().set_current_player(game->startplayer());
  if (game->type() == GameType::poverty)
    set_cards_information_for_poverty();
  game->play(GameplayActions::TrickOpen());

  if (!springe_in_laufendes_spiel) {
    if (game->swines().swines_announced()) {
      auto const p = game->swines().swines_owner().no();
      game->swines().swines_owner_ = nullptr;
      game->swines().swines_announced_ = false;
      set_swines(p);
    }
    if (game->swines().hyperswines_announced()) {
      auto const p = game->swines().hyperswines_owner().no();
      game->swines().hyperswines_owner_ = nullptr;
      game->swines().hyperswines_announced_ = false;
      set_hyperswines(p);
    }
    if (ai() == game->startplayer()) {
      socket->optional_sende_ansage();
      // gleich losspielen?
      //socket->sende_karte(waehle_karte());
    }
  }
  if (springe_in_laufendes_spiel) {
    // Unbekannte Karten ausspielen
    auto const index = "<<index>>" + std::to_string(spielernummer()) + "<</index>>";
    auto const karten_gespielt = (hand().count(Card::unknown)
                                  - (karten_auf_tisch.find(index) != string::npos ? 1 : 0));
    auto const startplayer = game->startplayer().no();
    for (size_t i = 0; i < 4 * karten_gespielt; ++i) {
      auto const action = GameplayActions::CardPlayed((startplayer + i) % 4, Card::unknown);
      DEBUG("gameplay") << spieler << ": " << action << '\n';
      game->play(action);
      if ((i + 1) % 4 == 0 && i + 1 < 4 * karten_gespielt) {
        auto const action = GameplayActions::TrickFull(game->tricks().current());
        DEBUG("gameplay") << spieler << ": " << action << '\n';
        // game->play(action); // Das ruft bereits Game auf, wenn die erste Karte des nächsten Stiches gespielt wird
      }
    }
    if (karten_gespielt) {
      auto const action = GameplayActions::TrickFull(game->tricks().current());
      DEBUG("gameplay") << spieler << ": " << action << '\n';
      game->play(action); // Das wird wohl nicht von Game aufgerufen
    }
    if (ai().team() == Team::unknown) {
      ai().set_team(Team::contra);
    }
    if (!karten_auf_tisch.empty()) {
      ersetze_unbekannte_karte_aus_karten_auf_tisch();
    }
    auto const karten_auf_tisch_bak = karten_auf_tisch;
    karten_auf_tisch.clear();
    setze_karten_auf_tisch(karten_auf_tisch_bak);
    ai().cards_information().set_hand(ai(), hand());
  }
}


void Spieltisch::set_soloplayer(unsigned playerno)
{
  if (!game)
    return ;
  if (game->status() >= Game::Status::play)
    return ;
  auto const status_bak = game->status();
  game->set_status(Game::Status::init);
  game->players().set_soloplayer(game->players().player(playerno));
  if (::is_solo(game->type()) && rule()(Rule::Type::lustsolo_player_leads))
    game->set_startplayer(playerno);
  game->set_status(status_bak);
}


void Spieltisch::set_swines(unsigned const playerno)
{
  if (!game)
    return ;
  if (game->status() < Game::Status::reservation)
    return ;
  if (game->swines().swines_announced())
    return ;
  auto& player = game->players().player(playerno);
  if (player != ai() && !springe_in_laufendes_spiel) {
    auto& hand = player.hand();
    auto const swine = game->cards().swine();
    if (!hand.contains(swine)) {
      hand.set_known(swine, 2);
    }
  }
  auto const action = GameplayActions::Swines(player);
  DEBUG("gameplay") << spieler << ": " << action << '\n';
  if (springe_in_laufendes_spiel || game->status() < Game::Status::play) {
    game->swines().swines_owner_ = &player;
    game->swines().swines_announced_ = true;
    game->add(action);
  } else {
    advance_game_if_possible();
    game->play(action);
  }
}


void Spieltisch::set_hyperswines(unsigned const playerno)
{
  if (!game)
    return ;
  if (game->status() < Game::Status::reservation)
    return ;
  if (game->swines().hyperswines_announced())
    return ;
  auto& player = game->players().player(playerno);
  if (player != ai() && !springe_in_laufendes_spiel) {
    auto& hand = player.hand();
    auto const hyperswine = game->cards().hyperswine();
    if (!hand.contains(hyperswine)) {
      hand.set_known(hyperswine, 2);
    }
  }
  auto const action = GameplayActions::Hyperswines(player);
  DEBUG("gameplay") << spieler << ": " << action << '\n';
  if (springe_in_laufendes_spiel || game->status() < Game::Status::play) {
    game->swines().hyperswines_owner_ = &player;
    game->swines().hyperswines_announced_ = true;
    game->add(action);
  } else {
    game->play(action);
  }
}


void Spieltisch::set_announcement(unsigned const playerno, Team const team, Announcement const announcement)
{
  if (!game)
    return ;
  if (game->status() < Game::Status::play)
    return ;
  auto& player = game->players().player(playerno);
  if (player.team() == Team::unknown) {
    player.set_team(team);
  }

  auto const action = GameplayActions::Announcement(playerno, announcement);
  DEBUG("gameplay") << spieler << ": " << action << '\n';
  game->play(action);
}

void Spieltisch::announcement_made(Announcement announcement, Player const& player)
{
  if (player != ai())
    return;
  auto const ansage = (ai().team() == Team::re ? "Re" : "Kontra") + " "s + to_dokolounge_name(announcement);
  if (::chat_keys.count("KI") && offene_ki_informationen()) {
    socket->sage(ansage + ":\n" + _(ai().last_announcement_rationale()));
  }
  socket->sende_ansage(ansage);
}

void Spieltisch::adjust_trick_startplayer_if_required(unsigned playerno)
{
  if (!game)
    return ;
  auto& trick = game->tricks().current();
  if (!trick.isempty())
    return ;
  if (trick.startplayerno() == playerno)
    return ;
  cerr << spieler << ": Startspieler vom Stich passt nicht. Erwarte " << trick.startplayerno() + 1 << ", es ist aber " << playerno + 1 << " " << game->player(playerno).name() << ". Ändere den Startspieler auf " + std::to_string(playerno + 1) + " " + game->player(playerno).name() + ".\n";
  socket->write_log("Unzulässiges Spiel: Startspieler vom Stich passt nicht. Erwarte " + std::to_string(trick.startplayerno() + 1) + ", es ist aber " + std::to_string(playerno + 1) + ". Ändere den Startspieler auf " + std::to_string(playerno + 1) + " " + game->player(playerno).name() + ".");
  trick.set_startplayer(playerno);
  game->players().set_current_player(trick.startplayer());
}


auto Spieltisch::team_of_player() const -> Team
{
  if (is_real(team))
    return team;
  return hand().contains(Card::club_queen) ? Team::re : Team::contra;
}


auto Spieltisch::rule() const -> Rule const&
{
  return party->rule();
}


auto Spieltisch::rule() -> Rule&
{
  return party->rule();
}


auto Spieltisch::player() const -> Player const&
{
  DEBUG_ASSERTION(game,
                  "Spieltisch::player()\n"
                  "   game is not set");
  return game->players().player(playerno());
}


auto Spieltisch::ai() const -> Ai const&
{
  DEBUG_ASSERTION(game,
                  "Spieltisch::ai()\n"
                  "   game is not set");
  return dynamic_cast<Ai const&>(game->players().player(playerno()));
}


auto Spieltisch::ai() -> Ai&
{
  DEBUG_ASSERTION(game,
                  "Spieltisch::ai()\n"
                  "   game is not set");
  return dynamic_cast<Ai&>(game->players().player(playerno()));
}


auto Spieltisch::hand() const -> Hand const&
{
  return ai().hand();
}


auto Spieltisch::hand() -> Hand&
{
  return ai().hand();
}

auto operator<<(ostream& ostr, Spieltisch const& tisch) -> ostream&
{
  ostr << "Kenne Regeln:       " << tisch.kenne_regeln         << '\n';
  ostr << "Kenne Startspieler: " << tisch.kenne_startspieler   << '\n';
  ostr << "Kenne Team:         " << tisch.kenne_team           << '\n';
  ostr << "Spielstatus:        " << (party->in_game() ? _(party->game().status()) : _(party->status())) << '\n';
  ostr << "KI-Schwierigkeit:   " << _(tisch.ai().difficulty()) << '\n';

  if (tisch.game) {
    ostr << '\n';
    //if (tisch.kenne_regeln)
    //ostr << tisch.rule();
    if (tisch.kenne_startspieler)
      ostr << "Startspieler: " << tisch.game->startplayer().name() << '\n';
    if (tisch.kenne_team)
      ostr << "Team:         " << tisch.ai().team()                << '\n';
    ostr << "Solospieler:  " <<  tisch.solospieler            << '\n';
    ostr << "Partner:      " <<  tisch.partner_hochzeit_armut << '\n';
    ostr << "Sau:          " <<  tisch.spieler_mit_sau        << '\n';
    ostr << "Supersau:     " <<  tisch.spieler_mit_supersau   << '\n';
  }

  return ostr;
}

} // namespace DokoLounge

namespace {
auto schweine_vor_ansage(Game const& game) -> bool
{
  for (auto const& action : game.gameplay().actions()) {
    if (action->type() == GameplayActions::Type::swines)
      return true;
    else if (action->type() == GameplayActions::Type::announcement)
      return false;
  }
  return false;
}


auto endabrechnung_text(Game const& game) -> string
{
  vector<string> textbausteine;
  auto const summary = GameSummary(game);

  enum class GewinnText {
    keiner_hat_gewonnen,
    re_hat_gewonnen,
    re_hat_verloren,
    kontra_hat_gewonnen,
    kontra_hat_verloren
  };
  GewinnText gewinntext = GewinnText::keiner_hat_gewonnen;
  { // Gewinner
    string re_spieler;
    string kontra_spieler;
    unsigned n = 0;
    for (auto p : game.players()) {
      if (p.team() == Team::re) {
        if (!re_spieler.empty())
          re_spieler += " und ";
        re_spieler += p.name();
        n += 1;
      } else {
        if (!kontra_spieler.empty())
          kontra_spieler += " und ";
        kontra_spieler += p.name();
      }
    }

    if (summary.winnerteam() == Team::noteam) {
      gewinntext = GewinnText::keiner_hat_gewonnen;
      textbausteine.emplace_back("Re hat " + std::to_string(summary.trick_points(Team::re)) + " Augen.");
      if (summary.points() >= 0) {
        if (summary.is_solo()) {
          textbausteine.emplace_back("Re bekommt " + std::to_string(3 * summary.points()) + " Punkte:");
        } else {
          textbausteine.emplace_back("Re bekommt " + std::to_string(summary.points()) + (summary.points() == 1 ? " Punkt:" : " Punkte:"));
        }
      } else {
        textbausteine.emplace_back("Kontra bekommt " + std::to_string(-summary.points()) + (-summary.points() == 1 ? " Punkt:" : " Punkte:"));
      }
    } else if (summary.winnerteam() == Team::re) {
      if (   summary.highest_announcement_re() != Announcement::noannouncement
          && summary.highest_announcement_re() != Announcement::reply) {
        gewinntext = GewinnText::re_hat_gewonnen;
        textbausteine.emplace_back(re_spieler
                                   + (n == 1 ? " hat" : " haben")
                                   + " das Spiel mit " + std::to_string(summary.trick_points(Team::re)) + " Augen gewonnen.");
        if (summary.is_solo()) {
          textbausteine.emplace_back("Re bekommt " + std::to_string(3 * summary.points()) + " Punkte:");
        } else {
          textbausteine.emplace_back("Re bekommt " + std::to_string(summary.points()) + (std::abs(summary.points()) == 1 ? " Punkt:" : " Punkte:"));
        }
      } else if (   summary.highest_announcement_contra() != Announcement::noannouncement
                 && summary.highest_announcement_contra() != Announcement::reply) {
        gewinntext = GewinnText::kontra_hat_verloren;
        textbausteine.emplace_back(kontra_spieler + " haben das Spiel mit " + std::to_string(summary.trick_points(Team::contra)) + " Augen verloren.");
        textbausteine.emplace_back("Kontra verliert " + std::to_string(summary.points()) + (std::abs(summary.points()) == 1 ? " Punkt:" : " Punkte:"));
      } else {
        gewinntext = GewinnText::re_hat_gewonnen;
        textbausteine.emplace_back(re_spieler
                                   + (n == 1 ? " hat" : " haben")
                                   + " das Spiel mit " + std::to_string(summary.trick_points(Team::re)) + " Augen gewonnen.");
        if (summary.is_solo()) {
          textbausteine.emplace_back("Re bekommt " + std::to_string(3 * summary.points()) + " Punkte:");
        } else {
          textbausteine.emplace_back("Re bekommt " + std::to_string(summary.points()) + (std::abs(summary.points()) == 1 ? " Punkt:" : " Punkte:"));
        }
      }
    } else { // Team::contra
      if (   summary.highest_announcement_contra() != Announcement::noannouncement
          && summary.highest_announcement_contra() != Announcement::reply) {
        gewinntext = GewinnText::kontra_hat_gewonnen;
        textbausteine.emplace_back(kontra_spieler + " haben das Spiel mit " + std::to_string(summary.trick_points(Team::contra)) + " Augen gewonnen.");
        textbausteine.emplace_back("Kontra bekommt " + std::to_string(summary.points()) + (std::abs(summary.points()) == 1 ? " Punkt:" : " Punkte:"));
      } else if (   summary.highest_announcement_re() != Announcement::noannouncement
                 && summary.highest_announcement_re() != Announcement::reply) {
        gewinntext = GewinnText::re_hat_verloren;
        textbausteine.emplace_back(re_spieler
                                   + (n == 1 ? " hat" : " haben")
                                   + " das Spiel mit " + std::to_string(summary.trick_points(Team::re)) + " Augen verloren.");
        if (summary.is_solo()) {
          textbausteine.emplace_back("Re verliert " + std::to_string(3 * summary.points()) + " Punkte:");
        } else {
          textbausteine.emplace_back("Re verliert " + std::to_string(summary.points()) + (std::abs(summary.points()) == 1 ? " Punkt:" : " Punkte:"));
        }
      } else {
        gewinntext = GewinnText::kontra_hat_gewonnen;
        textbausteine.emplace_back(kontra_spieler + " haben das Spiel mit " + std::to_string(summary.trick_points(Team::contra)) + " Augen gewonnen.");
        textbausteine.emplace_back("Kontra bekommt " + std::to_string(summary.points()) + (std::abs(summary.points()) == 1 ? " Punkt:" : " Punkte:"));
      }
    }
  } // Gewinner

  auto specialpoints = summary.specialpoints();
  auto dokolounge_order_type = [](Specialpoint::Type const type) -> int {
    switch (type) {
    case Specialpoint::Type::nospecialpoint:
      return 0;
    case Specialpoint::Type::won:
      return 1;
    case Specialpoint::Type::no90:
      return 10;
    case Specialpoint::Type::no60:
      return 11;
    case Specialpoint::Type::no30:
      return 12;
    case Specialpoint::Type::no0:
      return 13;
    case Specialpoint::Type::announcement_reply:
    case Specialpoint::Type::no120_said:
      return 20;
    case Specialpoint::Type::no90_said:
      return 21;
    case Specialpoint::Type::no60_said:
      return 22;
    case Specialpoint::Type::no30_said:
      return 23;
    case Specialpoint::Type::no0_said:
      return 24;
    case Specialpoint::Type::no90_said_120_got:
      return 30;
    case Specialpoint::Type::no60_said_90_got:
      return 31;
    case Specialpoint::Type::no30_said_60_got:
      return 32;
    case Specialpoint::Type::no0_said_30_got:
      return 33;
    case Specialpoint::Type::solo:
      return 50;
    case Specialpoint::Type::contra_won:
      return 60;
    case Specialpoint::Type::doppelkopf:
      return 70;
    case Specialpoint::Type::caught_fox:
      return 71;
    case Specialpoint::Type::charlie:
      return 72;
    case Specialpoint::Type::caught_charlie:
      return 73;
      // ungenutzt
    case Specialpoint::Type::dulle_caught_dulle:
    case Specialpoint::Type::heart_trick:
    case Specialpoint::Type::caught_fox_last_trick:
    case Specialpoint::Type::fox_last_trick:
    case Specialpoint::Type::bock:
      return 99;
    }
    return 99;
  };
  auto order_dokolounge = [&dokolounge_order_type](Specialpoint const& lhs, Specialpoint const& rhs) -> bool {
    if (lhs.type == rhs.type) {
      return (rhs.team == Team::contra);
    }
    return (dokolounge_order_type(lhs.type) < dokolounge_order_type(rhs.type));
  };
  sort(specialpoints, order_dokolounge);
  if (   !summary.highest_announcement_re()
      && !summary.highest_announcement_contra())
    textbausteine.emplace_back("Es wurde keine Ansage gemacht.");
  auto const winnerteam = game.winnerteam();
  Specialpoint const* voriger_sonderpunkt = nullptr;
  int anzahl_sonderpunkt = 0;
  for (auto const& s : specialpoints) {
    string const teamname       = (s.team == Team::re ? "Re" : "Kontra");
    string const gegenteamname  = (s.team == Team::re ? "Kontra" : "Re");
    string const teamname_gross = (s.team == Team::re ? "RE" : "KONTRA");
    if (  !voriger_sonderpunkt
        || s.type != voriger_sonderpunkt->type
        || s.counting_team != voriger_sonderpunkt->counting_team)
      anzahl_sonderpunkt = 0;
    anzahl_sonderpunkt += 1;
    auto const points = s.value * (s.counting_team == winnerteam ? 1 : -1);
    switch (s.type) {
    case Specialpoint::Type::nospecialpoint:
      break;
    case Specialpoint::Type::caught_fox:
      if (anzahl_sonderpunkt > 1) {
        textbausteine.back() = (teamname + " hat " + std::to_string(anzahl_sonderpunkt) + " x Fuchs gefangen = "
                                + std::to_string(anzahl_sonderpunkt * s.value * (s.counting_team == winnerteam ? 1 : -1)) + " Punkte");
      } else {
        textbausteine.emplace_back(teamname + " hat 1 x Fuchs gefangen = " + std::to_string(points) + " Punkt");
      }
      break;
    case Specialpoint::Type::caught_fox_last_trick:
      break;
    case Specialpoint::Type::fox_last_trick:
      break;
    case Specialpoint::Type::charlie:
      textbausteine.emplace_back(teamname + " hat Karlchen gemacht = " + std::to_string(points) + " Punkt");
      break;
    case Specialpoint::Type::caught_charlie:
      textbausteine.emplace_back(teamname + " hat einen Karlchen gefangen = " + std::to_string(points) + " Punkt");
      break;
    case Specialpoint::Type::dulle_caught_dulle:
      break;
    case Specialpoint::Type::heart_trick:
      break;
    case Specialpoint::Type::doppelkopf:
      if (anzahl_sonderpunkt > 1) {
        textbausteine.back() = (teamname + " hat " + std::to_string(anzahl_sonderpunkt) + " x Doppelkopf gemacht = "
                                + std::to_string(anzahl_sonderpunkt * s.value * (s.counting_team == winnerteam ? 1 : -1)) + " Punkte");
      } else {
        textbausteine.emplace_back(teamname + " hat 1 x Doppelkopf gemacht = " + std::to_string(points) + " Punkt");
      }
      break;
    case Specialpoint::Type::won:
      switch (gewinntext) {
      case GewinnText::keiner_hat_gewonnen:
        break;
      case GewinnText::re_hat_gewonnen:
      case GewinnText::kontra_hat_verloren:
        textbausteine.emplace_back("Re hat gewonnen = 1 Punkt");
        break;
      case GewinnText::re_hat_verloren:
      case GewinnText::kontra_hat_gewonnen:
        textbausteine.emplace_back("Kontra hat gewonnen = 1 Punkt");
        break;
      }
      break;
    case Specialpoint::Type::no90:
      textbausteine.emplace_back(gegenteamname + " hat keine 90 = 1 Punkt");
      break;
    case Specialpoint::Type::no60:
      textbausteine.emplace_back(gegenteamname + " hat keine 60 = 1 Punkt");
      break;
    case Specialpoint::Type::no30:
      textbausteine.emplace_back(gegenteamname + " hat keine 30 = 1 Punkt");
      break;
    case Specialpoint::Type::no0:
      textbausteine.emplace_back(gegenteamname + " wurde SCHWARZ = 1 Punkt");
      break;
    case Specialpoint::Type::announcement_reply:
    case Specialpoint::Type::no120_said:
      textbausteine.emplace_back(teamname + " hat <" + teamname_gross + "> angesagt = 2 Punkte");
      break;
    case Specialpoint::Type::no90_said:
      textbausteine.emplace_back(teamname + " hat <keine 90> angesagt = 1 Punkt");
      break;
    case Specialpoint::Type::no60_said:
      textbausteine.emplace_back(teamname + " hat <keine 60> angesagt = 1 Punkt");
      break;
    case Specialpoint::Type::no30_said:
      textbausteine.emplace_back(teamname + " hat <keine 30> angesagt = 1 Punkt");
      break;
    case Specialpoint::Type::no0_said:
      textbausteine.emplace_back(teamname + " hat <SCHWARZ> angesagt = 1 Punkt");
      break;
    case Specialpoint::Type::no90_said_120_got:
      textbausteine.emplace_back(teamname + " hat 120 gegen <keine 90> erreicht = 1 Punkt");
      break;
    case Specialpoint::Type::no60_said_90_got:
      textbausteine.emplace_back(teamname + " hat 90 gegen <keine 60> erreicht = 1 Punkt");
      break;
    case Specialpoint::Type::no30_said_60_got:
      textbausteine.emplace_back(teamname + " hat 60 gegen <keine 30> erreicht = 1 Punkt");
      break;
    case Specialpoint::Type::no0_said_30_got:
      textbausteine.emplace_back(teamname + " hat 30 gegen <SCHWARZ> erreicht = 1 Punkt");
      break;
    case Specialpoint::Type::contra_won:
      textbausteine.emplace_back("Gegen die Alten = 1 Punkt");
      break;
    case Specialpoint::Type::solo:
      // ungenutzt
    case Specialpoint::Type::bock:
      break;
    }
    voriger_sonderpunkt = &s;
  }

  for (auto const& p : game.players())
    textbausteine.emplace_back(p.name() + ": " + std::to_string(summary.points(p)) + "");

  string text;
  for (auto const& t : textbausteine) {
    text += t;
    text += "\n";
  }

  String::replace_all(text, "_ki ", " ");
  String::replace_all(text, "_ki:", ":");

  return text;
}

} // namespace

#endif
