/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "element.h"
#include "chatter.h"

#include "../party/party.h"
#include "../party/rule.h"
#include "../game/game.h"
#include "../game/game_summary.h"
#include "../player/aiconfig_type.h"
class Ai;

namespace DokoLounge {
namespace WebSockets {
class Client;
} // namespace WebSockets

class Spieltisch {
  using Cards = vector<Card>;
  friend class Chatter;
  friend class Client;
  friend auto operator<<(ostream& ostr, Spieltisch const& tisch) -> ostream&;

  struct GelegteKarte {
    unsigned spielernummer = 0;
    Kartennummer nummer;
  };
  struct BufferGewaehlteKarte {
    string karten_auf_tisch;
    Kartennummer karte = 0;
  };
  struct Ansage {
    Spielername spieler;
    Team team = Team::unknown;
    Announcement announcement = Announcement::noannouncement;
  };
  using Art = string;

public:
  Spieltisch() = delete;
  Spieltisch(WebSockets::Client& socket, Spielername spieler, Tischnummer nummer);
  Spieltisch(Spieltisch const&) = delete;
  Spieltisch& operator=(Spieltisch const&) = delete;
  ~Spieltisch();

  operator bool() const;

  void clean();

  void chat(string text);

  void karten_legen(string const& text);

  void setze_ki_schwierigkeit(string const& text);
  void setze_regeln(string const& text);
  void setze_spieler(string const& text);
  void setze_knecht(string const& text);
  void setze_karten(string const& text);
  void setze_trumpf(string const& text);
  void setze_team(string const& text);
  void setze_ist_re(string const& text);
  void setze_ansage(string const& text);
  void setze_solo(string const& text);
  void setze_hochzeit(Spielername const& braut);
  void setze_hochzeitspartner(Spielername const& braeutigam);
  void setze_armut_abgelehnt();
  void setze_armut_team(string const& text);
  void setze_sau(Spielername const& spieler);
  void setze_supersau(Spielername const& spieler);
  void setze_spiel_laeuft();
  void setze_karten_auf_tisch(string const& text);
  void setze_startspieler_aus_karten_auf_tisch(string const& text);
  void ersetze_unbekannte_karte_aus_karten_auf_tisch();
  void spieler_ist_dran(Spielername const& name);
  void setze_stich_vom_tisch(string const& text);
  void endabrechnung(string text);
  void erstelle_automatische_fehlerberichte(GameSummary const& game_summary);
  void debug(string const& text);
  void warte_wegen_sprachausgabe(string const& text);

  void setze_startspieler(Spielernummer nummer);
  void setze_kenne_spieler();
  void setze_kenne_regeln();
  void setze_kenne_startspieler();
  void setze_kenne_trumpf();
  void setze_kenne_team();
  void setze_kenne_karten();

  auto waehle_vorbehalt(bool pflichtsolo = false) -> string;
  auto waehle_karte()                     -> Kartennummer;
  auto waehle_ansage()                    -> string;
  auto waehle_armut_karten()              -> string;
  auto waehle_armut_annahme(string text)  -> string;
  auto wechsel_armut_karten(string text)  -> string;

  auto karte_aus_blatt(Card card) -> Kartennummer;
  void entferne_aus_blatt(Kartennummer karte);

  auto spielernummer(string name) const -> Spielernummer;
  auto spielernummer()            const -> Spielernummer;
  auto alle_spieler_freedoko()    const -> bool;

  auto zaehle_menschen()          const -> unsigned;
  auto offenes_blatt()            const -> bool;
  auto offene_ki_informationen()  const -> bool;

  auto status_string()            const -> string;

  void erstelle_fehlerbericht(string text, string ersteller = "")    const;

private:
  // FreeDoko-Schnittstelle
  auto get_reservation()  -> Reservation;
  auto get_card()         -> Card;
  auto get_announcement() -> Announcement;
  auto get_poverty_take_accept() -> bool;
  void advance_game_if_possible();
  void create_game();
  void set_hand();
  void set_cards_information_for_poverty();
  void set_game_type();
  void start_game();
  void set_startplayer(unsigned startplayer);
  void set_soloplayer(unsigned playerno);
  void set_swines(unsigned playerno);
  void set_hyperswines(unsigned playerno);
  void set_announcement(unsigned playerno, Team team, Announcement announcement);
  void announcement_made(Announcement announcement, Player const& player);
  void adjust_trick_startplayer_if_required(unsigned playerno);
  auto playerno()                        const -> unsigned;
  auto team_of_player()                  const -> Team;
  auto rule()                            const -> Rule const&;
  auto rule()                                  -> Rule&;
  auto player()                          const -> Player const&;
  auto ai()                              const -> Ai const&;
  auto ai()                                    -> Ai&;
  auto hand()                            const -> Hand const&;
  auto hand()                                  -> Hand&;

private:
  WebSockets::Client* socket = nullptr;
  Chatter chatter;

public:
  Tischnummer nummer                   = 0;
  Spielername spieler;
  Knecht knecht;
  Trumpf trumpf;
  Spielernummer solospieler            = 0;
  Spielernummer partner_hochzeit_armut = 0;
  Spielernummer spieler_mit_sau        = 0;
  Spielernummer spieler_mit_supersau   = 0;
  Blatt blatt;
  Team team                            = Team::noteam;
  vector<GelegteKarte> gelegte_karten;
  string karten;
  string karten_auf_tisch;
  BufferGewaehlteKarte buffer_gewaehlte_karte;
  Cards poverty_shifted_cards;
  Cards poverty_returned_cards;

  bool kenne_regeln       = false;
  bool kenne_startspieler = false;
  bool kenne_team         = false;
  bool springe_in_laufendes_spiel = false;
  bool meldung_gesendet   = false; // wird in Client::sende_meldung auf true gesetzt
  bool spiel_gestartet    = false;
  int  armut_mit_trumpf   = -1;

  bool in_invalid_game = false;

  bool mit_absagen = true;

private:
  Game* game = nullptr;
  AiconfigDifficulty difficulty = AiconfigDifficulty::standard;
};

auto operator<<(ostream& ostr, Spieltisch const& tisch) -> ostream&;
} // namespace DokoLounge

#endif
