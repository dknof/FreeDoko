/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

#include "string_lc.h"

namespace DokoLounge {


string_lc::string_lc() = default;


string_lc::string_lc(string const& text_) :
  text(text_)
{ }


string_lc::string_lc(string_view const& text_) :
  text(text_)
{ }


string_lc::string_lc(char const* const text_) :
  text(text_)
{ }


string_lc::string_lc(string_lc const&) = default;


auto string_lc::operator=(string_lc const&) -> string_lc& = default;


auto string_lc::operator=(string const& text_) -> string_lc& 
{
  text = text_;
  return *this;
}


auto string_lc::operator=(string_view const text_) -> string_lc& 
{
  text = text_;
  return *this;
}


auto string_lc::operator=(char const* text_) -> string_lc& 
{
  text = text_;
  return *this;
}


string_lc::operator string const&() const
{
  return text;
}


auto string_lc::empty() const -> bool
{
  return text.empty();
}


auto string_lc::size() const -> size_t
{
  return text.size();
}


void string_lc::clear()
{
  text.clear();
}

auto string_lc::front() const -> char
{
  return text.front();
}

auto string_lc::substr(size_type pos, size_type count) const -> string_lc
{
  return string_lc(text.substr(pos, count));
}


auto operator==(string_lc const& lhs, string_lc const& rhs) -> bool
{
  return tolower(lhs.text) == tolower(rhs.text);
}

auto operator==(string_lc const& lhs, string const& rhs) -> bool
{
  return lhs == string_lc(rhs);
}

auto operator==(string_lc const& lhs, string_view const  rhs) -> bool
{
  return lhs == string_lc(rhs);
}

auto operator==(string_lc const& lhs, char const* rhs) -> bool
{
  return lhs == string_lc(rhs);
}

auto operator==(string const& lhs, string_lc const& rhs) -> bool
{
  return string_lc(lhs) == rhs;
}

auto operator==(string_view const  lhs, string_lc const& rhs) -> bool
{
  return string_lc(lhs) == rhs;
}

auto operator==(char const* lhs, string_lc const& rhs) -> bool
{
  return string_lc(lhs) == rhs;
}

auto operator!=(string_lc const& lhs, string_lc const& rhs) -> bool
{
  return !(lhs == rhs);
}

auto operator!=(string_lc const& lhs, string const& rhs) -> bool
{
  return !(lhs == rhs);
}

auto operator!=(string_lc const& lhs, string_view const  rhs) -> bool
{
  return !(lhs == rhs);
}

auto operator!=(string_lc const& lhs, char const* rhs) -> bool
{
  return !(lhs == rhs);
}

auto operator!=(string const& lhs, string_lc const& rhs) -> bool
{
  return !(lhs == rhs);
}

auto operator!=(string_view const  lhs, string_lc const& rhs) -> bool
{
  return !(lhs == rhs);
}

auto operator!=(char const* lhs, string_lc const& rhs) -> bool
{
  return !(lhs == rhs);
}


auto operator+(string_lc   const& lhs, string_lc   const& rhs) -> string_lc
{
  return string_lc(lhs.text + rhs.text);
}

auto operator+(string_lc   const& lhs, string      const& rhs) -> string_lc
{
  return string_lc(lhs.text + rhs);
}

auto operator+(string_lc   const& lhs, string_view const& rhs) -> string_lc
{
  return string_lc(lhs.text + string(rhs));
}

auto operator+(string_lc   const& lhs, char        const* rhs) -> string_lc
{
  return string_lc(lhs.text + rhs);
}

auto operator+(string      const& lhs, string_lc   const& rhs) -> string_lc
{
  return string_lc(lhs + rhs.text);
}

auto operator+(string_view const& lhs, string_lc   const& rhs) -> string_lc
{
  return string_lc(string(lhs) + rhs.text);
}

auto operator+(char        const* lhs, string_lc   const& rhs) -> string_lc
{
  return string_lc(lhs + rhs.text);
}


auto operator<<(ostream& ostr, string_lc const& text) -> ostream&
{
  ostr << text.text;
  return ostr;
}

auto tolower(string text) -> string
{
  for (auto& c : text)
    c = std::tolower(c);
  return text;
}

} // namespace WebSockets
#endif
