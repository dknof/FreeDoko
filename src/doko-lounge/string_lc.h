/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#if defined(WEBSOCKETS_SERVICE) && defined(WITH_DOKOLOUNGE)

namespace DokoLounge {

/** string Klasse, die beim Vergleich Groß-/Kleinschreibung ignoriert
 **/
class string_lc {
  using size_type = string::size_type;
  static auto constexpr npos = string::npos;
public:
  string_lc();
  explicit string_lc(string      const& text);
  explicit string_lc(string_view const& text);
  explicit string_lc(char        const* text);
  string_lc(string_lc const&);
  auto operator=(string_lc   const& text) -> string_lc&;
  auto operator=(string      const& text) -> string_lc&;
  auto operator=(string_view const  text) -> string_lc&;
  auto operator=(char        const* text) -> string_lc&;

  operator string const&() const;

  auto empty() const -> bool;
  auto size()  const -> size_t;

  void clear();

  auto front() const -> char;
  auto substr(size_type pos = 0, size_type count = npos) const -> string_lc;

public:
  string text;
};

auto operator==(string_lc   const& lhs, string_lc   const& rhs) -> bool;
auto operator==(string_lc   const& lhs, string      const& rhs) -> bool;
auto operator==(string_lc   const& lhs, string_view const  rhs) -> bool;
auto operator==(string_lc   const& lhs, char        const* rhs) -> bool;
auto operator==(string      const& lhs, string_lc   const& rhs) -> bool;
auto operator==(string_view const  lhs, string_lc   const& rhs) -> bool;
auto operator==(char        const* lhs, string_lc   const& rhs) -> bool;
auto operator!=(string_lc   const& lhs, string_lc   const& rhs) -> bool;
auto operator!=(string_lc   const& lhs, string      const& rhs) -> bool;
auto operator!=(string_lc   const& lhs, string_view const  rhs) -> bool;
auto operator!=(string_lc   const& lhs, char        const* rhs) -> bool;
auto operator!=(string      const& lhs, string_lc   const& rhs) -> bool;
auto operator!=(string_view const  lhs, string_lc   const& rhs) -> bool;
auto operator!=(char        const* lhs, string_lc   const& rhs) -> bool;

auto operator+(string_lc   const& lhs, string_lc   const& rhs) -> string_lc;
auto operator+(string_lc   const& lhs, string      const& rhs) -> string_lc;
auto operator+(string_lc   const& lhs, string_view const& rhs) -> string_lc;
auto operator+(string_lc   const& lhs, char        const* rhs) -> string_lc;
auto operator+(string      const& lhs, string_lc   const& rhs) -> string_lc;
auto operator+(string_view const& lhs, string_lc   const& rhs) -> string_lc;
auto operator+(char        const* lhs, string_lc   const& rhs) -> string_lc;

auto operator<<(ostream& ostr, string_lc const& text) -> ostream&;

auto tolower(string text) -> string;

} // namespace DokoLounge

#endif
