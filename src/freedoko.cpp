/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "options.h"
#include "versions.h"
#include "basetypes/program_flow_exception.h"

#include "utils/file.h"

#include "basetypes/fast_play.h"

#include "misc/preferences.h"
#include "misc/references_check.h"

#include "os/seed.h"

#include "ui/ui.h"

#ifdef USE_SOUND
#include "sound/sound.h"
#endif

#include "party/party.h"
#include "party/rule.h"
#include "game/game_summary.h"

// the date of the expiration of the different cardsets
Date const Preferences::innocard_expiration_date(2025, 12, 31);
Date const Preferences::altenburg_expiration_date(2013, 12, 31);

Preferences preferences; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
string argv0; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

unique_ptr<Party> party; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
unique_ptr<UI> ui; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

#ifdef WITH_UI_INTERFACE
namespace {
void init_after_option_parsing();
void check_for_new_version();
void write_seeds_of_all_games();
void create_config_directory();
void remove_obsolete_files();
void play_parties();
} // namespace

int
main(int argc, char* argv[])
{
  argv0 = argv[0];
  File::initialize_executable_directory(argv0);

  try {
    init_before_option_parsing();
    load_options_file();
    ::preferences.load();
    parse_options(argc, argv);

#ifdef USE_SOUND
    auto sound = Sound::create();
#endif
    init_after_option_parsing();

    play_parties();

    if (::preferences(Preferences::Type::automatic_savings))
      ::preferences.save();

  } catch (ProgramFlowException::Quit const&) {
    return EXIT_SUCCESS;
  } catch (std::exception const& e) {
    cerr << "main()\n"
      << "  uncaught standard exception: " << e.what()
      << "  (" << &e << ")" << endl;
    return EXIT_FAILURE;
  } catch (std::string const& text) {
    cerr << "main()\n"
      << "  uncaught exception: " << text << endl;
    return EXIT_FAILURE;
  } catch (Card const& card) {
    cerr << "main()\n"
      << "  caught card exception: " << card << endl;
    return EXIT_FAILURE;
  } catch (...) {
    cerr << "main()\n"
      << "  uncaught unknown exception" << endl;
    return EXIT_FAILURE;
  } // try

#ifdef CHECK_RUNTIME
  // In the destructor
  //cout << ::runtime << endl;
#endif

  return EXIT_SUCCESS;
}

namespace {

void init_after_option_parsing()
{

  if (::preferences(Preferences::Type::automatic_savings)) {
    create_config_directory();
    remove_obsolete_files();
    write_seeds_of_all_games();
  }

  check_for_new_version();
  ::preferences.check_for_outdated_cardset();

#ifndef RELEASE
#if 0
  // output of the party points
  unique_ptr<OS_NS::PartyPoints> party_points = {};
  if (::preferences(Preferences::Type::automatic_savings))
    party_points = make_unique<OS_NS::PartyPoints>(new ofstream("FreeDoko.party_points"));
#endif
#endif

  // If no game has been saved in the party, the ui gets a problem with
  // a random startplayer.
  // So we check here, whether a game was already played.
  // Here, because so the rules and players are taken from the saved party
  // and not from the other files.
  if (party->gameno() == 0)
    party->set_status(Party::Status::init);
  if (party->status() == Party::Status::loaded)
    party->set_status(Party::Status::initial_loaded);

  if (::fast_play & FastPlay::seed_info) {
    party->rule().reset_to_seed_statistics();
  }

  if (::fast_play & FastPlay::player)
    party->players().change_type(Player::Type::human, Player::Type::ai);

  if (::fast_play & FastPlay::dummy_ai)
    party->players().change_type(Player::Type::ai, Player::Type::ai_dummy);
}


void check_for_new_version()
{ // read the version of the last usage of FreeDoko and write the new version

  // read the old version
  auto const version_old = Version::create_from_file(::preferences.config_directory() / "Version");

  if (::preferences(Preferences::Type::automatic_savings)) {
    { // write the new version
      ofstream ostr(::preferences.config_directory() / "Version");
      if (!ostr.fail()) {
        ostr << *::version;
      }
    } // write the new version
  } // if (::preferences(Preferences::Type::automatic_savings))

  if (version_old == nullptr) {
    // This seems to be the first start -- give some introduction.
    ::ui->first_run(_("Greeting::first run"));
  } else { // if !(version_old == nullptr)
#ifdef RELEASE
    // give information if the version is updated
    if (*version_old < *::version) {
      ::ui->program_updated(*version_old);
    }
#endif
  } // if (version_old != nullptr)

} // read the version of the last usage of FreeDoko and write the new version


void write_seeds_of_all_games()
{
  // shall last till end of program
  static auto ofstr = ofstream(::preferences.config_directory() / "seed", std::ios::app);
  static auto seed_out = make_unique<OS_NS::Seed>(ofstr);
  (void) seed_out;
}

void create_config_directory()
{
  InformationType information_type = InformationType::normal;
  string information = {};

  auto const dir = ::preferences.config_directory();
  if (!is_directory(dir)) {
    try {
      information = (_("Message::creating directory '%s'", dir.string()));
      std::filesystem::create_directory(dir);
    } catch (...) {
      information += (" - " + _("Message::failed") + "!");
      information_type = InformationType::warning;
    }
  } // if (!isdirectory(dir))
  if (!information.empty()) {
    if (information_type == InformationType::normal)
      cout << information << '\n';
    else
      ::ui->information(information, information_type);
  }
}

#ifdef BACKWARD_COMPABILITY
// Version 0.7.21: change in configuration files
void remove_obsolete_files()
{
  if (is_regular_file(::preferences.config_directory() / "party")) {
    for (auto const file : {"parties/current", "rules/last", "player.1", "player.2", "player.3", "player.4"}) {
      auto const path = ::preferences.config_directory() / file;
      if (!is_regular_file(path))
        continue;
      try {
        remove(path);
      } catch (...) {
      }
    }
  }

  for (auto const dir : {"ai", "parties", "rules"}) {
    auto const path = ::preferences.config_directory() / dir;
    if (!is_directory(path))
      continue;
    if (!is_empty(path))
      continue;
    try {
      remove(path);
    } catch (...) {
    }
  }
}
#endif

void play_parties()
{
  auto& party = *::party;

  if (party.status() == Party::Status::programstart)
    party.set_status(Party::Status::init);

  while (party.status() != Party::Status::quit) {
    try {
      try {
        party.open();

        if (   party.status() == Party::Status::initial_loaded
            || party.status() == Party::Status::loaded) {
          party.signal_loaded();
          ::ui->party_loaded();
        } else {
          party.signal_get_settings();
          ::ui->party_get_settings();
        }
        if (party.status() == Party::Status::quit)
          throw ProgramFlowException::Quit();

        if (party.status() == Party::Status::init)
          party.reset();

        // start playing
        party.play();
      } catch (Party::Status const status) {
        if (status == Party::Status::finished)
          party.set_status(status);
        else
          throw;
      } // try
      if (::fast_play & FastPlay::quit_when_finished)
        throw ProgramFlowException::Quit();

      party.auto_save();
      party.signal_finish();

      party.set_status(Party::Status::init);
    } catch (Party::Status const& status) {
      DEBUG_ASSERTION((   status == Party::Status::init
                       || status == Party::Status::loaded
                       || status == Party::Status::quit),
                      "main():\n"
                      "  caught status '" << status << "'");
      if (status != Party::Status::loaded)
        party.close();
      party.set_status(status);
    } catch (...) {
      party.set_status(Party::Status::quit);
      party.close();
      throw;
    }
    // last chance to save the party and the players
    if (party.status() == Party::Status::quit) {
      party.auto_save();
    }

#ifdef CHECK_RUNTIME
    cout << "games: " << party.finished_games() << '\n';
#endif
    if (party.status() != Party::Status::loaded)
      party.close();

    if (::references_check) {
      try {
        party.set_status(Party::Status::finished);
        party.close();
        ::references_check->next_reference();
      } catch (Party::Status const& status) {
        DEBUG_ASSERTION((   status == Party::Status::init
                         || status == Party::Status::quit),
                        "main():\n"
                        "  caught status '" << status << "'");
        party.set_status(status);
      } catch (...) {
        throw;
      }
    } // if (::reference_check)
  } // while (party.status() != quit)
}

} // namespace
#endif
