/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../basetypes/announcement.h"
#include "../basetypes/team.h"

class Game;
class Player;

/**
 ** The announcements of a game
 **/
class GameAnnouncements {
  friend class Game;
  public:
  explicit GameAnnouncements(Game& game);
  GameAnnouncements(GameAnnouncements const& announcements, Game& game);

  GameAnnouncements(GameAnnouncements const&) = delete;

  auto last(Team team)            const -> AnnouncementWithTrickno;
  auto last(Player const& player) const -> AnnouncementWithTrickno;
  auto all(Player const& player)  const -> vector<AnnouncementWithTrickno> const&;

  void init();
  void request_from_players();
  void evaluate_trick(Trick const& trick);

  void make_announcement(Announcement announcement, Player const& player);
  auto is_valid(Announcement announcement, Player const& player)                const -> bool;
  auto valid_announcement(Player const& player)                                 const -> Announcement;
  auto last_chance_to_announce(Announcement announcement, Player const& player) const -> bool;

  void remove_all_but_re_contra();

  private:
  auto reply_to_explicit_announcement(Announcement announcement, Player const& player) const -> Announcement;

  private:
  Game* const game_;
  vector<vector<AnnouncementWithTrickno> > announcements_;

  public:
  Signal<void(Announcement, Player const&)> signal_announcement_made;
};
