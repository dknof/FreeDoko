/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "cards.h"
#include "game.h"
#include "../party/rule.h"

GameCards::GameCards(Game& game) :
  game_(&game),
  cards_(Card::number_of_tcolors)
{ }

GameCards::GameCards(GameCards const& cards, Game& game) :
  game_(&game),
  cards_(cards.cards_)
{ }

void GameCards::update()
{
  auto const& game = *game_;
  auto const& rule = game.rule();

  cards_ = vector<vector<Card>>(Card::number_of_tcolors);

  // first all color cards

  for (auto const c : rule.card_colors())
    for (auto const v : rule.card_values())
      if (!Card(c, v).istrump(game))
        cards_[c].emplace_back(c, v);


  // second all trumps (manual fill)

  vector<Card>& trumps = cards_[Card::trump];
  switch (game.type()) {
  case GameType::normal:
  case GameType::fox_highest_trump:
  case GameType::poverty:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
  case GameType::redistribute:
    if (rule(Rule::Type::dullen))
      trumps.push_back(Card::dulle);
    trumps.push_back(Card::club_queen);
    trumps.push_back(Card::spade_queen);
    trumps.push_back(Card::heart_queen);
    trumps.push_back(Card::diamond_queen);
    trumps.push_back(Card::club_jack);
    trumps.push_back(Card::spade_jack);
    trumps.push_back(Card::heart_jack);
    trumps.push_back(Card::diamond_jack);
    trumps.emplace_back(trumpcolor(), Card::ace);
    if (!(   (trumpcolor() == Card::heart)
          && rule(Rule::Type::dullen)))
      trumps.emplace_back(trumpcolor(), Card::ten);
    trumps.emplace_back(trumpcolor(), Card::king);
    if (rule(Rule::Type::with_nines))
      trumps.emplace_back(trumpcolor(), Card::nine);
    break;
  case GameType::thrown_nines:
    trumps = {
    Card::club_nine,
    Card::spade_nine,
    Card::heart_nine,
    Card::diamond_nine,
    };
    break;
  case GameType::thrown_kings:
    trumps = {
      Card::club_king,
      Card::spade_king,
      Card::heart_king,
      Card::diamond_king,
    };
    break;
  case GameType::thrown_nines_and_kings:
    trumps = {
      Card::club_king,
      Card::spade_king,
      Card::heart_king,
      Card::diamond_king,
      Card::club_nine,
      Card::spade_nine,
      Card::heart_nine,
      Card::diamond_nine,
    };
    break;
  case GameType::thrown_richness:
    trumps = {
      Card::diamond_jack,
      Card::heart_jack,
      Card::spade_jack,
      Card::club_jack,
      Card::diamond_queen,
      Card::heart_queen,
      Card::spade_queen,
      Card::club_queen,
      Card::diamond_king,
      Card::heart_king,
      Card::spade_king,
      Card::club_king,
      Card::diamond_ten,
      Card::heart_ten,
      Card::spade_ten,
      Card::club_ten,
      Card::diamond_ace,
      Card::heart_ace,
      Card::spade_ace,
      Card::club_ace,
    };
    if (rule(Rule::Type::with_nines)) {
      trumps.push_back(Card::diamond_nine);
      trumps.push_back(Card::heart_nine);
      trumps.push_back(Card::spade_nine);
      trumps.push_back(Card::club_nine);
    }
    break;
  case GameType::solo_meatless:
    break;
  case GameType::solo_jack:
    trumps = {
      Card::club_jack,
      Card::spade_jack,
      Card::heart_jack,
      Card::diamond_jack,
    };
    break;
  case GameType::solo_queen:
    trumps = {
      Card::club_queen,
      Card::spade_queen,
      Card::heart_queen,
      Card::diamond_queen,
    };
    break;
  case GameType::solo_king:
    trumps = {
      Card::club_king,
      Card::spade_king,
      Card::heart_king,
      Card::diamond_king,
    };
    break;
  case GameType::solo_queen_jack:
    trumps = {
      Card::club_queen,
      Card::spade_queen,
      Card::heart_queen,
      Card::diamond_queen,
      Card::club_jack,
      Card::spade_jack,
      Card::heart_jack,
      Card::diamond_jack,
    };
    break;
  case GameType::solo_king_jack:
    trumps = {
      Card::club_king,
      Card::spade_king,
      Card::heart_king,
      Card::diamond_king,
      Card::club_jack,
      Card::spade_jack,
      Card::heart_jack,
      Card::diamond_jack,
    };
    break;
  case GameType::solo_king_queen:
    trumps = {
      Card::club_king,
      Card::spade_king,
      Card::heart_king,
      Card::diamond_king,
      Card::club_queen,
      Card::spade_queen,
      Card::heart_queen,
      Card::diamond_queen,
    };
    break;
  case GameType::solo_koehler:
    trumps = {
      Card::club_king,
      Card::spade_king,
      Card::heart_king,
      Card::diamond_king,
      Card::club_queen,
      Card::spade_queen,
      Card::heart_queen,
      Card::diamond_queen,
      Card::club_jack,
      Card::spade_jack,
      Card::heart_jack,
      Card::diamond_jack,
    };
    break;
  }; // switch (type())

  DEBUG_ASSERTION((this->trumps().size()
                   == (count(Card::trump)
                       / rule(Rule::Type::number_of_same_cards))),
                  "GameCards::update()\n"
                  "  trumps size " << this->trumps().size()
                  << " does not match trumps number "
                  << (count(Card::trump)
                      / rule(Rule::Type::number_of_same_cards)));
}

void GameCards::update_swines()
{
  auto& trumps = cards_[Card::trump];
  remove(trumps, swine());
  trumps.insert(trumps.begin(), swine());
}

void GameCards::update_hyperswines()
{
  auto& trumps = cards_[Card::trump];
  remove(trumps, hyperswine());
  trumps.insert(trumps.begin(), hyperswine());
}

auto GameCards::count() const -> unsigned
{
  auto const& rule = game_->rule();
  return (count_unique()
          * rule(Rule::Type::number_of_same_cards));
}

auto GameCards::count_unique() const -> unsigned
{
  auto const& rule = game_->rule();
  return rule(Rule::Type::number_of_cards);
}

auto GameCards::count(Card::TColor const tcolor) const -> unsigned
{
  auto const& rule = game_->rule();
  if (tcolor == Card::unknowncardcolor) {
    return count();
  } else {
    return (count_unique(tcolor)
            * rule(Rule::Type::number_of_same_cards));
  }
}

auto GameCards::count_unique(Card::TColor const tcolor) const -> unsigned
{
  auto const& rule = game_->rule();
  if (tcolor == Card::unknowncardcolor)
    return count();
  if (tcolor == Card::trump)
    return (count_trumps() / rule(Rule::Type::number_of_same_cards));
  if (tcolor == trumpcolor())
    return 0;

  switch (game_->type()) {
  case GameType::normal:
  case GameType::fox_highest_trump:
  case GameType::poverty:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_diamond:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_club:
  case GameType::redistribute:
    {
      unsigned number = rule(Rule::Type::number_of_card_values) - 2;
      if (   (tcolor == Card::heart)
          && rule(Rule::Type::dullen)) {
        number -= 1;
      }
      return number;
    }
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
    return (rule(Rule::Type::number_of_card_values) - 1);

  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
    return (rule(Rule::Type::number_of_card_values) - 2);

  case GameType::solo_koehler:
    return (rule(Rule::Type::number_of_card_values) - 3);

  case GameType::solo_meatless:
    return rule(Rule::Type::number_of_card_values);

  case GameType::thrown_nines:
  case GameType::thrown_kings:
    return rule(Rule::Type::number_of_card_colors);

  case GameType::thrown_nines_and_kings:
    return (2 * rule(Rule::Type::number_of_card_colors));

  case GameType::thrown_richness:
    return 0;
  } // type()

  return 0;
}

auto GameCards::count_trumps() const -> unsigned
{
  auto const& game = *game_;
  auto const& rule = game.rule();

  unsigned trumps = 0;

  switch (game.type()) {
  case GameType::normal:
  case GameType::fox_highest_trump:
  case GameType::poverty:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_diamond:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_club:
  case GameType::redistribute:
    trumps = 2 * rule(Rule::Type::number_of_card_colors); // jack and queen
    trumps += (rule(Rule::Type::number_of_card_values)
               - 2); // rest trump color
    if (   rule(Rule::Type::dullen)
        && (game.type() != GameType::solo_heart))
      trumps += 1;
    break;
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
    trumps = rule(Rule::Type::number_of_card_colors);
    break;
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
    trumps = 2 * rule(Rule::Type::number_of_card_colors);
    break;
  case GameType::solo_koehler:
    trumps = 3 * rule(Rule::Type::number_of_card_colors);
    break;
  case GameType::solo_meatless:
    trumps = 0;
    break;
  case GameType::thrown_nines:
  case GameType::thrown_kings:
    trumps = rule(Rule::Type::number_of_card_colors);
    break;

  case GameType::thrown_nines_and_kings:
    trumps = 2 * rule(Rule::Type::number_of_card_colors);
    break;

  case GameType::thrown_richness:
    trumps = (rule(Rule::Type::number_of_cards)
              / rule(Rule::Type::number_of_same_cards));
    break;
  } // type()
  trumps *= rule(Rule::Type::number_of_same_cards);

  return trumps;
}

auto GameCards::count_played_trumps() const -> unsigned
{
  auto const& game = *game_;
  auto const& tricks = game.tricks();
  // number of trumps
  unsigned trumps = 0;
  for (auto const& trick : tricks) {
    for (unsigned c = 0; c < trick.actcardno(); ++c) {
      if (trick.card(c).istrump()) {
        trumps += 1;
      }
    }
  }

  return trumps;
}

auto GameCards::count_played() const -> unsigned
{
  auto const& game = *game_;
  auto const& rule = game.rule();
  auto const& tricks = game.tricks();

  if (tricks.empty())
    return 0;

  if (game_->status() == Game::Status::finished) {
    return (rule(Rule::Type::number_of_tricks_in_game)
            * rule(Rule::Type::number_of_players_in_game));
  }

  return (tricks.real_current_no()
          * rule(Rule::Type::number_of_players_in_game)
          + tricks.current().actcardno());
}

auto GameCards::count_remaining() const -> unsigned
{
  auto const& rule = game_->rule();
  return (rule(Rule::Type::number_of_tricks_in_game)
          * rule(Rule::Type::number_of_players_in_game)
          - count_played());
}

auto GameCards::count_played(Card const& card) const -> unsigned
{
  return container_algorithm::sum(game_->tricks(),
                                  [card](auto const& trick) {
                                  return container_algorithm::count(trick.cards(), card);
                                  });
}

auto GameCards::trumpcolor() const -> Card::Color
{
  return ::trumpcolor(game_->type());
}

auto GameCards::colors() const -> vector<Card::Color>
{
  switch (game_->type()) {
  case GameType::normal:
  case GameType::poverty:
  case GameType::fox_highest_trump:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_diamond:
  case GameType::redistribute:
    return {Card::club, Card::spade, Card::heart};
  case GameType::solo_heart:
    return {Card::club, Card::spade, Card::diamond};
  case GameType::solo_spade:
    return {Card::club, Card::heart, Card::diamond};
  case GameType::solo_club:
    return {Card::spade, Card::heart, Card::diamond};
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
  case GameType::solo_meatless:
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
    return {Card::club, Card::spade, Card::heart, Card::diamond};
  } // switch(game_type)

  DEBUG_ASSERTION(false,
                  "GameCards::colors(game_type)\n"
                  "  after return");
  return {};
}

auto GameCards::tcolors() const -> vector<Card::TColor>
{
  switch (game_->type()) {
  case GameType::normal:
  case GameType::poverty:
  case GameType::fox_highest_trump:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_diamond:
  case GameType::redistribute:
    return {Card::trump, Card::club, Card::spade, Card::heart};
  case GameType::solo_heart:
    return {Card::trump, Card::club, Card::spade, Card::diamond};
  case GameType::solo_spade:
    return {Card::trump, Card::club, Card::heart, Card::diamond};
  case GameType::solo_club:
    return {Card::trump, Card::spade, Card::heart, Card::diamond};
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
    return {Card::trump, Card::club, Card::spade, Card::heart, Card::diamond};
  case GameType::solo_meatless:
    return {Card::club, Card::spade, Card::heart, Card::diamond};
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
    return {Card::trump, Card::club, Card::spade, Card::heart};
  } // switch(game_type)

  DEBUG_ASSERTION(false,
                  "GameCards::colors(game_type)\n"
                  "  after return");
  return {};
}

auto GameCards::cards() const -> vector<Card>
{
  vector<Card> cards;
  auto const n = game_->rule()(Rule::Type::number_of_same_cards);
  for (auto const& c : cards_) {
    for (unsigned i = 1; i < n; ++i) {
      cards.insert(cards.end(), c.begin(), c.end());
    }
  }

  return cards;
}

auto GameCards::cards(Card::TColor const tcolor) const -> vector<Card> const&
{
  DEBUG_ASSERTION(tcolor <= Card::trump,
                  "GameCards::cards(tcolor)\n"
                  "  tcolor = " << tcolor << " is not valid.");

  return cards_[tcolor];
}

auto GameCards::trumps() const -> vector<Card> const&
{
  return cards(Card::trump);
}

auto GameCards::sorted_single_cards() const -> vector<Card>
{
  vector<Card> cards = trumps();
  {
    if (game_->swines().swines_announced()) {
      cards.erase(find(cards, swine()));
      cards.insert(cards.begin(), swine());
    }
    if (game_->swines().hyperswines_announced()) {
      cards.erase(find(cards, hyperswine()));
      cards.insert(cards.begin(), hyperswine());
    }
  }
  for (auto const color : {Card::club, Card::spade, Card::heart, Card::diamond}) {
    auto const& color_cards = this->cards(color);
    cards.insert(cards.end(), color_cards.begin(), color_cards.end());
  }

  return cards;
}

auto GameCards::less(Card const& lhs, Card const& rhs) const -> bool
{
  if (rhs.is_empty())
    return false;
  if (lhs.is_empty())
    return true;

  Game const& game = *game_;
  Rule const& rule = game.rule();

  // both cards are not trump
  if (!lhs.istrump(game) && !rhs.istrump(game)) {
    if (lhs.color() == rhs.color()) // same color
      return (lhs.value() < rhs.value());
    else // different color
      return false;
  }

  if (lhs.istrump(game) && !rhs.istrump(game))
    return false;

  if (!lhs.istrump(game) && rhs.istrump(game))
    return true;


  // The difficult case: both cards are trump

  switch(game.type()) {
  case GameType::normal:
  case GameType::poverty:
  case GameType::fox_highest_trump:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_diamond:
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::redistribute:

    // Hyperswines
    if (rule(Rule::Type::hyperswines)) {
      if (game.swines().hyperswines_announced()) {
        if (lhs == hyperswine())
          return false;
        if (rhs == hyperswine())
          return true;
      }
    }

    // Swines
    if (rule(Rule::Type::swines)) {
      if (game.swines().swines_announced()) {
        if (lhs == swine())
          return false;
        if (rhs == swine())
          return true;
      }
    }

    // Dullen
    if (rule(Rule::Type::dullen)) {
      if (lhs == Card::dulle) {
        if (rhs == Card::dulle) { // both cards are dullen
          if (    (rule(Rule::Type::dullen_first_over_second_with_swines)
                   && game.swines().swines_announced()) )
            return false;
          if (rule(Rule::Type::dullen_contrary_in_last_trick)
              && game.tricks().is_last_trick()) {
            // we are in the last trick
            // and the rule 'dullen_contrary_in_last_trick' is valid
            if (rule(Rule::Type::dullen_second_over_first)) {
              return false;
            } else { // if !(rule(Rule::Type::dullen_second_over_first)
              return true;
            } // if !(rule(Rule::Type::dullen_second_over_first)
          } else { // if !(game.tricks().is_last_trick())
            if (rule(Rule::Type::dullen_second_over_first)) {
              return true;
            } else { // if !(rule(Rule::Type::dullen_second_over_first)
              return false;
            } // if !(rule(Rule::Type::dullen_second_over_first)
          } // if !(game.tricks().is_last_trick())
        } else { // if !(b == Card::dulle)
          return false;
        } // if !(b == Card::dulle)
      } else { // if !(a == Card::dulle)
        if (rhs == Card::dulle)
          return true;
      } // if !(a == Card::dulle)
    } // if (rule.dullen)

    if (lhs.value() == Card::queen) {
      if (rhs.value() == Card::queen) {
        return (lhs.color() < rhs.color());
      } else {
        return false;
      }
    }

    if (lhs.value() == Card::jack) {
      if (rhs.value() == Card::queen) {
        return true;
      } else if (rhs.value() == Card::jack) {
        return (lhs.color() < rhs.color());
      } else {
        return false;
      }
    }

    // 'a' is a simple ace/ten/king/nine
    if ( (rhs.value() == Card::queen) ||
        (rhs.value() == Card::jack) ) // 'b' is higher than an ace/ten/king/nine
      return true;

    // both cards are simple ace/ten/king/nine
    return (lhs.value() < rhs.value());
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
    return (lhs.color() < rhs.color());
  case GameType::solo_queen_jack:
    if (lhs.value() != rhs.value()) // one card is queen, one is jack
      return (lhs.value() == Card::jack);
    // both card are jack/queen
    return (lhs.color() < rhs.color());
  case GameType::solo_king_jack:
    if (lhs.value() != rhs.value()) // one card is king, one is jack
      return (lhs.value() == Card::jack);
    // both card are jack/king
    return (lhs.color() < rhs.color());
  case GameType::solo_king_queen:
    if (lhs.value() != rhs.value()) // one card is king, one is queen
      return (lhs.value() == Card::queen);
    // both card are queen/king
    return (lhs.color() < rhs.color());
  case GameType::solo_koehler:
    if (lhs.value() == rhs.value()) // both cards are the same value
      return (lhs.color() < rhs.color());
    // both cards have different values
    if (lhs.value() == Card::jack)
      return true;
    if (rhs.value() == Card::jack)
      return false;
    // one card is queen, one is king
    // -- hey, that is like in 'solo_king_queen', take that code
    return (lhs.value() == Card::queen);
  case GameType::solo_meatless:
    // cannot be, because no card is trump
    DEBUG_ASSERTION(false,
                    "  solo_meatless with trump ");

    break;
  case GameType::thrown_nines:
    return (lhs.color() < rhs.color());
  case GameType::thrown_kings:
    return (lhs.color() < rhs.color());
  case GameType::thrown_nines_and_kings:
    if (   (lhs.value() == Card::nine)
        && (rhs.value() == Card::king) )
      return true;
    if (   (lhs.value() == Card::king)
        && (rhs.value() == Card::nine) )
      return false;
    return (lhs.color() < rhs.color());

  case GameType::thrown_richness:
    return (   (lhs.value() < rhs.value())
            || (   (lhs.value() == rhs.value())
                && (lhs.color() < rhs.color())));
  } // switch(game.type())

  DEBUG_ASSERTION(false,
                  "Card::less(lhs, rhs):\n"
                  "  after 'switch(game.type())'");

  return false;
}

auto GameCards::next_lower_card(Card const& card) const -> Card
{
  auto const& game = *game_;
  if (card == hyperswine()) {
    return swine();
  }
  if (card == swine()) {
    if (game.rule(Rule::Type::dullen))
      return dulle();
    return Card::club_queen;
  }
  if (   game.rule(Rule::Type::dullen)
      && card == dulle()) {
    return Card::club_queen;
  }
  auto const& color_cards = cards(card.tcolor(game));
  auto p = find(color_cards, card);
  do {
    ++p;
    if (p == color_cards.end())
      return {};
  } while (   *p == swine()
           || *p == hyperswine());
  return *p;
}

auto GameCards::next_higher_card(Card const& card) const -> Card
{
  auto const& game = *game_;
  auto const& color_cards = cards(card.tcolor(game));
  auto p = find(color_cards.rbegin(), color_cards.rend(), card);
  ++p;
  if (p == color_cards.rend()) {
    if (card.istrump(game))
      return {};
    else
      return trumps().front();
  }
  return *p;
}


// NOLINTNEXTLINE(misc-no-recursion)
auto GameCards::next_higher_card(Card const& card, unsigned const n) const -> Card
{
  if (n == 0)
    return card;
  else if (n == 1)
    return next_higher_card(card);
  else // if (n > 1)
    return next_higher_card(card, n-1);
}

auto GameCards::lower_cards(Card const& card) const -> vector<Card>
{
  auto const& game = *game_;
  auto const& color_cards = cards(card.tcolor(game));
  auto p = find(color_cards, card);
  return vector<Card>(++p, color_cards.end());
}

auto GameCards::higher_cards(Card const& card) const -> vector<Card>
{
  auto const& game = *game_;
  auto const& color_cards = cards(card.tcolor(game));
  auto p = find(color_cards.rbegin(), color_cards.rend(), card);
  auto cards = vector<Card>(++p, color_cards.rend());

  if (!card.istrump(game)) {
    cards.insert(cards.end(), trumps().rbegin(), trumps().rend());
  }
  return cards;
}

auto GameCards::cards_between(Card const& lhs, Card const& rhs) const -> vector<Card>
{
  // the cards between
  auto cards = higher_cards(lhs);

  // search the first card not less than 'rhs'
  auto c = cards.begin();
  for (; c != cards.end(); ++c) {
    if (   *c == rhs
        || !less(*c, rhs)) {
      break;
    }
  }

  return vector<Card>(cards.begin(), c);
}

auto GameCards::lower_trumps(Card const& card) const -> vector<Card>
{
  auto const& game = *game_;
  if (!card.istrump(game))
    return {};

  auto p = find(trumps(), card);

  return vector<Card>(p, trumps().end());
}

auto GameCards::is_special(HandCard const& card) const -> bool
{
  auto const& game = *game_;
  auto const& rule = game.rule();
  if (card.is_empty())
    return false;

  // dulle
  if (   rule(Rule::Type::dullen)
      && (card == Card::dulle))
    return true;

  // swines
  if (   rule(Rule::Type::swines)
      && (card == swine())
      && (   game.swines().swines_announced()
          || game.swines().swines_announcement_valid(card.player())) )
    return true;

  // hyperswines
  if (   rule(Rule::Type::hyperswines)
      && (card == hyperswine())
      && (   game.swines().hyperswines_announced()
          || game.swines().hyperswines_announcement_valid(card.player())) )
    return true;

  return false;
}

auto GameCards::fox() const -> Card
{
  auto const& game = *game_;
  auto const& rule = game.rule();
  if (game.swines().swines_announced())
    return {};
  if (  rule(Rule::Type::extrapoints_in_solo)
      ? !is_with_trump_color(game.type())
      : game.is_solo()) {
    return {};
  }
  return {trumpcolor(), Card::ace};
}

auto GameCards::dulle() const -> Card
{
  auto const game_type = game_->type();
  auto const& rule = game_->rule();
  if (!rule(Rule::Type::dullen))
    return {};

  if (   ::is_normal(game_type)
      || ::is_poverty(game_type)
      || ::is_marriage(game_type)
      || ::is_color_solo(game_type)) {
    return Card::dulle;
  } else {
    return {};
  }
}

auto GameCards::swine() const -> Card
{
  auto const game_type = game_->type();
  auto const& rule = game_->rule();
  if (!rule(Rule::Type::swines))
    return {};

  if (   ::is_normal(game_type)
      || ::is_poverty(game_type)
      || ::is_marriage(game_type)
      || (   rule(Rule::Type::swines_in_solo)
          && ::is_color_solo(game_type)) ) {
    return {trumpcolor(), Card::ace};
  } else {
    return {};
  }
}

auto GameCards::hyperswine() const -> Card
{
  auto const game_type = game_->type();
  auto const& rule = game_->rule();
  if (!rule(Rule::Type::hyperswines))
    return {};

  if (   ::is_normal(game_type)
      || ::is_poverty(game_type)
      || ::is_marriage(game_type)
      || (   rule(Rule::Type::hyperswines_in_solo)
          && ::is_color_solo(game_type)) ) {
    return {trumpcolor(), rule(Rule::Type::with_nines) ? Card::nine : Card::king};
  } else {
    return {};
  }
}

auto trumpcolor(GameType game_type) noexcept -> Card::Color
{
  switch (game_type) {
  case GameType::normal:
  case GameType::poverty:
  case GameType::fox_highest_trump:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_diamond:
  case GameType::redistribute:
    return Card::diamond;
  case GameType::solo_heart:
    return Card::heart;
  case GameType::solo_spade:
    return Card::spade;
  case GameType::solo_club:
    return Card::club;
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
  case GameType::solo_meatless:
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
    return Card::nocardcolor;
  }

  return Card::nocardcolor;
}
