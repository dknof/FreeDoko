/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "game.h"

#include "gameplay.h"
#include "gameplay_actions.h"

#include "../party/party.h"
#include "../party/rule.h"
#include "../player/player.h"
#include "../card/trick.h"

Signal<void(Game&)> Game::signal_open; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

Game::Game(Party& party, Player& startplayer) :
  party_(&party),
  startplayer_(&startplayer),
  gameplay_(make_unique<Gameplay>(*this)),
  seed_(party.seed()),
  cards_(*this),
  poverty_(*this),
  marriage_(*this),
  swines_(*this),
  tricks_(*this),
  announcements_(*this),
  teaminfo_(*this)
{
  players_ = make_unique<Players>(*this);
}


Game::Game(Game const& game, vector<unique_ptr<Player>>& players) :
  party_(game.party_),
  players_(make_unique<Players>(*this, game.players(), players)),
  gameplay_(game.gameplay_->clone(*this)),
  seed_(game.seed_),
  type_(game.type_),
  cards_(game.cards_, *this),
  poverty_(game.poverty_, *this),
  marriage_(game.marriage_, *this),
  swines_(game.swines_, *this),
  tricks_(game.tricks_, *this),
  announcements_(game.announcements_, *this),
  teaminfo_(game.teaminfo_, *this),
  finished_(game.finished_)
{
  // set the players
  if (game.startplayer_)
    startplayer_ = &player(game.startplayer_->no());

  DEBUG_ASSERT(self_check());
}


Game::~Game() = default;


auto Game::self_check() const -> bool
{
  DEBUG_ASSERTION(party_,
		  "Game::self_check()\n"
		  "  no pointer to a party\n"
		  "  this = " << this);

  if (startplayer_) {
    DEBUG_ASSERTION((this == &startplayer_->game()),
                    "Game::self_check()\n"
                    "  the startplayer is not of the same game");
  }

  players().self_check();
  swines().self_check();
  tricks().self_check();

  return true;
} // bool Game::self_check() const


auto Game::write(ostream& ostr) const -> void
{
  ostr << "Game:\n"
    << "-----\n"
    << "\n"
    << "seed" << ": " << seed() << "\n"
    << "current trick number: " << tricks().current_no() << "\n"
    << "type: " << type() << "\n";
  if (marriage().selector() != MarriageSelector::team_set)
    ostr << "marriage selector: " << marriage().selector() << '\n';
  if (startplayer_)
    ostr << "startplayer: " << startplayer_->no() << '\n';
  if (swines_.swines_announced())
    ostr << "swines: " << swines().swines_owner().no() << '\n';
  if (swines_.hyperswines_announced())
    ostr << "hyperswines: " << swines().hyperswines_owner().no() << '\n';

  ostr << "\n"
    << "teams" << ": \n";
  for (auto const& player : players())
    ostr << player.no() << ": " << player.team() << "\n";

  ostr << "\n"
    << "teaminfo" << ": \n";
  teaminfo().write(ostr);

  tricks().write(ostr);
} // void Game::write(ostream& ostr) const


void Game::write_hands(ostream& ostr) const
{
  ostr << "\n"
    << "hands:\n"
    << "\n";
  for (auto const& p : players()) {
    ostr << p.no() << ' ' << p.name() << '\n'
      << p.hand()
      << '\n';
  }
}


auto Game::rest_query() const -> string
{
  string query;
  query += "startplayer=" + std::to_string(startplayer().no()) + ";";
  query += rule().rest_query() + ";";
  if (status() <= Game::Status::reservation) {
    query.pop_back();
    return query;
  }

  query += "hand=";
  for (auto const& card : players().current_player().hand().cards_all()) {
    query += to_string(card) + ",";
  }
  query.back() = ';';

  query += "game type=";
  switch (type()) {
  case GameType::normal:
  case GameType::redistribute:
    query += to_string(GameType::normal);
    break;
  case GameType::poverty:
    query += to_string(GameType::poverty);
    break;
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
    query += to_string(GameType::marriage) + "," + std::to_string(players().soloplayer().no());
    break;
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
  case GameType::fox_highest_trump:
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
  case GameType::solo_meatless:
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
    query += to_string(type()) + "," + std::to_string(players().soloplayer().no());
    break;
  }
  query += ";";

  for (auto const& action : gameplay().actions()) {
    switch (action->type()) {
    case GameplayActions::Type::reservation:
      break;
    case GameplayActions::Type::card_played: {
      auto const& a = dynamic_cast<GameplayActions::CardPlayed const&>(*action);
      query += to_string(a.type()) + " = " + std::to_string(a.player) + ", " + to_string(a.card) + ";";
      break;
    }
    case GameplayActions::Type::announcement:
    case GameplayActions::Type::swines:
    case GameplayActions::Type::hyperswines:
      query += "," + to_string(*action) + ";";
      break;
    case GameplayActions::Type::marriage:
      // ToDo
      break;
    case GameplayActions::Type::poverty_shift:
    case GameplayActions::Type::poverty_accepted:
    case GameplayActions::Type::poverty_returned:
    case GameplayActions::Type::poverty_denied:
    case GameplayActions::Type::poverty_denied_by_all:
      // ToDo
      break;
    default:
      break;
    }
  }

  query.pop_back();
  return query;
}

auto to_string(Game const& game) -> string
{
  ostringstream ostr;
  ostr << game;
  return ostr.str();
}


auto operator<<(ostream& ostr,  Game const& game) -> ostream&
{
  game.write(ostr);
  return ostr;
}


auto Game::status() const noexcept -> Status
{
  return status_;
}


void Game::set_status(Status const status) noexcept
{
  // @todo: Übergänge prüfen
  status_ = status;
}


auto Game::in_running_game() const noexcept -> bool
{
  switch (status()) {
  case Status::play:
  case Status::full_trick:
  case Status::trick_taken:
    return true;
  default:
    return false;
  }
}


auto Game::isvirtual() const -> bool
{ return (this != &party().game()); }


auto Game::is_finished() const -> bool
{ return finished_; }


auto Game::party() const -> Party const&
{
  return *party_;
}


auto Game::rule() const noexcept -> Rule const&
{
  return party().rule();
}


auto Game::rule(Rule::Type::Bool const type) const -> bool
{
  return rule()(type);
}


auto Game::rule(Rule::Type::Unsigned const type) const -> unsigned
{
  return rule()(type);
}


auto Game::rule(Rule::Type::UnsignedExtra const type) const -> unsigned
{
  return rule()(type);
}


auto Game::rule(GameType const game_type) const -> bool
{
  return rule()(game_type);
}


auto Game::second_dulle_over_first() const -> bool
{
  return (   rule()(Rule::Type::dullen)
          && !(   rule()(Rule::Type::dullen_first_over_second_with_swines)
               && swines().swines_announced())
          && (  !tricks().is_last_trick()
              ? rule()(Rule::Type::dullen_second_over_first)
              : (rule()(Rule::Type::dullen_second_over_first)
                 != rule()(Rule::Type::dullen_contrary_in_last_trick))
             )
         );
} // bool Game::second_dulle_over_first() const


auto Game::bock_multiplier() const -> int
{
  return party().current_bock_multiplier();
}


auto Game::is_duty_solo() const noexcept -> bool
{
  return is_duty_solo_;
}


auto Game::seed() const noexcept -> Seed
{ return seed_; }


void Game::set_seed(Seed const seed) noexcept
{ seed_ = seed; }


auto Game::gameplay() const noexcept -> Gameplay const&
{ return *gameplay_; }


auto Game::type() const noexcept -> GameType
{
  if (   !is_finished()
      && (type_ == GameType::marriage_silent)
      && (marriage().selector() == MarriageSelector::silent) )
    return GameType::normal;

  return type_;
}

void Game::set_type(GameType const type) noexcept
{
  type_ = type;
  cards().update();
}


void Game::force_set_solo(GameType const type, Player const& soloplayer)
  // Wird für Doko-Lounge gebraucht, um beim Sprung ins Spiel den Spieltyp nachträglich korrigieren zu können
{
  type_ = type;
  players_->set_soloplayer(players_->player(soloplayer.no()));
  for (auto& p : players()) {
    p.set_team(p == soloplayer ? Team::re : Team::contra);
    teaminfo().set(p, p.team());
  }
}


auto Game::is_solo() const noexcept -> bool
{
  return ::is_solo(type());
}


auto Game::is_real_solo() const noexcept -> bool
{
  return ::is_real_solo(type());
}


auto Game::set_is_duty_solo() -> bool
{
  is_duty_solo_ = false;

  if (   party().is_duty_soli_round()
      && status() <= Game::Status::redistribute) {
    is_duty_solo_ = true;
    return true;
  }

  if (is_real_solo()) {
    auto const& soloplayer = (  status() <= Game::Status::reservation
                              ? players().current_player()
                              : players().soloplayer());
    if (   is_color_solo(type())
        && party().remaining_duty_color_soli(soloplayer))
      is_duty_solo_ = true;

    if (   is_picture_solo(type())
        && party().remaining_duty_picture_soli(soloplayer))
      is_duty_solo_ = true;

    if (party().remaining_duty_free_soli(soloplayer))
      is_duty_solo_ = true;
  }

  return is_duty_solo();
} // bool Game::set_is_duty_solo()


#ifdef WITH_DOKOLOUNGE
void Game::set_is_duty_solo(bool is_duty_solo)
{
  is_duty_solo_ = is_duty_solo;
}
#endif

void Game::set_startplayer(unsigned const startplayer)
{
  DEBUG_ASSERTION(status() <= Game::Status::cards_distribution,
                  "Game::set_startplayer(" << startplayer << ")\n"
                  "  status = " << status() << " > game_init");

  startplayer_ = &player(startplayer);
}


auto Game::startplayer_stays() const -> bool
{
  if (!::is_real_solo(type()))
    return false;
  // some solo
  if (party().is_duty_soli_round())
    return (   status() == Game::Status::reservation
            || players().soloplayer() != party().players()[party().startplayer()]);
  if (is_duty_solo())
    return true;
  // some lust solo
  return rule()(Rule::Type::lustsolo_player_leads);
} // bool Game::startplayer_stays() const


auto Game::cards() const noexcept -> GameCards const&
{
  return cards_;
}


auto Game::cards() noexcept -> GameCards&
{
  return cards_;
}


auto Game::poverty() const noexcept -> GamePoverty const&
{
  return poverty_;
}


auto Game::poverty() noexcept -> GamePoverty&
{
  return poverty_;
}


auto Game::marriage() const noexcept -> GameMarriage const&
{
  return marriage_;
}


auto Game::marriage() noexcept -> GameMarriage&
{
  return marriage_;
}


auto Game::swines() const noexcept -> GameSwines const&
{
  return swines_;
}


auto Game::swines() noexcept -> GameSwines&
{
  return swines_;
}


auto Game::tricks() const noexcept -> GameTricks const&
{
  return tricks_;
}


auto Game::tricks() noexcept -> GameTricks&
{
  return tricks_;
}


auto Game::announcements() const noexcept -> Announcements const&
{
  return announcements_;
}


auto Game::announcements() noexcept -> Announcements&
{
  return announcements_;
}


auto Game::teaminfo() const noexcept -> TeamInfo const&
{
  return teaminfo_;
}


auto Game::teaminfo() noexcept -> TeamInfo&
{
  return teaminfo_;
}


auto operator==(Game const& game1, Game const& game2) noexcept -> bool
{
  return (&game1 == &game2);
}


auto operator!=(Game const& game1, Game const& game2) noexcept -> bool
{
  return !(game1 == game2);
}
