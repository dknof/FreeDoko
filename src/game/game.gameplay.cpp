/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "game.h"

#include "../basetypes/fast_play.h"

#include "gameplay.h"
#include "gameplay_actions.h"

#include "../party/party.h"
#include "../party/rule.h"
#include "../player/playersDb.h"
#include "../player/ai/ai.h"
#include "../player/human/human.h"
#include "../card/trick.h"
#include "../ui/ui.h"
#include "../misc/preferences.h"
#include "../misc/bug_report.h"
#include "../os/bug_report_replay.h"
#include "../utils/random.h"

#include <ctime>


#ifdef DKNOF
#include "../card/weighted_card_list.h"
#endif

namespace {
auto discrepancy_from_preferences(Hand const& hand) -> unsigned;
} // namespace

void Game::play()
{
  if (status() != Status::init)
    return ;

redistribute: // if the cards have to be redistributed
  for (auto& p : *players_)
    p.game_open(*this);

  set_is_duty_solo();
  if (status() == Status::manual_cards_distribution) {
    teaminfo().reset();
    set_type(GameType::normal);
  } else if (status() == Status::redistribute) {
    status_ = Status::init;
    teaminfo().reset();
    set_type(GameType::normal);
  } else { // if !(status() == Status::redistribute)
    if (!isvirtual()) {
      ::ui->game_open(*this);
      Game::signal_open(*this);
    }
  } // if !(status() == Status::redistribute)

  if (status() != Status::init) {
    if (!isvirtual()) {
      signal_close();
      ::ui->game_close();
    }
    return ;
  } // if (status() != Status::init)

  status_ = Status::init;

manual_cards_distribution: // if the cards shall be manually distributed
  init();

  distribute_cards();
  party_->set_seed(seed_);

  if (status() != Status::cards_distribution)
    return ;

  status_ = Status::reservation;
  signal_cards_distributed();
  ::ui->game_cards_distributed();

  players().set_current_player(startplayer());

  try {
    get_reservations();

    if (::fast_play & FastPlay::seed_info) {
      print_seed_statistics();
      return ;
    }

    select_reservation();
  } catch (Game::Status const status) {
    if (status == Status::manual_cards_distribution) {
      status_ = status;
    } else {
      throw;
    }
  }

  if (::fast_play & FastPlay::seed_info)
    return ;

#ifndef RELEASE
  if (::debug("jack solo") && type() != GameType::solo_jack) {
    status_ = Status::redistribute;
  }
#endif

  if (   status() != Status::reservation
      && status() != Status::manual_cards_distribution
      && status() != Status::redistribute) {
    return ;
  }
  if (status() == Status::manual_cards_distribution) {
    goto manual_cards_distribution; // NOLINT(cppcoreguidelines-avoid-goto,hicpp-avoid-goto)
  }

  if (status() == Status::reservation)
    status_ = Status::start;

  // update the team info in 'game' and in the players
  teaminfo().set_at_gamestart();
  set_is_duty_solo();

  // set the startplayer
  if (   is_duty_solo()
      || (   rule()(Rule::Type::lustsolo_player_leads)
          && ::is_solo(type())) )
    players().set_current_player(players().soloplayer());
  else
    players().set_current_player(startplayer());

  if (!isvirtual()) {
    signal_start();
    ::ui->game_start();
  }
  for (auto& p : *players_) {
    p.game_start();
  }

  if (   !isvirtual()
      && ::debug("solo")) {
    if (!::is_solo(type())) {
      status_ = Status::redistribute;
    }
    ::ui->wait();
  }

  if (status() == Status::redistribute) {
    poverty().reset();

    set_seed(party_->next_seed());

    if (!isvirtual()) {
      signal_redistribute();
      ::ui->game_redistribute();
    }

    goto redistribute; // NOLINT(cppcoreguidelines-avoid-goto,hicpp-avoid-goto)
  } // if (status() == Status::redistribute)

  // testing swines
  swines().test_swines_from_reservations();

  if (status() != Status::start)
    return ;

  status_ = Status::play;

  // the reserve is needed because the players keep pointers to the tricks
  // in their trickpiles
  for (unsigned t = 0; t < rule()(Rule::Type::number_of_tricks_in_game); ++t) {
    try {
      tricks_.add(Trick(players().current_player()));
      for (auto& p : *players_)
        p.trick_open(tricks().current());
      gameplay_->add(GameplayActions::TrickOpen());
      if (!isvirtual()) {
        tricks().signal_trick_open(tricks().current());
        ::ui->trick_open(tricks().current());
        signal_gameplay_action(GameplayActions::TrickOpen());
        ::ui->gameplay_action(GameplayActions::TrickOpen());
      }
      announcements().request_from_players();

      while (!tricks().current().isfull()) {
        announcements().request_from_players();
        nextplayer();
        if (status() != Status::play)
          throw(status());
      } // while(!tricks().current().isfull())

      status_ = Status::full_trick;
      evaluatetrick();
      status_ = Status::play;
      announcements().request_from_players();
    } catch (...) {
      if (!isvirtual()) {
        tricks().signal_trick_close(tricks().current());
        ::ui->trick_close();
      }
      throw;
    } // try

    if (!isvirtual()) {
      tricks().signal_trick_close(tricks().current());
      ::ui->trick_close();
    }
  } // for (t < trickno())
}


void Game::play(Gameplay const& gameplay)
{
  for (auto const& action : gameplay.actions())
    play(*action);
}


// NOLINTNEXTLINE(misc-no-recursion)
void Game::play(GameplayAction const& action)
{
  switch (action.type()) {
  case GameplayActions::Type::reservation:
  case GameplayActions::Type::poverty_shift:
  case GameplayActions::Type::poverty_accepted:
  case GameplayActions::Type::poverty_returned:
  case GameplayActions::Type::poverty_denied:
  case GameplayActions::Type::poverty_denied_by_all:
    // already done
    break;
  case GameplayActions::Type::card_played:
    if (   !tricks().empty()
        && tricks().current().isfull()) {
      evaluatetrick();
    }
    if (   tricks().empty()
        || tricks().current().isfull()) {
      play(GameplayActions::TrickOpen());
    }
    play_card(dynamic_cast<GameplayActions::CardPlayed const&>(action).card);
    break;
  case GameplayActions::Type::announcement:
    announcements().make_announcement(dynamic_cast<GameplayActions::Announcement const&>(action).announcement,
                                      player(dynamic_cast<GameplayActions::Announcement const&>(action).player));
    break;
  case GameplayActions::Type::swines:
    swines().swines_announce(player(dynamic_cast<GameplayActions::Swines const&>(action).player));
    break;
  case GameplayActions::Type::hyperswines:
    swines().hyperswines_announce(player(dynamic_cast<GameplayActions::Hyperswines const&>(action).player));
    break;
  case GameplayActions::Type::marriage:
    // todo
    break;
  case GameplayActions::Type::trick_open:
    tricks_.add(Trick(players().current_player()));
    for (auto& p : *players_)
      p.trick_open(tricks().current());
    status_ = Status::play;
    break;
  case GameplayActions::Type::trick_full:
    evaluatetrick();
    break;
  case GameplayActions::Type::trick_taken:
  case GameplayActions::Type::check:
    // nothing to do
    break;
  case GameplayActions::Type::print:
    cout << dynamic_cast<GameplayActions::Print const&>(action).info << '\n';
    break;
  case GameplayActions::Type::quit:
    std::exit(EXIT_SUCCESS);
    break ;
  } // switch (action->type())
}


void Game::add(GameplayAction const& action)
{
  gameplay_->add(action);
}


void Game::init()
{
  players_->reset();
  poverty_.reset();
  marriage_.reset();
  swines_.reset();
  announcements_.init();
  teaminfo_.reset();
  finished_ = false;

  players().set_current_player(startplayer());

  set_type(GameType::normal);

  // initialise the players
  auto& players = *players_;
  for (auto& p : players)
    p.new_game(*this);
}


void Game::reset_to_first_trick()
{

#ifdef POSTPONED
  player_current_ = &player(startplayer().no());

  marriage_selector_ = MarriageSelector::team_set;
  marriage_determination_trickno_ = UINT_MAX;
  tricks_.clear();
  teaminfo_set_at_gamestart();
  // ToDo: add human to info
  announcement_.clear();
  swines_owner_ = nullptr;
  first_fox_catcher_ = nullptr;
  swines_announced_ = false;
  hyperswines_owner_ = nullptr;
  hyperswines_announced_ = false;
  finished_ = false;
  for (auto& p : *players_)
    p.set_hand({p->hand().cards_all()});

  if (gameplay_) {
    // gameplay
    Gameplay gameplay;
    gameplay.game_open(*this);
    gameplay.game_cards_distributed();
    gameplay.game_start();
    auto a = gameplay_->actions_.begin();
    for (;
         a != gameplay_->actions_.end();
         ++a) {
      gameplay.add(**a);
    } // for (a : gameplay().actions())
    for (auto a2 = a;
         a2 != gameplay().actions().end();
         ++a2)
      delete *a2;
    gameplay_->actions_.erase(a, gameplay_->actions_.end());

    for (unsigned p = 0; p < playerno(); ++p)
      gameplay_->hands_[p] = Hand(player(p),
                                  player(p).hand().cards_all());
  } // gameplay
#endif
}


auto Game::has_made_reservation(Player const& player) const -> bool
{
  if (status() < Status::reservation)
    return false;
  if (status() > Status::reservation)
    return true;

  return ( ( player.no()
            + playerno()
            - startplayer().no()
           ) % playerno()
          <  (players().current_player().no()
              + playerno()
              - startplayer().no()
             ) % playerno()
         );
} // bool Game::has_made_reservation(Player player) const


auto Game::reservation(Player const& player) const -> Reservation const&
{
  return player.reservation();
} // Reservation const& Game::reservation(Player const& player) const


void Game::distribute_cards()
{
  if (status() == Status::manual_cards_distribution) {
    distribute_cards_manually();
#ifdef BUG_REPORT_REPLAY
  } else if (::bug_report_replay) {
    distribute_cards_by_bug_report_replay();
#endif
  } else if (party().is_replayed_game()) {
    if (   players().count_humans() == 1
        && ::preferences(Preferences::Type::replayed_game_with_random_distribution_of_players)) {
      Hands hands(4);
      hands[players().human().no()] = party().hands_for_replay()[players().human().no()];
      distribute_cards(hands);
      fill_up_hands(true);
    } else {
      distribute_cards(party().hands_for_replay());
    }
  } else {
    distribute_cards_by_seed();
  }
}


void Game::distribute_cards_by_seed()
{
  status_ = Status::cards_distribution;

  auto const time_limit = std::chrono::steady_clock::now() + std::chrono::seconds(::preferences(Preferences::Type::max_attempts_sec_for_distribution));

  auto best_discrepancy = UINT_MAX;
  vector<vector<Card>> best_hands;
  do {
    vector<vector<Card>> hands(playerno());
    auto all_cards = rule().cards();
    for (unsigned i = 1; i < rule()(Rule::Type::number_of_same_cards); ++i) {
      all_cards.insert(all_cards.end(),
                       rule().cards().begin(),
                       rule().cards().end());
    }

    if (::preferences(Preferences::Type::shuffle_without_seed)) {
      // Take the random generator from the standard library
      // Bug reports are no longer replayable
      static auto rd = std::random_device();
      static auto rng = std::default_random_engine(rd());
      std::shuffle(all_cards.begin(), all_cards.end(), rng);
    }

    auto rand = make_unique<Random>(seed_);
    while (!all_cards.empty()) {
      for (auto& h : hands) {
        DEBUG_ASSERTION(!all_cards.empty(),
                        "Game::distribute_cards_by_seed():\n"
                        "  valid cards cannot be distributed between the players");
        auto const n = (*rand)(all_cards.size());
        auto c = all_cards.begin() + n;
        h.push_back(*c);
        all_cards.erase(c);
      } // for (h : hands)
    } // while (!cards.empty())

    if (players().count_humans() != 1) {
      best_hands = hands;
      break;
    }

    auto const hand = Hand(players().human(), hands[(players().human().no() + rule()(Rule::Type::number_of_players_in_game) - startplayer().no()) % playerno()]);
    auto const discrepancy = discrepancy_from_preferences(hand);
    if (discrepancy == 0) {
      best_hands = hands;
      break;
    }

    if (discrepancy < best_discrepancy) {
      best_hands = hands;
      best_discrepancy = discrepancy;
    }

    if (std::chrono::steady_clock::now() >= time_limit) {
      cerr << "Game: Time limit for cards distribution (" << ::preferences(Preferences::Type::max_attempts_sec_for_distribution) << " sec) has exceeded without finding a matching hand. Taking best cards distribution so far.\n";
      break;
    }

    seed_ += 1;
    ::ui->update();
  } while (true);

  // create the hands for the players
  for (unsigned p = 0; p < playerno(); p++) {
    player((p + startplayer().no()) % playerno()).set_hand(Hand{best_hands[p]});
  }
}


void Game::distribute_cards_manually()
{
  status_ = Status::manual_cards_distribution;
  vector<Game::Player*> players;
  for (unsigned p = 0; p < playerno(); p++) {
    players.push_back(&player((p + startplayer().no()) % playerno()));
  }

  for (auto& p : players) {
    p->set_hand(Hand(vector<Card>(rule()(Rule::Type::number_of_cards_on_hand), Card::unknown)));
  }
  ::ui->game_distribute_cards_manually();

  fill_up_hands();
  status_ = Status::cards_distribution;
}


#ifdef BUG_REPORT_REPLAY
void Game::distribute_cards_by_bug_report_replay()
{
  status_ = Status::cards_distribution;
  distribute_cards(::bug_report_replay->hands());
}
#endif


void Game::distribute_cards(Hands hands)
{
  status_ = Status::manual_cards_distribution;
  for (auto& hand : hands) {
    DEBUG_ASSERTION(hand.cardsnumber() <= rule()(Rule::Type::number_of_tricks_in_game),
                    "Game::distribute_cards(hands)\n"
                    "  Hand has more cards then allowed: " << hand.cardsnumber() << " > " << rule()(Rule::Type::number_of_tricks_in_game));
    hand.add(Card::unknown, rule()(Rule::Type::number_of_tricks_in_game) - hand.cardsnumber());
  }
  for (unsigned p = 0;
       p < playerno();
       p++) {
    player(p).set_hand(hands[p]);
  }

  status_ = Status::cards_distribution;
}


void Game::fill_up_hands(bool const random_seed)
{
  DEBUG_ASSERTION(   status() == Status::manual_cards_distribution
                  || status() == Status::cards_distribution,
                  "Game::fill_up_hands()\n"
                  "  game_status is not game_manual_cards_distribution but " << status());

  vector<Game::Player*> players;
  for (unsigned p = 0; p < playerno(); p++) {
    players.push_back(&player((p + startplayer().no()) % playerno()));
  }

  for (auto& p : players) {
    auto& hand = p->hand();
    hand.remove_all({Card::empty, Card::unknown});
    hand.remove_played_cards();
  }

  auto all_cards = rule().cards();
  for (unsigned i = 1; i < rule()(Rule::Type::number_of_same_cards); ++i) {
    all_cards.insert(all_cards.end(),
                     rule().cards().begin(),
                     rule().cards().end());
  }

  // remove the cards already in the hands
  for (auto const& p : players) {
    for (auto const& c : p->hand()) {
      auto it = container_algorithm::find(all_cards, c);
      DEBUG_ASSERTION(it != end(all_cards),
                      "Game::distribute_cards_manually():\n"
                      "  Card " << c << " cannot be removed from all_cards\n");
      all_cards.erase(it);
    }
  }

  if (::preferences(Preferences::Type::shuffle_without_seed)) {
    static auto rd = std::random_device();
    static auto rng = std::default_random_engine(rd());
    std::shuffle(all_cards.begin(), all_cards.end(), rng);
  }

  // distribute the remaining cards
  auto rand = make_unique<Random>(random_seed ? ::random_value.get() : seed_);

  while (!all_cards.empty()) {
    for (auto& p : players) {
      auto& hand = p->hand();
      if (hand.cardsnumber() == rule()(Rule::Type::number_of_cards_on_hand))
        continue;
      DEBUG_ASSERTION(!all_cards.empty(),
                      "Game::distribute_cards_manually():\n"
                      "  too few cards to distribute");
      auto const n = (*rand)(all_cards.size());
      auto c = all_cards.begin() + n;
      hand.add(*c);
      all_cards.erase(c);
    } // for (h : hands)
  } // while (!cards.empty())
}


void Game::get_reservations()
{
  auto& players = *players_;
  signal_reservation_ask(players.current_player());
  ::ui->reservation_ask(players.current_player());
  set_type(GameType::normal);
  players.current_player().get_reservation();
  gameplay_->add(GameplayActions::Reservation(players.current_player().no(),
                                              players.current_player().reservation()));
  signal_gameplay_action(GameplayActions::Reservation(players.current_player().no(),
                                                      players.current_player().reservation()));
  ::ui->gameplay_action(GameplayActions::Reservation(players.current_player().no(),
                                                     players.current_player().reservation()));
  signal_reservation_got(players.current_player().reservation(),
                         players.current_player());
  ::ui->reservation_got(players.current_player().reservation(),
                        players.current_player());
  if (status() != Status::reservation)
    return ;
  if (players.current_player().reservation().game_type == GameType::redistribute) {
    set_type(GameType::redistribute);
    status_ = Status::redistribute;
    players.set_soloplayer(players.current_player());
    return ;
  }

  if (party().is_duty_soli_round()) {
    DEBUG_ASSERTION(startplayer().is_remaining_duty_solo(startplayer().reservation().game_type),
                    "Game::get_reservations()\n"
                    "  Duty solo round and player still has to player a solo:\n"
                    << "  free duty solo:    " << startplayer().remaining_duty_free_soli() << '\n'
                    << "  free picture solo: " << startplayer().remaining_duty_picture_soli() << '\n'
                    << "  free color solo:   " << startplayer().remaining_duty_color_soli() << '\n'
                    << "  But he has selected no duty solo:\n"
                    << startplayer().reservation());
  }


  if (!(   party().is_duty_soli_round()
        && !reservation(startplayer()).offer_duty_solo) ) {
    for (players.next_current_player();
         players.current_player() != startplayer();
         players.next_current_player()) {
      set_type(GameType::normal);
      signal_reservation_ask(players.current_player());
      ::ui->reservation_ask(players.current_player());
      set_type(GameType::normal);
      players.current_player().get_reservation();
      gameplay_->add(GameplayActions::Reservation(players.current_player().no(),
                                                  players.current_player().reservation()));
      signal_gameplay_action(GameplayActions::Reservation(players.current_player().no(),
                                                          players.current_player().reservation()));
      ::ui->gameplay_action(GameplayActions::Reservation(players.current_player().no(),
                                                         players.current_player().reservation()));
      signal_reservation_got(players.current_player().reservation(),
                             players.current_player());
      ::ui->reservation_got(players.current_player().reservation(),
                            players.current_player());
      if (status() != Status::reservation)
        return ;
    }
  } // if (duty solo selected)
  set_type(GameType::normal);
}


void Game::select_reservation()
{
  auto& players = *players_;
  // testing a solo game
  for (unsigned i = 0;
       i < playerno();
       i++,
       players.next_current_player()) {
    if (::is_solo(reservation(players.current_player()).game_type)
        && rule()(reservation(players.current_player()).game_type)) {
      // the player 'p' is playing a solo
      set_type(reservation(players.current_player()).game_type);
      players.set_soloplayer(players.current_player());

      // no other player can play a solo
      if (!(   party().is_duty_soli_round()
            && (players.current_player() == startplayer())
            && reservation(players.current_player()).offer_duty_solo) )
        break;
    }
  } // for (i < playerno())

  for (unsigned i = 0;
       i < playerno();
       i++,
       players.next_current_player()) {
    if (reservation(players.current_player()).game_type == GameType::redistribute) {
      set_type(GameType::redistribute);
      status_ = Status::redistribute;
      players.set_soloplayer(players.current_player());
      break;
    } // if (reservation)
  } // for (i < playerno())

  if (   type() == GameType::normal
      || rule()(Rule::Type::throwing_before_solo) ) {
    // testing fox highest trump
    if (rule()(Rule::Type::throw_when_fox_highest_trump)) {
      for (unsigned i = 0;
           i < playerno();
           i++,
           players.next_current_player()) {
        if ( (reservation(players.current_player()).game_type
              == GameType::fox_highest_trump)) {
          // look, whether there is a card that is higher than the fox
          unsigned c = 0;
          for (; c < players.current_player().hand().cardsnumber(); c++) {
            auto const& card = players.current_player().hand().card(c);
            if (card.istrump() && !card.is_at_max_fox())
              break;
          }
          if (c == players.current_player().hand().cardsnumber()) {
            set_type(GameType::fox_highest_trump);
            status_ = Status::redistribute;
            players.set_soloplayer(players.current_player());
            break;
          } // if (c < player(p).hand().cardsnumber())
        } // if (reservation)
      } // for (p < playerno())
    } // if (rule()(Rule::Type::throw_when_fox_highest_trump))

    // testing thrown nines
    if (rule()(Rule::Type::throw_with_nines)) {
      for (unsigned i = 0;
           i < playerno();
           i++,
           players.next_current_player()) {
        if ( ((reservation(players.current_player()).game_type
               == GameType::thrown_nines)
             )
            && (players.current_player().hand().count(Card::nine)
                >= rule()(Rule::Type::min_number_of_throwing_nines))) {
          set_type(GameType::thrown_nines);
          status_ = Status::redistribute;
          players.set_soloplayer(players.current_player());
          break;
        } // if (reservation)
      } // for (p < playerno())
    } // if (rule()(Rule::Type::throw_with_nines))

    // testing thrown kings
    if (rule()(Rule::Type::throw_with_kings)) {
      for (unsigned i = 0;
           i < playerno();
           i++,
           players.next_current_player()) {
        if ( ((reservation(players.current_player()).game_type
               == GameType::thrown_kings)
             )
            && (players.current_player().hand().count(Card::king)
                >= rule()(Rule::Type::min_number_of_throwing_kings))) {
          set_type(GameType::thrown_kings);
          status_ = Status::redistribute;
          players.set_soloplayer(players.current_player());
          break;
        } // if (reservation)
      } // for (p < playerno())
    } // if (rule()(Rule::Type::throw_with_kings))

    // testing thrown nines and kings
    if (rule()(Rule::Type::throw_with_nines_and_kings)) {
      for (unsigned i = 0;
           i < playerno();
           i++,
           players.next_current_player()) {
        if ( ((reservation(players.current_player()).game_type
               == GameType::thrown_nines_and_kings)
             )
            && (players.current_player().hand().count(Card::nine)
                + players.current_player().hand().count(Card::king)
                >= rule()(Rule::Type::min_number_of_throwing_nines_and_kings))) {
          set_type(GameType::thrown_nines_and_kings);
          status_ = Status::redistribute;
          players.set_soloplayer(players.current_player());
          break;
        } // if (reservation)
      } // for (p < playerno())
    } // if (rule()(Rule::Type::throw_with_nines_and_kings))

    // testing thrown richness
    if (rule()(Rule::Type::throw_with_richness)) {
      for (unsigned i = 0;
           i < playerno();
           i++,
           players.next_current_player()) {
        if ( ((reservation(players.current_player()).game_type
               == GameType::thrown_richness)
             )
            && (players.current_player().hand().points()
                >= rule()(Rule::Type::min_richness_for_throwing))) {
          set_type(GameType::thrown_richness);
          status_ = Status::redistribute;
          players.set_soloplayer(players.current_player());
          break;
        } // if (reservation)
      } // for (p < playerno())
    } // if (rule()(Rule::Type::throw_with_richness))
  }

  if (type() == GameType::normal) {
    // testing a marriage
    for (unsigned i = 0;
         i < playerno();
         i++,
         players.next_current_player()) {
      if (   reservation(players.current_player()).game_type == GameType::marriage
          && players.current_player().hand().count(Card::club_queen) == 2) {
        set_type(GameType::marriage);
        players.set_soloplayer(players.current_player());
        marriage().selector_ = reservation(players.current_player()).marriage_selector;
        DEBUG_ASSERTION(rule()(marriage().selector()),
                        "Game::select_reservation():\n"
                        "  marriage selector '" << marriage().selector()
                        << "' is not valid.\n");
      }
    } // for (p < playerno())
  } // if (type() == GameType::normal)

  if (   (type() == GameType::normal)
      || (   is_solo()
          && rule()(Rule::Type::throwing_before_solo) )
      || (   (type() == GameType::marriage)
          && !rule()(Rule::Type::marriage_before_poverty) )
     ) {
    // testing a poverty
    if (rule()(Rule::Type::poverty)) {
      for (unsigned i = 0;
           i < playerno();
           i++,
           players.next_current_player()) {
        if (   (   (reservation(players.current_player()).game_type
                    == GameType::poverty)
               )
            && players.current_player().hand().has_poverty() ) {
          if (   rule()(Rule::Type::throwing_before_solo)
              && (type() != GameType::normal)
              && !(   (type() == GameType::marriage)
                   && !rule()(Rule::Type::marriage_before_poverty) )
              && rule()(Rule::Type::poverty_shift)
              && !(   (players.current_player().hand().count(Card::trump) > 1)
                   && rule()(Rule::Type::throw_with_one_trump) )
             )
            continue;
          players.set_soloplayer(players.current_player());
          set_type(GameType::poverty);
          swines().test_swines_from_reservations();
          poverty().shift();
          if (status() != Status::reservation)
            return ;
          break;
        } // if (reservation)
      } // for (p < playerno())
    } // if (rule()(Rule::Type::poverty))
  } // if (type() == GameType::normal)

  if (type() != GameType::marriage)
    marriage().selector_ = MarriageSelector::team_set;

  marriage().check_for_silent_marriage();
}


void Game::nextplayer()
{
  DEBUG_ASSERT(self_check());

  auto& current_player = players().current_player();
#ifdef POSTPONED
  DEBUG_ASSERTION((current_player.hand().cardsnumber()
                   == trickno() - tricks().current_no()),
                  "Game::nextplayer():\n"
                  "  current player has not the right number of cards on the hand\n"
                  "trick " << tricks().current_no() << "\n"
                  << current_player);
#endif


  // ***ToDo DK this uses the processor time
  const clock_t time_before_playing_card = ::clock();

  auto const played_card = current_player.card_get();

  DEBUG_ASSERTION(!played_card.is_empty(),
                  "Game::nextplayer():\n"
                  "  result of 'Player::card_get()' is an empty card.\n"
                  "  Player number: " << current_player.no() << "\n"
                  "  Player type:   " << current_player.type());
  if (!current_player.hand().contains(played_card)) {
    cerr << "hand:   " << current_player.hand() << '\n';
    cerr << "current trick:\n" << tricks().current() << '\n';
    cerr << "played card: " << played_card << '\n';
    cerr << "heuristic:   " << dynamic_cast<Ai const&>(current_player).last_heuristic() << '\n';
    cerr << dynamic_cast<Ai const&>(current_player).last_heuristic_rationale() << '\n';
    DEBUG_ASSERTION(current_player.hand().contains(played_card),
                    "Game::nextplayer(" + current_player.name() + ", " + to_string(played_card) + ")\n"
                    "The card is not on the hand:\n"
                    << current_player.hand());
  }

  // here always update,
  // so that the user can do something during the calculation of the ai
  ::ui->update();

  // testing, whether the card is valid.
  // if it is not, take the first valid card of the hand.
  if (!tricks().current().isvalid(played_card)
      && !(   isvirtual()
           && (current_player.hand().cardsnumber()
               > current_player.cards_to_play()))
     ) {
#ifndef RELEASE
    cerr << "ai type = " << played_card.player().type() << endl;
    cerr << "heuristic = " << dynamic_cast<Ai const&>(current_player
                                                     ).last_heuristic()
      << endl;
    cerr << *this;
#endif
#ifndef POSTPONED
    try {
      DEBUG_ASSERTION(false,
                      "Game::nextplayer()\n"
                      "  card \"" << played_card << "\" is not valid\n"
                      "hand: " << current_player.hand() << "\n"
                      "current trick:\n" << tricks().current() << "\n"
                      "heuristic:   " << dynamic_cast<Ai const&>(current_player).last_heuristic() << "\n"
                      "rationale:\n" << dynamic_cast<Ai const&>(current_player).last_heuristic_rationale() << "\n"
                     );
    } catch (...) {
      DEBUG_ASSERTION(false,
                      "Game::nextplayer()\n"
                      "  card \"" << played_card << "\" is not valid"
                      "hand: " << current_player.hand() << "\n"
                      "current trick:\n" << tricks().current());
    }
#else
    // *** the opposite team has won
#endif
  }

  if (   !(::fast_play & FastPlay::pause)
#ifdef BUG_REPORT_REPLAY
      && !(   ::bug_report_replay
           && ::bug_report_replay->auto_action())
#endif
     ) {

    // wait a bit, before the card is played
    // if it is the human who has to play
    // Bug: DK
    // When the time wraps around, it is assumed, that the calculation
    // has taken no time

    if (current_player.type() != Player::Type::human) {
      if (!isvirtual()) {

#ifdef POSTPONED
        // ToDo: cannot use this code when using 'clock()'
        while (time_before_playing_card
               + (::preferences(Preferences::Type::card_play_delay)
                  * static_cast<double>(CLOCKS_PER_SEC)
                  / 1000)
               > ::clock() ) {
#endif

          ::ui->sleep(max(0l,
                          static_cast<long>(::preferences(Preferences::Type::card_play_delay)
                                            - ((::clock()
                                                - time_before_playing_card)
                                               / (static_cast<double>(CLOCKS_PER_SEC)
                                                  / 1000) ))));

#ifdef POSTPONED
          // ToDo: cannot use this code when using 'clock()'
        } // while (still have to wait)
#endif
      } // if (!isvirtual())
    } // if (current_player.type() & Player::Type::ai)
  } // if (!(::fast_play & FastPlay::pause))

  play_card(current_player, played_card);

  // request announcement of the players
  announcements().request_from_players();
}


void Game::play_card(Player& player, Card const card)
{
  // check that it is the right player
  DEBUG_ASSERTION(player == players().current_player(),
                  "Game::play_card(" + player.name() + ", " + to_string(card) + ")\n"
                  "The player is not the current one (" + players().current_player().name() + ")");
  play_card(card);
}


void Game::play_card(Card const card_)
{
  auto& player = players().current_player();
  auto& hand = player.hand();

  if (   !hand.contains(card_)
      && hand.contains(Card::unknown)) {
    hand.set_known(card_);
  }
  // check that the hand contains the card
  DEBUG_ASSERTION(hand.contains(card_),
                  "Game::play_card(" + player.name() + ", " + to_string(card_) + ")\n"
                  "The card is not on the hand:\n"
                  << player.hand());

  auto const card = HandCard(hand, card_);

  // testing, whether the card is valid.
  // if it is not, take the first valid card of the hand.
  if (!tricks().current().isvalid(card)
      && !(   isvirtual()
           && hand.cardsnumber() > player.cards_to_play())
     ) {
    DEBUG_ASSERTION(false,
                    "Game::play_card(" + player.name() + ", " + to_string(card) + ")\n"
                    "Card is not valid.\n");
  }

  // remove the card from the hand, add it to the trick
  // and give the message to the ui
  hand.playcard(card);
  tricks().current().add(card);
  // set the next player as current player
  if (tricks().current().isfull())
    players().set_current_player(tricks().current().winnerplayer());
  else
    players().next_current_player();

  teaminfo().update();
  for (auto& p : players())
    p.card_played(card);

  marriage().determine_silent_marriage();
  if (isvirtual()) {
    signal_virtual_card_played(card);
  } else {
    signal_card_played(card);
    ::ui->card_played(card);
    auto heuristic = Aiconfig::Heuristic::no_heuristic;
    switch (player.type()) {
    case Player::Type::human:
    case Player::Type::ai:
      heuristic = dynamic_cast<Ai const&>(player).last_heuristic();
      break;
    case Player::Type::unset:
    case Player::Type::ai_dummy:
    case Player::Type::ai_random:
      heuristic = Aiconfig::Heuristic::no_heuristic;
      break;
    } // switch (player.type())

    gameplay_->add(GameplayActions::CardPlayed(player.no(),
                                               card,
                                               heuristic));
    signal_gameplay_action(GameplayActions::CardPlayed(player.no(),
                                                       card,
                                                       heuristic));
    ::ui->gameplay_action(GameplayActions::CardPlayed(player.no(),
                                                      card,
                                                      heuristic));
  }
}


void Game::set_trick_taken()
{
  status_ = Status::trick_taken;
}


void Game::evaluatetrick()
{
  auto const& trick = tricks().current();
  auto const& winnerplayer = trick.winnerplayer();

  // test for marriage
  marriage().determine_marriage();

  swines().evaluate_trick(trick);
  announcements().evaluate_trick(trick);

  // tell the players and the ui, that the trick is full
  for (auto& p : *players_)
    p.trick_full(tricks().current());
  if (isvirtual()) {
    signal_virtual_trick_full();

    // request announcement of the players
    announcements().request_from_players();

  } else { // if !(isvirtual())
    gameplay_->add(GameplayActions::TrickFull(tricks().current()));
    signal_gameplay_action(GameplayActions::TrickFull(tricks().current()));
    ::ui->gameplay_action(GameplayActions::TrickFull(tricks().current()));
    tricks().signal_trick_full(tricks().current());
    ::ui->trick_full();

    announcements().request_from_players();

    for (auto& p : *players_)
      p.trick_taken();

    announcements().request_from_players();

    status_ = Status::trick_taken;
    gameplay_->add(GameplayActions::TrickTaken());
    signal_gameplay_action(GameplayActions::TrickTaken());
    tricks().signal_trick_move_in_pile(tricks().current());
    ::ui->gameplay_action(GameplayActions::TrickTaken());
  } // if !(isvirtual())

  // move the trick in the trickpile
  tricks().current().move_in_trickpile();

  // Startplayer for next round
  players().set_current_player(winnerplayer);
}


void Game::finish()
{
  status_ = Status::finished;

  if (marriage().is_undetermined()) {
    auto const& soloplayer = players().soloplayer();
    marriage().determination_trickno_ = tricks().current_no();
    set_type(GameType::marriage_solo);
    for (auto& p : players()) {
      auto const team = ((p == soloplayer)
                         ? Team::re
                         : Team::contra);
      teaminfo_.set(p, team);
    }
    marriage().selector_ = MarriageSelector::team_set;
    // update the teaminfo of the players
    for (auto& p : players())
      p.marriage(soloplayer, soloplayer);
  } // if (undetermined marriage)
  if (   !isvirtual()
      && ::debug("solo")
      && ::is_solo(type())) {
    ::debug.ostr() << seed() << ": " << type() << ": ";
    if (winnerteam() == Team::re) {
      if (::debug("color"))
        ::debug.ostr() << "\e[01;32m"; // grün
      ::debug.ostr() << "re won: " << points_of_team(Team::re) << " points (" << players().soloplayer().name() << ").\n";
      if (::debug("color"))
        ::debug.ostr() << "\e[;0m";
    } else {
      if (::debug("color"))
        ::debug.ostr() << "\e[01;31m"; // rot
      auto const text = ("re lost: " + std::to_string(points_of_team(Team::re)) + " points (" + players().soloplayer().name() + ").\n");
      ::debug.ostr() << text;
#ifdef BUG_REPORT
      BugReport::create_bug_report(party(), text, "solo_decision");
#endif
      if (::debug("color"))
        ::debug.ostr() << "\e[;0m";
    }
  }

  for (auto& p : players()) {
    teaminfo_.set(p, p.team());
  }

  finished_ = true;
}


void Game::close()
{
  for (auto& p : *players_)
    p.game_close(*this);
  signal_close();
  ::ui->game_close();
}


void Game::print_seed_statistics() const
{
  // format:
  // seed
  // double club queen
  // double dolle
  // number blank colors
  // double cards

  // next line must be called in 'freedoko.cpp'
  static bool first_run = true;
  if (first_run) {
    cout << "Startwert;Hochzeit;Doppeldullen;blanke Farben;Doppelkarten\n";
    first_run = false;
  }
  cout << seed() << ';';
  bool marriage = false;
  bool double_dulle = false;
  bool club_blank = false;
  bool spade_blank = false;
  bool heart_blank = false;
  unsigned double_cards = 0;
  for (auto const& player : players()) {
    Hand const& hand = player.hand();
    if (hand.count(Card::club_queen) == 2)
      marriage = true;
    if (hand.count(Card::dulle) == 2)
      double_dulle = true;
    auto const is_double_card = [&hand](auto const card) { return hand.count(card) == 2; };
    double_cards += count_if(cards().cards(), is_double_card);
    if (hand.count(Card::club) == 0)
      club_blank = true;
    if (hand.count(Card::spade) == 0)
      spade_blank = true;
    if (hand.count(Card::heart) == 0)
      heart_blank = true;
  } // for (p)
  cout << marriage << ';';
  cout << double_dulle << ';';
  cout << (club_blank ? 1 : 0) + (spade_blank ? 1 : 0) + (heart_blank ? 1 : 0) << ';';
  cout << double_cards << ';';
  cout << '\n';
}


void Game::print_seed_statistics_for_players() const
{
  // format (for each player)
  // seed
  // playerno
  // playername
  // number trumps
  // number club queens
  // number dullen
  // number queens
  // number jacks
  // number swines
  // number hyperswines
  // number blank colors
  // number color aces
  // double cards
  // reservation (gametype)

  // next line must be called in 'freedoko.cpp'
  static bool first_run = true;
  if (first_run) {
    cout << "Startwert;Spielernummer;Name;Trümpfe;Kreuz Damen;Pik Damen;Dullen;Damen;Buben;Karo Asse;Hyperschweine;Kreuz;Pik;Herz;blanke Farben;Farbasse;Doppelkarten;Vorbehalt\n";
    first_run = false;
  }
  for (auto const& player : players()) {
    Hand const& hand = player.hand();
    cout << seed() << ';';
    cout << player.no() << ';' << player.name() << ';';
    cout << hand.count(Card::trump) << ';';
    cout << hand.count(Card::club_queen) << ';';
    cout << hand.count(Card::spade_queen) << ';';
    cout << hand.count(Card::dulle) << ';';
    cout << hand.count(Card::queen) << ';';
    cout << hand.count(Card::jack) << ';';
    cout << hand.count(Card::diamond_ace) << ';';
    cout << hand.count(rule()(Rule::Type::with_nines) ? Card::diamond_nine : Card::diamond_king) << ';';

    // cout << (hand.has_hyperswines() ? "hyperswines" : "") << ';';
    for (auto const color : {Card::club, Card::spade, Card::heart}) {
      cout << hand.count(color) << ';';
    }
    cout << 3 - (  (hand.contains(Card::club) ? 1 : 0)
                 + (hand.contains(Card::spade) ? 1 : 0)
                 + (hand.contains(Card::heart) ? 1 : 0)) << ';';
    cout << hand.count(Card::ace) - hand.count(Card::trump, Card::ace) << ';';
    cout << count_if(cards().cards(), [&hand](auto const card) { return hand.count(card) == 2; }) << ';';
    cout << player.reservation().game_type << ';';

    cout << '\n';
  }
}


namespace {
auto discrepancy_from_preferences(Hand const& hand) -> unsigned
{
  unsigned result = 0;

  auto discrepancy = [](unsigned const n,
                        Preferences::Type::Unsigned const min,
                        Preferences::Type::Unsigned const max) {
    return (  (n < preferences(min) ? quad(preferences(min) - n) : 0)
            + (n > preferences(max) ? quad(n - preferences(max)) : 0));
  };

  result += discrepancy(hand.count(Card::club_queen),
                        Preferences::Type::min_club_queens_to_distribute,
                        Preferences::Type::max_club_queens_to_distribute);

  result += discrepancy(hand.count(Card::diamond_ace),
                        Preferences::Type::min_diamond_aces_to_distribute,
                        Preferences::Type::max_diamond_aces_to_distribute);

  result += discrepancy(hand.count(Card::heart_ten),
                        Preferences::Type::min_heart_tens_to_distribute,
                        Preferences::Type::max_heart_tens_to_distribute);

  result += discrepancy(hand.count(Card::queen),
                        Preferences::Type::min_queens_to_distribute,
                        Preferences::Type::max_queens_to_distribute);

  result += discrepancy(hand.count(Card::jack),
                        Preferences::Type::min_jacks_to_distribute,
                        Preferences::Type::max_jacks_to_distribute);

  result += discrepancy(hand.count(Card::trump),
                        Preferences::Type::min_trumps_to_distribute,
                        Preferences::Type::max_trumps_to_distribute);

  result += discrepancy(hand.count(Card::club_ace)
                        + hand.count(Card::spade_ace)
                        + hand.count(Card::heart_ace),
                        Preferences::Type::min_color_aces_to_distribute,
                        Preferences::Type::max_color_aces_to_distribute);
  result += discrepancy(hand.count_blank_colors(GameType::normal),
                        Preferences::Type::min_blank_colors_to_distribute,
                        Preferences::Type::max_blank_colors_to_distribute);

  return result;
}

} // namespace
