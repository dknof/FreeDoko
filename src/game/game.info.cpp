/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "game.h"

#include "../player/player.h"
#include "../party/rule.h"
#include "../card/trick.h"


auto Game::points_of_player(Player const& player) const -> unsigned
{
  auto const points = [](Trick const& trick) {
    return trick.points();
  };
  auto const winnerplayer = [&player](Trick const& trick) {
    return (   trick.winnerplayer() == player
            && trick.intrickpile());
  };
  return sum_if(tricks(), points, winnerplayer);
}


auto Game::points_of_player(unsigned const playerno) const -> unsigned
{
  return points_of_player(player(playerno));
}


auto Game::points_of_player(Player const& player, unsigned trickno) const -> unsigned
{
  unsigned points = 0;

  if (trickno != UINT_MAX)
    trickno += 1;
  for (auto trick = tricks().begin();
       ((trickno > 0)
        && (trick != tricks().end()));
       ++trick, --trickno) {
    if (   (trick->winnerplayer() == player)
        && trick->intrickpile()) {
      points += trick->points();
    }
  }

  return points;
}


auto Game::points_of_player(unsigned const playerno, unsigned const trickno) const -> unsigned
{
  return points_of_player(player(playerno), trickno);
}


auto Game::points_of_team(Team const& team) const -> unsigned
{
  unsigned points = 0;

  for (auto const& p : players()) {
    if (p.team() == team)
      points += points_of_player(p);
  }

  return points;
}


auto Game::played_points() const -> unsigned
{
  unsigned points = 0;

  for (auto const& p : players())
    points += points_of_player(p);
  if (status() < Status::finished)
    points += tricks().current().points();

  return points;
}


auto Game::remaining_points() const -> unsigned
{
  return (240 - played_points());
}


auto Game::needed_points_to_win(Team const team) const -> unsigned
{
  Announcement const highest_bid_re
    = announcements().last(Team::re);
  Announcement const highest_bid_contra
    = announcements().last(Team::contra);

  switch (team) {
  case Team::re:
    if (   (highest_bid_re == Announcement::noannouncement)
        && (highest_bid_contra == Announcement::noannouncement) )
      return 121;
    else if (highest_bid_re == Announcement::noannouncement)
      return point_limit_for_opposite_team(highest_bid_contra);
    else
      return max(needed_points(highest_bid_re),
                 point_limit_for_opposite_team(highest_bid_contra));
  case Team::contra:
    if (   (highest_bid_contra == Announcement::noannouncement)
        && (highest_bid_re == Announcement::noannouncement) )
      return 120;
    else if (   (highest_bid_contra == Announcement::no120)
             && (highest_bid_re == Announcement::no120) )
      return 120;
    else if (highest_bid_contra == Announcement::noannouncement)
      return point_limit_for_opposite_team(highest_bid_re);
    else
      return max(needed_points(highest_bid_contra),
                 point_limit_for_opposite_team(highest_bid_re));
  default:
    return 0;
  } // switch(team)
}


auto Game::winnerteam() const -> Team
{
  // Siehe https://de.wikipedia.org/wiki/Doppelkopf#Spielauswertung_nach_den_Turnierspielregeln

  // The official rules (http://www.doko-verband.de/download/turnierspielregeln.pdf)
  // distinguish between two kinds of announcements:
  // "ANsagen" (re and contra, let's call them "calls") and
  // "ABsagen" (no90, no60 and so on, let's call them "bids").
  // This distinction is important when deciding on the winner team, because bids determine how
  // many points a party needs to win, while all calls are ignored with one exception:
  // if a game ends 120-120 and the only announcement was a contra call, the contra team
  // will lose (it wins all other 120-120 situations unless it has made a bid).

  DEBUG_ASSERTION((tricks().remaining_no() == 0),
                  "Game::winnerteam():\n"
                  "  game is not finished, yet\n"
                  << "current no: " << tricks().current_no() << '\n'
                  << "remaining no: " << tricks().remaining_no() << '\n');

  // the points the team has made
  auto const points_re     = points_of_team(Team::re);
  auto const points_contra = points_of_team(Team::contra);

  Announcement highest_bid_re     = announcements().last(Team::re);
  Announcement highest_bid_contra = announcements().last(Team::contra);

  // Eine Erwiderung ist für die Gewinnerermittlung nicht nötig
  if (highest_bid_re == Announcement::reply)
    highest_bid_re = Announcement::noannouncement;
  if (highest_bid_contra == Announcement::reply)
    highest_bid_contra = Announcement::noannouncement;
#ifdef WORKAROUND
  // Re/Kontra -> Reply, wenn die andere Partei eine Absage getätigt hat
  // Die Umwandlung no120 -> reply sollte schon früher im Spiel erfolgen
  if (!rule()(Rule::Type::knocking)) {
    if (highest_bid_re == Announcement::no120 && highest_bid_contra > Announcement::no120)
      highest_bid_re = Announcement::noannouncement;
    if (highest_bid_contra == Announcement::no120 && highest_bid_re > Announcement::no120)
      highest_bid_contra = Announcement::noannouncement;
  }
#endif

  // Now determine the winning team

  // Sonderfall: Feigheitsregel
  if (   rule(Rule::Type::cowardliness_dokolounge)
      && !highest_bid_re
      && !highest_bid_contra) {
    if (points_re < 30)
      return Team::re;
    if (points_contra < 30)
      return Team::contra;
  }

  // Case #1: no party made a bid
  // * there's always a winner
  // * if no call was made or both teams made calls or only the re team made a call:
  // * Re-Team wins with 121 or more points [according to official rules article 7.1.2,1-3]
  // * Contra-Team wins with 120 or more points [7.1.3,1-3]
  // * if only the contra team made a call:
  // * Re-Team wins with 120 ore more points [7.1.2,4] (see above)
  // * Contra-Team wins with 121 or more points [7.1.3,4] (see above)

  if (   highest_bid_re     == Announcement::noannouncement
      && highest_bid_contra == Announcement::noannouncement) {
    if (points_re > 120)
      return Team::re;
    else
      return Team::contra;
  }

  // special case: only contra has announced 'contra'
  if (   highest_bid_contra == Announcement::no120
      && highest_bid_re     == Announcement::noannouncement) {
    if (points_re >= 120)
      return Team::re;
    else
      return Team::contra;
  }

  // special case: only re has announced 're'
  if (   highest_bid_re     == Announcement::no120
      && (   highest_bid_contra == Announcement::noannouncement
          || highest_bid_contra == Announcement::no120) ) {
    if (points_contra >= 120)
      return Team::contra;
    else
      return Team::re;
  }

  // Case #2: at least one of the teams made a bid
  // * both teams are allowed to make bids but it's only possible for one team to fulfill its bid =>
  // * Re-Team wins if it fulfills its bid [7.1.2,5-6]
  // * Contra-Team wins if it fulfills its bid [7.1.3,5-6]

  // check if re fulfilled its bid
  if (   (   highest_bid_re == Announcement::no120
          && points_contra < 120)
      || (   highest_bid_re == Announcement::no90
          && points_contra < 90 )
      || (   highest_bid_re == Announcement::no60
          && points_contra < 60 )
      || (   highest_bid_re == Announcement::no30
          && points_contra < 30 )
      || (   highest_bid_re == Announcement::no0
          && !tricks().has_trick(Team::contra) ) ) {
    return Team::re;
  }

  // check if contra fulfilled its bid
  if (   (   highest_bid_contra == Announcement::no120 
          && points_re < 120 )
      || (   highest_bid_contra == Announcement::no90
          && points_re < 90  )
      || (   highest_bid_contra == Announcement::no60
          && points_re < 60 )
      || (   highest_bid_contra == Announcement::no30
          && points_re < 30 )
      || (   highest_bid_contra == Announcement::no0
          && !tricks().has_trick(Team::re) ) ) {
    return Team::contra;
  }

  // Case #3: at least one bid has been made, but no bid has been fulfilled
  // the case that no team has made an annoucement / bid is Case #1,
  // so here there exists at least one team with a bid
  // * Re-Team wins if it didn't make a bid [7.1.2,7-8]
  // * Contra-Team wins if it didn't make a bid [7.1.3,7-8]
  // * no winner if both teams made bids [7.1.4]

  if (!highest_bid_re)
    // contra has not fulfilled its announcement
    return Team::re;
  else if (!highest_bid_contra)
    // re has not fulfilled its announcement
    return Team::contra;
  else
    // no player has made enough points for his announcement
    return Team::noteam;
}
