/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "game.h"

#include "../party/party.h"
#include "../party/rule.h"
#include "../ui/ui.h"
#include "../misc/preferences.h"
#include "../player/human/human.h"


auto Game::playerno() const -> unsigned
{
  return rule()(Rule::Type::number_of_players_in_game);
}


auto Game::players() const -> GamePlayers const&
{
  return *players_;
}


auto Game::players() -> GamePlayers&
{
  return *players_;
}


auto Game::player(unsigned p) const -> GamePlayer const&
{
  return players().player(p);
}


auto Game::player(unsigned p) -> GamePlayer&
{
  return players().player(p);
}


auto Game::startplayer() const -> Player const&
{
  DEBUG_ASSERTION(startplayer_,
                  "Game::startplayer:\n"
                  "  startplayer_ == NULL");

  return *startplayer_;
}
