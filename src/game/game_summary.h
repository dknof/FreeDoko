/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../basetypes/bock_trigger.h"
#include "specialpoint.h"
#include "exception.h"
#include "game.h"
class Rule;
class Player;

/**
 ** The summary of a game:
 ** - seed
 ** - startplayer
 ** - gametype
 ** - gamepoints
 **/
class GameSummary {
public:
  GameSummary() = delete;
  explicit GameSummary(Game const& game);
  GameSummary(Rule const& rule, istream& istr);

  void write(ostream& ostr) const;
  void read(istream& istr);

  auto rule()                            const -> Rule const&;
  auto seed()                            const -> Seed;
  auto players_number()                  const -> unsigned;
  auto party_player(unsigned p)          const -> unsigned;
  auto game_player(Player const& player) const -> unsigned;
  auto has_played(Player const& player)  const -> bool;

  auto startplayer()                 const -> unsigned;
  auto soloplayer()                  const -> unsigned;
  auto game_type()                   const -> GameType;
  auto swines_player()               const -> unsigned;
  auto hyperswines_player()          const -> unsigned;
  auto highest_announcement_re()     const -> Announcement;
  auto highest_announcement_contra() const -> Announcement;
  auto winnerteam()                  const -> Team;

  auto is_solo()           const -> bool;
  auto is_duty_solo()      const -> bool;
  auto is_lust_solo()      const -> bool;
  auto startplayer_stays() const -> bool;
  auto next_startplayer()  const -> unsigned;

  auto is_replayed_game() const -> bool;

  auto trick_points(Player const& player) const -> unsigned;
  auto trick_points(Team team)            const -> unsigned;

  auto team_of_points()      const -> Team;
  auto get_points(Team team) const -> bool;
  auto points()              const -> int;
  auto points(Team team)     const -> int;

  auto get_points(Player const& player) const -> bool;
  auto points(Player const& player)     const -> int;

  auto team(Player const& player)    const -> Team;
  auto partner(Player const& player) const -> unsigned;

  auto specialpoints()          const -> Specialpoints const&;
  auto specialpoint(unsigned i) const -> Specialpoint const&;

  auto bock_multiplier() const -> int;
  auto bock_triggers()   const -> vector<BockTrigger> const&;

private:
  void evaluate(Game const& game);

private:
  Rule const* rule_        = nullptr;
  bool is_replayed_game_   = false;
  unsigned seed_           = UINT_MAX;
  unsigned players_number_ = 0;
  vector<unsigned> party_players_;
  unsigned startplayer_    = UINT_MAX;
  unsigned soloplayer_     = UINT_MAX;

  GameType game_type_          = GameType::normal;
  bool duty_solo_              = false;
  unsigned swines_player_      = UINT_MAX;
  unsigned hyperswines_player_ = UINT_MAX;
  vector<Team> teams_;

  Announcement highest_announcement_re_     = Announcement::noannouncement;
  Announcement highest_announcement_contra_ = Announcement::noannouncement;

  vector<unsigned> trick_points_;
  Team winnerteam_ = Team::noteam;
  int points_      = 0;

  Specialpoints specialpoints_;
  vector<BockTrigger> bock_triggers_;

private: // unused
}; // class GameSummary

// write the game summary into the output stream
auto operator<<(ostream& ostr, GameSummary const& game_summary) -> ostream&;
