/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "gameplay.h"
#include "gameplay_actions.h"

#include "game.h"
#include "../card/trick.h"
#include "../card/hand.h"
#include "../player/player.h"

Gameplay::Gameplay(Game const& game) :
  game_(&game)
{ }


Gameplay::Gameplay(Game const& game, Gameplay const& gameplay) :
  game_(&game),
  seed_(gameplay.seed_),
  gametype_(gameplay.gametype_),
  soloplayer_(gameplay.soloplayer_),
  hands_(gameplay.hands_)
{
  for (auto const& a : gameplay.actions_)
    actions_.push_back(a->clone());
  for (auto& h : hands_)
    h.set_player(game.player(h.player().no()));
}


Gameplay::~Gameplay() = default;


auto Gameplay::clone(Game const& game) const -> unique_ptr<Gameplay>
{
  return make_unique<Gameplay>(game, *this);
}


auto Gameplay::write(ostream& ostr) const -> ostream&
{
  ostr
    << "seed = " << seed_ << '\n'
    << "gametype = " << gametype_ << '\n'
    << "soloplayer = " << soloplayer_ << '\n';

  ostr
    << "hands\n"
    << "{\n";
  for (auto const& h : hands_) {
    ostr
      << "{\n"
      << h
      << "}\n";
  }
  ostr << "}\n";

  ostr
    << "actions\n"
    << "{\n";
  for (auto const& a : actions_)
    ostr << "  " << *a << '\n';
  ostr << "}\n";

  return ostr;
}


void Gameplay::cards_distributed()
{
  seed_ = game_->seed();

  hands_.clear();
  for (auto const& p : game_->players())
    hands_.emplace_back(p.hand());
}


void Gameplay::redistribute()
{
  actions_.clear();
}


void Gameplay::game_start()
{
  soloplayer_ = (  game_->is_solo()
                       ? game_->players().soloplayer().no()
                       : UINT_MAX);
}


auto Gameplay::actions() const -> vector<unique_ptr<GameplayAction>> const&
{
  return actions_;
}


void Gameplay::add(GameplayAction const& action)
{
  actions_.push_back(action.clone());
}


auto operator<<(ostream& ostr, Gameplay const& gameplay) -> ostream&
{
  gameplay.write(ostr);
  return ostr;
}
