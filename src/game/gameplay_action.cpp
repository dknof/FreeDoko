/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "gameplay_action.h"
#include "gameplay_actions.h"

#include "../card/trick.h"
#include "../card/hand.h"
#include "../player/player.h"

#include "../utils/string.h"

namespace GameplayActions {

auto GameplayAction::create(string const& line) -> unique_ptr<GameplayAction>
{
  if (line.empty())
    return {};

  auto const keyword
    = String::remove_blanks(  (std::find(line.begin(), line.end(), '=')
                               != line.end())
                            ? string(line.begin(),
                                     std::find(line.begin(), line.end(), '='))
                            : line
                           );

  if (keyword == to_string(Type::reservation))
    return make_unique<Reservation>(line);
  else if (keyword == to_string(Type::marriage))
    return make_unique<Marriage>(line);
  else if (keyword == to_string(Type::poverty_shift))
    return make_unique<PovertyShift>(line);
  else if (keyword == to_string(Type::poverty_accepted))
    return make_unique<PovertyAccepted>(line);
  else if (keyword == to_string(Type::poverty_returned))
    return make_unique<PovertyReturned>(line);
  else if (keyword == to_string(Type::poverty_denied))
    return make_unique<PovertyDenied>(line);
  else if (keyword == to_string(Type::poverty_denied_by_all))
    return make_unique<PovertyDeniedByAll>(line);
  else if (keyword == to_string(Type::announcement))
    return make_unique<Announcement>(line);
  else if (keyword == to_string(Type::swines))
    return make_unique<Swines>(line);
  else if (keyword == to_string(Type::hyperswines))
    return make_unique<Hyperswines>(line);
  else if (keyword == to_string(Type::card_played))
    return make_unique<CardPlayed>(line);
  else if (keyword == to_string(Type::trick_open))
    return make_unique<TrickOpen>(line);
  else if (keyword == to_string(Type::trick_full))
    return make_unique<TrickFull>(line);
  else if (keyword == to_string(Type::trick_taken))
    return make_unique<TrickTaken>(line);
  else if (   (keyword == to_string(Type::check))
           || (keyword == "CHECK"))
    return make_unique<Check>(line);
  else if (   (keyword == to_string(Type::print))
           || (keyword == "PRINT"))
    return make_unique<Print>(line);
  else if (   (keyword == to_string(Type::quit))
           || (keyword == "QUIT"))
    return make_unique<Quit>(line);

  return {};
}


auto GameplayAction::create(string const& line, istream& istr) -> unique_ptr<GameplayAction>
{
  if (line.empty())
    return {};

#ifndef OUTDATED
  // old bug report format (< 0.7.3)
  auto const keyword
    = String::remove_blanks
    ( (std::find(line.begin(), line.end(), ':')
       < std::find(line.begin(), line.end(), '='))
     ? string(line.begin(),
              std::find(line.begin(), line.end(), ':'))
     : (  (std::find(line.begin(), line.end(), '=')
           != line.end())
        ? string(line.begin(),
                 std::find(line.begin(), line.end(), '='))
        : line
       )
    );
#else
  auto const keyword
    = String::remove_blanks
    (  (std::find(line.begin(), line.end(), '=')
        != line.end())
     ? string(line.begin(),
              std::find(line.begin(), line.end(), '=') - 1)
     : line
    );
#endif

#ifndef OUTDATED
  // old bug report format (< 0.7.3)
  if (keyword == "Marriage")
    return make_unique<Marriage>(line);
  else if (keyword == "Poverty Shift")
    return make_unique<PovertyShift>(line);
  else if (keyword == "Poverty Returned")
    return make_unique<PovertyReturned>(line);
  else if (keyword == "Poverty Denied By All")
    return make_unique<PovertyDeniedByAll>(line);
  else if (keyword == "Announcement")
    return make_unique<Announcement>(line);
  else if (keyword == "Swines")
    return make_unique<Swines>(line);
  else if (keyword == "Hyperswines")
    return make_unique<Hyperswines>(line);
  else if (keyword == "Played")
    return make_unique<CardPlayed>(line);
  else if (keyword == "Trick full")
    return make_unique<TrickFull>(line, istr);
#endif

  if (keyword == to_string(Type::reservation))
    return make_unique<Reservation>(line);
  else if (keyword == to_string(Type::marriage))
    return make_unique<Marriage>(line);
  else if (keyword == to_string(Type::poverty_shift))
    return make_unique<PovertyShift>(line);
  else if (keyword == to_string(Type::poverty_accepted))
    return make_unique<PovertyAccepted>(line);
  else if (keyword == to_string(Type::poverty_returned))
    return make_unique<PovertyReturned>(line);
  else if (keyword == to_string(Type::poverty_denied))
    return make_unique<PovertyDenied>(line);
  else if (keyword == to_string(Type::poverty_denied_by_all))
    return make_unique<PovertyDeniedByAll>(line);
  else if (keyword == to_string(Type::announcement))
    return make_unique<Announcement>(line);
  else if (keyword == to_string(Type::swines))
    return make_unique<Swines>(line);
  else if (keyword == to_string(Type::hyperswines))
    return make_unique<Hyperswines>(line);
  else if (keyword == to_string(Type::card_played))
    return make_unique<CardPlayed>(line);
  else if (keyword == to_string(Type::trick_open))
    return make_unique<TrickOpen>(line);
  else if (keyword == to_string(Type::trick_full))
    return make_unique<TrickFull>(line, istr);
  else if (keyword == to_string(Type::trick_taken))
    return make_unique<TrickTaken>(line);
  else if (keyword == to_string(Type::check))
    return make_unique<Check>(line);
  else if (keyword == to_string(Type::print))
    return make_unique<Print>(line);
  else if (string(line, 0, to_string(Type::print).size()) == to_string(Type::print))
    return make_unique<Print>(line);
  else if (keyword == to_string(Type::quit))
    return make_unique<Quit>(line);

  return {};
}


auto GameplayAction::clone() const -> unique_ptr<GameplayAction>
{
  switch (type()) {
  case Type::reservation:
    return make_unique<Reservation>(dynamic_cast<Reservation const&>(*this));
  case Type::marriage:
    return make_unique<Marriage>(dynamic_cast<Marriage const&>(*this));
  case Type::poverty_shift:
    return make_unique<PovertyShift>(dynamic_cast<PovertyShift const&>(*this));
  case Type::poverty_accepted:
    return make_unique<PovertyAccepted>(dynamic_cast<PovertyAccepted const&>(*this));
  case Type::poverty_returned:
    return make_unique<PovertyReturned>(dynamic_cast<PovertyReturned const&>(*this));
  case Type::poverty_denied:
    return make_unique<PovertyDenied>(dynamic_cast<PovertyDenied const&>(*this));
  case Type::poverty_denied_by_all:
    return make_unique<PovertyDeniedByAll>(dynamic_cast<PovertyDeniedByAll const&>(*this));
  case Type::announcement:
    return make_unique<Announcement>(dynamic_cast<Announcement const&>(*this));
  case Type::swines:
    return make_unique<Swines>(dynamic_cast<Swines const&>(*this));
  case Type::hyperswines:
    return make_unique<Hyperswines>(dynamic_cast<Hyperswines const&>(*this));
  case Type::card_played:
    return make_unique<CardPlayed>(dynamic_cast<CardPlayed const&>(*this));
  case Type::trick_open:
    return make_unique<TrickOpen>(dynamic_cast<TrickOpen const&>(*this));
  case Type::trick_full:
    return make_unique<TrickFull>(dynamic_cast<TrickFull const&>(*this));
  case Type::trick_taken:
    return make_unique<TrickTaken>(dynamic_cast<TrickTaken const&>(*this));
  case Type::check:
    return make_unique<Check>(dynamic_cast<Check const&>(*this));
  case Type::print:
    return make_unique<Print>(dynamic_cast<Print const&>(*this));
  case Type::quit:
    return make_unique<Quit>(dynamic_cast<Quit const&>(*this));
  } // switch (type())

  return {};
}


auto GameplayAction::equal(GameplayAction const& action) const -> bool
{
  return (type() == action.type());
}


auto player(GameplayActions::GameplayAction const& action) -> unsigned
{
  using namespace GameplayActions;
  switch (action.type()) {
  case Type::reservation:
    return dynamic_cast<GameplayActions::Reservation const&>(action).player;
  case Type::marriage:
    return dynamic_cast<GameplayActions::Marriage const&>(action).player;
  case Type::poverty_shift:
    return dynamic_cast<GameplayActions::PovertyShift const&>(action).player;
  case Type::poverty_accepted:
    return dynamic_cast<GameplayActions::PovertyAccepted const&>(action).player;
  case Type::poverty_returned:
    return dynamic_cast<GameplayActions::PovertyReturned const&>(action).player;
  case Type::poverty_denied:
    return dynamic_cast<GameplayActions::PovertyDenied const&>(action).player;
  case Type::announcement:
    return dynamic_cast<GameplayActions::Announcement const&>(action).player;
  case Type::swines:
    return dynamic_cast<GameplayActions::Swines const&>(action).player;
  case Type::hyperswines:
    return dynamic_cast<GameplayActions::Hyperswines const&>(action).player;
  case Type::card_played:
    return dynamic_cast<GameplayActions::CardPlayed const&>(action).player;
  case Type::poverty_denied_by_all:
  case Type::trick_open:
  case Type::trick_full:
  case Type::trick_taken:
  case Type::check:
  case Type::print:
  case Type::quit:
    return UINT_MAX;
  } // switch (action.type())
  return UINT_MAX;
}


auto to_string(GameplayActions::Type const type) -> string
{
  using namespace GameplayActions;
  switch(type) {
  case Type::reservation:
    (void)_("GameplayAction::reservation");
    return "reservation";
  case Type::marriage:
    (void)_("GameplayAction::marriage");
    return "marriage";
  case Type::poverty_shift:
    (void)_("GameplayAction::poverty shift");
    return "poverty shift";
  case Type::poverty_accepted:
    (void)_("GameplayAction::poverty accepted");
    return "poverty accepted";
  case Type::poverty_returned:
    (void)_("GameplayAction::poverty returned");
    return "poverty returned";
  case Type::poverty_denied:
    (void)_("GameplayAction::poverty denied");
    return "poverty denied";
  case Type::poverty_denied_by_all:
    (void)_("GameplayAction::poverty denied by all");
    return "poverty denied by all";
  case Type::announcement:
    (void)_("GameplayAction::announcement");
    return "announcement";
  case Type::swines:
    (void)_("GameplayAction::swines");
    return "swines";
  case Type::hyperswines:
    (void)_("GameplayAction::hyperswines");
    return "hyperswines";
  case Type::card_played:
    (void)_("GameplayAction::card played");
    return "card played";
  case Type::trick_open:
    (void)_("GameplayAction::trick open");
    return "trick open";
  case Type::trick_full:
    (void)_("GameplayAction::trick full");
    return "trick full";
  case Type::trick_taken:
    (void)_("GameplayAction::trick taken");
    return "trick taken";
  case Type::check:
    (void)_("GameplayAction::check");
    return "check";
  case Type::print:
    (void)_("GameplayAction::print");
    return "print";
  case Type::quit:
    (void)_("GameplayAction::quit");
    return "quit";
  } // switch(type)

  DEBUG_ASSERTION(false,
                  "to_string(GameplayActions::Type type):\n"
                  "  unknown type '" << static_cast<int>(type) << "'");

  return "";
}


auto gettext(Type const type) -> string
{
  return ::gettext("GameplayAction::" + to_string(type));
}


auto operator<<(ostream& ostr, Type const type) -> ostream&
{
  return (ostr << to_string(type));
}


auto to_string(Discrepancy const discrepancy) -> string
{
  switch(discrepancy) {
  case Discrepancy::future:
    (void)_("Discrepancy::future");
    return "future";
  case Discrepancy::none:
    (void)_("Discrepancy::none");
    return "none";
  case Discrepancy::skipped:
    (void)_("Discrepancy::skipped");
    return "skipped";
  case Discrepancy::player:
    (void)_("Discrepancy::player");
    return "player";
  case Discrepancy::card:
    (void)_("Discrepancy::card");
    return "card";
  case Discrepancy::other:
    (void)_("Discrepancy::other");
    return "other";
  } // switch(discrepancy)

  DEBUG_ASSERTION(false,
                  "to_string(Discrepancy discrepancy):\n"
                  "  unknown discrepancy "
                  << "'" << static_cast<int>(discrepancy) << "'");

  return "";
}


auto operator<<(ostream& ostr, GameplayActions::Discrepancy const discrepancy) -> ostream&
{
  return (ostr << to_string(discrepancy));
}


auto operator!(GameplayActions::Discrepancy const discrepancy) -> bool
{
  return (discrepancy == GameplayActions::Discrepancy::none);
}


auto operator||(GameplayActions::Discrepancy const lhs, bool const rhs) -> bool
{
  return ((lhs != GameplayActions::Discrepancy::none) || rhs);
}


auto operator<<(ostream& ostr, GameplayAction const& action) -> ostream&
{
  action.write(ostr);
  return ostr;
}


auto to_string(GameplayAction const& action) -> string
{
  ostringstream ostr;
  action.write(ostr);
  return ostr.str();
}


auto operator==(GameplayAction const& lhs, GameplayAction const& rhs) -> bool
{
  return rhs.equal(lhs);
}


auto operator!=(GameplayAction const& lhs, GameplayAction const& rhs) -> bool
{
  return !(lhs == rhs);
}

} // namespace GameplayActions
