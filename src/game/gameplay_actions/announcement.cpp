/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "announcement.h"

#include "../../utils/string.h"

namespace GameplayActions {

Announcement::Announcement(unsigned const playerno,
                           ::Announcement const& announcement) :
  BasePlayer(playerno),
  announcement(announcement)
{ }


Announcement::Announcement(string line)
{
#ifndef OUTDATED
  // old format in the bug report:
  //   Announcement: player 1: no 120
  if (string(line, 0, strlen("Announcement: ")) == "Announcement: ") {
    // there is an announcement, look, which player has made it
    String::word_first_remove_with_blanks(line);
    String::word_first_remove_with_blanks(line);
    player = static_cast<unsigned>(atoi(line.c_str()));

    // read the announcement
    String::word_first_remove_with_blanks(line);
    announcement = announcement_from_string(line);

    return ;
  }
#endif

  // ex: announcement = 0, no 90
  auto const pre = to_string(type()) + " =";
  if (string(line, 0, pre.length()) != pre)
    return ;

  istringstream istr(string(line, pre.length()));
  istr >> player;
  if (istr.get() != ',')
    return ;
  if (istr.get() != ' ')
    return ;
  string announcement_name;
  getline(istr, announcement_name);
  announcement = announcement_from_string(announcement_name);
}


auto Announcement::equal(GameplayAction const& action) const -> bool
{
  return (   BasePlayer::equal(action)
          && announcement == dynamic_cast<Announcement const&>(action).announcement);
}


void Announcement::write(ostream& ostr) const
{
  ostr << type();
  ostr << " = " << player;
  ostr << ", " << announcement;
}


auto Announcement::data_translation() const -> string
{
  return (std::to_string(player)
          + ": "
          + _(announcement));
}

} // namespace GameplayActions
