/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "base_player.h"
#include "../../card/card.h"

namespace GameplayActions {

/**
 ** base class for gameplay action of one player with cards
 ** t.i. there is one player who has initiated the action and it contains cards
 **/
template<Type type>
  struct BaseCards : public BasePlayer<type> {
    BaseCards() = default;
    BaseCards(unsigned playerno, vector<Card> cards);
    BaseCards(Player const& player, vector<Card> cards);
    explicit BaseCards(string const& line);

    ~BaseCards() override = default;

    auto equal(GameplayAction const& action) const -> bool override;

    void write(ostream& ostr) const override;

    auto data_translation() const -> string override;

    vector<Card> cards;
  }; // struct BaseCards : public BasePlayer<type>

} // namespace GameplayActions
#include "base_cards.hpp"
