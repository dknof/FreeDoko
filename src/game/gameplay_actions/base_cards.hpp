/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "base_cards.h"

#include "../../utils/string.h"

namespace GameplayActions {

template<Type type>
  BaseCards<type>::BaseCards(unsigned const player,
			     vector<Card> cards_) :
    BasePlayer<type>(player),
    cards(cards_)
{ }

template<Type type>
BaseCards<type>::BaseCards(Player const& player,
			   vector<Card> cards_) :
  BasePlayer<type>(player),
  cards(cards_)
{ }

template<Type type>
BaseCards<type>::BaseCards(string const& line) :
  BaseCards<type>()
{
  auto const pre = to_string(this->type()) + " =";
  if (string(line, 0, pre.length()) != pre)
    return ;

  // first split the line in the single parts
  auto const parts(String::split(string(line, pre.length()),
				 ','));
  auto p = parts.begin();

  if (p == parts.end()) {
    this->player = UINT_MAX;
    return ;
  }
  { // read the player
    if (*p == "-") {
      this->player = UINT_MAX;
      return ;
    }

    istringstream istr(*p);
    istr >> this->player;
    ++p;
  } // read the player

  { // read the cards
    for (; p != parts.end(); ++p)
      this->cards.push_back(Card(*p));
  } // read the cards
}

template<Type type>
auto BaseCards<type>::equal(GameplayAction const& action) const -> bool
{
  if (!this->BasePlayer<type>::equal(action))
    return false;

  auto const& rhs = static_cast<BaseCards<type> const&>(action);
  if (this->cards.size() != rhs.cards.size())
    return false;

  for (size_t i = 0; i < this->cards.size(); ++i)
    if (this->cards[i] != rhs.cards[i])
      return false;

  return true;
}

template<Type type>
void BaseCards<type>::write(ostream& ostr) const
{
  ostr << this->type();
  ostr << " = " << this->player;
  for (auto const& c : this->cards)
    ostr << ", " << c;
}

template<Type type>
auto BaseCards<type>::data_translation() const -> string
{
  auto c = this->cards.begin();
  if (c == this->cards.end())
    return {};

  auto translation = gettext(*c);
  for (++c; c != this->cards.end(); ++c)
    translation += ", " + gettext(*c);

  return translation;
}

} // namespace GameplayActions
