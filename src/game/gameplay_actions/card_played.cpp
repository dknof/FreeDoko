/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "card_played.h"

#include "../../utils/string.h"

namespace GameplayActions {

CardPlayed::CardPlayed(unsigned const playerno,
                       Card const card) :
  BasePlayer(playerno),
  card(card)
{ }

CardPlayed::CardPlayed(unsigned const playerno,
                       Card const card,
                       Aiconfig::Heuristic const heuristic) :
  BasePlayer(playerno),
  card(card),
  heuristic(heuristic)
{ }

CardPlayed::CardPlayed(string line)
{
#ifndef OUTDATED
  // old format in the bug report:
  //   Played: Player 2: club ace
  if (string(line, 0, strlen("Played: ")) == "Played: ") {
    // the first word is 'Played'
    String::word_first_remove_with_blanks(line);
    // the next word is 'player', remove it also
    String::word_first_remove_with_blanks(line);
    // next comes the player number, followed by a ':'
    player = static_cast<unsigned>(atoi(line.c_str()));
    // remove the player number
    String::word_first_remove_with_blanks(line);

    // the last part is the card
    card = Card(line);

    return ;
  }
#endif

  // example: card played = 0, club ace
  auto const pre = to_string(type());
  if (string(line, 0, pre.length()) != pre)
    return ;

  istringstream istr(string(line, pre.length()));
  while (istr.good() && !istr.eof() && isspace(istr.peek()))
    (void)istr.get();
  if (!istr.good() || istr.eof())
    return ;
  if (istr.peek() != '=')
    return ;
  (void)istr.get();

  istr >> player;
  if (istr.get() != ',')
    return ;
  istr >> card;


  // ToDo: read heuristic
  while (   istr.good()
         && ::isspace(istr.peek()))
    istr.get();

  if (!istr.good())
    return ;

  if (istr.peek() != '(')
    return ;
  istr.get();

  string heuristic_name;
  while (   istr.good()
         && (istr.peek() != ')'))
    heuristic_name.push_back(istr.get());

  if (istr.peek() != ')')
    return ;

  heuristic = Aiconfig::Heuristic_from_name(heuristic_name);
}

auto CardPlayed::equal(GameplayAction const& action) const -> bool
{
  return (   (BasePlayer::equal(action))
          && (card == dynamic_cast<CardPlayed const&>(action).card)
          //&& (heuristic == dynamic_cast<CardPlayed const&>(action).heuristic)
         );
}

void CardPlayed::write(ostream& ostr) const
{
  ostr << type();
  ostr << " = " << player;
  ostr << ", " << card;

  if (heuristic != Aiconfig::Heuristic::no_heuristic)
    ostr << " (" << heuristic << ")";
}

auto CardPlayed::data_translation() const -> string
{
  if (heuristic == Aiconfig::Heuristic::no_heuristic)
    return _(card);
  else
    return (_(card)
            + " (" + _(heuristic) + ")");
}

} // namespace GameplayActions
