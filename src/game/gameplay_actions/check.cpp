/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "check.h"

#include "../../utils/string.h"

namespace GameplayActions {

Check::Check(string const& line)
{
  auto const pre = to_string(type()) + " =";
  if (string(line, 0, pre.length()) != pre)
    return ;

  comment = string(line, pre.length());
  String::remove_blanks(comment);
}

void Check::write(ostream& ostr) const
{
  ostr << type();
  if (!comment.empty())
    ostr << " = " << comment;
}

auto Check::data_translation() const -> string
{
  return comment;
}

} // namespace GameplayActions

