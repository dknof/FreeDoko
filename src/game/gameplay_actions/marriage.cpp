/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "marriage.h"

#include "../../utils/string.h"

namespace GameplayActions {

Marriage::Marriage(unsigned const bridegroom,
                   unsigned const bride) :
  BasePlayer(bridegroom),
  bride(bride)
{ }

Marriage::Marriage(string line)
{
#ifndef OUTDATED
  // old format in the bug report:
  //   Played: Player 2: club ace
  if (string(line, 0, strlen("Marriage: ")) == "Marriage: ") {
    // ToDo

    return ;
  }
#endif

  // ex: marriage = 0, 1
  auto const pre = to_string(type()) + " =";
  if (string(line, 0, pre.length()) != pre)
    return ;

  istringstream istr(string(line, pre.length()));
  istr >> player;
  if (istr.get() != ',')
    return ;
  istr >> bride;
}

auto Marriage::equal(GameplayAction const& action) const -> bool
{
  return (   BasePlayer::equal(action)
          && (bride == dynamic_cast<Marriage const&>(action).bride));
}

void Marriage::write(ostream& ostr) const
{
  ostr << type();
  ostr << " = " << player << ", " << bride;
}

auto Marriage::data_translation() const -> string
{
  return std::to_string(bride);
}

} // namespace GameplayActions
