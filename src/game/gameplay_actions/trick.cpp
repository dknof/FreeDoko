/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "trick.h"

#include "../../utils/string.h"

namespace GameplayActions {

TrickFull::TrickFull(Trick const& trick) :
  trick(trick)
{ }

TrickFull::TrickFull(string const& line)
{
  // ex: trick full = 0, diamond queen, diamond jack, spade queen, diamond nine, 2

  auto const pre = to_string(type()) + " =";
  if (string(line, 0, pre.length()) != pre)
    return ;

  // first split the line in the single parts
  auto const parts(String::split(string(line, pre.length()),
						    ','));
  auto p = parts.begin();
  if (p == parts.end())
    return ;

  unsigned startplayer = UINT_MAX;
  unsigned winnerplayer = UINT_MAX;
  vector<Card> cards;
  { // read the player
    if (*p == "-")
      return ;

    istringstream istr(*p);
    istr >> startplayer;
    ++p;
  } // read the player

  { // read the cards
    for (; p != parts.end(); ++p) {
      if (isdigit((*p)[0]))
	  break;
      cards.emplace_back(*p);
    }
  } // read the cards
  { // read the winnerplayer
    if (p != parts.end()) {
      istringstream istr(*p);
      istr >> winnerplayer;
      ++p;
    }
  } // read the winnerplayer
  if (p != parts.end())
    return ;

  trick = Trick(startplayer, cards, winnerplayer);
}

TrickFull::TrickFull(string const& line, istream&)
{
  // copied from 'TrickFull::TrickFull(string line)'

  // ex: trick full = 0, diamond queen, diamond jack, spade queen, diamond nine, 2

    auto const pre = to_string(type()) + " =";
    if (string(line, 0, pre.length()) != pre)
      return ;

    // first split the line in the single parts
    auto const parts(String::split(string(line, pre.length()),
						      ','));
    auto p = parts.begin();
    if (p == parts.end())
      return ;

    unsigned startplayer = UINT_MAX;
    unsigned winnerplayer = UINT_MAX;
    vector<Card> cards;
    { // read the player
      if (*p == "-")
	return ;

      istringstream istr(*p);
      istr >> startplayer;
      ++p;
    } // read the player

    { // read the cards
      for (; p != parts.end(); ++p) {
	if (isdigit((*p)[0]))
	  break;
        cards.emplace_back(*p);
      }
    } // read the cards
    { // read the winnerplayer
      if (p != parts.end()) {
	istringstream istr(*p);
	istr >> winnerplayer;
	++p;
      }
    } // read the winnerplayer
    if (p != parts.end())
      return ;

    trick = Trick(startplayer, cards, winnerplayer);
}

auto TrickFull::equal(GameplayAction const& action) const -> bool
{
  return (   Base::equal(action)
	  && (trick == dynamic_cast<GameplayActions::TrickFull const&>(action).trick));
}

void TrickFull::write(ostream& ostr) const
{
  ostr << type() << " = ";
  if (trick.startplayerno() == UINT_MAX) {
    ostr << "-";
    return ;
  }

  ostr << trick.startplayerno();
  for (unsigned c = 0; c < trick.actcardno(); ++c)
    ostr << ", " << trick.card(c);

  // cannot use 'Trick::isfull()' because that needs a corresponding game/party
  if (trick.winnerplayerno() != UINT_MAX)
    ostr << ", " << trick.winnerplayerno();
}

auto TrickFull::data_translation() const -> string
{
  if (trick.startplayerno() == UINT_MAX)
    return "-";

  string translation;

  translation += std::to_string(trick.startplayerno());

  for (unsigned c = 0; c < trick.actcardno(); ++c)
    translation += ", " + _(trick.card(c));

  if (trick.winnerplayerno() != UINT_MAX)
    translation += ", " + std::to_string(trick.winnerplayerno());

  return translation;
}

} // namespace GameplayActions
