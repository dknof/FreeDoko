/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "game.h"
#include "announcements.h"

#include "gameplay.h"
#include "gameplay_actions/marriage.h"

#include "../party/rule.h"
#include "../player/player.h"
#include "../card/trick.h"
#include "../ui/ui.h"

GameMarriage::GameMarriage(Game& game) noexcept :
  game_(&game)
{ }

GameMarriage::GameMarriage(GameMarriage const& marriage, Game& game) noexcept :
  game_(&game),
  selector_(marriage.selector_),
  determination_trickno_(marriage.determination_trickno_)
{ }

auto GameMarriage::selector() const noexcept -> MarriageSelector
{ return selector_; }

void GameMarriage::set_selector(MarriageSelector const selector)
{
#ifndef WEBSOCKETS_SERVICE
  DEBUG_ASSERTION(   game_->status() == Game::Status::reservation
                  || game_->status() == Game::Status::start,
                  "MarriageSelector::set_selector(" << selector << ")\n"
                  "  Is only allowed in the reservation or at game start, but the game status is " << game_->status());
#endif
  selector_ = selector;
}

auto GameMarriage::determination_trickno() const noexcept -> unsigned
{ return determination_trickno_; }


void GameMarriage::check_for_silent_marriage()
{
  if (game_->status() == Game::Status::redistribute)
    return ;

  DEBUG_ASSERTION((game_->status() == Game::Status::reservation),
                  "GameMarriage::check_for_silent_marriage()\n"
                  "  game status is not 'reservation' "
                  "but '" << game_->status() << "'");

  auto& game = *game_;
  auto const& rule = game.rule();

  if (game.type() != GameType::normal)
    return ;

  for (auto& p : game.players()) {
    if (p.hand().count(Card::club_queen)
        == rule(Rule::Type::number_of_same_cards)) {
      game.players().set_soloplayer(p);
      game.set_type(GameType::marriage_silent);
      selector_ = MarriageSelector::silent;
      determination_trickno_ = UINT_MAX;
      break;
    } // if (silent marriage)
  } // for (p : game.players())
}

void GameMarriage::determine_silent_marriage()
{
  auto& game = *game_;
  auto const& rule = game.rule();

  if (   game.type_ == GameType::marriage_silent
      && selector() == MarriageSelector::team_set)
    return ;
  if (   game.type_ != GameType::normal
      && game.type_ != GameType::marriage_silent
      && game.type_ != GameType::marriage)
    return ;
  if (   game.type_ == GameType::marriage
      && selector() != MarriageSelector::silent)
    return ;

  // whether the silent marriage is known to all
  bool solved = false;
  Player const* soloplayer = nullptr;

  { // check whether both club queens have been played
    for (auto const& p : game.players()) {
      if (p.hand().count_played(Card::club_queen) == rule(Rule::Type::number_of_same_cards) ) {
        solved = true;
        soloplayer = &p;
      }
    }
  } // check whether both club queens have been played

  { // check whether all other players are known to be contra
    unsigned contra_no = 0;
    for (auto& p : game.players())
      if (game.teaminfo().get(p) == Team::contra)
        contra_no += 1;

    if (contra_no == game.playerno() - 1) {
      solved = true;
      for (auto const& p : game.players()) {
        if (game.teaminfo().get(p) != Team::contra)
          soloplayer = &p;
      }
    }
  } // check whether all other players are known to be contra

  if (solved) {
    game.set_type(GameType::marriage_silent);
    game.players().set_soloplayer(game.players().player(soloplayer->no()));
    for (auto& p : game.players())
      if (p.team() == Team::unknown)
        p.set_team(Team::contra);
    selector_ = MarriageSelector::team_set;
    determination_trickno_ = game.tricks().current_no();
  } // if (solved)
}

void GameMarriage::determine_marriage()
{
  if (!is_undetermined())
    return ;

  auto& game = *game_;
  auto const& rule = game.rule();
  auto const& tricks = game.tricks();
  auto const& current_trick = tricks.current();
  auto const& soloplayer = game.players().soloplayer();

  // whether the player has to play alone
  // note that 'tricks().current_no()' begins at '0'
  // but 'determination' at '1'
  bool alone = (tricks.current_no() >= rule(Rule::Type::marriage_determination));
  auto const& winnerplayer = current_trick.winnerplayer();

  if (   rule(Rule::Type::marriage_first_one_decides)
      && is_selector(current_trick.startcard().tcolor(), selector())
      && (winnerplayer == soloplayer) )
    alone = true;

  if (   is_selector(current_trick.startcard().tcolor(), selector())
      && (winnerplayer != soloplayer) ) {
    // set the teams
    for (auto& p : game.players()) {
      auto const team = ((   (p == soloplayer)
                          || (p == winnerplayer))
                         ? Team::re
                         : Team::contra);
      game.teaminfo_.set(p, team);
      p.set_team(team);
    }
    selector_ = MarriageSelector::team_set;
    determination_trickno_ = tricks.current_no();
    // update the teaminfo of the players
    for (auto& p : game.players()) {
      p.marriage(soloplayer, winnerplayer);
    }

    game.gameplay_->add(GameplayActions::Marriage(soloplayer.no(), winnerplayer.no()));
    if (!game.isvirtual()) {
      ::ui->marriage(soloplayer, winnerplayer);
      signal_marriage(soloplayer, winnerplayer);
      ::ui->gameplay_action(GameplayActions::Marriage(soloplayer.no(),
                                                      winnerplayer.no()));
      game.signal_gameplay_action(GameplayActions::Marriage(soloplayer.no(),
                                                            winnerplayer.no()));
    }

    alone = false;
  } // if selected trick is caught


  if (   selector() != MarriageSelector::team_set
      && !alone) {
    // test, whether there is a remaining card of the color
    bool has_card = false; // whether a player has a card of the color
    for (auto& p : game.players()) {
      switch(selector()) {
      case MarriageSelector::silent:
      case MarriageSelector::team_set:
      case MarriageSelector::first_foreign:
        has_card = true;
        break;
      case MarriageSelector::first_trump:
        if (p.hand().contains(Card::trump))
          has_card = true;
        break;
      case MarriageSelector::first_color:
        if (   p.hand().contains(Card::club)
            || p.hand().contains(Card::spade)
            || p.hand().contains(Card::heart))
          has_card = true;
        break;
      case MarriageSelector::first_club:
        if (p.hand().contains(Card::club))
          has_card = true;
        break;
      case MarriageSelector::first_spade:
        if (p.hand().contains(Card::spade))
          has_card = true;
        break;
      case MarriageSelector::first_heart:
        if (p.hand().contains(Card::heart))
          has_card = true;
        break;
      } // switch (selector())
    } // for (p : players())

    if (!has_card)
      alone = true;
  } // if (!alone)

  if (alone) {
    determination_trickno_ = tricks.current_no();
    game.set_type(GameType::marriage_solo);
    for (auto& p : game.players()) {
      auto const team = (  (p == soloplayer)
                         ? Team::re
                         : Team::contra);
      game.teaminfo_.set(p, team);
      p.set_team(team);
    } // for (p : players())
    selector_ = MarriageSelector::team_set;

    for (auto& p : game.players())
      p.marriage(soloplayer, soloplayer);

    game.gameplay_->add(GameplayActions::Marriage(soloplayer.no(), soloplayer.no()));
    if (!game.isvirtual()) {
      ::ui->marriage(soloplayer, soloplayer);
      signal_marriage(soloplayer, soloplayer);
      ::ui->gameplay_action(GameplayActions::Marriage(soloplayer.no(),
                                                      soloplayer.no())
                           );
      game.signal_gameplay_action(GameplayActions::Marriage(soloplayer.no(),
                                                            soloplayer.no())
                                 );
    }
  } // if (acttrick() >= rule(Rule::Type::determination))

  // it can be, that the team of the swines owner or the first fox catcher
  // has changed
  if (rule(Rule::Type::swine_only_second)
      && game.swines().swines_announced()
      && game.swines().first_fox_caught()
      && (selector() == MarriageSelector::team_set)){
    if (!game.isvirtual())
      ::ui->redraw_all();
  } // if (first fox of swines in trick)

  game_->announcements().request_from_players();
}

void GameMarriage::reset() noexcept
{
  selector_ = MarriageSelector::team_set;
  determination_trickno_ = 0;
}

auto GameMarriage::is_undetermined() const noexcept -> bool
{
  return (   game_->type() == GameType::marriage
          && selector() != MarriageSelector::team_set);
}
