/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "players.h"
#include "game.h"

#include "../party/party.h"
#include "../party/rule.h"
#include "../player/player.h"
#include "../player/human/human.h"
#include "../ui/ui.h"
#include "../misc/preferences.h"
#include "../utils/string.h"

GamePlayers::GamePlayers(Game& game) :
  game_(&game)
{
  (void)game_;
  for (auto& p : game.party_->players()) {
    players_.push_back(&p);
  }
  for (auto& player : *this) {
    player.new_game(game);
  }
}


GamePlayers::GamePlayers(Game& game,
                         GamePlayers const& old_players,
                         vector<Player*>& new_players) :
  game_(&game)
{
  DEBUG_ASSERTION(new_players.size() == game.rule(Rule::Type::number_of_players_in_game),
                  "GamePlayers::GamePlayers(game, players)\n"
                  "  Number of players does not match: " << new_players.size() << " != " << game.rule(Rule::Type::number_of_players_in_game) << '\n');
  players_ = new_players;
  for (auto& player : *this) {
    player.set_game(game);
  }
  current_player_ = &player(old_players.current_player().no());
  if (old_players.exists_soloplayer()) {
    soloplayer_ = &player(old_players.soloplayer().no());
  }
}


GamePlayers::GamePlayers(Game& game,
                         GamePlayers const& old_players,
                         vector<unique_ptr<Player>>& new_players) :
  game_(&game)
{
  DEBUG_ASSERTION(new_players.size() == game.rule(Rule::Type::number_of_players_in_game),
                  "GamePlayers::GamePlayers(game, players)\n"
                  "  Number of players does not match: " << new_players.size() << " != " << game.rule(Rule::Type::number_of_players_in_game) << '\n');
  for (auto& p : new_players)
    players_.push_back(p.get());
  for (auto& player : *this) {
    player.set_game(game);
  }
  current_player_ = &player(old_players.current_player().no());
  if (old_players.exists_soloplayer()) {
    soloplayer_ = &player(old_players.soloplayer().no());
  }
}


void GamePlayers::self_check() const
{
  for (auto const& player : *this) {
    DEBUG_ASSERTION(&player.game() == game_,
                    "GamePlayers::self_check()\n"
                    "  a player is not of this game");
    DEBUG_ASSERTION(player.self_check(),
                    "GamePlayers::self_check()\n"
                    "  self check of a player returned 'false'");
  }

  if (current_player_) {
    DEBUG_ASSERTION((game_ == &current_player_->game()),
                    "GamePlayers::self_check()\n"
                    "  the current player is not of the same game");
  }

  if (soloplayer_) {
    DEBUG_ASSERTION((game_ == &soloplayer_->game()),
                    "GamePlayers::self_check()\n"
                    "  the soloplayer is not of the same game");
  }
}


void GamePlayers::reset() noexcept
{
  soloplayer_ = {};
}


auto GamePlayers::empty() const noexcept -> bool
{
  return players_.empty();
}


auto GamePlayers::size() const noexcept -> unsigned
{
  return players_.size();
}


auto GamePlayers::begin() noexcept -> iterator
{
  return GamePlayers::iterator(*this, 0);
}


auto GamePlayers::end() noexcept -> iterator
{
  return GamePlayers::iterator(*this, size());
}


auto GamePlayers::begin() const noexcept -> const_iterator
{
  return GamePlayers::const_iterator(*this, 0);
}


auto GamePlayers::end() const noexcept -> const_iterator
{
  return GamePlayers::const_iterator(*this, size());
}


auto GamePlayers::player(unsigned const p) -> Player&
{
  DEBUG_ASSERTION(p < players_.size(),
                  "GamePlayers::player(p):\n"
                  "'p' is not valid (" << p << ")");

  return *players_[p];
}


auto GamePlayers::player(unsigned const p) const -> Player const&
{
  DEBUG_ASSERTION(p < players_.size(),
                  "GamePlayers::player(p):\n"
                  "'p' is not valid (" << p << ")");

  return *players_[p];
}


auto GamePlayers::operator[](unsigned const p) const -> Player const&
{
  DEBUG_ASSERTION(p < players_.size(),
                  "GamePlayers::player(p):\n"
                  "'p' is not valid (" << p << ")");

  return *players_[p];
}


auto GamePlayers::player(Person const& person) const noexcept -> Player const&
{
  for (auto const& player : players_)
    if (&player->person() == &person)
      return *player;

  DEBUG_ASSERTION(false,
                  "GamePlayers::player(person = " << person.name() << ")\n"
                  "   player not found");
}


auto GamePlayers::no(Person const& person) const noexcept -> unsigned
{
  for (size_t p = 0; p < players_.size(); ++p)
    if (&players_[p]->person() == &person)
      return p;

  return UINT_MAX;
}


auto GamePlayers::no(Player const& player) const noexcept -> unsigned
{
  for (size_t p = 0; p < size(); ++p)
    if (&this->player(p) == &player)
      return p;
  return UINT_MAX;
}


auto GamePlayers::contains(Player const& player) const noexcept -> bool
{
  return container_algorithm::contains(players_, &player);
}


auto GamePlayers::current_player() const -> Player const&
{
  DEBUG_ASSERTION(current_player_,
                  "GamePlayers::current_player:\n"
                  "  current_player_ == NULL");

  return *current_player_;
}


auto GamePlayers::current_player() -> Player&
{
  DEBUG_ASSERTION(current_player_,
                  "GamePlayers::current_player:\n"
                  "  current_player_ == NULL");

  return *current_player_;
}


void GamePlayers::set_current_player(Player const& player)
{
  DEBUG_ASSERTION(&player.game() == game_,
                  "GamePlayers::set_current_player(player)\n"
                 "  Game does not match");
  current_player_ = players_[player.no()];
}


void GamePlayers::next_current_player() noexcept
{
  current_player_ = players_[(current_player_->no() + 1) % size()];
}


auto GamePlayers::exists_soloplayer() const noexcept -> bool
{
  return soloplayer_;
}


auto GamePlayers::is_soloplayer(Player const& player) const noexcept -> bool
{
  return (   exists_soloplayer()
          && (soloplayer() == player));
}


auto GamePlayers::soloplayer() const -> Player const&
{
  DEBUG_ASSERTION(soloplayer_,
                  "GamePlayers::soloplayer:\n"
                  "  soloplayer_ == NULL");

  return *soloplayer_;
}


auto GamePlayers::soloplayer() -> Player&
{
  DEBUG_ASSERTION(soloplayer_,
                  "GamePlayers::soloplayer:\n"
                  "  soloplayer_ == NULL");

  return *soloplayer_;
}


void GamePlayers::set_soloplayer(Player& player)
{
  DEBUG_ASSERTION(&player.game() == game_,
                  "GamePlayers::set_soloplayer(player)\n"
                  "  Game does not match");
  soloplayer_ = &player;
}


void GamePlayers::unset_soloplayer() noexcept
{
  soloplayer_ = {};
}


auto GamePlayers::poverty_partner() const -> Player const&
{
  DEBUG_ASSERTION((game_->type() == GameType::poverty),
                  "GamePlayers::poverty_partner()\n"
                  "  gametype is not poverty: " << game_->type());
  for (auto const& p : *this) {
    if (   (p.team() == Team::re)
        && (&p != soloplayer_ )) {
      return p;
    }
  }

  return player(0);
}


auto GamePlayers::count_humans() const noexcept -> unsigned
{
  return count_if(players_,
                  [](auto const p) {
                  return p->type() == Player::Type::human;
                  });
}


auto GamePlayers::human() -> Human&
{
  DEBUG_ASSERTION(count_humans() == 1,
                  "GamePlayers::humans()\n"
                  "  Number of humans is not 1");

  auto const p = find_if(players_,
                         [](auto const p) {
                         return (p->type() == Player::Type::human);
                         });
  return dynamic_cast<Human&>(**p);
}


auto GamePlayers::human() const -> Human const&
{
  DEBUG_ASSERTION(count_humans() == 1,
                  "GamePlayers::humans()\n"
                  "  Number of humans is not 1");

  auto const p = find_if(players_,
                         [](auto const p) {
                         return (p->type() == Player::Type::human);
                         });
  return dynamic_cast<Human const&>(**p);
}


auto GamePlayers::following(Player const& player) const -> Player const&
{
  DEBUG_ASSERTION(player == this->player(player.no()),
                  "GamePlayers::following(player):\n"
                  "  player not found in the game-players");

  return this->player((player.no() + 1)
                      % size());
}


auto GamePlayers::following(Player& player) -> Player&
{
  DEBUG_ASSERTION(player == this->player(player.no()),
                  "GamePlayers::following(player):\n"
                  "  player not found in the game-players");

  return this->player((player.no() + 1)
                      % size());
}


auto GamePlayers::previous(Player const& player) const -> Player const&
{
  DEBUG_ASSERTION(player == this->player(player.no()),
                  "GamePlayers::previous(player):\n"
                  "  player not found in the game-players");

  return this->player((player.no() + size() - 1)
                      % size());
}


void GamePlayers::write(ostream& ostr) const
{
  ostr << "players\n"
    << "{\n";
  for (auto const& player : players_)
    ostr << "{\n" << *player << "}\n\n";
  ostr << "}\n";
}


void GamePlayers::write_short(ostream& ostr) const
{
  ostr << "game players:\n";
  for (unsigned p = 0; p < players_.size(); ++p) {
    ostr << p << ": ";
    player(p).write_short(ostr);
    ostr << '\n';
  }
}
