/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../player/player.h"
class Game;
using GamePlayer = Player;
class Human;
#include "players_iterator.h"

#include "../party/players.h"

/**
 ** The players of a game
 **/
class GamePlayers {
public:
  using Player = GamePlayer;
  using iterator = GamePlayersIterator;
  using const_iterator = GamePlayersConstIterator;

  explicit GamePlayers(Game& game);
  GamePlayers(Game& game, GamePlayers const& old_players, vector<unique_ptr<PartyPlayer>>& new_players);
  GamePlayers(Game& game, GamePlayers const& old_players, vector<Player*>& new_players);

  GamePlayers(GamePlayers const&) = delete;

  void self_check() const;
  void reset() noexcept;

  auto empty() const noexcept -> bool;
  auto size()  const noexcept -> unsigned;

  auto begin()       noexcept -> iterator;
  auto end()         noexcept -> iterator;
  auto begin() const noexcept -> const_iterator;
  auto end()   const noexcept -> const_iterator;

  auto player(unsigned p)           -> Player&;
  auto player(unsigned p)     const -> Player const&;
  auto operator[](unsigned p) const -> Player const&;

  auto player(Person const& person) const noexcept -> Player const&;
  auto no(Person const& person)     const noexcept -> unsigned;

  auto no(Player const& player)       const noexcept -> unsigned;
  auto contains(Player const& player) const noexcept -> bool;

  auto current_player() const -> Player const&;
  auto current_player()       -> Player&;
  void set_current_player(Player const& player);
  void next_current_player() noexcept;

  auto exists_soloplayer()                 const noexcept -> bool;
  auto is_soloplayer(Player const& player) const noexcept -> bool;
  auto soloplayer()                        const          -> Player const&;
  auto soloplayer()                                       -> Player&;
  void set_soloplayer(Player& player);
  void unset_soloplayer()                        noexcept;

  auto poverty_partner() const -> Player const&;

  auto count_humans() const noexcept -> unsigned;
  auto human()                       -> Human&;
  auto human()        const          -> Human const&;

  auto following(Player const& player) const -> Player const&;
  auto following(Player& player)             -> Player&;
  auto previous(Player const& player)  const -> Player const&;

  void write(ostream& ostr)       const;
  void write_short(ostream& ostr) const;

private:
  Game* const game_;
  vector<GamePlayer*> players_;
  Player* current_player_ = nullptr;
  Player* soloplayer_ = nullptr;
};
