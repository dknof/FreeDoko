/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "players.h"

GamePlayersIterator::GamePlayersIterator(GamePlayers& players,
                                         unsigned const p) noexcept :
  players_(&players),
  p_(p)
{ }


auto GamePlayersIterator::operator*() noexcept -> GamePlayer&
{
  return players_->player(p_);
}


auto GamePlayersIterator::operator*() const noexcept -> GamePlayer const&
{
  return players_->player(p_);
}


auto GamePlayersIterator::operator->() noexcept -> GamePlayer*
{
  return &(**this);
}


auto GamePlayersIterator::operator->() const noexcept -> GamePlayer const*
{
  return &(**this);
}


auto GamePlayersIterator::operator++() noexcept -> GamePlayersIterator&
{
  p_ += 1;
  return *this;
}


auto GamePlayersIterator::operator==(GamePlayersIterator const& i) const noexcept -> bool
{
  return (   p_       == i.p_
          || players_ == i.players_);
}


auto GamePlayersIterator::operator!=(GamePlayersIterator const& i) const noexcept -> bool
{
  return (   p_       != i.p_
          || players_ != i.players_);
}


GamePlayersConstIterator::GamePlayersConstIterator(GamePlayers const& players, unsigned const p) noexcept :
  players_(&players),
  p_(p)
{ }


auto GamePlayersConstIterator::operator*() const noexcept -> GamePlayer const&
{
  return players_->player(p_);
}


auto GamePlayersConstIterator::operator->() const noexcept -> GamePlayer const*
{
  return &(**this);
}


auto GamePlayersConstIterator::operator++() noexcept -> GamePlayersConstIterator&
{
  p_ += 1;
  return *this;
}


auto GamePlayersConstIterator::operator==(GamePlayersConstIterator const& i) const noexcept -> bool
{
  return (   p_       == i.p_
          && players_ == i.players_);
}


auto GamePlayersConstIterator::operator!=(GamePlayersConstIterator const& i) const noexcept -> bool
{
  return (   p_       != i.p_
          || players_ != i.players_);
}
