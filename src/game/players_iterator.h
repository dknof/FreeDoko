/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

// included by players.h

#pragma once

#include "constants.h"

class GamePlayers;

class GamePlayersIterator {
  friend class GamePlayers;
public:
  using iterator_category = std::forward_iterator_tag;
  using value_type      = GamePlayer;
  using difference_type = std::ptrdiff_t;
  using pointer         = GamePlayer*;
  using reference       = GamePlayer&;

private:
  GamePlayersIterator(GamePlayers& players, unsigned p) noexcept;

public:
  auto operator*()        noexcept -> GamePlayer&;
  auto operator*()  const noexcept -> GamePlayer const&;
  auto operator->()       noexcept -> GamePlayer*;
  auto operator->() const noexcept -> GamePlayer const*;
  auto operator++()       noexcept -> GamePlayersIterator&;
  auto operator==(GamePlayersIterator const& i) const noexcept -> bool;
  auto operator!=(GamePlayersIterator const& i) const noexcept -> bool;

private:
  GamePlayers* const players_;
  unsigned p_ = 0;
};

class GamePlayersConstIterator {
  friend class GamePlayers;
public:
  using iterator_category = std::forward_iterator_tag;
  using value_type      = GamePlayer;
  using difference_type = std::ptrdiff_t;
  using pointer         = GamePlayer*;
  using reference       = GamePlayer&;

private:
  GamePlayersConstIterator(GamePlayers const& players, unsigned p) noexcept;

public:
  GamePlayersConstIterator(GamePlayersIterator i) noexcept;
  auto operator*()  const noexcept -> GamePlayer const&;
  auto operator->() const noexcept -> GamePlayer const*;
  auto operator++()       noexcept -> GamePlayersConstIterator&;
  auto operator==(GamePlayersConstIterator const& i) const noexcept -> bool;
  auto operator!=(GamePlayersConstIterator const& i) const noexcept -> bool;

private:
  GamePlayers const* const players_;
  unsigned p_ = 0;
};
