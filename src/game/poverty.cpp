/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "game.h"
#include "gameplay.h"
#include "gameplay_actions/poverty.h"

#include "../basetypes/fast_play.h"

#include "../os/bug_report_replay.h"
#include "../misc/preferences.h"
#include "../party/rule.h"
#include "../ui/ui.h"

GamePoverty::GamePoverty(Game& game) noexcept :
  game_(&game)
{ }


GamePoverty::GamePoverty(GamePoverty const& poverty, Game& game) :
  game_(&game),
  //shifted_cards_(poverty.shifted_cards_), // ToDo
  //returned_cards_(poverty.returned_cards_), // ToDo
  shifted_(poverty.shifted_)
{ }


auto GamePoverty::shifted() const noexcept -> bool
{
  return shifted_;
}


auto GamePoverty::shifted_cardno() const noexcept -> unsigned
{
  return shifted_cards_.size();
}


auto GamePoverty::returned_cardno() const noexcept -> unsigned
{
  return returned_cards_.size();
}


auto GamePoverty::returned_trumpno() const noexcept -> unsigned
{
  auto const is_trump = [this](Card const card) {
    return card.istrump(*game_);
  };
  return count_if(returned_cards_, is_trump);
}


auto GamePoverty::shifted_cards() const -> vector<Card> const&
{
  DEBUG_ASSERTION(game_->type() == GameType::poverty,
                  "GamePoverty::shifted_cards():\n"
                  "  invalid gametype");

  return shifted_cards_;
}


auto GamePoverty::returned_cards() const -> Cards const&
{
  DEBUG_ASSERTION(game_->type() == GameType::poverty,
                  "GamePoverty::returned_cards():\n"
                  "  invalid gametype '" << game_->type() << "'");

  return returned_cards_;
}


void GamePoverty::reset() noexcept
{
  shifted_cards_.clear();
  returned_cards_.clear();
  shifted_ = false;
}


void GamePoverty::shift()
{
  auto& game = *game_;
  auto& players = game.players();
  auto const& rule = game.rule();
  auto& soloplayer = players.soloplayer();
  auto& gameplay = *game.gameplay_;

  if (   (game.type() != GameType::poverty)
      || !rule(Rule::Type::poverty_shift)
      || (   (soloplayer.hand().count(Card::trump) <= 1)
          && rule(Rule::Type::throw_with_one_trump)
         )
     ) {
    players.set_current_player(soloplayer);
    game_->status_ = Game::Status::redistribute;
    return ;
  } // if (not poverty shift)

  game_->status_ = Game::Status::poverty_shift;

  game.teaminfo().set(soloplayer, Team::re);
  soloplayer.set_team(Team::re);

  game.swines().test_swines_from_reservations();

  if (!game.isvirtual()) {
    ::ui->poverty_shifting(soloplayer);
    signal_shifting(soloplayer);
  }

  shifted_cards_ = soloplayer.poverty_shift();

  DEBUG_ASSERTION((soloplayer.hand().count(Card::trump)
                   == 0),
                  "Game::poverty_shift():\n"
                  "  Poverty: the soloplayer still has trump on the hand.\n"
                  "remaining trumps: "
                  << soloplayer.hand().count(Card::trump) << '\n'
                  << "Hand:\n"
                  << soloplayer.hand()
                  << "shifted cards:\n"
                  << shifted_cards());

  DEBUG_ASSERTION( (shifted_cardno()
                    + soloplayer.hand().cardsnumber()
                    == rule(Rule::Type::number_of_tricks_in_game)),
                  "Game::poverty_shift():\n"
                  "  wrong number of cards shifted: "
                  << shifted_cardno());

  if (rule(Rule::Type::poverty_shift_only_trump))
    DEBUG_ASSERTION(HandCards(soloplayer.hand(), shifted_cards()).count(Card::trump)
                      == shifted_cardno(),
                    "Game::poverty_shift():\n"
                    "  wrong number of cards shifted: "
                    << "trumps = " << HandCards(soloplayer.hand(), shifted_cards()).count(Card::trump)
                    << " != " << shifted_cardno() << " = cards shifted");
  else if (!rule(Rule::Type::poverty_fox_do_not_count)
           || game.swines().swines_announced() )
    // normal case: shift at max 3 cards
    DEBUG_ASSERTION(shifted_cardno()
                      == rule(Rule::Type::max_number_of_poverty_trumps),
                    "Game::poverty_shift():\n"
                    "  wrong number of cards shifted: "
                    << "poverty cards = " << rule(Rule::Type::max_number_of_poverty_trumps)
                    << " != " << shifted_cardno() << " = cards shifted");
  else if (!rule(Rule::Type::poverty_fox_shift_extra))
    // at max 3 cards or number of trumps
    DEBUG_ASSERTION( (   HandCards(soloplayer.hand(), shifted_cards()).count(Card::trump) == shifted_cardno()
                      || shifted_cardno() == rule(Rule::Type::max_number_of_poverty_trumps)),
                    "Game::poverty_shift():\n"
                    "  wrong number of cards shifted: "
                    << "trumps or "
                    << "poverty cards = " << rule(Rule::Type::max_number_of_poverty_trumps)
                    << " != " << shifted_cardno() << " = cards shifted");
  else
    // fox are shifted extra
    DEBUG_ASSERTION( (shifted_cardno()
                      - HandCards(soloplayer.hand(), shifted_cards()).count(Card::trump, Card::ace)
                      == rule(Rule::Type::max_number_of_poverty_trumps)),
                    "Game::poverty_shift():\n"
                    "  wrong number of cards shifted: "
                    << "poverty cards = " << rule(Rule::Type::max_number_of_poverty_trumps)
                    << " != " << shifted_cardno() << " - "
                    << HandCards(soloplayer.hand(), shifted_cards()).count(Card::trump, Card::ace)
                    << " = cards shifted - fox");

  for (auto& p : players)
    p.poverty_shift(soloplayer, shifted_cardno());

  gameplay.add(GameplayActions::PovertyShift(soloplayer.no(),
                                                         shifted_cards()));
  if (!game.isvirtual()) {
    ::ui->poverty_shift(soloplayer, shifted_cardno());
    signal_shift(soloplayer, shifted_cardno());
    ::ui->gameplay_action(GameplayActions::PovertyShift(soloplayer.no(),
                                                        shifted_cards()));
    game.signal_gameplay_action(GameplayActions::PovertyShift(soloplayer.no(),
                                                              shifted_cards()));
  }

  // ask all players, whether they accept the poverty or not
  bool accept = false;
  players.set_current_player(soloplayer);
  for (players.next_current_player();
       players.current_player() != soloplayer;
       players.next_current_player()) {
    auto& current_player = players.current_player();
    if (!game.isvirtual()) {
      ::ui->poverty_ask(current_player, shifted_cardno());
      signal_ask(current_player, shifted_cardno());
      if (   !(::fast_play & FastPlay::pause)
#ifdef BUG_REPORT_REPLAY
          && !(   ::bug_report_replay
               && ::bug_report_replay->auto_action())
#endif
         ) {
        if (current_player.type() != Player::Type::human)
          ::ui->sleep(::preferences(Preferences::Type::card_play_delay));
      }
      if (game_->status() != Game::Status::poverty_shift)
        return ;
    } // if (!isvirtual())


    accept = current_player.poverty_take_accept(shifted_cardno());
    if (game_->status() != Game::Status::poverty_shift)
      return ;

    if (accept) {
      { // test for swines and hyperswines before taking the cards
        if (rule(Rule::Type::swines)
            && rule(Rule::Type::swines_announcement_begin)
            && game.reservation(current_player).swines)
          game.swines().swines_announce(current_player);
        if (rule(Rule::Type::hyperswines)
            && rule(Rule::Type::hyperswines_announcement_begin)
            && game.reservation(current_player).hyperswines)
          game.swines().hyperswines_announce(current_player);
      } // test for swines and hyperswines before taking the cards
      gameplay.add(GameplayActions::PovertyAccepted(current_player.no()));
      ::ui->gameplay_action(GameplayActions::PovertyAccepted(current_player.no()));
      game.signal_gameplay_action(GameplayActions::PovertyAccepted(current_player.no()));
      break;
    } else { // if !(accept)
      for (auto& p : players) {
        p.poverty_take_denied(current_player);
      }
      gameplay.add(GameplayActions::PovertyDenied(current_player.no()));
      if (!game.isvirtual()) {
        ::ui->poverty_take_denied(current_player);
        signal_take_denied(current_player);
        ::ui->gameplay_action(GameplayActions::PovertyDenied(current_player.no()));
        game.signal_gameplay_action(GameplayActions::PovertyDenied(current_player.no()));
      }
    } // if !(accept)
  } // for (current_player)

  if (!accept) {
    players.set_current_player(soloplayer);

    for (auto& p : players)
      p.poverty_take_denied_by_all();
    gameplay.add(GameplayActions::PovertyDeniedByAll());
    if (!game.isvirtual()) {
      ::ui->poverty_take_denied_by_all();
      signal_take_denied_by_all();
      ::ui->gameplay_action(GameplayActions::PovertyDeniedByAll());
      game.signal_gameplay_action(GameplayActions::PovertyDeniedByAll());
    }

    soloplayer.hand().add(shifted_cards());

    game_->status_ = Game::Status::redistribute;
  } else { // if !(!accept)
    auto& current_player = players.current_player();
    // set the teaminfo
    auto& teaminfo = game.teaminfo();
    teaminfo.set(current_player, Team::re);
    current_player.set_team(Team::re);

    for (auto& p : players) {
      if (teaminfo.get(p) == Team::unknown) {
        teaminfo.set(p, Team::contra);
        p.set_team(Team::contra);
      }
    } // for (p)
    teaminfo.set_at_gamestart();

    returned_cards_ = current_player.poverty_cards_change(shifted_cards_);
    if (game_->status() != Game::Status::poverty_shift)
      return ;

    // check for shifted swines
    if (rule(Rule::Type::swines)
        && game.swines().swines_announced()
        && (game.swines().swines_owner() == soloplayer)) {
      game.swines().swines_announce(current_player);
    }
    if (rule(Rule::Type::hyperswines)
        && game.swines().hyperswines_announced()
        && (game.swines().hyperswines_owner() == soloplayer)) {
      if (!rule(Rule::Type::swines_and_hyperswines_joint)) {
        game.swines().hyperswines_owner_ = nullptr;
        game.swines().hyperswines_announced_ = false;
      }
      game.swines().hyperswines_announce(current_player);
    }

    DEBUG_ASSERTION(returned_cards_.size() == shifted_cardno(),
                    "Game::poverty_shift():\n"
                    "  number of cards returned ("
                    << returned_cards().size()
                    << ") is not the same as are shifted ("
                    << shifted_cardno() << ").");

    for (auto& p : players) {
      p.poverty_take_accepted(current_player,
                              returned_cardno(),
                              returned_trumpno());
    }
    gameplay.add(GameplayActions::PovertyReturned(current_player.no(),
                                                              returned_cards_));
    if (!game.isvirtual()) {
      ::ui->poverty_take_accepted(current_player,
                                  returned_cardno(),
                                  returned_trumpno());
      signal_take_accepted(current_player,
                           returned_cardno(),
                           returned_trumpno());
      ::ui->gameplay_action(GameplayActions::PovertyReturned(current_player.no(),
                                                             returned_cards()));
      game.signal_gameplay_action(GameplayActions::PovertyReturned(current_player.no(),
                                                                   returned_cards()));
    }


    // add the cards to the player
    players.set_current_player(soloplayer);
    soloplayer.poverty_cards_get_back(returned_cards_);
    if (!game.isvirtual()) {
      ::ui->gametype_changed();
      game.signal_gametype_changed();
    }

    if (game_->status() != Game::Status::poverty_shift)
      return ;

  } // if !(!accept)

  shifted_ = true;

  if (accept) {
    if (game.swines().swines_announced()) {
      // check, that the swines announcement is correct
      auto const& swines_owner = game.swines().swines_owner();
      if (swines_owner.team() == Team::re) {
        auto p = container_algorithm::find_if(players,
                                              [](auto const& player) {
                                              return player.hand().count(Card::trump, Card::ace) == 2;
                                              });

        if (p != players.end()) {
          game.swines().swines_announce(*p);
        } else {
          game.swines().swines_owner_ = nullptr;
          game.swines().swines_announced_ = false;
        }
      } // if (swines_owner().team() == Team::re)
    } // if (swines_owner())
  } // if (accept)

  if (game_->status() == Game::Status::poverty_shift)
    game_->status_ = Game::Status::reservation;
}
