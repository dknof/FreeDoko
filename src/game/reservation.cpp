/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "reservation.h"

#include "game.h"
#include "../party/rule.h"

#include "../utils/string.h"

Reservation::Reservation(string const& line)
{
  // syntax:
  //   'game type', 'marriage selector', swines, hyperswines

  // first split the line in the single parts
  vector<string> const parts(String::split(line, ','));
  auto p = parts.begin();

  game_type = game_type_from_string(*p);
  ++p;
  if (p == parts.end()) {
    if (game_type == GameType::marriage) {
      marriage_selector = MarriageSelector::first_foreign;
    }
    return ;
  }

  if (game_type == GameType::marriage) {
    marriage_selector = marriage_selector_from_string(*p);
    ++p;
  } // if (game_type == GameType::marriage)

  for ( ; p != parts.end(); ++p) {
    if ((*p) == "offer duty solo") {
      offer_duty_solo = true;
    } else if ((*p) == "swines") {
      swines = true;
    } else if ((*p) == "hyperswines"){
      swines = true;
    } else {
      DEBUG_ASSERTION(false,
                      "Reservation(line)\n"
                      "  unknown part '" << *p << "'");
    }
  } // for (p \in parts)
}


auto operator==(Reservation const lhs, Reservation const rhs) noexcept -> bool
{
  return (   lhs.game_type == rhs.game_type
          && lhs.game_type == GameType::marriage ? (lhs.marriage_selector == rhs.marriage_selector) : true
          && lhs.offer_duty_solo == rhs.offer_duty_solo
          && lhs.swines == rhs.swines
          && lhs.hyperswines == rhs.hyperswines);
}


auto to_string(Reservation const& reservation) -> string
{
  string result = "game type: " + to_string(reservation.game_type) + "\n";
  if (reservation.game_type == GameType::marriage)
    result += "marriage selector: " + to_string(reservation.marriage_selector) + "\n";
  if (reservation.offer_duty_solo)
    result += "offer duty solo\n";
  if (reservation.swines)
    result += "swines\n";
  if (reservation.hyperswines)
    result += "hyperswines\n";
  return result;
}


auto to_rest_string(Reservation const& reservation) -> string
{
  string result = to_string(reservation.game_type);
  if (   reservation.game_type == GameType::marriage
      && reservation.marriage_selector != MarriageSelector::first_foreign) {
    result += ", " + to_string(reservation.marriage_selector);
  }
  if (reservation.offer_duty_solo)
    result += ", offer";
  if (reservation.swines)
    result += ";swines";
  if (reservation.hyperswines)
    result += ";hyperswines";
  return result;
}


auto operator<<(ostream& ostr, Reservation const& reservation) -> ostream&
{
  ostr << to_string(reservation);
  return ostr;
}
