/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "specialpoint.h"

#include "../party/party.h"
#include "../party/rule.h"
#include "../game/game.h"
#include "../class/readconfig/readconfig.h"

#include "../utils/string.h"


auto is_winning(Specialpoint::Type const sp) noexcept -> bool
{
  switch (sp) {
  case Specialpoint::Type::won:
  case Specialpoint::Type::no90:
  case Specialpoint::Type::no60:
  case Specialpoint::Type::no30:
  case Specialpoint::Type::no0:
  case Specialpoint::Type::contra_won:
  case Specialpoint::Type::no90_said_120_got:
  case Specialpoint::Type::no60_said_90_got:
  case Specialpoint::Type::no30_said_60_got:
  case Specialpoint::Type::no0_said_30_got:
  case Specialpoint::Type::solo:
    return true;
    break;
  default:
    return false;
    break;
  }; // switch(sp)
} // bool is_winning(Specialpoint::Type sp)


auto is_announcement(Specialpoint::Type const sp) noexcept -> bool
{
  switch (sp) {
  case Specialpoint::Type::no120_said:
  case Specialpoint::Type::no90_said:
  case Specialpoint::Type::no60_said:
  case Specialpoint::Type::no30_said:
  case Specialpoint::Type::no0_said:
  case Specialpoint::Type::announcement_reply:
    return true;
    break;
  default:
    return false;
    break;
  }; // switch(sp)
} // bool is_announcement(Specialpoint::Type sp)


auto value(Specialpoint::Type const sp) noexcept -> int
{
  switch (sp) {
  case Specialpoint::Type::nospecialpoint:
    return 0;
  case Specialpoint::Type::caught_fox:
  case Specialpoint::Type::caught_fox_last_trick:
  case Specialpoint::Type::fox_last_trick:
  case Specialpoint::Type::charlie:
  case Specialpoint::Type::caught_charlie:
  case Specialpoint::Type::dulle_caught_dulle:
  case Specialpoint::Type::heart_trick:
  case Specialpoint::Type::doppelkopf:
  case Specialpoint::Type::won:
  case Specialpoint::Type::no90:
  case Specialpoint::Type::no60:
  case Specialpoint::Type::no30:
  case Specialpoint::Type::no0:
    return 1;
  case Specialpoint::Type::no120_said:
  case Specialpoint::Type::announcement_reply:
    return 2;
  case Specialpoint::Type::no90_said:
  case Specialpoint::Type::no60_said:
  case Specialpoint::Type::no30_said:
  case Specialpoint::Type::no0_said:
  case Specialpoint::Type::no90_said_120_got:
  case Specialpoint::Type::no60_said_90_got:
  case Specialpoint::Type::no30_said_60_got:
  case Specialpoint::Type::no0_said_30_got:
  case Specialpoint::Type::contra_won:
    return 1;
  case Specialpoint::Type::solo:
    return 3;
  case Specialpoint::Type::bock:
    return 0;
  } // switch(sp)

  return 0;
} // int value(Specialpoint::Type sp)


auto to_string(Specialpoint::Type const type) noexcept -> string
{
  switch(type) {
  case Specialpoint::Type::nospecialpoint:
    (void)_("Specialpoint::no specialpoint");
    return "no specialpoint";
  case Specialpoint::Type::caught_fox:
    (void)_("Specialpoint::caught fox");
    return "caught fox";
  case Specialpoint::Type::caught_fox_last_trick:
    (void)_("Specialpoint::caught fox last trick");
    return "caught fox last trick";
  case Specialpoint::Type::fox_last_trick:
    (void)_("Specialpoint::fox last trick");
    return "fox last trick";
  case Specialpoint::Type::charlie:
    (void)_("Specialpoint::charlie");
    return "charlie";
  case Specialpoint::Type::caught_charlie:
    (void)_("Specialpoint::caught charlie");
    return "caught charlie";
  case Specialpoint::Type::dulle_caught_dulle:
    (void)_("Specialpoint::dulle caught dulle");
    return "dulle caught dulle";
  case Specialpoint::Type::heart_trick:
    (void)_("Specialpoint::heart trick");
    return "heart trick";
  case Specialpoint::Type::doppelkopf:
    (void)_("Specialpoint::Doppelkopf");
    return "Doppelkopf";
  case Specialpoint::Type::won:
    (void)_("Specialpoint::won");
    return "won";
  case Specialpoint::Type::no90:
    (void)_("Specialpoint::no 90");
    return "no 90";
  case Specialpoint::Type::no60:
    (void)_("Specialpoint::no 60");
    return "no 60";
  case Specialpoint::Type::no30:
    (void)_("Specialpoint::no 30");
    return "no 30";
  case Specialpoint::Type::no0:
    (void)_("Specialpoint::no 0");
    return "no 0";
  case Specialpoint::Type::no120_said:
    (void)_("Specialpoint::no 120 said");
    return "no 120 said";
  case Specialpoint::Type::no90_said:
    (void)_("Specialpoint::no 90 said");
    return "no 90 said";
  case Specialpoint::Type::no60_said:
    (void)_("Specialpoint::no 60 said");
    return "no 60 said";
  case Specialpoint::Type::no30_said:
    (void)_("Specialpoint::no 30 said");
    return "no 30 said";
  case Specialpoint::Type::no0_said:
    (void)_("Specialpoint::no 0 said");
    return "no 0 said";
  case Specialpoint::Type::no90_said_120_got:
    (void)_("Specialpoint::got 120 against no 90");
    return "got 120 against no 90";
  case Specialpoint::Type::no60_said_90_got:
    (void)_("Specialpoint::got 90 against no 60");
    return "got 90 against no 60";
  case Specialpoint::Type::no30_said_60_got:
    (void)_("Specialpoint::got 60 against no 30");
    return "got 60 against no 30";
  case Specialpoint::Type::no0_said_30_got:
    (void)_("Specialpoint::got 30 against no 0");
    return "got 30 against no 0";
  case Specialpoint::Type::announcement_reply:
    (void)_("Specialpoint::announcement reply said");
    return "announcement reply said";
  case Specialpoint::Type::contra_won:
    (void)_("Specialpoint::contra won");
    return "contra won";
  case Specialpoint::Type::solo:
    (void)_("Specialpoint::solo");
    return "solo";
  case Specialpoint::Type::bock:
    (void)_("Specialpoint::bock");
    return "bock";
  } // switch(specialpoints)

  return "";
} // string to_string(Specialpoint::Type specialpoint)


auto specialpoint_type_from_string(string const& name) -> Specialpoint::Type
{
#ifndef OUTDATED
  // 0.7.5: no 120 -> won
  if (name == "no 120")
    return Specialpoint::Type::won;
  if (name == "no 90 won")
    return Specialpoint::Type::no90;
  if (name == "no 60 won")
    return Specialpoint::Type::no60;
  if (name == "no 30 won")
    return Specialpoint::Type::no30;
  if (name == "no 0 won")
    return Specialpoint::Type::no0;
#endif
#ifndef OUTDATED
  if (name == "dolle caught dolle")
    return Specialpoint::Type::dulle_caught_dulle;
#endif
  for (auto const t : Specialpoint::types)
    if (name == to_string(t))
      return t;

  throw ReadException("unknown special point '" + name + "'");
  return Specialpoint::Type::nospecialpoint; // cppcheck-suppress duplicateBreak
} // Specialpoint::Type specialpoint_type_from_string(string name)


auto operator<<(ostream& ostr, Specialpoint::Type const type) -> ostream&
{
  return (ostr << to_string(type));
} // ostream operator<<(ostream& ostr, Specialpoint::Type type);


Specialpoint::Specialpoint(Specialpoint::Type const sp,
                             Team const team) noexcept :
  type(sp),
  team(team),
  counting_team(team),
  value(::value(sp))
{ }


Specialpoint::Specialpoint(Specialpoint::Type const type,
                             Team const team,
                             Team const counting_team) noexcept :
  type(type),
  team(team),
  counting_team(counting_team),
  value(::value(type))
{ }


Specialpoint::Specialpoint(Specialpoint::Type const type,
                             int const value) noexcept :
  type(type),
  value(value)
{ }


Specialpoint::Specialpoint(istream& istr)
{
  Config config;
  istr >> config;

  type = specialpoint_type_from_string(config.name);
  value = ::value(type); // NOLINT(cppcoreguidelines-prefer-member-initializer)
  string::size_type pos_begin = 0;
  while (   pos_begin < config.value.size()
         && isspace(config.value[pos_begin]))
    ++pos_begin;
  auto pos_end = config.value.find(',');
  if (pos_end == string::npos) {
    return ;
  }
  team = team_from_string(string(config.value,
                                       pos_begin, pos_end - pos_begin));
  pos_begin = pos_end + 1;
  while (   pos_begin < config.value.size()
         && isspace(config.value[pos_begin]))
    ++pos_begin;
  pos_end = config.value.find(',', pos_begin);
  if (pos_end == string::npos) {
    return ;
  }
  counting_team = team_from_string(string(config.value,
                                               pos_begin, pos_end - pos_begin));
  pos_begin = pos_end + 1;
  while (   pos_begin < config.value.size()
         && isspace(config.value[pos_begin]))
    ++pos_begin;
  pos_end = config.value.find(',', pos_begin);
  if (pos_end == string::npos) {
    return ;
  }
  player_get_no = Unsigned(string(config.value,
                                        pos_begin, pos_end - pos_begin)
                                );
  pos_begin = pos_end + 1;
  while (   pos_begin < config.value.size()
         && isspace(config.value[pos_begin]))
    ++pos_begin;
  pos_end = config.value.find(',', pos_begin);
  if (pos_end == string::npos) {
    return ;
  }
  player_of_no = Unsigned(string(config.value,
                                       pos_begin, pos_end - pos_begin)
                               );
  pos_begin = pos_end + 1;
  while (   pos_begin < config.value.size()
         && isspace(config.value[pos_begin]))
    ++pos_begin;
  pos_end = config.value.size();
  if (pos_end == string::npos) {
    return ;
  }

  String::from_string(string(config.value,
                                        pos_begin, pos_end - pos_begin),
                                 value);
  DEBUG_ASSERTION((type == Specialpoint::Type::bock)
                  || (value == ::value(type)),
                  "Specialpoints::Specialpoints(istream& istr)\n"
                  "  type = " << type << " != bock and "
                  "value = " << value << " != "
                  << ::value(type)
                  << " = value of the type");
}


auto Specialpoint::is_valid(vector<Team> const& team) const -> bool
{
  switch(type) {
  case Type::caught_fox:
  case Type::caught_fox_last_trick:
  case Type::caught_charlie:
  case Type::dulle_caught_dulle:
    return (team[player_of_no] != team[player_get_no]);
  case Type::fox_last_trick:
  case Type::charlie:
  case Type::heart_trick:
  case Type::doppelkopf:
    return true;
  case Type::nospecialpoint:
  case Type::won:
  case Type::no90:
  case Type::no60:
  case Type::no30:
  case Type::no0:
  case Type::no120_said:
  case Type::no90_said:
  case Type::no60_said:
  case Type::no30_said:
  case Type::no0_said:
  case Type::no90_said_120_got:
  case Type::no60_said_90_got:
  case Type::no30_said_60_got:
  case Type::no0_said_30_got:
  case Type::announcement_reply:
  case Type::contra_won:
  case Type::solo:
  case Type::bock:
    return true;
  } // switch(specialpoints)
  return false;
} // bool Specialpoint::is_valid(vector<Team> team) const


auto operator<<(ostream& ostr, Specialpoint const& sp) -> ostream&
{
  ostr << sp.type << " = "
    << sp.team << ", "
    << sp.counting_team << ", "
    << Unsigned(sp.player_get_no) << ", "
    << Unsigned(sp.player_of_no) << ", "
    << sp.value
    << '\n';

  return ostr;
} // ostream operator<<(ostream& ostr, Specialpoint sp);


auto operator<<(ostream& ostr, Specialpoints const& spv) -> ostream&
{
  for (auto const sp : spv)
    ostr << sp;

  return ostr;
} // ostream operator<<(ostream& ostr, Specialpoints spv);


auto sum(Specialpoints const& spv, Team const winner, Game const& game) -> int
{
  int re_played = 0;
  int re_ann = 0;
  int re_trick = 0;
  int contra_played = 0;
  int contra_trick = 0;
  int contra_ann = 0;
  int no120s = 1;
  auto const& rule = game.rule();

  for (auto const& sp : spv) {
    // splitting of points in Announcements and trick specialpoints
    // for doubling with re
    switch (sp.type) {
    case Specialpoint::Type::nospecialpoint:
      break;

    case Specialpoint::Type::caught_fox:
    case Specialpoint::Type::caught_fox_last_trick:
    case Specialpoint::Type::fox_last_trick:
    case Specialpoint::Type::charlie:
    case Specialpoint::Type::caught_charlie:
    case Specialpoint::Type::dulle_caught_dulle:
    case Specialpoint::Type::heart_trick:
    case Specialpoint::Type::doppelkopf:
      if (sp.counting_team == Team::re)
        re_trick += sp.value;
      else
        contra_trick += sp.value;
      break;

    case Specialpoint::Type::won:
      if (sp.counting_team == Team::re)
        re_played += sp.value;
      else
        contra_played += sp.value;
      break;
    case Specialpoint::Type::no90:
    case Specialpoint::Type::no60:
    case Specialpoint::Type::no30:
    case Specialpoint::Type::no0:
      if (sp.counting_team == Team::re)
        re_played += sp.value;
      else
        contra_played += sp.value;
      break;
    case Specialpoint::Type::no120_said:
      if (rule(Rule::Type::announcement_re_doubles)) {
        no120s *= sp.value;
      } else {
        if (sp.counting_team == Team::re)
          re_ann += sp.value;
        else
          contra_ann+= sp.value;
      }
      break;
    case Specialpoint::Type::no90_said:
    case Specialpoint::Type::no60_said:
    case Specialpoint::Type::no30_said:
    case Specialpoint::Type::no0_said:
      if (sp.counting_team == Team::re)
        re_ann += sp.value;
      else
        contra_ann += sp.value;
      break;
    case Specialpoint::Type::no90_said_120_got:
    case Specialpoint::Type::no60_said_90_got:
    case Specialpoint::Type::no30_said_60_got:
    case Specialpoint::Type::no0_said_30_got:
      if (sp.counting_team == Team::re)
        re_played += sp.value;
      else
        contra_played += sp.value;
      break;
    case Specialpoint::Type::announcement_reply:
      if (rule(Rule::Type::announcement_re_doubles)) {
        no120s *= sp.value;
      } else {
        if (sp.counting_team == Team::re)
          re_ann += sp.value;
        else
          contra_ann += sp.value;
      }
      break;
    case Specialpoint::Type::contra_won:
      if (rule(Rule::Type::announcement_contra_doubles_against_re))
        contra_played += sp.value;
      else
        contra_trick += sp.value;
      break;

    case Specialpoint::Type::solo:
      // multiplier
      break;
    case Specialpoint::Type::bock:
      // multiplier
      break;
    } // switch (sp.type)
  } // for (sp : spv)


  DEBUG_ASSERTION((no120s <= (rule(Rule::Type::knocking) ? 16 : 4)),
                  "Sum_of_Specialpoints():\n"
                  "  'no120s' = " << no120s << " > " << (rule(Rule::Type::knocking) ? 16 : 4) << "\n"
                  << spv);

  { // add the points
    int result = 0;
    switch (winner) {
    case Team::re:
      result += re_played + re_ann - contra_played;
      result *= no120s;
      result += re_trick - contra_trick;
      break;

    case Team::contra:
      result += contra_played + contra_ann - re_played;
      result *= no120s;
      result += contra_trick - re_trick;
      break;

    case Team::noteam:
      result += re_played - contra_played;
      result *= no120s;
      result += re_trick - contra_trick;
      break;

    default:
      break;
    } // switch (winner)

    auto const multiplier = [](auto const& specialpoint) {
      return specialpoint.value;
    };
    auto const is_bock = [](auto const& specialpoint) {
      return specialpoint.type == Specialpoint::Type::bock;
    };
    return result * prod_if(spv, multiplier, is_bock);
  } // add the points
} // int sum(Specialpoints, Team winner)


auto gettext(Specialpoint::Type const sp) -> string
{
  return gettext("Specialpoint::" + to_string(sp));
} // string gettext(Specialpoint::Type sp)


auto gettext(Specialpoint const& sp) -> string
{
  return gettext(sp.type);
} // string gettext(Specialpoints special_points)
