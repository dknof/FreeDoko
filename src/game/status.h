/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

enum class GameStatus {
  init,
  cards_distribution,
  manual_cards_distribution,
  reservation,
  redistribute,
  poverty_shift,
  start,
  play,
  full_trick,
  trick_taken,
  finished
}; // enum class GameStatus

auto to_string(GameStatus game_status) noexcept -> string;
auto gettext(GameStatus game_status)            -> string;
auto operator<<(ostream& ostr, GameStatus game_status) -> ostream&;

