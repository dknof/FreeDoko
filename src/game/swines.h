/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../card/hand_cards.h"
class Game;
class Player;
namespace DokoLounge {
class Spieltisch;
} // namespace DokoLounge

/**
 ** The swines part of a game
 **/
class GameSwines {
  friend class Game;
  friend class GamePoverty;
  friend class DokoLounge::Spieltisch;
  public:
  explicit GameSwines(Game& game) noexcept;
  GameSwines(GameSwines const& poverty, Game& game);

  GameSwines(GameSwines const&) = delete;
  auto operator=(GameSwines const&) = delete;

  void self_check() const;

  void reset() noexcept;

  auto first_fox_caught()                                   const noexcept -> bool;
  auto first_fox_catcher()                                  const noexcept -> Player const&;
  auto swines_announced()                                   const noexcept -> bool;
  auto swines_owner()                                       const noexcept -> Player const&;
  auto swines_announcement_valid(Player const& player)      const          -> bool;
  auto swines_announce(Player& player)                                     -> bool;
  auto hyperswines_announced()                              const noexcept -> bool;
  auto hyperswines_owner()                                  const noexcept -> Player const&;
  auto hyperswines_announcement_valid(Player const& player) const          -> bool;
  auto hyperswines_announce(Player& player)                                -> bool;

  void test_swines_from_reservations();
  void evaluate_trick(Trick const& trick);

  private:
  Game* const game_;

  Player* swines_owner_            = {};
  Player const* first_fox_catcher_ = {};
  bool swines_announced_ = false;

  Player* hyperswines_owner_  = {};
  bool hyperswines_announced_ = false;

  public:
  Signal<void(Player const&)> signal_swines_announced;
  Signal<void(Player const&)> signal_hyperswines_announced;
};
