/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "teaminfo.h"
#include "game.h"
#include "../party/rule.h"
#include "../ui/ui.h"
#include "../player/team_information.h"
#include "../player/human/human.h"

GameTeamInfo::GameTeamInfo(Game& game) noexcept :
  game_(&game)
{ }


GameTeamInfo::GameTeamInfo(GameTeamInfo const& teaminfo,
                           Game& game) noexcept :
game_(&game),
  teaminfo_(teaminfo.teaminfo_)
{ }

void GameTeamInfo::reset()
{
  auto& game = *game_;
  auto const& rule = game.rule();
  auto const n = rule(Rule::Type::number_of_players_in_game);
  for (auto& player : game.players()) {
    player.set_team(Team::unknown);
  }
  teaminfo_ = vector<Team>(n, Team::unknown);
}


void GameTeamInfo::set_at_gamestart()
{
  auto& game = *game_;
  auto const n = game.rule(Rule::Type::number_of_players_in_game);

  if (game.type() == GameType::normal) {
    teaminfo_ = vector<Team>(n, Team::unknown);
    for (auto& player : game.players()) {
      player.set_team(player.hand().contains(Card::club_queen)
                      ? Team::re
                      : player.hand().contains(Card::unknown)
                      ? Team::unknown
                      : Team::contra);
    }
    all_known_ = false;
  } else if (game.type() == GameType::poverty) {
    // is done in 'poverty_shift()'

    all_known_ = true;
  } else if (game.type() == GameType::marriage) {
    teaminfo_ = vector<Team>(n, Team::noteam);
    teaminfo_[game.players().soloplayer().no()] = Team::re;
  } else if (::is_solo(game.type())) {
    all_known_ = false;
    // set all teams to contra but of the soloplayer
    teaminfo_ = vector<Team>(n, Team::contra);
    teaminfo_[game.players().soloplayer().no()] = Team::re;
    all_known_ = true;
  }
}


// NOLINTNEXTLINE(misc-no-recursion)
void GameTeamInfo::update()
{
  if (all_known_)
    return ;

  if (game_->status() < Game::Status::play)
    return ;

  auto& game = *game_;
  auto const& rule = game.rule();
  auto const& tricks = game.tricks();
  auto const& current_trick = tricks.current();

  if (!current_trick.isstartcard()) {
    Player const& player
      = current_trick.player_of_card(current_trick.actcardno() - 1);
    auto const card = current_trick.card_of_player(player);
    auto const startcard = current_trick.startcard();

    if (!is_real(get(player))) {
      if (card == Card::club_queen) {
        set(player, Team::re);
      } else if (   startcard.istrump()
                 && (!card.istrump())
                 && !player.hand().contains(Card::club_queen)) {
        set(player, Team::contra);
      } else if (   (player.cards_to_play() == 0)
                 && !player.hand().contains(Card::club_queen)) {
        set(player, Team::contra);
      }

    } // if (team of 'player' unknown)
  } // if (!current_trick.isstartcard())

  { // count the played club queens for a silent marriage
    for (auto const& player : game.players()) {
      if (get(player) == Team::re) {
        // We only want to know, whether the player has played his second
        // club queen, so he must already be known as re.
        unsigned n = 0;
        for (auto const& trick : tricks) {
          if (trick.cardno_of_player(player) < trick.actcardno()) {
            if (trick.card_of_player(player) == Card::club_queen) {
              n += 1;
            }
          }
        } // for (t : tricks())

        if (n == rule(Rule::Type::number_of_same_cards)) {
          set_unknown(Team::contra);
          return ;
        } // if (both club queens played)
      } // if (teaminfo_[player] == Team::re)
    } // for (player)
  } // count the played club queens for a silent marriage

  if (   rule(Rule::Type::swine_only_second)
      && game.swines().swines_announced()) {
    // the swines owner has to say his team
    set(game.swines().swines_owner(),
              game.swines().swines_owner().team());
  }

  { // count the 're's and 'contra's
    auto const re_no = static_cast<unsigned>(count(teaminfo_, Team::re));
    auto const contra_no = static_cast<unsigned>(count(teaminfo_, Team::contra));
    auto const players_per_team = rule(Rule::Type::number_of_players_per_team);
    auto const playerno = rule(Rule::Type::number_of_players_in_game);
    if (re_no == players_per_team) {
      set_unknown(Team::contra);
      return ;
    }
    if (contra_no == playerno - 1) {
      set_unknown(Team::re);
      game.marriage().determine_silent_marriage();
      return ;
    }

    if (contra_no == playerno - 2) {
      if (!current_trick.isstartcard()) {
        Player const& player
          = current_trick.player_of_card(current_trick.actcardno() - 1);
        if (   (get(player) == Team::re)
            && current_trick.startcard().istrump()
            && !current_trick.card_of_player(player).istrump()) {

          // all players but two are contra, one is re and does not have
          // any more club queen
          // so the remaining player must be re
          set_unknown(Team::re);
        } // // if (the player is re but can play no more trump)
      } // if (!current_trick.isstartcard())
    } // if (contra_no == playerno() - 2)

    if (re_no + contra_no == playerno) {
      all_known_ = true;
    }
  } // count the 're's and 'contra's
} // void GameTeamInfo::update()


auto GameTeamInfo::get(Player const& player) const -> Team
{
  return teaminfo_[player.no()];
}

  auto GameTeamInfo::get(unsigned playerno) const -> Team
{
  return teaminfo_.at(playerno);
}

auto GameTeamInfo::get_human_teaminfo(Player const& player) const -> Team
{
  if (game_->players().count_humans() == 1) {
    auto const team = game_->players().human().team_information().team(player);
    if (is_real(team))
      return to_real(team);
  }
  return get(player);
}


auto GameTeamInfo::get_all() const noexcept -> vector<Team> const&
{
  return teaminfo_;
}


// NOLINTNEXTLINE(misc-no-recursion)
void GameTeamInfo::set(Player const& player, Team const team)
{
  DEBUG_ASSERTION(is_real(team),
                  "Game::set_teaminfo(player, team):\n"
                  "  'team' not valid: " << team);
  DEBUG_ASSERTION(game_->status() >= Game::Status::play
                  && !game_->isvirtual()
                  && is_real(team)
                  && is_real(player.team())
                  ? player.team() == team
                  : true,
                  "Game::set_teaminfo(" << player.name() << ", " << team << "):\n"
                  "  'team' is false: " << team << " != " << player.team() << " = team of player");

  auto const n = player.no();
  if (teaminfo_[n] == team)
    return ;
  teaminfo_[n] = team;

  Game& game = *game_;
  if (player.team() == Team::unknown)
    game.players().player(player.no()).set_team(team);

  signal_changed(player);
  if (!game.isvirtual()) {
    ::ui->teaminfo_changed(player);
  }

  update();
  for (auto& player : game.players()) // cppcheck-suppress shadowArgument
    player.teaminfo_update();
}


void GameTeamInfo::set_unknown(Team const team) noexcept
{
  for (auto& t : teaminfo_) {
    if (!is_real(t)) {
      t = team;
    }
  }
  all_known_ = true;
}


void GameTeamInfo::write(ostream& ostr) const
{
  for (auto const& player : game_->players())
    ostr << player.no() << ": " << get(player) << '\n';
}

auto operator<<(ostream& ostr, GameTeamInfo const& teaminfo) -> ostream&
{
  teaminfo.write(ostr);
  return ostr;
}
