/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "tricks.h"
#include "game.h"
#include "../party/rule.h"

GameTricks::GameTricks(Game& game) noexcept :
game_(&game)
{ }


GameTricks::GameTricks(GameTricks const& tricks, Game& game) :
  game_(&game),
  tricks_(tricks.tricks_)
{
  for (auto& t : tricks_)
    t.set_game(game);
}


void GameTricks::self_check() const
{
  for (auto const& trick : *this) {
    DEBUG_ASSERTION(&trick.game() == game_,
                    "GameTricks::self_check()\n"
                    "  a trick is not of this game");

    DEBUG_ASSERTION(trick.self_check(),
                    "GameTricks::self_check()\n"
                    "  self check of a trick returned 'false'");
  }
}


auto GameTricks::empty() const noexcept -> bool
{
  return tricks_.empty();
}


auto GameTricks::size() const noexcept -> unsigned
{
  return tricks_.size();
}


auto GameTricks::max_size() const -> unsigned
{
  return game_->rule()(Rule::Type::number_of_tricks_in_game);
}


auto GameTricks::begin() noexcept -> iterator
{
  return GameTricks::iterator(*this, 0);
}


auto GameTricks::end() noexcept -> iterator
{
  return GameTricks::iterator(*this, size());
}


auto GameTricks::begin() const noexcept -> const_iterator
{
  return GameTricks::const_iterator(*this, 0);
}


auto GameTricks::end() const noexcept -> const_iterator
{
  return GameTricks::const_iterator(*this, size());
}


auto GameTricks::trick(unsigned const t) -> Trick&
{
  DEBUG_ASSERTION(t < tricks_.size(),
                  "GameTricks::trick(t):\n"
                  "'t' is not valid (" << t << ")");

  return tricks_[t];
}


auto GameTricks::trick(unsigned const t) const -> Trick const&
{
  DEBUG_ASSERTION(t < tricks_.size(),
                  "GameTricks::trick(t):\n"
                  "'t' is not valid (" << t << ")");

  return tricks_[t];
}


void GameTricks::add(Trick trick)
{
  tricks_.push_back(trick);
  tricks_.back().set_game(*game_);
}


auto GameTricks::current() const -> Trick const&
{ return tricks_.back(); }


auto GameTricks::current() -> Trick&
{ return tricks_.back(); }


auto GameTricks::tricks_in_trickpiles() const -> unsigned
{
  if (tricks_.empty()) {
    if (game_->status() < Game::Status::play)
      return UINT_MAX;
    else
      return 0;
  }

  if (   game_->status() == Game::Status::finished
      || game_->status() == Game::Status::trick_taken)
    return tricks_.size();

  return (tricks_.size() - 1);
}


auto GameTricks::current_no() const -> unsigned
{
  if (tricks_.empty()) {
    if (game_->status() < Game::Status::play)
      return UINT_MAX;
    else
      return 0;
  }

  if (   game_->status() == Game::Status::finished
      || current().isfull())
    return tricks_.size();

  return (tricks_.size() - 1);
}


auto GameTricks::real_current_no() const noexcept -> unsigned
{
  if (tricks_.empty()) {
    if (game_->status() < Game::Status::play)
      return UINT_MAX;
    else
      return 0;
  }

  if (game_->status() == Game::Status::finished)
    return tricks_.size();

  return (tricks_.size() - 1);
}


auto GameTricks::remaining_no() const -> unsigned
{
  if (   game_->status() == Game::Status::reservation
      || game_->status() == Game::Status::poverty_shift)
    return game_->rule()(Rule::Type::number_of_tricks_in_game);

  DEBUG_ASSERTION(current_no() != UINT_MAX,
                  "Game::tricks().remaining_no():\n"
                  "  tricks().current_no() == -1");

  return (game_->rule()(Rule::Type::number_of_tricks_in_game)
          - current_no());
}


auto GameTricks::real_remaining_no() const -> unsigned
{
  DEBUG_ASSERTION((real_current_no() != UINT_MAX),
                  "Game::tricks().real_remaining_no():\n"
                  "  real_tricks().current_no() == -1");

  return (game_->rule()(Rule::Type::number_of_tricks_in_game)
          - real_current_no());
}


auto GameTricks::is_last_trick() const -> bool
{
  if (   game_->status() == Game::Status::reservation
      || game_->status() == Game::Status::poverty_shift)
    return false;
  return (remaining_no() == 1);
}


auto GameTricks::trump_tricks_no() const -> unsigned
{
  auto const is_trump_trick = [](Trick const& trick) {
    return (   !trick.isempty()
            && trick.startcard().istrump());
  };
  return count_if(tricks_, is_trump_trick);
}


auto GameTricks::has_trick(Team const team) const -> bool
{
  auto const team_has_won_trick = [team](Trick const& trick) {
    return (   trick.winnerteam() == team
            && trick.intrickpile());
  };
  return any_of(tricks_, team_has_won_trick);
}


auto GameTricks::has_trick(Player const& player) const -> bool
{
  auto const player_has_won_trick = [&player](Trick const& trick) {
    return (   trick.winnerplayer() == player
            && trick.intrickpile());
  };
  return any_of(tricks_, player_has_won_trick);
}

auto GameTricks::number_of_tricks(Player const& player) const -> unsigned
{
  auto const player_has_won = [&player](auto const& trick) {
    return (   trick.winnerplayer() == player
            && trick.intrickpile());
  };
  return count_if(tricks_, player_has_won);
}

auto GameTricks::number_of_tricks_till_trick(Player const& player,
                                        unsigned trickno) const -> unsigned
{
  unsigned number = 0;

  if (trickno != UINT_MAX)
    trickno += 1;
  for (auto trick = tricks_.begin();
       (   (trickno > 0)
        && (trick != tricks_.end()));
       ++trick, --trickno) {
    if (   (trick->winnerplayer() == player)
        && trick->intrickpile()) {
      number += 1;
    }
  }

  return number;
}


auto GameTricks::number_of_tricks(Team const team) const -> unsigned
{
  return count_if(tricks_,
                  [team](auto const& trick) {
                  return (   (trick.winnerteam() == team)
                          && trick.intrickpile());
                  });
}


void GameTricks::write(ostream& ostr) const
{
  ostr << '\n'
    << "tricks:\n"
    << '\n';
  unsigned i = 0;
  for (auto const& trick : *this) {
    ostr << "trick " << i+1 << '\n'
      << trick << '\n';
    i += 1;
  }

  ostr << '\n';
}
