/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../card/trick.h"
#include "tricks_iterator.h"
class Game;

/**
 ** The tricks of a game
 **/
class GameTricks {
  friend class Game;
public:
  using iterator = GameTricksIterator;
  using const_iterator = GameTricksConstIterator;

  explicit GameTricks(Game& game) noexcept;
  GameTricks(GameTricks const& tricks, Game& game);

  GameTricks(GameTricks const&) = delete;

  void self_check() const;

  auto empty()    const noexcept -> bool;
  auto size()     const noexcept -> unsigned;
  auto max_size() const          -> unsigned;

  auto begin()       noexcept -> iterator;
  auto end()         noexcept -> iterator;
  auto begin() const noexcept -> const_iterator;
  auto end()   const noexcept -> const_iterator;

  auto trick(unsigned p)       -> Trick&;
  auto trick(unsigned p) const -> Trick const&;

  void add(Trick trick);

  auto current()              const          -> Trick const&;
  auto current()                             -> Trick&;
  auto tricks_in_trickpiles() const          -> unsigned;
  auto current_no()           const          -> unsigned;
  auto real_current_no()      const noexcept -> unsigned;
  auto remaining_no()         const          -> unsigned;
  auto real_remaining_no()    const          -> unsigned;
  auto is_last_trick()        const          -> bool;
  auto trump_tricks_no()      const          -> unsigned;

  auto has_trick(Team team)                   const -> bool;
  auto has_trick(Player const& player)        const -> bool;
  auto number_of_tricks(Player const& player) const -> unsigned;
  auto number_of_tricks_till_trick(Player const& player, unsigned trickno) const -> unsigned;
  auto number_of_tricks(Team team)            const -> unsigned;

  void write(ostream& ostr) const;

private:
  Game* const game_;
  vector<Trick> tricks_;

public:
  Signal<void(Trick const&)> signal_trick_open;
  Signal<void(Trick const&)> signal_trick_full;
  Signal<void(Trick const&)> signal_trick_move_in_pile;
  Signal<void(Trick const&)> signal_trick_close;
};
