/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

// included by tricks.h

#pragma once

#include "constants.h"

class GameTricks;

class GameTricksIterator {
  friend class GameTricks;
public:
  using iterator_category = std::forward_iterator_tag;
  using value_type      = Trick;
  using difference_type = std::ptrdiff_t;
  using pointer         = Trick*;
  using reference       = Trick&;

private:
  GameTricksIterator(GameTricks& tricks, size_t p) noexcept;

public:
  auto operator*() -> Trick&;
  auto operator*() const -> Trick const&;
  auto operator->() -> Trick*;
  auto operator->() const -> Trick const*;
  auto operator++() noexcept -> GameTricksIterator&;
  auto operator==(GameTricksIterator const& i) const noexcept -> bool;
  auto operator!=(GameTricksIterator const& i) const noexcept -> bool;

private:
  GameTricks* const tricks_;
  size_t p_ = 0;
};

class GameTricksConstIterator {
  friend class GameTricks;
public:
  using iterator_category = std::forward_iterator_tag;
  using value_type      = Trick;
  using difference_type = std::ptrdiff_t;
  using pointer         = Trick*;
  using reference       = Trick&;

private:
  GameTricksConstIterator(GameTricks const& tricks, size_t p) noexcept;

public:
  GameTricksConstIterator(GameTricksIterator i);
  auto operator*()  const -> Trick const&;
  auto operator->() const -> Trick const*;
  auto operator++() noexcept -> GameTricksConstIterator&;
  auto operator==(GameTricksConstIterator const& i) const noexcept -> bool;
  auto operator!=(GameTricksConstIterator const& i) const noexcept -> bool;

private:
  GameTricks const* const tricks_;
  size_t p_ = 0;
}; 
