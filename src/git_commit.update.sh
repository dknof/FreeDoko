#!/bin/sh

GIT_COMMIT=\"`git describe --tags --dirty=" -- modifiziert"`\"

if [ -f git_commit.txt ]; then
  echo "${GIT_COMMIT}" | diff -q - git_commit.txt >/dev/null 2>&1
  #echo "${GIT_COMMIT}" | diff -q - git_commit.txt
  if [ $? -ne 0 ]; then
    echo "${GIT_COMMIT}" > git_commit.txt
  fi
else
  echo "${GIT_COMMIT}" > git_commit.txt
fi

