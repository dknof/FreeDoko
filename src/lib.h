/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "game/reservation.h"
#include "card/card.h"
#include "basetypes/announcement.h"

namespace FreeDoko {
auto reservation(int player, string const& text)  -> Reservation;
auto reservation(int player, istream& istr)       -> Reservation;
auto card_to_play(int player, string const& text) -> Card;
auto card_to_play(int player, istream& istr)      -> Card;
auto announcement(int player, string const& text) -> Announcement;
auto announcement(int player, istream& istr)      -> Announcement;
} // namespace FreeDoko
