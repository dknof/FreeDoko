/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef BUG_REPORT

#include "bug_report.h"

#include <fstream>

#include "../versions.h"
#include "../utils/file.h"
#include "../utils/string.h"

#include "../basetypes/program_flow_exception.h"
#include "../party/party.h"
#include "../party/rule.h"
#include "../game/gameplay.h"
#include "../game/game_summary.h"
#include "../player/ai/ai.h"
#include "../misc/preferences.h"
#include "../os/bug_report_replay.h"
#include "../ui/ui.h"

#ifdef WINDOWS
// for 'mkdir'
#include <io.h>
#else
// for 'mkdir'
#include <sys/stat.h>
#endif
#include <ctime>
#include <utility>


namespace {
auto create_ostr(Path const& subdir, Party const& party) -> std::pair<unique_ptr<ofstream>, Path>;
auto standard_directory() -> Path;
auto unique_path(Path directory, string const& prefix) -> Path;
void write_header(ostream& ostr, string const& message);
void write_party(ostream& ostr, Party const& party);
void write_game_type(ostream& ostr, Game const& game);
void write_hands(ostream& ostr, Game const& game);
void write_gameplay(ostream& ostr, Gameplay const& gameplay);
void write_game_summary(ostream& ostr, GameSummary const& game_summary);
string const bug_reports_directory = "FreeDoko Fehlerberichte"s;
string const bug_report_file = "Fehlerbericht.FreeDoko"s;
} // namespace


namespace BugReport {

void create_assertion_bug_report(string const& message)
{
  auto const backtrace = backtrace_string(2);
  // always ouput the error message on stderr
  cerr << message << '\n';
  cerr << "\n\n"
    << "Backtrace:\n" << backtrace;
#ifdef BUG_REPORT_REPLAY
  if (::bug_report_replay)
    cerr << "--\n"
      << _("replayed bug report: %s", ::bug_report_replay->file().string()) << '\n';
#endif

  static bool already_running = false;
  if (already_running) {
    cerr << _("BugReport::automatic report creation failed")
      << '\n';

    cerr << _("BugReport::write seed: %u",
              ::party->seed())
      << '\n';
    cerr << _("BugReport::write startplayer: %u",
              ::party->startplayer())
      << '\n';

#ifdef ASSERTION_GENERATES_SEGFAULT
    {
      cerr << '\n'
        << _("Creating segmentation fault.") << '\n';
      SEGFAULT;
    }
#endif // #ifdef ASSERTION_GENERATES_SEGFAULT

    std::exit(EXIT_FAILURE);
  } // if (already_running)
  already_running = true;

  ::ui->error(message, backtrace);

  already_running = false;
}

auto create_bug_report(Party const& party, string const& message, string const& subdir) -> Path
{
  auto result = create_ostr(subdir, party);
  std::ostream* ofstr = result.first.get();
  auto& path = result.second;
  if (!ofstr || ofstr->fail()) {
    cerr << _("BugReport::could not save")
      << endl;
    cerr << _("Press <Return> to display the bug report.")
      << endl;
    std::cin.get();
    ofstr = &cerr;
    path = "standard error";
  }

  write_header(*ofstr, message);
  write_party(*ofstr, party);

  if (party.in_game()) {
    auto const& game = party.game();

    write_game_type(*ofstr, game);
    write_hands(*ofstr, game);
    write_gameplay(*ofstr, game.gameplay());
    if (game.status() >= GameStatus::finished)
      write_game_summary(*ofstr, party.last_game_summary());
  }

  // close the file stream
  *ofstr << flush;

  return path;
}

void create_from_dokolounge(string const erstellername,
                            int const tischnummer, string const spielername,
                            string const message)
{
  auto const now = std::time(nullptr);
  char today[11]; // NOLINT
  std::strftime(today, sizeof(today), "%Y-%m-%d", std::localtime(&now));
  auto const directory = File::executable_directory / "Fehlerberichte" / today;
  if (!is_directory(directory)) {
    cout << "Erstelle Verzeichnis " << directory << " für die Fehlerberichte.\n";
    create_directory(directory);
  }

  // ToDo: Tischnummer, Spielername, laufende Nummer
  auto file = directory / (spielername + "_" + std::to_string(tischnummer) + ".Fehlerbericht.FreeDoko");

  if (is_regular_file(file)) {
    int i = 0;
    do {
      i += 1;
      file = directory / (spielername + "_" + std::to_string(tischnummer) + "-" + std::to_string(i) + ".Fehlerbericht.FreeDoko");
    } while (is_regular_file(file));
  }

  ofstream ostr(file);
  if (ostr.fail())
    return ;

  write_header(ostr, message);

  ostr.close();

  cout << "Fehlerbericht in " << file << " gespeichert.\n";
}


void create_from_dokolounge(string const erstellername,
                            Game const& game,
                            int const tischnummer, string const spielername,
                            string const message)
{
  auto const now = std::time(nullptr);
  char today[11]; // NOLINT
  std::strftime(today, sizeof(today), "%Y-%m-%d", std::localtime(&now));
  auto const directory = File::executable_directory / "Fehlerberichte" / today;
  if (!is_directory(directory)) {
    cout << "Erstelle Verzeichnis " << directory << " für die Fehlerberichte.\n";
    create_directory(directory);
  }

  // ToDo: Tischnummer, Spielername, laufende Nummer
  auto file = directory / (std::to_string(tischnummer) + "_" + spielername + ".Fehlerbericht.FreeDoko");

  if (is_regular_file(file)) {
    int i = 0;
    do {
      i += 1;
      file = directory / (std::to_string(tischnummer) + "_" + spielername + "-" + std::to_string(i) + ".Fehlerbericht.FreeDoko");
    } while (is_regular_file(file));
  }

  ofstream ostr(file);
  if (ostr.fail())
    return ;

  auto const& rule = game.rule();

  write_header(ostr, message);

  ostr << "\nstartplayer: " << game.startplayer().no() << '\n';

  ostr << '\n'
    << "rules\n"
    << "{\n";
  for (auto const r : {
       Rule::Type::with_nines,
       Rule::Type::poverty,
       Rule::Type::swines,
       Rule::Type::hyperswines,
       Rule::Type::swines_announcement_begin,
       Rule::Type::hyperswines_announcement_begin,
       Rule::Type::lustsolo_player_leads,
       Rule::Type::dullen_second_over_first,
       Rule::Type::dullen_contrary_in_last_trick,
       Rule::Type::extrapoint_catch_charlie,
       Rule::Type::extrapoints_in_solo,
       Rule::Type::cowardliness_dokolounge,
       }) {
    ostr << "  " << to_string(r) << " = " << (rule.value(r)? "true" : "false") << '\n';
  }
  ostr << '\n';
  for (auto const r : {
       Rule::Type::announcement_no_120,
       Rule::Type::announcement_no_90,
       Rule::Type::announcement_no_60,
       Rule::Type::announcement_no_30,
       Rule::Type::announcement_no_0,
       }) {
    ostr << "  " << to_string(r) << " = " << rule.value(r) << '\n';
  }
  ostr << "}\n";
  ostr << '\n';
  ostr << "players\n"
    << "{\n";
  for (auto const& player : game.players()) {
    ostr << "{\n"
      << "name = " << player.name() << '\n'
      << "type = " << (player.name() == erstellername ? "human" : "ai") << '\n';
    if (player.type() == Person::Type::ai) {
      ostr << "difficulty = " << dynamic_cast<Ai const&>(player).difficulty() << '\n';
    }
    ostr << "}\n";
  }
  ostr << "}\n\n";

  write_game_type(ostr, game);
  write_hands(ostr, game);

  write_gameplay(ostr, game.gameplay());

  if (game.status() >= GameStatus::finished)
    write_game_summary(ostr, GameSummary(game));

  ostr.close();

  cout << "Fehlerbericht in " << file << " gespeichert.\n";
}


auto default_filename(Party const& party) -> string
{
  string const prefix = (  party.in_game()
                         ? String::to_string(party.game().seed(), 6, '0')
                         : string("party")
                        );
  return prefix + "." + bug_report_file;
}

} // namespace BugReport

namespace {

auto create_ostr(Path const& subdir, Party const& party) -> std::pair<unique_ptr<ofstream>, Path>
{
  // Remark: could use a stack here
  vector<Path> const directories = {
#ifdef REST_SERVICE
    "./Fehlerberichte",
#endif
    standard_directory(),
    ::preferences.config_directory() / bug_reports_directory,
    ::preferences.config_directory(),
    "./" + bug_reports_directory,
    "."
  };

  string const prefix = (  party.in_game()
                         ? String::to_string(party.game().seed(), 6, '0')
                         : string("party")
                        );

  for (auto const& dir : directories) {
    auto const directory = ((subdir.empty() || (subdir == "."))
                            ? dir
                            : dir / subdir);
    if (!is_directory(directory)) {
      create_directory(directory);
      ::ui->information(_("BugReport::created directory '%s'", directory.string()),
                        InformationType::normal);
    }

    auto const file = unique_path(directory, prefix);
    auto ofstr = make_unique<ofstream>(file, std::ios::app);
    if (!ofstr->fail())
      return std::make_pair(std::move(ofstr), file);
  }
  return std::make_pair<unique_ptr<ofstream>, Path>({}, {});
}


auto standard_directory() -> Path
{
  if (::preferences(Preferences::Type::save_bug_reports_on_desktop)) {
    return (Path(File::desktop_directory())
            / bug_reports_directory);
  }

  return (::preferences.config_directory() / bug_reports_directory);
}


auto unique_path(Path const directory, string const& prefix) -> Path
{
  auto filename = prefix + "." + bug_report_file;
  if (!is_regular_file(directory / filename))
    return directory / filename;

  for (unsigned n = 1; n < 999; ++n) { // NOLINT
    // Check whether the file does not exists, yet.
    // If the file does already exists,
    // append a number and search the first which does not exists.
    filename = (prefix
                + "-" + String::to_string(n)
                + "." + bug_report_file); // NOLINT performance-inefficient-string-concatenation
    if (!is_regular_file(directory / filename))
      return directory / filename;
  }

  return directory / filename;
}


void write_header(ostream& ostr, string const& message)
{
  auto const time_now = time(nullptr);
  ostr << "# FreeDoko Bugreport\n"
    << '\n'
    << "version: " << *::version
    << '\n'
    << "compiled: " << __DATE__ << ", " << __TIME__ << '\n'
#ifdef WINDOWS
    << "system: Windows\n"
#endif
#ifdef LINUX
    << "system: Linux\n"
#endif
#ifdef HPUX
    << "system: HPUX\n"
#endif
    << "time: " << asctime(localtime(&time_now)) << "\n"
    << "language: " << ::preferences(Preferences::Type::language) << "\n";

  if (   ::party->in_game()
      && ::party->game().status() != Game::Status::init)
    ostr << "trick: " << ::party->game().tricks().current_no() << '\n';

  ostr << '\n'
    << "/---------\\\n"
    << "| message |\n"
    << "\\---------/\n"
    << message << "\n\n"
    << "/--------\\\n"
    << "| output |\n"
    << "\\--------/\n";
}


void write_party(ostream& ostr, Party const& party)
{
  auto const write_database_bak = Player::write_database;
  Player::write_database = false;

  ostr << "seed: " << party.seed() << '\n';
  ostr << "startplayer: " << party.startplayer() << '\n';
  ostr << '\n';
  ostr << "party\n"
    << "{\n"
    << *::party
    << "}\n";

  Player::write_database = write_database_bak;
}


void write_game_type(ostream& ostr, Game const& game)
{
  ostr << '\n'
    << "gametype: " << game.type()
    << '\n';

  switch (game.type()) {
  case GameType::normal:
    break;

  case GameType::marriage_silent:
  case GameType::marriage: {
    ostr << "selector: " << to_string(game.players().soloplayer().reservation().marriage_selector) << '\n'
      << "bride: player "
      << game.players().soloplayer().no()
      << '\n';
  } break;

  case GameType::poverty: {
    // ToDo
  } break;

  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
  case GameType::fox_highest_trump:
  case GameType::redistribute:
  case GameType::marriage_solo:
  case GameType::solo_meatless:
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond: {
    ostr << "soloplayer: player "
      << game.players().soloplayer().no()
      << '\n';
    break;
  }
  }
}


void write_hands(ostream& ostr, Game const& game)
{
  // ToDo: In einer Armut die originalen Verteilungen vor dem Schieben ausgeben
  ostr << '\n'
    << "hands\n"
    << "{";
  for (auto const& player : game.players()) {
    ostr << '\n'
      << "# " << player.name() << "\n"
      << player.hand().cards_all();
  }
  ostr << "}\n";
}


void write_gameplay(ostream& ostr, Gameplay const& gameplay)
{
  ostr << '\n'
    << "gameplay actions\n"
    << "{\n";
  unsigned t = 0;
  for (auto const& a : gameplay.actions()) {
    auto const& action = *a;
    switch (action.type()) {
    case GameplayActions::Type::poverty_denied_by_all:
    case GameplayActions::Type::trick_taken:
    case GameplayActions::Type::check:
    case GameplayActions::Type::print:
    case GameplayActions::Type::quit:
      // do not write
      break;

    case GameplayActions::Type::reservation:
    case GameplayActions::Type::poverty_shift:
    case GameplayActions::Type::poverty_accepted:
    case GameplayActions::Type::poverty_returned:
    case GameplayActions::Type::poverty_denied:
    case GameplayActions::Type::card_played:
    case GameplayActions::Type::announcement:
    case GameplayActions::Type::swines:
    case GameplayActions::Type::hyperswines:
    case GameplayActions::Type::marriage:
    case GameplayActions::Type::trick_full:
      // do write
      ostr << "  " << action << '\n';
      break;

    case GameplayActions::Type::trick_open:
      t += 1;
      ostr << "  # trick " << t << '\n';
    }
  }
  ostr << "}\n";
}


void write_game_summary(ostream& ostr, GameSummary const& game_summary)
{
  ostr << '\n'
    << "game summary\n"
    << "{\n"
    << game_summary
    << "}\n";

  { // special points
    ostr << "special points\n"
      << "{\n"
      << "\tteam\tvalue\tdescription:\n";
    for (auto const& special_point
         : game_summary.specialpoints()) {
      ostr << '\t';
      ostr << special_point.team;
      // extra rule
      if ((special_point.type == Specialpoint::Type::no120_said)
          && (::party->rule()(Rule::Type::announcement_re_doubles)))
        ostr << "\t" << "*2";
      else
        ostr << "\t"
          << value(special_point.type);
      ostr << "\t" << to_string(special_point.type)
        << '\n';
    } // for (special_point)
    ostr << "\t= " << game_summary.points() << '\n';

    ostr << "}\n";

  } // special points
}

} // namespace

#endif
