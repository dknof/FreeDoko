/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"


#include "../card/card.h"

#include "preferences.h"

class Preferences::CardsOrder {
public:
  enum class Direction {
    none, up, down
  };
  using Position = unsigned;

public:
  explicit CardsOrder(Preferences& preferences);
  CardsOrder(Preferences& preferences,
             Card::TColor c1, Direction d1,
             Card::TColor c2, Direction d2,
             Card::TColor c3, Direction d3,
             Card::TColor c4, Direction d4,
             Card::TColor c5, Direction d5);
  CardsOrder(Preferences& preferences, string const& s);
  CardsOrder(CardsOrder const& cards_order);
  auto operator=(CardsOrder const& cards_order) -> CardsOrder&;

  auto ensure_validity() -> bool;

  operator bool() const;
  operator string() const;

  auto operator==(CardsOrder const& rhs) const -> bool;

  auto sorted() const -> bool;
  auto mixed() const -> bool;

  auto relative_position(HandCard a, HandCard b) const -> int;

  auto direction(Card::TColor tcolor) const -> Direction;
  auto direction(Position pos) const -> Direction;
  auto pos(Card::TColor tcolor) const -> Position;
  auto tcolor(Position pos) const -> Card::TColor;

  // toggle / set the direction
  void direction_toggle(Card::TColor tcolor);
  void direction_toggle(Position pos);
  void direction_set(Card::TColor tcolor, Direction direction);
  void direction_set(Position pos, Direction direction);

  void pos_to_left(Position pos);
  void pos_to_right(Position pos);

  void sorted_set(bool sorted);

  // write of the cards order
  void write(ostream& ostr) const;

private:
  Preferences* const preferences_;

  bool sorted_ = false;

  vector<Position> pos_;
  vector<Direction> direction_;
};
auto to_string(Preferences::CardsOrder::Direction direction) -> string;
auto gettext(Preferences::CardsOrder::Direction direction)   -> string;
auto operator<<(ostream& ostr, Preferences::CardsOrder const& cards_order) -> ostream&;
