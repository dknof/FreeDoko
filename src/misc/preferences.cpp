/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "preferences.h"
#include "preferences.cardsorder.h"

#include "../versions.h"
#include "../basetypes/fast_play.h"

#ifdef LINUX
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
#endif
#ifdef WINDOWS
#include <Shlobj.h>
#endif
#include <sys/stat.h>

#include <thread>

#include "../party/party.h"
#include "../game/game.h"
#include "../player/human/human.h"

#include "../ui/ui.h"

#include "../utils/file.h"
#include "../utils/string.h"
#include "../utils/windows.h"
#include "../class/readconfig/readconfig.h"

namespace {
auto default_config_directory() -> ::Directory;
} // namespace


Preferences::Preferences() :
  cards_order_(make_unique<CardsOrder>(*this))
{
  for (auto t : Type::bool_list) {
    signal_changed_bool_[t] = {};
  }
  for (auto t : Type::unsigned_list) {
    signal_changed_unsigned_[t] = {};
  }
  for (auto t : Type::string_list) {
    signal_changed_string_[t] = {};
  }
}


Preferences::Preferences(Preferences const& preferences):
  bool_(preferences.bool_),
  unsigned_(preferences.unsigned_),
  string_(preferences.string_),
  cards_order_(make_unique<CardsOrder>(*preferences.cards_order_)),
  data_directories_(preferences.data_directories_),
  config_directory_(preferences.config_directory_),
  string_path_(preferences.string_path_)
{
  update_all();
}


auto Preferences::operator=(Preferences const& preferences) -> Preferences&
{
  if (this == &preferences)
    return *this;
  string_path_ = preferences.string_path_;
  for (auto t : Type::bool_list)
    set(t, preferences.value(t));

  for (auto t : Type::unsigned_list)
    set(t, preferences.value(t));

  for (auto t : Type::string_list)
    set(t, preferences.value(t));

  set(Type::cards_order,
            preferences.value(Type::cards_order));

  return *this;
}


void Preferences::init()
{
  set_data_directories();
  config_directory_ = default_config_directory();
  set_to_hardcoded();
}


auto Preferences::signal_changed() noexcept -> Signal<void(int, void const*)>&
{ return signal_changed_; }


auto Preferences::signal_changed() const noexcept -> Signal<void(int, void const*)> const&
{ return signal_changed_; }


auto Preferences::signal_changed(Preferences::Type::Bool const type) -> Signal<void(bool)>&
{ return signal_changed_bool_[type]; }


auto Preferences::signal_changed(Preferences::Type::Bool const type) const -> Signal<void(bool)> const&
{ return signal_changed_bool_.find(type)->second; }


auto Preferences::signal_changed(Preferences::Type::Unsigned const type) -> Signal<void(unsigned)>&
{ return signal_changed_unsigned_[type]; }


auto Preferences::signal_changed(Preferences::Type::Unsigned const type) const -> Signal<void(unsigned)> const&
{ return signal_changed_unsigned_.find(type)->second; }


auto Preferences::signal_changed(Preferences::Type::String const type) -> Signal<void(string)>&
{ return signal_changed_string_[type]; }


auto Preferences::signal_changed(Preferences::Type::String const type) const -> Signal<void(string)> const&
{ return signal_changed_string_.find(type)->second; }


auto Preferences::signal_changed(Preferences::Type::CardsOrder const type) -> Signal<void(Preferences::CardsOrder const&)>&
{ return signal_changed_cards_order_; }


auto Preferences::signal_changed(Preferences::Type::CardsOrder const type) const -> Signal<void(Preferences::CardsOrder const&)> const&
{ return signal_changed_cards_order_; }


void Preferences::set_to_hardcoded()
{
#if 0
  set(Path::logo_file,                "logo.png");
  set(Path::icon_file,                "icon.png");
  set(Path::manual_directory,         "/usr/share/doc/manual");
#endif

  bool_[Type::use_threads]                = true;

  bool_[Type::automatic_savings]          = true;
  bool_[Type::save_party_changes]         = true;
  bool_[Type::additional_party_settings]  = false;

  string_[Type::language]                 = "";

  string_[Type::name]                     = "";

  bool_[Type::show_splash_screen]         = true;
  bool_[Type::splash_screen_transparent]  = false;

  bool_[Type::show_bug_report_button_in_game_finished_window] = true;
  bool_[Type::save_bug_reports_on_desktop] = true;

  bool_[Type::sound]                      = true;

  bool_[Type::automatic_card_suggestion]  = false;
  bool_[Type::announce_swines_automatically] = true;
  bool_[Type::shuffle_without_seed]       = false;
  bool_[Type::replayed_game_with_random_distribution_of_players] = true;
  bool_[Type::show_if_valid]              = true;
  bool_[Type::emphasize_valid_cards]      = false;
  bool_[Type::announce_in_table]          = true;
  bool_[Type::show_all_hands]             = false;
  bool_[Type::show_all_tricks]            = true;
  bool_[Type::show_ai_information_hands]  = false;
  bool_[Type::show_ai_information_teams]  = false;
  bool_[Type::show_trickpiles_points]     = true;
  bool_[Type::show_known_teams_in_game]   = true;
  bool_[Type::show_soloplayer_in_game]    = true;

  bool_[Type::show_full_trick_window]     = false;
  bool_[Type::show_full_trick_window_if_special_points]= true;
  bool_[Type::close_full_trick_automatically] = true;

  bool_[Type::show_gametype_window]                = true;
  bool_[Type::close_gametype_window_automatically] = false;
  bool_[Type::show_marriage_window]                = true;
  bool_[Type::close_marriage_window_automatically] = false;
  bool_[Type::show_announcement_window]            = true;
  bool_[Type::close_announcement_window_automatically] = false;
  bool_[Type::show_swines_window]                  = true;
  bool_[Type::close_swines_window_automatically]   = false;

  bool_[Type::rotate_trick_cards]        = false;
  set(Type::cards_order, "trump up, club down, heart down, spade down, diamond down");


  unsigned_[Type::max_attempts_sec_for_distribution]  =  2;
  unsigned_[Type::min_trumps_to_distribute]       =  0;
  unsigned_[Type::max_trumps_to_distribute]       = 12;
  unsigned_[Type::min_queens_to_distribute]       =  0;
  unsigned_[Type::max_queens_to_distribute]       =  8;
  unsigned_[Type::min_jacks_to_distribute]        =  0;
  unsigned_[Type::max_jacks_to_distribute]        =  8;
  unsigned_[Type::min_club_queens_to_distribute]  =  0;
  unsigned_[Type::max_club_queens_to_distribute]  =  2;
  unsigned_[Type::min_heart_tens_to_distribute]   =  0;
  unsigned_[Type::max_heart_tens_to_distribute]   =  2;
  unsigned_[Type::min_diamond_aces_to_distribute] =  0;
  unsigned_[Type::max_diamond_aces_to_distribute] =  2;
  unsigned_[Type::min_color_aces_to_distribute]   =  0;
  unsigned_[Type::max_color_aces_to_distribute]   =  6;
  unsigned_[Type::min_blank_colors_to_distribute] =  0;
  unsigned_[Type::max_blank_colors_to_distribute] =  3;

  unsigned_[Type::card_play_delay]                 = 500;
  unsigned_[Type::full_trick_close_delay]          = 2000;

  unsigned_[Type::gametype_window_close_delay]     = 2000;
  unsigned_[Type::marriage_window_close_delay]     = 20;
  unsigned_[Type::announcement_window_close_delay] = 2000;
  unsigned_[Type::swines_window_close_delay]       = 2000;

  bool_[Type::original_cards_size]                 = true;
  unsigned_[Type::cards_height]                    = 140;

  bool_[Type::own_hand_on_table_bottom]            = true;
  bool_[Type::static_hands]                        = true;
  unsigned_[Type::table_rotation]                  = 0;

  bool_[Type::ui_main_window_fullscreen]           = false;
  unsigned_[Type::ui_main_window_pos_x]            = 0;
  unsigned_[Type::ui_main_window_pos_y]            = 0;
  unsigned_[Type::ui_main_window_width]            = 0;
  unsigned_[Type::ui_main_window_height]           = 0;

  //string_[Type::cardset]        = "xskat/french";
  //string_[Type::cards_back]            = "xskat";
  { // set the cardset
    string_[Type::cardset] = "";
    string_path_[Type::cardset] = "";
    string_[Type::cards_back] = "";
    string_path_[Type::cards_back] = "";

    vector<string> cardsets;

    if ((*this)(Type::language) == "de") {
      if (Date() <= Preferences::innocard_expiration_date)
        cardsets.emplace_back("InnoCard/french/185");
      cardsets.emplace_back("VectorPlayingCards/185");
      cardsets.emplace_back("xskat/french");
      cardsets.emplace_back("xskat/german");
    } else if ((*this)(Type::language) == "de-alt") {
      cardsets.emplace_back("xskat/german");
      cardsets.emplace_back("freedoko-pingucards");
    } else { // english and so
      if (Date() <= Preferences::innocard_expiration_date)
        cardsets.emplace_back("InnoCard/english/185");
      cardsets.emplace_back("VectorPlayingCards/185");
      cardsets.emplace_back("dondorf");
    } // if (language == )
    cardsets.emplace_back("VectorPlayingCards/185");
    cardsets.emplace_back("xskat/french");
    cardsets.emplace_back("dondorf");
    cardsets.emplace_back("kdecarddecks/spaced");
    cardsets.emplace_back("pysol/gpl");
    cardsets.emplace_back("pysol/a-m/gpl");
    cardsets.emplace_back("openclipart");

    auto cardset = cardsets.begin();
    for (; cardset != cardsets.end(); ++cardset) {
      set(Type::cardset, *cardset);
      if (!path(Type::cardset).empty())
        break;
    }

    if (cardset == cardsets.end()) {
      string_[Type::cardset] = "";
      if (::ui)
        ::ui->error(_("Error::loading default cardset %s",
                      cardsets.front()
                     ));
    } else {
      string_[Type::cardset] = *cardset;
    }
  } // set the cardset
  { // set the cards back
    vector<string> backs = {"penguin", "back"};

    auto back = backs.begin();
    for (; back != backs.end(); ++back) {
      string_[Type::cards_back] = *back;
      if (!path(Type::cards_back).empty())
        break;
    } // for (back \in backs)

    if (back == backs.end()) {
      string_[Type::cards_back] = "";
      if (::ui)
        // gettext: %s = back, %s = cardset
        ::ui->error(_("Error::loading default cards back %s (%s)",
                      backs.front(),
                      value(Type::cardset)
                     ));
    } else {
      string_[Type::cards_back] = *back;
    }
  } // set the cards back
  { // set the icons
    string_[Type::iconset] = "default";
    string_path_[Type::iconset] = "";
  } // set the icons
  { // set the background
    string_[Type::background] = "";
    string_path_[Type::background] = "";

    vector<string> backgrounds = {"table.png"};

    auto background = backgrounds.begin();
    for (; background != backgrounds.end(); ++background) {
      string_[Type::background] = *background;
      update_path(Type::background);
      if (!path(Type::background).empty())
        break;
    } // for (background \in backgrounds)

    if (background == backgrounds.end()) {
      string_[Type::background] = "";
      if (::ui)
        ::ui->error(_("Error::loading default background %s",
                      backgrounds.front()
                     ));
    }
  } // set the background


#ifdef WINDOWS
  string_[Type::name_font]            = "Arial Bold Italic 24";
#else
  string_[Type::name_font]            = "Sans Bold Italic 24";
#endif
  string_[Type::name_font_color]        = "black";
  string_[Type::name_active_font_color]    = "red";
  string_[Type::name_reservation_font_color] = "blue";
#ifdef WINDOWS
  string_[Type::trickpile_points_font]    = "Arial Bold 24";
#else
  string_[Type::trickpile_points_font]    = "Sans Bold 24";
#endif
  string_[Type::trickpile_points_font_color] = "black";

  string_[Type::poverty_shift_arrow_color] = "black";

#ifdef USE_SOUND_COMMAND
  string_[Type::play_sound_command]        = "";
#endif
  string_[Type::browser_command]        = "";
  string_[Type::cardset_license]        = "";
  string_[Type::iconset_license]        = "";

#ifdef DOKOLOUNGE_THEME
  string_[Type::name_font_color]        = "#eadccc";
//RGB(234,220,204)
  string_[Type::iconset] = "Doko-Lounge";
  set(Type::background, "Doko-Lounge.jpg");
#endif
} // void Preferences::set_to_hardcoded()


auto Preferences::data_directories() const -> vector<Directory> const&
{ return data_directories_; }

 
void Preferences::set_data_directories()
{
  // search the directory
  data_directories_.clear();
  data_directories_.emplace_back(".");
  if (getenv("FREEDOKO_DATA_DIRECTORY"))
    data_directories_.emplace_back(getenv("FREEDOKO_DATA_DIRECTORY"));
  if (getenv("FREEDOKO_MORE_DATA_DIRECTORY"))
    data_directories_.emplace_back(getenv("FREEDOKO_MORE_DATA_DIRECTORY"));
  if (getenv("XDG_DATA_HOME")) {
    data_directories_.emplace_back(Directory(getenv("XDG_DATA_HOME")) / "FreeDoko");
  } else if (getenv("HOME")) {
    data_directories_.emplace_back(Directory(getenv("HOME")) /".local"/"share"/"FreeDoko");
  }
  data_directories_.emplace_back(config_directory());

#ifdef PUBLIC_DATA_DIRECTORY_VALUE
  data_directories_.emplace_back(PUBLIC_DATA_DIRECTORY_VALUE);
#else // !#ifdef PUBLIC_DATA_DIRECTORY_VALUE

#ifndef RELEASE
  data_directories_.emplace_back(File::absolute_path_from_executable_directory("../data"));
#endif
  data_directories_.emplace_back(File::absolute_path_from_executable_directory("."));

#endif // !#ifdef PUBLIC_DATA_DIRECTORY_VALUE

  data_directories_ = File::without_nonexisting(data_directories_);
  data_directories_ = File::absolute(data_directories_);
  data_directories_ = File::without_duplicates(data_directories_);
}


void Preferences::add_data_directory(Directory path)
{
  if (path.is_relative())
    path = File::executable_directory / path;
  if (contains(data_directories_, path))
    return ;
  if (!is_directory(path))
    return ;
  data_directories_.push_back(path);
}

auto Preferences::data_directories_string(string const indent) const -> string
{
  string l;
  for (auto const& d : data_directories_)
    l += indent + d.string() + "\n";
  return l;
}


void Preferences::set_config_directory(Directory path)
{
  config_directory_ = path;
}


auto Preferences::config_directory() const -> Directory
{
  return config_directory_;
}


auto Preferences::directories(Type::String const type) const -> vector<Directory>
{
  using Result = vector<Directory>;
  switch (type) {
  case Type::background: {
    Result directories;
    for (auto const& d : data_directories()) {
      directories.emplace_back(d / "backgrounds");
    }
#ifdef WINDOWS
    directories.push_back("C:/Program Files (x86)/PySol Fan Club edition/data/tiles"s);
    directories.push_back("C:/Program Files/PySol Fan Club edition/data/tiles"s);
#else
    directories.push_back("/usr/share/PySolFC/tiles"s); // archlinux, CentOS
    directories.push_back("/usr/share/games/pysolfc/tiles"s); // debian
    directories.push_back("/usr/local/share/PySolFC/tiles"s);
    directories.push_back("/usr/local/share/games/pysolfc/tiles"s);
#endif
    return directories;
  }
  case Type::cardset: {
    Result directories;
    for (auto const& d : data_directories()) {
      directories.emplace_back(d / "cardsets");
    }
#ifdef WINDOWS
    directories.push_back("C:/Program Files (x86)/PySol Fan Club edition/data"s);
    directories.push_back("C:/Program Files/PySol Fan Club edition/data"s);
#else
    directories.push_back("/usr/share/PySolFC"s); // archlinux, CentOS
    directories.push_back("/usr/share/games/pysolfc"s); // debian
    directories.push_back("/usr/local/share/PySolFC"s);
    directories.push_back("/usr/local/share/games/pysolfc"s);
#endif
    return directories;
  }
  case Type::cards_back: {
    Result directories;
    switch (cardset_format()) {
    case CardsetFormat::freedoko:
      for (auto const& d : data_directories()) {
        directories.emplace_back(d / "cardsets" / value(Type::cardset) / "backs");
      }
      break;
    case CardsetFormat::pysol:
      directories.push_back(path(Type::cardset));
      break;
    case CardsetFormat::kde:
      directories.push_back(path(Type::cardset));
      break;
    case CardsetFormat::gnome:
      directories.push_back(path(Type::cardset));
      break;
    case CardsetFormat::unknown:
      break;
    }
    return directories;
  }
  case Type::iconset: {
    Result directories;
    for (auto const& d : data_directories()) {
      directories.emplace_back(d / "iconsets");
    }
    return directories;
  }
  default:
    break;
  }

  return {};
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Preferences::operator()(Type::Bool const type) const -> bool
{
  if (   ::fast_play & FastPlay::show_all_hands
      && type == Type::show_all_hands)
    return true;

  return (   dependencies(type)
          && value(type));
}


auto Preferences::operator()(Type::Unsigned const type) const -> unsigned
{
  return value(type);
}


auto Preferences::operator()(Type::String const type) const -> string
{
  string value = this->value(type);

  if (value.empty()) {
    switch(type) {
    case Type::name:
#ifdef LINUX
      value = getpwuid(geteuid())->pw_gecos;
      if (value.empty())
        // no real name - take the login
        value = getpwuid(geteuid())->pw_name;
#endif
#ifdef WINDOWS
#ifdef USE_REGISTRY
      {
        TCHAR  infoBuf[1001];
        infoBuf[1000] = 0;
        DWORD  bufCharCount = 1000;
        if (!GetUserName(infoBuf, &bufCharCount))
          value = "Player";
        else
          value = infoBuf;
      }
#else
      value = "Player";
#endif
#endif
      break;
    case Type::language:
      value = "en";
#ifdef LINUX
      if (getenv("LANG")) {
        if (string(getenv("LANG"), 0, 2) == "de"s) // NOLINT(bugprone-string-constructor)
          value = "de";
      }
#endif
#ifdef WINDOWS
      value = Reg_read(HKEY_CURRENT_USER,
                       "Software\\FreeDoko",
                       "Language");
      if (value.empty())
        value = Reg_read(HKEY_LOCAL_MACHINE,
                         "Software\\FreeDoko",
                         "Language");

      if (value.empty()) {
        // Ein bisschen kompliziert, aus der Registry die Sprache auszulesen:
        // zuerst wird der Laenderkode gelesen (Deutsch: 407),
        // anschliessend wird geschaut, welche Sprache zu dem Laendercode
        // gehört.
        string const language_code
          = Reg_read(HKEY_CURRENT_USER,
                     "Control Panel\\International",
                     "Locale");

        if (!language_code.empty()) {
          if (string(language_code, language_code.size() - 1)
              == "7") { // deutsch
            value = "de";
          } else if (string(language_code, language_code.size() - 1)
                     == "9") { // english
            value = "en";
          } else if (string(language_code, language_code.size() - 1)
                     == "A") { // spanish
            value = "en";
          } else if (string(language_code, language_code.size() - 1)
                     == "C") { // francais
            value = "en";
          } else { // unknown language code
            value = "en";
          } // unknown langugage code
        } // if (!language_code.empty())

      } // if (value.empty())

      if (value.empty()) {
        // *** DK
        // I didn't find the language in the registry, so I use this workaround
        if ((Reg_read(HKEY_LOCAL_MACHINE,
                      "Software\\Microsoft\\Windows\\CurrentVersion",
                      "ProgramFilesDir") == string("C:\\Programme"))
            && (Reg_read(HKEY_LOCAL_MACHINE,
                         "Software\\Microsoft\\Windows\\CurrentVersion",
                         "ProgramFilesPath") == string("C:\\Programme")
               )
           )
          value = "de";
      } // if (value.empty())
      // We have mostly german users
      if (value.empty())
        value = "de";
#endif
      break;
    case Type::cardset:
      value = "xskat/french";
      break;
    case Type::cards_back:
      value = "default";
      break;
    case Type::iconset:
      value = "default";
      break;
    case Type::background:
      value = "default";
      break;
    case Type::name_font:
      value = "Sans Bold Italic 16";
      break;
    case Type::name_font_color:
      value = "black";
      break;
    case Type::name_active_font_color:
      value = "red";
      break;
    case Type::name_reservation_font_color:
      value = "blue";
      break;
    case Type::trickpile_points_font:
      value = "Sans Bold 20";
      break;
    case Type::trickpile_points_font_color:
      value = "black";
      break;
    case Type::poverty_shift_arrow_color:
      value = "black";
      break;
#ifdef USE_SOUND_COMMAND
    case Type::play_sound_command:
#ifdef WINDOWS
#ifdef USE_REGISTRY
      { // Look in the registry for the default program for wav files
        string class_str
          = Reg_read(HKEY_CURRENT_USER,
                     "Software\\CLASSES\\.wav",
                     "");
        if (class_str.empty())
          class_str = Reg_read(HKEY_LOCAL_MACHINE,
                               "Software\\CLASSES\\.wav",
                               "");
        if (!class_str.empty()) {
          value = Reg_read(HKEY_CURRENT_USER,
                           "Software\\CLASSES\\" + class_str
                           + "\\shell\\open\\command",
                           "");
          if (value.empty())
            value = Reg_read(HKEY_LOCAL_MACHINE,
                             "Software\\CLASSES\\" + class_str
                             + "\\shell\\open\\command",
                             "");
          if (   (value.size() >= 5)
              && string(value, value.size() - 4, std::string::npos) == "\"\%1\"")
            value = string(value, 0, value.size() - 4);
        } // if (!class_str.empty())
      } // Look in the registry for the default program for wav files
#else
#endif
      if (value.empty())
        value = "explorer";
#else // #ifdef WINDOWS
      value = "aplay";
#endif // #ifdef WINDOWS
      break;
#endif // #ifdef USE_SOUND_COMMAND

    case Type::browser_command:
#ifdef LINUX
      value = "echo could not open a browser for"; // if nothing else works
      if (getenv("BROWSER") != nullptr) {
        value = string(getenv("BROWSER"));
        break;
      }  // if (getenv("BROWSER") != nullptr)

      { // test some browsers and take the first found
        static auto constexpr browsers = {
          "firefox",
          "chromium",
          "google-chrome",
          "epiphany",
          "galeon",
          "mozilla",
          "opera",
          "konqueror",
          "rekonq",
          "midori",
          "arora",
          "web",
          "xterm -e w3m",
          "xterm -e links",
          "xterm -e lynx",
        };

        for (auto const& browser : browsers) {
          if (::system(("test -x /usr/bin/"s + browser // NOLINT(cert-env33-c)
                        + " -o -x /usr/bin/X11/"s + browser
                        + " -o -x /usr/local/bin/"s + browser).c_str())
              == 0) {
            value = browser;
            break;
          }
        }
      } // test some browsers and take the first found
#endif
#ifdef WINDOWS
#ifdef USE_REGISTRY
      { // Look in the registry for the default browser for html
        string class_str
          = Reg_read(HKEY_CURRENT_USER,
                     "Software\\CLASSES\\.html",
                     "");
        if (class_str.empty())
          class_str = Reg_read(HKEY_LOCAL_MACHINE,
                               "Software\\CLASSES\\.html",
                               "");
        if (!class_str.empty()) {
          value = Reg_read(HKEY_CURRENT_USER,
                           "Software\\CLASSES\\" + class_str
                           + "\\shell\\open\\command",
                           "");
          if (value.empty())
            value = Reg_read(HKEY_LOCAL_MACHINE,
                             "Software\\CLASSES\\" + class_str
                             + "\\shell\\open\\command",
                             "");
          if (   (value.size() >= 5)
              && string(value, value.size() - 4, std::string::npos) == "\"\%1\"")
            value = string(value, 0, value.size() - 4);
        } // if (!class_str.empty())
      } // Look in the registry for the default browser for html
#else
#endif
      if (value.empty())
        value = "explorer";
#endif
      break;
    case Type::cardset_license:
    case Type::iconset_license:
      value = "???";
      break;
    } // switch(type)
  } // if (value.empty())

  return value;
}


auto Preferences::operator()(Path const type) const -> ::Path
{
  return value(type);
}


auto Preferences::operator()(Type::CardsOrder type) const -> Preferences::CardsOrder const&
{
  return value(type);
}


auto Preferences::operator()(Type::CardsOrder const type) -> Preferences::CardsOrder&
{
  return value(type);
}


auto Preferences::type(string const& name) const -> int
{
  for (auto t : Type::bool_list)
    if (name == to_string(t))
      return t;

  for (auto t : Type::unsigned_list)
    if (name == to_string(t))
      return t;

  for (auto t : Type::string_list)
    if (name == to_string(t))
      return t;

  if (name == to_string(Type::cards_order))
    return Type::cards_order;

  return -1;
}


auto Preferences::type_translated(string const& name) const -> int
{
  for (auto t : Type::bool_list)
    if (name == _(t))
      return t;

  for (auto t : Type::unsigned_list)
    if (name == _(t))
      return t;

  for (auto t : Type::string_list)
    if (name == _(t))
      return t;

  if (name == _(Type::cards_order))
    return Type::cards_order;

  return -1;
}


auto Preferences::type_translated_or_normal(string const& name) const -> int
{
  int type = type_translated(name);
  if (type == -1)
    type = this->type(name);

  return type;
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Preferences::dependencies(Type::Bool const type) const -> bool
{
  switch(type) {
  case Type::use_threads:
#ifdef USE_THREADS
    return true;
#else
    return std::thread::hardware_concurrency() > 1;
#endif
  case Type::automatic_savings:
    return true;
  case Type::save_party_changes:
    return (*this)(Type::automatic_savings);
  case Type::additional_party_settings:
    return true;
  case Type::show_splash_screen:
    return true;
  case Type::splash_screen_transparent:
    return (*this)(Type::show_splash_screen);
  case Type::show_bug_report_button_in_game_finished_window:
  case Type::save_bug_reports_on_desktop:
  case Type::sound:
  case Type::automatic_card_suggestion:
  case Type::announce_swines_automatically:
  case Type::shuffle_without_seed:
  case Type::replayed_game_with_random_distribution_of_players:
  case Type::show_if_valid:
  case Type::emphasize_valid_cards:
  case Type::announce_in_table:
  case Type::show_all_hands:
  case Type::show_all_tricks:
  case Type::show_ai_information_hands:
  case Type::show_ai_information_teams:
  case Type::show_trickpiles_points:
  case Type::show_known_teams_in_game:
  case Type::show_soloplayer_in_game:
    return true;
  case Type::show_full_trick_window_if_special_points:
    return !(*this)(Type::show_full_trick_window);
  case Type::close_gametype_window_automatically:
    return (*this)(Type::show_gametype_window);
  case Type::close_marriage_window_automatically:
    return (*this)(Type::show_marriage_window);
  case Type::close_announcement_window_automatically:
    return (*this)(Type::show_announcement_window);
  case Type::close_swines_window_automatically:
    return (*this)(Type::show_swines_window);
  case Type::show_full_trick_window:
  case Type::close_full_trick_automatically:
  case Type::show_gametype_window:
  case Type::show_marriage_window:
  case Type::show_announcement_window:
  case Type::show_swines_window:
  case Type::rotate_trick_cards:
  case Type::own_hand_on_table_bottom:
  case Type::static_hands:
  case Type::original_cards_size:
  case Type::ui_main_window_fullscreen:
    return true;
  } // switch(type)

  return true;
}


auto Preferences::dependencies(Type::Unsigned const type) const -> bool
{
  switch(type) {
  case Type::full_trick_close_delay:
    return (*this)(Type::close_full_trick_automatically);
  case Type::gametype_window_close_delay:
    return (*this)(Type::close_gametype_window_automatically);
  case Type::marriage_window_close_delay:
    return (*this)(Type::close_marriage_window_automatically);
  case Type::announcement_window_close_delay:
    return (*this)(Type::close_announcement_window_automatically);
  case Type::swines_window_close_delay:
    return (*this)(Type::close_swines_window_automatically);
  case Type::cards_height:
    return !(*this)(Type::original_cards_size);
  default:
    return true;
  } // switch(type)

  return true;
}


auto Preferences::dependencies(Type::String const type) const -> bool
{
  (void)type;
  return true;
}


auto Preferences::value(Type::Bool const type) const -> bool
{
  return bool_.at(type);
}


auto Preferences::value(Type::Unsigned const type) const -> unsigned
{
  return unsigned_.at(type);
}


auto Preferences::value(Type::String const type) const -> string const&
{
  return string_.at(type);
}


auto Preferences::path(Type::String const type) const -> ::Path const&
{
  return string_path_.at(type);
}

auto Preferences::path(Path const type) const -> ::Path
{
  switch (type) {
  case Path::logo_file:
  case Path::icon_file: {
    // search the directory

    for (auto const& dd : data_directories()) {
      auto const file = (dd / value(type));
      if (is_regular_file(file))
        return file; // NOLINT(performance-no-automatic-move)
    }

    return {};
  } // case Path::logo_file, icon_file
  case Path::manual_directory:
    return "";
  } // switch(type)

  return "";
}


// NOLINTNEXTLINE(misc-no-recursion)
void Preferences::search_cards_back()
{
  if (!path(Type::cards_back).empty())
    return ;

  switch (cardset_format()) {
  case CardsetFormat::freedoko:
    { // search the file from the preferences
      auto const file = (path(Type::cardset)
                         / "backs"
                         / value(Type::cards_back));
      if (is_regular_file(file)) {
        set(Type::cards_back, file.string());
        return ;
      }

      for (auto const& dd : data_directories()) {
        auto const file = (dd / (*this)(Type::cardset)
                           / "backs"
                           / value(Type::cards_back));
        if (is_regular_file(file)) {
          set(Type::cards_back, file.string());
          return ;
        }
      } // for (dd : data_directories)
    } // search the file from the preferences

    { // search any back
      for (auto const& dd : data_directories()) {
        for (auto const& entry : std::filesystem::directory_iterator(dd / "backs")) {
          auto const& path = entry.path();
          if (!std::filesystem::is_regular_file(path))
            continue;
          if (   path.extension() == ".png"
              || path.extension() == ".gif"
              || path.extension() == ".jpg") {
            set(Type::cards_back, path.filename().string());
            return ;
          }
        }
      }
    }
    break;
  case CardsetFormat::pysol:
    { // search the file from the preferences
      auto const file = (path(Type::cardset)
                         / value(Type::cards_back));
      if (is_regular_file(file)) {
        set(Type::cards_back, file.string());
        return ;
      }
    } // search the file from the preferences

    { // search any back
      auto const file = (path(Type::cardset)
                         / "back01.gif");
      if (is_regular_file(file)) {
        set(Type::cards_back, file.string());
        return ;
      }
    }
    break;
  case CardsetFormat::kde:
    // ToDo
  case CardsetFormat::gnome:
    // ToDo
  case CardsetFormat::unknown:
    break;
  }

  if (::ui)
    ::ui->error(_("Error::could not find any back for cardset %s", value(Type::cardset)));
}


void Preferences::search_background()
{
  if (!path(Type::background).empty())
    return ;

  // search any background
  for (auto const& directory : directories(Type::background)) {
    for (auto const& entry : std::filesystem::directory_iterator(directory)) {
      auto const& path = entry.path();
      if (!std::filesystem::is_regular_file(path))
        continue;

      auto const extension = path.extension();
      if (   extension == ".png"
          || extension == ".jpg"
          || extension == ".gif") {
        set(Type::background, path.filename().string());
        return ;
      }
    }
  }

  if (::ui)
    ::ui->error(_("Error::could not find any background"));
}


auto Preferences::value(Path const type) const -> ::Path
{
  switch(type) {
  case Path::logo_file:
    return "logo.png";
  case Path::icon_file:
    return "icon.png";
  case Path::manual_directory:
#ifdef MANUAL_DIRECTORY_VALUE
    return MANUAL_DIRECTORY_VALUE;
#endif // !#ifdef MANUAL_DIRECTORY_VALUE
    if (   is_directory(::Path("../src"s))
        && is_directory(::Path("../manual"s)))
      return "../manual";

    return "manual";
  } // switch(type)

  return "";
}


auto Preferences::value(Type::CardsOrder const type) const -> CardsOrder const&
{
  return *cards_order_;
}


auto Preferences::value(Type::CardsOrder const type) -> CardsOrder&
{
  return *cards_order_;
}


auto Preferences::cardset_format() const -> CardsetFormat
{
  return cardset_format({path(Type::cardset)});
}


auto Preferences::cardset_format(::Path const path) const -> CardsetFormat
{
  if (is_directory(path)) {
    if (is_regular_file(path / "cards/club_queen.png"))
      return CardsetFormat::freedoko;
    if (is_regular_file(path / "12c.gif")) // club queen
      return CardsetFormat::pysol;
  } else if (is_regular_file(path)) {
#ifdef TODO
    // p.e. /usr/share/kde4/apps/carddecks/svg-xskat-french/french.svgz
    return CardsetFormat::kde;
    // p.e. /usr/share/aisleriot/cards/dondorf.svgz
    return CardsetFormat::gnome;
#endif
  }

  return CardsetFormat::unknown;
}


auto Preferences::min(Type::Unsigned const type) const -> unsigned
{
  switch(type) {
  case Type::max_attempts_sec_for_distribution:
  case Type::min_trumps_to_distribute:
  case Type::max_trumps_to_distribute:
  case Type::min_queens_to_distribute:
  case Type::max_queens_to_distribute:
  case Type::min_jacks_to_distribute:
  case Type::max_jacks_to_distribute:
  case Type::min_color_aces_to_distribute:
  case Type::max_color_aces_to_distribute:
  case Type::min_blank_colors_to_distribute:
  case Type::max_blank_colors_to_distribute:
  case Type::min_club_queens_to_distribute:
  case Type::max_club_queens_to_distribute:
  case Type::min_heart_tens_to_distribute:
  case Type::max_heart_tens_to_distribute:
  case Type::min_diamond_aces_to_distribute:
  case Type::max_diamond_aces_to_distribute:
    return 0;

  case Type::table_rotation:
    return 0;
  case Type::cards_height:
    return 2;
  default:
    return 0;
  } // switch(type)
}


auto Preferences::max(Type::Unsigned const type) const -> unsigned
{
  switch(type) {
  case Type::max_attempts_sec_for_distribution:
    return 5*60;
  case Type::min_trumps_to_distribute:
  case Type::max_trumps_to_distribute:
    return 12;
  case Type::min_queens_to_distribute:
  case Type::max_queens_to_distribute:
  case Type::min_jacks_to_distribute:
  case Type::max_jacks_to_distribute:
    return 8;
  case Type::min_color_aces_to_distribute:
  case Type::max_color_aces_to_distribute:
    return 6;
  case Type::min_blank_colors_to_distribute:
  case Type::max_blank_colors_to_distribute:
    return 3;
  case Type::min_club_queens_to_distribute:
  case Type::max_club_queens_to_distribute:
  case Type::min_heart_tens_to_distribute:
  case Type::max_heart_tens_to_distribute:
  case Type::min_diamond_aces_to_distribute:
  case Type::max_diamond_aces_to_distribute:
    return 2;

  case Type::card_play_delay:
    return 60000; // 60 seconds
  case Type::full_trick_close_delay:
  case Type::gametype_window_close_delay:
  case Type::marriage_window_close_delay:
  case Type::announcement_window_close_delay:
  case Type::swines_window_close_delay:
    return 60000; // 60 seconds
  case Type::table_rotation:
    return 3;
  case Type::cards_height:
    return 1000;
  case Type::ui_main_window_pos_x:
  case Type::ui_main_window_pos_y:
  case Type::ui_main_window_width:
  case Type::ui_main_window_height:
    return UINT_MAX;
  } // switch(type)

  return 0;
}


void Preferences::set(Type::Bool const type, bool const value)
{
  if (this->value(type) == value)
    return ;

  bool_[type] = value;

  signal_changed(type)(value);
  signal_changed_(type, &value);
}


void Preferences::set(Type::Bool const type, string const& value)
{
  if (   (value != "true")
      && (value != "false")
      && (value != "yes")
      && (value != "no")
      && (value != "0")
      && (value != "1")) {
    cerr << "illegal value '" << value << "' for '" << type << "', "
      << "must be a boolean ('true' or 'false' or 'yes' or 'no' or '1' or '0'. "
      << "Taking 'false'."
      << endl;
  }

  set(type, ((   value == "false"
              || value == "no"
              || value == "0")
             ? false : true));
}


void Preferences::set(Type::Unsigned const type, unsigned const value)
{
  if (this->value(type) == value)
    return ;

  unsigned_[type] = value;

  signal_changed(type)(value);
  signal_changed_(type, &value);
}


void Preferences::set(Type::Unsigned const type, string const& value)
{
  try {
    auto const number = stol(value);
    set(type, number);
  } catch (...) {
    cerr << "illegal value '" << value << "' for '" << type << "', "
      << "must be a digit. "
      << endl;
  }
}


// NOLINTNEXTLINE(misc-no-recursion)
void Preferences::set(Type::String const type, string const& value)
{
  if (this->value(type) == value)
    return ;

  auto const old_value = string_[type];

  // First test, whether this is the default value.
  // If it is, then leave the value empty.
#ifdef POSTPONED
  string_[type] = "";
#endif // #ifdef POSTPONED

  string_[type] = value;

  update_path(type);

  { // check for path values
    switch (type) {
    case Type::background:
    case Type::cards_back:
    case Type::cardset:
    case Type::iconset:
      if (path(type).empty()) {
        string_[type] = old_value;
        break ;
      }
      break;
    default:
      break;
    } // switch (type)
  } // check for path values

  if (this == &::preferences) {
    switch(type) {
    case Type::name:
      break;
    case Type::language:
      set_language((*this)(Type::language));
      break;
    case Type::cardset:
    case Type::iconset:
      { // read the license
        vector<string> const license_files
          = {"license",
            "License",
            "LICENSE",
            "license.txt",
            "License.txt",
            "LICENSE.txt",
            "Lizenz.txt",
            "copyright",
            "Copyright",
            "COPYright",
            "copyright.txt",
            "Copyright.txt",
            "COPYRIGHT.txt",
            "copying.txt",
            "Copying.txt",
            "COPYING.txt"};

        auto const directory = path(type);

        string license_text;

        for (auto const& license : license_files) {
          if (is_regular_file(directory / license)) {
            license_text = String::getfile(directory / license);
          } else if (is_regular_file(directory / ".." / license)) {
            license_text = String::getfile(directory / ".." / license);
          } // if (license found)
          if (!license_text.empty())
            break;
        } // for (license : license_files)

        if (license_text.empty()) {
          // no license found
          if (cardset_format() == CardsetFormat::pysol) {
            license_text = _("cardset pysol: license not found");
          } else {
            license_text = _("license not found");
          }
        }

        if (type == Type::cardset) {
          string_[Type::cardset_license] = license_text;
        } else {
          string_[Type::iconset_license] = license_text;
        }
      } // read the license
      if (type == Type::cardset) {
        search_cards_back();
      }
      break;

    default:
      break;
    } // switch(type)
  } // if (this == &::preferences)

  signal_changed(type)(value);
  signal_changed_(type, &value);
}


void Preferences::set(Type::CardsOrder const type, CardsOrder const& value)
{
  if (this->value(type) == value) {
    return ;
  }

  // the following code keeps the sorting order when the cards are mixed,
  if (!value.sorted()) {
    cards_order_->sorted_set(value.sorted());
  } else { // if !(!value.sorted())
    *cards_order_ = value;
  } // if !(!value.sorted())
  signal_changed(type)(value);
  signal_changed_(type, &value);
}


void Preferences::set(Type::CardsOrder const type, string const& value)
{
  set(type, CardsOrder(*this, value));
}


auto Preferences::set(string const& type, string const& value) -> bool
{
  for (auto t : Type::bool_list) {
    if (   (type == to_string(t))
        || (type == _(t))) {
      set(t, value);
      return true;
    }
  }
  for (auto t : Type::unsigned_list) {
    if (   (type == to_string(t))
        || (type == _(t))) {
      set(t, value);
      return true;
    }
  }
  for (auto t : Type::string_list) {
    if (   (type == to_string(t))
        || (type == _(t))) {
      set(t, value);
      return true;
    }
  }

  if ((type == to_string(Type::CardsOrder::cards_order))
      || (type == _(Type::cards_order))) {
    set(Type::cards_order, value);
    return true;
  }

  return false;
}


auto Preferences::position(unsigned const playerno) const -> Position
{
  auto rotation = ::preferences(Type::table_rotation);
  if (   ::preferences(Type::own_hand_on_table_bottom)
      && ::party->players().count_humans() == 1
      && ::party->in_game()) {
    auto const& human = *::party->players().human_player();
    auto const& game = ::party->game();
    if (game.players().contains(human))
      rotation = game.players().no(human);
  }

  switch ( (playerno + 4 - rotation) % 4) {
  case 0:
    return Position::south;
  case 1:
    return Position::west;
  case 2:
    return Position::north;
  case 3:
    return Position::east;
  default:
    DEBUG_ASSERTION(false,
                    "Table::position(playerno):\n"
                    "  player number " << playerno
                    << " invalid.");
    break;
  } // switch(playerno)

  return Position::north;
}


void Preferences::load()
{
  for (auto const& file : {
       config_directory() / "FreeDokorc",
       }) {
    if (is_regular_file(file))
      load(file);
  }
}


// NOLINTNEXTLINE(misc-no-recursion)
void Preferences::load(::Path const path)
{
  ifstream istr(path);

  if (istr.fail()) {
    cerr << "Preferences::load(" << path << "):\n"
      << "  Error while opening the file"
      << '\n';
    return ;
  }

  // load the preferences
  while (istr.good()) {
    Config config;
    istr >> config;

    // finished with the config file
    if (config.empty())
      break;

    if (config.separator) {
      // a preferences
      if (!set(config.name, config.value)) {
        cerr << "preferences file:    "
          << "ignoring unknown preferences '" << config.name << "'."
          << endl;
      }
    } else { // if (config.separator)
      // a preferences
      // if the value is in parentencies, remove both
      if (config.name == "!input") {
        // include the given file
        load(path.parent_path() / config.value);
      } else if (config.name == "!end") {
        // ignore the rest of the file
        break;
      } else if (config.name == "!stdout") {
        // output of the data to 'stdout'
        cout << config.value << endl;
      } else if (config.name == "!stderr") {
        // output of the data to 'stderr'
        cerr << config.value << endl;
      } else if (config.name.empty()) {
        cerr << "Preferences file \'" << path << "\':    "
          << "Ignoring line \'" << config.value << "\'.\n";
      } else {
        cerr << "Preferences file \'" << path << "\':    "
          << "Preferences '" << config.name << "' unknown.\n"
          << "Ignoring it.\n";
      } // if (config.name == .)
    } // config.separator

  } // while (istr.good())
}


void Preferences::check_for_outdated_cardset()
{
  // since version 0.7.12 (from 2014)
  // replace Altenburg cardset with InnoCard
  auto const cardset_old = value(Type::cardset);
  bool changed = false;
  if (value(Type::cardset) == "InnoCard/french") {
    set(Type::cardset, "InnoCard/french/185");
    changed = true;
  }
  //if (string(value(Type::cardset), 0, 8) == "InnoCard") {
  //set(Type::cardset, "xskat/french");
  //changed = true;
  //}
  if (value(Type::cardset) == "Altenburg/english") {
    set(Type::cardset, "InnoCard/english/185");
    //set(cardset, "dondorf");
    changed = true;
  }
  if (value(Type::cardset) == "Altenburg/german") {
    set(Type::cardset, "xskat/german");
    changed = true;
  }
  if (string(value(Type::cardset), 0, 10) == "Altenburg") {
    set(Type::cardset, "InnoCard/french/185");
    //set(Type::cardset, "xskat/french");
    changed = true;
  }
  if (Date() > Preferences::innocard_expiration_date) {
    if (value(Type::cardset) == "InnoCard/french/140") {
      set(Type::cardset, "VectorPlayingCards/140");
      changed = true;
    }
    if (value(Type::cardset) == "InnoCard/french/150") {
      set(Type::cardset, "VectorPlayingCards/140");
      changed = true;
    }
    if (value(Type::cardset) == "InnoCard/french/185") {
      set(Type::cardset, "VectorPlayingCards/185");
      changed = true;
    }
    if (value(Type::cardset) == "InnoCard/french/240") {
      set(Type::cardset, "VectorPlayingCards/240");
      changed = true;
    }
    if (value(Type::cardset) == "InnoCard/english/140") {
      set(Type::cardset, "VectorPlayingCards/140");
      changed = true;
    }
    if (value(Type::cardset) == "InnoCard/english/150") {
      set(Type::cardset, "VectorPlayingCards/140");
      changed = true;
    }
    if (value(Type::cardset) == "InnoCard/english/185") {
      set(Type::cardset, "VectorPlayingCards/185");
      changed = true;
    }
    if (value(Type::cardset) == "InnoCard/english/240") {
      set(Type::cardset, "VectorPlayingCards/240");
      changed = true;
    }
  }

  if (changed && (value(Type::cardset) == cardset_old)) {
    set(Type::cardset, "xskat/french");
  }
  if (changed) {
    // gettext: %s = old cardset, %s = replacement
    save();
    ::ui->information(_("cardset replaced: %s, %s", cardset_old, value(Type::cardset)),
                      InformationType::problem, true);
  }
}


auto Preferences::cardset_license_expired(::Path const path) const -> bool
{
  if (path.string().find("Altenburg") != string::npos)
    return true;
  if (Date() > Preferences::innocard_expiration_date) {
    if (path.string().find("InnoCard") != string::npos)
      return true;
  }
  return false;
}


auto Preferences::save() const -> bool
{
  return save(config_directory() / "FreeDokorc");

}


auto Preferences::save(::Path const path) const -> bool
{
  auto path_tmp = path;
  path_tmp += ".tmp";
  ofstream ostr(path_tmp);
  if (!ostr.good()) {
    ::ui->information(_("Error::Preferences::save: Error opening temporary file %s. Aborting saving.", path_tmp.string()), InformationType::problem);
    return false;
  }

  ostr << "#!FreeDoko -s\n"
    << "# FreeDoko preferences file (version " << *::version << ")\n"
    << '\n';
  write(ostr);

  if (!ostr.good()) {
    ::ui->information(_("Error::Preferences::save: Error saving in temporary file %s. Keeping temporary file (for bug tracking).", path_tmp.string()), InformationType::problem);
    return false;
  }
  ostr.close();

  try {
    if (is_regular_file(path))
      remove(path);
    rename(path_tmp, path);
  } catch (std::filesystem::filesystem_error const& error) {
    ::ui->information(_("Error::Preferences::save: Could not rename temporary file %s to requested file %s. Keeping temporary file.", path_tmp.string(), path.string())
                      + "\n"
                      + _("Error message: %s", error.what()),
                      InformationType::problem);
    return false;
  } catch (...) {
    ::ui->information(_("Error::Preferences::save: Could not rename temporary file %s to requested file %s. Keeping temporary file.", path_tmp.string(), path.string()), InformationType::problem);
    return false;
  }

  return true;
}


void Preferences::update_all()
{
  if (this != &::preferences)
    return ;

  for (auto t : Type::bool_list) {
    auto const v = value(t);
    signal_changed_bool_[t](v);
    signal_changed_(t, &v);
  }
  for (auto t : Type::unsigned_list) {
    auto const v = value(t);
    signal_changed_unsigned_[t](v);
    signal_changed_(t, &v);
  }
  for (auto t : Type::string_list) {
    auto const& v = value(t);
    signal_changed_string_[t](v);
    signal_changed_(t, &v);
  }
  {
    auto const& v = value(Type::cards_order);
    signal_changed_cards_order_(v);
    signal_changed_(Type::cards_order, &v);
  }
}


// NOLINTNEXTLINE(misc-no-recursion)
void Preferences::update_path(Type::String const type)
{
  auto const path_bak = string_path_[type];

  switch(type) {
  case Type::cardset: {
    // search the directory
    for (auto const& directory : directories(type)) {
      auto const cardset = (*this)(type);
      if (is_regular_file(directory / cardset / "cards/club_queen.png")) {
        // FreeDoko cardset
        string_path_[type] = directory / cardset;
        break;
      }
      if (is_regular_file(directory / ("cardset-" + cardset) / "12c.gif")) {
        // pysol cardset
        string_path_[type] = directory / ("cardset-" + cardset);
        break;
      }
    }

    update_path(Type::cards_back);
    break;
  } // case Type::cardset:

  case Type::cards_back: {

    vector<string> const backs // backs to search
      = {value(Type::cards_back),
        "penguin.png",
        "back.png",
        "back01.gif",
        "deck1.png"};
    for (auto const& back : backs) {
      for (auto const& directory : directories(type)) {
        auto file = (directory / back);
        if (!is_regular_file(file))
          continue;
        if (back == (*this)(Type::cards_back)) {
          string_path_[type] = file;
          goto end_cards_back;
        } else {
          set(Type::cards_back, back);
          goto end_cards_back;
        }
      }
    }
end_cards_back:
    break;
  } // case Type::cards_back:

  case Type::iconset: {
    // search a directory
    for (auto const& directory : directories(type)) {
      if (is_directory(directory)) {
        string_path_[type] = directory;
        break;
      }
    } // for (dd : data_directiories())
    break;
  } // case Type::iconset:

  case Type::background: {
    // search the directory
    for (auto const& dd : directories(type)) {
      auto const file = dd / value(Type::background);
      // check for the file
      if (is_regular_file(file)) {
        string_path_[type] = file;
        break;
      }
    } // for (dd : data_directiories())

    break;
  } // case background:

#ifdef USE_SOUND_COMMAND
  case Type::play_sound_command:
    string_path_[type] = (*this)(type);
    break ;
#endif
  case Type::browser_command:
    string_path_[type] = (*this)(type);
    break ;
  default:
    break;
  } // switch(type)

  if (string_path_[type].empty())
    string_path_[type] = path_bak;
}


auto Preferences::write(ostream& ostr) const -> ostream&
{
  auto const flags = ostr.flags();
  ostr << std::boolalpha;

  ostr << to_string(Type::name) << " = "
    << value(Type::name) << "\n"

    << to_string(Type::language) << " = "
    << value(Type::language) << "\n"

    << to_string(Type::sound) << " = "
    << value(Type::sound) << "\n"

#ifdef USE_SOUND_COMMAND
    << to_string(Type::play_sound_command) << " = "
    << value(Type::play_sound_command) << "\n"
#endif

    << to_string(Type::browser_command) << " = "
    << value(Type::browser_command) << "\n"

    << to_string(Type::show_bug_report_button_in_game_finished_window) << " = "
    << value(Type::show_bug_report_button_in_game_finished_window) << "\n"

    << to_string(Type::save_bug_reports_on_desktop) << " = "
    << value(Type::save_bug_reports_on_desktop) << "\n"

    << to_string(Type::use_threads) << " = "
    << value(Type::use_threads) << "\n"

    << "\n"
    << "# party preferences\n"
    << to_string(Type::additional_party_settings) << " = "
    << value(Type::additional_party_settings) << "\n"


    << "\n"
    << "# cards distribution\n"
    << Type::max_attempts_sec_for_distribution << " = "
    << value(Type::max_attempts_sec_for_distribution) << '\n'
    << Type::min_trumps_to_distribute << " = "
    << value(Type::min_trumps_to_distribute) << '\n'
    << Type::max_trumps_to_distribute << " = "
    << value(Type::max_trumps_to_distribute) << '\n'
    << Type::min_queens_to_distribute << " = "
    << value(Type::min_queens_to_distribute) << '\n'
    << Type::max_queens_to_distribute << " = "
    << value(Type::max_queens_to_distribute) << '\n'
    << Type::min_club_queens_to_distribute << " = "
    << value(Type::min_club_queens_to_distribute) << '\n'
    << Type::max_club_queens_to_distribute << " = "
    << value(Type::max_club_queens_to_distribute) << '\n'
    << Type::min_heart_tens_to_distribute << " = "
    << value(Type::min_heart_tens_to_distribute) << '\n'
    << Type::max_heart_tens_to_distribute << " = "
    << value(Type::max_heart_tens_to_distribute) << '\n'
    << Type::min_diamond_aces_to_distribute << " = "
    << value(Type::min_diamond_aces_to_distribute) << '\n'
    << Type::max_diamond_aces_to_distribute << " = "
    << value(Type::max_diamond_aces_to_distribute) << '\n'
    << Type::min_color_aces_to_distribute << " = "
    << value(Type::min_color_aces_to_distribute) << '\n'
    << Type::max_color_aces_to_distribute << " = "
    << value(Type::max_color_aces_to_distribute) << '\n'
    << Type::min_blank_colors_to_distribute << " = "
    << value(Type::min_blank_colors_to_distribute) << '\n'
    << Type::max_blank_colors_to_distribute << " = "
    << value(Type::max_blank_colors_to_distribute) << '\n'

    << '\n'


    << "\n"
    << "# information and delay\n"

    << to_string(Type::card_play_delay) << " = "
    << ((value(Type::card_play_delay) == UINT_MAX)
        ? "-1"
        : String::to_string(value(Type::card_play_delay)).c_str())
    << "\n"

    << to_string(Type::automatic_card_suggestion) << " = "
    << value(Type::automatic_card_suggestion) << "\n"

    << to_string(Type::announce_swines_automatically) << " = "
    << value(Type::announce_swines_automatically) << "\n"

    << to_string(Type::shuffle_without_seed) << " = "
    << value(Type::shuffle_without_seed) << "\n"

    << to_string(Type::replayed_game_with_random_distribution_of_players) << " = "
    << value(Type::replayed_game_with_random_distribution_of_players) << "\n"

    << to_string(Type::show_if_valid) << " = "
    << value(Type::show_if_valid) << "\n"

    << to_string(Type::emphasize_valid_cards) << " = "
    << value(Type::emphasize_valid_cards) << "\n"

    << to_string(Type::announce_in_table) << " = "
    << value(Type::announce_in_table) << "\n"

    << to_string(Type::show_full_trick_window) << " = "
    << value(Type::show_full_trick_window) << "\n"

    << to_string(Type::show_full_trick_window_if_special_points) << " = "
    << value(Type::show_full_trick_window_if_special_points) << "\n"

    << to_string(Type::close_full_trick_automatically) << " = "
    << value(Type::close_full_trick_automatically) << "\n"

    << to_string(Type::full_trick_close_delay) << " = "
    << value(Type::full_trick_close_delay) << "\n"


    << to_string(Type::show_gametype_window) << " = "
    << value(Type::show_gametype_window) << "\n"

    << to_string(Type::close_gametype_window_automatically) << " = "
    << value(Type::close_gametype_window_automatically) << "\n"

    << to_string(Type::gametype_window_close_delay) << " = "
    << value(Type::gametype_window_close_delay) << "\n"


    << to_string(Type::show_marriage_window) << " = "
    << value(Type::show_marriage_window) << "\n"

    << to_string(Type::close_marriage_window_automatically) << " = "
    << value(Type::close_marriage_window_automatically) << "\n"

    << to_string(Type::marriage_window_close_delay) << " = "
    << value(Type::marriage_window_close_delay) << "\n"



    << to_string(Type::show_announcement_window) << " = "
    << value(Type::show_announcement_window) << "\n"

    << to_string(Type::close_announcement_window_automatically) << " = "
    << value(Type::close_announcement_window_automatically) << "\n"

    << to_string(Type::announcement_window_close_delay) << " = "
    << value(Type::announcement_window_close_delay) << "\n"


    << to_string(Type::show_swines_window) << " = "
    << value(Type::show_swines_window) << "\n"

    << to_string(Type::close_swines_window_automatically) << " = "
    << value(Type::close_swines_window_automatically) << "\n"

    << to_string(Type::swines_window_close_delay) << " = "
    << value(Type::swines_window_close_delay) << "\n"


    << to_string(Type::show_trickpiles_points) << " = "
    << value(Type::show_trickpiles_points) << "\n"

    << to_string(Type::show_known_teams_in_game) << " = "
    << value(Type::show_known_teams_in_game) << "\n"

    << to_string(Type::show_soloplayer_in_game) << " = "
    << value(Type::show_soloplayer_in_game) << "\n"

    << to_string(Type::show_all_hands) << " = "
    << value(Type::show_all_hands) << "\n"

    << to_string(Type::show_all_tricks) << " = "
    << value(Type::show_all_tricks) << "\n"

    << to_string(Type::show_ai_information_hands) << " = "
    << value(Type::show_ai_information_hands) << "\n"

    << to_string(Type::show_ai_information_teams) << " = "
    << value(Type::show_ai_information_teams) << "\n"

    << "\n";

  // so the sorting is saved, even if the mixed modus is aktivated

  if (!value(Type::cards_order).sorted()) {
    CardsOrder cards_order_tmp(value(Type::cards_order));

    cards_order_tmp.sorted_set(true);

    ostr << to_string(Type::cards_order) << " = "
      << cards_order_tmp << "\n";
  } // if (value(Type::cards_order).mixed())

  ostr << to_string(Type::cards_order) << " = "
    << value(Type::cards_order) << "\n";

  ostr << "# appearance\n"

    << "#   table\n"

    << to_string(Type::own_hand_on_table_bottom) << " = "
    << value(Type::own_hand_on_table_bottom) << "\n"

    << to_string(Type::static_hands) << " = "
    << value(Type::static_hands) << "\n"

    << to_string(Type::table_rotation) << " = "
    << value(Type::table_rotation) << "\n"

    << to_string(Type::rotate_trick_cards) << " = "
    << value(Type::rotate_trick_cards) << "\n"

    << "#   cards\n"

    << to_string(Type::original_cards_size) << " = "
    << value(Type::original_cards_size) << "\n"

    << to_string(Type::cards_height) << " = "
    << value(Type::cards_height) << "\n"

    << "#   theme\n"

    << to_string(Type::cardset) << " = "
    << value(Type::cardset) << "\n"

    << to_string(Type::cards_back) << " = "
    << value(Type::cards_back) << "\n"

    << to_string(Type::iconset) << " = "
    << value(Type::iconset) << "\n"

    << to_string(Type::background) << " = "
    << value(Type::background) << "\n"

    << "#   fonts and colors\n"

    << to_string(Type::name_font) << " = "
    << value(Type::name_font) << "\n"

    << to_string(Type::name_font_color) << " = "
    << value(Type::name_font_color) << "\n"

    << to_string(Type::name_active_font_color) << " = "
    << value(Type::name_active_font_color) << "\n"

    << to_string(Type::name_reservation_font_color) << " = "
    << value(Type::name_reservation_font_color) << "\n"

    << to_string(Type::trickpile_points_font) << " = "
    << value(Type::trickpile_points_font) << "\n"

    << to_string(Type::trickpile_points_font_color) << " = "
    << value(Type::trickpile_points_font_color) << "\n"

    << to_string(Type::poverty_shift_arrow_color) << " = "
    << value(Type::poverty_shift_arrow_color) << "\n"

    << "#   splash screen\n"

    << to_string(Type::show_splash_screen) << " = "
    << value(Type::show_splash_screen) << "\n"

    << to_string(Type::splash_screen_transparent) << " = "
    << value(Type::splash_screen_transparent) << "\n"

    << "# ui\n"

    << to_string(Type::ui_main_window_pos_x) << " = "
    << value(Type::ui_main_window_pos_x) << "\n"

    << to_string(Type::ui_main_window_pos_y) << " = "
    << value(Type::ui_main_window_pos_y) << "\n"

    << to_string(Type::ui_main_window_width) << " = "
    << value(Type::ui_main_window_width) << "\n"

    << to_string(Type::ui_main_window_height) << " = "
    << value(Type::ui_main_window_height) << "\n"

    << to_string(Type::ui_main_window_fullscreen) << " = "
    << value(Type::ui_main_window_fullscreen) << "\n"
    ;

  ostr << "\n";

  ostr.flags(flags);
  return ostr;
}


auto operator<<(ostream& ostr, Preferences const& preferences) -> ostream&
{
  preferences.write(ostr);
  return ostr;
}


auto operator==(Preferences const& lhs, Preferences const& rhs) -> bool
{
  for (auto t : Preferences::Type::bool_list) {
    if (lhs.value(t) != rhs.value(t)) {
      return false;
    }
  }
  for (auto t : Preferences::Type::unsigned_list) {
    if (lhs.value(t) != rhs.value(t)) {
      return false;
    }
  }
  for (auto t : Preferences::Type::string_list) {
    if (lhs.value(t) != rhs.value(t)) {
      return false;
    }
  }
  for (auto t : Preferences::Type::cards_order_list) {
    if (lhs.value(t) != rhs.value(t)) {
      return false;
    }
  }

  return true;
}


auto operator!=(Preferences const& lhs, Preferences const& rhs) -> bool
{
  return !(lhs == rhs);
}

namespace {
auto default_config_directory() -> ::Directory
{
#ifndef WINDOWS
  {
    auto const home = getenv("HOME");
    if (home) {
      return ::Directory(home) / ".FreeDoko";
    }
  }
#endif
#ifdef WINDOWS
  {
    char out[MAX_PATH];
    if (SHGetSpecialFolderPathA(nullptr, out, CSIDL_APPDATA, 0)) {
      return ::Directory(out) / "FreeDoko";
    }
  }
  {
    auto const appdata = getenv("APPDATA");
    if (appdata) {
      return ::Directory(appdata) / "FreeDoko";
    }
  }
#endif
  return {"."};
}
} // namespace
