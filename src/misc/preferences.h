/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../basetypes/position.h"

#include "../utils/date.h"

#include "preferences_type.h"

class Preferences;
extern Preferences preferences; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
extern string argv0;            // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

/**
 ** the class for all preferences
 **/
class Preferences {
public:
  // cards order
  class CardsOrder;

public:
  // the date of the expiration of the Altenburg cardset
  static Date const altenburg_expiration_date;
  // the date of the expiration of the InnoCard cardset
  static Date const innocard_expiration_date;

public:
  using Type = PreferencesType;
  using Path = PreferencesPath;

public:
  Preferences();
  Preferences(Preferences const& preferences);
  auto operator=(Preferences const& preferences) -> Preferences&;

  void init();

  auto signal_changed()                            noexcept -> Signal<void(int, void const*)>&;
  auto signal_changed()                      const noexcept -> Signal<void(int, void const*)> const&;
  auto signal_changed(Type::Bool type)                      -> Signal<void(bool)>&;
  auto signal_changed(Type::Bool type)       const          -> Signal<void(bool)> const&;
  auto signal_changed(Type::Unsigned type)                  -> Signal<void(unsigned)>&;
  auto signal_changed(Type::Unsigned type)   const          -> Signal<void(unsigned)> const&;
  auto signal_changed(Type::String type)                    -> Signal<void(string)>&;
  auto signal_changed(Type::String type)     const          -> Signal<void(string)> const&;
  auto signal_changed(Type::CardsOrder type)                -> Signal<void(CardsOrder const&)>&;
  auto signal_changed(Type::CardsOrder type) const          -> Signal<void(CardsOrder const&)> const&;

  auto data_directories() const -> vector<Directory> const&;
  void set_data_directories();
  void add_data_directory(Directory path);
  // a string containing the data directories
  auto data_directories_string(string indent = "") const -> string;

  void set_config_directory(Directory path);
  auto config_directory() const -> Directory;

  auto directories(Type::String type) const -> vector<Directory>;


  // returns, whether the preferences is activ or not (or the value)
  auto operator()(Type::Bool type)       const -> bool;
  auto operator()(Type::Unsigned type)   const -> unsigned;
  auto operator()(Type::String type)     const -> string;
  auto operator()(Path type)             const -> ::Path;
  auto operator()(Type::CardsOrder type) const -> CardsOrder const&;
  auto operator()(Type::CardsOrder type)       -> CardsOrder&;

  auto type(string const& name)                      const -> int;
  auto type_translated(string const& name)           const -> int;
  auto type_translated_or_normal(string const& name) const -> int;

  // returns, whether the dependencies of the preferencess are met
  auto dependencies(Type::Bool type)     const -> bool;
  auto dependencies(Type::Unsigned type) const -> bool;
  auto dependencies(Type::String type)   const -> bool;

  // returns the preferences
  auto value(Type::Bool type)       const -> bool;
  auto value(Type::Unsigned type)   const -> unsigned;
  auto value(Type::String type)     const -> string const&;
  auto path(Type::String type)      const -> ::Path const&;
  auto value(Path type)             const -> ::Path;
  auto path(Path type)              const -> ::Path;
  auto value(Type::CardsOrder type) const -> CardsOrder const&;
  auto value(Type::CardsOrder type)       -> CardsOrder&;
  auto cardset_format()             const -> CardsetFormat;
  auto cardset_format(::Path path)  const -> CardsetFormat;

  // the minimum and maximum value
  auto min(Type::Unsigned type) const -> unsigned;
  auto max(Type::Unsigned type) const -> unsigned;

  // sets the preferences
  auto set(string const& type, string const& value) -> bool;
  void set(Type::Bool type, bool value);
  void set(Type::Bool type, string const& value);
  void set(Type::String type, string const& value);
  void set(Type::Unsigned type, unsigned value);
  void set(Type::Unsigned type, string const& value);
  void set(Type::CardsOrder type, CardsOrder const& value);
  void set(Type::CardsOrder type, string const& value);

  // returns the position of 'playerno'
  auto position(unsigned playerno) const -> Position;

  void load();
  void load(::Path path);

  auto save()            const -> bool;
  auto save(::Path path) const -> bool;

  // check whether a cardset is outdated and replaces it
  void check_for_outdated_cardset();
  auto cardset_license_expired(::Path path) const -> bool;

  // write of the preferencess
  auto write(ostream& ostr) const -> ostream&;

private:
  void set_to_hardcoded();

  void search_cards_back();
  void search_background();

  void update_all();
  void update_path(Type::String type);

private:
  // the preferencess
  map<Type::Bool,     bool>     bool_;
  map<Type::Unsigned, unsigned> unsigned_;
  map<Type::String,   string>   string_;

  unique_ptr<CardsOrder> cards_order_;

  vector<Directory> data_directories_;
  Directory config_directory_;
  map<Type::String, ::Path> string_path_;

  Signal<void(int, void const*)>              signal_changed_;
  map<Type::Bool,     Signal<void(bool)>>     signal_changed_bool_;
  map<Type::Unsigned, Signal<void(unsigned)>> signal_changed_unsigned_;
  map<Type::String,   Signal<void(string)>>   signal_changed_string_;
  Signal<void(CardsOrder const&)>             signal_changed_cards_order_;
}; // class Preferences

auto operator<<(ostream& ostr, Preferences const& preferences) -> ostream&;

// comparison of two preferencess (value)
auto operator==(Preferences const& preferences1, Preferences const& preferences2) -> bool;
auto operator!=(Preferences const& preferences1, Preferences const& preferences2) -> bool;

//extern string GPL;
//extern string cardset_license;

#include "preferences.cardsorder.h"
