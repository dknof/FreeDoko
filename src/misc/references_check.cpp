/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef REFERENCES_REPLAY

#include "references_check.h"

#include "../basetypes/program_flow_exception.h"
#include "../game/game.h"
#include "../player/player.h"
#include "../card/trick.h"
#include "../os/bug_report_replay.h"

#include "../utils/file.h"

#include <fstream>
#include <cstring>

// the references check
unique_ptr<ReferencesCheck> references_check = {}; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)


ReferencesCheck::ReferencesCheck(Directory const references_dir) :
  report_file_("References.report.csv")
{
  set_main_directory(references_dir);
  next_reference();
  //statistics_.reset();
}


ReferencesCheck::~ReferencesCheck()
{
  write_statistics();
}


void ReferencesCheck::set_main_directory(Directory const references_main_dir)
{
  references_main_dir_ = references_main_dir;
  set_directory(references_main_dir);
  { // create the statistics file
    ofstream ostr(references_main_dir_ / report_file_);
    if (ostr.fail()) {
      report_file_.clear();
      return ;
    } // if (ostr.fail())

    ostr << "file;"
      << "trick;"
      << "player;"
      << "discrepancy;"
      << "real action;"
      << "ref action;"
      << "comment\n";

    cout << "Created file '"
      << references_main_dir_ / report_file_
      << "' for the references report.\n";

    statistics_.reset();
  } // load the next reference and create the statistics file
}


void ReferencesCheck::set_directory(Directory const references_dir)
{
  if (!is_directory(references_dir)) {
    cerr << "ReferencesCheck:set_directory()\n"
      << "'" << references_dir << "' is no directory. Aborting."
      << endl;
    std::exit(EXIT_FAILURE);
  }
  references_dir_ = references_dir;
  if (references_dir_.empty())
    return ;

  for (auto const& subdir : std::filesystem::directory_iterator(references_dir_)) {
    if (!std::filesystem::is_directory(subdir))
      continue;
    if (std::filesystem::is_regular_file(subdir.path() / "_skip"))
      continue;
    references_subdir_.push_back(subdir.path());
  }
}


// NOLINTNEXTLINE(misc-no-recursion)
auto ReferencesCheck::next_reference() -> bool
{
  if (dir_itr == std::filesystem::directory_iterator())
    dir_itr = std::filesystem::directory_iterator(references_dir_);
  else
    ++dir_itr;
  for (;
       dir_itr != std::filesystem::directory_iterator();
       ++dir_itr) {
    try {
      if (dir_itr->path().filename() == report_file_) {
        continue;
      }
      if (!std::filesystem::is_directory(*dir_itr)
          && (dir_itr->path().filename().string().size()
              >= strlen(".Reference.FreeDoko"))
          && (dir_itr->path().filename().string().substr(dir_itr->path().filename().string().size()
                                                         - strlen(".Reference.FreeDoko"))
              == ".Reference.FreeDoko")
         ) {
        if (load_reference()) {
          if  (   ::party->status() != Party::Status::play
               && ::party->status() != Party::Status::quit) {
            return true;
          } else {
            if (::party->gameno() > 0)
              throw Party::Status::loaded; // NOLINT cert-err09-cpp
            else
              throw Party::Status::init; // NOLINT cert-err09-cpp
          }
        }
        return false;
      }
    } catch (std::exception const& ex) {
      cerr << references_dir_ << "/" << dir_itr->path().filename() << " " << ex.what() << endl;
      references_dir_ = "";
    }
  } // for (dir_itr)

  { // go to the next subdir

    if (references_subdir_.empty()) {
      throw ProgramFlowException::Quit();
    }
    auto const subdir = references_subdir_.front();
    references_subdir_.erase(references_subdir_.begin());
    set_directory(subdir);
    next_reference();
    return true;
  } // go to the next subdir

  return false;
} // bool ReferencesCheck::next_reference()


auto ReferencesCheck::load_reference() -> bool
{
  if (dir_itr == std::filesystem::directory_iterator())
    return false;

  ::bug_report_replay
    = make_unique<OS_NS::BugReportReplay>(dir_itr->path().string(),
                                          OS_NS::BugReportReplay::verbose_none);
  // ToDo: check for auto action end
  if (!bug_report_replay->loaded()) {
    cerr << "Error loading the reference '" << dir_itr->path().string()
      << "'" << endl;
    if (!report_file_.empty()) {
      ofstream ostr(references_main_dir_ / report_file_, std::ios::app);
      if (!ostr.fail())
        ostr << dir_itr->path() << ";loading error\n";
    } // if (output set)
    ::bug_report_replay = {};
    return false;
  }

  statistics_.no_files += 1;

  return true;
} // bool ReferencesCheck::load_reference()


void ReferencesCheck::check(Game const& game,
                            GameplayAction const& game_action,
                            GameplayAction const& check_action,
                            string const& comment,
                            GameplayActions::Discrepancy const& discrepancy,
                            bool const last_check)
{
  statistics_.no_checks += 1;
  if (!discrepancy) {
    statistics_.no_success += 1;
  }

  string output;
  { // create the result line
    // format:
    //   seed (trick, player); OK; ; real action; text
    //   seed (trick, player); discrepancy; real action; check action; text
    // semicolon separated list:
    // 1) seed
    // 2) trick
    // 3) player
    // 4) discrepancy
    // 5) real gameplay action / empty
    // 6) gameplay action from the reference
    // 7) comment (optional)
    ostringstream ostr;
    ostr
      << string(references_dir_.string() + "/",
                references_main_dir_.string().size() + 1)
      << std::setfill('0') << setw(6) << dir_itr->path().filename().string()
      << ';' << setw(2) << game.tricks().current_no() << ';'
      << player(check_action) << ';';
    if (!discrepancy)
      ostr << "OK";
    else
      ostr << discrepancy;
    ostr << ';' << to_string(game_action)
      << ';' << to_string(check_action);
    if (!comment.empty())
      ostr << ';' << comment;
    output = ostr.str();
  } // create the result line

  // print the result line
  cout << "check: " << output << endl;

  { // save the result line
    if (   references_dir_.empty()
        && !report_file_.empty())
      //set_directory(".");
      next_reference();
    if (!report_file_.empty()) {
      ofstream ostr(references_main_dir_ / report_file_, std::ios::app);
      if (ostr.fail()) {
        report_file_.clear();
        return ;
      } // if (ostr.fail())
      ostr << output << '\n';
    } // if (output set)
  } // save the result line

  if (discrepancy || last_check) {
    throw Party::Status::init; // NOLINT cert-err09-cpp
  }
}


void ReferencesCheck::write_statistics() const
{
  // print the statistics
  cout << '\n'
    << "reference checks\n"
    << "  files:   "
    << setw(static_cast<int>(ceil(log10(statistics_.no_checks))))
    << statistics_.no_files << '\n'
    << "  checks:  "
    << statistics_.no_checks << '\n'
    << "  success: "
    << setw(static_cast<int>(ceil(log10(statistics_.no_checks))))
    << statistics_.no_success << '\n'
    << "  failed:  "
    << setw(static_cast<int>(ceil(log10(statistics_.no_checks))))
    << (statistics_.no_checks
        - statistics_.no_success) << '\n'
    << '\n';

  { // save the result line
    if (references_dir_.empty()
        && !report_file_.empty()) {
      return ;
    }
    if (!report_file_.empty()) {
      ofstream ostr(references_main_dir_ / report_file_, std::ios::app);
      if (ostr.fail()) {
        return ;
      } // if (ostr.fail())
      ostr << '\n'
        << "files;"
        << statistics_.no_files << '\n'
        << "checks;"
        << statistics_.no_checks << '\n'
        << "success;"
        << statistics_.no_success << '\n'
        << "failed;"
        << (statistics_.no_checks
            - statistics_.no_success) << '\n';
    } // if (output set)
  } // save the result line
}
#endif
