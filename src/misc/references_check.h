/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#ifdef REFERENCES_REPLAY

#include "../game/gameplay_actions/check.h"
class Game;

/**
 ** a check for reference files over a directory
 **
 ** @todo       add statistics
 ** @todo       add support for subdirectories
 **/
class ReferencesCheck {
public:
  // the statistics for the reference checks
  struct Statistics {
    Statistics() = default;
    ~Statistics() = default;
    void reset()
    { this->no_files = this->no_checks = this->no_success = 0; }

    // number of files checked
    int no_files = 0;
    // number of checks made
    int no_checks = 0;
    // number of successfully checks
    int no_success = 0;
  }; // class Statistics

public:
  ReferencesCheck() = default;
  ReferencesCheck(Directory references_dir);
  ReferencesCheck(ReferencesCheck const&) = delete;
  auto operator=(ReferencesCheck const&) = delete;
  ~ReferencesCheck();

  // set the main directory
  void set_main_directory(Directory references_main_dir);
  // set the directory
  void set_directory(Directory references_dir);

  // a check has been made
  void check(Game const& game,
             GameplayAction const& game_action,
             GameplayAction const& check_action,
             string const& comment,
             GameplayActions::Discrepancy const& discrepancy,
             bool last_check);

  auto next_reference() -> bool;
  auto load_reference() -> bool;

  // write the statistics
  void write_statistics() const;


private:
  Directory references_main_dir_;
  Directory references_dir_;
  vector<Directory> references_subdir_;
  string report_file_ = "References.report.csv";
  std::filesystem::directory_iterator dir_itr;
  Statistics statistics_;
}; // class ReferencesCheck

// the references check
extern unique_ptr<ReferencesCheck> references_check; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

#endif
