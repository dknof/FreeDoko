/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "../utils/file.h"
#include "preferences.h"

#include <cstdlib>

#ifndef LOCALE_DIRECTORY
#define LOCALE_DIRECTORY "../po" // NOLINT(cppcoreguidelines-macro-usage)
#endif


// NOLINTNEXTLINE(misc-no-recursion)
void translation_init()
{
  bind_textdomain_codeset("FreeDoko", "utf-8");
  auto locale = setlocale(LC_ALL, ""); // NOLINT
  if (!locale) {
    cerr << "Locale not found, setting it to 'de_DE.utf8'.\n";
#ifdef WINDOWS
    SetEnvironmentVariableA("LANG", "de_DE.utf8");
    putenv("LANG=de_DE.utf8");
    SetEnvironmentVariableA("LANGUAGE", "de");
    putenv("LANGUAGE=de");
#else
    setenv("LANG", "de_DE.utf8", true);
    setenv("LANGUAGE", "de", true);
#endif
    locale = setlocale(LC_ALL, "");
    if (!locale) {
      cerr << "Could not set locale to 'de_DE.utf8'\n";
    }
  }

  auto const domainname = textdomain("FreeDoko"); // NOLINT
  if (domainname == nullptr)
    return ;

  vector<Path> paths;
  if (getenv("FREEDOKO_LOCALE_DIRECTORY"))
    paths.emplace_back(getenv("FREEDOKO_LOCALE_DIRECTORY"));
  paths.push_back(File::absolute_path_from_executable_directory(LOCALE_DIRECTORY));
  for (auto const& p : ::preferences.data_directories()) {
    paths.emplace_back(p / "po");
  }
  for (auto const& path : paths) {
    if (is_directory(path)) {
      bindtextdomain(domainname, path.string().c_str());
      break;
    }
  }
  if (getenv("LANGUAGE") == nullptr) {
    cerr << "Environment variable LANGUAGE not set.\n"
      << "Setting environment variable LANGUAGE to 'de'.\n";
#ifdef WINDOWS
    SetEnvironmentVariableA("LANGUAGE", "de");
    putenv("LANGUAGE=de");
#else
    setenv("LANGUAGE", "de", true);
#endif
    translation_init();
  } else if (   _("").empty()
             && getenv("LANGUAGE") != "de"s) {
    cerr << "Could not load the language.\n"
      << "Setting environment variable LANGUAGE from '" << getenv("LANGUAGE") << "' to 'de'.\n";
#ifdef WINDOWS
    SetEnvironmentVariableA("LANGUAGE", "de");
    putenv("LANGUAGE=de");
#else
    setenv("LANGUAGE", "de", true);
#endif
    translation_init();
  }
}


auto get_language() -> string
{
#ifdef WINDOWS
  string locale = Reg_read(HKEY_CURRENT_USER,
                           "Software\\FreeDoko",
                           "Language");
  if (locale.empty())
    locale = Reg_read(HKEY_LOCAL_MACHINE,
                      "Software\\FreeDoko",
                      "Language");

  if (locale.empty()) {
    // Ein bisschen kompliziert, aus der Registry die Sprache auszulesen:
    // zuerst wird der Laenderkode gelesen (Deutsch: 407),
    // anschliessend wird geschaut, welche Sprache zu dem Laendercode
    // gehört.
    string const language_code
      = Reg_read(HKEY_CURRENT_USER,
                 "Control Panel\\International",
                 "Locale");

    if (!language_code.empty()) {
      if (string(language_code, language_code.size() - 1)
          == "7") { // deutsch
        locale = "de";
      } else if (string(language_code, language_code.size() - 1)
                 == "9") { // english
        locale = "en";
      } else if (string(language_code, language_code.size() - 1)
                 == "A") { // spanish
        locale = "en";
      } else if (string(language_code, language_code.size() - 1)
                 == "C") { // francais
        locale = "en";
      } else { // unknown language code
        locale = "en";
      } // unknown langugage code
    } // if (!language_code.empty())

  } // if (locale.empty())

  if (locale.empty()) {
    // *** DK
    // I didn't find the language in the registry, so I use this workaround
    if ((Reg_read(HKEY_LOCAL_MACHINE,
                  "Software\\Microsoft\\Windows\\CurrentVersion",
                  "ProgramFilesDir") == string("C:\\Programme"))
        && (Reg_read(HKEY_LOCAL_MACHINE,
                     "Software\\Microsoft\\Windows\\CurrentVersion",
                     "ProgramFilesPath") == string("C:\\Programme")
           )
       )
      locale = "de";
  } // if (locale.empty())
#else
  // non Windows
  string locale = setlocale(LC_MESSAGES, nullptr);
#endif
  return locale;
}


void set_language(string const language)
{
#ifdef WINDOWS
  SetEnvironmentVariableA("LANGUAGE", language.c_str());
  putenv(("LANGUAGE=" + language).c_str());
  SetEnvironmentVariableA("LANG", language.c_str());
  putenv(("LANG=" + language).c_str());
#endif
#ifndef WINDOWS
  setenv("LANGUAGE", language.c_str(), 1);
  setenv("LANG", language.c_str(), 1);
#endif
}


// NOLINTNEXTLINE(misc-no-recursion)
auto gettext(string const text) -> string
{
  if (::debug("gettext")) {
    auto const translation = gettext(text.c_str()); // NOLINT
    if (translation == text) {
      ::debug.ostr("gettext") << _("Translation missing: %s", text) << '\n';
    }
    return translation;
  } else {
    return gettext(text.c_str());
  }
}


// NOLINTNEXTLINE(misc-no-recursion)
auto _(string const& text) -> string
{
  return gettext(text);
}


// NOLINTNEXTLINE(misc-no-recursion)
auto replace_substrings(string const& text) -> string
{
  auto const pos = text.find('%');
  if (pos == text.npos)
    return text;
  if (pos + 1 == text.npos)
    return text;
  if (text[pos + 1] == '%')
    return (  string(text, 0, pos)
            + replace_substrings(string(text, pos + 2, text.npos)));
  return text;
}


auto replace_substring(string const& text, string const& param_string) -> string
{
  return replace_substring(text, param_string, 's', 't');
}


auto replace_substring(string const& text, int const param_int) -> string
{
  return replace_substring(text, std::to_string(param_int), 'i', 'd');
}


auto replace_substring(string const& text, double const param_double) -> string
{
  auto param_string = std::to_string(param_double);
  if (   param_string.find('.') != string::npos    // englische Schreibweise
      || param_string.find(',') != string::npos) { // deutsche Schreibweise
    while (param_string.back() == '0')
      param_string.pop_back();
    if (   param_string.back() == '.'
        || param_string.back() == ',')
      param_string.pop_back();
  }
  return replace_substring(text, param_string, 'f');
}


auto replace_substring(string const& text, uint32_t const param_unsigned) -> string
{
  return replace_substring(text, std::to_string(param_unsigned), 'u');
}


#ifdef OSX
auto replace_substring(string const& text, unsigned long param_unsigned) -> string
#else
auto replace_substring(string const& text, uint64_t const param_unsigned) -> string
#endif
{
  return replace_substring(text, std::to_string(param_unsigned), 'u');
}


auto replace_substring(string const& text, string const& replacement,
                       char c1, // character for identifying the part to replace
                       char c2) // optional second character for identifying the part to replace
  -> string
{
  // start of a '%'-substring
  auto const pos = text.find('%');
  // Bei Release oder Websocket keinen Absturz durch fehlerhafte Übersetzungsdateien
  if (!(   (pos != text.npos)
        && (pos + 1 != text.npos))) {
    cerr << "WARNING(file " << __FILE__ << ", line " << __LINE__ << "): "
      << "replace_substring(text, replacement, " << c1 << ", " << c2 << ")\n"
      "  Found no %, but should replace a string '" << replacement << "'. Text is: "
      << text << endl;
#if defined(RELEASE) || defined(WEBSOCKETS_SERVICE)
    return text;
#endif
    std::abort();
  }
  if (!(   (text[pos + 1]) == c1
        || (text[pos + 1]) == c2)) {
    cerr << "WARNING(file " << __FILE__ << ", line " << __LINE__ << "): "
      "replace_substring(text, replacement, " << c1 << ", " << c2 << ")\n"
      "  wrong type at position " << pos + 1 << ":\n"
      "  The text says '" << text[pos + 1] << "' but the code says '" << c1 << "'"
      << (c2 ? (" or '" + string(1, c2) + "'") : ""s)
      << ".\n"
      "  Text: " << text;
#if defined(RELEASE) || defined(WEBSOCKETS_SERVICE)
    return text;
#endif
    std::abort();
  }

  return (  replace_substrings(string(text, 0, pos))
          + replacement
          + replace_substrings(string(text, pos + 2, text.npos)));
}
