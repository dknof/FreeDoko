/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "options.h"

#include <unistd.h>
#include <cstring>

#include "class/getopt/getopt.h"
#include "utils/file.h"

#include "lib.h"
#include "versions.h"

#include "basetypes/fast_play.h"
#include "basetypes/information.h"
#include "basetypes/program_flow_exception.h"
#include "misc/bug_report.h"
#include "misc/preferences.h"
#include "misc/references_check.h"
#include "os/bug_report_replay.h"
#include "os/auto_bug_report.h"
#include "os/gameplay.h"
#include "party/party.h"
#include "party/rule.h"
#include "game/game.h"
#include "game/game_summary.h"

#include "ui/ui.dummy.h"
#if defined(USE_UI_GTKMM)
#include "ui/gtkmm/console_output.h"
#endif

namespace {
void parse_option(GetOpt::Option option);
#ifdef REFERENCES_REPLAY
void start_references(Directory path);
#endif
void load_party_or_bug_report(Path path);
#ifdef BUG_REPORT_REPLAY
auto is_path_for_bug_report(Path path) -> bool;
void load_bug_report(Path path);
#endif
void load_party(Path path);
void load_default_party();
void set_ui(unique_ptr<UI> selected_ui);
#ifdef BUG_REPORT_REPLAY
auto ends_with(string text, string x) -> bool;
#endif

// the ui selected by command line option
unique_ptr<UI> selected_ui = {}; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
#ifdef BUG_REPORT
// shall last till end of program
auto auto_bug_report = make_unique<OS_NS::AutoBugReport>(); // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
#endif

const vector<GetOpt::Syntax> option_syntax = {
  {"help",             '?',  GetOpt::Syntax::BOOL},
  {"hilfe",            'h',  GetOpt::Syntax::BOOL},
  {"version",          'v',  GetOpt::Syntax::BOOL},
  {"license",          'L',  GetOpt::Syntax::BOOL},
  {"defines",          '\0', GetOpt::Syntax::BOOL},
  {"directories",      '\0', GetOpt::Syntax::BOOL},
  {"po",               '\0', GetOpt::Syntax::BOOL},
  {"debug",           '\0',  GetOpt::Syntax::BSTRING},
  {"ui",               'u',  GetOpt::Syntax::BSTRING},
  {"settings",         '\0', GetOpt::Syntax::BSTRING},
  {"name",             'n',  GetOpt::Syntax::BSTRING},
  {"language",         'l',  GetOpt::Syntax::BSTRING},
  {"cardset",          'C',  GetOpt::Syntax::BSTRING},
  {"cards-height",     'H',  GetOpt::Syntax::UNSIGNED},
  {"width",           '\0',  GetOpt::Syntax::UNSIGNED},
  {"height",          '\0',  GetOpt::Syntax::UNSIGNED},
  {"fullscreen",       'f',  GetOpt::Syntax::BOOL},
  {"seed",             's',  GetOpt::Syntax::UNSIGNED},
  {"bug-report",       'b',  GetOpt::Syntax::BSTRING},
  {"references",       'r',  GetOpt::Syntax::BSTRING},
  {"auto-bug-report", '\0', GetOpt::Syntax::BSTRING},
  {"auto-bug-reports", '\0', GetOpt::Syntax::BOOL},
  {"config-directory", '\0', GetOpt::Syntax::BSTRING},
  {"no-automatic-savings", '\0', GetOpt::Syntax::BOOL},
  {"fast-play",        'F',  GetOpt::Syntax::INT},
  {"seed-info",         '\0',  GetOpt::Syntax::BOOL},
#ifdef USE_THREADS
  {"threads",          'T',  GetOpt::Syntax::BOOL},
#endif
};
} // namespace

/** do the initializations before the options are parsed
 **/
void init_before_option_parsing()
{
  // initialisation for random functions
  ::srand(static_cast<unsigned>(::time(nullptr) % RAND_MAX));

  ::preferences.init();
  ::translation_init();
  ::party = make_unique<Party>();
  ::ui = make_unique<UI_Dummy>();

#ifdef BUG_REPORT
  ::debug_function = &BugReport::create_assertion_bug_report;
#endif
}


void load_options_file()
{
  auto istr = std::ifstream(File::executable_directory / "options");
  GetOpt::Option option;

  while (   istr.good()
         && (option = GetOpt::getopt(istr, option_syntax))) {
    if (option)
      parse_option(option);
  }
}


void parse_options(int argc, char* argv[])
{
  GetOpt::Option option;
  while ((option = GetOpt::getopt(argc, argv, option_syntax))) {
    parse_option(option);
  }

  if (party->status() != Party::Status::initial_loaded)
    load_default_party();

  set_ui(move(selected_ui));
}

auto usage_string(string const program, GetOpt::Option const& option) -> string
{
  return (program + "\n"
          + "wrong usage: "
          + to_string(option.error()) + " " + option.value_string() + "\n"
          + "For the syntax type '" + program + " -?'\n");
}


auto help_string() -> string const&
{
#include "text/help.txt.h"

  return help_txt;
}


auto version_string() -> string
{
  auto const git_commit =
#include "git_commit.txt"
    ;
  auto const compile_date =
#include "compile_date.txt"
    ;
  // output of the version
  return ("FreeDoko "s + to_string(*::version) + "\n"
          + (git_commit ? "git commit: "s + git_commit + "\n" : ""s)
          + "compiled: " + compile_date + "\n"
#ifdef WINDOWS
          + "system: Windows\n"
#endif
#ifdef LINUX
          + "system: Linux\n"
#endif
#ifdef HPUX
          + "system: HPUX\n"
#endif
          + "compiler: "
#ifdef __GNUG__
          + "g++"
#endif
          + '\n'
#ifdef __VERSION__
          + "  version: " + __VERSION__ + "\n"
#endif
#ifdef __cplusplus
          + "  C++ version: " + std::to_string(__cplusplus) + "\n"
#endif
          );
}

/** @return   the license in string form
 **/
string license_string()
{
  // output of the license (GPL)
  return ("FreeDoko -- License:\n\n"s
          + gpl_txt
          + '\n'
          + '\n'
          + "----------\n"
          + '\n'
          + "Cardset '" + ::preferences(Preferences::Type::cardset) + "' license:\n"
          + ::preferences(Preferences::Type::cardset_license)
          + '\n');
}


auto defines_string() -> string
{
  return (""s
#ifdef VERSION_DESCRIPTION
          + "VERSION_DESCRIPTION = " + VERSION_DESCRIPTION + '\n'
#endif
#ifdef RELEASE
          + "RELEASE\n"
#endif
#ifdef ASSERTION_GENERATES_SEGFAULT
          + "ASSERTION_GENERATES_SEGFAULT\n"
#endif
#ifdef DKNOF
          + "DKNOF\n"
#endif
          + '\n'
          + "directories:\n"
#ifdef PUBLIC_DATA_DIRECTORY_VALUE
          + "  PUBLIC_DATA_DIRECTORY_VALUE: " + PUBLIC_DATA_DIRECTORY_VALUE + '\n'
#endif
#ifdef MANUAL_DIRECTORY_VALUE
          + "  MANUAL_DIRECTORY_VALUE: " + MANUAL_DIRECTORY_VALUE + '\n'
#endif
#ifdef LOCLALE_DIRECTORY
          + "  LOCALE_DIRECTORY: " + LOCALE_DIRECTORY + '\n'
#endif
          + '\n'
          + "modules:\n"
#ifdef REST_SERVICE
          + "  REST_SERVICE\n"
#endif
#ifdef WEBSOCKETS_SERVICE
          + "  WEBSOCKETS_SERVICE\n"
#endif
#ifdef BUG_REPORT
          + "  BUG_REPORT\n"
#endif
#ifdef BUG_REPORT_REPLAY
          + "  BUG_REPORT_REPLAY\n"
#endif
#ifdef REFERENCES_REPLAY
          + "  REFERENCES_REPLAY\n"
#endif
#ifdef USE_UI_GTKMM
          + "  USE_UI_GTKMM\n"
#endif
#ifdef USE_SOUND
          + "  USE_SOUND\n"
#ifdef USE_SOUND_ALUT
          + "    USE_SOUND_ALUT\n"
#endif
#ifdef USE_SOUND_COMMAND
          + "    USE_SOUND_COMMAND\n"
#endif
#ifdef USE_SOUND_PLAYSOUND
          + "    USE_SOUND_PLAYSOUND\n"
#endif
#endif
#ifdef USE_THREADS
          + "  USE_THREADS\n"
#endif
#ifdef NO_UPDATE_ON_DEMAND
          + "  NO_UPDATE_ON_DEMAND\n"
#endif
          + '\n'
          + "locale: " + setlocale(LC_MESSAGES, nullptr) + '\n'
          + "textdomain: " + textdomain(nullptr) + '\n'
#ifndef WINDOWS
          + "base directory: " + bindtextdomain(textdomain(nullptr), nullptr) + '\n'
#endif
          + "language: " + ::preferences(Preferences::Type::language) + '\n'
          );
}


auto directories_string() -> string
{
  string directories;
  { // current directory
    char current_dir[PATH_MAX + 1];
    current_dir[PATH_MAX] = '\0';
    if (getcwd(current_dir, PATH_MAX) != nullptr)
      directories +=  ("current directory\n"s
                       + "  " + current_dir + "\n");
  } // current directory
  { // data directory
    directories += "data directories\n";
    for (auto const& directory : ::preferences.data_directories())
      directories += "  " + directory.string() + "\n";
  } // data directory

  return directories;
}


void add_debug(string const keyword)
{
  if (keyword.empty()) {
    return ;
  }
  if (keyword == "help") {
    cout << "Debug-Ausgaben:\n"
      << "Schalter\n"
      << "  color:    Farbausgabe (für gameplay)\n"
      << "  codeline: Code-Zeile mit ausgeben    (Voreinstellung: an)\n"
      << "  time:     Uhrzeit mit ausgeben       (Voreinstellung: aus)\n"
      << "  clock:    Prozessorzeit mit ausgeben (Voreinstellung: aus)\n"
      << "Allgemein\n"
      << "  gameplay: Spielverlauf\n"
      << "  throw:    Werfen von Exceptions (Karteninformationen)\n"
      << "GUI\n"
      << "  mouse:    Maus-Koordinaten\n"
      << "  init ui:  Einzelne Schritte der UI-Initialisierung\n"
      << "  screenshots: Screenshots nach jeder ausgespielten Karte erstellen\n"
      << "KI-Logik\n"
      << "  solo: Entscheidungslogik für die Solospiele\n"
      << "  heuristics: Entscheidungslogik für das Kartenausspiel\n"
      << "  announcement: Entscheidungslogik für An- und Absagen\n"
      << "  Einzelne Heuristiken, Ansagen können über deren Namen angegeben werden\n"
      << "  Wenn ein Spielername / Spielernummer ([0-3]) vorgestellt wird, wird nur für diesen Spieler die Logik ausgegeben.\n"
      ;
    std::exit(EXIT_SUCCESS);
  }
  if (   (keyword == "gameplay")
      || (keyword == "+gameplay") ) {
    // shall last till end of program
    static auto gameplay = make_unique<OS_NS::Gameplay>();
    (void)gameplay;
  }
  if (keyword[0] == '-') {
    ::debug.remove(string(keyword, 1));
    return ;
  }
  if (keyword[0] == '+') {
    ::debug.add(string(keyword, 1));
    return ;
  }
  ::debug.add(keyword);
}


namespace {

void parse_option(GetOpt::Option option) {

  if (option.fail()) {
    cout << usage_string(argv0, option);
#if defined(USE_UI_GTKMM)
    // Zeige ein separates Fenster an, da die Windows-Konsole ansonsten die Ausgabe verschluckt
    UI_GTKMM_NS::show_console_output("usage", usage_string(argv0, option));
    (void)_("Window::usage");
#endif
    std::exit(EXIT_FAILURE);
  } // if (option.fail())

  if (   (option.name() == "help")
      || (option.name() == "hilfe")) {
    cout << help_string();
#if defined(WINDOWS) && defined(RELEASE) && defined(USE_UI_GTKMM)
    // Zeige ein separates Fenster an, da die Windows-Konsole ansonsten die Ausgabe verschluckt
    UI_GTKMM_NS::show_console_output("help", help_string());
#endif
    std::exit(EXIT_SUCCESS);
  } else if (option.name() == "version") {
    cout << version_string();
#if defined(WINDOWS) && defined(RELEASE) && defined(USE_UI_GTKMM)
    // Zeige ein separates Fenster an, da die Windows-Konsole ansonsten die Ausgabe verschluckt
    UI_GTKMM_NS::show_console_output("version", version_string());
#endif
    std::exit(EXIT_SUCCESS);
  } else if (option.name() == "license") {
    cout << license_string();
#if defined(WINDOWS) && defined(RELEASE) && defined(USE_UI_GTKMM)
    // Zeige ein separates Fenster an, da die Windows-Konsole ansonsten die Ausgabe verschluckt
    UI_GTKMM_NS::show_console_output("license", license_string());
#endif
    std::exit(EXIT_SUCCESS);
  } else if (option.name() == "defines") {
    cout << defines_string();
    std::exit(EXIT_SUCCESS);
  } else if (option.name() == "directories") {
    cout << directories_string();
    std::exit(EXIT_SUCCESS);
  } else if (option.name() == "po") {
    cout << _("");
    std::exit(EXIT_SUCCESS);
  } else if (option.name() == "debug") {
    add_debug(option.value_string());
  } else if (option.name() == "ui") {
    selected_ui = {};
    if (option.value_string() == "none") {
      selected_ui = UI::create(UIType::dummy);
#ifdef USE_UI_GTKMM
    } else if (option.value_string() == "gtkmm") {
      selected_ui = UI::create(UIType::gtkmm);
#endif
    } else {
      cerr << "ui '" << option.value_string() << "' not implemented.\n"
        << "exiting";
      std::exit(EXIT_FAILURE);
    } // if (option.value_string() == "...")

  } else if (option.name() == "settings") {
    ::preferences.load(option.value_string());

  } else if (option.name() == "name") {
    ::preferences.set(Preferences::Type::name, option.value_string());

  } else if (option.name() == "language") {
    ::preferences.set(Preferences::Type::language, option.value_string());

  } else if (option.name() == "cards-height") {
    ::preferences.set(Preferences::Type::cards_height,
                      option.value(GetOpt::Option::UNSIGNED));
  } else if (option.name() == "width") {
    ::preferences.set(Preferences::Type::ui_main_window_width,
                      option.value(GetOpt::Option::UNSIGNED));
  } else if (option.name() == "height") {
    ::preferences.set(Preferences::Type::ui_main_window_height,
                      option.value(GetOpt::Option::UNSIGNED));
  } else if (option.name() == "fullscreen") {
    ::preferences.set(Preferences::Type::ui_main_window_fullscreen,
                      option.value(GetOpt::Option::BOOL));

  } else if (option.name() == "cardset") {
    ::preferences.set(Preferences::Type::cardset, option.value_string());

  } else if (option.name() == "seed") {
    party->set_seed(option.value(GetOpt::Option::UNSIGNED));

#ifdef BUG_REPORT_REPLAY
  } else if (option.name() == "bug-report") {
    load_bug_report(option.value_string());
#endif
#ifdef REFERENCES_REPLAY
  } else if (option.name() == "references") {
    start_references(option.value_string());
#endif
#ifdef BUG_REPORT
  } else if (option.name() == "auto-bug-report") {
    auto_bug_report->add_condition(option.value_string());
  } else if (option.name() == "auto-bug-reports") {
    //auto_bug_report->add_condition("game");
    //auto_bug_report->add_condition("trick");
    //auto_bug_report->add_condition("card");
    //auto_bug_report->add_condition("no heuristic");
    auto_bug_report->add_condition("dulle lost");
    auto_bug_report->add_condition("fox lost");
    auto_bug_report->add_condition("swine lost");
    auto_bug_report->add_condition("poverty lost");
    auto_bug_report->add_condition("solo lost");
    auto_bug_report->add_condition("bad announcement");
    auto_bug_report->add_condition("no announcement");
#endif
  } else if (option.name() == "config-directory") {
    ::preferences.set_config_directory(option.value_string());
  } else if (option.name() == "no-automatic-savings") {
    ::preferences.set(Preferences::Type::automatic_savings,
                      !option.value(GetOpt::Option::BOOL));
  } else if (option.name() == "fast-play") {
    ::fast_play |= static_cast<FastPlayBit>(option.value(GetOpt::Option::INT));
  } else if (option.name() == "seed-info") {
    if (option.value(GetOpt::Option::BOOL))
      ::fast_play |= FastPlay::seed_info;
    else
      ::fast_play.remove(FastPlay::seed_info);
#ifdef USE_THREADS
  } else if (option.name() == "threads") {
    ::preferences.set(Preferences::Type::use_threads,
                      option.value(GetOpt::Option::BOOL));
#endif // #ifdef USE_THREADS

  } else if (option.name().empty()) {
    load_party_or_bug_report(option.value_string());
  } else {
    cerr << "Option '" << option.name() << "' unknown!\n";
    std::exit(EXIT_FAILURE);
  } // if (option.name() == ...)
}


#ifdef REFERENCES_REPLAY
/** start the references
 **/
void start_references(Directory const path)
{
  ::references_check = make_unique<ReferencesCheck>(path);
  ::party->set_status(Party::Status::initial_loaded);
  ::fast_play |= FastPlay::party_start;
  ::fast_play |= FastPlay::pause;
  ::fast_play |= FastPlay::quit_when_finished;
}
#endif


/** load a party or a bug report
 **/
void load_party_or_bug_report(Path path_)
{
  Path const path = (path_.string().substr(0, 7) == "file://" ? Path(path_.string().substr(7)) : path_);
  if (path.string() == "-") {
    // this ensures, that the default party is not loaded
    ::party->set_status(Party::Status::initial_loaded);
    return ;
  }
  if (!is_regular_file(path))
    return ;

#ifdef BUG_REPORT_REPLAY
  if (is_path_for_bug_report(path)) {
    load_bug_report(path);
    if (::bug_report_replay)
      return ;
  }
#endif
  load_party(path);
}


#ifdef BUG_REPORT_REPLAY
/** @return   whether the path is for a bug report
 **/
auto is_path_for_bug_report(Path const path) -> bool
{
  if (!is_regular_file(path))
    return false;
  return (   ends_with(path.filename().string(), ".BugReport.FreeDoko")
          || ends_with(path.filename().string(), ".Fehlerbericht.FreeDoko")
          || ends_with(path.filename().string(), ".Reference.FreeDoko"));
}

/** load a bug report
 **/
void load_bug_report(Path const path)
{
  ::bug_report_replay
    = make_unique<OS_NS::BugReportReplay>(path,
                                          OS_NS::BugReportReplay::verbose_all);
  if (!bug_report_replay->loaded()) {
    cerr << "Error loading the bug report '" << path << "'\n";
    bug_report_replay = {};
    return ;
  }
  ::party->set_status(Party::Status::initial_loaded);
}
#endif


/** load the party
 **/
void load_party(Path const path)
{
  try {
    if (::party->load(path))
      ::party->set_status(Party::Status::initial_loaded);
    return ;
  } catch (ReadException const& exception) {
    // gettext: %s = filename
    ui->information(_("Message::party load: load error '%s'",
                      path.string())
                    + "\n"
                    + exception.message(),
                    InformationType::problem);
  }
}

void load_default_party()
{
  Path const path = ::preferences.config_directory() / "party";
  if (!is_regular_file(path)) {
#ifndef OUTDATED
    // 0.7.21, 2019-05-01: Es werden Regeln und Spieler immer in der Datei party gespeichert
    // load the rules
    party->rule().load(::preferences.config_directory() / "rules/last");
    party->load_players();
    { // load the default party
      Path const party_filename = ::preferences.config_directory() / "parties/current";
      if (is_regular_file(party_filename))
        if (party->load(party_filename))
          ::party->set_status(Party::Status::initial_loaded);
    } // load the default party
#endif
    return ;
  }
  load_party(path);
}

void set_ui(unique_ptr<UI> selected_ui)
{
  { // set the ui
    if (selected_ui) {
    } else if (::fast_play & FastPlay::seed_info) {
      selected_ui = UI::create(UIType::dummy);
    } else { // if !(selected_ui) && (::fast_play & FastPlay::seed_info)
      selected_ui
#if defined(USE_UI_GTKMM)
        = UI::create(UIType::gtkmm);
#else
      = UI::create(UIType::dummy);
#endif
    } // if !(selected_ui) && (::fast_play & FastPlay::seed_info)
    if (selected_ui) {
      ::ui = std::move(selected_ui);
    } // if (selected_ui)
  } // set the ui

  {
    auto const party_status_bak = ::party->status();
    ::party->set_status(Party::Status::programstart);
    // init the ui
    {
      int argc = 1;
      char** argv = new char*[2];
      argv[0] = const_cast<char*>(argv0.c_str());
      argv[1] = nullptr;
      ::ui->init(argc, argv);
      delete[] argv;
    }
    // show the messages in the new ui
    if (selected_ui) {
      // show information messages from the initializing
      for (auto const& m : ::ui->messages_shown())
        ::ui->information(m.first, m.second, true);
    }
    ::party->set_status(party_status_bak);
  }
}

#ifdef BUG_REPORT_REPLAY
/** @return   whether the string ends with 'x'
 **/
auto ends_with(string const text, string const x) -> bool
{
  if (text.length() <= x.length())
    return false;
  return (string(text, text.length() - x.length()) == x);
}
#endif

} // namespace
