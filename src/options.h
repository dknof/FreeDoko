/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "class/getopt/getopt.h"

void init_before_option_parsing();
void load_options_file();
void parse_options(int argc, char* argv[]);

auto usage_string(string program, GetOpt::Option const& option) -> string;
auto help_string()        -> string const& ;
auto directories_string() -> string;
auto version_string()     -> string;
auto license_string()     -> string;
auto defines_string()     -> string;
void add_debug(string keyword);
