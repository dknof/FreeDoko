/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef BUG_REPORT_REPLAY

#include "bug_report_replay.h"

#include "../basetypes/program_flow_exception.h"
#include "../party/party.h"
#include "../game/game.h"
#include "../game/gameplay_actions.h"
#include "../game/gameplay_actions/check.h"
#include "../game/gameplay_actions/card_played.h"
#include "../card/trick.h"
#include "../player/team_information.h"
#include "../player/ai/ai.h"

#include "../ui/ui.h"

#include "../misc/preferences.h"
#include "../misc/references_check.h"

#include "../utils/string.h"
#include "../utils/file.h"

#include <sstream>
#include <chrono>


// There can only be one 'bug report replay' instance.
// If a new is created, the old is removed.
unique_ptr<OS_NS::BugReportReplay> bug_report_replay; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)


namespace OS_NS {

Signal<void(BugReportReplay&)> BugReportReplay::signal_open; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

// returns the value from the line 'line'
static string get_value(string const& line);
// returns the keyword from the line 'line'
static string get_keyword(string const& line);


/** -> result
 **
 ** @param    line   the line
 **
 ** @return   the value from the given line
 **/
string
  get_value(string const& line)
  {
    if (line.empty())
      return "";

    string value = line;
    String::word_first_remove_with_blanks(value);

    return value;
  } // static string get_value(string line)

/** -> result
 **
 ** @param    line   the line
 **
 ** @return   the keyword from the given line
 **/
string
  get_keyword(string const& line)
  {
    if (line.empty())
      return "";

    string const word
      = (  (std::find(line.begin(), line.end(), ':')
            != line.end())
         ? string(line.begin(),
                  std::find(line.begin(), line.end(), ':'))
         : line
        );

    return word;
  } // static string get_keyword(string line)

/** Constructor
 ** (to be called from a child class)
 **
 ** @param    file   the file with the bug report
 **/
BugReportReplay::BugReportReplay(Path file,
                                 unsigned verbose) :
  file_(file),
  verbose_(verbose)
{
  if (!is_regular_file(file)) {
    cerr << "Selected bug report '" << file << "' is no file." << endl;
    return ;
  }

  try {
    read_file();
  } catch (ReadException const& read_exception) {
    return ;
  }

  init();

  print_header();
}

/** Destructor
 **/
BugReportReplay::~BugReportReplay()
{
  signal_close();
}

/** initializes the bug report
 **/
void
BugReportReplay::init()
{
  Party::signal_open.connect(*this, &BugReportReplay::party_open, disconnector_);
  Game::signal_open.connect(*this, &BugReportReplay::game_open, disconnector_);

  ::preferences.set(Preferences::Type::save_party_changes, false);
  ::preferences.set(Preferences::Type::additional_party_settings, true);
  check_action_ = false;

  if (::party->status() == Party::Status::init) {
    BugReportReplay::party_open(*::party);
    BugReportReplay::party_get_settings();
  }

#ifdef POSTPONED
  // is called in 'party_open' so that this party is set
  // (-> UI_GTKMM::BugReportReplay::update_info)
  ::ui->bug_report_replay_open(*this);
#endif
} // void BugReportReplay::init()

/** write the data of this bug report into 'ostr'
 **
 ** @param    ostr   output stream
 **/
void
BugReportReplay::write(ostream& ostr) const
{
  ostr << "file: "      << file_ << '\n';
  if (version_)
    ostr << "version: " << *version_ << '\n';
  else
    ostr << "version: -\n";
  ostr << "compiled: "  << compiled_ << '\n';
  ostr << "system: "    << system_ << '\n';
  ostr << "time: "      << time_ << '\n';
  ostr << "language: "  << language_ << '\n';
  ostr << "trickno: "   << trickno_ << '\n';

  ostr << '\n';

  ostr << "message\n"
    << "{\n"
    << message_
    << "}\n";

  ostr << '\n';

  ostr << "seed: "              << seed_ << '\n';
  ostr << "startplayer_no: "    << startplayer_no_ << '\n';
  ostr << "game_type: "         << game_type_ << '\n';
  ostr << "marriage_selector: " << marriage_selector_ << '\n';
  ostr << "soloplayer_no: "     << soloplayer_no_ << '\n';

  ostr << '\n';

  ostr << "rule\n"
    << "{\n"
    << rule_
    << "}\n";

  ostr << '\n';

  ostr << "persons\n"
    << "{\n";
  for (auto const& p : persons_)
    ostr << "{\n" << *p << "}\n";
  ostr << "}";

  ostr << '\n';

  if (!poverty_cards_shifted_.empty()) {
    ostr << "poverty cards shifted\n"
      << "{\n";
    for (auto const c : poverty_cards_shifted_)
      ostr << c << '\n';
    ostr << "}\n";
  } else {
    ostr << "poverty cards shifted: -\n";
  }

  if (!poverty_cards_returned_.empty()) {
    ostr << "poverty cards returned\n"
      << "{\n";
    for (auto const c : poverty_cards_returned_)
      ostr << c << '\n';
    ostr << "}\n";
  } else {
    ostr << "poverty cards returned: -\n";
  }

  ostr << '\n';

  ostr << "hands\n"
    << "{\n";
  for (auto const& h : hands_)
    ostr << "{\n" << h << "}\n";
  ostr << "}\n";

  ostr << '\n';

  ostr << "swines: ";
  if (swines_player_no_ == UINT_MAX)
    ostr << '-';
  else
    ostr << swines_player_no_;
  ostr << '\n';
  ostr << "hyperswines: ";
  if (hyperswines_player_no_ == UINT_MAX)
    ostr << '-';
  else
    ostr << hyperswines_player_no_;
  ostr << '\n';


  ostr << '\n';

  ostr << "game actions\n"
    << "{\n";
  for (auto const& a : actions_)
    ostr << *a << '\n';
  ostr << "}\n";

  ostr << '\n';

  ostr << "full tricks\n"
    << "{\n";
  for (auto const& t : full_tricks_)
    ostr << t << '\n';
  ostr << "}\n";

  ostr << '\n';

  ostr << "game summary\n"
    << "{\n";
  if (game_summary_)
    ostr << *game_summary_;
  ostr << "}\n";

  ostr << '\n';

  ostr << "current hands\n"
    << "{\n";
  for (auto const& h : current_hands_)
    ostr << "{\n" << h << "}\n";
  ostr << "}\n";

  ostr << '\n';

  ostr << "current trick\n"
    << current_trick_;

  ostr << '\n';

  ostr << "human actions\n"
    << "{\n";
  for (auto const& a : human_actions_)
    ostr << *a << '\n';
  ostr << "}\n";
} // void BugReportReplay::write(ostream& ostr) const

/** test, whether the line is the expected
 **
 ** @param    read_line       line that was read
 ** @param    expected_line   line that is read
 **
 ** @return   whether the line is the expected
 **/
bool
BugReportReplay::expect_line(string const& read_line,
                             string const& expected_line)
{
  if (read_line != expected_line) {
    cerr << "BugReportReplay:\n"
      << "  expected line '" << expected_line << "', "
      << "got: '" << read_line << "'"
      << "\n";

    mismatch();
    return false;
  } // if (read_line != expected_line)

  return true;
} // bool BugReportReplay::expect_line(string read_line, string expected_line)

/** test, whether the keyword is the expected
 **
 ** @param    read_keyword       keyword that was read
 ** @param    expected_keyword   keyword that is read
 **
 ** @return   whether the keyword is the expected
 **/
bool
BugReportReplay::expect_keyword(string const& read_keyword,
                                string const& expected_keyword)
{
  if (read_keyword != expected_keyword) {
    cerr << "BugReportReplay:\n"
      << "  expected keyword '" << expected_keyword << "', "
      << "got: '" << read_keyword << "'"
      << "\n";

    mismatch();
    return false;
  } // if (read_keyword != expected_keyword)

  return true;
} // bool BugReportReplay::expect_keyword(string read_keyword, string expected_line)

/** reads the whole file
 **
 ** @todo   poverty
 ** @todo   game summary
 **/
void
BugReportReplay::read_file() // NOLINT(hicpp-function-size)
{
  cout << "Loading bug report " << file() << endl;
  ifstream istr(file());
  string line;    // the line read
  string keyword; // the keyword read
  string value;   // the value read

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GETLINE \
  do { \
    if (istr.eof()) { \
      throw EOF; \
    } \
    std::getline(istr, line);  \
    String::remove_blanks(line); \
    if ( !line.empty() && (*(line.end() - 1) == '\r') ) \
    line.erase(line.end() - 1); \
  } while (line.empty() && istr.good()) ; \
  keyword = get_keyword(line); \
  value = get_value(line); \

  // reads the variable from 'value'
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define READ_FROM_VALUE(var)  \
  { \
    value = get_value(line); \
    istringstream istr(value); \
    istr >> var; \
  }

  // expect the keyword
  // if it is not valid, return from the function
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define EXPECT_LINE(expected_line) \
  if (true) { \
    if (!expect_line(line, expected_line)) \
    throw ReadException("expected line '" + string(line) + "'") ; \
  } else
#if 0
  ;
#endif

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define EXPECT_KEYWORD(expected_keyword) \
  if (true) { \
    if (!expect_keyword(keyword, expected_keyword)) \
    throw ReadException("expected keyword '" + string(keyword) + "'") ; \
  } (void)0
#if 0
  ;
#endif

  try {
    human_finished_ = true;
    { // read the first lines (including message)
      // the first line
      GETLINE;
      // Byte Order Mark for UTF-8
      string bom = "...";
      bom[0] = 0xef;
      bom[1] = 0xbb;
      bom[2] = 0xbf;

      if (   (line != "# FreeDoko Bugreport")
          && (line != bom + "# FreeDoko Bugreport")
          && (line != "# FreeDoko Reference")
          && (line != bom + "# FreeDoko Reference") ) {
        cerr << "BugReportReplay::read_first_lines()\n"
          << "  The file is no bug report! First line does not match. Expecting\n"
          << "# FreeDoko Bugreport\n"
          << "  or\n"
          << "# FreeDoko Reference\n"
          << "  but got\n"
          << line
          << endl;
        mismatch();
        return ;
      } // if (line != "# FreeDoko Bugreport")

      // read and print the next informations
      // Example:
      // Version: 0.6.7b  (2004-12-19)
      // Compiled: Dec 19 2004, 19:36:18
      // System: Windows
      // Time: Fri Feb 25 22:55:22 2005
      //
      // Language: de
      // Trick: 9
      do { // while(!line.empty());
        GETLINE;

        if (line.empty()) {
        } else if (line == "/---------\\") {
          break;
        } else if (keyword == "version") {
          auto version = Version::create(value);
          if (version)
            version_ = make_unique<Version>(*version);
          else
            version_ = {};
          if (!version_) {
            cerr << "BugReportReplay: "
              << "could not read version: '" << value << "'\n"
              << "ignoring it." << endl;

          }
          if (   version_
              && (*version_ <= Version(0, 6, 6)) ) {
            cerr << "BugReportReplay: Too old version "
              << *version_
              << " < 0.6.6"
              << endl;
#ifndef DKNOF
            std::exit(EXIT_FAILURE);
#endif
          }
        } else if (keyword == "compiled") {
          compiled_  = value;
        } else if (keyword == "system") {
          system_    = value;
        } else if (keyword == "time") {
          time_      = value;
        } else if (keyword == "language") {
          language_  = value;
        } else if (keyword == "trick") {
          READ_FROM_VALUE(trickno_);
        } else { // if !(keyword == ...)
          cerr << "BugReportReplay: "
            << "unknown keyword '" << keyword << "'\n"
            << "ignoring it." << endl;
        } // if !(keyword == ...)

      } while(line != "/---------\\");

      { // read the message

        // the header exists of three lines -- ignore them
        GETLINE;
        GETLINE;

        message_.clear();
        do { // while (line != "/---------\\")
          GETLINE;
          if (line != "/--------\\")
            message_ += "  " + line + "\n";

          if (istr.fail()) {
            cerr << "BugReportReplay::read_first_lines()\n"
              << "  (unexpected) Error reading the first lines.\n"
              << "Aborting."
              << endl;
            std::exit(EXIT_FAILURE);
          } // if (istr.fail())
        } while(line != "/--------\\") ;

        // the foot exists of three lines -- ignore them (one is already ignored)
        GETLINE;
        GETLINE;
      } // read the message
      GETLINE;
    } // read the first lines
    if (line == "finished") {
      throw EOF;
    } // if (line == "finished")
    { // seed, startplayer
      if (keyword == "seed") {
        EXPECT_KEYWORD("seed");
        READ_FROM_VALUE(seed_);
        GETLINE;
      }

      if (keyword == "startplayer") {
        EXPECT_KEYWORD("startplayer");
        READ_FROM_VALUE(startplayer_no_);
        GETLINE;
      }
    } // seed, startplayer
    { // rules, players
      if (line == "party") {
        EXPECT_LINE("party");
        // a '{'
        GETLINE;
        EXPECT_LINE("{");
        ::party->read(istr);
        persons_.clear();
        for (auto const& p : ::party->players())
          persons_.push_back(p.clone());
        GETLINE;
        rule_ = ::party->rule();
        EXPECT_LINE("}");
#ifdef WORKAROUND
        // ToDo: this shouldn't be necessary
        ::party->set_seed(seed_);
        ::party->set_startplayer(startplayer_no_);
#endif
        GETLINE;
      } else { // if !(line == "party")
        EXPECT_LINE("rules");
        rule_.read(istr);
        // set the rules here, so that the default ai types see the setting of with_nines
        set_party_rule();

        GETLINE;
        if (line == "players") {
          EXPECT_LINE("players");

          // a '{'
          GETLINE;
          EXPECT_LINE("{");

          while (istr.peek() != '}') {
            persons_.push_back(Player::create(istr));
            if (   !istr.good()
                || !persons_.back()) {
              cerr << "BugReportReplay::read_file()\n"
                << "  could not load player.\n"
                << "Aborting."
                << endl;
              std::exit(EXIT_FAILURE);
            } // if (!players.back())
            while ((istr.peek() == '\n')
                   || (istr.peek() == '\r'))
              istr.get();
          } // while (istr.peek() != '}')

          GETLINE;
          EXPECT_LINE("}");
          GETLINE;
        } else {
          persons_.push_back(Player::create(Player::Type::ai, 0));
          persons_.push_back(Player::create(Player::Type::ai, 1));
          persons_.push_back(Player::create(Player::Type::ai, 2));
          persons_.push_back(Player::create(Player::Type::ai, 3));
        }
      } // if !(line == "party")
    } // players, players
    { // gametype
      if (keyword == "hands") {
        game_type_ = GameType::normal;
      } else { // if !(keyword == "hands")
        EXPECT_KEYWORD("gametype");
        String::word_first_remove_with_blanks(line);
        game_type_ = game_type_from_string(line);
        { // special cases
          if (   (is_solo(game_type_) && game_type_ != GameType::marriage_silent)
              || game_type_ == GameType::thrown_nines
              || game_type_ == GameType::thrown_kings
              || game_type_ == GameType::thrown_nines_and_kings
              || game_type_ == GameType::thrown_richness
              || game_type_ == GameType::fox_highest_trump) {
            GETLINE;
            EXPECT_KEYWORD("soloplayer");
            String::word_first_remove_with_blanks(line);
            String::word_first_remove_with_blanks(line);
            soloplayer_no_
              = static_cast<unsigned>(stoi(line));
          }
          if (   game_type_ == GameType::marriage
              || game_type_ == GameType::marriage_silent) {
            // read the selector and the bride
            GETLINE;
            EXPECT_KEYWORD("selector");
            String::word_first_remove_with_blanks(line);
            marriage_selector_ = marriage_selector_from_string(line);

            GETLINE;
            EXPECT_KEYWORD("bride");
            String::word_first_remove_with_blanks(line);
            String::word_first_remove_with_blanks(line);
            soloplayer_no_ = static_cast<unsigned>(stoi(line));
          } // if (game_type() == GameType::marriage)
          if (game_type_ == GameType::poverty) {
            // ToDo
          } // if (game_type == GameType::poverty)
        } // special cases
      } // if !(keyword == "hands")
    } // gametype
    { // hands
      if (keyword != "hands")
        GETLINE;
      EXPECT_LINE("hands");
      GETLINE;
      EXPECT_LINE("{");

      for (auto const& person : persons_) {
        (void)person;
        // the player number
        GETLINE;
        hands_.emplace_back(istr);
      }
      // a '}'
      GETLINE;
      EXPECT_LINE("}");
    } // hands
    { // actions, tricks
      GETLINE;
      EXPECT_LINE("gameplay actions");
      GETLINE;
      EXPECT_LINE("{");
      do { // while (line != "}")
        GETLINE;

        if (line == "}") {
          break;
        } else if (line[0] == '#') {
          continue;
        } else if (line == "stop") {
          auto_action_end_ = actions_.size();
          auto_start_party_ = true;
          continue;
        } else if (line.substr(0, 6) == "debug ") {
          ::debug.add(String::remove_blanks(line.substr(6)));
          continue;
        }

        auto action = GameplayAction::create(line, istr);
        if (!action) {
          cerr << "BugReportReplay:\n"
            << "  Unknown action '" << line << "'\n"
            << "  Ignoring it." << endl;
#ifdef DKNOF
          SEGFAULT;
#endif
          continue;
        }

#ifndef POSTPONED
        if (action->type() == GameplayActions::Type::trick_full)
          full_tricks_.push_back(dynamic_cast<GameplayActions::TrickFull*>(action.get())->trick);
#endif

        if (action->type() == GameplayActions::Type::check) {
          auto_action_end_ = actions_.size() + 1;
          auto_start_party_ = true;
        }

        { // special behaviour in a poverty: save the shifted cards
          if (action->type() == GameplayActions::Type::poverty_shift) {
            poverty_cards_shifted_
              = vector<Card>(dynamic_cast<GameplayActions::PovertyShift*>(action.get())->cards);
          } else if (action->type() == GameplayActions::Type::poverty_returned) {
            poverty_cards_returned_
              = vector<Card>(dynamic_cast<GameplayActions::PovertyReturned*>(action.get())->cards);
          } // if (action->type == GameplayAction::poverty_shift)
        } // special behaviour in a poverty: save the shifted cards

        actions_.push_back(std::move(action));

      } while (line != "}");
    } // actions, tricks
    { // game summary
      GETLINE;
      if (line == "game summary") {
        EXPECT_LINE("game summary");
        game_summary_ = make_unique<GameSummary>(rule_, istr);
        GETLINE;
        if (line == "special points") {
          // Just skip them -- they are contained in the game summary
          EXPECT_LINE("special points");
          GETLINE;
          EXPECT_LINE("{");

          do {
            GETLINE;
          } while (line != "}");
          GETLINE;
        } // if (line == "special points")
      } // if (line == "game summary")
    } // game summary
    if (istr.eof())
      throw EOF;
    { // current hands
      EXPECT_LINE("current hands");
      GETLINE;
      EXPECT_LINE("{");

      for (auto const& person : persons_) {
        (void)person;
        GETLINE;
        current_hands_.emplace_back(istr);
      } // for (person : persons_)
      // a '}'
      GETLINE;
      EXPECT_LINE("}");
      GETLINE;
    } // current hands
    if (istr.eof())
      throw EOF;
    { // current trick
      if (line == "current trick")
      {
        current_trick_ = Trick(istr);
        GETLINE;
      }
    } // current trick
    if (istr.eof())
      throw EOF;
    { // human actions
      EXPECT_LINE("human actions");
      GETLINE;
      EXPECT_LINE("{");
      do { // while (line != "}")

        GETLINE;
        if (line == "}")
          break;

        auto action = GameplayAction::create(line, istr);

        if (!action) {
          cerr << "BugReportReplay:\n"
            << "  Unknown human action '" << line << "'\n"
            << "  Ignoring it." << endl;
          continue;
        }

        human_actions_.push_back(std::move(action));

      } while (line != "}");
      human_finished_ = human_actions_.empty();
    } // human actions

  } catch (int const status) {
    if (status == EOF) {
      // finished
      // return ;
    } else {
      throw;
    }
  } catch (ReadException const& read_exception) {
    cerr << "BugReportReplay::read_file()\n"
      << "  read exception: " << read_exception.message()
      << endl;
    throw;
  } catch (...) {
    cerr << "BugReportReplay::read_file()\n"
      << "  unknown exception" << endl;
    throw;
  }

#undef GETLINE
#undef READ_FROM_VALUE
#undef EXPECT_LINE
#undef EXPECT_KEYWORD

  check_action_ = false;
  loaded_ = true;
} // void BugReportReplay::read_file()

/** @return   the current gameplay action
 **/
GameplayAction const&
BugReportReplay::current_action() const
{
  DEBUG_ASSERTION((current_action_no_ < actions_.size()),
                  "BugReportReplay::current_action()\n"
                  "  no further gameplay action");
  return *actions_[current_action_no_];
} // GameplayAction BugReportReplay::current_action() const

/** @return   the number of remaining actions
 **/
unsigned
BugReportReplay::remaining_actions() const
{
  return actions_.size() - current_action_no_;
}

/** @return   the current gameplay action
 **/
GameplayAction const&
BugReportReplay::next_action() const
{
  DEBUG_ASSERTION((current_action_no_ + 1 < actions_.size()),
                  "BugReportReplay::next_action()\n"
                  "  no further gameplay action");
  return *actions_[current_action_no_ + 1];
}

// NOLINTNEXTLINE(misc-no-recursion)
void BugReportReplay::action_processed(GameplayAction const& action,
                                       GameplayActions::Discrepancy const discrepancy)
{
  if (finished_)
    return ;

  // check the action when requested
  if (   check_action_
      && current_action().type() != GameplayActions::Type::check) {
    DEBUG_ASSERTION(actions_[current_action_no_ - 1]->type()
                    == GameplayActions::Type::check,
                    "BugReportReplay::action_processed(discrepancy)\n"
                    "  last action is not 'check'");
    reference_check_game_action(action, discrepancy);
    check_action_ = false;
  } // if (check_action_)

  if (current_action().type() == GameplayActions::Type::check) {
    check_action_ = true;
    actions_discrepancies_.push_back(discrepancy);
    current_action_no_ += 1;
    return ;
  }

  // test for a human action
  if (   !human_finished_
      && current_action() == current_human_action()) {
    human_actions_discrepancies_.push_back(discrepancy);
    current_human_action_no_ += 1;
    if (current_human_action_no_ == human_actions_.size()) {
      human_finished_ = true;
    }
  } // if (human action)

  // save the discrepancy and go to the next action
  actions_discrepancies_.push_back(discrepancy);
  current_action_no_ += 1;

  if (current_action_no_ == actions_.size()) {
    end_reached();
    return ;
  }

  if (!::party->in_game())
    return ;
  auto const& game = ::party->game();

  // automatic actions for the human player
  if (game.status() >= Game::Status::play)
    handle_current_human_action();

  if (finished_)
    return ;

  // test for printing to perform
  if (current_action().type() == GameplayActions::Type::print) {
    if (   game.status() >= Game::Status::poverty_shift
        && game.status() < Game::Status::finished) {
      // in a running game
      auto const& action = dynamic_cast<GameplayActions::Print const&>(current_action());
      auto const& player = game.players().current_player();
      auto ai = dynamic_cast<Ai const*>(&player);
      if (action.info == "help") {
        cout << "print teams: print the team information of the ai\n";
        cout << "print cards: print the cards information of the ai\n";
        cout << "print time:  print the current time\n";
      } else if (action.info == "time") {
        auto time =  std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        auto timeinfo = localtime(&time);
        char s[20];
        std::strftime(s, 19, "%H:%M:%S", timeinfo); // NOLINT
        cout << s << '\n'; // NOLINT
      } else if (ai && (action.info == "teams")) {
        cout << '\n'
          << ai->name() << ": "
          << ai->team_information() << '\n';
      } else if (ai && (action.info == "cards")) {
        cout << '\n'
          << ai->name() << ": "
          << ai->cards_information() << '\n';
      } else {
        cout << action.info << '\n';
      }
    } // if (game_status ...)
    action_processed(current_action(), GameplayActions::Discrepancy::none);
  } // if (current_action().type() == GameplayAction::print)

  if (current_action().type() == GameplayActions::Type::quit) {
    throw ProgramFlowException::Quit();
  }

  if (current_action().type() == GameplayActions::Type::check) {
    check_action_ = true;
    action_processed(current_action(), GameplayActions::Discrepancy::none);
  }
} // void BugReportReplay::action_processed(GameplayAction action, GameplayAction::Discrepancy discrepancy)

/** check the game action of a reference
 ** Print and save the result.
 ** End the game, if it is the last check
 **
 ** @param    action        processed action
 ** @param    discrepancy   discrepancy of the processed and the saved action
 **/
void
BugReportReplay::reference_check_game_action(GameplayAction const& action,
                                             GameplayActions::Discrepancy discrepancy)
{
  DEBUG_ASSERTION(current_action_no_ > 0,
                  "BugreportReplay::reference_check_game_action(action, discrepancy)\n"
                  "  this is the first action in the bug report replay");

  DEBUG_ASSERTION((actions_[current_action_no_ - 1]->type()
                   == GameplayActions::Type::check),
                  "BugreportReplay::reference_check_game_action(action, discrepancy)\n"
                  "  last action is no check");
  auto const& check_action
    = dynamic_cast<GameplayActions::Check const&>(*actions_[current_action_no_ - 1]);

  if (::references_check)
    ::references_check->check(::party->game(),
                              action, current_action(),
                              check_action.comment,
                              discrepancy,
                              (auto_action_end_
                               == current_action_no_) );
} // void BugReportReplay::reference_check_game_action(GameplayAction action, GameplayAction::Discrepancy discrepancy)

/** -> result
 **
 ** @param    action_no   number of the action
 **
 ** @return   the discrepancy for the given action
 **/
GameplayActions::Discrepancy
BugReportReplay::discrepancy(unsigned const action_no) const
{
  if (action_no >= actions_discrepancies_.size())
    return GameplayActions::Discrepancy::future;

  return actions_discrepancies_[action_no];
} // Discrepancy BugReportReplay::discrepancy(unsigned action_no) const

/** @return   the current human action
 **/
GameplayAction const&
BugReportReplay::current_human_action() const
{
  DEBUG_ASSERTION((current_human_action_no_ < human_actions_.size()),
                  "BugReportReplay::current_human_action()\n"
                  "  no further human action");
  return *human_actions_[current_human_action_no_];
} // GameplayAction BugReportReplay::current_human_action() const

/** -> result
 **
 ** @param    action_no   number of the human action
 **
 ** @return   the discrepancy for the given human action
 **/
GameplayActions::Discrepancy
BugReportReplay::human_discrepancy(unsigned const action_no) const
{
  if (action_no >= human_actions_discrepancies_.size())
    return GameplayActions::Discrepancy::future;

  return human_actions_discrepancies_[action_no];
} // Discrepancy BugReportReplay::human_discrepancy(unsigned action_no) const

/** handle the current human action
 **/
void
BugReportReplay::handle_current_human_action()
{
  if (   finished_
      || human_finished_
      || current_action() != current_human_action())
    return ;

  // print the coming action
  print_current_human_action();

  auto& game = ::party->game();

  // partly execute the action automatically
  switch (current_human_action().type()) {
  case GameplayActions::Type::announcement: {
    auto const& announcement_action
      = dynamic_cast<GameplayActions::Announcement const&>(current_human_action());
    if (verbose_ & verbose_action)
      cout << "BugReportReplay:\n"
        << "  making announcement '" << announcement_action.announcement << "'"
        << " for player " << announcement_action.player
        << endl;

    // make the announcement in place of the human and tell it the user
    game.announcements().make_announcement(announcement_action.announcement,
                                           game.player(announcement_action.player));
  } break;

  case GameplayActions::Type::swines: {
    auto const& swines_action
      = dynamic_cast<GameplayActions::Swines const&>(current_human_action());
    if (verbose_ & verbose_action)
      cout << "BugReportReplay:\n"
        << "  announce 'swines' for human player "
        << swines_action.player
        << endl;
    if (!game.swines().swines_announce(game.player(swines_action.player)))
      cerr << "Error announcing 'swines' for human player"
        << swines_action.player
        << endl;
  } break;

  case GameplayActions::Type::hyperswines: {
    auto const& hyperswines_action
      = dynamic_cast<GameplayActions::Hyperswines const&>(current_human_action());
    if (verbose_ & verbose_action)
      cout << "BugReportReplay:\n"
        << "  announce 'hyperswines' for human player "
        << hyperswines_action.player
        << endl;
    if (!game.swines().hyperswines_announce(game.player(hyperswines_action.player)))
      cerr << "Error announcing 'hyperswines' for human player"
        << hyperswines_action.player
        << endl;
  } break;

  default:
    break;
  } // switch (current_human_action().type())
} // void BugReportReplay::handle_current_human_action()

/** print the next human action
 **/
void
BugReportReplay::print_current_human_action() const
{
  if (!(verbose_ & verbose_human_action))
    return ;
  switch (current_human_action().type()) {
  case GameplayActions::Type::card_played: {
    auto const& card_played_action
      = dynamic_cast<GameplayActions::CardPlayed const&>(current_human_action());
    cout << "BugReportReplay: Human\n"
      << "  play " << card_played_action.card
      << endl;
  } break;

  case GameplayActions::Type::reservation: {
    auto const& reservation_action
      = dynamic_cast<GameplayActions::Reservation const&>(current_human_action());
    cout << "BugReportReplay: Human\n"
      << "  gametype " << reservation_action.reservation.game_type << '\n';
    if (reservation_action.reservation.game_type == GameType::marriage)
      cout << "  marriage selector " << reservation_action.reservation.marriage_selector <<'\n';
    if (reservation_action.reservation.swines)
      cout << "  swines\n";
    if (reservation_action.reservation.hyperswines)
      cout << "  hyperswines\n";
  } break;

  case GameplayActions::Type::poverty_shift: {
    auto const& poverty_shift_action
      = dynamic_cast<GameplayActions::PovertyShift const&>(current_human_action());
    cout << "BugReportReplay: Human\n"
      << "  poverty: shift\n";
    for (auto const& c : poverty_shift_action.cards)
      cout << "    " << c << '\n';
  } break;

  case GameplayActions::Type::poverty_returned: {
    auto const& poverty_returned_action
      = dynamic_cast<GameplayActions::PovertyReturned const&>(current_human_action());
    cout << "BugReportReplay: Human\n"
      << "  poverty: return\n";
    for (auto const& c : poverty_returned_action.cards)
      cout << "    " << c << '\n';
  } break;

  case GameplayActions::Type::poverty_accepted: {
    cout << "BugReportReplay: Human\n"
      << "  poverty: accept\n";
  } break;

  case GameplayActions::Type::poverty_denied: {
    cout << "BugReportReplay: Human\n"
      << "  poverty: deny\n";
  } break;

  case GameplayActions::Type::poverty_denied_by_all:
  case GameplayActions::Type::trick_open:
  case GameplayActions::Type::trick_full:
  case GameplayActions::Type::trick_taken:
  case GameplayActions::Type::marriage:
  case GameplayActions::Type::check:
  case GameplayActions::Type::print:
  case GameplayActions::Type::quit:
    // nothing to do
    break;

  case GameplayActions::Type::announcement:
  case GameplayActions::Type::swines:
  case GameplayActions::Type::hyperswines:
    // -> action_processed
    break;
  } // switch (current_human_action().type())
} // void BugReportReplay::print_current_human_action() const

/** the bug report is inconsistent:
 **/
void
BugReportReplay::mismatch()
{
  cerr << "BugReport mismatch!" << endl;
} // void BugReportReplay::mismatch()

/** the bug report is finished.
 **/
void
BugReportReplay::end_reached()
{
  finished_ = true;
  human_finished_ = true;

  if (verbose_ & verbose_finish) {
    cout << "BugReport: finished" << endl;
    cout << "Message:\n"
      << "{\n"
      << message_
      << "}"
      <<endl;
  }
} // void BugReportReplay::end_reached()

/** prints the header and the differences
 **/
void
BugReportReplay::print_header() const
{
  if (!(verbose_ & verbose_header))
    return;
  // print the first lines
  cout << "BugReport\n"
    << "\n";

  if (version_)
    cout << "version:     " << *version_  << '\n';
  else
    cout << "version:     -\n";
  cout
    << "compiled:    " << compiled_        << '\n'
    << "system:      " << system_          << '\n'
    << "time:        " << time_            << '\n'
    << "language:    " << language_        << '\n'
    << "seed:        " << seed_            << '\n'
    << "startplayer: " << startplayer_no_  << '\n'
    << "trick:       " << trickno_         << '\n'
    << '\n'
    << "message:\n"
    << "{\n"
    <<   message_
    << "}\n";
} // void BugReportReplay::print_header() const

/** @return   the file
 **/
Path const&
BugReportReplay::file() const
{ return file_; }

/** @return   wether the bug report was loaded successfully
 **/
bool
BugReportReplay::loaded() const
{ return loaded_; }

/** @return   whether the bug report replay is finished
 **/
bool
BugReportReplay::finished() const
{ return finished_; }

/** @return   wether the party is automatically started
 **/
bool
BugReportReplay::auto_start_party() const
{ return auto_start_party_; }

/** @return   the version
 **/
Version const&
BugReportReplay::version() const
{ return *version_; }

/** @return   the compiled string
 **/
string const&
BugReportReplay::compiled() const
{ return compiled_; }

/** @return   the system string
 **/
string const&
BugReportReplay::system() const
{ return system_; }

/** @return   the time string
 **/
string const&
BugReportReplay::time() const
{ return time_; }

/** @return   the language string
 **/
string const&
BugReportReplay::language() const
{ return language_; }

/** @return   the trick number
 **/
unsigned
BugReportReplay::trickno() const
{ return trickno_; }

/** @return   the message
 **/
string const&
BugReportReplay::message() const
{ return message_; }

/** @return   the game type
 **/
GameType
BugReportReplay::game_type() const
{ return game_type_; }

/** @return   the seed
 **/
::Seed
BugReportReplay::seed() const
{ return seed_; }

/** @return   the number of the startplayer
 **/
unsigned
BugReportReplay::startplayer_no() const
{ return startplayer_no_; }

/** @return   the number of the soloplayer
 **/
unsigned
BugReportReplay::soloplayer_no() const
{ return soloplayer_no_; }

/** @return   the shifted cards
 **/
vector<Card> const&
BugReportReplay::poverty_cards_shifted() const
{ return poverty_cards_shifted_; }

/** @return   the returned cards
 **/
vector<Card> const&
BugReportReplay::poverty_cards_returned() const
{ return poverty_cards_returned_; }

/** @return   the hands
 **/
vector<Hand> const&
BugReportReplay::hands() const
{ return hands_; }

/** @return   the marriage selector
 **/
MarriageSelector
BugReportReplay::marriage_selector() const
{ return marriage_selector_; }

/** @return   the number of the current action
 **/
unsigned
BugReportReplay::current_action_no() const
{ return current_action_no_; }

/** @return   the number of the current human action
 **/
unsigned
BugReportReplay::current_human_action_no() const
{ return current_human_action_no_; }

/** @return   the game summary
 **/
unique_ptr<GameSummary> const&
BugReportReplay::game_summary() const
{ return game_summary_; }

/** @return   the actions
 **/
vector<unique_ptr<GameplayAction>> const&
BugReportReplay::actions() const
{ return actions_; }

/** @return   the action discrepancies
 **/
vector<GameplayActions::Discrepancy> const&
BugReportReplay::actions_discrepancies() const
{ return actions_discrepancies_; }

/** @return   the human actions
 **/
vector<unique_ptr<GameplayAction>> const&
BugReportReplay::human_actions() const
{ return human_actions_; }

/** @return   the human action discrepancies
 **/
vector<GameplayActions::Discrepancy> const&
BugReportReplay::human_actions_discrepancies() const
{ return human_actions_discrepancies_; }

/** a party is opened
 **
 ** @param    party   the party that is opened
 **/
void
BugReportReplay::party_open(Party const& party)
{
  ::party->signal_get_settings.connect(*this, &BugReportReplay::party_get_settings, disconnector_);
  ::party->signal_loaded      .connect(*this, &BugReportReplay::party_loaded,       disconnector_);
  ::party->signal_start       .connect(*this, &BugReportReplay::party_start,        disconnector_);

  finished_ = false;
  current_action_no_ = 0;
  current_human_action_no_ = 0;
  actions_discrepancies_.clear();
  human_actions_discrepancies_.clear();

  human_finished_ = human_actions_.empty();

  BugReportReplay::signal_open(*this);

  if (current_action_no_ == actions_.size()) {
    end_reached();
    return ;
  }

  if (current_action().type() == GameplayActions::Type::check) {
    check_action_ = true;
    action_processed(current_action(),
                     GameplayActions::Discrepancy::none);
  } // if (current_action().type == GameplayActions::Type::check)
} // void BugReportReplay::party_open(Party party)

/** set the settings of the party
 **/
void
BugReportReplay::party_get_settings()
{
  ::party->set_seed(seed_);
  ::party->set_startplayer(startplayer_no_);

  // set the rules and print the rule differences
  set_party_rule();

  { // set the players
    for (unsigned n = 0; n < persons_.size(); ++n) {
      ::party->players().set(n, persons_[n]->clone());
    }
  } // set the players

  if (verbose_ & verbose_message)
    // repeat the message
    cout << '\n'
      << "Message:\n"
      << "{\n"
      <<   message_
      << "}\n";
} // void BugReportReplay::party_get_settings()

/** the party is loaded
 **/
void
BugReportReplay::party_loaded()
{
  // (re)set the rules and print the rule differences
  set_party_rule();

  if (verbose_ & verbose_message) {
    // repeat the message
    cout << '\n'
      << "Message:\n"
      << "{\n"
      <<   message_
      << "}\n";
  }
} // void BugReportReplay::party_loaded()

/** set the rule of the party
 **/
void
BugReportReplay::set_party_rule()
{
  ::party->rule() = rule_;
  auto const& rule = ::party->rule();

  if (verbose_ & verbose_rules) {
    cout << '\n'
      << "rule differences:\n"
      << "{\n";
    cout << setw(38) << ""
      << setw(12) << "bug_report"
      << setw(12) << "tournament"
      << "\n";

    auto const print_difference = [&rule](auto const type) {
      if (rule(type) != ::tournament_rule(type)) {
        cout << setw(38) << type
          << setw(12) << rule(type)
          << setw(12) << ::tournament_rule(type)
          << "\n";
      }
    };

    for (int i = Rule::Type::first; i <= Rule::Type::last; i++) {
      if (   i >= Rule::Type::bool_first
          && i <= Rule::Type::bool_last) {
        print_difference(Rule::Type::Bool(i));
      } else if (   i >= Rule::Type::unsigned_first
                 && i <= Rule::Type::unsigned_last) {
        print_difference(Rule::Type::Unsigned(i));
      } else if (   i >= Rule::Type::unsigned_extra_first
                 && i <= Rule::Type::unsigned_extra_last) {
        print_difference(Rule::Type::UnsignedExtra(i));
      } else if (i == Rule::Type::counting) {
        print_difference(Rule::Type::counting);
      } else {
        DEBUG_ASSERTION(false,
                        "BugReportReplay::party_get_settings():\n"
                        "  unknown ruletype number " << i);
      }
    }
    cout << "}\n";
  }
}

/** the party is started
 **/
void
BugReportReplay::party_start()
{
  auto_start_party_ = false;
} // void BugReportReplay::party_start()

/** the game is opened
 **
 ** @param    game   game that is opened
 **/
void
BugReportReplay::game_open(Game const& game)
{
  {
    auto& game = ::party->game();
    game.signal_gameplay_action    .connect(*this, &BugReportReplay::gameplay_action, disconnector_);
    game.poverty().signal_ask      .connect(*this, &BugReportReplay::poverty_ask,     disconnector_);
  }
} // void BugReportReplay::game_open(Game game)

/** a gameplay action
 **
 ** @param    action   the action
 **/
void
BugReportReplay::gameplay_action(GameplayAction const& action)
{
  if (finished_)
    return ;

  //auto const& game = ::party->game();
  // do handle all actions (see BugReport::gameplay_action(action) )
  switch (action.type()) {
  case GameplayActions::Type::poverty_denied_by_all:
  case GameplayActions::Type::trick_taken:
    // do not handle
    return ;
  case GameplayActions::Type::check:
  case GameplayActions::Type::print:
  case GameplayActions::Type::quit:
    // shall not happen
    DEBUG_ASSERTION(false,
                    "Gameplay action 'check'/'print'/'quit' should not be processed here");
    return ;

  case GameplayActions::Type::trick_open:
    handle_current_human_action();
    return ; // replaced by trick_full
  case GameplayActions::Type::reservation:
  case GameplayActions::Type::poverty_shift:
  case GameplayActions::Type::poverty_returned:
  case GameplayActions::Type::poverty_accepted:
  case GameplayActions::Type::poverty_denied:
  case GameplayActions::Type::card_played:
  case GameplayActions::Type::trick_full:
  case GameplayActions::Type::announcement:
  case GameplayActions::Type::swines:
  case GameplayActions::Type::hyperswines:
  case GameplayActions::Type::marriage:
    // do handle
    break;
  } // switch (action.type())

  // check that this action conforms to the gameplay
  if (   action == current_action()
      || (   action.type() == GameplayActions::Type::trick_open
          && current_action().type() == GameplayActions::Type::trick_full)
     ) {
    action_processed(action, GameplayActions::Discrepancy::none);
    return ;
  }
  if (   remaining_actions() >= 2
      && action == next_action()) {
    cerr << "BugReport action skipped:\n"
      << "  bug report: " << current_action() << endl;
    action_processed(current_action(), GameplayActions::Discrepancy::skipped);
    action_processed(action, GameplayActions::Discrepancy::none);
    return ;
  }
  // the action differs from the one of the bug report
  cerr << "BugReport different actions:\n"
    << "  game:       " << action << '\n'
    << "  bug report: " << current_action() << endl;
  if (action.type() == GameplayActions::Type::card_played) {
    auto const& player = ::party->game().player(dynamic_cast<GameplayActions::CardPlayed const&>(action).player);
    cerr << player.name() << '\n';
    try {
      cerr << dynamic_cast<Ai const&>(player).last_heuristic_rationale() << '\n';
    } catch (...) {
    }
  }
  if (action.type() == GameplayActions::Type::announcement) {
    auto const& player = ::party->game().player(dynamic_cast<GameplayActions::Announcement const&>(action).player);
    cerr << player.name() << '\n';
    try {
      cerr << dynamic_cast<Ai const&>(player).last_announcement_rationale() << '\n';
    } catch (...) {
    }
  }

  // if the type is equal there is only some other gameplay
  if (action.type() == current_action().type()) {
    switch (current_action().type()) {
    case GameplayActions::Type::card_played:
      if (dynamic_cast<GameplayActions::CardPlayed const&>(action).card
          != dynamic_cast<GameplayActions::CardPlayed const&>(current_action()).card) {
        action_processed(action, GameplayActions::Discrepancy::card);
        break;
      }
      [[fallthrough]];
    default:
      action_processed(action, GameplayActions::Discrepancy::other);
      break;
    } // switch (current_action().type())

    return ;
  } // if (action.type() == current_action().type())

  // skip announcements
  switch (current_action().type()) {
  case GameplayActions::Type::announcement:
  case GameplayActions::Type::swines:
  case GameplayActions::Type::hyperswines:
  case GameplayActions::Type::marriage:
    break;

  default:
    break;
  } // switch (current_action().type())

  switch (action.type()) {
  case GameplayActions::Type::card_played:
    if (current_action().type() == GameplayActions::Type::card_played) {
      action_processed(action, GameplayActions::Discrepancy::skipped);
      break;
    }
    // search the next card-played action
    while (!finished_) {
      action_processed(action, GameplayActions::Discrepancy::skipped);
      if (finished_)
        break ;
      if (current_action().type() == GameplayActions::Type::card_played)
        break ;
    } // while (!finished_)
    break;

  case GameplayActions::Type::reservation:
  case GameplayActions::Type::poverty_shift:
  case GameplayActions::Type::poverty_accepted:
  case GameplayActions::Type::poverty_returned:
  case GameplayActions::Type::poverty_denied:
  case GameplayActions::Type::poverty_denied_by_all:
  case GameplayActions::Type::trick_open:
  case GameplayActions::Type::trick_full:
  case GameplayActions::Type::trick_taken:
    // ToDo
    break;

  case GameplayActions::Type::announcement:
  case GameplayActions::Type::marriage:
  case GameplayActions::Type::swines:
  case GameplayActions::Type::hyperswines:
  case GameplayActions::Type::check:
  case GameplayActions::Type::print:
  case GameplayActions::Type::quit:
    // ignore
    break;
  } // switch (action.type())
} // void BugReportReplay::gameplay_action(GameplayAction action)

/** ask 'player' whether to accept the poverty
 **
 ** @param    player   player that is asked
 ** @param    cardno   number of shifted cards
 **/
void
BugReportReplay::poverty_ask(Player const& player,
                             unsigned const cardno)
{
  handle_current_human_action();
} // void BugReportReplay::poverty_ask(Player player, unsigned cardno);

/** -> result
 **
 ** @param    player   player
 **
 ** @return   the next card played by the player
 **/
HandCard
BugReportReplay::next_card(Player const& player) const
{
  if (human_finished_) {
    if (finished_)
      return {};
    auto const& action = current_action();
    if (action.type() != GameplayActions::Type::card_played)
      return {};
    auto const& card_played = dynamic_cast<GameplayActions::CardPlayed const&>(action);
    if (card_played.player != player.no())
      return {};
    return HandCard(player.hand(), card_played.card);
  }

  auto const& action = current_human_action();

  if (action.type() != GameplayActions::Type::card_played) {
    cerr << "Bug report: human action should be 'card played' but is:\n"
      << "  " << current_human_action() << endl;
    return {};
  } // if (current_human_action().type() != GameplayActions::Type::card_played)

  return HandCard(player.hand(), dynamic_cast<GameplayActions::CardPlayed const&>(current_human_action()).card);
}

/** @return   the number of the last auto action
 **/
unsigned
BugReportReplay::auto_action_end() const
{
  return auto_action_end_;
}

/** set the auto action end
 **
 ** @param    auto_action_end   the end of the auto actions
 **/
void
BugReportReplay::set_auto_action_end(unsigned const auto_action_end)
{
  auto_action_end_ = auto_action_end;
  auto_start_party_ = true;
}

/** @return   whether the current action should be executed automatically
 **/
bool
BugReportReplay::auto_action() const
{
  if (finished_)
    return false;

  return (!check_action_
          && (current_action_no_ < auto_action_end_));
} // bool BugReportReplay::auto_action() const

} // namespace OS_NS

#endif
