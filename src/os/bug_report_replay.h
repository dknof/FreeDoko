/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#ifdef BUG_REPORT_REPLAY

#include "../game/gameplay.h"
#include "../game/gameplay_action.h"
#include "../card/hand.h"
#include "../card/trick.h"
#include "../party/party.h"
#include "../party/rule.h"
#include "../game/game.h"
#include "../game/game_summary.h"

#include "../utils/version.h"

namespace OS_NS {
class BugReportReplay;
} // namespace OS_NS 
extern unique_ptr<OS_NS::BugReportReplay> bug_report_replay; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

namespace OS_NS {

/**
 ** A class to simplify the replay of the bug report.
 ** All info's from the bug report are used and differences to the
 ** gameplay are shown.
 **
 ** Parts of the bug report:
 ** - version      is printed
 ** - system       is printed
 ** - date         is printed
 ** - message      is printed
 ** - seed         is set
 ** - startplayer  is set
 ** - ruleset      is set, differences to the standard ruleset and
 **                the default are printed
 ** - players      are set, differences to the standard aiconfig
 **                are printed
 ** - hands        are checked
 **                * marriage: bridge is checked
 ** - gametype     is checked
 **                * marriage: bridge is checked
 **                * poverty: shifted cards are checked
 **
 ** @todo   poverty
 ** @todo   marriage
 ** @todo   automatic announcements for the player
 **/
class BugReportReplay {
public:
  enum Verbose {
    verbose_none = 0,
    verbose_seed = 1 << 1,
    verbose_header = 1 << 2,
    verbose_message = 1 << 3,
    verbose_rules = 1 << 4,
    verbose_action = 1 << 5,
    verbose_human_action = 1 << 6,
    verbose_finish = 1 << 7,
    verbose_all = ((1 << 8) - 1)
  }; // enum Verbose
public:
  BugReportReplay() = delete;
  BugReportReplay(Path file, unsigned verbose);
  BugReportReplay(BugReportReplay const&) = delete;
  auto operator=(BugReportReplay const&) = delete;
  ~BugReportReplay();

  void write(ostream& ostr) const;

  // prints the header and the differences
  void print_header() const;

  auto file() const -> Path const&;
  auto loaded() const -> bool;
  auto finished() const -> bool;

  auto auto_start_party()       const -> bool;
  auto version()                const -> Version const&;
  auto compiled()               const -> string const&;
  auto system()                 const -> string const&;
  auto time()                   const -> string const&;
  auto language()               const -> string const&;
  auto trickno()                const -> unsigned;
  auto message()                const -> string const&;
  auto game_type()              const -> GameType;
  auto seed()                   const -> ::Seed;
  auto startplayer_no()         const -> unsigned;
  auto soloplayer_no()          const -> unsigned;
  auto marriage_selector()      const -> MarriageSelector;
  auto poverty_cards_shifted()  const -> vector<Card> const&;
  auto poverty_cards_returned() const -> vector<Card> const&;
  auto hands()                  const -> vector<Hand> const&;
  auto current_action_no()      const -> unsigned;
  auto current_human_action_no()const -> unsigned;
  auto game_summary()           const -> unique_ptr<GameSummary> const&;
  auto actions()                const -> vector<unique_ptr<GameplayAction>> const&;
  auto actions_discrepancies()  const -> vector<GameplayActions::Discrepancy> const&;
  auto human_actions()          const -> vector<unique_ptr<GameplayAction>> const&;
  auto human_actions_discrepancies() const -> vector<GameplayActions::Discrepancy> const&;

  void party_open(Party const& party);
  void party_get_settings();
  void party_loaded();
  void set_party_rule();
  void party_start();

  void game_open(Game const& game);
  void gameplay_action(GameplayAction const& action);

  void poverty_ask(Player const& player, unsigned cardno);

  auto next_card(Player const& player) const -> HandCard;

  auto discrepancy(unsigned action_no) const -> GameplayActions::Discrepancy;
  auto human_discrepancy(unsigned action_no) const -> GameplayActions::Discrepancy;

  auto current_action() const -> GameplayAction const&;
  auto remaining_actions() const -> unsigned;
  auto next_action() const -> GameplayAction const&;
  auto auto_action_end() const -> unsigned;
  void set_auto_action_end(unsigned auto_action_end);
  auto auto_action() const -> bool;

private:
  void init();

private:
  void read_file();

  auto expect_line(string const& read_line, string const& expected_line) -> bool;
  auto expect_value(string const& read_value, string const& expected_value) -> bool;
  auto expect_keyword(string const& read_keyword, string const& expected_keyword) -> bool;

  void action_processed(GameplayAction const& action, GameplayActions::Discrepancy discrepancy);
  void reference_check_game_action(GameplayAction const& action, GameplayActions::Discrepancy discrepancy);

  auto current_human_action() const -> GameplayAction const&;
  void handle_current_human_action();
  void print_current_human_action() const;

  void mismatch();
  void end_reached();

private:
  AutoDisconnector disconnector_;

  Path file_;
  unsigned verbose_ = verbose_none;
  bool loaded_              = false;
  bool inconsistent_        = false;
  bool finished_            = false;
  bool human_finished_      = false;
  bool auto_start_party_    = false;
  unsigned auto_action_end_ = 0;
  bool check_action_        = false;

  unique_ptr<Version> version_;
  string compiled_;
  string system_;
  string time_;
  string language_;
  unsigned trickno_ = UINT_MAX;
  string message_;
  ::Seed seed_ = UINT_MAX;
  unsigned startplayer_no_ = UINT_MAX;
  ::GameType game_type_ = GameType::normal;
  ::MarriageSelector marriage_selector_ = MarriageSelector::team_set;
  unsigned soloplayer_no_ = UINT_MAX;

  ::Rule rule_;
  vector<unique_ptr<Player>> persons_;
  vector<Card> poverty_cards_shifted_;
  vector<Card> poverty_cards_returned_;
  vector<Hand> hands_;
  unsigned swines_player_no_ = UINT_MAX;
  unsigned hyperswines_player_no_ = UINT_MAX;
  vector<unique_ptr<GameplayAction>> actions_;
  unsigned current_action_no_ = 0;
  vector<GameplayActions::Discrepancy> actions_discrepancies_;

  vector<::Trick> full_tricks_;
  unique_ptr<::GameSummary> game_summary_;

  vector<::Hand> current_hands_;
  ::Trick current_trick_;
  vector<unique_ptr<GameplayAction>> human_actions_;
  unsigned current_human_action_no_ = 0;
  vector<GameplayActions::Discrepancy> human_actions_discrepancies_;

public:
  static Signal<void(BugReportReplay&)> signal_open; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
  Signal<void()> signal_close;
};

} // namespace OS_NS

#endif
