/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "gameplay.h"

#include "../party/party.h"
#include "../party/rule.h"
#include "../game/game.h"
#include "../game/game_summary.h"
#include "../card/trick.h"

namespace OS_NS {

Gameplay::Gameplay(ostream* ostr) :
  ostr(ostr)
{
  Party::signal_open.connect(*this, &Gameplay::party_open, disconnector_);
  Game::signal_open .connect(*this, &Gameplay::game_open,  disconnector_);
}

Gameplay::~Gameplay() = default;

void Gameplay::party_open(Party const& party)
{
  party_ = &party;
  ::party->signal_start .connect(*this, &Gameplay::party_start,  disconnector_);
  ::party->signal_finish.connect(*this, &Gameplay::party_finish, disconnector_);
  ::party->signal_close .connect(*this, &Gameplay::party_close,  disconnector_);
  *ostr << "party open\n";
}

void Gameplay::party_start()
{
  *ostr << "party start\n";
}

void Gameplay::party_finish()
{
  *ostr << "party finish\n";
}

void Gameplay::party_close()
{
  party_ = {};
  *ostr << "party close\n";
}

void Gameplay::game_open(Game const& game)
{
  game_ = &game;
  {
    auto& game = ::party->game(); // cppcheck-suppress shadowArgument
    game.signal_cards_distributed  .connect(*this, &Gameplay::game_cards_distributed, disconnector_);
    game.signal_redistribute       .connect(*this, &Gameplay::game_redistribute,      disconnector_);
    game.signal_reservation_got    .connect(*this, &Gameplay::reservation_got,        disconnector_);
    game.signal_start              .connect(*this, &Gameplay::game_start,             disconnector_);
    game.signal_finish             .connect(*this, &Gameplay::game_finish,            disconnector_);
    game.signal_close              .connect(*this, &Gameplay::game_close,             disconnector_);
    game.tricks().signal_trick_open.connect(*this, &Gameplay::trick_open,             disconnector_);
    game.tricks().signal_trick_full.connect(*this, &Gameplay::trick_full,             disconnector_);
    game.signal_card_played        .connect(*this, &Gameplay::card_played,            disconnector_);
    game.announcements().signal_announcement_made.connect(*this, &Gameplay::announcement_made,     disconnector_);
    game.swines().signal_swines_announced        .connect(*this, &Gameplay::swines_announced,      disconnector_);
    game.swines().signal_hyperswines_announced   .connect(*this, &Gameplay::hyperswines_announced, disconnector_);
    game.poverty().signal_shift             .connect(*this, &Gameplay::poverty_shift, disconnector_);
    game.poverty().signal_take_denied_by_all.connect(*this, &Gameplay::poverty_take_denied_by_all, disconnector_);
    game.poverty().signal_take_accepted     .connect(*this, &Gameplay::poverty_take_accepted, disconnector_);
    game.marriage().signal_marriage         .connect(*this, &Gameplay::marriage, disconnector_);
  }
  *ostr << "game open: " << party_->gameno() << '\n';
  *ostr << "seed: " << game.seed() << '\n';
}

void Gameplay::game_cards_distributed()
{
  *ostr << "cards distributed\n";
}

void Gameplay::game_redistribute()
{
  *ostr << "game redistribute\n";
}

void Gameplay::reservation_got(Reservation const& reservation, Player const& player)
{
  *ostr << "reservation: "
    << player.no() << " " << player.name() << ": "
    << reservation;
}

void Gameplay::game_start()
{
  auto const& game = *game_;
  *ostr << "game start: " << game.type() << "\n";
  if (is_solo(game.type())) {
    *ostr << "Soloplayer: "
      << game.players().soloplayer().no()
      << "\n";
  }
}

void Gameplay::game_finish()
{
  *ostr << "game finish\n";
  *ostr << party_->last_game_summary();
}

void Gameplay::game_close()
{
  game_ = {};
  *ostr << "game close\n";
}

void Gameplay::card_played(HandCard const card)
{
  *ostr << game_->tricks().current().actcardno() << ". "
    << card.player().name() << ": "
    << card << "\n";
}

void Gameplay::announcement_made(Announcement const announcement, Player const& player)
{
  *ostr << "announcement: "
    << "player " << player.no() << ": "
    << announcement
    << "\n";
}

void Gameplay::swines_announced(Player const& player)
{
  *ostr << "swines announced: "
    << "player " << player.no()
    << "\n";
}

void Gameplay::hyperswines_announced(Player const& player)
{
  *ostr << "hyperswines announced: "
    << "player " << player.no()
    << "\n";
}

void Gameplay::poverty_shift(Player const& player, unsigned const cardno)
{
  *ostr << "poverty: "
    << "player " << player.no()
    << " shifts " << cardno << " cards.\n";
}

void Gameplay::poverty_take_denied_by_all()
{
  *ostr << "poverty: "
    << "denied by all players\n";
}

void Gameplay::poverty_take_accepted(Player const& player,
                                     unsigned const cardno,
                                     unsigned const trumpno)
{
  *ostr << "poverty: "
    << "player " << player.no() << " accepts poverty and returns "
    << cardno << " cards with " << trumpno << " trumps.\n";
}

void Gameplay::marriage(Player const& bridegroom,
                        Player const& bride)
{
  *ostr << "Marriage: "
    << "player " << bridegroom.no()
    << " with player " << bridegroom.no()
    << '\n';
}

void Gameplay::trick_open(Trick const& trick)
{
  *ostr << "\n"
    << "Trick: " << game_->tricks().current_no() << "\n";
}

void Gameplay::trick_full(Trick const& trick)
{
  *ostr << "Winner: "
    << trick.winnerplayer().name()
    << "\n";
}

} // namespace OS_NS
