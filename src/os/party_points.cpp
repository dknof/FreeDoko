/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "party_points.h"

#include "../party/party.h"
#include "../game/game_summary.h"

namespace OS_NS {

PartyPoints::PartyPoints(ostream* ostr) :
  ostr(ostr)
{
  Game::signal_open.connect(*this, &PartyPoints::game_open, disconnector_);
}

PartyPoints::~PartyPoints() = default;

void PartyPoints::game_open(Game const& game)
{
  ::party->game().signal_finish.connect(*this, &PartyPoints::game_finish, disconnector_);
}

void PartyPoints::game_finish()
{
  auto const& party = *::party; // cppcheck-suppress shadowVariable
  auto const& game_summary = party.last_game_summary();

  *ostr << setw(5) << party.gameno() << ": ";
  for (auto const& p : party.players())
    *ostr << setw(8) << game_summary.points(p);
  *ostr << '\n';
}

} // namespace OS_NS
