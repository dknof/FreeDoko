/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

class Game;

namespace OS_NS {

/// The default Seed
class Seed { // cppcheck-suppress noConstructor
public:
  explicit Seed(ostream& ostr);
  Seed(Seed const&) = delete;
  Seed& operator=(Seed const&) = delete;
  ~Seed();

  void game_open(Game& game);
  void game_start();

private:
  ostream& ostr;
  AutoDisconnector disconnector_;
}; // class Seed

} // namespace OS_NS
