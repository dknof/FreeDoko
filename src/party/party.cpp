/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include <iterator>

#include "constants.h"
#include "party.h"

#include "../versions.h"
#include "../utils/string.h"
#include "../utils/random.h"
#include "../utils/container_algorithm.h"
#include "../basetypes/fast_play.h"
#include "../basetypes/program_flow_exception.h"

#include "rule.h"
#include "../game/game_summary.h"
#include "../player/player.h"
#include "../player/ai/ai.h"
#include "../player/human/human.h"
#include "../card/trick.h"

#include "../misc/preferences.h"
#include "../misc/bug_report.h"

#include "../ui/ui.h"

#ifdef CHECK_RUNTIME
#include "../runtime.h"
#endif

#include <ctime>

Signal<void(Party&)> Party::signal_open; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

Party::Party() :
  rule_(make_unique<Rule>()),
  players_(make_unique<Players>(*this))
{
  set_random_seed();
  rule_->signal_changed.connect_back(*this, &Party::rule_changed);
}


auto Party::status() const noexcept -> Status
{
  return status_;
}


void Party::set_status(Status const status)
{
  status_ = status;
}



auto Party::rule() const noexcept -> Rule const&
{
  return *rule_;
}


auto Party::rule() noexcept -> Rule&
{
  return *rule_;
}


auto Party::players() const noexcept -> Players const&
{
  return *players_;
}


auto Party::players() noexcept -> Players&
{
  return *players_;
}


auto Party::random_seed() const noexcept -> bool
{
  return random_seed_;
}


auto Party::seed() const noexcept -> Seed
{
  if (in_configuration()) {
    if (random_seed_)
      return 0;
    return first_seed_;
  }

  return seed_;
}


void Party::set_seed(Seed const seed) noexcept
{
  if (in_configuration()) {
    if (first_seed_ != seed) {
      first_seed_ = seed;
      signal_seed_changed();
    }
  }
  if (!::preferences(Preferences::Type::additional_party_settings) && game_summaries_.empty())
    set_random_seed();
  else if (seed == UINT_MAX)
    set_random_seed();
  else {
    random_seed_ = false;
    seed_ = seed;
  }
}


void Party::set_random_seed() noexcept
{
  if (in_configuration()) {
    if (!random_seed_) {
      random_seed_ = true;
      signal_seed_changed();
    }
  }

  seed_ = ::random_value.get();
}


auto Party::next_seed() noexcept -> Seed
{
  // just increase the seed by one for a new cards distribution
  seed_ += 1;

  return seed_;
}


auto Party::random_startplayer() const noexcept -> bool
{
  return random_startplayer_;
}


auto Party::startplayer() const noexcept -> unsigned
{
  if (in_configuration()) {
    return first_startplayer_;
  }

  return startplayer_;
}


void Party::set_startplayer(unsigned const startplayer) noexcept
{
  if (in_configuration()) {
    if (first_startplayer_ != startplayer) {
      first_startplayer_ = startplayer;
      signal_startplayer_changed();
    }
  }
  if (!::preferences(Preferences::Type::additional_party_settings))
    startplayer_ = 0;
  else if (startplayer == UINT_MAX)
    set_random_startplayer();
  else {
    random_startplayer_ = false;
    startplayer_ = startplayer;
  }
}


void Party::set_random_startplayer() noexcept
{
  if (in_configuration()) {
    if (!random_startplayer_) {
      random_startplayer_ = true;
      signal_startplayer_changed();
    }
  }

  startplayer_ = ::random_value(players_->size());
}


auto Party::points() const noexcept -> Points
{
  return points_;
}


auto Party::in_game() const noexcept -> bool
{
  return (game_ != nullptr);
}


auto Party::game() const noexcept -> Game const&
{
  return *game_;
}


auto Party::game() noexcept -> Game&
{
  return *game_;
}


void Party::set_replay_game() noexcept
{
  replay_game_ = true;
}


auto Party::is_replayed_game() const noexcept -> bool
{
  return is_replayed_game_;
}


auto Party::hands_for_replay() const -> Hands const&
{
  DEBUG_ASSERTION(!hands_for_replay_.empty(),
                  "Party::hands_for_replay()\n"
                  "  hands are not set");
  return hands_for_replay_;
}


auto Party::in_configuration() const noexcept -> bool
{
  switch (status()) {
  case Status::programstart:
  case Status::init:
    return true;
  case Status::initial_loaded:
    return (gameno() == 0);
  default:
    return false;
  }
}


auto Party::roundno() const noexcept -> unsigned
{
  if (round_startgame_.empty())
    return 0;

  if (status() == Status::finished)
    return round_startgame_.size();

  return (round_startgame_.size() - 1);
}


auto Party::remaining_rounds() const -> unsigned
{
  // Started rounds are not counted.
  // If the last game of a round is finished, the round is counted as full.
  if (status() == Status::finished)
    return 0;

  if (!rule()(Rule::Type::number_of_rounds_limited))
    return 0;

  if (is_duty_soli_round())
    return 0;

  if (   rule()(Rule::Type::number_of_rounds) == roundno()
      && starts_new_round(gameno()))
    return 0;

#ifdef WORKAROUND
  // Manchmal ist remaining_rounds = -1. Ich habe bislang nicht rausgefunden, in welchen Fällen.
#ifdef DKNOF
  DEBUG_ASSERTION(rule()(Rule::Type::number_of_rounds)
                  > roundno(),
                  "Party::remaining_rounds()\n"
                  "  number of rounds = " << rule()(Rule::Type::number_of_rounds) << " <= " << roundno() << " = roundno()");
#endif
  if (rule()(Rule::Type::number_of_rounds) <= roundno())
    return 0;
#endif
  return (rule()(Rule::Type::number_of_rounds)
          - roundno()
          - 1);
}


auto Party::remaining_normal_games() const -> unsigned
{
  // A started game is not counted as remaining.
  // A finished game (gamestatus = game_finished) is not counted as remaining.
  if (!rule()(Rule::Type::number_of_rounds_limited))
    return 0;
  if (is_duty_soli_round())
    return 0;

#ifdef WORKAROUND
  // Sicherheitshalber..., siehe remaining_rounds()
  if (rule()(Rule::Type::number_of_rounds) * rule()(Rule::Type::number_of_games_per_round) < finished_normal_games_)
    return 0;
#endif
  return (  (  rule()(Rule::Type::number_of_rounds)
             * rule()(Rule::Type::number_of_games_per_round))
          - finished_normal_games_);
}


auto Party::remaining_games() const -> unsigned
{
  if (!rule()(Rule::Type::number_of_rounds_limited))
    return 0;

  return (remaining_normal_games() + remaining_duty_soli());
}


auto Party::gameno() const noexcept -> unsigned
{
  if (in_game() && game().status() == Game::Status::finished)
    return (game_summaries_.size() - 1);

  return game_summaries_.size();
}


auto Party::finished_games() const noexcept -> unsigned
{
  return game_summaries_.size();
}


auto Party::is_duty_soli_round() const -> bool
{
  if (   status() == Party::Status::finished
      && !game_summaries_.empty()
      && !is_real_solo(last_game_summary().game_type()))
    return false;

  return (   duty_soli_round_
          && roundno() >= *duty_soli_round_);
}


auto Party::is_duty_soli_round(unsigned const round) const -> bool
{
  return (   duty_soli_round_
          && (round == *duty_soli_round_));
}


auto Party::is_last_game() const -> bool
{
  // must be in a running party
  if (!in_game())
    return false;

  // if there are still duty soli to play
  if (remaining_duty_soli()
      > ( (   game().status() > Game::Status::reservation
           && game().status() < Game::Status::finished
           && game().is_duty_solo())
         ? 1 : 0))
    return false;

  if (   rule()(Rule::Type::number_of_rounds_limited)
      && remaining_normal_games() == 0
      && remaining_duty_soli()    == 0
     ) {
    return true;
  }

  // last game in duty soli round
  if (is_duty_soli_round()
      && (remaining_duty_soli()
          == ((game().status() < Game::Status::finished)
              ? 1
              : 0) )
      && (   (game().status() <= Game::Status::reservation)
          || game().is_duty_solo() ) )
    return true;

  // the points are limited
  if (   rule()(Rule::Type::points_limited)
      && remaining_points()    <= 0
      && remaining_duty_soli() == 0)
    return true;

  return false;
}


auto Party::normal_games_finished() const -> bool
{
  if (   rule()(Rule::Type::number_of_rounds_limited)
      && remaining_normal_games() == 0)
    return true;
  if (   rule()(Rule::Type::points_limited)
      && remaining_points() <= 0)
    return true;
  return false;
}


auto Party::is_finished() const -> bool
{
  return (   normal_games_finished()
          && (remaining_duty_soli() == 0));
}


auto Party::remaining_points() const noexcept -> Points
{
  if (!rule()(Rule::Type::points_limited))
    return -1;

  return max(0, static_cast<int>(rule()(Rule::Type::points)) - points());
}


auto Party::starts_new_round(unsigned const gameno) const noexcept -> bool
{
  if ((gameno == 0) || round_startgame_.empty())
    return true;
  if (gameno <= round_startgame_.back()) {
    for (auto const s : round_startgame_) {
      if (s == gameno) {
        return true;
      } else if (s > gameno) {
        return false;
      }
    }
    return false;
  } else { // if !(gameno <= round_startgame_.back())
    // the startgame of the next round
    unsigned next_round_startgame
      = round_startgame_.back() + players_->size();

    // check for played solo games in the current round
    for (unsigned g = round_startgame_.back(); g < this->gameno(); ++g)
      if (game_summary(g).startplayer_stays())
        next_round_startgame += 1;

    // check whether the current game is a solo
    if (   in_game()
        && game().startplayer_stays())
      next_round_startgame += 1;

    if (next_round_startgame > gameno)
      return false;
    if (rule()(Rule::Type::number_of_rounds_limited)
        && (  roundno() + (gameno - next_round_startgame) / players_->size()
            > rule()(Rule::Type::number_of_rounds)) )
      return false;
    return ((gameno - next_round_startgame) % players_->size()
            == 0);
  }
}


auto Party::round_of_game(unsigned const gameno) const -> unsigned
{
  if (gameno == 0)
    return 0;

  if (gameno == this->gameno()) {
    return roundno();
  }
  if (gameno < max(round_startgame_.back(), this->gameno())) {
    for (unsigned r = 1; r <= roundno(); ++r) {
      if (round_startgame_[r] > gameno) {
        return r - 1;
      }
    }
    if (gameno < this->gameno()) {
      return roundno() - 1;
    }
    return roundno();
  }

  // the startgame of the next round
  unsigned next_round_startgame
    = round_startgame_.back() + players_->size();

  // check for played solo games in the current round
  for (unsigned g = round_startgame_.back(); g < this->gameno(); ++g)
    if (game_summary(g).startplayer_stays())
      next_round_startgame += 1;

  // check whether the current game is a solo
  if (   in_game()
      && game().startplayer_stays())
    next_round_startgame += 1;

  if (is_duty_soli_round())
    next_round_startgame = gameno;
  if (gameno < next_round_startgame)
    return roundno();

  unsigned const round
    = (roundno() + 1
       + (gameno - next_round_startgame) / players_->size());

  // do not exceed the limited rounds (+ duty soli round)
  if (rule()(Rule::Type::number_of_rounds_limited))
    return min(round,
               rule()(Rule::Type::number_of_rounds)
               + ( (remaining_duty_soli() || is_duty_soli_round())
                  ? 1 : 0));
  else
    return round;
}


void Party::add_game_summary(unique_ptr<GameSummary> game_summary_p)
{
  game_summaries_.push_back(std::move(game_summary_p));
  auto const game_summary = game_summaries_.back().get();
  if (!game_summary->startplayer_stays() ) {
    finished_normal_games_ += 1;
  }
  if (!bock_multipliers_.empty()
      && !game_summary->startplayer_stays() ) {
    bock_multipliers_.erase(bock_multipliers_.begin());
  }

  for (auto const& p : *players_) {
    auto const n = players().no(p);
    if (rule()(Rule::Type::counting) == Counting::plus)
      points_ += game_summary->points(p);
    else
      points_ -= game_summary->points(p);
    pointsums_for_players_[n].emplace_back(  (pointsums_for_players_[n].empty()
                                              ? 0
                                              : pointsums_for_players_[n].back())
                                           + game_summary->points(p));
  }

  // add the bock multipliers
  if (rule()(Rule::Type::bock)) {
    if (!game_summary->bock_triggers().empty()) {
      if (rule()(Rule::Type::bock_append)) {
        for (unsigned g = 0;
             g < rule()(Rule::Type::number_of_games_per_round);
             ++g) {
          bock_multipliers_.push_back(rule()(Rule::Type::bock_multiplier));
        }
      } else { // if !(rule()(Rule::Type::bock_append))
        for (unsigned g = 0;
             g < rule()(Rule::Type::number_of_games_per_round);
             ++g) {
          if (bock_multipliers_.size() > g) {
            if (bock_multipliers_[g] < (1 << 10))
              bock_multipliers_[g] *= rule()(Rule::Type::bock_multiplier);
          } else {
            bock_multipliers_.push_back(rule()(Rule::Type::bock_multiplier));
          }
        }
      }
    }

    if (   rule()(Rule::Type::number_of_rounds_limited)
        && (bock_multipliers_.size()
            > (remaining_normal_games()
               + (remaining_duty_soli() ? 1 : 0) ) ) ) {
      bock_multipliers_.resize(remaining_normal_games()
                               + (remaining_duty_soli() ? 1 : 0));
    }
  }
}


auto Party::last_game_summary() const noexcept -> GameSummary const&
{
  return *game_summaries_.back();
}


auto Party::game_summary(unsigned const gameno) const -> GameSummary const&
{
  DEBUG_ASSERTION((gameno < game_summaries_.size()),
                  "Party::game_summary(gameno):\n"
                  "  gameno = " << gameno << " is too great "
                  "(maximal " << game_summaries_.size() << ")");
  return *game_summaries_[gameno];
}


auto Party::bock_multipliers() const noexcept -> vector<unsigned> const&
{
  return bock_multipliers_;
}


auto Party::current_bock_multiplier() const -> unsigned
{
  if (bock_multipliers_.empty())
    return 1;

  DEBUG_ASSERTION(bock_multipliers_[0] > 1,
                  "Party::current_bock_multiplier()\n"
                  "  bock multiplier is not > 1: " << bock_multipliers_[0]);
  return bock_multipliers_[0];
}


auto Party::bock_multiplier(unsigned const gameno) const noexcept -> unsigned
{
  if (gameno < finished_games())
    return game_summary(gameno).bock_multiplier();

  if (gameno >= finished_games() + bock_multipliers_.size())
    return 1;

  return bock_multipliers_[gameno - finished_games()];
}


auto Party::round_startgame(unsigned const roundno) const noexcept -> unsigned
{
  if (roundno == 0) {
    return 0;
  }
  if (roundno < round_startgame_.size()) {
    return round_startgame_[roundno];
  }
  if (   status() == Status::finished
      && roundno == round_startgame_.size()) {
    return game_summaries_.size();
  }

  // if roundno > current round we have to estimate
  if (in_game()) {
    return (gameno()
            + (players_->size()
               - ((players_->size()
                   + startplayer() - first_startplayer_
                  ) % players_->size())
              )
            + (game().startplayer_stays() ? 1 : 0)
            + (roundno - this->roundno() - 1) * players_->size()
           );
  } else if (is_finished()) {
    return gameno();
  } else {
    return (gameno()
            + (last_game_summary().startplayer_stays() ? 1 : 0)
            + (players_->size()
               - ((startplayer() - first_startplayer_
                   + players_->size()) % players_->size())
              )
            + (roundno - this->roundno() - 1) * players_->size()
           );
  }
}


auto Party::pointsum() const -> Points
{
  if (game_summaries_.empty())
    return 0;
  return pointsum_till_game(game_summaries_.size() - 1);
}


auto Party::pointsum_till_game(unsigned const gameno) const -> Points
{
  Points points = 0;

  for (unsigned g = 0; g <= min(gameno, finished_games()); ++g)
    points += game_summary(g).points();

  return points;
}


auto Party::pointsum_till_round(unsigned const roundno) const -> Points
{
  unsigned const gameno_end
    = ( (roundno == this->roundno())
       ? gameno() + ((in_game() && game().status() == Game::Status::finished)
                     ? 1
                     : 0)
       : round_startgame(roundno + 1) );

  Points points = 0;
  for (unsigned g = 0;
       g < min(gameno_end, finished_games());
       ++g) {
    points += game_summary(g).points();
  }

  return points;
}


auto Party::pointsum_in_round(unsigned const roundno) const -> Points
{
  unsigned const gameno_end
    = ( (roundno == this->roundno())
       ? gameno() + ((in_game() && game().status() == Game::Status::finished)
                     ? 1
                     : 0)
       : round_startgame(roundno + 1) );

  Points points = 0;
  for (unsigned g = round_startgame(roundno);
       g < min(gameno_end, finished_games());
       ++g) {
    points += game_summary(g).points();
  }

  return points;
}


auto Party::pointsum(Player const& player) const -> Points
{
  return pointsum(players_->no(player));
}


auto Party::pointsum(unsigned const p) const -> Points
{
  if (game_summaries_.empty())
    return 0;
  return pointsum_till_game(game_summaries_.size() - 1, p);
}


auto Party::pointsum_till_game(unsigned const gameno, unsigned const p) const -> Points
{
  auto const& pointsums = pointsums_for_players_[p];
  if (gameno < pointsums.size()) {
    DEBUG_ASSERTION(pointsums.size() > gameno,
                    "Party::pointsum_till_game(" << gameno << ", " << p << ")\n"
                    "   Size of pointsums_for_players_[p=" << p << "] = " << pointsums.size() << " is less then gameno = " << gameno << ".");
    return pointsums[gameno];
  }

  Points points = pointsums.back();

  if (in_game() && game().status() == Game::Status::finished) {
    auto const& player = players_->player(p);
    if (game_summary(gameno).has_played(player))
      points += game_summary(gameno).points(player);
  }

  return points;
}


auto Party::pointsum_till_round(unsigned const roundno, unsigned const p) const -> Points
{
  unsigned const gameno_end
    = ( (roundno == this->roundno())
       ? gameno() + ((in_game() && game().status() == Game::Status::finished)
                     ? 1
                     : 0)
       : round_startgame(roundno + 1) );

  if (gameno_end == 0)
    return 0;
  return pointsum_till_game(gameno_end - 1, p);
}


auto Party::pointsum_in_round(unsigned const roundno, unsigned const p) const -> Points
{
  auto const& player = players_->player(p);
  unsigned const gameno_end
    = ( (roundno == this->roundno())
       ? gameno() + ((in_game() && game().status() == Game::Status::finished)
                     ? 1
                     : 0)
       : round_startgame(roundno + 1) );

  Points points = 0;

  for (unsigned g = round_startgame(roundno);
       g < min(gameno_end, finished_games());
       ++g) {
    if (game_summary(g).has_played(player))
      points += game_summary(g).points(player);
  }

  return points;
}


void Party::recalc_pointsums_for_players()
{
  pointsums_for_players_.clear();
  pointsums_for_players_.resize(players_->size());
  for (auto const& g : game_summaries_) {
    for (auto const& p : *players_) {
      auto const n = players().no(p);
      pointsums_for_players_[n].emplace_back(  (pointsums_for_players_[n].empty()
                                                ? 0
                                                : pointsums_for_players_[n].back())
                                             + g->points(p));
    }
  }
}


auto Party::rang(Player const& player) const -> unsigned
{
  return rang(players_->no(player));
}


auto Party::rang(unsigned const p) const -> unsigned
{
  auto const points = pointsum(p);
  return count_if(*players_,
                  [points, this](Player const& player)
                  { return (points < pointsum(player)); }
                 );
}


auto Party::played_duty_soli(Player const& player) const -> unsigned
{
  return played_duty_soli(players_->no(player));
}


auto Party::played_duty_soli(unsigned const p) const -> unsigned
{
  unsigned n = 0;

  for (auto const& game_summary : game_summaries_) {
    if (   game_summary->is_duty_solo()
        && (game_summary->soloplayer() == p) )
      n += 1;
  }

  return min(n, rule()(Rule::Type::number_of_duty_soli));
}


auto Party::played_duty_color_soli(Player const& player) const -> unsigned
{
  return played_duty_color_soli(players_->no(player));
}


auto Party::played_duty_color_soli(unsigned const p) const -> unsigned
{
  unsigned n = 0;

  for (auto const& game_summary : game_summaries_)
    if (   game_summary->is_duty_solo()
        && is_color_solo(game_summary->game_type())
        && (game_summary->soloplayer() == p) )
      n += 1;

  return min(n, rule()(Rule::Type::number_of_duty_color_soli));
}


auto Party::played_duty_picture_soli(Player const& player) const -> unsigned
{
  return played_duty_picture_soli(players_->no(player));
}


auto Party::played_duty_picture_soli(unsigned const p) const -> unsigned
{
  unsigned n = 0;

  for (auto const& game_summary : game_summaries_) {
    if (   game_summary->is_duty_solo()
        && is_picture_solo(game_summary->game_type())
        && (game_summary->soloplayer() == p) )
      n += 1;
  }

  return min(n, rule()(Rule::Type::number_of_duty_picture_soli));
}


auto Party::played_duty_free_soli(Player const& player) const -> unsigned
{
  return played_duty_free_soli(players_->no(player));
}


auto Party::played_duty_free_soli(unsigned const p) const -> unsigned
{
  return (  played_duty_soli(p)
          - played_duty_color_soli(p)
          - played_duty_picture_soli(p));
}


auto Party::played_lust_soli(Player const& player) const -> unsigned
{
  return played_lust_soli(players_->no(player));
}


auto Party::played_lust_soli(unsigned const p) const -> unsigned
{
  return count_if(game_summaries_,
                  [p](auto const& gs) {
                  return (   gs->is_lust_solo()
                          && (gs->soloplayer() == p) );
                  });
}


auto Party::remaining_duty_soli() const -> unsigned
{
  unsigned n = 0;
  for (auto const& p : *players_)
    n += remaining_duty_soli(p);

  return n;
}


auto Party::remaining_duty_soli(Player const& player) const -> unsigned
{
  return remaining_duty_soli(players_->no(player));
}


auto Party::remaining_duty_soli(unsigned const p) const -> unsigned
{
  return (rule()(Rule::Type::number_of_duty_soli)
          - played_duty_soli(p));
}


auto Party::remaining_duty_color_soli(Player const& player) const -> unsigned
{
  return remaining_duty_color_soli(players_->no(player));
}


auto Party::remaining_duty_color_soli(unsigned const p) const -> unsigned
{
  return (rule()(Rule::Type::number_of_duty_color_soli)
          - played_duty_color_soli(p));
}


auto Party::remaining_duty_picture_soli(Player const& player) const -> unsigned
{
  return remaining_duty_picture_soli(players_->no(player));
}


auto Party::remaining_duty_picture_soli(unsigned const p) const -> unsigned
{
  return (rule()(Rule::Type::number_of_duty_picture_soli)
          - played_duty_picture_soli(p));
}


auto Party::remaining_duty_free_soli(Player const& player) const -> unsigned
{
  return remaining_duty_free_soli(players_->no(player));
}


auto Party::remaining_duty_free_soli(unsigned const p) const -> unsigned
{
  return (  remaining_duty_soli(p)
          - remaining_duty_color_soli(p)
          - remaining_duty_picture_soli(p));
}


void Party::write(ostream& ostr) const
{
  if (   status() < Party::Status::play
      || game_summaries_.empty()) {
    ostr << "first seed = " << (random_seed_ ? "-"s : std::to_string(first_seed_)) << '\n'
      << "first startplayer = " << (random_startplayer_ ? "-"s : std::to_string(first_startplayer_)) << '\n'
      << '\n';
  } else if (status() == Party::Status::finished) {
    ostr << "first seed = " << (random_seed_ ? "-"s : std::to_string(first_seed_)) << '\n'
      << "first startplayer = " << (random_startplayer_ ? "-"s : std::to_string(first_startplayer_)) << '\n'
      << '\n';
  } else {
    ostr << "# first seed (setting) = " << (random_seed_ ? "-"s : std::to_string(first_seed_)) << '\n'
      << "# first startplayer (setting) = " << (random_startplayer_ ? "-"s : std::to_string(first_startplayer_)) << '\n'
      << "first seed = " << first_seed_ << '\n'
      << "first startplayer = " << first_startplayer_ << '\n'
      << '\n';
  }

  ostr << "rules\n"
    << "{\n"
    << rule()
    << "}\n";

  ostr<< '\n';

  ostr << "players\n"
    << "{\n";
  for (auto const& player : *players_)
    ostr << "{\n" << player << "}\n\n";
  ostr << "}\n";

  ostr << '\n';

  ostr << "game summaries\n";
  ostr << "{\n";
  for (unsigned i = 0; i < game_summaries_.size(); ++i) {
    if (starts_new_round(i))
      ostr << "# round " << round_of_game(i) << '\n';
    ostr << "#  game " << i << '\n'
      << "{\n" << game_summary(i) << "}\n";
  }
  ostr << "}\n";

  ostr << '\n';
  ostr << "points\n";
  ostr << "{\n";
  for (auto const& p : *players_)
    ostr << p.name() << ": " << pointsum(p) << '\n';
  ostr << "}\n";
}


void Party::write_pointstable(ostream& ostr) const
{
  ostr << '\n';
  ostr << "party\n" ;
  ostr << "gameno: " << gameno() << '\n';
  ostr << "round startgame" << '\n';
  for (unsigned r = 0; r < round_startgame_.size(); ++r)
    ostr << r << ": " << round_startgame(r) << '\n';

  ostr << "game ";
  for (auto const& p : *players_)
    ostr << "| " << p.name() << ' ';
  ostr << "| bm ";
  ostr << "| gametype\n";
  ostr << "-----";
  for (auto const& p : *players_)
    ostr << "+-" << setw(p.name().length())
      << "-" << '-';
  ostr << "+----";
  ostr << "+-----\n";
  for (unsigned g = 0; g < gameno(); ++g) {
    if (starts_new_round(g)) {
      if (is_duty_soli_round(round_of_game(g)))
        ostr << "-duty";
      else
        ostr << "-" << setw(3) << round_of_game(g) << " ";
      for (auto const& p : *players_)
        ostr << "+-" << setw(p.name().length())
          << "-" << '-';
      ostr << "+----";
      ostr << "+-----\n";
    }
    ostr << setw(3) << g << "  ";
    for (auto const& p : *players_)
      ostr << "| " << setw(p.name().length())
        << game_summary(g).points(p)
        << ' ';
    ostr << "|" << setw(3);
    if (bock_multiplier(g) == 1)
      ostr << " ";
    else
      ostr << bock_multiplier(g);
    ostr << ' ';
    ostr << "| ";
    ostr << (game_summary(g).is_solo() ? '*' : ' ');
    if (!game_summary(g).bock_triggers().empty())
      ostr << game_summary(g).bock_triggers().size();
    ostr << '\n';
  }
}


auto Party::read(istream& istr) -> bool
{
  // party config values
  optional<unsigned> first_seed = 0;
  optional<unsigned> first_startplayer = 0;
  unique_ptr<Rule> rule;
  unique_ptr<Players> players;
  vector<unique_ptr<GameSummary>> game_summaries;

  Config config; // the config read
  try {
    int depth = 0; // the depth in the blocks
    do { // while (istr)
      istr >> config;

      if (istr.eof())
        break;

      if (istr.fail())
        break;

      if (config.empty())
        break;

      if (config.value.empty()) {
        if (config.name == "{") {
          depth += 1;
        } else if (config.name == "}") {
          depth -= 1;
          // finished with the party
          if (depth == -1)
            istr.putback('}');
          if (depth <= 0)
            break;

        } else if (config.name == "rules") {
          if (rule)
            throw ReadException("Party read: rule already read");
          rule = make_unique<Rule>();
          if (!rule->read(istr))
            throw ReadException("Party read: rule failed");

        } else if (config.name == "players") {
          if (players)
            throw ReadException("Party read: multiple players");

          istr >> config;
          if (! (config.value.empty()
                 && (config.name == "{")) )
            throw ReadException("Party read: players: expected '{'");

          players = make_unique<Players>(*this, istr);

          istr >> config;
          if (! (config.value.empty()
                 && (config.name == "}")) )
            throw ReadException("Party read: players: expected '}'");

        } else if (config.name == "game summaries") {
          {
            string const line = read_line(istr);
            if (line != "{")
              throw ReadException("Party read: game summaries: expected '{', "
                                  " found '" + line + "'");
          }
          unique_ptr<GameSummary> game_summary;
          while (istr.good()
                 && istr.peek() != '}') {
            try {
              game_summary = make_unique<GameSummary>(*rule, istr);
            } catch(...) {
              throw;
            }
            game_summaries.push_back(std::move(game_summary));
          }
          {
            string const line = read_line(istr);
            if (line != "}")
              throw ReadException("Party read: game summaries: expected '}', "
                                  " found '" + line + "'");
          }

        } else if (config.name == "points") {
          // ignore the block
          {
            string const line = read_line(istr);
            if (line != "{")
              throw ReadException("Party read: points: expected '{', "
                                  " found '" + line + "'");
          }
          while (istr.good()
                 && istr.peek() != '}') {
            Config points_config;
            istr >> points_config;
          }
          {
            string const line = read_line(istr);
            if (line != "}")
              throw ReadException("Party read: points: expected '}', "
                                  " found '" + line + "'");
          }

        } else { // if (config.name == ...)
          throw ReadException("Party read: unknown line "
                              "'" + config.name + "'");
        }
      } else { // if !(config.value.empty())
        if (config.name == "first seed") {
          istringstream istr2(config.value);
          istr2 >> first_seed;
          if (!istr2.good() && !istr2.eof())
            throw ReadException("Party read: error loading first seed");
        } else if (config.name == "first startplayer") {
          istringstream istr2(config.value);
          istr2 >> first_startplayer;
          if (!istr2.good() && !istr2.eof())
            throw ReadException("Party read: error loading first seed");
        } else { // if (config.name == ...)
          throw ReadException("Party read: unknown line "
                              "'" + config.name + " = " + config.value + "'");
        }
      }

    } while (istr.good());

    if (!istr.good() && !istr.eof())
      throw ReadException("Party read: unknown error in the input stream");

    if (!rule)
      throw ReadException("Party read: rule not loaded");

    if (players->size() != (*rule)(Rule::Type::number_of_players_in_game))
      throw ReadException("Party read: incorrect number of players");

  } catch (...) {
    throw;
  }

  status_ = Status::loaded;
  rule_ = std::move(rule);
  rule_->signal_changed.connect_back(*this, &Party::rule_changed);

  players_ = std::move(players);

  if (first_seed) {
    random_seed_ = false;
    first_seed_ = *first_seed;
  } else {
    random_seed_ = true;
    first_seed_ = 0;
  }
  if (first_startplayer) {
    random_startplayer_ = false;
    first_startplayer_ = *first_startplayer;
  } else {
    random_startplayer_ = true;
    first_startplayer_ = 0;
  }

  game_summaries_.clear();
  bock_multipliers_.clear();
  pointsums_for_players_.clear();
  pointsums_for_players_.resize(this->rule()(Rule::Type::number_of_players));

  if (game_summaries.empty()) {
    seed_ = first_seed_;
    startplayer_ = first_startplayer_;
    round_startgame_.clear();
    finished_normal_games_ = 0;
    points_ = 0;
  } else { // if !(game_summaries.empty())
    first_seed_ = game_summaries.front()->seed();
    first_startplayer_ = game_summaries.front()->startplayer();
    seed_ = game_summaries.back()->seed();
    startplayer_ = game_summaries.back()->startplayer();
    finished_normal_games_ = 0;
    points_ = 0;

    { // calculate round startgame
      unsigned gameno = 0;
      round_startgame_.clear();
      round_startgame_.push_back(0);
      for (auto& gs : game_summaries) {
        // check for a new round
        // ToDo: in the duty soli round not all players do play, so the check of the startplayer should be able to accept skipped players
        if (   gameno > 0
            && last_game_summary().startplayer()      != first_startplayer_
            && last_game_summary().next_startplayer() == first_startplayer_
            && !is_duty_soli_round()) {
          round_startgame_.push_back(finished_games());
        }
        // check for duty soli round
        if (!is_duty_soli_round()
            && (   (   this->rule()(Rule::Type::number_of_rounds_limited)
                    && roundno() >= this->rule()(Rule::Type::number_of_rounds)
                    && !last_game_summary().startplayer_stays()
                    && last_game_summary().next_startplayer() == first_startplayer_
                   )
                || (   this->rule()(Rule::Type::points_limited)
                    && remaining_points() <= 0)
               ) ) {
          duty_soli_round_ = roundno();
        }
        add_game_summary(std::move(gs));
        gameno += 1;
      }

      // We have to check for a new round because of the summary window after loading a party
      // check for a new round
      if (   gameno > 0
          && last_game_summary().startplayer()      != first_startplayer_
          && last_game_summary().next_startplayer() == first_startplayer_
          && !is_duty_soli_round()
          && !is_finished()) {
        round_startgame_.push_back(finished_games());
      }
      if (!is_duty_soli_round()
          && (   (   this->rule()(Rule::Type::number_of_rounds_limited)
                  && roundno() >= this->rule()(Rule::Type::number_of_rounds))
              || (   this->rule()(Rule::Type::points_limited)
                  && remaining_points() <= 0)
             ) ) {
        duty_soli_round_ = roundno();
      }

      if (is_duty_soli_round()) {
        if (   !game_summaries_.empty()
            && !last_game_summary().is_duty_solo()
            && !round_startgame_.empty()
            && round_startgame_.back() != this->gameno()
           ) {
          round_startgame_.push_back(this->gameno());
        }
      }
    }

    // we want to start the following game
    if (   !last_game_summary().startplayer_stays()
        || (   is_duty_soli_round()
            && last_game_summary().is_duty_solo())) {
      (startplayer_ += 1) %= players_->size();
    }
    next_seed();
  }

  return (istr.good() || istr.eof());
}


auto Party::save(Path const path) const -> bool
{
  auto path_tmp = path;
  path_tmp += ".tmp";
  ofstream ostr(path_tmp);
  if (!ostr.good()) {
    ::ui->information(_("Error::Party::save: Error opening temporary file %s. Aborting saving.", path_tmp.string()), InformationType::problem);
    return false;
  }

  ostr
    << "#!FreeDoko\n"
    << "# FreeDoko party (version " << *::version << ")\n"
    << '\n';
  write(ostr);

  if (!ostr.good()) {
    ::ui->information(_("Error::Party::save: Error saving in temporary file %s. Keeping temporary file (for bug tracking).", path_tmp.string()), InformationType::problem);
    return false;
  }
  ostr.close();

  try {
    if (is_regular_file(path))
      remove(path);
    rename(path_tmp, path);
  } catch (std::filesystem::filesystem_error const& error) {
    ::ui->information(_("Error::Party::save: Could not rename temporary file %s to requested file %s. Keeping temporary file.", path_tmp.string(), path.string())
                      + "\n"
                      + _("Error message: %s", error.what()),
                      InformationType::problem);
    return false;
  } catch(...) {
    ::ui->information(_("Error::Party::save: Could not rename temporary file %s to requested file %s. Keeping temporary file.", path_tmp.string(), path.string()), InformationType::problem);
    return false;
  }

  return true;
}


auto Party::load(Path const path) -> bool
{
  try {
    ifstream istr(path);
    if (istr.fail())
      throw ReadException("Party load: open failed");

    read(istr);

    if (!istr.good() && !istr.eof())
      throw ReadException("Party load: unknown error");
  } catch (ReadException const& exception) {
    ::ui->information(_("Message::party load: load error '%s'",
                        path.string())
                      + '\n'
                      + exception.message(),
                      InformationType::problem);
    return false;
  }

  return true;
}


#ifndef OUTDATED
auto Party::load_players() -> bool
{
  bool loaded_fail = false;
  for (auto const& p : *players_) {
    auto const n = players_->no(p);
    auto player = Player::create(::preferences.config_directory() / ("player." + String::to_string(n + 1)));
    if (player)
      players_->set(n, std::move(player));
    else
      loaded_fail = true;
  }

  return !loaded_fail;
}
#endif


void Party::reset()
{
  for (auto const& player : *players_) {
    ::ui->name_changed(player);
  }

  if (ui->type() == UIType::dummy) {
    for (unsigned p = 0; p < players_->size(); ++p) {
      if (players_->player(p).type() == Player::Type::human)
        players_->set_type(p, Player::Type::ai);
    }
  }


  // reset the points
  points_ = 0;
  duty_soli_round_.reset();

  // save the starting values
  first_seed_ = seed_;
  first_startplayer_ = startplayer_;


  pointsums_for_players_.clear();
  pointsums_for_players_.resize(players_->size());
}


void Party::open()
{
  ::ui->party_open(*this);
  Party::signal_open(*this);
}


void Party::play()
{
  status_ = Status::play;
  if (seed() == UINT_MAX)
    set_seed(seed());
  if (startplayer() == UINT_MAX)
    set_startplayer(startplayer());
  recalc_pointsums_for_players();

  signal_start();

  try {
    bool start_new_round = round_startgame_.empty();
    // Standardspiele
    while (!normal_games_finished()) {
      if (start_new_round) {
        round_startgame_.push_back(finished_games());
        signal_start_round(roundno());
        start_new_round = false;
      }


      auto_save();
      DEBUG("party") << "round " << roundno() << ", game " << gameno() << '\n';
      play_game();

      next_seed();

      if (::fast_play & FastPlay::seed_info)
        continue;

      if (!last_game_summary().startplayer_stays()) {
        (startplayer_ += 1) %= players().size();
        if (startplayer() == first_startplayer_)
          start_new_round = true;
      }

      if (rule()(Rule::Type::mutate)) {
        rule_->mutate();
      }
    } // while (!normal_games_finished())

    if (remaining_duty_soli() > 0 && !duty_soli_round_) {
      round_startgame_.push_back(finished_games());
      duty_soli_round_ = roundno();
      signal_start_round(roundno());
    }

    while (remaining_duty_soli() > 0) {
      // search the next player to play a duty solo
      while (!remaining_duty_soli(startplayer()))
        (startplayer_ += 1) %= players().size();

      auto_save();
      play_game();

      next_seed();

      if (last_game_summary().is_duty_solo()) {
        (startplayer_ += 1) %= players().size();
      }
    } // while (remaining_duty_soli() > 0)

    status_ = Party::Status::finished;
  } catch (Status const& status) {
    if (status != Party::Status::finished)
      throw;

    status_ = Party::Status::finished;
  }
}


auto Party::create_game() -> Game&
{
  game_ = make_unique<Game>(*this, players_->player(startplayer()));
  return *game_;
}


// NOLINTNEXTLINE(misc-no-recursion)
void Party::play_game()
{
  // create a new game
  create_game();
  try { // play the game
    game().play();
    if (::fast_play & FastPlay::seed_info) {
      if (seed_ == first_seed_ + 10000 - 1) {
        cout << std::flush;
        throw ProgramFlowException::Quit();
      }
      game().close();
      game_.reset();
      status_ = Party::Status::play;
      return ;
    }

    DEBUG_ASSERTION((status() == Party::Status::play),
                    "Party::play():\n"
                    "  wrong gamestatus (needs 'game_play'): "
                    << status());

    game().finish();

    if (!is_replayed_game()) {
      auto game_summary = make_unique<GameSummary>(game());
      auto const gs = game_summary.get();
      add_game_summary(std::move(game_summary));
      for (auto& p : *players_)
        p.db_->evaluate(p, *gs);

#ifndef RELEASE
      if (::debug("jack solo") && gs->game_type() == GameType::solo_jack) {
        static unsigned n = 0;
        n += 1;
        cout << n << ";" << seed() << ";" << gs->trick_points(Team::re) << ";" << gs->points(Team::re) / 3 << '\n';
        //cout << n << '\t' << gs->trick_points(Team::re) << '\t' << gs->points(Team::re) / 3 << '\n';
        //cout << gs->trick_points(Team::re) << '\n';
#if 0
        if (n == 98) {
          BugReport::create_bug_report(*this);
          exit(0);
        }
#endif
      }
#endif
    }

    auto_save();

    game().signal_finish();
    ::ui->game_finish();
  } catch (...) { // play the game
    game().close();
    game_.reset();
    throw;
  }

  if (replay_game_) {
    hands_for_replay_.resize(game().playerno());
    for (auto const& p : game().players()) {
      hands_for_replay_[p.no()] = Hand(p.hand().cards_all());
    }
    if (game().type() == GameType::poverty) {
      auto& poverty_hand = hands_for_replay_[game().players().soloplayer().no()];
      poverty_hand.remove(game().poverty().returned_cards());
      poverty_hand.add(game().poverty().shifted_cards());
      auto& poverty_partner_hand = hands_for_replay_[game().players().poverty_partner().no()];
      poverty_partner_hand.add(game().poverty().returned_cards());
      poverty_partner_hand.remove(game().poverty().shifted_cards());
    }
  } else {
    hands_for_replay_.clear();
  }

  game().close();
#ifdef CHECK_RUNTIME
  cout << '\n' << ::runtime << endl;
#endif

  game_.reset();

  status_ = Party::Status::play;
  if (replay_game_) {
    replay_game_ = false;
    is_replayed_game_ = true;
    play_game();
    is_replayed_game_ = false;
  }
}


void Party::close()
{
  ::ui->party_close();
  signal_close();

  round_startgame_.clear();
  finished_normal_games_ = 0;
  random_seed_           = true;
  first_seed_            = 0;
  random_startplayer_    = false;
  first_startplayer_     = 0;

  game_summaries_       .clear();
  bock_multipliers_     .clear();
  pointsums_for_players_.clear();

  // remove all connections
  signal_get_settings       .disconnect_all();
  signal_loaded             .disconnect_all();
  signal_start              .disconnect_all();
  signal_start_round        .disconnect_all();
  signal_finish             .disconnect_all();
  signal_close              .disconnect_all();
  signal_seed_changed       .disconnect_all();
  signal_startplayer_changed.disconnect_all();
}


void Party::rule_changed(int const type, void const* const old_value)
{
  if (   type == Rule::Type::counting
      || type == Rule::Type::solo_always_counts_triple) {
    recalc_pointsums_for_players();
  }
  for (auto& player : *players_)
    player.rule_changed(type, old_value);
}


auto Party::auto_save() const -> bool
{
  if (!::preferences(Preferences::Type::save_party_changes))
    return false;
  auto const success = save(::preferences.config_directory() / "party");
  if (!success) {
    ::preferences.set(Preferences::Type::save_party_changes, false);
  }
  return success;
}


auto operator<<(ostream& ostr, Party const& party) -> ostream&
{
  party.write(ostr);
  return ostr;
}
