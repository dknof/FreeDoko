/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "status.h"
#include "players.h"
#include "../player/player.h"
#include "../card/hand.h"
class Rule;
class GamePointsTable;
class GameSummary;
class Human;


class Party;
extern unique_ptr<Party> party; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

using PartyPlayer = Player;

class Party {
  friend class Game;
public:
  using Status  = PartyStatus;
  using Player  = PartyPlayer;
  using Players = PartyPlayers;
  using Points  = int;

  Party();
  Party(Party const&) = delete;
  auto operator=(Party const&) = delete;

  auto status() const noexcept -> Status;
  void set_status(Status status);

  auto rule() const noexcept -> Rule const&;
  auto rule()       noexcept -> Rule&;

  auto players() const noexcept -> Players const&;
  auto players()       noexcept -> Players&;

  auto random_seed()       const noexcept -> bool;
  auto seed()              const noexcept -> Seed;
  void set_seed(Seed seed)       noexcept;
  void set_random_seed()         noexcept;
  auto next_seed()               noexcept -> Seed;

  auto random_startplayer()                  const noexcept -> bool;
  auto startplayer()                         const noexcept -> unsigned;
  void set_startplayer(unsigned startplayer)       noexcept;
  void set_random_startplayer()                    noexcept;

  auto points()  const noexcept -> Points;

  auto in_game() const noexcept -> bool;
  auto game()    const noexcept -> Game const&;
  auto game()          noexcept -> Game&;

  void set_replay_game()        noexcept;
  auto is_replayed_game() const noexcept -> bool;
  auto hands_for_replay() const          -> Hands const&;

  // whether the party can be configured (seed, startplayer)
  auto in_configuration() const noexcept -> bool;

  auto roundno()                          const noexcept -> unsigned;
  auto remaining_rounds()                 const          -> unsigned;
  auto remaining_normal_games()           const          -> unsigned;
  auto remaining_games()                  const          -> unsigned;
  auto gameno()                           const noexcept -> unsigned;
  auto finished_games()                   const noexcept -> unsigned;
  auto is_duty_soli_round()               const          -> bool;
  auto is_duty_soli_round(unsigned round) const          -> bool;
  auto is_last_game()                     const          -> bool;
  auto normal_games_finished()            const          -> bool;
  auto is_finished()                      const          -> bool;

  // the remaining points for the party
  auto remaining_points() const noexcept -> Points;

  auto starts_new_round(unsigned gameno) const noexcept -> bool;
  auto round_of_game(unsigned gameno)    const          -> unsigned;
  auto round_startgame(unsigned roundno) const noexcept -> unsigned;

  void add_game_summary(unique_ptr<GameSummary> game_summary);
  auto last_game_summary()           const noexcept -> GameSummary const&;
  auto game_summary(unsigned gameno) const          -> GameSummary const&;

  auto bock_multipliers()               const noexcept -> vector<unsigned> const&;
  auto current_bock_multiplier()        const          -> unsigned;
  auto bock_multiplier(unsigned gameno) const noexcept -> unsigned;


  auto pointsum()                                               const -> Points;
  auto pointsum_till_game(unsigned gameno)                      const -> Points;
  auto pointsum_till_round(unsigned roundno)                    const -> Points;
  auto pointsum_in_round(unsigned roundno)                      const -> Points;
  auto pointsum(Player const& player)                           const -> Points;
  auto pointsum(unsigned playerno)                              const -> Points;
  auto pointsum_till_game(unsigned gameno,   unsigned playerno) const -> Points;
  auto pointsum_till_round(unsigned roundno, unsigned playerno) const -> Points;
  auto pointsum_in_round(unsigned roundno,   unsigned playerno) const -> Points;
private:
  void recalc_pointsums_for_players();
public:

  auto rang(unsigned playerno)    const -> unsigned;
  auto rang(Player const& player) const -> unsigned;

  auto played_duty_soli(unsigned playerno)            const -> unsigned;
  auto played_duty_soli(Player const& player)         const -> unsigned;
  auto played_duty_color_soli(unsigned playerno)      const -> unsigned;
  auto played_duty_color_soli(Player const& player)   const -> unsigned;
  auto played_duty_picture_soli(unsigned playerno)    const -> unsigned;
  auto played_duty_picture_soli(Player const& player) const -> unsigned;
  auto played_duty_free_soli(unsigned playerno)       const -> unsigned;
  auto played_duty_free_soli(Player const& player)    const -> unsigned;
  auto played_lust_soli(unsigned playerno)            const -> unsigned;
  auto played_lust_soli(Player const& player)         const -> unsigned;

  auto remaining_duty_soli()                             const -> unsigned;
  auto remaining_duty_soli(unsigned playerno)            const -> unsigned;
  auto remaining_duty_soli(Player const& player)         const -> unsigned;
  auto remaining_duty_color_soli(unsigned playerno)      const -> unsigned;
  auto remaining_duty_color_soli(Player const& player)   const -> unsigned;
  auto remaining_duty_picture_soli(unsigned playerno)    const -> unsigned;
  auto remaining_duty_picture_soli(Player const& player) const -> unsigned;
  auto remaining_duty_free_soli(unsigned playerno)       const -> unsigned;
  auto remaining_duty_free_soli(Player const& player)    const -> unsigned;

  void write(ostream& ostr) const;
  void write_pointstable(ostream& ostr) const;
  auto read(istream& istr) -> bool;

  auto save(Path filename) const -> bool;
  auto load(Path filename)       -> bool;
#ifndef OUTDATED
  // 0.7.21: Ersetzt mittels durchgehendes Speichern des gesamten Turniers in ~/.FreeDoko/party
  auto load_players() -> bool;
#endif


  void open();
  void reset();
  void play();
  auto create_game() -> Game&;
  void play_game();
  void close();

  void rule_changed(int type, void const* old_value);

  auto auto_save() const -> bool;

private:
  void increment_round();

private:
  Status status_ = Status::programstart;
  unique_ptr<Rule> rule_;
  unique_ptr<Players> players_;
  bool random_seed_               = true;
  Seed first_seed_                = 0;
  bool random_startplayer_        = false;
  unsigned first_startplayer_     = 0;
  vector<unsigned> round_startgame_;
  unsigned finished_normal_games_ = 0;

  optional<unsigned> duty_soli_round_;

  Points points_         = 0;
  unique_ptr<Game> game_;
  Seed seed_             = 0;
  unsigned startplayer_  = 0;
  bool replay_game_      = false;
  Hands hands_for_replay_;
  bool is_replayed_game_ = false;

  vector<unique_ptr<GameSummary>> game_summaries_;
  vector<unsigned> bock_multipliers_;
  vector<vector<Points>> pointsums_for_players_;

  // set_ai_players
  // partie typ (tutorial...)
  // pflichtstsoli
  // punkte topf

public:
  static Signal<void(Party&)> signal_open; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
  Signal<void()> signal_get_settings;
  Signal<void()> signal_loaded;
  Signal<void()> signal_start;
  Signal<void(unsigned)> signal_start_round;
  Signal<void()> signal_finish;
  Signal<void()> signal_close;
  Signal<void()> signal_seed_changed;
  Signal<void()> signal_startplayer_changed;
}; // class Party

auto operator<<(ostream& ostr, Party const& p) -> ostream&;
