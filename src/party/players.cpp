/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "players.h"
#include "party.h"
#include "rule.h"

#include "../player/human/human.h"
#include "../ui/ui.h"
#include "../misc/preferences.h"
#include "../utils/string.h"

PartyPlayers::PartyPlayers(Party& party) :
  party_(&party)
{
#ifdef WITH_UI_INTERFACE
  players_.push_back(Player::create(Player::Type::human, 0));
  while (size() < party.rule()(Rule::Type::number_of_players)) {
    players_.push_back(Player::create(Player::Type::ai, size()));
  }
#else
  while (size() < party.rule()(Rule::Type::number_of_players)) {
    players_.push_back(Player::create(Player::Type::ai_dummy, size()));
  }
#endif
}


PartyPlayers::PartyPlayers(Party& party, istream& istr) :
  party_(&party)
{
  while (istr.good()
         && istr.peek() != '}') {
    auto player = Player::create(istr);
    if (!player)
      throw ReadException("Party Players read: read of player failed");

    players_.push_back(std::move(player));

    // ignore empty lines
    while (   istr.peek() == '\n'
           || istr.peek() == '\r')
      istr.get();
  }
}


auto PartyPlayers::empty() const -> bool
{
  return players_.empty();
}


auto PartyPlayers::size() const -> unsigned
{
  return players_.size();
}


auto PartyPlayers::begin() -> iterator
{
  return PartyPlayers::iterator(*this, 0);
}


auto PartyPlayers::end() -> iterator
{
  return PartyPlayers::iterator(*this, size());
}


auto PartyPlayers::begin() const -> const_iterator
{
  return PartyPlayers::const_iterator(*this, 0);
}


auto PartyPlayers::end() const -> const_iterator
{
  return PartyPlayers::const_iterator(*this, size());
}


auto PartyPlayers::player(unsigned const p) -> Player&
{
  DEBUG_ASSERTION(p < players_.size(),
                  "PartyPlayers::player(p):\n"
                  "'p' is not valid (" << p << ")");

  return *players_[p];
}


auto PartyPlayers::player(unsigned const p) const -> Player const&
{
  DEBUG_ASSERTION(p < players_.size(),
                  "PartyPlayers::player(p):\n"
                  "'p' is not valid (" << p << ")");

  return *players_[p];
}


auto PartyPlayers::operator[](unsigned const p) -> Player&
{
  DEBUG_ASSERTION(p < players_.size(),
                  "PartyPlayers::player(p):\n"
                  "'p' is not valid (" << p << ")");

  return *players_[p];
}


auto PartyPlayers::operator[](unsigned const p) const -> Player const&
{
  DEBUG_ASSERTION(p < players_.size(),
                  "PartyPlayers::player(p):\n"
                  "'p' is not valid (" << p << ")");

  return *players_[p];
}


auto PartyPlayers::player(Person const& person) const noexcept -> Player const&
{
  for (auto const& player : players_)
    if (&player->person() == &person)
      return *player;

  DEBUG_ASSERTION(false,
                  "PartyPlayers::player(person = " << person.name() << ")\n"
                  "   player not found");
}


auto PartyPlayers::no(Person const& person) const noexcept -> unsigned
{
  for (size_t p = 0; p < players_.size(); ++p)
    if (&players_[p]->person() == &person)
      return p;

  return UINT_MAX;
}


auto PartyPlayers::no(Player const& player) const -> unsigned
{
  for (unsigned p = 0; p < size(); ++p)
    if (&this->player(p) == &player)
      return p;
  return UINT_MAX;
}


auto PartyPlayers::count_humans() const -> unsigned
{
  return count_if(players_,
                  [](auto const& player) { return player->type() == Player::Type::human; });
}


auto PartyPlayers::human_player() -> Human*
{
  if (count_humans() != 1)
    return {};

  return dynamic_cast<Human*>(find_if(players_,
                                      [](auto const& player) { return player->type() == Player::Type::human; }
                                     )->get());
}


auto PartyPlayers::human_player() const -> Human const*
{
  if (count_humans() != 1)
    return nullptr;

  return dynamic_cast<Human*>(find_if(players_,
                                      [](auto const& player) { return player->type() == Player::Type::human; }
                                     )->get());
}


void PartyPlayers::set(unsigned const n, unique_ptr<Player> player)
{
  DEBUG_ASSERTION(n < players_.size(),
                  "PartyPlayers::set(n, player):\n"
                  "'n' is not valid");

  DEBUG_ASSERTION(!party_->in_game(),
                  "PartyPlayers::set_player(n, player):\n"
                  "  in a game.");

  players_[n] = std::move(player);

  ::ui->player_changed(this->player(n));
  signal_player_changed(this->player(n));
}


void PartyPlayers::swap(unsigned const n0, unsigned const n1)
{
  DEBUG_ASSERTION(n0 < players_.size(),
                  "PartyPlayers::swap_players(n0, n1):\n"
                  "'n0' is not valid");

  DEBUG_ASSERTION(n1 < players_.size(),
                  "PartyPlayers::swap_players(n0, n1):\n"
                  "'n1' is not valid");

  DEBUG_ASSERTION(!party_->in_game(),
                  "PartyPlayers::swap_players(n0, n1):\n"
                  "  in a game.");

  // first set the names, so that empty names are replaced by the default names
  static_cast<Person&>(player(n0)).set_name(player(n0).name());
  static_cast<Person&>(player(n1)).set_name(player(n1).name());
  std::swap(players_[n0], players_[n1]);
  ::ui->players_switched(player(n0), player(n1));
  signal_players_swapped(player(n0), player(n1));
}


void PartyPlayers::set_type(Player& player, Player::Type const new_type)
{
  set_type(no(player), new_type);
}


void PartyPlayers::set_type(unsigned const player_no, Player::Type const new_type)
{
  DEBUG_ASSERTION(player_no < players_.size(),
                  "PartyPlayers::set_type(): "
                  << "  Illegal Value for player number: "
                  << player_no);

  DEBUG_ASSERTION(players_[player_no],
                  "PartyPlayers::set_type(): "
                  << "  player " << player_no << " does not exists");

  auto const& player = this->player(player_no);

  if (player.type() == new_type)
    return ;

  bool const default_name = (Person::default_name(player.type(), player_no) == player.name());
  // create the new player
  set(player_no, Player::create(new_type, player));

  if (default_name) {
    static_cast<Person&>(this->player(player_no)).set_default_name();
  }
}


void PartyPlayers::change_type(Player::Type const type_from, Player::Type const type_to)
{
  auto ui_bak = std::move(::ui);
  ::ui = UI::create(UIType::dummy);
  for (auto& player : *this) {
    if (player.type() == type_from)
      set_type(player, type_to);
  }
  ::ui = std::move(ui_bak);
}


void PartyPlayers::write(ostream& ostr) const
{
  ostr << "players\n"
    << "{\n";
  for (auto const& player : players_)
    ostr << "{\n" << *player << "}\n\n";
  ostr << "}\n";
}


void PartyPlayers::write_short(ostream& ostr) const
{
  ostr << "party players:\n";
  for (unsigned p = 0; p < players_.size(); ++p) {
    ostr << p << ": ";
    player(p).write_short(ostr);
    ostr << '\n';
  }
}

#ifndef OUTDATED
// 0.7.21: Ersetzt mittels durchgehendes Speichern des gesamten Turniers in ~/.FreeDoko/party
auto PartyPlayers::load() -> bool
{
  bool loaded_fail = false;
  for (unsigned p = 0; p < size(); ++p) {
    auto player = Player::create(::preferences.config_directory()
                                 / ("player." + String::to_string(p + 1)));
    if (player)
      set(p, std::move(player));
    else
      loaded_fail = true;
  } // for (p < size())

  return !loaded_fail;
}


void PartyPlayers::save() const
{
  unsigned n = 0;
  for (auto const& p : players_) {
    ++n;
    p->save(::preferences.config_directory()
            / ("player." + String::to_string(n)));
  } // for (p : players_)
}
#endif
