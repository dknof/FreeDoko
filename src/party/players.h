/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../player/player.h"
class Party;
using PartyPlayer = Player;
class Human;
#include "players_iterator.h"

/**
 ** The players of a party
 **/
class PartyPlayers {
public:
  using Player = PartyPlayer;
  using iterator = PartyPlayersIterator;
  using const_iterator = PartyPlayersConstIterator;

  explicit PartyPlayers(Party& party);
  PartyPlayers(Party& party, istream& istr);

  PartyPlayers() = delete;
  PartyPlayers(PartyPlayers const&) = delete;
  auto operator=(PartyPlayers const&) = delete;

  auto empty() const -> bool;
  auto size()  const -> unsigned;

  auto begin()       -> iterator;
  auto end()         -> iterator;
  auto begin() const -> const_iterator;
  auto end()   const -> const_iterator;

  auto player(unsigned p)           -> Player&;
  auto player(unsigned p)     const -> Player const&;
  auto operator[](unsigned p)       -> Player&;
  auto operator[](unsigned p) const -> Player const&;
  // the position of a player
  auto no(Player const& player) const -> unsigned;

  auto player(Person const& person) const noexcept -> Player const&;
  auto no(Person const& person)     const noexcept -> unsigned;

  auto count_humans() const -> unsigned;
  auto human_player()       -> Human*;
  auto human_player() const -> Human const*;

  void set(unsigned n, unique_ptr<Player> player);
  void swap(unsigned n0, unsigned n1);

  void set_type(Player& player, Player::Type new_type);
  void set_type(unsigned n, Player::Type new_type);
  void change_type(Player::Type type_from, Player::Type type_to);

  void write(ostream& ostr) const;
  void write_short(ostream& ostr) const;

#ifndef OUTDATED
  auto load() -> bool;
  void save() const;
#endif

private:
  Party* const party_;
  vector<unique_ptr<Player>> players_;

public:
  Signal<void(Player const&, Player const&)> signal_players_swapped;
  Signal<void(Player const&)> signal_player_changed;
}; // class PartyPlayers
