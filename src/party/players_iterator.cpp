/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "players.h"

PartyPlayersIterator::PartyPlayersIterator(PartyPlayers& players,
                                           unsigned p) :
  players_(&players),
  p_(p)
{ }


auto PartyPlayersIterator::operator*() -> PartyPlayer&
{
  return players_->player(p_);
}

auto PartyPlayersIterator::operator*() const -> PartyPlayer const&
{
  return players_->player(p_);
}


auto PartyPlayersIterator::operator->() -> PartyPlayer*
{
  return &(**this);
}


auto PartyPlayersIterator::operator->() const -> PartyPlayer const*
{
  return &(**this);
}


auto PartyPlayersIterator::operator++() -> PartyPlayersIterator&
{
  p_ += 1;
  return *this;
}


auto PartyPlayersIterator::operator!=(PartyPlayersIterator const& i) const -> bool
{
  return (   (p_ != i.p_)
          || (players_ != i.players_));
}


PartyPlayersConstIterator::PartyPlayersConstIterator(PartyPlayers const& players,
                                                     unsigned p) :
  players_(&players),
  p_(p)
{ }

auto PartyPlayersConstIterator::operator*() const -> PartyPlayer const&
{
  return players_->player(p_);
}


auto PartyPlayersConstIterator::operator->() const -> PartyPlayer const*
{
  return &(**this);
}


auto PartyPlayersConstIterator::operator++() -> PartyPlayersConstIterator&
{
  p_ += 1;
  return *this;
}


auto PartyPlayersConstIterator::operator!=(PartyPlayersConstIterator const& i) const -> bool
{
  return (   p_ != i.p_
          || players_ != i.players_);
}
