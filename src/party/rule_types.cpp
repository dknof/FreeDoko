/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "rule_types.h"

/** -> result
 **
 ** @param    type   rule type
 **
 ** @return   the name of 'type'
 **/
string
to_string(RuleType::Bool const type)
{
  switch(type) {
  case RuleType::mutate:
    (void)_("Rule::mutate");
    (void)_("Rule::Description::mutate");
    return "mutate";
  case RuleType::with_nines:
    (void)_("Rule::with nines");
    (void)_("Rule::Description::with nines");
    return "with nines";
  case RuleType::bock:
    (void)_("Rule::bock");
    (void)_("Rule::Description::bock");
    return "bock";
  case RuleType::bock_append:
    (void)_("Rule::append bock");
    (void)_("Rule::Description::append bock");
    return "append bock";
  case RuleType::bock_120:
    (void)_("Rule::bock after 120-120");
    (void)_("Rule::Description::bock after 120-120");
    return "bock after 120-120";
  case RuleType::bock_solo_lost:
    (void)_("Rule::bock after lost solo");
    (void)_("Rule::Description::bock after lost solo");
    return "bock after lost solo";
  case RuleType::bock_re_lost:
    (void)_("Rule::bock after lost re/contra");
    (void)_("Rule::Description::bock after lost re/contra");
    return "bock after lost re/contra";
  case RuleType::bock_heart_trick:
    (void)_("Rule::bock after heart trick");
    (void)_("Rule::Description::bock after heart trick");
    return "bock after heart trick";
  case RuleType::bock_black:
    (void)_("Rule::bock after black game");
    (void)_("Rule::Description::bock after black game");
    return "bock after black game";
  case RuleType::throw_with_nines:
    (void)_("Rule::throw with nines");
    (void)_("Rule::Description::throw with nines");
    return "throw with nines";
  case RuleType::throw_with_kings:
    (void)_("Rule::throw with kings");
    (void)_("Rule::Description::throw with kings");
    return "throw with kings";
  case RuleType::throw_with_nines_and_kings:
    (void)_("Rule::throw with nines and kings");
    (void)_("Rule::Description::throw with nines and kings");
    return "throw with nines and kings";
  case RuleType::throw_with_richness:
    (void)_("Rule::throw with richness");
    (void)_("Rule::Description::throw with richness");
    return "throw with richness";
  case RuleType::poverty:
    (void)_("Rule::poverty");
    (void)_("Rule::Description::poverty");
    return "poverty";
  case RuleType::poverty_shift:
    (void)_("Rule::poverty shift");
    (void)_("Rule::Description::poverty shift");
    return "poverty shift";
  case RuleType::poverty_shift_only_trump:
    (void)_("Rule::poverty shift only trump");
    (void)_("Rule::Description::poverty shift only trump");
    return "poverty shift only trump";
  case RuleType::poverty_fox_do_not_count:
    (void)_("Rule::poverty fox do not count");
    (void)_("Rule::Description::poverty fox do not count");
    return "poverty fox do not count";
  case RuleType::poverty_fox_shift_extra:
    (void)_("Rule::poverty fox shift extra");
    (void)_("Rule::Description::poverty fox shift extra");
    return "poverty fox shift extra";
  case RuleType::poverty_fox_shift_open:
    (void)_("Rule::poverty fox shift open");
    (void)_("Rule::Description::poverty fox shift open");
    return "poverty fox shift open";
  case RuleType::throw_with_one_trump:
    (void)_("Rule::throw with one trump");
    (void)_("Rule::Description::throw with one trump");
    return "throw with one trump";
  case RuleType::throw_when_fox_highest_trump:
    (void)_("Rule::throw when fox highest trump");
    (void)_("Rule::Description::throw when fox highest trump");
    return "throw when fox highest trump";
  case RuleType::marriage_first_foreign:
    (void)_("Rule::marriage first foreign");
    (void)_("Rule::Description::marriage first foreign");
    return "marriage first foreign";
  case RuleType::marriage_first_color:
    (void)_("Rule::marriage first color");
    (void)_("Rule::Description::marriage first color");
    return "marriage first color";
  case RuleType::marriage_first_single_color:
    (void)_("Rule::marriage first single color");
    (void)_("Rule::Description::marriage first single color");
    return "marriage first single color";
  case RuleType::marriage_first_trump:
    (void)_("Rule::marriage first trump");
    (void)_("Rule::Description::marriage first trump");
    return "marriage first trump";
  case RuleType::marriage_first_one_decides:
    (void)_("Rule::marriage first one decides");
    (void)_("Rule::Description::marriage first one decides");
    return "marriage first one decides";
  case RuleType::marriage_before_poverty:
    (void)_("Rule::marriage before poverty");
    (void)_("Rule::Description::marriage before poverty");
    return "marriage before poverty";
  case RuleType::solo:
    (void)_("Rule::solo");
    (void)_("Rule::Description::solo");
    return "solo";
  case RuleType::solo_jack:
    (void)_("Rule::solo jack");
    (void)_("Rule::Description::solo jack");
    return "solo jack";
  case RuleType::solo_queen:
    (void)_("Rule::solo queen");
    (void)_("Rule::Description::solo queen");
    return "solo queen";
  case RuleType::solo_king:
    (void)_("Rule::solo king");
    (void)_("Rule::Description::solo king");
    return "solo king";
  case RuleType::solo_queen_jack:
    (void)_("Rule::solo queen jack");
    (void)_("Rule::Description::solo queen jack");
    return "solo queen jack";
  case RuleType::solo_king_jack:
    (void)_("Rule::solo king jack");
    (void)_("Rule::Description::solo king jack");
    return "solo king jack";
  case RuleType::solo_king_queen:
    (void)_("Rule::solo king queen");
    (void)_("Rule::Description::solo king queen");
    return "solo king queen";
  case RuleType::solo_koehler:
    (void)_("Rule::solo koehler");
    (void)_("Rule::Description::solo koehler");
    return "solo koehler";
  case RuleType::solo_color:
    (void)_("Rule::solo color");
    (void)_("Rule::Description::solo color");
    return "solo color";
  case RuleType::solo_meatless:
    (void)_("Rule::solo meatless");
    (void)_("Rule::Description::solo meatless");
    return "solo meatless";
  case RuleType::throwing_before_solo:
    (void)_("Rule::throwing before solo");
    (void)_("Rule::Description::throwing before solo");
    return "throwing before solo";
  case RuleType::dullen:
    (void)_("Rule::dullen");
    (void)_("Rule::Description::dullen");
    return "dullen";
  case RuleType::dullen_second_over_first:
    (void)_("Rule::dullen second over first");
    (void)_("Rule::Description::dullen second over first");
    return "dullen second over first";
  case RuleType::dullen_contrary_in_last_trick:
    (void)_("Rule::dullen contrary in last trick");
    (void)_("Rule::Description::dullen contrary in last trick");
    return "dullen contrary in last trick";
  case RuleType::dullen_first_over_second_with_swines:
    (void)_("Rule::dullen first over second with swines");
    (void)_("Rule::Description::dullen first over second with swines");
    return "dullen first over second with swines";
  case RuleType::swines:
    (void)_("Rule::swines");
    (void)_("Rule::Description::swines");
    return "swines";
  case RuleType::swines_announcement_begin:
    (void)_("Rule::swines announcement begin");
    (void)_("Rule::Description::swines announcement begin");
    return "swines announcement begin";
  case RuleType::swine_only_second:
    (void)_("Rule::swine only second");
    (void)_("Rule::Description::swine only second");
    return "swine only second";
  case RuleType::swines_in_solo:
    (void)_("Rule::swines in solo");
    (void)_("Rule::Description::swines in solo");
    return "swines in solo";
  case RuleType::swines_in_poverty:
    (void)_("Rule::swines in poverty");
    (void)_("Rule::Description::swines in poverty");
    return "swines in poverty";
  case RuleType::hyperswines:
    (void)_("Rule::hyperswines");
    (void)_("Rule::Description::hyperswines");
    return "hyperswines";
  case RuleType::hyperswines_announcement_begin:
    (void)_("Rule::hyperswines announcement begin");
    (void)_("Rule::Description::hyperswines announcement begin");
    return "hyperswines announcement begin";
  case RuleType::hyperswines_in_solo:
    (void)_("Rule::hyperswines in solo");
    (void)_("Rule::Description::hyperswines in solo");
    return "hyperswines in solo";
  case RuleType::hyperswines_in_poverty:
    (void)_("Rule::hyperswines in poverty");
    (void)_("Rule::Description::hyperswines in poverty");
    return "hyperswines in poverty";
  case RuleType::swines_and_hyperswines_joint:
    (void)_("Rule::swines and hyperswines joint");
    (void)_("Rule::Description::swines and hyperswines joint");
    return "swines and hyperswines joint";
  case RuleType::extrapoint_catch_fox:
    (void)_("Rule::extrapoint catch fox");
    (void)_("Rule::Description::extrapoint catch fox");
    return "extrapoint catch fox";
  case RuleType::extrapoint_catch_fox_last_trick:
    (void)_("Rule::extrapoint catch fox last trick");
    (void)_("Rule::Description::extrapoint catch fox last trick");
    return "extrapoint catch fox last trick";
  case RuleType::extrapoint_fox_last_trick:
    (void)_("Rule::extrapoint fox last trick");
    (void)_("Rule::Description::extrapoint fox last trick");
    return "extrapoint fox last trick";
  case RuleType::extrapoint_double_fox_last_trick:
    (void)_("Rule::extrapoint double fox last trick");
    (void)_("Rule::Description::extrapoint double fox last trick");
    return "extrapoint double fox last trick";
  case RuleType::extrapoint_dulle_jabs_dulle:
    (void)_("Rule::extrapoint dulle jabs dulle");
    (void)_("Rule::Description::extrapoint dulle jabs dulle");
    return "extrapoint dulle jabs dulle";
  case RuleType::extrapoint_heart_trick:
    (void)_("Rule::extrapoint heart trick");
    (void)_("Rule::Description::extrapoint heart trick");
    return "extrapoint heart trick";
  case RuleType::extrapoint_charlie:
    (void)_("Rule::extrapoint charlie");
    (void)_("Rule::Description::extrapoint charlie");
    return "extrapoint charlie";
  case RuleType::extrapoint_catch_charlie:
    (void)_("Rule::extrapoint catch charlie");
    (void)_("Rule::Description::extrapoint catch charlie");
    return "extrapoint catch charlie";
  case RuleType::extrapoint_double_charlie:
    (void)_("Rule::extrapoint double charlie");
    (void)_("Rule::Description::extrapoint double charlie");
    return "extrapoint double charlie";
  case RuleType::extrapoint_catch_double_charlie:
    (void)_("Rule::extrapoint catch double charlie");
    (void)_("Rule::Description::extrapoint catch double charlie");
    return "extrapoint catch double charlie";
  case RuleType::extrapoint_catch_charlie_only_with_diamond_queen:
    (void)_("Rule::extrapoint catch charlie only with diamond queen");
    (void)_("Rule::Description::extrapoint catch charlie only with diamond queen");
    return "extrapoint catch charlie only with diamond queen";
  case RuleType::extrapoints_in_solo:
    (void)_("Rule::extrapoints in solo");
    (void)_("Rule::Description::extrapoints in solo");
    return "extrapoints in solo";
  case RuleType::announcements:
    (void)_("Rule::announcements");
    (void)_("Rule::Description::announcements");
    return "announcements";
  case RuleType::knocking:
    (void)_("Rule::knocking");
    (void)_("Rule::Description::knocking");
    return "knocking";
  case RuleType::announcement_individual_limits:
    (void)_("Rule::announcement individual limits");
    (void)_("Rule::Description::announcement individual limits");
    return "announcement individual limits";
  case RuleType::announcement_limit_only_for_current:
    (void)_("Rule::announcement limit only for current");
    (void)_("Rule::Description::announcement limit only for current");
    return "announcement limit only for current";
  case RuleType::announcement_till_full_trick:
    (void)_("Rule::announcement till full trick");
    (void)_("Rule::Description::announcement till full trick");
    return "announcement till full trick";
  case RuleType::announcement_first_trick_thirty_points:
    (void)_("Rule::announcement first trick thirty points");
    (void)_("Rule::Description::announcement first trick thirty points");
    return "announcement first trick thirty points";
  case RuleType::announcement_first_trick_thirty_points_only_first:
    (void)_("Rule::announcement first trick thirty points only first");
    (void)_("Rule::Description::announcement first trick thirty points only first");
    return "announcement first trick thirty points only first";
  case RuleType::announcement_first_trick_thirty_points_only_re_contra:
    (void)_("Rule::announcement first trick thirty points only re/contra");
    (void)_("Rule::Description::announcement first trick thirty points only re/contra");
    return "announcement first trick thirty points only re/contra";
  case RuleType::announcement_first_trick_thirty_points_in_marriage:
    (void)_("Rule::announcement first trick thirty points in marriage");
    (void)_("Rule::Description::announcement first trick thirty points in marriage");
    return "announcement first trick thirty points in marriage";
  case RuleType::announcement_re_doubles:
    (void)_("Rule::announcement re/contra doubles");
    (void)_("Rule::Description::announcement re/contra doubles");
    return "announcement re/contra doubles";
  case RuleType::announcement_contra_doubles_against_re:
    (void)_("Rule::announcement contra doubles against re");
    (void)_("Rule::Description::announcement contra doubles against re");
    return "announcement contra doubles against re";
  case RuleType::cowardliness_dokolounge:
    (void)_("Rule::cowardliness");
    (void)_("Rule::Description::cowardliness");
    return "cowardliness";
  case RuleType::number_of_rounds_limited:
    (void)_("Rule::number of rounds limited");
    (void)_("Rule::Description::number of rounds limited");
    return "number of rounds limited";
  case RuleType::points_limited:
    (void)_("Rule::points limited");
    (void)_("Rule::Description::points limited");
    return "points limited";
  case RuleType::offer_duty_solo:
    (void)_("Rule::offer duty solo");
    (void)_("Rule::Description::offer duty solo");
    return "offer duty solo";
  case RuleType::lustsolo_player_leads:
    (void)_("Rule::lustsolo player leads");
    (void)_("Rule::Description::lustsolo player leads");
    return "lustsolo player leads";
  case RuleType::solo_always_counts_triple:
    (void)_("Rule::solo always counts triple");
    (void)_("Rule::Description::solo always counts triple");
    return "solo always counts triple";
  } // switch(type)

  DEBUG_ASSERT(false);
  return {};
} // string to_string(RuleType::Bool type)

/** -> result
 **
 ** @param    type   rule type
 **
 ** @return   the name of 'type'
 **/
string
to_string(RuleType::Unsigned const type)
{
  switch(type) {
  case RuleType::bock_multiplier:
    (void)_("Rule::bock multiplier");
    (void)_("Rule::Description::bock multiplier");
    return "bock multiplier";
  case RuleType::min_number_of_throwing_nines:
    (void)_("Rule::min number of throwing nines");
    (void)_("Rule::Description::min number of throwing nines");
    return "min number of throwing nines";
  case RuleType::min_number_of_throwing_kings:
    (void)_("Rule::min number of throwing kings");
    (void)_("Rule::Description::min number of throwing kings");
    return "min number of throwing kings";
  case RuleType::min_number_of_throwing_nines_and_kings:
    (void)_("Rule::min number of throwing nines and kings");
    (void)_("Rule::Description::min number of throwing nines and kings");
    return "min number of throwing nines and kings";
  case RuleType::min_richness_for_throwing:
    (void)_("Rule::min richness for throwing");
    (void)_("Rule::Description::min richness for throwing");
    return "min richness for throwing";
  case RuleType::marriage_determination:
    (void)_("Rule::marriage determination");
    (void)_("Rule::Description::marriage determination");
    return "marriage determination";
  case RuleType::announcement_no_120:
    (void)_("Rule::announcement no 120");
    (void)_("Rule::Description::announcement no 120");
    return "announcement no 120";
  case RuleType::announcement_no_90:
    (void)_("Rule::announcement no 90");
    (void)_("Rule::Description::announcement no 90");
    return "announcement no 90";
  case RuleType::announcement_no_60:
    (void)_("Rule::announcement no 60");
    (void)_("Rule::Description::announcement no 60");
    return "announcement no 60";
  case RuleType::announcement_no_30:
    (void)_("Rule::announcement no 30");
    (void)_("Rule::Description::announcement no 30");
    return "announcement no 30";
  case RuleType::announcement_no_0:
    (void)_("Rule::announcement no 0");
    (void)_("Rule::Description::announcement no 0");
    return "announcement no 0";
  case RuleType::number_of_rounds:
    (void)_("Rule::number of rounds");
    (void)_("Rule::Description::number of rounds");
    return "number of rounds";
  case RuleType::points:
    (void)_("Rule::points");
    (void)_("Rule::Description::points");
    return "points";
  case RuleType::number_of_duty_soli:
    (void)_("Rule::number of duty soli");
    (void)_("Rule::Description::number of duty soli");
    return "number of duty soli";
  case RuleType::number_of_duty_color_soli:
    (void)_("Rule::number of duty color soli");
    (void)_("Rule::Description::number of duty color soli");
    return "number of duty color soli";
  case RuleType::number_of_duty_picture_soli:
    (void)_("Rule::number of duty picture soli");
    (void)_("Rule::Description::number of duty picture soli");
    return "number of duty picture soli";
  case RuleType::number_of_players:
    (void)_("Rule::number of players");
    (void)_("Rule::Description::number of players");
    return "number of players";
  case RuleType::number_of_players_in_game:
    (void)_("Rule::number of players in game");
    (void)_("Rule::Description::number of players in game");
    return "number of players in game";
  } // switch(type)

  DEBUG_ASSERT(false);
  return {};
} // string to_string(RuleType::Unsigned type)

/** -> result
 **
 ** @param    type   rule type
 **
 ** @return   the name of 'type'
 **/
string
to_string(RuleType::UnsignedExtra const type)
{
  switch(type) {
  case RuleType::number_of_same_cards:
    (void)_("Rule::number of same cards");
    return "number of same cards";
  case RuleType::number_of_card_colors:
    (void)_("Rule::number of card colors");
    return "number of card colors";
  case RuleType::number_of_card_tcolors:
    (void)_("Rule::number of card tcolors");
    return "number of card tcolors";
  case RuleType::number_of_card_values:
    (void)_("Rule::number of card values");
    return "number of card values";
  case RuleType::number_of_cards_per_value:
    (void)_("Rule::number of cards per value");
    return "number of card values";
  case RuleType::number_of_cards:
    (void)_("Rule::number of cards");
    return "number of cards";
  case RuleType::number_of_teams:
    (void)_("Rule::number of teams");
    return "number of teams";
  case RuleType::number_of_players_per_team:
    (void)_("Rule::number of players per team");
    return "number of players per team";
  case RuleType::number_of_games_per_round:
    (void)_("Rule::number of games per round");
    return "number of games per round";
  case RuleType::max_number_of_tricks_in_game:
    (void)_("Rule::maximal number of tricks in game");
    return "maximal number of tricks in game";
  case RuleType::number_of_tricks_in_game:
    (void)_("Rule::number of tricks in game");
    return "number of tricks in game";
  case RuleType::points_for_doppelkopf:
    (void)_("Rule::points for doppelkopf");
    return "points for doppelkopf";
  case RuleType::max_number_of_poverty_trumps:
    (void)_("Rule::max number of poverty trumps");
    return "max number of poverty trumps";
  } // switch(type)

  DEBUG_ASSERTION(false,
                  "Rule::to_string(" << static_cast<int>(type) << ")\n"
                  "  type = " << static_cast<int>(type) << " not in switch");
  return {};
} // string to_string(RuleType::UnsignedExtra type)

/** -> result
 **
 ** @param    type   rule type
 **
 ** @return   the name of 'type'
 **/
string
to_string(RuleType::Counting const type)
{
  (void)_("Rule::counting");
  (void)_("Rule::Description::counting");
  return "counting";
} // string to_string(RuleType::UnsignedExtra type)

/** -> result
 **
 ** @param    counting   the counting
 **
 ** @return   the name of 'counting'
 **/
string
to_string(Counting const counting)
{
  switch(counting) {
  case Counting::plus:
    (void)_("Counting::plus");
    (void)_("Counting::Description::plus");
    return "plus";
  case Counting::minus:
    (void)_("Counting::minus");
    (void)_("Counting::Description::minus");
    return "minus";
  case Counting::plusminus:
    (void)_("Counting::plus-minus");
    (void)_("Counting::Description::plus-minus");
    return "plus-minus";
  } // switch(counting)

  DEBUG_ASSERT(false);
  return {};
} // string to_string(Counting counting)

/** prints the name of 'type'
 **
 ** @param    ostr   output stream
 ** @param    type   type to print the name of
 **
 ** @return   output stream
 **/
ostream&
operator<<(ostream& ostr, RuleType::Bool const type)
{
  ostr << to_string(type);

  return ostr;
} // ostream& operator<<(ostream& ostr, RuleType::Bool type)

/** prints the name of 'type'
 **
 ** @param    ostr   output stream
 ** @param    type   type to print the name of
 **
 ** @return   output stream
 **/
ostream&
operator<<(ostream& ostr, RuleType::Unsigned const type)
{
  ostr << to_string(type);
  return ostr;
} // ostream& operator<<(ostream& ostr, RuleType::Unsigned type)

/** prints the name of 'type'
 **
 ** @param    ostr   output stream
 ** @param    type   type to print the name of
 **
 ** @return   output stream
 **/
ostream&
operator<<(ostream& ostr, RuleType::UnsignedExtra const type)
{
  ostr << to_string(type);
  return ostr;
} // ostream& operator<<(ostream& ostr, RuleType::UnsignedExtra type)

/** prints the name of 'type'
 **
 ** @param    ostr   output stream
 ** @param    type   type to print the name of
 **
 ** @return   output stream
 **/
ostream&
operator<<(ostream& ostr, RuleType::Counting type)
{
  ostr << to_string(type);
  return ostr;
} // ostream& operator<<(ostream& ostr, RuleType::Counting type)

/** prints the name of 'counting'
 **
 ** @param    ostr   output stream
 ** @param    counting   counting to print
 **
 ** @return   output stream
 **/
ostream&
operator<<(ostream& ostr, Counting const counting)
{
  ostr << to_string(counting);
  return ostr;
} // ostream& operator<<(ostream& ostr, Counting counting)

/** @param    type   type
 **
 ** @return   translation of the type
 **/
string
gettext(RuleType::Bool const type)
{
  return gettext("Rule::" + to_string(type));
} // string gettext(RuleType::Bool type)

/** @param    type   type
 ** @param    text   text to append
 **
 ** @return   translation of the type
 **/
string
gettext(RuleType::Bool const type, string const& text)
{ return gettext("Rule::" + to_string(type)
                 + (text.empty() ? text : " -- " + text));
}

/** @param    type   type
 **
 ** @return   translation of the type
 **/
string
gettext(RuleType::Unsigned const type)
{
  return gettext("Rule::" + to_string(type));
} // string gettext(RuleType::Unsigned type)

/** @param    type   type
 ** @param    text   text to append
 **
 ** @return   translation of the type
 **/
string
gettext(RuleType::Unsigned const type, string const& text)
{
  return gettext("Rule::"
                 + to_string(type)
                 + (text.empty() ? text : " -- " + text));
} // string gettext(RuleType::Unsigned type, string text)

/** @param    type   type
 **
 ** @return   translation of the type
 **/
string
gettext(RuleType::Counting const type)
{
  return gettext("Rule::" + to_string(type));
} // string gettext(RuleType::Counting type)

/** @param    counting   counting
 **
 ** @return   translation of the type
 **/
string
gettext(Counting const counting)
{
  return gettext("Counting::" + to_string(counting));
} // string gettext(Counting counting)
