/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "ai.h"

#include "../../game/game.h"
#include "../../party/rule.h"
#include "../../card/trick.h"


AiDummy::AiDummy(Name const name) :
Player(Player::Type::ai_dummy, name)
{ }


AiDummy::AiDummy(Player const& player) :
  Player(player)
{
  static_cast<Person&>(*this).set_type(Player::Type::ai_dummy);
}


AiDummy::AiDummy(Player const& player, Aiconfig const& aiconfig) :
  Player(player),
  Aiconfig(aiconfig)
{
  static_cast<Person&>(*this).set_type(Player::Type::ai_dummy);
}


auto AiDummy::clone() const -> unique_ptr<Player>
{
  return make_unique<AiDummy>(*this);
}


auto AiDummy::get_reservation() -> Reservation const&
{
  reservation() = Player::get_default_reservation();

  if (   game().is_duty_solo()
      && *this == game().startplayer()) {
    for (auto const t : game_type_list) {
      if (   is_remaining_duty_solo(t)
          && game().rule(t) ) {
        reservation().game_type = t;
        break;
      }
    }
  }

  return reservation();
}

auto AiDummy::poverty_shift() -> HandCards
{
  // the number of cards to shift
  unsigned const numberofpovertycards = hand().count_poverty_cards();
  // the cards to shift
  HandCards cards_to_shift(hand());

  // first add all poverty cards
  for (auto const& c : hand().cards())
    if (c.istrump())
      cards_to_shift.push_back(c);

  // second add all color cards in the order: ace, nine, king, ten
  vector<Card::Value> values = {Card::ace, Card::nine, Card::king, Card::ten};
  for (auto v = values.begin();
       (   v != values.end()
        && cards_to_shift.size() < numberofpovertycards);
       ++v)
    for (auto c = hand().cards().begin();
         (   c != hand().cards().end()
          && cards_to_shift.size() < numberofpovertycards);
         ++c)
      if (!c->istrump())
        if (c->value() == *v)
          cards_to_shift.push_back(*c);

  hand().remove(cards_to_shift);

  return cards_to_shift;
}


auto AiDummy::poverty_take_accept(unsigned cardno) -> bool
{
  return false;
}


auto AiDummy::poverty_cards_change(vector<Card> const& cards) -> HandCards
{
  DEBUG_ASSERTION(false,
                  "AiDummy::poverty_cards_change(cards)\n"
                  "  call of this function althoug the dummy ai should reject any poverty");

  return HandCards(hand(), cards);
}


void AiDummy::poverty_cards_get_back(vector<Card> const& cards)
{
  hand().add(cards);
}


auto AiDummy::card_get() -> HandCard
{
  // the first card -- play the highest card
  // if the ai can jab the trick: play the highest card
  // if the ai cannot get the trick: play the lowest card

  HandCard card;
  bool can_win = false;

  for (auto const& c : hand().cards()) {
    if (game().tricks().current().isvalid(c, hand())) {
      if (card.is_empty()) {
        card = c;
      } else { // if !(!card)
        bool const is_winnercard
          = (game().tricks().current().isempty()
             ? true
             : game().tricks().current().isjabbed(c)
            );
        can_win |= is_winnercard;

        if (game().tricks().current().isempty()) {
          // take the smallest card
          if (card.is_jabbed_by(c))
            card = c;
        } else if (game().tricks().current().islastcard()) {
          // if the player can win the trick take the smallest winnercard,
          // else take the smallest card.

          if (can_win
              && is_winnercard
              && c.is_jabbed_by(card))
            card = c;
          if (!can_win
              && !is_winnercard
              && c.is_jabbed_by(card))
            card = c;
        } else { // if !(is first or last card in trick)
          // if the player can win the trick take the greatest winnercard,
          // else take the smallest card.

          if (can_win
              && is_winnercard
              && card.is_jabbed_by(c))
            card = c;
          if (!can_win
              && !is_winnercard
              && c.is_jabbed_by(card))
            card = c;
        } // if !(is first or last card in trick)
      } // if !(!card)
    } // if (game().tricks().current().isvalid(c, hand()))
  } // for (c : hand().cards())

  // valid_cards.card(RAND(valid_cards.cardsnumber()))
  return card;
}


auto AiDummy::announcement_request() const -> Announcement
{
  // points of the own team
  unsigned points = 0;

  // count the points, the own team has made
  for (auto const& player : game().players()) {
    if (game().teaminfo().get(player) == team())
      points += game().points_of_player(player);
  }

  if (points == 240)
    return Announcement::no0;
  if (points > 240 - 30)
    return Announcement::no30;
  if (points > 240 - 60)
    return Announcement::no60;
  if (points > 240 - 90)
    return Announcement::no90;
  if (points > 240 - 120)
    return Announcement::no120;

  return Announcement::noannouncement;
}
