/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../player.h"
#include "../aiconfig.h"

/**
 ** dummy ai
 ** If the ai can jab the trick, take the highest card,
 ** else take the lowest.
 **/
class AiDummy : public Player, public Aiconfig {
public:
  AiDummy() = delete;
  explicit AiDummy(Name name);
  explicit AiDummy(Player const& player);
  AiDummy(Player const& player, Aiconfig const& aiconfig);
  AiDummy(AiDummy const& ai_dummy) = default;
  AiDummy& operator=(AiDummy const& ai_dummy) = default;
  auto clone() const -> unique_ptr<Player> override ;

  ~AiDummy() override = default;

  auto get_reservation()                                 -> Reservation const& override;
  auto poverty_shift()                                   -> HandCards          override;
  auto poverty_take_accept(unsigned cardno)              -> bool               override;
  auto poverty_cards_change(vector<Card> const& cards)   -> HandCards          override;
  void poverty_cards_get_back(vector<Card> const& cards)                       override;

  auto card_get()                   -> HandCard      override;
  auto announcement_request() const -> Announcement  override;
};
