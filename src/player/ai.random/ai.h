/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../player.h"
#include "../aiconfig.h"
#include "../../utils/random.h"

// Random ai_dummy (selects a random valid card
class AiRandom : public Player, public Aiconfig {
public:
  AiRandom() = delete;
  AiRandom(Name name);
  AiRandom(Player const& player);
  AiRandom(Player const& player, Aiconfig const& aiconfig);
  AiRandom(AiRandom const& ai_dummy) = default;
  AiRandom& operator=(AiRandom const& ai_dummy) = default;
  auto clone() const -> unique_ptr<Player> override;

  ~AiRandom() override = default;

  auto get_reservation()            -> Reservation const& override;
  auto card_get()                   -> HandCard override;
  auto announcement_request() const -> Announcement override;

  auto poverty_shift()                                   -> HandCards override;
  auto poverty_take_accept(unsigned cardno)              -> bool      override;
  auto poverty_cards_change(vector<Card> const& cards )  -> HandCards override;
  void poverty_cards_get_back(vector<Card> const& cards)              override;
};
