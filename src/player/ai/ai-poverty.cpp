/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "ai.h"

#include "../cards_information.h"
#include "../../party/rule.h"
#include "../../game/gameplay_actions.h"
#include "../../os/bug_report_replay.h"


auto Ai::poverty_shift() -> HandCards
{
  DEBUG_ASSERTION(hand().count(Card::trump) <= 5,
                  "Ai::poverty_shift()\n"
                  "  more then 5 trumps:\n" << hand());

#ifdef BUG_REPORT_REPLAY
  {
    if (   ::bug_report_replay
        && ::bug_report_replay->auto_action()
        && (::bug_report_replay->current_action().type()
            == GameplayActions::Type::poverty_shift)
        && (dynamic_cast<GameplayActions::PovertyShift const&>(::bug_report_replay->current_action()).player == no())
       ) {
      auto const cards = HandCards(hand(),
                                   dynamic_cast<GameplayActions::PovertyShift const&>(::bug_report_replay->current_action()).cards);
      hand().remove(cards);
      return cards;
    } // if (auto execute)
  }
#endif

  Hand& hand = this->hand();

  HandCards res(hand);

  // number of remaining cards to shift
  unsigned rem = hand.count_poverty_cards();

  for (auto const& card : hand) {
    if (card.istrump())
      res.push_back(card);
  }

  hand.remove(res);

  DEBUG_ASSERTION((res.size() <= rem),
                  "Ai::poverty_shift()\n"
                  "  too many trumps: " << res.size() << " > " << rem << '\n'
                  << "  trumps found:\n"
                  << res);
  rem -= res.size();

  if (   !game().rule(Rule::Type::poverty_shift_only_trump)
      && rem > 0) {
    HandCards addon(hand);

    auto color = Card::heart;
    auto size = hand.count(color);

    if (hand.count(Card::spade) < size) {
      color = Card::spade;
      size = hand.count(color);
    }

    if (hand.count(Card::club) < size) {
      color = Card::club;
      size = hand.count(color);
    }

    Hand::Position p = 0;
    while (rem > 0) {
      if (p >= hand.cardsnumber())
        p = 0;
      auto const card = hand.card(p);
      if (size > 0) {
        if (card.color() == color) {
          addon.push_back(card);
          hand.remove(p);
          size--;
          rem--;
        }
      } else {
        addon.push_back(card);
        hand.remove(p);
        rem--;
      }
      p += 1;
    }

    res.insert(res.end(), addon.begin(), addon.end());
  }// !game().rule( Rule::Type::poverty_shift_only_trump ) && rem > 0

  return res;
}


auto Ai::poverty_take_accept(unsigned const cardno) -> bool
{
#ifdef BUG_REPORT_REPLAY
  {
    if (   ::bug_report_replay
        && ::bug_report_replay->auto_action()) {
      if (   (::bug_report_replay->current_action().type()
              == GameplayActions::Type::poverty_accepted)
          && (dynamic_cast<GameplayActions::PovertyAccepted const&>(::bug_report_replay->current_action()).player == no()) )
        return true;
      if (   (::bug_report_replay->current_action().type()
              == GameplayActions::Type::poverty_denied)
          && (dynamic_cast<GameplayActions::PovertyDenied const&>(::bug_report_replay->current_action()).player == no()) )
        return false;
    } // if (auto execute)
  }
#endif

  unsigned trumpno_possible = cardno;
  if (game().rule(Rule::Type::poverty_shift_only_trump)) {
    if (!game().rule(Rule::Type::throw_with_one_trump))
      trumpno_possible = 2;
    else
      trumpno_possible = 1;
  }

  // ToDo: Analog zu den Ansagen verschiedene Kombinationen angeben

  // Keine Farbkarte mehr auf den Hand behalten
  if (hand().cardsnumber() - hand().count(Card::trump) > trumpno_possible)
    return false;

  auto const& game   = this->game();
  auto const& rule   = game.rule();
  auto const with_nines  = rule(Rule::Type::with_nines);
  auto const with_dullen = rule(Rule::Type::dullen);

  auto const& hand = this->hand();
  auto const dullen = hand.count(Card::dulle);
  auto const queens = hand.count(Card::queen);
  auto const club_queens = hand.count(Card::club_queen);
  int queen_modi = 0;
  if (value(Aiconfig::Type::cautious)) {
    queen_modi -= 1;
  }
  if (value(Aiconfig::Type::chancy)) {
    queen_modi += 1;
  }
  if (game.swines().swines_announced()) {
    if (hand.has_swines()) {
      queen_modi += with_nines ? 3 : 5;
    } else {
      queen_modi -= with_nines ? 3 : 5;
    }
  }
  if (game.swines().hyperswines_announced()) {
    if (hand.has_hyperswines()) {
      queen_modi += with_nines ? 2 : 4;
    } else {
      queen_modi -= with_nines ? 2 : 4;
    }
  }
  auto const queens_value = queens + queen_modi;

  if ( with_dullen) {
    return (   (dullen == 2 && queens_value >= 4)
            || (dullen == 2 && queens_value >= 3 && club_queens == 1)
            || (dullen == 2 && queens_value >= 2 && club_queens == 2)
            || (dullen == 1 && queens_value >= 5 && club_queens == 1)
            || (dullen == 1 && queens_value >= 4 && club_queens == 2)
            || (               queens_value >= 7)
            );
  } else {
    return (   (queens_value >= 6)
            || (queens_value >= 5 && club_queens >= 1)
            || (queens_value >= 4 && club_queens >= 2)
           );
  }
  return false;
}


auto Ai::poverty_cards_change(vector<Card> const& cards) -> HandCards
{
#ifdef BUG_REPORT_REPLAY
  {
    if (   ::bug_report_replay
        && ::bug_report_replay->auto_action()) {
      if (::bug_report_replay->current_action().type()
          == GameplayActions::Type::poverty_returned) {
        HandCards const reshifted_cards(hand(),
                                        dynamic_cast<GameplayActions::PovertyReturned const&>(::bug_report_replay->current_action()).cards);
        hand().add(cards);
        hand().remove(reshifted_cards);
        return reshifted_cards;
      }
    } // if (auto execute)
  }
#endif

  auto& hand = this->hand();
  hand.add(cards);

  HandCards res(hand);
  unsigned rem = cards.size();

  // create blank for heart
  if (   hand.contains(Card::heart)
      && hand.count(Card::heart) <= rem
      && game().rule(Rule::Type::dullen)) {
    HandCards addon(hand);

    for (auto const& c : hand) {
      if (c.tcolor() == Card::heart)
        addon.push_back(c);
    }
    hand.remove(addon);
    res.insert(res.end(), addon.begin(), addon.end());
    rem -= addon.size();
  }

  // create blank for Club
  if (   hand.contains(Card::club)
      && hand.count(Card::club) <= rem) {
    bool ace_removed = false;
    if (hand.count(Card::club_ace) == 2) {
      hand.remove(Card::club_ace); // keep second ace.
      ace_removed = true;
    }

    HandCards addon(hand);
    for (auto const& card : hand) {
      if (card.tcolor() == Card::club)
        addon.push_back(card);
    }
    hand.remove(addon);
    res.insert(res.end(), addon.begin(), addon.end());
    rem-= addon.size();

    if (ace_removed)
      hand.add( Card::club_ace );
  }


  // create blank for Spade
  if (   hand.contains(Card::spade)
      && hand.count(Card::spade) <= rem) {
    bool ace_removed = false;
    if (hand.count(Card::spade_ace) == 2) {
      hand.remove (Card::spade_ace); // keep second ace.
      ace_removed = true;
    }

    HandCards addon(hand);
    for (auto const& card : hand) {
      if (card.tcolor() == Card::spade)
        addon.push_back(card);
    }
    hand.remove( addon );
    res.insert(res.end(), addon.begin(), addon.end());
    rem-= addon.size();

    if (ace_removed)
      hand.add( Card::spade_ace );
  }

  // create blank for Heart
  if (   hand.contains(Card::heart)
      && hand.count(Card::heart) <= rem) {

    bool ace_removed = false;
    if( hand.count( Card::heart_ace ) == 2 )
    {
      hand.remove (Card::heart_ace); // keep second ace.
      ace_removed = true;
    }
    HandCards addon(hand);
    for (auto const& card : hand) {
      if (card.tcolor() == Card::heart)
        hand.remove( addon );
    }
    res.insert(res.end(), addon.begin(), addon.end());
    rem-= addon.size();

    if (ace_removed)
      hand.add( Card::heart_ace );
  }


  while ( rem > 0 ) {
    //adding highest color card not ace
    if ( hand.count(Card::trump) < hand.cardsnumber()) {
      HandCard c;
      for (auto const& card : hand) {
        if (!card.istrump()) {
          c = card;
          break;
        }
      }

      if (!c.is_empty()) {
        for (auto const& card : hand) {
          if (!card.istrump() && c.is_jabbed_by(card))
            c = card;
        }

        hand.remove( c );
        res.push_back( c );
        rem--;
        continue;
      }
    }

    { // adding color card
      if (hand.count(Card::trump) < hand.cardsnumber()) {
        HandCard c;
        for (auto const& card : hand) {
          if (!card.istrump()) {
            c = card;
          }
        }

        if (!c.is_empty()) {
          hand.remove(c);
          res.push_back(c);
          rem--;
          continue;
        }
      }
    } // adding color card

    if (rem > 0 ) {
      // add low trump cards
      static vector<Card> const cards_to_check = {
        Card::diamond_nine,
        Card::diamond_king,
        Card::diamond_jack,
        Card::diamond_queen,
        Card::diamond_ten,
        Card::heart_jack,
        Card::spade_jack,
        Card::club_jack,
        Card::diamond_ace,
        Card::heart_queen,
        Card::spade_queen,
        Card::club_queen,
      };

      for (auto card = cards_to_check.begin(); rem > 0; ++card) {
        while ((hand.count(*card) > 0) && (rem > 0)) {
          auto c = HandCard(hand, *card);
          if (c.possible_swine() || c.possible_hyperswine() )
            break;
          hand.remove(c);
          res.push_back(c);
          rem--;
        }
      } // for (card)
    } // add low trump cards

  } // if ( rem > 0 )

  cards_information().reset();
  set_hand(hand); // to update the cards information

  return res;
}


void Ai::poverty_cards_get_back(vector<Card> const& cards)
{
  hand().add(cards);
  cards_information().reset();
  set_hand(hand()); // to update the cards information
}


