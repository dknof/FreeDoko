/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "aiDb.h"

#include "../player.h"
#include "../aiconfig.h"
#include "../../game/game.h"

#include "heuristic.h"

class SoloDecision;

class Ai: public Player, public Aiconfig {
  friend class Gametree;
  friend struct TrickWeighting;
  friend class ::DecisionInGame;

  friend class HeuristicAi;
  public:

  Ai();
  explicit Ai(Name name);
  Ai(Aiconfig const& aiconfig);
  Ai(Player const& player);
  Ai(Player const& player, Aiconfig const& aiconfig);
  Ai(istream& istr);
  Ai(Ai const& ai);
  auto operator=(Ai const& ai) = delete;
  // clone the player
  auto clone() const -> unique_ptr<Player> override;

  ~Ai() override;

  auto db() const noexcept -> AiDb const&;
  auto db()       noexcept -> AiDb&;

  auto write(ostream& ostr) const -> ostream& override;

  auto read(Config const& config, istream& istr) -> bool override;

  void set_game(Game& game) override;
  void set_hand(Hand const& hand) override;

  void new_game(Game& game) override;
  auto get_reservation() -> Reservation const& override;
  void rule_changed(int type, void const* old_value) override;
  void game_open(Game& game) override;
  void game_start() override;
  auto card_get() -> HandCard override;
  virtual auto card_suggestion() -> HandCard;
  void check_swines_announcement_at_game_start() override;
  void swines_announced(Player const& player) override;
  auto nextcard(Trick const& trick) -> Card;

  auto last_heuristic() const -> Heuristic;
  auto last_heuristic_rationale() const -> Rationale const&;
  void set_last_heuristic_to_manual();
  void set_last_heuristic_to_bug_report();

  auto last_announcement_rationale() const -> Rationale const&;

  // the points the team has made
  auto points_of_team() const -> unsigned;

  // set the teams
  virtual void set_teams(vector<Team> const& teams);

  auto announcement_request() const -> Announcement override;

  auto poverty_shift()                                         -> HandCards override;
  auto poverty_take_accept(unsigned cardno)                    -> bool      override;
  auto poverty_cards_change(Game::Poverty::Cards const& cards) -> HandCards override;
  void poverty_cards_get_back(Game::Poverty::Cards const& cards)            override;

  public:
  auto trump_card_limit()        const -> HandCard override;
  auto lowest_trump_card_limit() const -> HandCard;

  private:

  auto marriage_selector() const -> MarriageSelector;

  private:
  vector<unique_ptr<SoloDecision>> solo_decisions_;
  bool silent_marriage_ = false;

  Aiconfig::Heuristic last_heuristic_ = Aiconfig::Heuristic::no_heuristic;
  mutable Rationale last_heuristic_rationale_;
  mutable Rationale last_announcement_rationale_;
}; // class Ai
