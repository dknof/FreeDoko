/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "aiDb.h"

#include <sstream>

AiDb::AiDb() = default;


AiDb::AiDb(istream& istr)
{
  read(istr);
}


AiDb::AiDb(PlayersDb const& players_db) :
  PlayersDb(players_db)
{}


AiDb::AiDb(AiDb const& aidb) = default;


AiDb::~AiDb() = default;


auto AiDb::heuristics_group_general() const -> unsigned
{
  unsigned num = 0;

  for (auto const heuristic : aiconfig_heuristic_list) {
    if (is_general(heuristic)) {
      num += this->heuristic(heuristic);
    }
  }

  return num;
}


auto AiDb::heuristics_group_marriage() const -> unsigned
{
  unsigned num = 0;

  for (auto const heuristic : aiconfig_heuristic_list) {
    if (for_marriage(heuristic)) {
      num += this->heuristic(heuristic);
    }
  }

  return num;
}


auto AiDb::heuristics_group_poverty() const -> unsigned
{
  unsigned num = 0;

  for (auto const heuristic : aiconfig_heuristic_list) {
    if (for_poverty(heuristic)) {
      num += this->heuristic(heuristic);
    }
  }

  return num;
}


auto AiDb::heuristics_group_solo() const -> unsigned
{
  unsigned num = 0;

  for (auto const heuristic : aiconfig_heuristic_list) {
    if (for_solo(heuristic)) {
      num += this->heuristic(heuristic);
    }
  }

  return num;
}


auto AiDb::heuristics_group_solo_color() const -> unsigned
{
  unsigned num = 0;

  for (auto const heuristic : aiconfig_heuristic_list) {
    if (for_color_solo(heuristic)) {
      num += this->heuristic(heuristic);
    }
  }

  return num;
}


auto AiDb::heuristics_group_solo_picture() const -> unsigned
{
  unsigned num = 0;

  for (auto const heuristic : aiconfig_heuristic_list) {
    if (for_picture_solo(heuristic)) {
      num += this->heuristic(heuristic);
    }
  }

  return num;
}


auto AiDb::heuristics_group_solo_meatless() const -> unsigned
{
  unsigned num = 0;

  for (auto const heuristic : aiconfig_heuristic_list) {
    if (for_meatless_solo(heuristic)) {
      num += this->heuristic(heuristic);
    }
  }

  return num;
}


void AiDb::write(ostream& ostr) const
{
  PlayersDb::write(ostr);

  ostr << '\n';

  ostr << "heuristics\n"
    << "{\n";
  for (auto const heuristic : aiconfig_heuristic_list) {
    ostr << setw(32) << heuristic << " =\t"
      << this->heuristic(heuristic) << '\n';
  }
  ostr <<"}\n";
}


auto AiDb::read_group(string const& group, istream& istr) -> bool
{
  if (group == "heuristics")
    read_group_heuristics(istr);
#ifndef OUTDATED
  // 0.7.4   2006-09-14
  else if (group == "Heuristics")
    read_group_heuristics(istr);
#endif
  else
    return PlayersDb::read_group(group, istr);

  return true;
}


void AiDb::read_group_heuristics(istream& istr)
{
  unsigned depth = 0;

  // load the configuration
  while (istr.good()) {
    Config config;
    istr >> config;

    if (config.separator) {
      auto const heuristic = Aiconfig::Heuristic_from_name(config.name);
      if (heuristic != Aiconfig::Heuristic::error) {
        istringstream istr_value(config.value);
        istr_value >> heuristics_[heuristic];
      } else {
        cerr << "ai database: group 'heuristics': "
          << "unknown type '" << config.name << "'."
          << endl;
      }
    } else { // if (config.separator)
      if(config.name == "{") {
        depth += 1;
      } else if(config.name == "}") {
        if (depth == 0) {
          cerr << "ai database: group 'heuristics': "
            << "found a '}' without a '{' before.\n"
            << "Finish reading the the file."
            << endl;
          break;
        } // if (depth == 0)
        depth -= 1;
        if (depth == 0)
          break;
      } else if(config.name.empty()) {
        cerr << "ai database: group 'heuristics': "
          << "Ignoring line \'" << config.value << "\'.\n";
      } else {
        cerr << "ai database: group 'heuristics': "
          << "type '" << config.name << "' unknown.\n"
          << "Ignoring it.\n";
      } // if (config.name == .)
    } // config.separator
  } // while (istr.good())
}


auto AiDb::heuristic(Aiconfig::Heuristic const heuristic) const -> unsigned
{
  return heuristics_[heuristic];
}


void AiDb::increase(Aiconfig::Heuristic const heuristic)
{
  heuristics_[heuristic] += 1;
}


void AiDb::clear()
{
  for (auto const h : aiconfig_heuristic_list)
    heuristics_[h] = 0;
  PlayersDb::clear();
}
