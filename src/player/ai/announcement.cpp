/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "announcement.h"
#include "ai.h"
#include "../player.h"
#include "../team_information.h"
#include "../../party/rule.h"

#include "announcements/normal/base.h"
#include "announcements/poverty/base.h"
#include "announcements/solo/color/base.h"
#include "announcements/solo/picture_single/base.h"
#include "announcements/solo/picture_double/base.h"
#include "announcements/solo/koehler/base.h"
#include "announcements/solo/meatless/base.h"

namespace {
bool have_enough_points(Announcement announcement, Player const& player, TeamInformation const& team_information, Trick const& trick);
bool opposite_team_has_too_many_points(Announcement announcement, Player const& player, TeamInformation const& team_information, Trick const& trick);
Trick::Points points_of_own_team(Player const& player, TeamInformation const& team_information, Trick const& trick);
Trick::Points points_of_opposite_team(Player const& player, TeamInformation const& team_information, Trick const& trick);
Trick::Points points_of_team(Team team, TeamInformation const& team_information, Trick const& trick);
Trick::Points points_of_soloplayer(Trick const& trick);
Trick::Points points_of_solo_contra(Trick const& trick);
} // namespace

/** @return   announcement to make
 **/
unique_ptr<AnnouncementHeuristic>
AnnouncementHeuristic::create(Ai const& ai, Player const& player, Trick const& trick)
{
  auto const& game = player.game();
  auto const& team_information = ai.team_information();
  for (auto const announcement : {
       Announcement::no120,
       Announcement::no90,
       Announcement::no60,
       Announcement::no30,
       Announcement::no0,
       }) {
    if (!(announcement > game.announcements().last(player.team())))
      continue;
    if (opposite_team_has_too_many_points(announcement, player, team_information, trick))
      continue;
    if (!game.announcements().is_valid(announcement, player))
      continue;
    if (have_enough_points(announcement, player, team_information, trick)) {
      return AnnouncementHeuristic::create(ai, player, announcement); // ToDo
    }
    if (!game.announcements().last_chance_to_announce(announcement, player))
      continue;
    return AnnouncementHeuristic::create(ai, player, announcement);
  }
  return {};
}

/** @return   whether to make an announcement earlier so that the partner can pfund
 **/
unique_ptr<AnnouncementHeuristic>
AnnouncementHeuristic::create_to_show_team(Ai const& ai, Player const& player, Trick const& trick, HandCard const card_to_play)
{
  auto const& game = player.game();
  if (game.type() != GameType::normal)
    return {};
  if (is_real(game.teaminfo().get(player)))
    return {};

  if (!trick.isjabbed(card_to_play))
    return {};

  if (trick.isempty()) {
    if (card_to_play.value() != Card::ace)
      return {};
    if (card_to_play.istrump())
      return {};

    auto const color = card_to_play.color();
    if (ai.cards_information().played(color)) // first color run
      return {};
  } else {
    if (trick.startcard().istrump())
      return {};

    auto const color = trick.startcard().color();
    if (ai.cards_information().played(color) > trick.cards().count(color)) // first color run
      return {};
    if (   !card_to_play.istrump()
        && card_to_play.value() != Card::ace)
      return {};
  }

  auto heuristic = AnnouncementHeuristic::create(ai, player, Announcement::no120);
  if (!heuristic->make_announcement(trick)) {
    return {};
  }
  return heuristic;
}

/** @return   a heuristic object
 **/
unique_ptr<AnnouncementHeuristic>
AnnouncementHeuristic::create(Ai const& ai, Player const& player, Announcement const announcement)
{
  switch (player.game().type()) {
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
  case GameType::fox_highest_trump:
  case GameType::redistribute:
    break;
  case GameType::normal:
  case GameType::marriage:
  case GameType::marriage_silent:
    return AnnouncementHeuristics::Normal::create(ai, player, announcement);
  case GameType::poverty:
    return AnnouncementHeuristics::Poverty::create(ai, player, announcement);
  case GameType::marriage_solo:
  case GameType::solo_club:
  case GameType::solo_spade:
  case GameType::solo_heart:
  case GameType::solo_diamond:
    return AnnouncementHeuristics::SoloColor::create(ai, player, announcement);
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
    return AnnouncementHeuristics::SoloSinglePicture::create(ai, player, announcement);
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
    return AnnouncementHeuristics::SoloDoublePicture::create(ai, player, announcement);
  case GameType::solo_koehler:
    return AnnouncementHeuristics::Koehler::create(ai, player, announcement);
  case GameType::solo_meatless:
    return AnnouncementHeuristics::Meatless::create(ai, player, announcement);
  }

  return {};
}

/** constructor
 **/
AnnouncementHeuristic::AnnouncementHeuristic(Ai const& ai,
                                             Player const& player,
                                             Announcement const announcement) :
  DecisionInGame(ai),
  announcement_(announcement)
{ set_player(player); }

/** destructor
 **/
AnnouncementHeuristic::~AnnouncementHeuristic() = default;

/** @return   the announcement
 **/
Announcement
AnnouncementHeuristic::announcement() const
{
  return announcement_;
}

/** @return   card to play, if the conditions are met
 **/
bool
AnnouncementHeuristic::make_announcement(Trick const& trick)
{
  rationale_.clear();
  if (!conditions_met(trick))
    return false;
  return true;
}

/** checks if an announcement should be made
 **
 ** @param    hi        heuristic interface
 **
 ** @return   returns true if an announcement should be made
 **/
bool
AnnouncementHeuristic::make_announcement()
{
  auto const& game   = this->game();
  auto const& player = this->player();
  auto const& hand   = this->hand();
  auto const trick   = (game.status() < Game::Status::play
                        ? Trick(game.startplayer())
                        : game.tricks().current());

  if (player.next_announcement() == Announcement::noannouncement) {
    return false;
  }

  if (max_points_for_opposite_team() < 120)
    return true;

  auto const opp_p = points_of_opposite_team();
  auto const own_p = points_of_own_team();
  if (own_p > 120)
    return true;
  if (opp_p >= 120)
    return false;

  int value = hand_value();

  value += own_p / 20; // previous 25

  if (   !game.is_solo()
      && !game.swines().swines_announced()
      && !game.swines().hyperswines_announced()) {
    value += ((hand.count(Card::club_queen)+1)*hand.count_dulle())*own_p / 25;
  }

  value -= opp_p / 15; // previous 20


  if(   (team() == Team::contra)
     && (hand.count_dulle() == 0)
     && !hand.has_swines()
     && !hand.has_hyperswines()
    ) {
    value -=1;
  }

  if( game.announcements().last( opposite( team() ) ).announcement
     != Announcement::noannouncement  ) {
    value -= 1;
    if (   is_solo(game.type())
        && (team() == Team::re) ) {
      value -= 6;

    }

  }
  if (   is_solo(game.type())
      && (team() == Team::re) )
  {
    value -= 2;

    if (game.type() == GameType::solo_meatless)
      value -=2;

    if (   game.swines().swines_announced()
        && game.players().soloplayer() != game.swines().swines_owner() ) {
      value -=5;

    }
    if (   game.swines().hyperswines_announced()
        && game.players().soloplayer() != game.swines().hyperswines_owner() ) value -=2;
  }

  if (   !is_solo(game.type())
      && team() == Team::contra
      && hand.count_dulle() == 2)
    value +=1; // reference 235643

  // as startplayer color aces to play increase the value
  if (   trick.no() == 0
      && trick.isempty()
      && !game.is_solo()) {
    if (trick.startplayer() == player) {
      auto const& cards_information = this->cards_information();
      int modifier = 0;
      for (auto const color : game.cards().colors()) {
        auto const ace = Card(color, Card::ace);
        if (   !ace.istrump(game)
            && hand.contains(ace)) {
          if (cards_information.remaining_others(color) == 4) {
            value += 2 + modifier;
            modifier -= 1;
          } else if (cards_information.remaining_others(color) > 4) {
            value += 4 + modifier;
            modifier -= 1;
          }
        }
      }
    }
  }


  // (trumps - no trumps) = trumps - (cards on hand - trumps ) = 2* trumps - cards on hand
  if (2 * hand.count(Card::trump) > hand.cardsnumber()) {

    if (this->value(Aiconfig::Type::aggressive)) {
      value += (2 * hand.count(Card::trump) - hand.cardsnumber() ) / 6;
    } else {
      value += (2 * hand.count(Card::trump) - hand.cardsnumber() ) / 9;
    }
  }

  if (opposite_team_can_win_trick(trick)) {
    value -= 1;
    if ( is_solo( game.type() )
        && ( team() == Team::re ) )
      value -= 5;
  } else  {
    value += 2;
  }

  if (   (game.type() == GameType::marriage)
      && (game.marriage().selector() == MarriageSelector::team_set)
      && ( team() == Team::re ) ) {
    if ( is_soloplayer())
      value += 1;
    else
      value += 3; // previous 6 // 2do should be 3*numberofqueens still in game
    if (is_winnerplayer(trick))
      value +=  hand.count( Card::club, Card::ace )
        + hand.count( Card::spade, Card::ace )
        + hand.count( Card::heart, Card::ace );

    if (   game.swines().swines_announced()
        && game.players().soloplayer() == game.swines().swines_owner())
    {
      value +=2 + 2*hand.count_dulle();
      if (!game.swines().hyperswines_announced())
        value +=2; // reference 040594
    }
    if (   game.swines().hyperswines_announced()
        && game.players().soloplayer() == game.swines().hyperswines_owner() )
      value +=2;
    if (   !game.swines().swines_announced()
        && !game.swines().hyperswines_announced()) {
      value += 2 + 2 * hand.count_dulle();
    }
  } else {
    if ( team() == Team::re ) {
      if ( hand.has_swines() ) {
        value +=2 * hand.count_dulle();
      }
      if (   !game.swines().swines_announced()
          && !game.swines().hyperswines_announced()) {
        value +=2*hand.count_dulle();
      }
    }

  }

  if (   (game.type() == GameType::poverty)
      && (player == game.players().poverty_partner()) )
  {
    value -= 15 - hand.count_dulle() - hand.count(Card::club_queen); // previously: 7, 8, 13
    // 13 Reference 243666


    if( trick.isfull() )
      value -= 6; // reference 243666
  }

  if (   (game.type() == GameType::normal)
      || (game.type() == GameType::marriage) ) {

    if( hand.count(Card::trump) < 1.5* hand.cardsnumber() )
      value -=  ( hand.cardsnumber() - hand.count(Card::trump) - 1);
  }

  if (game.in_running_game()) {
    if( (   trick.isfull()  // trick won
         && is_winnerplayer(trick)
         && trick.points() > 15
        )
      )
      value += 1;

    if (   trick.isfull()  // trick lost
        && !is_winnerplayer(trick) )
      value -= 1;

    if( trick.startplayer() != player
       && trick.isempty() )
    {
      value -= 1;

    }
  }


#ifdef ANNOUNCE_DEBUG
  if( !game.isvirtual() )
    cout << "re/CO: " << player.name() << "\t" << value << "\t"
      << value(Aiconfig::Type::announcelimit) << " + "
      << value(Aiconfig::Type::announceconfig) << std::endl;
#endif

  return ( value >=  (static_cast<int>(this->value(Aiconfig::Type::announcelimit))
                      + static_cast<int>(this->value(Aiconfig::Type::announceconfig))));
} // bool Heuristics::make_announcement( HeuristicInterface hi)

/** checks if No90 should be said
 **
 ** @param    hi        heuristic interface
 **
 ** @return   true if hi should say No90
 **/
bool
AnnouncementHeuristic::say_no90()
{
  if (max_points_for_opposite_team() < 90)
    return true;

  int value = 0;
  auto const& game = this->game();
  auto const& hand = this->hand();

  int own_p = points_of_own_team();
  int opp_p = points_of_opposite_team();

  if( own_p > 150 )
    return true;

  if (game.announcements().last(opposite(team())).announcement > Announcement::no120 )
    return false;

  value = hand_value()
    - max( 0, static_cast<int>(game.tricks().remaining_no()) - 7)
    * static_cast<int>(this->value(Aiconfig::Type::announcelimitdec));

  if (game.announcements().last( opposite( team() ) ).announcement
      != Announcement::noannouncement  )
    value -= 2;

  if  (   team_information().guessed_partner()
       && team_information().guessed_partner()->announcement() != Announcement::noannouncement
       && team() == Team::re ) {
    if (   !game.swines().swines_announced()
        && !game.swines().hyperswines_announced()) {
      value += 8*this->hand().count_dulle(); // 7: reference 123343, 8: 139722_1
    }
    opp_p -= 21* this->hand().count_dulle(); // reference 237268
    value += 2; // reference 139722_1,
  }

  if (   game.type() == GameType::poverty
      && game.players().poverty_partner() == player())
  {
    value -= 2;
    value -= (2-hand.count_dulle());
    value -= 5 - hand.count_dulle() - hand.count(Card::club_queen);
    value -= 7 - hand.count(Card::queen);
  }

  value -= (2-hand.count_dulle());

  if (   !game.swines().swines_announced()
      && !game.swines().hyperswines_announced())
  { // reference 101732
    value += hand.count_dulle()*hand.count(Card::club_queen);
    if (   cards_information().played(Card::club_queen) == 0
        && team() == Team::re
        && !is_solo( game.type())
       )
      value += 1 * hand.count_dulle(); // reference 139722
  }

  if( (   game.type() == GameType::marriage
       && game.marriage().selector() == MarriageSelector::team_set
       && team() == Team::re ) )

  {
    if (!aggressive()) {
      value -=3;
    }

    if (   cards_information().played(Card::club_queen) == 0
        && hand.count(Card::trump) >= game.tricks().remaining_no() - 1
       ) {
      value += 1 + hand.count_dulle();
      if (hand.count(Card::club_queen) == 2)
        value -=3;
    }
  }
  for (auto const color : game.cards().colors()) {
    if (color == game.cards().trumpcolor() )
      continue;

    if ( !jabbed_by_guessed_partner(color)
        && hand.count( color ) > 0 )
    {
      if( ( hand.count( color, Card::ace ) == 0 ) ) {
        value -= (color_runs( color )+hand.count( color ));
      } else
        value -= 1;
    }

  } // (for color)

  if (!game.tricks().has_trick(::opposite(team())) )
    value += 2 * static_cast<int>(this->value(Aiconfig::Type::announcelimitdec));

  opp_p+= 3 * game.tricks().remaining_no(); //previous: 5,4
  own_p+= 8 * game.tricks().remaining_no(); // previous: 6, 7

  own_p += static_cast<int>(1.9 * hand.count(Card::trump) * hand.count_swines());
  own_p += static_cast<int>(1.4 * hand.count(Card::trump) * hand.count_hyperswines());


  if ( is_solo( game.type() ) ) {
    own_p += 20 * hand.count(Card::trump);
    opp_p += 20 * cards_information().remaining_trumps_others();
  }

  Trick const trick = (game.status() < Game::Status::play
                       ? Trick(game.startplayer())
                       : game.tricks().current());

  if (opposite_team_can_win_trick(trick)) {
    value -= 2;
  } else {
    value += 1;

  }

  if( game.announcements().last( opposite( team() ) ).announcement
     != Announcement::noannouncement  ) {
    value -= 3;
  }

  if (!this->value(Aiconfig::Type::aggressive)) {
    value -= 1;
    if( team() == Team::contra)
      value -= 1;
  }

#ifdef ANNOUNCE_DEBUG
  if( !game.isvirtual() )
    cout << "no90: " << player.name() << "\t" << value << " + "
      << 2 * value(Aiconfig::Type::announcelimitdec ) << "\t"
      << value(Aiconfig::Type::announcelimit) << " + "
      << 2 * value(Aiconfig::Type::announceconfig) << std::endl;
  cout << "\t" << own_p << "\t" << 1.3 * opp_p << std::endl;
#endif
  return (   (    value + 2 * this->value(Aiconfig::Type::announcelimitdec )
              >     this->value(Aiconfig::Type::announcelimit)
              + 2*this->value(Aiconfig::Type::announceconfig)
             )
          && (   (game.played_points() <= 60)
              || (own_p > 1.3 * opp_p) )  // previous: 2, 1.5
          && (opp_p < 70)
          && (   opp_p > 0
              || game.announcements().last_chance_to_announce(Announcement::no90,
                                                              player())
             )
         );
} // bool Heuristics::say_no90( HeuristicInterface hi )

/** checks if No60 should be said
 **
 ** @param    hi        heuristic interface
 **
 ** @return   true if hi should say No60
 **/
bool
AnnouncementHeuristic::say_no60()
{
  if (max_points_for_opposite_team() < 60)
    return true;

  auto const& game   = this->game();
  auto const& hand   = this->hand();
  auto const& player = this->player();
  Trick const trick  = (game.status() < Game::Status::play
                        ? Trick(game.startplayer())
                        : game.tricks().current());

  int value = 0;

  if( game.announcements().last( opposite( team() ) ).announcement
     > Announcement::no120 )
    return false;



  value = hand_value()
    - max( 0, static_cast<int>(game.tricks().remaining_no()) - 5)
    * static_cast<int>(this->value(Aiconfig::Type::announcelimitdec));

  if( game.announcements().last( opposite( team() ) ).announcement
     != Announcement::noannouncement  )
    value -= 2;

  int own_p = points_of_own_team();
  int opp_p = points_of_opposite_team();


  if( own_p > 180 )
    return true;

  if (   (game.type() == GameType::poverty
          && game.players().poverty_partner() == player) )
  {
    value -= 7;

  }

  if (    is_solo(game.type()) )
  {
    value -= 6;
  }

  value -= game.tricks().remaining_no() / 4;

  opp_p += 3 * game.tricks().remaining_no(); //previous: 5, 4
  opp_p -=4; // reference 233652
  own_p += 6 * game.tricks().remaining_no(); // previous: 5

  own_p += 3 * hand.count(Card::trump) * hand.count_swines();
  own_p += 3 * hand.count(Card::trump) * hand.count_hyperswines();

  if (opposite_team_can_win_trick(trick)) {
    value -= 2;
  } else {
    value += 1;
  }

  if (   (game.type() == GameType::poverty)
      && (player == game.players().poverty_partner()) )
  {
    value -= 4 - hand.count_dulle() - hand.count(Card::club_queen);
  }
  if ( game.rule(Rule::Type::dullen) )
  {
    value -= 5 - 2*hand.count_dulle();
    if ( cards_information().played(Card::dulle)+hand.count_dulle() < 2 )
      value -=2;
  }
#ifdef ANNOUNCE_DEBUG
  if( !game.isvirtual() )
    cout << "no60: " << player.name() << "\t" << value << " + "
      << 3 * value(Aiconfig::Type::announcelimitdec ) << "\t"
      << value(Aiconfig::Type::announcelimit) << " + "
      << 3 * value(Aiconfig::Type::announceconfig) << std::endl;
  cout << own_p << "\t" << opp_p << std::endl;
#endif
  return (   (  value + 3 * this->value(Aiconfig::Type::announcelimitdec)
              >   this->value(Aiconfig::Type::announcelimit)
              + 3 * this->value(Aiconfig::Type::announceconfig)
             )
          && own_p > 1.7 * opp_p  // previous: 3, 2 (1.7 reference:233652)
          && opp_p < 60
          && (   opp_p > 0
              || game.announcements().last_chance_to_announce(Announcement::no60,
                                                              player)
             )
         );
} // bool Heuristics::say_no60( HeuristicInterface hi)

/** checks if No30 should be said
 **
 ** @param    hi        heuristic interface
 **
 ** @return   true if hi should say No30
 **/
bool
AnnouncementHeuristic::say_no30()
{
  if (max_points_for_opposite_team() < 30)
    return true;

  auto const& game   = this->game();
  auto const& player = this->player();

  int value = 0;

  if( game.announcements().last( opposite( team() ) ).announcement
     > Announcement::no120 )
    return false;
  if( game.announcements().last( team() ) != Announcement::no60 ) // do not skip an announcement
    return false;


  value = hand_value()
    - max( 0, static_cast<int>(game.tricks().remaining_no()) - 3)
    * static_cast<int>(this->value(Aiconfig::Type::announcelimitdec));

  if( game.announcements().last( opposite( team() ) ).announcement
     != Announcement::noannouncement  )
    value -= 2;

  int own_p = points_of_own_team();
  int opp_p = points_of_opposite_team();

  if( own_p > 210 )
    return true;


  opp_p+= 4 * game.tricks().remaining_no();
  own_p+= 5 * game.tricks().remaining_no();

  value -=  cards_information().remaining_others(Card::dulle);

#ifdef ANNOUNCE_DEBUG
  if( !game.isvirtual() )
    cout << "no30: " << player.name() << "\t" << value << " + "
      << 6 * value(Aiconfig::Type::announcelimitdec ) << "\t"
      << value(Aiconfig::Type::announcelimit) << " + "
      << value(Aiconfig::Type::announceconfig) << std::endl;
#endif
  return (  (   value + 6 * this->value(Aiconfig::Type::announcelimitdec)
             >   this->value(Aiconfig::Type::announcelimit)
             + this->value(Aiconfig::Type::announceconfig)
            )
          && own_p > 7 * opp_p
          && opp_p < 30
          && (    opp_p > 0
              || game.announcements().last_chance_to_announce(Announcement::no30,
                                                              player)
             )
         );
} // bool Heuristics::say_no30( HeuristicInterface hi)

/** test whether a 'no 0' could be announced
 ** checks, that the other team does not have a trick
 ** and that the hand value is big enoug.
 **
 ** @param    hi     heuristic interface
 **
 ** @return   whether to announce 'no 0'
 **/
bool
AnnouncementHeuristic::say_no0()
{
  auto const& game   = this->game();
  auto const& player = this->player();
  auto const& hand   = this->hand();
  Trick const trick = (game.status() < Game::Status::play
                       ? Trick(game.startplayer())
                       : game.tricks().current());

  if( game.announcements().last( opposite( team() ) ).announcement
     != Announcement::noannouncement )
    return false;

  if( game.announcements().last( team() ) != Announcement::no30 ) // do not skip an announcement
    return false;

  // make sure, that the other team does not have a trick
  for (auto const& player : game.players()) { // cppcheck-suppress shadowVariable
    if (   !guessed_same_team(player)
        && game.tricks().has_trick(player))
      return false;
  }

  // as long as the current trick is not closed, it has to be taken into account
  if (   trick.isfull()
      && !guessed_same_team(trick.winnerplayer()))
    return false;

  // wait for the last possible moment
  if (!game.announcements().last_chance_to_announce(Announcement::no0,
                                                    player))
    return false;

  if (   highest_trump_of_opposite_team().is_jabbed_by(hand.lowest_trump())
      && hand.count(Card::trump) >= game.tricks().remaining_no())
    return true;

  // ToDo: check that no opposite player has the highest trump card
  // Test: force a player by 'announcelimitdec' high

  int const value = hand_value()
    - max( 0, static_cast<int>(game.tricks().remaining_no()) )
    * static_cast<int>(this->value(Aiconfig::Type::announcelimitdec));

#ifdef ANNOUNCE_DEBUG
  if( !game.isvirtual() )
    cout << "no0: " << player.name() << "\t" << value << " + "
      << 4 * value(Aiconfig::Type::announcelimitdec ) << "\t"
      << value(Aiconfig::Type::announcelimit) << " + "
      << 3 * value(Aiconfig::Type::announceconfig) << std::endl;
#endif

  return (value + 4 * static_cast<int>(this->value(Aiconfig::Type::announcelimitdec))
          > static_cast<int>(this->value(Aiconfig::Type::announcelimit)
                             + 3 * this->value(Aiconfig::Type::announceconfig))
         );
} // bool Heuristics::say_no0( HeuristicInterface hi)

/** value of cards for announcement
 **
 ** @param    hi        heuristic interface
 **
 ** @return   the value of the hand
 **/
int
AnnouncementHeuristic::hand_value()
{
  int value = 0;

  unsigned v_queen = 0;
  unsigned v_jack = 0;
  unsigned v_highest = 0;
  unsigned v_king = 0;
  unsigned v_ace = 0;
  unsigned v_fehl = 0;

  auto const& game   = this->game();
  auto const& player = this->player();
  auto const& hand   = this->hand();
  Trick const trick = (game.status() < Game::Status::play
                       ? Trick(game.startplayer())
                       : game.tricks().current());

  switch (game.type()) {
  case GameType::normal:
    { v_queen = 2; v_jack = 1; v_highest = 3; v_king = 0; v_ace = 1; v_fehl = 2;

      if (aggressive())
        v_highest += 1;
      if (team() == Team::re)
      {
        value = 1;
      } else
      {
        value = 0;
      }
    }
    break;

  case GameType::poverty:
    { v_queen = 2; v_jack = 1; v_highest = 3; v_king = 0; v_ace = 0; v_fehl = 2;
      value = -3;
    }
    break;

  case GameType::marriage:
    { v_queen = 2; v_jack = 1; v_highest = 3; v_king = 0; v_ace = 1; v_fehl = 2; }
    break;

  case GameType::solo_jack:
    { v_queen = 0; v_jack = 3; v_highest = 0; v_king = 0; v_ace = 2; v_fehl = 1;
      if (team() == Team::re) {
        value = -6;
      } else {
        value = -2;
      }
    }
    break;

  case GameType::solo_queen:
    { v_queen = 3; v_jack = 0; v_highest = 0; v_king = 0; v_ace = 2; v_fehl = 1;
      if (team() == Team::re) {
        value = -6;
      } else {
        value = -2;
      }
    }
    break;

  case GameType::solo_king:
    { v_queen = 0; v_jack = 0; v_highest = 0; v_king = 3; v_ace = 2; v_fehl = 1;
      if (team() == Team::re) {
        value = -6;
      } else {
        value = -2;
      }
    }
    break;

  case GameType::solo_queen_jack:
    { v_queen = 3; v_jack = 2; v_highest = 0; v_king = 0; v_ace = 1; v_fehl = 1;
      if (team() == Team::re) {
        value = -6;
      } else {
        value = -3;
      }
    }
    break;

  case GameType::solo_king_jack:
    { v_queen = 0; v_jack = 2; v_highest = 0; v_king = 3; v_ace = 1; v_fehl = 1;
      if (team() == Team::re) {
        value = -6;
      } else {
        value = -3;
      }
    }
    break;

  case GameType::solo_king_queen:
    { v_queen = 2; v_jack = 0; v_highest = 0; v_king = 3; v_ace = 1; v_fehl = 1;
      if (team() == Team::re) {
        value = -6;
      } else {
        value = -3;
      }
    }
    break;

  case GameType::solo_koehler:
    { v_queen = 2; v_jack = 1; v_highest = 0; v_king = 3; v_ace = 0; v_fehl = 0;
      if (team() == Team::re) {
        value = -5;
      } else {
        value = -2;
      }
    }
    break;

  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
    { v_queen = 2; v_jack = 1; v_highest = 3; v_king = 0; v_ace = 1; v_fehl = 2;
      if (team() == Team::re) {
        value = -7;
      } else {
        value = 0;
      }
    }
    break;


  case GameType::solo_meatless:
    {
      v_queen = 0; v_jack = 0; v_highest = 0; v_king = 0; v_ace = 4; v_fehl = 1; // missing aces
      value = -2;
    }
    break;

  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
  case GameType::fox_highest_trump:
  case GameType::redistribute:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
    break;
  }

  if ( !this->game().rule(Rule::Type::with_nines) ) {
    v_ace= max(0,static_cast<int>(v_ace)-1);
    value += 1; // compensate for reduced ace value
  }

#ifdef ANNOUNCE_DEBUG_DETAIL
  cout << "CalcHandValue(1)\t" << player().name() << "\t" << value << endl;
#endif


  for (auto const& card : hand) {
    if (   card.isdulle()
        || card.isswine()
        || card.possible_swine()
        || card.ishyperswine()
        || card.possible_hyperswine()
       ) {
      value += v_highest;
    } else if (card.value()==Card::queen) {
      value += v_queen;
      if( game.type()==GameType::solo_queen &&
         card.color() == Card::club )
        value +=1;
    } else if( card.value()==Card::jack ) {
      value += v_jack;
      if( game.type()==GameType::solo_jack &&
         card.color() == Card::club )
        value +=1;
    } else if( card.value()==Card::king ) {
      value += v_king;
      if( game.type()==GameType::solo_king &&
         card.color() == Card::club )
        value +=1;
    } else if(    card.value()==Card::ace
              && !card.istrump()
              && !jabbed_by_not_guessed_partner(card.color())) {
      value += v_ace;
      if (game.in_running_game()
          && (trick.cardno_of_player( player ) == 0 ) ) {
        if (hand.count( card.color() ) == 1) {
          value += v_ace;
          if( color_runs( card.color() ) == 0 ) {
            value += v_ace;
          }
        } else {
          value += 1;
          if( color_runs( card.color() ) == 0 ) {
            value += v_ace;
          }
        }

      }
      if ( card.color() == Card::heart
          && hand.count( card.color() ) > 1
          && game.rule(Rule::Type::dullen)) {
        value -=1;
      }
    }

  }
#ifdef ANNOUNCE_DEBUG_DETAIL
  cout << "CalcHandValue(2)\t" << player.name() << "\t" << value << endl;
#endif

  for (auto const color : game.cards().colors()) {
    if (   !Card( color, Card::ace ).istrump(game)
        && !hand.contains(color)
        && !jabbed_by_not_guessed_partner(color)
       ) {
      if (game.in_running_game() ) {
        if(     trick.actcardno()>0
           && color_runs( color ) == 0
           && trick.startcard().tcolor() != color)
          value += v_fehl;
      } else {
        value += v_fehl;
      }
    }
  }

  if( game.type() == GameType::solo_meatless ) {
    for (auto const color : game.cards().colors()) {
      if( hand.contains(color) )
        value -=  (v_fehl
                   * ( 2 - hand.count(color, Card::ace ) ));
    }
    if( is_soloplayer()) {
      unsigned longColor = 0;
      for (auto const color : game.cards().colors()) {
        if(   hand.count(color, Card::ace)  == 2
           && hand.count(color) > longColor )
          longColor = hand.count(color);
      }
      value += longColor;
    }
  } else  {

    for (auto const color : game.cards().colors()) {

      if( color == game.cards().trumpcolor() )
        continue;

      // reduction for long colors
      if (         hand.count( color ) > 2
          && !jabbed_by_guessed_partner(color) )
        value -= hand.count( color ) - (2 + hand.count( color, Card::ace ));

      // including this trick the player does not have the color
      if (    !hand.contains( color )
          && !(    game.in_running_game()
               && ( trick.actcardno()
                   > trick.cardno_of_player( player ) )
               && ( trick.card_of_player( player ).tcolor()
                   == color )
              )
         )
        continue;

      if (  ( hand.count( color, Card::ace ) == 0 )
          && !jabbed_by_guessed_partner(color) )
      {
        // previous        value -= v_ace;
        value -= v_fehl;
        //unprotected tens on hand
        if (    hand.count( color, Card::ten ) > 0
            &&  hand.count( color, Card::ten ) == hand.count( color ) )
          value -= v_fehl;
      }

      if( (   hand.count( color )
           + color_runs( color )
           + (   game.in_running_game()
              && trick.actcardno() > 0
              && ( trick.startcard().color()
                  == color
                 ) ? 1 : 0 )
           > ( game.cards().count_unique(color) - 1
              - ( game.rule(Rule::Type::with_nines) ? 0 : 1 ) )
          ) && !jabbed_by_guessed_partner(color) )
        value -= v_fehl;

      if ( (   hand.count( color )
            + color_runs( color )
            + ( (   game.in_running_game()
                 && trick.actcardno()>0
                 && ( trick.startcard().color()
                     == color)
                ) ? 1 : 0)
            >= 3)
          && ( hand.count( Card( color, Card::ace ) ) < 2 )
          && !jabbed_by_guessed_partner(color) )
        value -= v_ace;

    } // (for color)
  } // if (...)
#ifdef ANNOUNCE_DEBUG_DETAIL
  cout << "CalcHandValue(3)\t" << player.name() << "\t" << value << endl;
#endif

  if( game.type() == GameType::solo_koehler ) {
    for (auto color : game.cards().colors()) {
      if (   hand.contains(color)
          && !jabbed_by_guessed_partner(color) )
        value -= 5;
    }
  } // if( game.type() == GameType::solo_koehler )


  if (   (   game.swines().swines_announced()
          && game.swines().swines_owner() != player )
      || game.swines().swines_announcement_valid( player )) {
    Player const& swines_player = (  game.swines().swines_announced()
                                   ? game.swines().swines_owner()
                                   : player );
    if (   guessed_team(swines_player) == team()
        || game.swines().swines_announcement_valid(swines_player)) {
      if (swines_player != player ) {
        value += v_highest;
      } else {
        value += 1;
      }
    } else {
      value -= 2* v_highest;
    }
  }

#ifdef ANNOUNCE_DEBUG_DETAIL
  cout << "CalcHandValue(4)\t" << player.name() << "\t" << value << endl;
#endif

  if(  (   game.swines().hyperswines_announced()
        && game.swines().hyperswines_owner() != player
       )
     || game.swines().hyperswines_announcement_valid(player)
    )
  {
    Player const& hyperswines_player = (  game.swines().hyperswines_announced()
                                        ? game.swines().hyperswines_owner()
                                        : player );
    if (   guessed_team(hyperswines_player) == team()
        || game.swines().hyperswines_announcement_valid( hyperswines_player )) {
      if (hyperswines_player != player) {
        value += v_highest;
      } else {
        value += 1;
      }
    } else {
      value -= 2 * v_highest;
    }
  }

#ifdef ANNOUNCE_DEBUG_DETAIL
  cout << "CalcHandValue(5)\t" << player.name() << "\t" << value << endl;
#endif


  if (   !is_solo( game.type() )
      || is_soloplayer())
  {
    if (hand.count(Card::trump)
        >= ( (cards_information().remaining_trumps_others() / 3.0)
            + round(  (game.in_running_game()
                       ? game.tricks().remaining_no()
                       : game.tricks().max_size()
                      ) / 3.0)
           ) )
      value += 2;
  }

  if (game.in_running_game()
      && (trick.cardno_of_player( player ) == 0 ) )
  { // count aces like trumps, if I serve
    unsigned cardno = hand.count(Card::trump);

    for (auto const color : game.rule().card_colors()) {

      if( color == game.cards().trumpcolor() )
        continue;

      if(  ((color_runs( color ) == 0)
            || jabbed_by_guessed_partner(color) )
         && (hand.count( color ) <= 1 + hand.count( Card( color, Card::ace ) ) )
        )
      {
        cardno += hand.count( Card( color, Card::ace ) );

      }

    }

    if (cardno >= 9 )
      value += 1;
    if (cardno >= game.tricks().remaining_no() - 1 )
      value += 1;
    if (cardno >= game.tricks().remaining_no() )
      value += 2;
    if (cardno == hand.cardsnumber() )
      value += 2;


    if ( game.is_solo()
        && cardno >= 9 )
      value += 2; // reference 096567

  } else
  {
    if (hand.count(Card::trump) >= 9 )
      value += 1;
    if (hand.count(Card::trump) >= game.tricks().remaining_no() - 1 )
      value += 2;
    if (hand.count(Card::trump) >= game.tricks().remaining_no() )
      value += 2;
    if (hand.count(Card::trump) == hand.cardsnumber() )
      value += 2;

    if ( game.is_solo()
        && hand.count(Card::trump) >= 9 )
      value += 1;
  }



  if (hand.count(Card::trump) -1 < (hand.cardsnumber()+1) / 2 )
    value -= 1;
  if (hand.count(Card::trump) -1 < (hand.cardsnumber()+1) / 3 )
    value -= 1;
  if (hand.count(Card::trump) -1 < (hand.cardsnumber()+1) / 4 )
    value -= 1;

  if ( game.type() == GameType::normal ) // remove a value if a lot of small trumps are on hand
  {
    value -= (  hand.count(Card::trump) //2do
              - hand.count( Card::queen )
              - hand.count( Card::jack )
              - hand.count_swines()
              - hand.count_hyperswines()
              - hand.count_dulle()) / 4;
  }

  if ( game.type() == GameType::normal ) // remove a value for two heart aces
  {
    if(    game.rule(Rule::Type::dullen)
       && game.cards().trumpcolor() != Card::heart
       && hand.count(Card::heart,Card::ace) == 2)
      value -=1;
  }

  if (   game.status() > Game::Status::start
      && game.type() == GameType::poverty
      && game.players().poverty_partner() == player)
    value -= 2;

  if ( (   game.type() == GameType::normal
        || game.type() == GameType::poverty )
      && team() == Team::re )
  {
    if( hand.count( Card::spade_queen ) == 2 )
      value += 2;
    if( hand.count( Card::heart_queen ) == 2 )
      value += 1;
  }

  if (   ( hand.count( Card::queen ) < 3 ) // check for some queens on hand
      ||(    team() == Team::contra
         && hand.count( Card::queen ) < 4 )
     )
  {
    value -= v_queen;
  }


  if (game.in_running_game()) {
    if( (   trick.isfull()  // lost trick
         && !(guessed_team(trick.winnerplayer()) & team())
        ) ||
       (    !trick.isempty() // lost color trick
        && !trick.startcard().istrump()
        && !is_winnerplayer(trick)
       )
      )
      value -= 1;
    if (   trick.isempty()
        && trick.startplayer() == player)
      value += 1;

    //first tricks, other player serves
    if (   trick.no() < 2
        && trick.startplayer() != player)
      value -= 1;
  }


#ifdef ANNOUNCE_DEBUG_DETAIL
  cout << "CalcHandValue(6)\t" << player.name() << "\t" << value << endl;
#endif

  return value;
}

/** check if opposite team can still win this trick.
 **
 ** @param    trick   the current trick
 ** @param    hi      heuristic interface
 **
 ** @return   returns true if the opposite team can win the trick
 **
 ** @todo      use highest_opposite_card_behind for oppositeTeamCanWinTrick
 **/
bool
AnnouncementHeuristic::opposite_team_can_win_trick(Trick const& trick)
{
  if (!guessed_same_team(trick.winnerplayer()))
    return true;

  for (Player const& player : trick.remaining_following_players()) {
    if (   !guessed_same_team(player)
        && guessed_hand(player).higher_card_exists(trick.winnercard()))
      return true;
  }

  return false;
}

/** -> result
 **
 ** @param    hi      heuristic interface
 **
 ** @return   the highest trump of the playes of the opposite team
 **/
HandCard
AnnouncementHeuristic::highest_trump_of_opposite_team()
{
  HandCard highest_trump;

  for (auto const& player : game().players()) {
    if (guessed_same_team(player))
      continue;

    auto const trump = guessed_hand(player).highest_trump();
    if (highest_trump.is_jabbed_by(trump))
      highest_trump = trump;
  }

  return highest_trump;
}


/** @return   whether the color was jabbed by a guessed partner
 **/
bool AnnouncementHeuristic::jabbed_by_guessed_partner(Card::TColor const color) const
{
  auto const is_jabbed = [this, color](Trick const& trick) {
    return (   !trick.isempty()
            && trick.startcard().tcolor() == color
            && trick.winnercard().istrump()
            && trick.winnerplayer() != player()
            && guessed_same_team(trick.winnerplayer())
           );
  };
  return container_algorithm::any_of(game().tricks(), is_jabbed);
}

/** @return   whether the color was jabbed by a player not to be a guessed partner
 **/
bool AnnouncementHeuristic::jabbed_by_not_guessed_partner(Card::TColor const color) const
{
  auto const is_jabbed = [this, color](Trick const& trick) {
    return (   !trick.isempty()
            && trick.startcard().tcolor() == color
            && trick.winnercard().istrump()
            && !guessed_same_team(trick.winnerplayer())
           );
  };
  return container_algorithm::any_of(game().tricks(), is_jabbed);
}

namespace {
/** @return   whether the player has enough points
 **/
bool
  have_enough_points(Announcement announcement, Player const& player, TeamInformation const& team_information, Trick const& trick)
  {
    auto const points = points_of_own_team(player, team_information, trick);
    switch (announcement) {
    case Announcement::no120:
      return (points > 240 - 120);
    case Announcement::no90:
      return (points > 240 -  90);
    case Announcement::no60:
      return (points > 240 -  60);
    case Announcement::no30:
      return (points > 240 -  30);
    case Announcement::no0:
      return false;
    default:
      return false;
    }
    return false;
  }

/** @return   whether the opposite team has too many points
 **/
bool
  opposite_team_has_too_many_points(Announcement announcement, Player const& player, TeamInformation const& team_information, Trick const& trick)
  {
    auto const points = points_of_opposite_team(player, team_information, trick);
    switch (announcement) {
    case Announcement::no120:
      return (points >= 120);
    case Announcement::no90:
      return (points >= 90);
    case Announcement::no60:
      return (points >= 60);
    case Announcement::no30:
      return (points >= 30);
    case Announcement::no0:
      return (points > 0);
    default:
      return false;
    }
    return false;
  }

/** @return   the points of the own team
 **/
Trick::Points
  points_of_own_team(Player const& player, TeamInformation const& team_information, Trick const& trick)
  {
    if (is_normal(player.game().type()))
      return points_of_team(player.team(), team_information, trick);
    // solo
    if (player.team() == Team::re)
      return points_of_soloplayer(trick);
    else
      return points_of_solo_contra(trick);
  }

/** @return   the points of the opposite team
 **/
Trick::Points
  points_of_opposite_team(Player const& player, TeamInformation const& team_information, Trick const& trick)
  {
    if (is_normal(player.game().type()))
      return points_of_team(opposite(player.team()), team_information, trick);
    // solo
    if (player.team() == Team::re)
      return points_of_solo_contra(trick);
    else
      return points_of_soloplayer(trick);
  }

/** @return   the points of the team 'team'
 **/
Trick::Points
  points_of_team(Team const team, TeamInformation const& team_information, Trick const& trick)
  {
    Trick::Points points = 0;
    auto const& players = trick.game().players();
    unsigned players_known = 0;
    Trick::Points min_points_of_other_players = 240;
    for (auto const& player : players) {
      if (team_information.team(player) == team) {
        players_known += 1;
        points += player.points_in_trickpile();
      } else if (player.points_in_trickpile() < min_points_of_other_players) {
        min_points_of_other_players = player.points_in_trickpile();
      }
    }
    if (   trick.isfull()
        && team_information.team(trick.winnerplayer()) == team) {
      points += trick.points();
    }

    if (players_known == 2) {
      return points;
    }

    if (players_known > 2) {
      players_known = 0;
      points = 0;
      for (auto const& player : players) {
        if (team_information.team(player) == team) {
          players_known += 1;
          points += player.points_in_trickpile();
        } else if (player.points_in_trickpile() < min_points_of_other_players) {
          min_points_of_other_players = player.points_in_trickpile();
        }
      }
      if (   trick.isfull()
          && team_information.team(trick.winnerplayer()) == team)
        points += trick.points();
    }

    if (players_known == 1) {
      return points + min_points_of_other_players;
    }

    // no player known
    return min({players[0].points_in_trickpile() + players[1].points_in_trickpile(),
               players[0].points_in_trickpile() + players[2].points_in_trickpile(),
               players[0].points_in_trickpile() + players[3].points_in_trickpile(),
               players[1].points_in_trickpile() + players[2].points_in_trickpile(),
               players[1].points_in_trickpile() + players[3].points_in_trickpile(),
               players[2].points_in_trickpile() + players[3].points_in_trickpile(),
               });

    return points;
  }

/** @return   the points of the soloplayer
 **/
Trick::Points
  points_of_soloplayer(Trick const& trick)
  {
    auto const& soloplayer = trick.game().players().soloplayer();
    auto points = soloplayer.points_in_trickpile();
    if (   trick.isfull()
        && trick.winnerplayer() == soloplayer)
      points += trick.points();
    return points;
  }

/** @return   the points of the contra team in a solo
 **/
Trick::Points
  points_of_solo_contra(Trick const& trick)
  {
    auto const& soloplayer = trick.game().players().soloplayer();
    Trick::Points points = 0;
    for (auto const& player : trick.game().players()) {
      if (player.team() != Team::re)
        points += player.points_in_trickpile();
    }
    if (   trick.isfull()
        && trick.winnerplayer() != soloplayer)
      points += trick.points();
    return points;
  }
} // namespace
