/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "base.h"

#include "re.h"
#include "contra.h"
#include "no_90.h"
#include "no_60.h"
#include "no_30.h"
#include "no_0.h"

#include "../../../player.h"
#include "../../../../game/game.h"
#include "../../../../game/cards.h"

namespace AnnouncementHeuristics::Normal {

auto create(Ai const& ai,
            Player const& player,
            Announcement const announcement) -> unique_ptr<Base>
{
  switch (announcement) {
  case Announcement::no120:
    switch (player.team()) {
    case Team::re:
      return make_unique<Re>(ai, player);
    case Team::contra:
      return make_unique<Contra>(ai, player);
    default:
      return {};
    }
  case Announcement::no90:
    return make_unique<No90>(ai, player);
  case Announcement::no60:
    return make_unique<No60>(ai, player);
  case Announcement::no30:
    return make_unique<No30>(ai, player);
  case Announcement::no0:
    return make_unique<No0>(ai, player);
  default:
    return {};
  }
  return {};
}

Base::Base(Ai const& ai,
           Player const& player,
           Announcement const announcement) :
  AnnouncementHeuristic(ai, player, announcement)
{ }


Base::~Base() = default;


auto Base::re_announced() -> bool
{
  return game().announcements().last(Team::re) != Announcement::noannouncement;
}


auto Base::contra_announced() -> bool
{
  return game().announcements().last(Team::contra) != Announcement::noannouncement;
}


auto Base::other_has_hyperswines() -> bool
{
  return (   game().swines().hyperswines_announced()
          && player() != game().swines().hyperswines_owner());
}


auto Base::count_blank_colors(Trick const& trick) const -> unsigned
{
  auto const& hand = this->hand();
  auto const trumpcolor = game().cards().trumpcolor();
  auto const& cards_information = this->cards_information();
  return count_if(game().cards().colors(),
                  [&](Card::Color const color) {
                  return (   color != trumpcolor
                          && (trick.isempty() || trick.startcard().tcolor() != color)
                          && !hand.contains(color)
                          && cards_information.color_runs(color) == 0);
                  });
}

auto Base::count_color_aces(Trick const& trick) -> unsigned
/** Only count each color one time
 ** Only count single ones, if the player starts the trick
 **/
{
  auto const& hand = this->hand();
  auto const trumpcolor = game().cards().trumpcolor();
  auto const cards_information = this->cards_information();
  auto consider_color_aces = (player() == (trick.isempty() ? trick.startplayer() : trick.winnerplayer()));
  if (   !consider_color_aces
      && !trick.isempty()
      && !trick.isfull()
      && trick.actplayer() == player()
      && !trick.startcard().istrump()) {
    auto const color = trick.startcard().color();
    if (   hand.contains(color, Card::ace)
        && trick.isjabbed(Card(color, Card::ace), hand)
        && cards_information.color_runs(color) == (trick.isempty() ? 0 : 1)
        && (   cards_information.played(color) == trick.cards().count(color)
            || trick.islastcard())) {
      consider_color_aces = true;
    } else if (   !hand.contains(color)
               && hand.can_jab(trick)) {
      consider_color_aces = true;
    }
  }
  auto can_play_color_ace = [&](Card::Color const color) {
    auto const ace = Card(color, Card::ace);
    if (color == trumpcolor)
      return false;
    if (cards_information.color_runs(color) > 0)
      return false;
    if (cards_information.remaining_others(color) + trick.cards().count(color) < (3u + (value(Aiconfig::Type::chancy) ? 0 : 1) + (value(Aiconfig::Type::cautious) ? 1 : 0)))
      return false;
    if (cards_information.played(color) > trick.cards().count(color) + hand.count_played(color))
      return false;
    if (hand.count(ace) == 2)
      return true;
    if (hand.contains(ace))
      return consider_color_aces;
    return false;
  };
  return count_if(game().cards().colors(), can_play_color_ace);
}


auto Base::count_color_tricks_jabbed(Trick const& trick) -> unsigned
{
  auto color_trick_jabbed = [this](Trick const& trick) {
    return (   !trick.isempty()
            && !trick.startcard().istrump()
            && trick.winnerplayer() == player()
            && (   trick.winnercard().istrump()
                || trick.isfull()));
  };
  return count_if(game().tricks(), color_trick_jabbed) + (can_jab_current_trick(trick) ? 1 : 0);
}


auto Base::can_jab_current_trick(Trick const& trick) -> bool
{
  return (   !trick.isempty()
          && !trick.startcard().istrump()
          && !trick.has_played(player())
          && hand().can_jab(trick)
          && (   trick.islastcard()
              || cards_information().color_runs(trick.startcard().tcolor()) <= 1));

}


auto Base::queen_value() -> double
{
  double value = 0;

  value += 1.4 * hand().count(Card::club_queen);
  value += 1.2 * hand().count(Card::spade_queen);
  value += 0.9 * hand().count(Card::heart_queen);
  value += 0.8 * hand().count(Card::diamond_queen);

  return value;
}

} // namespace AnnouncementHeuristics::Normal
