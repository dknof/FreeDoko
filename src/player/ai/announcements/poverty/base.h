/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../normal/base.h"

namespace AnnouncementHeuristics::Poverty {

class Base;

unique_ptr<Base> create(Ai const& ai, Player const& player, Announcement announcement);

/** Base class for announcement heuristics for poverty
 **/
class Base : public AnnouncementHeuristics::Normal::Base {
  public:
    Base(Ai const& ai, Player const& player, Announcement announcement);

    ~Base() override;

    auto conditions_met(Trick const& trick) -> bool override = 0;
};
} // namespace AnnouncementHeuristics::Poverty
