/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "contra.h"

#include "../../../../party/rule.h"
#include "../../../../game/game.h"
#include "../../../../game/swines.h"
#include "../../ai.h"

namespace AnnouncementHeuristics::Poverty {

Contra::Contra(Ai const& ai,
       Player const& player) :
  Base(ai, player, Announcement::no120)
{ }


Contra::~Contra() = default;

auto Contra::conditions_met(Trick const& trick) -> bool
{
  auto const with_nines  = game().rule(Rule::Type::with_nines);
  auto const with_dullen = game().rule(Rule::Type::dullen);

  auto const& hand = this->hand();
  auto const dullen = hand.count_all(Card::dulle);
  auto const queens = queen_value();
  auto const blank_colors = count_blank_colors(trick);
  auto const trumps = hand.count_all(Card::trump);
  auto const color_tricks_jabbed = count_color_tricks_jabbed(trick);
  auto const color_aces = count_color_aces(trick);
  auto const color_cards = hand.cardsnumber() - trumps;
  auto const queen_modi = this->queen_modi();
  auto const queens_value = queens + queen_modi;

  if (with_dullen) {
    rationale_.add(_("Announcements::have %u dullen, %f queens + %f (modifier), %u blank colors, %u color tricks jabbed, %u color aces to play, %u other color cards",
                     dullen, queens, queen_modi, blank_colors, color_tricks_jabbed, color_aces, color_cards));
  } else {
    rationale_.add(_("Announcements::have %f queens + %f (modifier), %u blank colors, %u color tricks jabbed, %u color aces to play, %u other color cards",
                     queens, queen_modi, blank_colors, color_tricks_jabbed, color_aces, color_cards));
  }
  if ( with_nines &&  with_dullen) {
    return (   (dullen == 2 && queens_value >= 6 && color_cards <= 1)
            || (dullen == 2 && queens_value >= 5 && color_cards <= 0)
            || (dullen == 1 && queens_value >= 8 && color_cards <= 0)
           );
  } else if (!with_nines &&  with_dullen) {
    return (   (dullen == 2 && queens_value >= 6 && color_cards <= 1)
            || (dullen == 2 && queens_value >= 5 && color_cards <= 0)
            || (dullen == 1 && queens_value >= 8 && color_cards <= 0)
           );
  } else if ( with_nines && !with_dullen) {
    return (   (queens_value >= 6 && color_cards <= 1)
            || (queens_value >= 5 && color_cards <= 0)
           );
  } else if (!with_nines && !with_dullen) {
    return (   (queens_value >= 6 && color_cards <= 1)
            || (queens_value >= 5 && color_cards <= 0)
           );
  }
  return false;
}


auto Contra::queen_modi() -> double
{
  auto const& game   = this->game();
  auto const& ai     = this->ai();
  auto const& player = this->player();
  auto const with_nines = game.rule(Rule::Type::with_nines);

  int queen_modi = 0;

  if (ai.value(Aiconfig::Type::cautious)) {
    rationale_.add(_("Announcement::cautious"));
    queen_modi -= 1;
  }
  if (ai.value(Aiconfig::Type::chancy)) {
    rationale_.add(_("Announcement::chancy"));
    queen_modi += 1;
  }
  if (game.type() == GameType::marriage) {
    rationale_.add(_("GameType::marriage"));
    queen_modi -= 1;
  }
  if (re_announced()) {
    rationale_.add(_("Announcement::re announced"));
    queen_modi -= 2;
    if (game.announcements().last(opposite(player.team())) >= Announcement::no90) {
      queen_modi -= 2;
    }
    if (game.announcements().last(opposite(player.team())) >= Announcement::no60) {
      queen_modi -= 2;
    }
  }

  if (game.swines().swines_announced()) {
    rationale_.add(_("Announcement::swines announced"));
    if (player == game.swines().swines_owner()) {
      queen_modi += with_nines ? 3 : 5;
    } else {
      queen_modi -= with_nines ? 3 : 5;
    }
  }
  if (game.swines().hyperswines_announced()) {
    rationale_.add(_("Announcement::hyperswines announced"));
    if (player == game.swines().hyperswines_owner()) {
      queen_modi += with_nines ? 2 : 4;
    } else {
      queen_modi -= with_nines ? 2 : 4;
    }
  }

  return queen_modi;
}

} // namespace AnnouncementHeuristics::Poverty
