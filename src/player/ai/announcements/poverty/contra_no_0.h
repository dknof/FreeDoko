/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "base.h"

namespace AnnouncementHeuristics::Poverty {

/** Heuristic for re annonucement
 **/
class ContraNo0 : public Base {
  public:
    ContraNo0(Ai const& ai, Player const& player);

    ~ContraNo0() override;

    bool conditions_met(Trick const& trick) override;
};
} // namespace AnnouncementHeuristics::Poverty
