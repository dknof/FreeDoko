/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "base.h"

#include "re.h"
#include "re_no_90.h"
#include "re_no_60.h"
#include "re_no_30.h"
#include "re_no_0.h"
#include "contra.h"
#include "contra_no_90.h"
#include "contra_no_60.h"
#include "contra_no_30.h"
#include "contra_no_0.h"

#include "../../../../player.h"

namespace AnnouncementHeuristics::Koehler {

/** @return   a heuristic object
 **/
unique_ptr<Base>
  create(Ai const& ai,
         Player const& player,
         Announcement const announcement)
  {
    switch (player.team()) {
    case Team::re:
      switch (announcement) {
      case Announcement::no120:
        return make_unique<Re>(ai, player);
      case Announcement::no90:
        return make_unique<ReNo90>(ai, player);
      case Announcement::no60:
        return make_unique<ReNo60>(ai, player);
      case Announcement::no30:
        return make_unique<ReNo30>(ai, player);
      case Announcement::no0:
        return make_unique<ReNo0>(ai, player);
      default:
        return {};
      }
    case Team::contra:
      switch (announcement) {
      case Announcement::no120:
        return make_unique<Contra>(ai, player);
      case Announcement::no90:
        return make_unique<ContraNo90>(ai, player);
      case Announcement::no60:
        return make_unique<ContraNo60>(ai, player);
      case Announcement::no30:
        return make_unique<ContraNo30>(ai, player);
      case Announcement::no0:
        return make_unique<ContraNo0>(ai, player);
      default:
        return {};
      }
    default:
      return {};
    }
    return {};
  }

/** constructor
 **/
Base::Base(Ai const& ai,
           Player const& player,
           Announcement const announcement) :
  AnnouncementHeuristic(ai, player, announcement)
{ }

/** destructor
 **/
Base::~Base() = default;

} // namespace AnnouncementHeuristics::Koehler
