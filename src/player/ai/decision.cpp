/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "decision.h"
#include "ai.h"
#include "../cards_information.h"
#include "../player.h"
#include "../../party/party.h"
#include "../../party/rule.h"

Decision::Decision(Ai const& ai) :
  ai_(ai)
{ }


Decision::~Decision() = default;


void Decision::set_player(Player const& player)
{
  player_ = &player;
}


auto Decision::player() const -> Player const&
{
  return *player_;
}


auto Decision::rationale() const -> Rationale const&
{
  return rationale_;
}


auto Decision::party() const -> Party const&
{
  return game().party();
}


auto Decision::rule() const -> Rule const&
{
  return game().rule();
}


auto Decision::game() const -> Game const&
{
  return player_->game();
}


auto Decision::ai() const -> Ai const&
{
  return ai_;
}


auto Decision::hand() const -> Hand const&
{
  return player_->hand();
}


auto Decision::value(Aiconfig::Type::Bool const type) const -> bool
{
  return ai().value(type);
}


auto Decision::value(Aiconfig::Type::Int const type) const -> int
{
  return ai().value(type);
}


auto Decision::value(Aiconfig::Type::Card const type) const -> Card
{
  return ai().value(type);
}


auto Decision::trusting() const -> bool
{
  return value(Aiconfig::Type::trusting);
}


auto Decision::aggressive() const -> bool
{
  return value(Aiconfig::Type::aggressive);
}


auto Decision::cautious() const -> bool
{
  return value(Aiconfig::Type::cautious);
}


auto Decision::chancy() const -> bool
{
  return value(Aiconfig::Type::chancy);
}
