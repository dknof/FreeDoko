/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../aiconfig.h"
#include "rationale.h"
class Party;
class Rule;
class Game;
class Ai;
class CardsInformation;
class Player;
class Hand;

/** Base class for decisions
 **/
class Decision {
public:
  explicit Decision(Ai const& ai);
  Decision(Decision const&) = delete;
  auto operator=(Decision const&) = delete;

  virtual ~Decision();

  void set_player(Player const& player);
  auto player() const -> Player const&;

  auto rationale() const -> Rationale const&;

protected: // Zugriffsfunktionen
  auto party()             const -> Party            const&;
  auto rule()              const -> Rule             const&;
  auto game()              const -> Game             const&;
  auto ai()                const -> Ai               const&;
  auto hand()              const -> Hand             const&;

  auto value(Aiconfig::Type::Bool type) const -> bool;
  auto value(Aiconfig::Type::Int type)  const -> int ;
  auto value(Aiconfig::Type::Card type) const -> Card;
  auto trusting()   const -> bool;
  auto aggressive() const -> bool;
  auto cautious()   const -> bool;
  auto chancy()     const -> bool;

private:
  Ai const& ai_;
  Player const* player_ = nullptr;
protected:
  Rationale rationale_;
};
