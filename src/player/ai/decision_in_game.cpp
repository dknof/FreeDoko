/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "decision_in_game.h"

#include "ai.h"
#include "../team_information.h"
#include "../../game/game.h"


DecisionInGame::DecisionInGame(Ai const& ai) :
  Decision(ai)
{ }


DecisionInGame::~DecisionInGame() = default;


auto DecisionInGame::trump_card_limit() const -> Card
{
  if (ai().silent_marriage_)
    return value(Aiconfig::Type::trumplimit_solocolor);

  switch (game().type()) {
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
    return value(Aiconfig::Type::trumplimit_solocolor);
  case GameType::solo_jack:
    return value(Aiconfig::Type::trumplimit_solojack);
  case GameType::solo_queen:
    return value(Aiconfig::Type::trumplimit_soloqueen);
  case GameType::solo_king:
    return value(Aiconfig::Type::trumplimit_soloking);
  case GameType::solo_queen_jack:
    return value(Aiconfig::Type::trumplimit_solojackqueen);
  case GameType::solo_king_jack:
    return value(Aiconfig::Type::trumplimit_solojackking);
  case GameType::solo_king_queen:
    return value(Aiconfig::Type::trumplimit_soloqueenking);
  case GameType::solo_koehler:
    return value(Aiconfig::Type::trumplimit_solokoehler);
  case GameType::solo_meatless:
    return value(Aiconfig::Type::trumplimit_meatless);
  default:
    return value(Aiconfig::Type::trumplimit_normal);
  }
}


auto DecisionInGame::lowest_trump_card_limit() const -> Card
{
  if (ai().silent_marriage_)
    return value(Aiconfig::Type::lowest_trumplimit_solocolor);

  switch (game().type()) {
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
    return value(Aiconfig::Type::lowest_trumplimit_solocolor);
  case GameType::solo_jack:
    return value(Aiconfig::Type::lowest_trumplimit_solojack);
  case GameType::solo_queen:
    return value(Aiconfig::Type::lowest_trumplimit_soloqueen);
  case GameType::solo_king:
    return value(Aiconfig::Type::lowest_trumplimit_soloking);
  case GameType::solo_queen_jack:
    return value(Aiconfig::Type::lowest_trumplimit_solojackqueen);
  case GameType::solo_king_jack:
    return value(Aiconfig::Type::lowest_trumplimit_solojackking);
  case GameType::solo_king_queen:
    return value(Aiconfig::Type::lowest_trumplimit_soloqueenking);
  case GameType::solo_koehler:
    return value(Aiconfig::Type::lowest_trumplimit_solokoehler);
  case GameType::solo_meatless:
    return value(Aiconfig::Type::lowest_trumplimit_meatless);
  default:
    return value(Aiconfig::Type::lowest_trumplimit_normal);
  }
}


auto DecisionInGame::below_trump_card_limit(Card card) const -> bool
{
  if (!card.istrump(game()))
    return true;
  return game().cards().less(card, trump_card_limit());
}


auto DecisionInGame::below_lowest_trump_card_limit(Card card) const -> bool
{
  if (!card.istrump(game()))
    return false;
  return game().cards().less(card, lowest_trump_card_limit());
}


auto DecisionInGame::team_information() const -> TeamInformation const&
{
  return ai().team_information();
}


auto DecisionInGame::team() const -> Team
{
  return player().team();
}


auto DecisionInGame::is_soloplayer() const -> bool
{
  return (   is_solo(game().type())
          && team() == Team::re);
}


auto DecisionInGame::own_team_known() const -> bool
{
  if (!is_real(team()))
    return false;
  return game().teaminfo().get(player()) == team();
}


auto DecisionInGame::own_team_guessed() const -> bool
{
  if (own_team_known())
    return true;

  if (team() == Team::re)
    return team_information().team_value(player()) >= (trusting() ?  30 : cautious() ?  50 :  40);
  else if (team() == Team::contra)
    return team_information().team_value(player()) <= (trusting() ? -30 : cautious() ? -50 : -40);
  return false;
}


auto DecisionInGame::partner() const -> Player const&
{
  auto const partner = team_information().partner();
  DEBUG_ASSERTION(partner,
                  "DecisionInGame::partner()\n"
                  "No (single) partner found.\n");
  if (!partner)
    throw "No (single) partner found."s;
  return *partner;
}

auto DecisionInGame::guessed_team(Player const& player) const -> GuessedTeam
{
  if (value(Aiconfig::Type::teams_known))
    return to_guessed(player.team());

  if (player.game().type() == GameType::marriage)
    return to_guessed(player.team());

  return this->player().teaminfo(player);
}


auto DecisionInGame::same_team(Player const& player) const -> bool
{
  if (!!player.game().marriage().selector())
    return false;
  return (guessed_team(player) == team());
}


auto DecisionInGame::surely_same_team(Player const& player) const -> bool
{
  if (!!player.game().marriage().selector())
    return false;
  return (guessed_team(player) & team());
}


auto DecisionInGame::opposite_team(Player const& player) const -> bool
{
  if (!!player.game().marriage().selector())
    return false;
  return (guessed_team(player) == opposite(team()));
}


auto DecisionInGame::guessed_same_team(Player const& player) const -> bool
{
  if (   player.game().marriage().selector() != MarriageSelector::team_set
      && player.no() != this->player().no())
    return false;
  return (guessed_team(player) & team());
}


auto DecisionInGame::guessed_opposite_team(Player const& player) const -> bool
{
  if (!!player.game().marriage().selector())
    return true;
  if (guessed_team(player) == Team::unknown)
    return false;
  return (guessed_team(player) & opposite(team()));
}


auto DecisionInGame::cards_information() const -> CardsInformation const&
{
  return ai().cards_information();
}


auto DecisionInGame::cards_information(Player const& player) const -> CardsInformation::OfPlayer const&
{
  return cards_information().of_player(player);
}


auto DecisionInGame::color_runs(Card::TColor const color) const -> unsigned
{
  return cards_information().color_runs(color);
}


auto DecisionInGame::color_runs() const -> unsigned
{
  return cards_information().color_runs();
}


auto DecisionInGame::count_swines_and_hyperswines_of_own_team() const -> unsigned
{
  auto const& swines = game().swines();
  unsigned n = hand().count_swines() + hand().count_hyperswines();
  if (   swines.swines_announced()
      && same_team(swines.swines_owner())) {
    n += cards_information().remaining_others(game().cards().swine());
  }
  if (   swines.hyperswines_announced()
      && same_team(swines.hyperswines_owner())) {
    n += cards_information().remaining_others(game().cards().hyperswine());
  }
  return n;
}


auto DecisionInGame::guessed_hand(Player const& player) const -> Hand const&
{
  auto const& game = player.game();
  if (   !game.isvirtual()
      && player == this->player()) {
    return hand();
  }
  if (value(Aiconfig::Type::hands_known))
    return player.hand();

  // If it is the last trick and the player has already played a card,
  // his hand is empty.
  if (   game.status() == Game::Status::play
      && game.tricks().real_remaining_no() == 1
      && game.tricks().current().cardno_of_player(player) < game.tricks().current().actcardno()) {
    return player.hand();
  }

#ifndef RELEASE
  Hand const& hand = cards_information().estimated_hand(player);
  hand.self_check();
  return hand;
#else
  return cards_information().estimated_hand(player);
#endif
}


auto DecisionInGame::guessed_hand_of_soloplayer() const -> Hand const&
{
  DEBUG_ASSERTION(game().players().exists_soloplayer(),
                  "DecisionInGame::guessed_hand_of_soloplayer(): there exists no soloplayer");

  return guessed_hand(game().players().soloplayer());
}


auto DecisionInGame::is_winnerplayer(Trick const& trick) const -> bool
{
  return (   !trick.isempty()
          && trick.winnerplayer() == player());
}


auto DecisionInGame::only_own_team_behind(Trick const& trick) const -> bool
{
  auto const playerno = game().players().size();
  // take a look if all players coming in this trick are of mine team
  for (unsigned i = trick.actcardno() + 1; i < playerno; ++i)
    if (!guessed_same_team(trick.player_of_card(i)))
      return false;

  return true;
}


auto DecisionInGame::points_of_own_team(bool const with_current_trick) -> Trick::Points
{
  if (!game().in_running_game())
    return 0;

  Game const& game = this->game();
  unsigned points = 0;
  unsigned players = 0;

  for (auto const& player : game.players()) {
    if (guessed_same_team(player)) {
      points += game.points_of_player(player);
      players += 1;
    }
  }

  if (   players == 1
      && !game.is_solo()) {
    // add the smallest points of the unknown players
    unsigned min_points = UINT_MAX;
    for (auto const& player : game.players()) {
      if (   !guessed_same_team(player)
          && !guessed_opposite_team(player)
          && (game.points_of_player(player) <= min_points))
        min_points = game.points_of_player(player);
    }
    if (min_points < UINT_MAX)
      points += min_points;
  }

  Trick const& trick = game.tricks().current();
  if (trick.isfull()) {
    if (guessed_same_team(trick.winnerplayer()))
      points += trick.points();
  } else if (with_current_trick) {
    if (   only_own_team_behind(trick)
        && (   guessed_same_team(trick.winnerplayer())
            || (   !trick.has_played(player())
                && trick.isjabbed(hand().validcards(trick).highest_card()))
           )
       )
      points += trick.points() + hand().validcards(trick).lowest_value();
  }

  return points;
}


auto DecisionInGame::points_of_opposite_team(bool const with_current_trick) -> unsigned
{
  if (!game().in_running_game())
    return 0;

  Game const& game = this->game();
  Trick const& trick = game.tricks().current();

  if (with_current_trick)
    return (game.played_points()
            - points_of_own_team(with_current_trick)
           );
  else
    return (game.played_points()
            - points_of_own_team(with_current_trick)
            - trick.points() // without current trick remove points of this trick => still remaining
           );
}


auto DecisionInGame::max_points_for_opposite_team() -> Trick::Points
{
  auto const& game = this->game();

  if (   game.is_solo()
      && game.players().is_soloplayer(player())) {
    return max_points_for_opposite_team_soloplayer();
  }

  if (is_real(team())) {
    return 240 - game.points_of_team(team());
  } else {
    return 240;
  }
}


auto DecisionInGame::max_points_for_opposite_team_soloplayer() -> unsigned
{
  auto const& game = this->game();

  // Stiche zählen, die nicht an den Solospieler gehen
  unsigned res = game.points_of_team(Team::contra);
  auto const& player = this->player();
  auto const& cards_information = this->cards_information();
  auto const& trick = game.tricks().current();

  auto hand = this->hand();
  if (trick.has_played(player)) {
    hand.add(trick.card_of_player(player));
  }

  { // prüfen, ob alle Trümpfe gezogen werden können
    auto const card = cards_information.highest_remaining_trump_of_others();
    if (card) {
      auto const n = hand.count_ge(card);
      if (n < cards_information.remaining_trumps_others()) {
        // Es können nicht alle Trümpfe gezogen werden
        return 240;
      }
    }
  }

  // Für jede Farbe zählen, wie viele Stiche sicher sind. Der Rest gibt 3 * 11 + Punkte der Karte für den Gegner
  for (auto const color : game.cards().colors()) {
    auto const card = cards_information.highest_remaining_card_of_others(color);
    if (!card)
      continue;
    auto const cards = hand.single_cards(color);
    auto const n = cards.count_ge(card);
    if (n == cards.size())
      continue;
    if (n >= cards_information.remaining_others(color))
      continue;
    for (auto const& c : cards.cards_not_ge(card)) {
      res += c.points() + 3 * 11;
    }
  }

  return res;
}

