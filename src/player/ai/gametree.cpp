/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "gametree.h"


#include "ai.h"
#include "../cards_information.h"
#include "../team_information.h"

#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../game/game_summary.h"
#include "../../ui/ui.h"
#include "../../misc/preferences.h"


#ifdef USE_THREADS
#include <future>
auto const main_thread_id = std::this_thread::get_id();
#endif

// the multiplier for a game point
auto constexpr game_points_multiplier = 10000;

// if all cards from the estimated hand always lead to an invalid game exception, take all cards into account
enum class CardsTest {
  valid_cards,
  all_cards,
  finished
};

/** constructor
 **
 ** @param    ai   the ai
 **/
Gametree::Gametree(Ai const& ai) :
  DecisionInGame(ai),
  rating_(ai.rating())
{ 
  set_player(ai);
}

/** destructor
 **/
Gametree::~Gametree() = default;

/** @return   the rating
 **/
Rating::Type
Gametree::rating() const
{ return rating_; }

/** @return   the best card according to the weighting
 **/
Card
Gametree::best_card() const
{
  auto const& hand = this->hand();
  auto const& game = this->game();
  auto const valid_cards = hand.validcards(game.tricks().current());

  HandCard best_card(hand);
  HandCards best_cards(hand);
  int weighting_best = INT_MIN;

  try {
    ::ui->set_max_progress(valid_cards.size());
    auto const time_limit_per_card = std::chrono::milliseconds(1000 * ai().value(Aiconfig::Type::max_sec_wait_for_gametree) / valid_cards.size());
    auto time_limit = std::chrono::steady_clock::now();

    unsigned i = 0;
    // now calculate for all cards the weighting
    for (auto const& card : valid_cards) {
      game.signal_ai_test_card(card, player().no());
      time_limit += time_limit_per_card;
      int const card_weighting = weighting(card, time_limit);
      game.signal_ai_card_weighting(card_weighting);

#ifdef RELEASE
      DEBUG_ASSERTION((   card_weighting != INT_MIN
                       && card_weighting != INT_MAX),
                      "Gametree::best_card()\n"
                      "  No valid game found.\n"
                      "  Card: " << card);
#else
      if (   card_weighting == INT_MIN
          || card_weighting == INT_MAX) {
        cerr << "Weighting-error\n";

        for (auto const& player : game.players())
          cerr << "player " << player.no() << ' ' << player.name() << ":\n"
            << cards_information().estimated_hand(player) << endl;
        DEBUG_ASSERTION(   card_weighting != INT_MIN
                        && card_weighting != INT_MAX,
                        "Gametree::best_card()\n"
                        "  No valid game found.\n"
                        "  Card: " << card);
      }
#endif

      // test, whether the card is better:
      // - the card weighting is better
      // - the card weighting is equal take the lower card
      // - the card weighting is equal, take the color card with the greater/lower value
      //                       if the trick goes to the own/opposite team so far
      if (card_weighting > weighting_best) {
        weighting_best = card_weighting;
        best_cards.clear();
        best_cards.push_back(card);
      } else if (card_weighting == weighting_best) {
        best_cards.push_back(card);
      }

      i += 1;
      ::ui->add_progress(static_cast<double>(i)
                         - ::ui->progress() * valid_cards.size());
    }

    best_card = choose_best_prefiltered_card(game.tricks().current(), hand, best_cards);
    DEBUG_ASSERTION(best_cards.count(best_card) > 0,
                    "Gametree::best_card()\n"
                    "  best card not in the cards:"
                    << best_card << " (" << best_cards.size() << ")\n"
                    << best_cards);
  } catch (...) {
    ::ui->finish_progress(false);

    throw;
  } // try
  ::ui->finish_progress();

  DEBUG_ASSERTION(!best_card.is_empty(),
                  "Gametree::best_card():\n"
                  "  no best card found.\n"
                  << "\n"
                  //<< "Hand:\n"
                  //<< hand()
                 );
  return best_card;
}

/** -> result
 **
 ** @param    card   the card
 **
 ** @return   the weighting for the card 'card'
 **/
int
Gametree::weighting(Card const card, TimePoint const time_limit) const
{
  auto hands_combinations = cards_information().estimated_hands_combinations();
  vector<int> hands_modus;
#ifdef USE_THREADS
  if (::preferences(Preferences::Type::use_threads)) {
    vector<std::future<int>> hands_modus_future;
    size_t const max_threads = std::thread::hardware_concurrency();
    for (size_t i = 0; i < min(max_threads, hands_combinations.size()); ++i) {
      hands_modus_future.emplace_back(std::async(&Gametree::weighting_of_distribution, this, card, hands_combinations[i]));
    }
    for (size_t i = 0; i < hands_combinations.size(); ++i) {
      auto const& hands = hands_combinations[i];
      game().signal_ai_test_hands(hands, cards_information().weighting(hands), i, player().no());
      while (hands_modus_future[i].wait_for(std::chrono::milliseconds(10)) != std::future_status::ready)
        ::ui->update();
      if (i + max_threads < hands_combinations.size()) {
        hands_modus_future.emplace_back(std::async(&Gametree::weighting_of_distribution, this, card, hands_combinations[i + max_threads]));
      }
      auto const modus = hands_modus_future[i].get();
      hands_modus.push_back(modus);
      game().signal_ai_hands_weighting(modus);
#ifdef WITH_UI_INTERFACE
      ::ui->add_progress((1.0 - 0.01) / hands_combinations.size());
#endif
      if (std::chrono::steady_clock::now() >= time_limit) {
        cerr << "Gametree: " << card << ": Time limit exceeded, only calculated " << i << " / " << hands_combinations.size() << " hands combinations.\n";
        break;
      }
    }
  } else
#endif
  {
    for (size_t i = 0; i < hands_combinations.size(); ++i) {
      auto const& hands = hands_combinations[i];
      game().signal_ai_test_hands(hands, cards_information().weighting(hands), i, player().no());
      auto const modus = weighting_of_distribution(card, hands);
      game().signal_ai_hands_weighting(modus);
      hands_modus.push_back(modus);

#ifdef WITH_UI_INTERFACE
      ::ui->add_progress((1.0 - 0.01) / hands_combinations.size());
#endif
      if (std::chrono::steady_clock::now() >= time_limit) {
        cerr << "Gametree: " << card << ": Time limit exceeded, only calculated " << i << " / " << hands_combinations.size() << " hands combinations.\n";
        break;
      }
    }
  }

  if (ai().value(Aiconfig::Type::cautious) && hands_modus.size() > 1) {
    sort(hands_modus);
    hands_modus.erase(hands_modus.begin() + hands_modus.size() / 2, hands_modus.end());
  } else if (ai().value(Aiconfig::Type::chancy)) {
    sort(hands_modus);
    hands_modus.erase(hands_modus.begin(), hands_modus.begin() + hands_modus.size() / 2);
  }

  return sum(hands_modus) / static_cast<int>(hands_modus.size());

#ifdef OUTDATED
  // Kompliziert und unnötig
  // Bei CardsInformation::estimated_hands_combinations() wird bereits vorselektiert, so dass die Blätter hier ähnliche Bewertungen haben.
  int min_weighting = INT_MAX;
  int sum_weighting = 0;
  for (auto const& hands : hands_combinations) {
    auto const weighting = cards_information().weighting(hands);
    min_weighting = std::min(min_weighting, weighting);
    sum_weighting += weighting;
  }
  min_weighting -= 100;

  {
    auto const weighting = cards_information().weighting(hands);
    double const hands_weighting = (  sum_weighting == static_cast<int>(hands_combinations.size()) * min_weighting
                                    ? 1.0 / hands_combinations.size()
                                    : (  1.0 * (weighting - min_weighting)
                                       / (sum_weighting - hands_combinations.size() * min_weighting) ));
    weighted_modus += hands_weighting * modus;
  }

  return weighted_modus;
#endif
}

/** -> result
 **
 ** @param    card_   the card
 ** @param    hands   the hands
 **
 ** @return   the weighting for the card 'card'
 **/
int
Gametree::weighting_of_distribution(Card const card_, Hands const& hands) const
{
  // Create new players.
  vector<unique_ptr<Player>> player_virt;
  for (auto const& player : game().players()) {
    (void)player;
    player_virt.push_back(ai().Player::clone());
  }

  // create a virtual game
  Game virt_game(game(), player_virt);

  unsigned p = 0;
  for (auto& player : virt_game.players()) {
    static_cast<Person&>(player).set_name(game().player(p).name());
    player.set_hand(hands[p]);
    auto team = ai().team_information().team(p);
    if (!is_real(team)) {
      team = player.hand().contains(Card::club_queen) ? GuessedTeam::re : GuessedTeam::contra;
    }
    player.set_team(maybe_to_real(team));
    player.self_check();
    ++p;
  }

  { // play the card
    auto& current_player = virt_game.players().current_player();
    auto const card = HandCard(current_player.hand(), card_);
    if (   card.possible_swine()
        && !card.isswine()) {
      virt_game.swines().swines_announce(current_player);
    }
    if (   card.possible_hyperswine()
        && !card.ishyperswine())
      virt_game.swines().hyperswines_announce(current_player);
    current_player.hand().playcard(card);
    virt_game.tricks().current().add(card);
    virt_game.teaminfo().update();
    virt_game.players().next_current_player();
    for (auto& player : virt_game.players()) {
      player.card_played(card);
    }
  } // play the card 'card_'
  return rated_modus(virt_game);
}

/** Returns the rated modi.
 ** From players of the same team, the modi is calculated positiv,
 ** for players of the opposite team, the modi is calculated negative
 **
 ** @param    virt_game   the virtual game
 **
 ** @return   the maximal minimum modi
 **/
// NOLINTNEXTLINE(misc-no-recursion)
auto Gametree::rated_modus(Game& virt_game) const -> int
{
#ifdef USE_THREADS
  if (main_thread_id == std::this_thread::get_id())
#endif
    ::ui->update();
  if (game().status() != Game::Status::play)
    return 0;

  if (virt_game.tricks().remaining_no() == 0) {
    try {
      virt_game.evaluatetrick();
    } catch (...) {
      throw;
    }
    int const modus = game_finished_modus(virt_game);
    return modus;
  }
  if (virt_game.tricks().current().isfull()) {
    int const modus = full_trick_modi(virt_game);
    return modus;
  }

  // Here we are in the middle of a trick.
  // Try all cards and take the worst/best modi if the player is in the
  // opposite/same team.

  // the correspoding virtual player to the ai
  auto const& virt_player = virt_game.player(player().no());
  auto const& current_player = virt_game.players().current_player();
  auto const& hand = current_player.hand();

  bool const same_team = (current_player.team() == virt_player.team());

  // When the ai does not 'know' the hand of the player
  // all cards have to be viewed, else there can result only invalid games.
  HandCards const valid_cards = hand.validcards(virt_game.tricks().current());

  // the rating used for the weightings
  auto const rating = Rating::create(this->rating());
  DEBUG_ASSERTION(rating,
                  "Gametree::rated_modus()\n"
                  "  rating '" << this->rating() << "' not created");

  // The sum of the modi is used in order to catch special points
  // in the case that not all teams are known (see below)
  int modi_sum = 0;
  // the number of modi in 'modi_sum'
  int modi_num = 0;
  unsigned valid_cards_no = 0;
  // now test all cards
  for (auto const& c : hand.validcards(virt_game.tricks().current())) {
    // create a new virtual game
    vector<unique_ptr<Player>> player_virt;
    for (auto const& player : virt_game.players()) {
      player_virt.push_back(player.clone());
    }
    auto virt_game_2 = make_unique<Game>(virt_game, player_virt);
    auto& current_player_2 = virt_game_2->players().current_player();

    try {
      { // now play the card
        HandCard const card2(current_player_2.hand(), c);
        if (   card2.possible_swine()
            && !card2.isswine())
          virt_game_2->swines().swines_announce(current_player_2);
        if (   card2.possible_hyperswine()
            && !card2.ishyperswine())
          virt_game_2->swines().hyperswines_announce(current_player_2);
        current_player_2.hand().playcard(card2);
        virt_game_2->tricks().current().add(card2);
        virt_game_2->teaminfo().update();

        virt_game_2->players().next_current_player();
        try {
          for (auto& p : virt_game_2->players()) {
            p.card_played(card2);
          }
        } catch (...) {
          throw;
        }
      } // play the card 'c'

#ifndef RELEASE
      if (virt_game_2->tricks().remaining_no() > 1) {
        for (auto const& player : virt_game_2->players()) {
          DEBUG_ASSERTION((player.hand().cardsnumber() >= 1),
                          "Gametree::rated_modus():\n"
                          "  empty hand.\n"
                          "Game: \n");
        }
      }
#endif
      // get the modi through the recursion
#ifdef USE_THREADS
      if (main_thread_id == std::this_thread::get_id())
#endif
        game().signal_ai_test_card(c, virt_game.players().current_player().no());
      try {
        int const modus = rated_modus(*virt_game_2);
#ifdef USE_THREADS
        if (main_thread_id == std::this_thread::get_id())
#endif
          game().signal_ai_card_weighting(modus);
        if (   modus != INT_MIN
            && modus != INT_MAX) {
          for (unsigned i = 0; i < hand.count(c); ++i) {
            valid_cards_no += 1;
            // the rating is from the view of the player of the card and not
            // from the ai
            if (same_team)
              rating->add(modus);
            else
              rating->add(-modus);
            modi_sum += modus;
            modi_num += 1;
          } // for (i)
        }
      } catch (...) {
#ifdef USE_THREADS
        if (main_thread_id == std::this_thread::get_id())
#endif
          game().signal_ai_card_weighting(INT_MIN);
        throw;
      }
    } catch (InvalidGameException const& e) {
      // nothing to do
    } catch (...) {
      throw;
    } // try
  }

  // delete the worst values, since the player will only try his best ones
  if (virt_game.tricks().remaining_no() - 1 >
      hand.cardsnumber() - valid_cards_no) {
    rating->delete_worst(virt_game.tricks().remaining_no() - 1
                         - (hand.cardsnumber() - valid_cards_no));
  }

  int const modus = (  same_team
                     ? rating->value()
                     : -rating->value());

  if (   modus == INT_MIN
      || modus == INT_MAX) {
    return modus;
  } else if (modi_num == 0) {
    return modus;
  } else {
    // the sum is added because else in the end when not all teams are known
    // special points (i.e. charlie) could be lost
    // note: when the game is not finished, 'modi_sum' < 1000
    return (modus + modi_sum / modi_num / 1000);
  }
}

// NOLINTNEXTLINE(misc-no-recursion)
auto Gametree::full_trick_modi(Game& virt_game) const -> int
{
  // From players of the same team, the modi is calculated positiv,
  // for players of the opposite team, the modi is calculated negative
  try {
    virt_game.evaluatetrick();
  } catch (...) {
    throw;
  }

  virt_game.tricks().add(Trick(virt_game.players().current_player()));
  for (auto& player : virt_game.players())
    player.trick_open(virt_game.tricks().current());
  int const modus = rated_modus(virt_game);
  return modus;
}

/** returns the modus for a finished game
 **/
int
Gametree::game_finished_modus(Game& virt_game) const
{
  virt_game.finish();

  auto const& player = virt_game.player(this->player().no());
  auto const team = player.team();
  auto const game_summary = GameSummary(virt_game);
  auto const game_points = (   game_summary.winnerteam() == Team::noteam
                            ? (  static_cast<int>(game_summary.points(team))
                               - static_cast<int>(game_summary.points(opposite(team))))
                            : (static_cast<int>(game_summary.points())
                               * (game_summary.winnerteam() == team ? 1 : -1)));
  // Die eigenen Punkte vom aktuellen Stich stärker bewerten, damit die KI lieber später Punkte verliert.
  // Beispiel: Keine Herz Zehn vor ein Schwein ausspielen (Referenz 156813).
  auto const& trick = virt_game.tricks().trick(game().tricks().current_no());
  int current_trick_modi = 0;
  if (trick.winnerteam() == team) {
    current_trick_modi += static_cast<int>(trick.points()) - static_cast<int>(trick.card_of_player(player).points());
    if (trick.card_of_player(player).isfox())
      current_trick_modi += 5;
  } else {
    current_trick_modi -= static_cast<int>(trick.card_of_player(player).points());
  }

  return (game_points_multiplier * game_points
          + static_cast<int>(game_summary.trick_points(team))
          + current_trick_modi);
}

/** -> result
 **
 ** @param    hi       heuristic interface
 ** @param    hand     hand of the player
 ** @param    cards    cards to choose from
 **
 ** @return   lowest card to play from 'cards'
 **/
Card
Gametree::choose_best_prefiltered_card(Trick const& trick, Hand const& hand, HandCards const& cards) const
{
  if (cards.empty())
    return {};
  if (cards.size() == 1)
    return cards[0];

  if (cards.count(Card::trump) < cards.size())
    return choose_best_prefiltered_color_card(trick, hand, cards);
  else
    return choose_best_prefiltered_trump_card(trick, hand, cards);
}

/** -> result
 **
 ** @param    hi       heuristic interface
 ** @param    hand     hand of the player
 ** @param    cards    cards to choose from
 **
 ** @return   lowest card to play from 'cards',
 **            Card::empty, if none is found
 **/
Card
Gametree::choose_best_prefiltered_color_card(Trick const& trick, Hand const& hand, HandCards const& cards) const
{
  DEBUG_ASSERTION(cards.count(Card::trump) < cards.size(),
                  "Gametree::choose_best_prefilterd_color_card(hand, cards)\n"
                  "  'cards' only contains trumps");
  Game const& game = this->game();
  Rule const& rule = game.rule();

  // whether the trick goes to the own team
  bool const own_team = (   !trick.isempty()
                         && trick.winnerteam() == player().team());

  if (own_team) {
    // search a single ace
    for (auto const c : rule.card_colors()) {
      if (   !Card(c, Card::ace).istrump(game)
          && hand.count(c) == 1
          && cards.contains(c, Card::ace)) {
        // ToDo: consider two colors with the same number of cards
        return Card(c, Card::ace);
      }
    }
    // search single ten
    for (auto const c : rule.card_colors()) {
      if (   !Card(c, Card::ten).istrump(game)
          && (hand.count(c) == 1)
          && (cards.contains(c, Card::ten))) {
        // ToDo: consider two colors with the same number of cards
        return Card(c, Card::ten);
      }
    } // for (c \in rule.card_colors())
  } // if (own team)

  // search the lowest / highest value
  Card::Value best_value = (own_team ? Card::nine : Card::ace);
  for (auto const& card : cards) {
    if (   !card.istrump()
        && (own_team
            ? card.value() > best_value
            : card.value() < best_value)
       ) {
      best_value = card.value();
    }
  } // for (c)

  // the card colors with the best value and not trump
  vector<Card::Color> card_colors;
  for (auto const c : rule.card_colors()) {
    if (   !Card(c, best_value).istrump(hand.game())
        && cards.contains(c, best_value) ) {
      card_colors.push_back(c);
    }
  }

  if (card_colors.empty())
    return Card::empty;


  if (best_value < Card::ace) {
    // search single card with best value
    for (auto const c : card_colors) {
      if (   (hand.count(c) == 1)
          && (cards.contains(c, best_value))) {
        // ToDo: consider two colors with the same number of cards
        return Card(c, best_value);
      }
    } // for (c : card_colors)
  } // if (best_value < Card::ace)

  if (best_value < Card::ten) {
    // search best card of the smallest color without ace and ten
    Card::Color best_color = Card::nocardcolor;
    for (auto const c : card_colors) {
      if (   cards.contains(c)
          && (hand.count(Card(c, Card::ace)) == 0)
          && (hand.count(Card(c, Card::ten)) == 0))
        if (   (best_color == Card::nocardcolor)
            || (cards.count(c) < hand.count(best_color)))
          // ToDo: improve selection -- card value comparison, compare remaining cards of the color, compare jabbing, ...
          best_color = c;
    } // for (c : card_colors)
    if (best_color != Card::nocardcolor) {
      return (own_team
              ? cards.highest_card(best_color)
              : cards.lowest_card(best_color));
    }
  } // if (best_value < Card::ten)

  if (best_value == Card::ace) {
    // search double ace
    for (auto const c : card_colors) {
      if (   cards.contains(c, best_value)
          && (hand.count(c, best_value) == 2) ) {
        return Card(c, best_value);
      }
    } // for (c : card_colors)
  } // if (best_value == Card::ace)

  if (best_value == Card::ten) {
    // search only tens
    for (auto const c : card_colors) {
      if (   cards.contains(c, best_value)
          && (hand.count(c)
              == hand.count(c, best_value)) ) {
        return Card(c, best_value);
      }
    }
  } // if (best_value == Card::ten)

  { // search best card of the biggest color with min 3 cards
    Card::Color best_color = Card::nocardcolor;
    unsigned best_cards_no = 2;
    for (auto const c : card_colors) {
      if (   (hand.count(c) > best_cards_no)
          && (cards.contains(c)) ) {
        best_color = c;
        best_cards_no = hand.count(c);
      }
    } // for (c : card_colors)
    if (   (best_color != Card::nocardcolor)
        && cards.contains(hand.lowest_card(best_color)) ) {
      // ToDo: take smallest card from cards
      return (own_team
              ? cards.highest_card(best_color)
              : cards.lowest_card(best_color));
    }
  } // search best card of the biggest color

  { // search best card of the largest color
    Card::Color best_color = Card::nocardcolor;
    for (auto const color : card_colors) {
      if (   cards.contains(color, best_value)
          && (   best_color == Card::nocardcolor
              || hand.count(color) > hand.count(best_color)))
        best_color = color;
    }
    if (best_color != Card::nocardcolor) {
      return (own_team
              ? cards.highest_card(best_color)
              : cards.lowest_card(best_color));
    }
  } // search best card

  return {};
}


/** -> result
 **
 ** @param    hi       heuristic interface
 ** @param    hand     hand of the player
 ** @param    cards    cards to choose from
 **
 ** @return   lowest card to play from 'cards'
 **/
Card
Gametree::choose_best_prefiltered_trump_card(Trick const& trick, Hand const& hand, HandCards const& cards) const
{
  DEBUG_ASSERTION(cards.count(Card::trump) == cards.size(),
                  "Gametree::choose_best_prefilterd_trump_card(hand, cards)\n"
                  "  'cards' does contain color cards");

  // whether the trick goes to the own team
  bool const own_team = (trick.winnerteam() == player().team());

  Game const& game = this->game();
  {
    auto const trumpcolor = game.cards().trumpcolor();
    if (own_team
        && (trumpcolor != Card::nocardcolor)) {
      if (cards.contains(trumpcolor, Card::ace))
        return Card(trumpcolor, Card::ace);
      if (cards.contains(trumpcolor, Card::ten))
        return Card(trumpcolor, Card::ten);
      if (cards.contains(trumpcolor, Card::king))
        return Card(trumpcolor, Card::king);
    }
  }

  HandCard card(hand);

  // find any card that's allowed for this trick
  for (auto const& c : cards) {
    if (c.istrump()) {
      card = c;
      break;
    }
  }

  if (!card)
    return {};

  // find a better card
  for (auto const& c : cards) {
    if (   c.is_jabbed_by(card)
        && c.istrump()
        && !c.isfox()
        && (   c.value() != Card::ten
            || card.isfox())
       ) {
      card = c;
    }
  }

  if(!card.is_jabbed_by(ai().trump_card_limit())) {
    // maybe a diamond ten isn't all that bad
    for (auto const& c : cards) {
      if (   c < card
          && c.istrump()
          && !c.isfox()
         ) {
        card = c;
      }
    }
  }

  if (!(card < ai().trump_card_limit())) {
    // maybe a diamond Ace isn't all that bad
    for (auto const& c : cards) {
      if (   c < card
          && c.istrump())
        card = c;
    }
  }

  return card;
}
