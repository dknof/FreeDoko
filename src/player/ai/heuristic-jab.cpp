/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "heuristic.h"
#include "ai.h"
#include "../cards_information.of_player.h"
#include "../team_information.h"

#include "../../game/game.h"
#include "../../party/rule.h"
#include "../../card/hand.h"
#include "../../card/trick.h"

/** @return   an optimized jabbing card
 **/
HandCard
Heuristic::optimized_jabbing_card(Trick const& trick, HandCard card)
{
  // Kreuz Dame behalten um sein Team zu zeigen
  if (   card == Card::club_queen
      && team() == Team::re
      && game().teaminfo().get(player()) != Team::re)
    return card;

  if (following_opposite_player_has(trick, card))
    return card;

  auto const& hand = this->hand();
  //auto const next_lower_card = hand.next_lower_card(card);
  auto const next_lower_card = [](Card const card) {
    return (  card == Card::club_queen    ? Card::spade_queen
            : card == Card::spade_queen   ? Card::heart_queen
            : card == Card::heart_queen   ? Card::diamond_queen
            : card == Card::diamond_queen ? Card::club_jack
            : card == Card::club_jack     ? Card::spade_jack
            : card == Card::spade_jack    ? Card::heart_jack
            : card == Card::heart_jack    ? Card::diamond_jack
            : Card::empty);
  };
  for (Card c = next_lower_card(card); c; c = next_lower_card(c)) {
    if (!trick.winnercard().is_jabbed_by(c))
      break;
    if (hand.contains(c))
      card = c;
    if (following_opposite_player_has(trick, c))
      break;
  }
  return card;
}

/** @return   the given card or a lower one, if they are equal in the sense of jabbing
 **/
HandCard
Heuristic::optimized_trump_to_jab(Card const card_,
                                  Trick const& trick,
                                  bool const only_same_points)
{
  if (!card_)
    return {};

  Hand const& hand     = this->hand();
  Game const& game     = this->game();
  Player const& player = this->player();
  HandCard const card(hand, card_);

  if (!card.istrump())
    return card;

  // do not optimize club queen in normal games
  if (   game.type() == GameType::normal
      && card == Card::club_queen
      && game.teaminfo().get(player) != Team::re
      && game.announcements().last(Team::re) == Announcement::noannouncement)
    return card;

  // optimize to a lower card, of there is no partner before the opposite team
  bool optimize_to_lower = false;
  if (trick.remainingcardno() <= 2)
    optimize_to_lower = true;
  if (guessed_same_team(trick.lastplayer()))
    optimize_to_lower = true;
  // only opposite team behind
  if (condition_only_opposite_or_unknown_player_behind(trick))
    optimize_to_lower = true;
  // I play the highest card in the game
  if (!cards_information().higher_card_exists(card))
    optimize_to_lower = true;
  // in a solo: the soloplayer is following directly
  if (   optimize_to_lower == false
      && is_solo(game.type())
      && trick.nextplayer().team() == Team::re)
    optimize_to_lower = true;

  if (!optimize_to_lower)
    return card;

  HandCard lower_card = card;
  auto const opposite_cards = valid_cards_behind_of_opposite_team(trick);
  bool const winnercard = trick.isjabbed(card);

  for (Card c_ = card;
       c_ != Card::empty;
       c_ = game.cards().next_lower_card(c_)) {
    auto const c = HandCard(hand, c_);
    if (   !card.is_special()
        && hand.contains(c)
        && c.is_special())
      continue;
    if (   only_same_points
        && c.value() != lower_card.value())
      break;
    // only optimize in jack/queen/king/special cards
    if (   c.value() != Card::jack
        && c.value() != Card::queen
        && !(   game.type() == GameType::solo_koehler
             && c.value() == Card::king)
        && !c.is_special())
      break;
    if (   winnercard
        && !trick.isjabbed(c))
      break;
    if (   count(opposite_cards, c) > 0
        && c.is_jabbed_by(c)) {
      break;
    }
    if (hand.contains(c)) {
      // save a charlie
      if (!(   (c == Card::charlie)
            && game.rule(Rule::Type::extrapoint_charlie)
            && (game.type() == GameType::normal) ) )
        lower_card = c;
    }
    if (count(opposite_cards, c) > 0) {
      // special case: club queen, but partner unknown
      if (!(   game.type() == GameType::normal
            && game.teaminfo().get(player) == Team::re
            && c == Card::club_queen))
        break;
    }
  } // for (c)

  return lower_card;
}

/** @return   a jabbing card for the last player
 **/
HandCard
Heuristic::jabbing_card_for_last_player(Trick const& trick)
{
  if (!hand().can_jab(trick)) {
    rationale_.add(_("Heuristic::return::cannot jab"));
    return {};
  }

  auto const startcolor = trick.startcard().tcolor();
  if (   startcolor != Card::trump
      && hand().contains(startcolor)) {
    return jabbing_card_for_last_player(trick, startcolor);
  }
  auto const card = jabbing_card_for_last_player_trump(trick);
  if (   card == Card::spade_queen
      && game().type() == GameType::normal
      && hand().contains(Card::club_queen)
      && team() == Team::re
      && game().teaminfo().get(player()) != Team::re) {
    rationale_.add(_("Heuristic::jab::prefer club queen in order to show my team"));
    return HandCard(hand(), Card::club_queen);
  }
  return card;
}

/** @return   a jabbing card for 'color'
 **/
HandCard
Heuristic::jabbing_card_for_last_player(Trick const& trick, Card::Color const color)
{
  auto const card = hand().highest_card(color);
  if (!trick.isjabbed(card))
    return {};
  rationale_.add(_("Heuristic::jab::highest color card: %s", _(card)));
  return card;
}

/** @return   a jabbing card for trump
 **/
HandCard
Heuristic::jabbing_card_for_last_player_trump(Trick const& trick)
{
  if (!is_with_trump_color(game().type()))
    return {};
  auto const& hand      = this->hand();
  auto const& game      = this->game();
  auto const trumpcolor = game.cards().trumpcolor();
  for (auto const card_ : {
       Card(trumpcolor, Card::ace),
       Card(trumpcolor, Card::ten),
       Card(trumpcolor, Card::king),
       Card::diamond_jack,
       Card::heart_jack,
       Card::spade_jack,
       }) {
    auto const card = HandCard(hand, card_);
    if (   hand.contains(card)
        && !card.is_special()
        && trick.isjabbed(card)) {
      rationale_.add(_("Heuristic::jab::small trump with many points: %s", _(card)));
      return HandCard(hand, card);
    }
  }
  auto const card = hand.next_jabbing_card(trick.winnercard());
  rationale_.add(_("Heuristic::jab::low jabbing card: %s", _(card)));
  return card;
}


/** -> result
 **
 ** @param    trick     current trick
 ** @param    hi        heuristic interface
 ** @param    jab_always   whether to jab always -- else return empty if jabbing is senseless (default: false)
 **
 ** @return   the "best" card to jab the trick
 **
 ** @todo      aiconfig aggressive
 **/
HandCard
Heuristic::best_jabbing_card(Trick const& trick,
                             bool const jab_always)
{
  // notes
  // * first trick: duty announcement
  // * color trick: not below a fox
  // * re: fox must not be saved (beware of dullen)
  // * small points: close overjab
  // * as contra: take heart instead of spade queen, since re will overjab with club queen
  // * high jabbing, also without a 10 in the trick: check remaining points
  // * overjab, so that the last player cannot pfund the partner
  // * check the position of the card according to the hand
  // * check the position of the card according to the remaining cards
  // * check few trumps (don't play the fox free)
  // * regard announcement of partner (take a high card)
  // * ensure announcement / winning
  //   -> heuristic 'get trick for announcement'
  // * senseless: overjab the partner close

  // card decision
  // a) narrow jabbing
  // b) high jabbing
  // c) medium
  // d) show team
  // e) save points (few trump)

  if (trick.isempty())
    return {};

  auto const& hand = this->hand();
  if (!hand.can_jab(trick))
    return {};

  auto const& game = this->game();
  // card to choose
  auto card = HandCard(hand);


  // 1) startplayer
  // 2) second player
  // 3) second last player
  // 4) last player

  // 4) / 3) + partner behind
  if (   trick.islastcard()
      || (   trick.remainingcardno() == 2
          && guessed_same_team(trick.lastplayer()) )
     ) {
    // ToDo: check for duty announcement
    // color ace / ten
    if (   !trick.startcard().istrump()
        && hand.contains(trick.startcard().tcolor()))
      card = hand.highest_card(trick.startcard().tcolor());
    if (!card) {
      // trump ace (fox) / trump ten / trump king
      auto cards = hand.cards_at_max_fox();
      if (hand.contains(Card::diamond_jack))
        cards.push_back(Card::diamond_jack);
      for (auto const& c : cards) {
        if (trick.isjabbed(c)) {
          card = c;
          break;
        }
      }
    } // if (trump is a color)
    // lowest winning card
    if (!card) {
      card = hand.next_higher_card(trick.winnercard());
    }
  } // 4) / 3) + partner behind
  else { // 2), 3)
    // 2), 3), color trick, has color
    if (!trick.startcard().istrump()) { //color trick
      Card::Color const color = trick.startcard().color();
      if (hand.contains(color)) {
        // highest card
        // ToDo: check whether to spare an ace
        card = hand.highest_card(color);
      } else if (color_runs(color) == 0) {
        // 2), 3), color trick, no color

        // first round, no color
        // ToDo: check whether another player has thrown the color / has the color in the hand
        // trump ace (fox) / trump ten / trump king
        {
          if (!(   (cards_information().remaining(color)
                    >= trick.remainingcardno())
                && (   guessed_same_team(trick.lastplayer())
                    || guessed_hand(trick.lastplayer()).contains(color))
                && (   (trick.actcardno() == 2)
                    || guessed_same_team(trick.player_of_card(2))
                    || guessed_hand(trick.player_of_card(2)).contains(color)))
             ) {
            // if the trick can not get through, take a high card
            // ToDo: check short colors (without nines, dulle)
            card = hand.same_or_higher_card(trump_card_limit());
            if (!card)
              card = hand.same_or_higher_card(lowest_trump_card_limit());
            if (!card)
              card = hand.same_or_higher_card(Card::diamond_jack);
          }

          if (!card) {
            if (!(   (   !guessed_same_team(trick.lastplayer())
                      && cards_information(trick.lastplayer()).played(color))
                  || (   (trick.actcardno() == 2)
                      && !guessed_same_team(trick.player_of_card(2))
                      && cards_information(trick.player_of_card(2)).played(color))
                 )) {
              // only play below diamond jack, if no opposite player has played the color before
              for (auto const& c : hand.cards_at_max_fox()) {
                if (trick.isjabbed({hand, c})) {
                  card = c;
                  break;
                }
              }
            }
          }

          if (   !card
              && !trick.winnercard().istrump()) {
            card = hand.same_or_higher_card(Card::diamond_jack);
          }
        } // if (trump is a color)

        if (   !card
            || !trick.isjabbed(card)) {
          card = hand.next_higher_card(trick.winnercard());
        }

        // show with club queen
        if (   game.type() == GameType::normal
            && game.cards().less(Card::charlie, card)
            && team() == Team::re
            && game.teaminfo().get(player()) != Team::re
            && hand.contains(Card::club_queen)
            && trick.isjabbed(Card::club_queen, hand)) {
          card = Card::club_queen;
        }

      } else { // 2), 3), color trick (other cases)
        // ToDo: jack / queen
        // check how many points are still out
        // check whether another player has thrown the color / has the color in the hand
        if (   (trick.points() >= 10)
            || (   cards_information().remaining_others(color, Card::ace)
                && !Card(color, Card::ace).istrump(game))

            || (   cards_information().remaining_others(color, Card::ten)
                && !Card(color, Card::ten).istrump(game)) ) {
          // make a very high jab
          if (   trick.actcardno() == 1
              &&  trick.points() >= 10) {
            card = hand.highest_trump();
          }
          if (   trick.actcardno() == 2
              && trick.points() >= 20
              && guessed_hand(trick.lastplayer()).count(color) <= 2) {
            card = hand.highest_trump();
          }
          // make a high jab
          if (   !card
              && cards_information().color_runs(trick.startcard().tcolor()) > 1
              && (   trick.points() >= 14
                  || condition_only_opposite_or_unknown_player_behind(trick) )
             ) {
            card = hand.same_or_lower_card(Card::club_queen);
          }
          if (!card) {
            card = hand.same_or_higher_card(lowest_trump_card_limit());
          }
        } else if (   trick.actcardno() == 1
                   && guessed_same_team(trick.startplayer()) ) {
          // jab high, if both opposite players are behind
          card = hand.same_or_higher_card(trump_card_limit());
        }

        if (!card) {
          vector<HandCard> cards;
          // ToDo: check short colors (without nines, dulle)
          if (cards_information().remaining(color)
              >= trick.remainingcardno()) {
            auto const trumpcolor = game.cards().trumpcolor();
            cards.emplace_back(hand, trumpcolor, Card::ace);
            cards.emplace_back(hand, trumpcolor, Card::ten);
            cards.emplace_back(hand, trumpcolor, Card::king);
          }
          cards.emplace_back(hand, Card::diamond_jack);
          for (auto const& c : cards) {
            if (   hand.contains(c)
                && !c.is_special()
                && trick.isjabbed(c)) {
              card = c;
              break;
            }
          }
        }
        if (!card) {
          // not many points or no high trump
          card = hand.same_or_higher_card(lowest_trump_card_limit());
        }
        if (   !card
            || !trick.isjabbed(card) ) {
          card = hand.next_higher_card(trick.winnercard());
        }

      } // cases in color trick
    } else { // if !(color trick)
      if (trick.actcardno() == game.playerno() - 1) {
        // 3), opposite team behind
        if (static_cast<int>(trick.points())
            >= value(Aiconfig::Type::limitqueen)) {
          card = hand.same_or_higher_card(trump_card_limit());
        }
      } else { // if !3 (opposite team behind)

        if (   guessed_same_team(trick.player_of_card(2))
            && !guessed_same_team(trick.player_of_card(3)) ) {
          // 2), 3: same team, 4: opposite team
          // take a high card (over trump card limit)
          // or the lowest winning card
          if (   (static_cast<int>(trick.points())
                  >= value(Aiconfig::Type::limitdulle))
              || (   (static_cast<int>(trick.points() >= 10))
                  && (hand.higher_cards_no(Card::club_queen)
                      >= 2) ) ) {
            card = hand.highest_trump();
          }
          if (!card) {
            card = hand.same_or_higher_card(trump_card_limit());
            if (   !card
                || !trick.isjabbed(card)
                || game.cards().less(Card::club_queen, card)) {
              card = hand.next_higher_card(Card::charlie);
            }
            if (!trick.isjabbed(card)) {
              card = hand.next_higher_card(trick.winnercard());
            }
          } // if (!card)
        } // 2), 3: same team, 4: opposite team
        else if (   !guessed_same_team(trick.player_of_card(2))
                 && guessed_same_team(trick.player_of_card(3)) ) {
          // 2), 3: opposite team, 4: same team
          // take lowest winning card
          card = hand.next_jabbing_card(trick.winnercard());
          while (  (   (card.value() >= 10)
                    || (   (   !game.is_real_solo()
                            || is_color_solo(game.type()) )
                        && HandCard(hand, card).is_at_max_fox()))
                 && !HandCard(hand, card).is_special()
                 && card)
          {
            card = hand.next_higher_card(card);
          }
          if (!card)
            card = hand.next_jabbing_card(trick.winnercard());
        } // 2), 3: opposite team, 4: same team
        else { // 2, two times opposite team behind
          // take the highest card, if the partner has played ten points
          if (trick.points() >= 10) {
            card = hand.highest_trump();
            // do not take a dulle when it can be jabbed
            if (   game.rule(Rule::Type::dullen_second_over_first)
                && (card == Card::dulle)
                && guessed_hand(trick.player_of_card(2)).contains(Card::dulle)
                && guessed_hand(trick.player_of_card(3)).contains(Card::dulle)
               ) {
              card = hand.same_or_lower_card(Card::club_queen);
              if (   (card.value() != Card::queen)
                  && (card.value() != Card::jack))
                card = Card::empty;
            }
          } // if (trick.points() >= 10)
          // if there is an announcement higher then 'no 120', then take the highest queen
          if (game.announcements().last(team()) > Announcement::no120) {
            card = hand.same_or_lower_card(Card::club_queen);
            if (   (card.value() != Card::queen)
                && (card.value() != Card::jack))
              card = Card::empty;
          }
          if (!card) {
            // take lowest queen, the highest jack or the lowest winnercard
            card = hand.same_or_higher_card(Card::diamond_queen);
            if (   !card
                || !trick.isjabbed(card)
                || game.cards().less(Card::club_queen, card)) {
              card = hand.same_or_lower_card(Card::club_jack);
              if (card.value() != Card::jack)
                card = Card::empty;
            }
          }
          if (   !card
              || !trick.isjabbed(card)) {
            card = hand.next_higher_card(trick.winnercard());
          }
        } // 2), two times opposite team behind
      } // if !3 (opposite team behind)
    } // if !(color trick)
  } // 2), 3)

  { // special case: dulle
    if (   !card
        && (trick.winnercard() == Card::dulle)
        && game.rule(Rule::Type::dullen_second_over_first)
        && hand.contains(Card::dulle))
      card = Card::dulle;

    Card const card_bak = card;
    // do not play a dulle / swine if the opposite player can overjab
    if ( (  (card == Card::dulle)
          || HandCard(hand, card).isswine())
        && (   ( (trick.actcardno() <= 2)
                ? (   !guessed_same_team(trick.lastplayer())
                   && guessed_hand(trick.lastplayer()).higher_card_exists(card))
                : false)
            || ( (trick.actcardno() <= 1)
                ? (   !guessed_same_team(trick.player_of_card(2))
                   && guessed_hand(trick.player_of_card(2)).higher_card_exists(card))
                : false) )
       ) {
      card = hand.next_lower_card(card);
      if (!trick.isjabbed(card))
        card = card_bak;
    }
  } // special case: dulle

  // ToDo: check for using a special card
  card = optimized_trump_to_jab(card, trick);

  // prefer a swine over a dulle with the rule 'dullen second over first'
  if (   card.isdulle()
      && game.rule(Rule::Type::dullen_second_over_first)
      && (   ( (trick.actcardno() <= 2)
              ? (   !guessed_same_team(trick.lastplayer())
                 && guessed_hand(trick.lastplayer()).contains(Card::dulle))
              : false)
          || ( (trick.actcardno() <= 1)
              ? (   !guessed_same_team(trick.player_of_card(2))
                 && guessed_hand(trick.player_of_card(2)).contains(Card::dulle))
              : false) )
     ) {
    if (hand.next_higher_card(card) != Card::empty) {
      card = hand.next_higher_card(card);
      return card;
    }
  }

  // with swines try to save a dulle
  if (   hand.count_dulle()
      && trick.isvalid(Card::dulle, hand)
      && trick.isjabbed({hand, Card::dulle})
      && card.is_jabbed_by(game.cards().dulle())
      && (   (   game.swines().swines_announced()
              && !player().has_swines()
              && !guessed_same_team(game.swines().swines_owner())
              && trick.has_played(game.swines().swines_owner()))
          ||   (   game.swines().hyperswines_announced()
                && !player().has_hyperswines()
                && !guessed_same_team(game.swines().hyperswines_owner())
                && trick.has_played(game.swines().hyperswines_owner()))
         ) ) {
    unsigned n = 2;
    if (   game.swines().swines_announced()
        && !player().has_swines()
        && !guessed_same_team(game.swines().swines_owner())) {
      n += cards_information().remaining(game.cards().swine());
    }
    if (   game.swines().hyperswines_announced()
        && !player().has_hyperswines()
        && !guessed_same_team(game.swines().hyperswines_owner())) {
      n += cards_information().remaining(game.cards().hyperswine());
    }
    if (hand.count_fehl() <= 2)
      n += 1;
    else if (trick.islastcard())
      n += 1;
    if (hand.count(Card::trump) <= (hand.count_dulle() + n) ) {
      card = Card::dulle;
      return card;
    }
  } // if (game.swines_owner() && hand.count_dulle())

  // check whether to show the team (club queen)
  if (   game.type() == GameType::normal
      // ToDo: check the own gameplay for team informations
      && game.teaminfo().get(player()) == Team::unknown
      && !team_information().all_known()
      && team() == Team::re
      && !game.announcements().last(Team::re)
      && hand.contains(Card::club_queen)
      && trick.isvalid(Card::club_queen, hand)
      && trick.isjabbed({hand, Card::club_queen})) {
    if (   (card == Card::spade_queen)
        || (card == Card::heart_queen)
        || (   (card == Card::diamond_queen)
            && !cards_information().remaining_others(Card::heart_queen)
            && !cards_information().remaining_others(Card::spade_queen)
           )
       ) {
      card = Card::club_queen;
      rationale_.add(_("Heuristic::jab::prefer club queen in order to show my team"));
      return card;
    }
    if (   !trick.startcard().istrump()
        && color_runs(trick.startcard().tcolor()) > 0
        && !trick.islastcard()
        && guessed_same_team(trick.lastplayer())) {
      card = Card::club_queen;
      rationale_.add(_("Heuristic::jab::prefer club queen in order to show my team"));
      return card;
    }
  } // if (re, not shown)

  if (!jab_always) {
    // do not take 10 points (fox, dulle, swine) if the player behind can overjab
    if (   (card.points() >= 10)
        && card.istrump()
        && !(   !trick.startcard().istrump()
             && cards_information().color_runs(trick.startcard().color()) == 0)
        && (   ( (trick.actcardno() <= 2)
                ? (   !guessed_same_team(trick.lastplayer())
                   && guessed_hand(trick.lastplayer()).higher_card_exists(card))
                : false)
            || ( (trick.actcardno() <= 1)
                ? (   !guessed_same_team(trick.player_of_card(2))
                   && guessed_hand(trick.player_of_card(2)).higher_card_exists(card))
                : false) ) ) {
      card = Card::empty;
    }

  } // if (!jab_always)

  return card;
}
