/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "heuristic.h"
#include "ai.h"
#include "../player.h"
#include "../cards_information.of_player.h"

#include "heuristics/bug_report.h"
#include "heuristics/only_one_valid_card.h"
#include "heuristics/valid_card.h"
#include "heuristics/first_card/draw_trump.h"
#include "heuristics/first_card/draw_remaining_trump.h"
#include "heuristics/first_card/draw_trump_for_team_announcement.h"
#include "heuristics/first_card/partner_backhand_draw_trump.h"
#include "heuristics/first_card/play_bad_color.h"
#include "heuristics/first_card/play_color_for_last_player_to_jab.h"
#include "heuristics/first_card/play_color_for_partner_ace.h"
#include "heuristics/first_card/play_color_for_partner_to_jab.h"
#include "heuristics/first_card/play_last_trumps.h"
#include "heuristics/first_card/play_to_jab_later.h"
#include "heuristics/first_card/remaining_tricks_to_me.h"
#include "heuristics/first_card/retry_color.h"
#include "heuristics/first_card/start_with_color.h"
#include "heuristics/first_card/start_with_color_double_ace.h"
#include "heuristics/first_card/start_with_color_single_ace.h"
#include "heuristics/first_card/start_with_color_blank_ace.h"
#include "heuristics/first_card/start_with_color_ten.h"
#include "heuristics/first_card/start_with_low_color.h"
#include "heuristics/first_card/start_with_low_trump.h"
#include "heuristics/first_card/start_with_lowest_color.h"
#include "heuristics/first_card/start_with_lowest_trump.h"
#include "heuristics/first_card/start_with_trump_for_partner_color_ace.h"
#include "heuristics/jab/jab_color_over_fox.h"
#include "heuristics/jab/jab_color_trick_high.h"
#include "heuristics/jab/jab_first_color_run.h"
#include "heuristics/jab/jab_for_ace.h"
#include "heuristics/jab/jab_fox.h"
#include "heuristics/jab/jab_rich_trick.h"
#include "heuristics/jab/jab_second_color_run.h"
#include "heuristics/jab/jab_small_color_card.h"
#include "heuristics/jab/jab_swine_with_hyperswine.h"
#include "heuristics/jab/jab_to_win.h"
#include "heuristics/jab/jab_to_not_loose.h"
#include "heuristics/jab/jab_trump_over_fox.h"
#include "heuristics/jab/jab_trump_because_of_partner_announcement.h"
#include "heuristics/jab/jab_trump_high_because_of_partner_announcement.h"
#include "heuristics/jab/jab_trump_high_after_opponent_announcement.h"
#include "heuristics/jab/jab_with_color_ace.h"
#include "heuristics/jab/jab_with_color_ten.h"
#include "heuristics/jab/jab_with_highest_trump.h"
#include "heuristics/jab/jab_with_lowest_card.h"
#include "heuristics/jab/jab_with_lowest_card_below_trump_card_limit.h"
#include "heuristics/jab/jab_with_relative_low_trump.h"
#include "heuristics/jab/second_last_jab_for_following_pfund.h"
#include "heuristics/jab/jab_with_opponent_backhand.h"
#include "heuristics/last_card/grab_trick.h"
#include "heuristics/last_card/jab_for_doppelkopf.h"
#include "heuristics/last_card/jab_for_points.h"
#include "heuristics/last_card/jab_with_trump_ace_or_ten.h"
#include "heuristics/last_card/pass_small_trick.h"
#include "heuristics/pfund/pfund_high_for_sure.h"
#include "heuristics/pfund/pfund_high_for_unsure.h"
#include "heuristics/pfund/pfund_for_sure.h"
#include "heuristics/pfund/pfund_for_unsure.h"
#include "heuristics/pfund/pfund_before_partner.h"
#include "heuristics/pfund/pfund_in_first_color_run.h"
#include "heuristics/pfund/start_with_trump_pfund.h"
#include "heuristics/cannot_jab.h"
#include "heuristics/gametree.h"
#include "heuristics/create_blank_color.h"
#include "heuristics/high_after_partner_low.h"
#include "heuristics/play_for_partner_worries.h"
#include "heuristics/play_highest_color.h"
#include "heuristics/play_trump.h"
#include "heuristics/save_dulle.h"
#include "heuristics/save_dulle_from_swines.h"
#include "heuristics/save_swines_from_hyperswines.h"
#include "heuristics/serve_color_trick.h"
#include "heuristics/serve_partner_with_lowest_card.h"
#include "heuristics/serve_trump_trick.h"
#include "heuristics/serve_with_lowest_card.h"
#include "heuristics/try_for_doppelkopf.h"

#include "heuristics/marriage/start_to_marry.h"
#include "heuristics/marriage/jab_to_marry.h"
#include "heuristics/marriage/start_to_get_married.h"
#include "heuristics/marriage/start_to_get_married_last_chance.h"
#include "heuristics/marriage/pfund_in_marriage_determination_trick.h"

#include "heuristics/poverty/special/start_with_trump.h"
#include "heuristics/poverty/special/play_pfund.h"
#include "heuristics/poverty/special/give_no_points.h"
#include "heuristics/poverty/special/offer_pfund.h"
#include "heuristics/poverty/re/jab_as_last_player.h"
#include "heuristics/poverty/re/jab_color_trick_with_trump.h"
#include "heuristics/poverty/re/start_with_trump.h"
#include "heuristics/poverty/contra/start_with_color.h"
#include "heuristics/poverty/contra/jab_color_trick_with_trump.h"
#include "heuristics/poverty/contra/leave_to_partner.h"
#include "heuristics/poverty/contra/overjab_re.h"
#include "heuristics/poverty/contra/pfund_high_for_unsure.h"
#include "heuristics/poverty/contra/pfund_high_for_sure.h"
#include "heuristics/poverty/contra/pfund_for_unsure.h"
#include "heuristics/poverty/contra/pfund_for_sure.h"
#include "heuristics/poverty/contra/pfund_before_partner.h"

#include "heuristics/solo/color/jab_for_ace.h"
#include "heuristics/solo/color/contra/overjab_to_get_partner_backhand.h"
#include "heuristics/solo/color/contra/pfund_in_first_color_run.h"
#include "heuristics/solo/color/contra/pfund_for_sure.h"
#include "heuristics/solo/color/contra/pfund_for_unsure.h"
#include "heuristics/solo/color/contra/pfund_high_for_sure.h"
#include "heuristics/solo/color/contra/pfund_high_for_unsure.h"
#include "heuristics/solo/color/contra/play_color_in_solo.h"

#include "heuristics/solo/picture/pull_down_color.h"
#include "heuristics/solo/picture/start_with_highest_color.h"
#include "heuristics/solo/picture/draw_trumps.h"
#include "heuristics/solo/picture/serve_trump_trick.h"
#include "heuristics/solo/picture/play_last_trumps.h"
#include "heuristics/solo/picture/jab_color_trick_for_sure.h"
#include "heuristics/solo/picture/re/get_first_trumps.h"
#include "heuristics/solo/picture/re/get_last_trumps.h"
#include "heuristics/solo/picture/re/get_trumps_before_blank_color_ace.h"
#include "heuristics/solo/picture/re/start_with_winning_card_before_bad_loosing_cards.h"
#include "heuristics/solo/picture/re/start_with_loosing_solo_color.h"
#include "heuristics/solo/picture/contra/discard_color_card.h"
#include "heuristics/solo/picture/contra/pfund_high_for_sure.h"
#include "heuristics/solo/picture/contra/pfund_high_for_unsure.h"
#include "heuristics/solo/picture/contra/jab_trump_trick.h"
#include "heuristics/solo/picture/contra/jab_color_trick.h"
#include "heuristics/solo/picture/contra/jab_with_high_color_card.h"
#include "heuristics/solo/picture/contra/pfund_for_sure.h"
#include "heuristics/solo/picture/contra/pfund_for_unsure.h"

#include "heuristics/solo/meatless/play_highest_color_card_in_game.h"
#include "heuristics/solo/meatless/play_blank_color_of_soloplayer.h"
#include "heuristics/solo/meatless/pull_down_color.h"
#include "heuristics/solo/meatless/retry_last_color.h"
#include "heuristics/solo/meatless/serve_color_trick.h"
#include "heuristics/solo/meatless/start_with_best_color.h"
#include "heuristics/solo/meatless/re/serve_trick_re.h"
#include "heuristics/solo/meatless/contra/overjab_re.h"
#include "heuristics/solo/meatless/contra/pfund_before_partner.h"
#include "heuristics/solo/meatless/contra/pfund_for_sure.h"
#include "heuristics/solo/meatless/contra/play_color_for_partner_ace.h"
#include "heuristics/solo/meatless/contra/serve_trick_contra.h"


/** @return   a heuristic object
 **/
unique_ptr<Heuristic>
Heuristic::create(Aiconfig::Heuristic const heuristic,
		  Ai const& ai)
{
  switch (heuristic) {
  case Aiconfig::Heuristic::error:
  case Aiconfig::Heuristic::no_heuristic:
  case Aiconfig::Heuristic::manual:
    DEBUG_ASSERTION(false,
                    "Heuristic::create(" << heuristic << ")\n"
                    "  heuristic '" << heuristic << "' is not valid");
    return {};
  case Aiconfig::Heuristic::bug_report:
    return make_unique<Heuristics::BugReport>(ai);
  case Aiconfig::Heuristic::only_one_valid_card:
    return make_unique<Heuristics::OnlyOneValidCard>(ai);
  case Aiconfig::Heuristic::valid_card:
    return make_unique<Heuristics::ValidCard>(ai);
  case Aiconfig::Heuristic::gametree:
    return make_unique<Heuristics::Gametree>(ai);

  case Aiconfig::Heuristic::remaining_tricks_to_me:
    return make_unique<Heuristics::RemainingTricksToMe>(ai);
  case Aiconfig::Heuristic::start_with_color_blank_ace:
    return make_unique<Heuristics::StartWithColorBlankAce>(ai);
  case Aiconfig::Heuristic::start_with_color_single_ace:
    return make_unique<Heuristics::StartWithColorSingleAce>(ai);
  case Aiconfig::Heuristic::start_with_color_double_ace:
    return make_unique<Heuristics::StartWithColorDoubleAce>(ai);
  case Aiconfig::Heuristic::start_with_color_ten:
    return make_unique<Heuristics::StartWithColorTen>(ai);
  case Aiconfig::Heuristic::start_with_lowest_color:
    return make_unique<Heuristics::StartWithLowestColor>(ai);
  case Aiconfig::Heuristic::start_with_lowest_trump:
    return make_unique<Heuristics::StartWithLowestTrump>(ai);
  case Aiconfig::Heuristic::jab_with_color_ace:
    return make_unique<Heuristics::JabWithColorAce>(ai);
  case Aiconfig::Heuristic::jab_with_color_ten:
    return make_unique<Heuristics::JabWithColorTen>(ai);
  case Aiconfig::Heuristic::pfund_in_first_color_run:
    return make_unique<Heuristics::PfundInFirstColorRun>(ai);
  case Aiconfig::Heuristic::jab_first_color_run:
    return make_unique<Heuristics::JabFirstColorRun>(ai);
  case Aiconfig::Heuristic::jab_second_color_run:
    return make_unique<Heuristics::JabSecondColorRun>(ai);
  case Aiconfig::Heuristic::jab_color_over_fox:
    return make_unique<Heuristics::JabColorOverFox>(ai);
  case Aiconfig::Heuristic::jab_color_trick_high:
    return make_unique<Heuristics::JabColorTrickHigh>(ai);
  case Aiconfig::Heuristic::jab_rich_trick:
    return make_unique<Heuristics::JabRichTrick>(ai);
  case Aiconfig::Heuristic::jab_small_color_card:
    return make_unique<Heuristics::JabSmallColorCard>(ai);
  case Aiconfig::Heuristic::jab_trump_over_fox:
    return make_unique<Heuristics::JabTrumpOverFox>(ai);
  case Aiconfig::Heuristic::jab_trump_because_of_partner_announcement:
    return make_unique<Heuristics::JabTrumpBecauseOfPartnerAnnouncement>(ai);
  case Aiconfig::Heuristic::jab_trump_high_because_of_partner_announcement:
    return make_unique<Heuristics::JabTrumpHighBecauseOfPartnerAnnouncement>(ai);
  case Aiconfig::Heuristic::jab_trump_high_after_opponent_announcement:
    return make_unique<Heuristics::JabTrumpHighAfterOpponentAnnouncement>(ai);
  case Aiconfig::Heuristic::jab_with_highest_trump:
    return make_unique<Heuristics::JabWithHighestTrump>(ai);
  case Aiconfig::Heuristic::jab_with_lowest_card:
    return make_unique<Heuristics::JabWithLowestCard>(ai);
  case Aiconfig::Heuristic::jab_with_lowest_card_below_trump_card_limit:
    return make_unique<Heuristics::JabWithLowestCardBelowTrumpCardLimit>(ai);
  case Aiconfig::Heuristic::jab_with_relative_low_trump:
    return make_unique<Heuristics::JabWithRelativeLowTrump>(ai);
  case Aiconfig::Heuristic::high_after_partner_low:
    return make_unique<Heuristics::HighAfterPartnerLow>(ai);
  case Aiconfig::Heuristic::second_last_jab_for_following_pfund:
    return make_unique<Heuristics::SecondLastJabForFollowingPfund>(ai);
  case Aiconfig::Heuristic::jab_with_opponent_backhand:
    return make_unique<Heuristics::JabWithOpponentBackhand>(ai);
  case Aiconfig::Heuristic::serve_with_lowest_card:
    return make_unique<Heuristics::ServeWithLowestCard>(ai);
  case Aiconfig::Heuristic::serve_partner_with_lowest_card:
    return make_unique<Heuristics::ServePartnerWithLowestCard>(ai);
  case Aiconfig::Heuristic::start_with_color:
    return make_unique<Heuristics::StartWithColor>(ai);
  case Aiconfig::Heuristic::start_with_low_color:
    return make_unique<Heuristics::StartWithLowColor>(ai);
  case Aiconfig::Heuristic::start_with_low_trump:
    return make_unique<Heuristics::StartWithLowTrump>(ai);
  case Aiconfig::Heuristic::retry_color:
    return make_unique<Heuristics::RetryColor>(ai);
  case Aiconfig::Heuristic::start_with_trump_for_partner_color_ace:
    return make_unique<Heuristics::StartWithTrumpForPartnerColorAce>(ai);
  case Aiconfig::Heuristic::start_with_trump_pfund:
    return make_unique<Heuristics::StartWithTrumpPfund>(ai);
  case Aiconfig::Heuristic::play_color_for_partner_to_jab:
    return make_unique<Heuristics::PlayColorForPartnerToJab>(ai);
  case Aiconfig::Heuristic::play_color_for_last_player_to_jab:
    return make_unique<Heuristics::PlayColorForLastPlayerToJab>(ai);
  case Aiconfig::Heuristic::play_color_for_partner_ace:
    return make_unique<Heuristics::PlayColorForPartnerAce>(ai);
  case Aiconfig::Heuristic::play_bad_color:
    return make_unique<Heuristics::PlayBadColor>(ai);
  case Aiconfig::Heuristic::save_dulle:
    return make_unique<Heuristics::SaveDulle>(ai);
  case Aiconfig::Heuristic::save_dulle_from_swines:
    return make_unique<Heuristics::SaveDulleFromSwines>(ai);
  case Aiconfig::Heuristic::save_swines_from_hyperswines:
    return make_unique<Heuristics::SaveSwinesFromHyperswines>(ai);
  case Aiconfig::Heuristic::jab_swine_with_hyperswine:
    return make_unique<Heuristics::JabSwineWithHyperswine>(ai);
  case Aiconfig::Heuristic::serve_color_trick:
    return make_unique<Heuristics::ServeColorTrick>(ai);
  case Aiconfig::Heuristic::serve_trump_trick:
    return make_unique<Heuristics::ServeTrumpTrick>(ai);
  case Aiconfig::Heuristic::pfund_high_for_sure:
    return make_unique<Heuristics::PfundHighForSure>(ai);
  case Aiconfig::Heuristic::pfund_for_sure:
    return make_unique<Heuristics::PfundForSure>(ai);
  case Aiconfig::Heuristic::pfund_high_for_unsure:
    return make_unique<Heuristics::PfundHighForUnsure>(ai);
  case Aiconfig::Heuristic::pfund_for_unsure:
    return make_unique<Heuristics::PfundForUnsure>(ai);
  case Aiconfig::Heuristic::pfund_before_partner:
    return make_unique<Heuristics::PfundBeforePartner>(ai);
  case Aiconfig::Heuristic::jab_for_ace:
    return make_unique<Heuristics::JabForAce>(ai);
  case Aiconfig::Heuristic::create_blank_color:
    return make_unique<Heuristics::CreateBlankColor>(ai);
  case Aiconfig::Heuristic::play_trump:
    return make_unique<Heuristics::PlayTrump>(ai);
  case Aiconfig::Heuristic::jab_fox:
    return make_unique<Heuristics::JabFox>(ai);
  case Aiconfig::Heuristic::jab_for_doppelkopf:
    return make_unique<Heuristics::JabForDoppelkopf>(ai);
  case Aiconfig::Heuristic::try_for_doppelkopf:
    return make_unique<Heuristics::TryForDoppelkopf>(ai);
  case Aiconfig::Heuristic::play_for_partner_worries:
    return make_unique<Heuristics::PlayForPartnerWorries>(ai);
  case Aiconfig::Heuristic::partner_backhand_draw_trump:
    return make_unique<Heuristics::PartnerBackhandDrawTrump>(ai);
  case Aiconfig::Heuristic::draw_trump:
    return make_unique<Heuristics::DrawTrump>(ai);
  case Aiconfig::Heuristic::draw_remaining_trump:
    return make_unique<Heuristics::DrawRemainingTrump>(ai);
  case Aiconfig::Heuristic::draw_trump_for_team_announcement:
    return make_unique<Heuristics::DrawTrumpForTeamAnnouncement>(ai);
  case Aiconfig::Heuristic::play_to_jab_later:
    return make_unique<Heuristics::PlayToJabLater>(ai);
  case Aiconfig::Heuristic::play_highest_color_card_in_game:
    return make_unique<Heuristics::PlayHighestColor>(ai);
  case Aiconfig::Heuristic::jab_to_win:
    return make_unique<Heuristics::JabToWin>(ai);
  case Aiconfig::Heuristic::jab_to_not_loose:
    return make_unique<Heuristics::JabToNotLoose>(ai);
  case Aiconfig::Heuristic::grab_trick:
    return make_unique<Heuristics::GrabTrick>(ai);
  case Aiconfig::Heuristic::last_player_jab_with_trump_ace_or_ten:
    return make_unique<Heuristics::LastPlayerJabWithTrumpAceOrTen>(ai);
  case Aiconfig::Heuristic::last_player_jab_for_points:
    return make_unique<Heuristics::LastPlayerJabForPoints>(ai);
  case Aiconfig::Heuristic::last_player_pass_small_trick:
    return make_unique<Heuristics::LastPlayerPassSmallTrick>(ai);
  case Aiconfig::Heuristic::cannot_jab:
    return make_unique<Heuristics::CannotJab>(ai);
  case Aiconfig::Heuristic::play_last_trumps:
    return make_unique<Heuristics::PlayLastTrumps>(ai);

  case Aiconfig::Heuristic::start_to_marry:
    return make_unique<Heuristics::StartToMarry>(ai);
  case Aiconfig::Heuristic::jab_to_marry:
    return make_unique<Heuristics::JabToMarry>(ai);
  case Aiconfig::Heuristic::start_to_get_married:
    return make_unique<Heuristics::StartToGetMarried>(ai);
  case Aiconfig::Heuristic::start_to_get_married_last_chance:
    return make_unique<Heuristics::StartToGetMarriedLastChance>(ai);
  case Aiconfig::Heuristic::pfund_in_marriage_determination_trick:
    return make_unique<Heuristics::PfundInMarriageDeterminationTrick>(ai);

  case Aiconfig::Heuristic::poverty_contra_pfund_high_for_sure:
    return make_unique<Heuristics::PovertyContraPfundHighForSure>(ai);
  case Aiconfig::Heuristic::poverty_contra_pfund_for_sure:
    return make_unique<Heuristics::PovertyContraPfundForSure>(ai);
  case Aiconfig::Heuristic::poverty_contra_pfund_high_for_unsure:
    return make_unique<Heuristics::PovertyContraPfundHighForUnsure>(ai);
  case Aiconfig::Heuristic::poverty_contra_pfund_for_unsure:
    return make_unique<Heuristics::PovertyContraPfundForUnsure>(ai);
  case Aiconfig::Heuristic::poverty_contra_pfund_before_partner:
    return make_unique<Heuristics::PovertyContraPfundBeforePartner>(ai);
  case Aiconfig::Heuristic::poverty_special_start_with_trump:
    return make_unique<Heuristics::PovertySpecialStartWithTrump>(ai);
  case Aiconfig::Heuristic::poverty_special_play_pfund:
    return make_unique<Heuristics::PovertySpecialPlayPfund>(ai);
  case Aiconfig::Heuristic::poverty_special_give_no_points:
    return make_unique<Heuristics::PovertySpecialGiveNoPoints>(ai);
  case Aiconfig::Heuristic::poverty_special_offer_pfund:
    return make_unique<Heuristics::PovertySpecialOfferPfund>(ai);
  case Aiconfig::Heuristic::poverty_re_jab_as_last_player:
    return make_unique<Heuristics::PovertyReJabAsLastPlayer>(ai);
  case Aiconfig::Heuristic::poverty_re_jab_color_trick_with_trump:
    return make_unique<Heuristics::PovertyReJabColorTrickWithTrump>(ai);
  case Aiconfig::Heuristic::poverty_re_start_with_trump:
    return make_unique<Heuristics::PovertyReStartWithTrump>(ai);
  case Aiconfig::Heuristic::poverty_contra_start_with_color:
    return make_unique<Heuristics::PovertyContraStartWithColor>(ai);
  case Aiconfig::Heuristic::poverty_contra_jab_color_trick_with_trump:
    return make_unique<Heuristics::PovertyContraJabColorTrickWithTrump>(ai);
  case Aiconfig::Heuristic::poverty_leave_to_partner:
    return make_unique<Heuristics::PovertyContraLeaveToPartner>(ai);
  case Aiconfig::Heuristic::poverty_overjab_re:
    return make_unique<Heuristics::PovertyContraOverjabRe>(ai);

  case Aiconfig::Heuristic::picture_pull_down_color:
    return make_unique<Heuristics::PicturePullDownColor>(ai);
  case Aiconfig::Heuristic::picture_start_with_highest_color:
    return make_unique<Heuristics::PictureStartWithHighestColor>(ai);
  case Aiconfig::Heuristic::picture_get_last_trumps:
    return make_unique<Heuristics::PictureGetLastTrumps>(ai);
  case Aiconfig::Heuristic::picture_get_trumps_before_blank_color_ace:
    return make_unique<Heuristics::PictureGetTrumpsBeforeBlankColorAce>(ai);
  case Aiconfig::Heuristic::picture_get_first_trumps:
    return make_unique<Heuristics::PictureGetFirstTrumps>(ai);
  case Aiconfig::Heuristic::picture_draw_trumps:
    return make_unique<Heuristics::PictureDrawTrumps>(ai);
  case Aiconfig::Heuristic::picture_start_with_winning_card_before_bad_loosing_cards:
    return make_unique<Heuristics::PictureStartWithWinningCardBeforeBadLoosingCard>(ai);
  case Aiconfig::Heuristic::picture_start_with_loosing_solo_color:
    return make_unique<Heuristics::PictureStartWithLoosingSoloColor>(ai);
  case Aiconfig::Heuristic::picture_play_last_trumps:
    return make_unique<Heuristics::PicturePlayLastTrumps>(ai);
  case Aiconfig::Heuristic::picture_jab_color_trick_for_sure:
    return make_unique<Heuristics::PictureJabColorTrickForSure>(ai);
  case Aiconfig::Heuristic::picture_contra_discard_color_card:
    return make_unique<Heuristics::PictureContraDiscardColorCard>(ai);
  case Aiconfig::Heuristic::picture_jab_trump_trick:
    return make_unique<Heuristics::PictureJabTrumpTrick>(ai);
  case Aiconfig::Heuristic::picture_contra_jab_color_trick:
    return make_unique<Heuristics::PictureContraJabColorTrick>(ai);
  case Aiconfig::Heuristic::picture_contra_jab_with_high_color_card:
    return make_unique<Heuristics::PictureContraJabWithHighColorCard>(ai);
  case Aiconfig::Heuristic::picture_serve_trump_trick:
    return make_unique<Heuristics::PictureServeTrumpTrick>(ai);
  case Aiconfig::Heuristic::picture_pfund_high_for_sure:
    return make_unique<Heuristics::PicturePfundHighForSure>(ai);
  case Aiconfig::Heuristic::picture_pfund_for_sure:
    return make_unique<Heuristics::PicturePfundForSure>(ai);
  case Aiconfig::Heuristic::picture_pfund_high_for_unsure:
    return make_unique<Heuristics::PicturePfundHighForUnsure>(ai);
  case Aiconfig::Heuristic::picture_pfund_for_unsure:
    return make_unique<Heuristics::PicturePfundForUnsure>(ai);

  case Aiconfig::Heuristic::meatless_retry_last_color:
    return make_unique<Heuristics::MeatlessRetryLastColor>(ai);
  case Aiconfig::Heuristic::meatless_start_with_best_color:
    return make_unique<Heuristics::MeatlessStartWithBestColor>(ai);
  case Aiconfig::Heuristic::meatless_pull_down_color:
    return make_unique<Heuristics::MeatlessPullDownColor>(ai);
  case Aiconfig::Heuristic::meatless_play_blank_color_of_soloplayer:
    return make_unique<Heuristics::MeatlessPlayBlankColorOfSoloplayer>(ai);
  case Aiconfig::Heuristic::meatless_overjab_re:
    return make_unique<Heuristics::MeatlessOverjabRe>(ai);
  case Aiconfig::Heuristic::meatless_pfund_before_partner:
    return make_unique<Heuristics::MeatlessPfundBeforePartner>(ai);
  case Aiconfig::Heuristic::meatless_pfund_for_sure:
    return make_unique<Heuristics::MeatlessPfundForSure>(ai);
  case Aiconfig::Heuristic::meatless_play_color_for_partner_ace:
    return make_unique<Heuristics::MeatlessPlayColorForPartnerAce>(ai);
  case Aiconfig::Heuristic::meatless_serve_color_trick:
    return make_unique<Heuristics::MeatlessServeColorTrick>(ai);
  case Aiconfig::Heuristic::meatless_serve_trick_re:
    return make_unique<Heuristics::MeatlessServeTrickRe>(ai);
  case Aiconfig::Heuristic::meatless_serve_trick_contra:
    return make_unique<Heuristics::MeatlessServeTrickContra>(ai);
  case Aiconfig::Heuristic::meatless_play_highest_color_card_in_game:
    return make_unique<Heuristics::MeatlessPlayHighestColorCardInGame>(ai);

  case Aiconfig::Heuristic::color_jab_for_ace:
    return make_unique<Heuristics::ColorJabForAce>(ai);
  case Aiconfig::Heuristic::color_pfund_in_first_color_run:
    return make_unique<Heuristics::ColorPfundInFirstColorRun>(ai);
  case Aiconfig::Heuristic::color_pfund_high_for_sure:
    return make_unique<Heuristics::ColorPfundHighForSure>(ai);
  case Aiconfig::Heuristic::color_pfund_for_sure:
    return make_unique<Heuristics::ColorPfundForSure>(ai);
  case Aiconfig::Heuristic::color_pfund_high_for_unsure:
    return make_unique<Heuristics::ColorPfundHighForUnsure>(ai);
  case Aiconfig::Heuristic::color_pfund_for_unsure:
    return make_unique<Heuristics::ColorPfundForUnsure>(ai);
  case Aiconfig::Heuristic::color_overjab_to_get_partner_backhand:
    return make_unique<Heuristics::ColorOverjabToGetPartnerBackhand>(ai);
  case Aiconfig::Heuristic::play_color_in_solo:
    return make_unique<Heuristics::PlayColorInSolo>(ai);
  }
  return {};
}

/** @return   whether the heuristic is vaild for the given game type and player type (team)
 **/
bool Heuristic::is_valid(Aiconfig::Heuristic const heuristic,
                         GameTypeGroup const game_type,
                         PlayerTypeGroup const player_type)
{
  switch (heuristic) {
  case Aiconfig::Heuristic::error:
  case Aiconfig::Heuristic::no_heuristic:
    return false;
  case Aiconfig::Heuristic::manual:
    return false;
  case Aiconfig::Heuristic::bug_report:
    return Heuristics::BugReport::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::only_one_valid_card:
    return Heuristics::OnlyOneValidCard::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::valid_card:
    return Heuristics::ValidCard::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::gametree:
    return Heuristics::Gametree::is_valid(game_type, player_type);

  case Aiconfig::Heuristic::remaining_tricks_to_me:
    return Heuristics::RemainingTricksToMe::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_with_color_blank_ace:
    return Heuristics::StartWithColorBlankAce::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_with_color_single_ace:
    return Heuristics::StartWithColorSingleAce::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_with_color_double_ace:
    return Heuristics::StartWithColorDoubleAce::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_with_color_ten:
    return Heuristics::StartWithColorTen::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_with_lowest_color:
    return Heuristics::StartWithLowestColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_with_lowest_trump:
    return Heuristics::StartWithLowestTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_with_color_ace:
    return Heuristics::JabWithColorAce::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_with_color_ten:
    return Heuristics::JabWithColorTen::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::pfund_in_first_color_run:
    return Heuristics::PfundInFirstColorRun::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_first_color_run:
    return Heuristics::JabFirstColorRun::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_second_color_run:
    return Heuristics::JabSecondColorRun::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_color_over_fox:
    return Heuristics::JabColorOverFox::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_color_trick_high:
    return Heuristics::JabColorTrickHigh::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_small_color_card:
    return Heuristics::JabSmallColorCard::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_rich_trick:
    return Heuristics::JabRichTrick::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_trump_over_fox:
    return Heuristics::JabTrumpOverFox::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_trump_because_of_partner_announcement:
    return Heuristics::JabTrumpBecauseOfPartnerAnnouncement::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_trump_high_because_of_partner_announcement:
    return Heuristics::JabTrumpHighAfterOpponentAnnouncement::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_trump_high_after_opponent_announcement:
    return Heuristics::JabTrumpHighBecauseOfPartnerAnnouncement::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_with_highest_trump:
    return Heuristics::JabWithHighestTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_with_lowest_card:
    return Heuristics::JabWithLowestCard::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_with_lowest_card_below_trump_card_limit:
    return Heuristics::JabWithLowestCardBelowTrumpCardLimit::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_with_relative_low_trump:
    return Heuristics::JabWithRelativeLowTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::high_after_partner_low:
    return Heuristics::HighAfterPartnerLow::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::second_last_jab_for_following_pfund:
    return Heuristics::SecondLastJabForFollowingPfund::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_with_opponent_backhand:
    return Heuristics::JabWithOpponentBackhand::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::serve_with_lowest_card:
    return Heuristics::ServeWithLowestCard::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::serve_partner_with_lowest_card:
    return Heuristics::ServePartnerWithLowestCard::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_with_color:
    return Heuristics::StartWithColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_with_low_color:
    return Heuristics::StartWithLowColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_with_low_trump:
    return Heuristics::StartWithLowTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::retry_color:
    return Heuristics::RetryColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_with_trump_for_partner_color_ace:
    return Heuristics::StartWithTrumpForPartnerColorAce::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_with_trump_pfund:
    return Heuristics::StartWithTrumpPfund::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::play_color_for_partner_to_jab:
    return Heuristics::PlayColorForPartnerToJab::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::play_color_for_last_player_to_jab:
    return Heuristics::PlayColorForLastPlayerToJab::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::play_color_for_partner_ace:
    return Heuristics::PlayColorForPartnerAce::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::play_bad_color:
    return Heuristics::PlayBadColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::save_dulle:
    return Heuristics::SaveDulle::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::save_dulle_from_swines:
    return Heuristics::SaveDulleFromSwines::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::save_swines_from_hyperswines:
    return Heuristics::SaveSwinesFromHyperswines::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_swine_with_hyperswine:
    return Heuristics::JabSwineWithHyperswine::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::serve_color_trick:
    return Heuristics::ServeColorTrick::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::serve_trump_trick:
    return Heuristics::ServeTrumpTrick::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::pfund_high_for_sure:
    return Heuristics::PfundHighForSure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::pfund_for_sure:
    return Heuristics::PfundForSure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::pfund_high_for_unsure:
    return Heuristics::PfundHighForUnsure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::pfund_for_unsure:
    return Heuristics::PfundForUnsure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::pfund_before_partner:
    return Heuristics::PfundBeforePartner::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_for_ace:
    return Heuristics::JabForAce::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::create_blank_color:
    return Heuristics::CreateBlankColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::play_trump:
    return Heuristics::PlayTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_fox:
    return Heuristics::JabFox::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_for_doppelkopf:
    return Heuristics::JabForDoppelkopf::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::try_for_doppelkopf:
    return Heuristics::TryForDoppelkopf::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::play_for_partner_worries:
    return Heuristics::PlayForPartnerWorries::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::partner_backhand_draw_trump:
    return Heuristics::PartnerBackhandDrawTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::draw_trump:
    return Heuristics::DrawTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::draw_remaining_trump:
    return Heuristics::DrawRemainingTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::draw_trump_for_team_announcement:
    return Heuristics::DrawTrumpForTeamAnnouncement::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::play_to_jab_later:
    return Heuristics::PlayToJabLater::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::play_highest_color_card_in_game:
    return Heuristics::PlayHighestColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_to_win:
    return Heuristics::JabToWin::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_to_not_loose:
    return Heuristics::JabToNotLoose::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::grab_trick:
    return Heuristics::GrabTrick::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::last_player_jab_with_trump_ace_or_ten:
    return Heuristics::LastPlayerJabWithTrumpAceOrTen::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::last_player_jab_for_points:
    return Heuristics::LastPlayerJabForPoints::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::last_player_pass_small_trick:
    return Heuristics::LastPlayerPassSmallTrick::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::cannot_jab:
    return Heuristics::CannotJab::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::play_last_trumps:
    return Heuristics::PlayLastTrumps::is_valid(game_type, player_type);

  case Aiconfig::Heuristic::start_to_marry:
    return Heuristics::StartToMarry::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::jab_to_marry:
    return Heuristics::JabToMarry::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_to_get_married:
    return Heuristics::StartToGetMarried::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::start_to_get_married_last_chance:
    return Heuristics::StartToGetMarriedLastChance::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::pfund_in_marriage_determination_trick:
    return Heuristics::PfundInMarriageDeterminationTrick::is_valid(game_type, player_type);

  case Aiconfig::Heuristic::poverty_contra_pfund_high_for_sure:
    return Heuristics::PovertyContraPfundHighForSure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_contra_pfund_for_sure:
    return Heuristics::PovertyContraPfundForSure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_contra_pfund_high_for_unsure:
    return Heuristics::PovertyContraPfundHighForUnsure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_contra_pfund_for_unsure:
    return Heuristics::PovertyContraPfundForUnsure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_contra_pfund_before_partner:
    return Heuristics::PovertyContraPfundBeforePartner::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_special_start_with_trump:
    return Heuristics::PovertySpecialStartWithTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_special_play_pfund:
    return Heuristics::PovertySpecialPlayPfund::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_special_give_no_points:
    return Heuristics::PovertySpecialGiveNoPoints::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_special_offer_pfund:
    return Heuristics::PovertySpecialOfferPfund::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_re_jab_as_last_player:
    return Heuristics::PovertyReJabAsLastPlayer::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_re_jab_color_trick_with_trump:
    return Heuristics::PovertyReJabColorTrickWithTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_re_start_with_trump:
    return Heuristics::PovertyReStartWithTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_contra_start_with_color:
    return Heuristics::PovertyContraStartWithColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_contra_jab_color_trick_with_trump:
    return Heuristics::PovertyContraJabColorTrickWithTrump::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_leave_to_partner:
    return Heuristics::PovertyContraLeaveToPartner::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::poverty_overjab_re:
    return Heuristics::PovertyContraOverjabRe::is_valid(game_type, player_type);

  case Aiconfig::Heuristic::picture_pull_down_color:
    return Heuristics::PicturePullDownColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_start_with_highest_color:
    return Heuristics::PictureStartWithHighestColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_get_first_trumps:
    return Heuristics::PictureGetFirstTrumps::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_get_last_trumps:
    return Heuristics::PictureGetLastTrumps::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_get_trumps_before_blank_color_ace:
    return Heuristics::PictureGetTrumpsBeforeBlankColorAce::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_draw_trumps:
    return Heuristics::PictureDrawTrumps::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_play_last_trumps:
    return Heuristics::PicturePlayLastTrumps::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_jab_color_trick_for_sure:
    return Heuristics::PictureJabColorTrickForSure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_contra_discard_color_card:
    return Heuristics::PictureContraDiscardColorCard::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_jab_trump_trick:
    return Heuristics::PictureJabTrumpTrick::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_start_with_winning_card_before_bad_loosing_cards:
    return Heuristics::PictureStartWithWinningCardBeforeBadLoosingCard::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_start_with_loosing_solo_color:
    return Heuristics::PictureStartWithLoosingSoloColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_contra_jab_color_trick:
    return Heuristics::PictureContraJabColorTrick::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_contra_jab_with_high_color_card:
    return Heuristics::PictureContraJabWithHighColorCard::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_serve_trump_trick:
    return Heuristics::PictureServeTrumpTrick::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_pfund_high_for_sure:
    return Heuristics::PicturePfundHighForSure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_pfund_for_sure:
    return Heuristics::PicturePfundForSure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_pfund_high_for_unsure:
    return Heuristics::PicturePfundHighForUnsure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::picture_pfund_for_unsure:
    return Heuristics::PicturePfundForUnsure::is_valid(game_type, player_type);

  case Aiconfig::Heuristic::meatless_retry_last_color:
    return Heuristics::MeatlessRetryLastColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::meatless_start_with_best_color:
    return Heuristics::MeatlessStartWithBestColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::meatless_pull_down_color:
    return Heuristics::MeatlessPullDownColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::meatless_play_blank_color_of_soloplayer:
    return Heuristics::MeatlessPullDownColor::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::meatless_overjab_re:
    return Heuristics::MeatlessOverjabRe::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::meatless_pfund_before_partner:
    return Heuristics::MeatlessPfundBeforePartner::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::meatless_pfund_for_sure:
    return Heuristics::MeatlessPfundForSure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::meatless_play_color_for_partner_ace:
    return Heuristics::MeatlessPlayColorForPartnerAce::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::meatless_serve_color_trick:
    return Heuristics::MeatlessServeColorTrick::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::meatless_serve_trick_re:
    return Heuristics::MeatlessServeTrickRe::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::meatless_serve_trick_contra:
    return Heuristics::MeatlessServeTrickContra::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::meatless_play_highest_color_card_in_game:
    return Heuristics::MeatlessPlayHighestColorCardInGame::is_valid(game_type, player_type);

  case Aiconfig::Heuristic::color_jab_for_ace:
    return Heuristics::ColorJabForAce::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::color_pfund_in_first_color_run:
    return Heuristics::ColorPfundInFirstColorRun::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::color_pfund_high_for_sure:
    return Heuristics::ColorPfundHighForSure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::color_pfund_for_sure:
    return Heuristics::ColorPfundForSure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::color_pfund_high_for_unsure:
    return Heuristics::ColorPfundHighForUnsure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::color_pfund_for_unsure:
    return Heuristics::ColorPfundForUnsure::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::color_overjab_to_get_partner_backhand:
    return Heuristics::ColorOverjabToGetPartnerBackhand::is_valid(game_type, player_type);
  case Aiconfig::Heuristic::play_color_in_solo:
    return Heuristics::PlayColorInSolo::is_valid(game_type, player_type);
  }
  return false;
}

/** @return   whether the heuristic is vaild for the given game type and team
 **/
bool Heuristic::is_valid(Aiconfig::Heuristic const heuristic, Player const& player)
{
  return Heuristic::is_valid(heuristic,
                             HeuristicsMap::group(player.game()),
                             HeuristicsMap::group(player));
}

/** constructor
 **/
Heuristic::Heuristic(Ai const& ai, Aiconfig::Heuristic const heuristic) :
  DecisionInGame(ai),
  heuristic_(heuristic),
  name_(to_string(heuristic))
{ }

/** destructor
 **/
Heuristic::~Heuristic() = default;

/** @return   the heuristic (enum)
 **/
Aiconfig::Heuristic
Heuristic::heuristic() const
{
  return heuristic_;
}

/** @return   the name of the heuristic
 **/
string const&
Heuristic::name() const
{
  return name_;
}

/** @return   card to play, if the conditions are met
 **/
Card
Heuristic::get_card(Trick const& trick)
{
  rationale_.clear();
  if (!conditions_met(trick))
    return {};
  return card_to_play(trick);
}

/** @return   whether the soloplayer is behind
 **/
bool
Heuristic::soloplayer_behind(Trick const& trick) const
{
  auto const& game = this->game();
  return (   game.is_solo()
          && !trick.has_played(game.players().soloplayer()));
}

/** @return   whether the color can be jabbed by another player
 **/
bool
Heuristic::can_be_jabbed_by_others(Card::Color color) const
{
  if (cards_information().remaining_others(color) < 3)
    return true;
  for (auto const& player : game().players()) {
    if (player == this->player())
      continue;
    if (!guessed_hand(player).contains(color))
      return true;
  }
  return false;
}

/** @return   whether a following opposite player has 'card'
 **/
bool
Heuristic::following_opposite_player_has(Trick const& trick, Card card) const
{
  for (Player const& player : trick.remaining_following_players()) {
    if (guessed_same_team(player))
      continue;
    if (guessed_hand(player).contains(card))
      return true;
  }
  return false;
}

auto Heuristic::highest_trump_of_opposite_team() const -> HandCard
{
  HandCard highest_card;

  // take a look if all players coming in this trick are of mine team
  for (Player const& player : game().players()) {
    if (guessed_same_team(player))
      continue;
    // check for 'must have' of the color of the trick
    auto const c = guessed_hand(player).highest_card();
    if (highest_card.is_jabbed_by(c))
      highest_card = c;
  }

  return highest_card;
}

/** @return    the highest card the players behind have (without own team)
 **/
HandCard
Heuristic::highest_card_behind_of_opposite_team(Trick const& trick) const
{
  HandCard highest_card;

  // take a look if all players coming in this trick are of mine team
  for (Player const& player : trick.remaining_following_players()) {
    if (game().marriage().is_undetermined()) {
      if (::is_selector((  trick.isempty()
                         ? Card::trump
                         : trick.startcard().tcolor()),
                        game().marriage().selector())) {
        if (   team() != Team::re
            && game().players().soloplayer() == player) {
          // in a selector trick, the re player is seen as of the own team
          continue;
        }
      }
    } else { // if (normal game)
      if (guessed_same_team(player))
        continue;
    }

    if (   game().type() == GameType::poverty
        && player == game().players().soloplayer() ) {
      if (   !trick.isempty()
          && !trick.startcard().istrump()) {
        auto const c = guessed_hand(player).highest_card(trick.startcard().tcolor());
        if (highest_card.is_jabbed_by(c))
          highest_card = c;
        continue;
      }
    }
    // check for 'must have' of the color of the trick
    auto const c = guessed_hand(player).highest_card();
    if (highest_card.is_jabbed_by(c))
      highest_card = c;
  }

  return highest_card;
}

/** @return   the cards of the playes of the opposite team behind
 **/
vector<HandCard>
Heuristic::valid_cards_behind_of_opposite_team(Trick const& trick) const
{
  vector<HandCard> cards;

  for (Player const& player : trick.remaining_following_players()) {
    if (game().marriage().is_undetermined()) {
      if (::is_selector((  trick.isempty()
                         ? Card::trump
                         : trick.startcard().tcolor()),
                        game().marriage().selector())) {
        if (   team() != Team::re
            && game().players().soloplayer() == player) {
          // in a selector trick, the re player is seen as of the own team
          continue;
        }
      }
    } else { // if (normal game)
      if (guessed_same_team(player))
        continue;
    }

    for (auto const& card : guessed_hand(player).validcards(trick))
      cards.emplace_back(card);
    // special case: second color run
    if (   !trick.isempty()
        && !trick.startcard().istrump()
        && cards_information().of_player(player).must_have(trick.startcard().tcolor()) == 0
        && color_runs(trick.startcard().tcolor()) > 0) {
      for (auto const& card : guessed_hand(player).single_trumps())
        cards.emplace_back(card);
    }
  }

  return cards;
}

/** @return   the number of secure tricks the own team has
 **/
bool
Heuristic::remaining_tricks_go_to_own_team() const
{
  auto const& hand = this->hand();
  for (auto const& player : game().players()) {
    if (guessed_same_team(player))
      continue;
    auto const& opposite_hand = guessed_hand(player);
    if (!opposite_hand.hastrump())
      continue;
    if (opposite_hand.count(Card::trump) > hand.count(Card::trump))
      return false;
    auto const highest_opposite_trump = opposite_hand.highest_trump();
    if (hand.count_ge(highest_opposite_trump) < opposite_hand.count(Card::trump))
      return false;
    for (auto const color : game().cards().colors()) {
      if (!hand.contains(color))
        continue;
      if (!opposite_hand.contains(color))
        continue;
      auto const highest_opposite_color_card = opposite_hand.highest_card(color);
      if (hand.count_ge_in_color(highest_opposite_color_card)
          < opposite_hand.count_ge_in_color(highest_opposite_color_card))
        return false;
    }
  }

  return true;
}

/** @return   lowest trump card on hand, but gives only fuchs or ten back if there are better cards to keep
 **/
HandCard
Heuristic::lowest_best_trump_card(Trick const& trick) const
{
  if (   !trick.isempty()
      && !trick.startcard().istrump()
      && hand().contains(trick.startcard().tcolor())) {
    return {};
  }
  auto const trumps = hand().single_trumps();
  if (   game().type() == GameType::poverty
      && team() == Team::re) {
    auto const& card = trumps.same_or_higher_card(Card::diamond_jack);
    if (card.is_special())
      return {};
    return card;
  }

  auto const& card = trumps.lowest_trump();
  if (card.value() < 10) {
    return card;
  }

  auto card2 = trumps.next_higher_card(card);
  while (   card2
         && card2.value() >= 10) {
    card2 = trumps.next_higher_card(card2);
  }
  if (!card2)
    return card;

  if (card2.is_jabbed_by(trump_card_limit()))
    return card2;

  return card;
}

/** @return   all colors, starting with color
 **/
vector<Card::Color>
Heuristic::colors_starting_with(Card::Color const color)
{
  switch (color) {
  case Card::club:
    return {{Card::club, Card::spade, Card::heart, Card::diamond}};
  case Card::spade:
    return {Card::spade, Card::club, Card::heart, Card::diamond};
  case Card::heart:
    return {Card::heart, Card::club, Card::spade, Card::diamond};
  case Card::diamond:
    return {Card::diamond, Card::club, Card::spade, Card::heart};
  default:
    return {Card::club, Card::spade, Card::heart, Card::diamond};
  }
  return {};
}

/** @return   jabbing card for sure (if exists)
 **/
HandCard
Heuristic::sure_jabbing_card(Trick const& trick)
{
  auto const& ai = this->ai();
  auto const& hand = ai.hand();
  auto const highest_opposite_card = highest_card_behind_of_opposite_team(trick);
  if (hand.highest_trump().is_jabbed_by(highest_opposite_card))
    return {};
  return lowest_highest_trump(trick);
}

/** @return   high trump to jab the trick
 **/
HandCard
Heuristic::result_high_trump_jabbing_card(Trick const& trick)
{
  auto const& ai = this->ai();
  auto const& hand = ai.hand();

  if (trick.islastcard()) {
    rationale_.add(_("Heuristic::condition::last card"));
    auto const card = best_jabbing_card(trick);
    rationale_.add(_("Heuristic::return::best jabbing card: %s", _(card)));
    return card;
  }

  if (   trick.issecondlastcard()
      && trick.points() < 10
      && game().announcements().last(team()) < Announcement::no60) {
    rationale_.add(_("Heuristic::condition::second last card"));
    rationale_.add(_("Heuristic::condition::not many points in the trick"));
    auto const card = best_jabbing_card(trick);
    rationale_.add(_("Heuristic::return::best jabbing card: %s", _(card)));
    return card;
  }

  {
    auto const card = sure_jabbing_card(trick);
    if (card) {
      rationale_.add(_("Heuristic::return::jab for sure: %s", _(card)));
      return card;
    }
  }

  if (   condition_first_trick()
      && condition_trump_trick(trick)) {
    auto const card = optimized_trump_to_jab(hand.highest_trump(), trick);
    if (trick.isjabbed(card)) {
      rationale_.add(_("Heuristic::return::best highest trump: %s", _(card)));
      return card;
    }
  }

  auto card = hand.highest_card(Card::queen);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no queen"));
    return {};
  }
  if (!trick.isjabbed(card)) {
    rationale_.add(_("Heuristic::reject::trick is not jabbed by highest queen %s", _(card)));
    return {};
  }

  card = optimized_trump_to_jab(card, trick);
  rationale_.add(_("Heuristic::return::jab with high queen %s", _(card)));
  return card;
}

/** @return   the lowest trump the player has which gets over the of the opposite team, or the highest trump; empty, if the trick can not be jabbed
 **/
HandCard
Heuristic::lowest_highest_trump(Trick const& trick)
{
  auto const lh_trump = hand().highest_trump();
  if (!lh_trump)
    return {};
  if (!trick.isjabbed(lh_trump))
    return {};

  return optimized_trump_to_jab(lh_trump, trick);
}

/** @return   the best element of container with predicate(element) == true and the maximum of value_function(element)
 **/
Card::Color
best_of_if(vector<Card::Color> container,
           std::function<bool(Card::Color)> predicate,
           std::function<int(Card::Color)> value_function)
{
  using Value = int;
  using Element = Card::Color;
  Value best_value = INT_MIN;
  Element best_element = Card::nocardcolor;
  for (auto const& element : container) {
    if (predicate(element)) {
      auto const value = value_function(element);
      if (value > best_value) {
        best_value = value;
        best_element = element;
      }
    }
  }
  return best_element;
}
