/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "cannot_jab.h"

#include "../ai.h"

namespace Heuristics {

auto CannotJab::is_valid(GameTypeGroup const game_type,
                         PlayerTypeGroup const player_group) -> bool
{
  return true;
}


CannotJab::CannotJab(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::cannot_jab)
{ }


CannotJab::~CannotJab() = default;


auto CannotJab::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && !condition_can_jab(trick)
         );
}


auto CannotJab::card_to_play(Trick const& trick) -> Card
{
  if (condition_must_serve(trick)) {
    if (trick.startcard().istrump()) {
      auto card = lowest_trump_card();
      card = optimize_card_for_gamepoint_step(trick, card);
      rationale_.add(_("Heuristic::return::lowest card to serve: %s", _(card)));
      return card;
    } else {
      auto const card = lowest_serve_card(trick);
      return card;
    }
  }

  if (hand().count_fehl() == 0) {
    auto const card_trump = lowest_trump_card();
    rationale_.add(_("Heuristic::return::lowest trump: %s", _(card_trump)));
    return card_trump;
  }

  auto const card_fehl = lowest_fehl_card();
  auto const card_trump = lowest_trump_card();
  auto const card = choose_fehl_or_trump(card_fehl, card_trump);
  if (card == card_fehl) {
    rationale_.add(_("Heuristic::return::lowest color card: %s", _(card_fehl)));
    return card_fehl;
  }
  rationale_.add(_("Heuristic::return::lowest trump: %s", _(card_trump)));
  return card_trump;
}


auto CannotJab::lowest_serve_card(Trick const& trick) -> Card
{
  auto const color = trick.startcard().tcolor();
  return hand().lowest_card(color);
}


auto CannotJab::lowest_fehl_card() -> Card
{
  for (auto const value : {Card::nine, Card::jack, Card::queen, Card::king}) {
    auto card = search_single_color_card(value);
    if (card)
      return card;
  }
  for (auto const value : {Card::nine, Card::jack, Card::queen, Card::king}) {
    auto const card = search_color_card(value);
    if (card)
      return card;
  }
  auto card = search_single_color_card(Card::ten);
  if (card)
    return card;
  card = search_color_card(Card::ten);
  if (card)
    return card;
  card = search_double_color_card(Card::ace);
  if (card)
    return card;
  card = search_color_card(Card::ace);
  if (card)
    return card;
  return {};
}


auto CannotJab::lowest_trump_card() -> HandCard
{
  auto const& hand = this->hand();
  auto const& game = this->game();
  auto const& trick = game.tricks().current();
  auto const lowest_trump = hand.lowest_trump();
  if (!lowest_trump)
    return {};

  if (lowest_trump.points() < 10) {
    rationale_.add(_("Heuristic::condition::lowest trump has less then 10 points"));
    return lowest_trump;
  }

  auto second_lowest_trump = hand.next_higher_card(lowest_trump);
  while (   second_lowest_trump
         && second_lowest_trump.points() >= 10) {
    second_lowest_trump = hand.next_higher_card(second_lowest_trump);
  }
  if (!second_lowest_trump || second_lowest_trump.jabs(ai().trump_card_limit()))
    return lowest_trump;

  if (   points_of_opposite_team() + lowest_trump.points()        >= game.needed_points_to_win(player().team())
      && points_of_opposite_team() + second_lowest_trump.points() <  game.needed_points_to_win(player().team())) {
    rationale_.add(_("Heuristic::condition::lowest trump leads to loosing"));
    return second_lowest_trump;
  }

  if (game.announcements().last(player().team()) != Announcement::noannouncement) {
    rationale_.add(_("Heuristic::condition::Announcement of own team, do not give ten points"));
    return second_lowest_trump;
  }

  if (trick.points() >= 30) {
    rationale_.add(_("Heuristic::condition::ten points lead to a doppelkopf, prefer %s", _(second_lowest_trump)));
    return second_lowest_trump;
  }

  if (trick.points() / trick.actcardno() >= 10) {
    rationale_.add(_("Heuristic::condition::ten points could lead to a doppelkopf, prefer %s", _(second_lowest_trump)));
    return second_lowest_trump;
  }


  if (   hand.count(Card::trump) <= 3
      && !hand.highest_trump().jabs(ai().trump_card_limit())) {
    if (!hand.next_higher_card(second_lowest_trump))
      return second_lowest_trump;
  }
  if (   lowest_trump.isfox()
      && hand.count(Card::trump) - hand.count(lowest_trump) >= 2) {
    return second_lowest_trump;
  }
  if (!is_picture_solo_or_meatless(game.type())) {
    if (   hand.count(Card::trump) <= 2 * hand.count_lt_trump(second_lowest_trump)
        && second_lowest_trump.value() != Card::jack) {
      return lowest_trump;
    }
  }
  if (second_lowest_trump.is_jabbed_by(ai().trump_card_limit()))
    return second_lowest_trump;
  return lowest_trump;
}


auto CannotJab::search_color_card(Card::Value value) -> Card
{
  auto const& hand = this->hand();
  auto const& game = this->game();
  auto best_card = Card();
  auto best_count = decltype(hand.count(Card::club))(0);
  for (auto const color : game.cards().colors()) {
    auto const card = Card(color, value);
    if (card.istrump(game))
      continue;
    if (!hand.contains(card))
      continue;
    auto const count = hand.count(color);
    if (   !best_card
        || count < best_count) {
      best_card = card;
      best_count = count;
    }
  }
  return best_card;
}


auto CannotJab::search_single_color_card(Card::Value const value) -> Card
{
  auto const& hand = this->hand();
  auto const& game = this->game();
  for (auto const color : game.cards().colors()) {
    auto const card = Card(color, value);
    if (card.istrump(game))
      continue;
    if (   hand.count(color) == 1
        && hand.contains(card)) {
      return card;
    }
  }
  return {};
}


auto CannotJab::search_double_color_card(Card::Value value) -> Card
{
  auto const& hand = this->hand();
  auto const& game = this->game();
  for (auto const color : game.cards().colors()) {
    auto const card = Card(color, value);
    if (card.istrump(game))
      continue;
    if (   hand.count(color) == 2
        && hand.count(card) == 2)
      return card;
  }
  return {};
}


auto CannotJab::choose_fehl_or_trump(Card const& card_fehl,
                                     HandCard const& card_trump) -> Card
{
  auto const& game = this->game();
  auto const& hand = this->hand();
  if (is_picture_solo_or_meatless(game.type()))
    return card_fehl;

  if (!card_trump)
    return card_fehl;
  if (!card_fehl)
    return card_trump;

  if (card_trump.points() >= 10) {
    rationale_.add(_("Heuristic::condition::lowest trump card at least 10 points"));
    return card_fehl;
  }

  if (card_fehl.points() < 10) {
    rationale_.add(_("Heuristic::condition::lowest fehl has less then 10 points"));
    auto const& highest_trump = hand.highest_trump();
    if (!highest_trump) {
      return card_fehl;
    }
    if (highest_trump.is_special())
      return card_fehl;

    if (highest_trump.value() == Card::nine) {
      rationale_.add(_("Heuristic::condition::highest trump is a nine"));
      return card_trump;
    }
    if (highest_trump.value() == Card::king) {
      if (card_fehl.value() == Card::nine) {
        return card_fehl;
      }
      rationale_.add(_("Heuristic::condition::highest trump is a king, but lowest chosen fehl also"));
      return card_trump;
    }
    return card_fehl;
  }

  rationale_.add(_("Heuristic::condition::lowest fehl card with 10 points"));
  if (card_trump.is_special()) {
    rationale_.add(_("Heuristic::condition::lowest trump is special"));
    return card_fehl;
  }
  if (card_trump.value() == Card::queen) {
    rationale_.add(_("Heuristic::condition::lowest trump is queen"));
    if (hand.count(Card::trump) >= hand.cardsnumber() * 3 / 4) {
      rationale_.add(_("Heuristic::condition::have many trumps"));
      return card_trump;
    }
    rationale_.add(_("Heuristic::condition::have not so many trumps"));
    return card_fehl;
  }

  rationale_.add(_("Heuristic::condition::lowest trump is below queen"));
  return card_trump;
}


auto CannotJab::optimize_card_for_gamepoint_step(Trick const& trick, Card const card) -> Card
{
  // Kleine Optimierung: nicht eine Punkte-Grenze überschreiten
  if (is_solo(game().type()))
    return card;
  if (!card.istrump(game()))
    return card;
  if (!trick.islastcard())
    return card;
  if (card.points() < 4)
    return card;

  if (!(   points_of_opposite_team() == 120 - 4
        || points_of_opposite_team() == 120 - 3
        || points_of_opposite_team() ==  90 - 4
        || points_of_opposite_team() ==  90 - 3
        || points_of_opposite_team() ==  60 - 4
        || points_of_opposite_team() ==  60 - 3
        || points_of_opposite_team() ==  30 - 4
        || points_of_opposite_team() ==  30 - 3))
    return card;


  if (hand().contains(Card::diamond_jack)) {
    rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(Card::diamond_jack)));
    return Card::diamond_jack;
  } else if (hand().contains(Card::heart_jack)) {
    rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(Card::heart_jack)));
    return Card::heart_jack;
  } else if (hand().contains(Card::spade_jack)) {
    rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(Card::spade_jack)));
    return Card::spade_jack;
  }
  return card;
}

} // namespace Heuristics
