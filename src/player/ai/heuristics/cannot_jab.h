/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../heuristic.h"

namespace Heuristics {

/** I cannot jab the trick so play a small card
 ** No pfund, that is another heuristic
 **/
class CannotJab : public Heuristic {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
explicit CannotJab(Ai const& ai);
CannotJab(CannotJab const&) = delete;
CannotJab& operator=(CannotJab const&) = delete;

~CannotJab() override;

auto conditions_met(Trick const& trick) -> bool override;
auto card_to_play(Trick const& trick)   -> Card override;

private:
auto lowest_serve_card(Trick const& trick)       -> Card;
auto lowest_fehl_card()                          -> Card;
auto lowest_trump_card()                         -> HandCard;
auto search_color_card(Card::Value value)        -> Card;
auto search_single_color_card(Card::Value value) -> Card;
auto search_double_color_card(Card::Value value) -> Card;

auto choose_fehl_or_trump(Card const& card_fehl, HandCard const& card_trump) -> Card;

auto optimize_card_for_gamepoint_step(Trick const& trick, Card card) -> Card;
};

} // namespace Heuristics
