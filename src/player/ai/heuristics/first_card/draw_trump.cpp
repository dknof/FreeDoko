/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "draw_trump.h"

#include "../../ai.h"

namespace Heuristics {

bool DrawTrump::is_valid(GameTypeGroup const game_type,
                         PlayerTypeGroup const player_group)
{
  return is_with_trump(game_type);
}

DrawTrump::DrawTrump(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::draw_trump)
{ }

DrawTrump::~DrawTrump() = default;

auto DrawTrump::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_opposite_team_has_trump()
          && !condition_opposite_team_announcement()
          && condition_has_many_trumps()
         );
}

auto DrawTrump::card_to_play(Trick const& trick) -> Card
{
  if (   (   game().type() == GameType::normal
          || game().type() == GameType::marriage)
      && team() == Team::re
      && hand().count_all(Card::club_queen) < 2) {
    auto const& cards_information = ai().cards_information();
    if (cards_information.played(Card::club_queen) == hand().count_played(Card::club_queen)) {
      rationale_.add(_("Heuristic::condition::re partner behind with club queen on the hand"));
      auto const c = hand().lowest_trump();
      rationale_.add(_("Heuristic::return::low trump: %s", _(c)));
      return c;
    }
  }

  auto const c = HandCard(hand(), lowest_best_trump_card(trick));
  rationale_.add(_("Heuristic::return::lowest best trump: %s", _(c)));
  return c;
}

auto DrawTrump::condition_has_many_trumps() -> bool
{
  auto const& game = this->game();
  auto const& hand = this->hand();
  auto const& cards_information = ai().cards_information();
  auto const trumpno = hand.count(Card::trump);
  auto const other_trumpno = cards_information.remaining_trumps_others();

  if (other_trumpno >= trumpno + 6) {
    rationale_.add(_("Heuristic::condition::the others have at least 6 more trumps"));
    return false;
  }

  if (is_solo(game.type())) {
    if (  team() == Team::re
        && other_trumpno >= trumpno * 1.5
       ) {
      rationale_.add(_("Heuristic::condition::soloplayer, the others have at least 1.5 times of trumps"));
      return false;
    }

    if (   team() == Team::contra
        && other_trumpno >= trumpno
       ) {
      rationale_.add(_("Heuristic::condition::solo, I have not the majority of the trumps"));
      return false;
    }
  }

  if (other_trumpno >= 2 * trumpno) {
    rationale_.add(_("Heuristic::condition::the others have at least two times of trumps"));
    return false;
  }

  rationale_.add(_("Heuristic::condition::many trumps"));
  return true;
}

} // namespace Heuristics
