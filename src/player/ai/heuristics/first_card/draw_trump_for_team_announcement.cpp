/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "draw_trump_for_team_announcement.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool DrawTrumpForTeamAnnouncement::is_valid(GameTypeGroup const game_type,
                                        PlayerTypeGroup const player_group)
{
  return (   is_with_trump(game_type)
          && is_with_partner(game_type, player_group));
}

/** constructor
 **/
DrawTrumpForTeamAnnouncement::DrawTrumpForTeamAnnouncement(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::draw_trump_for_team_announcement)
{ }

/** destructor
 **/
DrawTrumpForTeamAnnouncement::~DrawTrumpForTeamAnnouncement() = default;

/** @return  whether the conditions are met
 **/
bool
DrawTrumpForTeamAnnouncement::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && condition_own_team_announcement(Announcement::no90)
         );
}

/** @return   card to play
 **/
Card
DrawTrumpForTeamAnnouncement::card_to_play(Trick const& trick)
{
  auto const team = this->team();
  auto const& game = this->game();

  if (  game.announcements().last(opposite(team))
      > game.announcements().last(team)) {
    rationale_.add(_("Heuristic::reject::opposite team has made a higher announcement then my team"));
    return {};
  }

  auto const& hand = this->hand();
  if (hand.count(Card::trump) < 3) {
    rationale_.add(_("Heuristic::reject::too few trumps: %u", hand.count(Card::trump)));
  }

  auto const cards_to_play
    = { Card::diamond_king,
      Card::diamond_jack,
      Card::heart_jack,
      Card::spade_jack,
      Card::diamond_nine};

  for (auto const c : cards_to_play) {
    if (   hand.contains(c)
        && !HandCard(hand, c).is_special()) {
      rationale_.add(_("Heuristic::return::take %s", _(c)));
      return c;
    }
  }

  return {};
}

} // namespace Heuristics
