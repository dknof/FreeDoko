/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "partner_backhand_draw_trump.h"

#include "../../ai.h"
#include "../../../cards_information.of_player.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
auto PartnerBackhandDrawTrump::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (   is_with_trump(game_type)
          && is_with_partner(game_type, player_group));
}


/** constructor
 **/
PartnerBackhandDrawTrump::PartnerBackhandDrawTrump(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::partner_backhand_draw_trump)
{ }


/** destructor
 **/
PartnerBackhandDrawTrump::~PartnerBackhandDrawTrump() = default;


/** @return  whether the conditions are met
 **/
auto PartnerBackhandDrawTrump::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_last_player_guessed_same_team(trick)
         );
}


/** @return   card to play
 **/
auto PartnerBackhandDrawTrump::card_to_play(Trick const& trick) -> Card
{
  auto const team = this->team();
  auto const& game = this->game();

  if (  game.announcements().last(opposite(team))
      > game.announcements().last(team)) {
    rationale_.add(_("Heuristic::reject::opposite team has made a higher announcement then my team"));
    return {};
  }

  auto const partner_has_highest_trump = this->partner_has_highest_trump(trick);
  if (partner_has_highest_trump) {
    rationale_.add(_("Heuristic::condition::partner has the highest trump"));
  } else {
    rationale_.add(_("Heuristic::condition::partner does not have the highest trump"));
  }

  auto const& hand = this->hand();
  if (   !partner_has_highest_trump
      && hand.count(Card::trump) < 3
     ) {
    rationale_.add(_("Heuristic::reject::too few trumps: %u", hand.count(Card::trump)));
  }

  static vector<Card> const cards_to_play_case_highest_trump
    = { Card::diamond_ace,
      Card::diamond_ten,
      Card::diamond_king,
      Card::diamond_jack,
      Card::heart_jack,
      Card::spade_jack,
      Card::diamond_nine};
  static vector<Card> const cards_to_play_case_no_highest_trump
    = { Card::diamond_king,
      Card::diamond_jack,
      Card::heart_jack,
      Card::spade_jack,
      Card::diamond_nine};
  auto const& cards_to_play
    = (partner_has_highest_trump
       ? cards_to_play_case_highest_trump
       : cards_to_play_case_no_highest_trump);

  for (auto c : cards_to_play) {
    if (   hand.contains(c)
        && !HandCard(hand, c).is_special()) {
      rationale_.add(_("Heuristic::return::take %s", _(c)));
      return c;
    }
  }

  return {};
}


/** @return   whether the partner has the highest trump in the game
 **/
auto PartnerBackhandDrawTrump::partner_has_highest_trump(Trick const& trick) const -> bool
{
  auto const& hand = this->hand();
  auto const highest_trump = HandCard(hand, cards_information().highest_remaining_trump_of_others());

  Player const& partner = trick.lastplayer();
  if (!guessed_hand(partner).contains(highest_trump))
    return false;

  auto const& game = this->game();
  if (   highest_trump.isdulle()
      && game.second_dulle_over_first()
      && cards_information(partner).must_have(Card::dulle)) {
    return true;
  }

  for (auto const& p : game.players()) {
    if (   !guessed_same_team(p)
        && guessed_hand(p).contains(highest_trump)) {
      return false;
    }
  }
  return true;
}

} // namespace Heuristics
