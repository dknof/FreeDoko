/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "play_bad_color.h"

#include "../../ai.h"
#include "../../../../party/rule.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PlayBadColor::is_valid(GameTypeGroup const game_type,
                            PlayerTypeGroup const player_group)
{
  return (   is_with_trump_color(game_type)
          && is_with_partner(game_type, player_group));
}

/** constructor
 **/
PlayBadColor::PlayBadColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::play_bad_color)
{ }

/** destructor
 **/
PlayBadColor::~PlayBadColor() = default;

/** @return  whether the conditions are met
 **/
bool
PlayBadColor::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && condition_last_player_guessed_same_team(trick));
}

/** @return   card to play
 **/
Card
PlayBadColor::card_to_play(Trick const& trick)
{
  auto const trumpcolor = game().cards().trumpcolor();
  auto const& hand = this->hand();

  Player const& partner = trick.lastplayer();
  auto const& hand_of_partner = guessed_hand(partner);

  Card::Color selected_color = Card::nocardcolor;
  for (auto color : rule().card_colors()) {
    if (color == trumpcolor)
      continue;
    if (!hand.contains(color))
      continue;

    if (!hand_of_partner.contains(color))
      continue;
    if (   !cards_information().played(color)
        && hand_of_partner.contains(color, Card::ace))
      continue;

    if (selected_color == Card::nocardcolor) {
      rationale_.add(_("Heuristic::info::check color %s", _(color)));
      selected_color = color;
    } else if (hand_of_partner.count(color)
               < hand_of_partner.count(selected_color)) {
      rationale_.add(_("Heuristic::info::prefer color %s, the partner has less cards", _(selected_color)));
      selected_color = color;
    }
  } // for (color : colors)

  if (selected_color == Card::nocardcolor) {
    rationale_.add(_("Heuristic::reject::no color found, the partner has, too"));
    return Card::empty;
  }

  auto const card = hand.lowest_card(selected_color);
  rationale_.add(_("Heuristic::return::take lowest card of %s: %s", _(selected_color), _(card)));
  return card;
}

} // namespace Heuristics
