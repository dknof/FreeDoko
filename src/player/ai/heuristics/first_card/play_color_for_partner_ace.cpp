/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "play_color_for_partner_ace.h"

#include "../../../cards_information.of_player.h"
#include "../../../../game/game.h"

namespace Heuristics {

auto PlayColorForPartnerAce::is_valid(GameTypeGroup const game_type,
                                      PlayerTypeGroup const player_group) -> bool
{
  return (   is_with_partner(game_type, player_group)
          && game_type != GameTypeGroup::solo_meatless);
}


PlayColorForPartnerAce::PlayColorForPartnerAce(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::play_color_for_partner_ace)
{ }


PlayColorForPartnerAce::~PlayColorForPartnerAce() = default;


auto PlayColorForPartnerAce::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_partner_guessed()
         );
}


auto PlayColorForPartnerAce::card_to_play(Trick const& trick) -> Card
{
  auto const color = best_color_to_play(trick);

  if (!color) {
    rationale_.add(_("Heuristic::reject::no suitable color found"));
    return {};
  }

  auto const& hand = this->hand();
  auto const card = hand.highest_card(color);
  rationale_.add(_("Heuristic::return::highest card from color %s: %s", _(color), _(card)));
  return card;
}


auto PlayColorForPartnerAce::best_color_to_play(Trick const& trick) -> Card::Color
{
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();
  return best_of_if(game().cards().colors(),
                    [&](Card::Color const color) {
                    return (   condition_has(color)
                            && !condition_color_played_by_others(color)
                            && condition_next_in_line_with_ace_is_guessed_partner(trick, color)
                            && !hand.contains(color, Card::ace)
                            && cards_information.remaining_others(color) >= 3
                           );
                    },
                    [&hand](Card::Color const color) {
                    return hand.count(color);
                    }
                   );
}


auto PlayColorForPartnerAce::condition_color_played_by_others(Card::Color const color) -> bool
{
  if (   cards_information().played(color) > cards_information(player()).played(color)) {
    // gettext: %s color
    rationale_.add(_("Heuristic::condition::another player has played %s already", _(color)));
    return true;
  }
  // gettext: %s color
  rationale_.add(_("Heuristic::condition::no player has played %s so far", _(color)));
  return false;
}


auto PlayColorForPartnerAce::condition_next_in_line_with_ace_is_guessed_partner(Trick const& trick,
                                                                                Card::Color const color) -> bool
{
  auto const& game = this->game();
  auto const ace = Card(color, Card::ace);
  for (unsigned p = 1; p < game.playerno(); ++p) {
    auto const& player = trick.player_of_card(p);
    auto const& hand = guessed_hand(player);
    if (guessed_same_team(player)) {
      if (hand.contains(ace)) {
        // gettext: %s player name, %s ace
        rationale_.add(_("Heuristic::condition::%s can have %s and is of the same team", player.name(), _(ace)));
        return true;
      }
    } else {
      if (hand.contains(ace)) {
        // gettext: %s player name, %s ace
        rationale_.add(_("Heuristic::condition::%s can have %s but is not of the same team", player.name(), _(ace)));
        return false;
      }
      if (!hand.contains(color)) {
        // gettext: %s player name, %s color
        rationale_.add(_("Heuristic::condition::%s does not have %s but is not of the same team", player.name(), _(color)));
        return false;
      }
    }
  }

  rationale_.add(_("Heuristic::condition::no partner found with %s", _(ace)));
  return false;
}

} // namespace Heuristics
