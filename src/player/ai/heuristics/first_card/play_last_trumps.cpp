/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "play_last_trumps.h"

#include "../../ai.h"
#include "../../../../party/rule.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PlayLastTrumps::is_valid(GameTypeGroup const game_type,
                              PlayerTypeGroup const player_group)
{
  return (   !(   game_type == GameTypeGroup::poverty
               && player_group == PlayerTypeGroup::special)
          && game_type != GameTypeGroup::solo_meatless);
}

/** constructor
 **/
PlayLastTrumps::PlayLastTrumps(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::play_last_trumps)
{ }

/** destructor
 **/
PlayLastTrumps::~PlayLastTrumps() = default;

/** @return  whether the conditions are met
 **/
bool
PlayLastTrumps::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && condition_has_trump()
          && condition_noone_else_has_trump()
         );
}

/** @return   card to play
 **/
Card
PlayLastTrumps::card_to_play(Trick const& trick)
{
  if (is_real_solo(game().type())) {
    auto const card = hand().highest_trump();
    rationale_.add(_("Heuristic::return::play highest trump %s", _(card)));
    return card;
  }

  return get_trump_respecting_special_points();
}

/** @return  trump to play, respecting special points (charlie, ...)
 **/
Card
PlayLastTrumps::get_trump_respecting_special_points()
{

  auto const& hand = this->hand();
  auto const& rule = this->rule();
  vector<Card> trumps = hand.single_trumps();
  if (trumps.size() == 1) {
    auto const card = trumps[0];
    rationale_.add(_("Heuristic::return::remaining trump: %s", _(card)));
    return card;
  }
  if (   rule(Rule::Type::extrapoint_fox_last_trick)
      && contains(trumps, Card::diamond_ace)) {
    rationale_.add(_("Heuristic::action::do not select a fox"));
    remove(trumps, Card::diamond_ace);
    if (trumps.size() == 1) {
      auto const card = trumps[0];
      rationale_.add(_("Heuristic::return::remaining trump: %s", _(card)));
      return card;
    }
  }
  if (   rule(Rule::Type::extrapoint_charlie)
      && contains(trumps, Card::charlie)) {
    rationale_.add(_("Heuristic::action::do not select a charlie"));
    remove(trumps, Card::charlie);
    if (trumps.size() == 1) {
      auto const card = trumps[0];
      rationale_.add(_("Heuristic::return::remaining trump: %s", _(card)));
      return card;
    }
  }
  if (   rule(Rule::Type::dullen)
      && contains(trumps, Card::dulle)) {
    rationale_.add(_("Heuristic::action::do not select a dulle"));
    remove(trumps, Card::dulle);
    if (trumps.size() == 1) {
      auto const card = trumps[0];
      rationale_.add(_("Heuristic::return::remaining trump: %s", _(card)));
      return card;
    }
  }
  if (contains(trumps, Card::diamond_ace)) {
    rationale_.add(_("Heuristic::action::do not select card %s", _(Card::diamond_ace)));
    remove(trumps, Card::diamond_ace);
    if (trumps.size() == 1) {
      auto const card = trumps[0];
      rationale_.add(_("Heuristic::return::remaining trump: %s", _(card)));
      return card;
    }
  }
  if (contains(trumps, Card::diamond_ten)) {
    rationale_.add(_("Heuristic::action::do not select card %s", _(Card::diamond_ten)));
    remove(trumps, Card::diamond_ten);
  }
  auto card = hand.highest_trump();
  rationale_.add(_("Heuristic::return::highest remaining trump: %s", _(card)));
  return card;
}

} // namespace Heuristics
