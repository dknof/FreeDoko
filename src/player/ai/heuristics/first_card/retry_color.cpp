/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "retry_color.h"

#include "../../ai.h"
#include "../../../cards_information.of_player.h"

#include "../../../../card/trick.h"
#include "../../../../game/game.h"
#include "../../../../party/rule.h"


namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool RetryColor::is_valid(GameTypeGroup const game_type,
                          PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
RetryColor::RetryColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::retry_color)
{ }

/** destructor
 **/
RetryColor::~RetryColor() = default;

/** @return  whether the conditions are met
 **/
bool
RetryColor::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && condition_has_color());
}

/** @return   card to play
 **/
Card
RetryColor::card_to_play(Trick const& trick)
{
  auto const& game = trick.game();
  auto const& hand = this->hand();
  auto colors = colors_not_jabbed_by_opposite_team();

  for (auto const color : colors) {
    auto const card = Card(color, Card::ten);
    if (   !card.istrump(game)
        && hand.contains(card)
        && guessed_same_team(trick.lastplayer())
        && !cards_information(trick.lastplayer()).can_have(color)) {
      rationale_.add(_("Heuristic::return::own team at backhand can jab %s, take %s", _(color), _(card)));
      return card;
    }
  }

  for (auto const color : colors) {
    auto const card = hand.highest_card(color);
    if (   guessed_same_team(trick.lastplayer())
        && !cards_information(trick.lastplayer()).can_have(color)) {
      rationale_.add(_("Heuristic::return::own team at backhand can jab %s, take %s", _(color), _(card)));
      return card;
    }
  }
  for (auto const color : colors) {
    auto const card = Card(color, Card::nine);
    if (   hand.contains(card)
        && !card.istrump(game)) {
      rationale_.add(_("Heuristic::return::take %s", _(card)));
      return card;
    }
  }
  for (auto const color : colors) {
    auto const card = Card(color, Card::king);
    if (   hand.contains(card)
        && !card.istrump(game)) {
      rationale_.add(_("Heuristic::return::take %s", _(card)));
      return card;
    }
  }

  rationale_.add(_("Heuristic::reject::no suitable color found"));
  return Card::empty;
}

/** @return   all colors the opposite team has not jabbed so far
 **/
vector<Card::Color>
RetryColor::colors_not_jabbed_by_opposite_team()
{
  auto const& game = this->game();
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();
  vector<Card::Color> colors;
  for (auto const color : rule().card_colors()) {
    if (hand.contains(color) == 0) {
      rationale_.add(_("Heuristic::condition::%s not in the hand", _(color)));
      continue;
    }
    if (cards_information.color_runs(color) == 0) {
      rationale_.add(_("Heuristic::condition::%s not played so far", _(color)));
      continue;
    }

    if (can_be_jabbed_by_opposite_team(color)) {
      rationale_.add(_("Heuristic::condition::%s can be jabbed by opposite team", _(color)));
      continue;
    }

    // check that the trick could get through
    if (cards_information.played(color)
        + (game.playerno() - 1)
        + hand.count(color)
        > game.cards().count(color)) {
      rationale_.add(_("Heuristic::condition::not enough remaining cards for a second rund of %s", _(color)));
      continue;
    }

    rationale_.add(_("Heuristic::condition::%s possible color", _(color)));
    colors.push_back(color);
  }
  return colors;
}

/** @return   whether color was jabbed by the opposite team
 **/
bool
RetryColor::can_be_jabbed_by_opposite_team(Card::Color color)
{
  for (auto const& player : game().players()) {
    if (   guessed_opposite_team(player)
        && !cards_information(player).can_have(color)) {
      return true;
    }
  }
  return false;
}

} // namespace Heuristics
