/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "start_with_color_blank_ace.h"

#include "../../ai.h"
#include "../../../../game/game.h"


namespace Heuristics {

auto StartWithColorBlankAce::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return true;
}


StartWithColorBlankAce::StartWithColorBlankAce(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::start_with_color_blank_ace)
{ }


StartWithColorBlankAce::~StartWithColorBlankAce() = default;


auto StartWithColorBlankAce::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_has_color_blank_ace()
         );
}


auto StartWithColorBlankAce::card_to_play(Trick const& trick) -> Card
{
  auto const ace = get_blank_ace(trick);
  if (!ace) {
    rationale_.add(_("Heuristic::reject::no suitable solo color ace found"));
    return {};
  }

  rationale_.add(_("Heuristic::return::ace found: %s", _(ace)));
  return ace;
}


auto StartWithColorBlankAce::get_blank_ace(Trick const& trick) -> Card
{
  auto const& game = trick.game();
  auto const opposite_trump_behind = condition_opposite_trump_behind(trick);
  auto const solo_contra = (is_solo(game.type()) && player().team() == Team::contra);

  auto color_valid
    = [&](Card::Color const color) {
      return (   condition_has_color_blank_ace(color)
              && (   !opposite_trump_behind
                  || (   condition_opposite_players_behind_have_color(trick, color)
                      && (   solo_contra
                          || condition_enough_remaining_color_cards(trick, color)))));
    };
  auto const color = best_of_if(game.cards().colors(),
                                color_valid,
                                [this](Card::Color const color) {
                                return cards_information().remaining_others(color);
                                }
                               );

  if (color == Card::nocardcolor)
    return {};
  return Card(color, Card::ace);
}


} // namespace Heuristics
