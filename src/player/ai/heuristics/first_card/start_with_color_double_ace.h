/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../../heuristic.h"

namespace Heuristics {

/** Start with a color double ace
 **/
class StartWithColorDoubleAce : public Heuristic {
public:
static auto is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group) -> bool;

public:
explicit StartWithColorDoubleAce(Ai const& ai);
StartWithColorDoubleAce(StartWithColorDoubleAce const&) = delete;
auto operator=(StartWithColorDoubleAce const&)          = delete;

~StartWithColorDoubleAce() override;

auto conditions_met(Trick const& trick) -> bool override;
auto card_to_play(Trick const& trick)   -> Card override;

private:
auto condition_have_color_double_ace() -> bool;
auto condition_have_color_double_ace(Card::Color color) -> bool;

Card get_double_ace(Trick const& trick);
};
} // namespace Heuristics
