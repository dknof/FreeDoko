/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "start_with_color_ten.h"

#include "../../ai.h"
#include "../../../../game/game.h"
#include "../../../../party/rule.h"


namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool StartWithColorTen::is_valid(GameTypeGroup const game_type,
                                 PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
StartWithColorTen::StartWithColorTen(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::start_with_color_ten)
{ }

/** destructor
 **/
StartWithColorTen::~StartWithColorTen() = default;

/** @return  whether the conditions are met
 **/
bool
StartWithColorTen::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
         );
}

/** @return   card to play
 **/
Card
StartWithColorTen::card_to_play(Trick const& trick)
{
  auto const& game = this->game();
  auto const opposite_trump_behind = condition_opposite_trump_behind(trick);
  auto const solo_contra = (is_solo(game.type()) && player().team() == Team::contra);

  auto color_valid
    = [&](Card::Color const color) {
      auto const ten = Card(color, Card::ten);
      return (   condition_has(ten)
              && !condition_is_trump(ten)
              && condition_noone_else_has(Card(color, Card::ace))
              && (   !opposite_trump_behind
                  || (   condition_opposite_players_behind_have_color(trick, color)
                      && (   solo_contra
                          || condition_enough_remaining_color_cards(trick, color)))));
    };
  auto const color = best_of_if(game.cards().colors(),
                                color_valid,
                                [this](Card::Color const color) {
                                return remaining_cards(color);
                                }
                               );

  if (color == Card::nocardcolor) {
    rationale_.add(_("Heuristic::reject::no suitable color ten found (not enough cards / already jabbed)"));
    return {};
  }

  auto const ten = Card(color, Card::ten);
  rationale_.add(_("Heuristic::return::ten found: %s", _(ten)));
  return ten;
}

/** @return   the remaning cards of 'color'
 **/
int
StartWithColorTen::remaining_cards(Card::Color const color)
{
  auto const& game = this->game();
  auto const& hand = this->hand();
  auto const ten = Card(color, Card::ten);
  auto const& cards_information = this->cards_information();
  unsigned const remaining_cards
    = (game.cards().count(color)
       - (cards_information.played(color) + hand.count(color)));

  // modified remaining cards
  // Modified by the number of tens of the color.
  // This is used to prefer single tens to double tens.
  unsigned const modified_remaining_cards
    = (remaining_cards
       +  (  (hand.count(ten) + cards_information.played(ten)
              < rule()(Rule::Type::number_of_same_cards))
           ? (game.cards().count(color) - 1) // *Value*
           : 0) );

  return modified_remaining_cards;
}

} // namespace Heuristics
