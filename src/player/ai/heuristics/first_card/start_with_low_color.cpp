/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "start_with_low_color.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool StartWithLowColor::is_valid(GameTypeGroup const game_type,
                                 PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
StartWithLowColor::StartWithLowColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::start_with_low_color)
{ }

/** destructor
 **/
StartWithLowColor::~StartWithLowColor() = default;

/** @return  whether the conditions are met
 **/
bool
StartWithLowColor::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && condition_has_color()
          && !condition_partner_guessed()
          && !condition_next_player_guessed_opposite_team(trick)
         );
}

/** @return   card to play
 **/
Card
StartWithLowColor::card_to_play(Trick const& trick)
{
  if (auto const card = singleton_color_card()) {
    rationale_.add(_("Heuristic::return::singleton color card - %s", _(card)));
    return card;
  }
  if (auto const card = smallest_color_card_from_longest_color()) {
    rationale_.add(_("Heuristic::return::smallest color card from longest color - %s", _(card)));
    return card;
  }

  rationale_.add(_("Heuristic::reject::no good color card with points < 10 found"));
  return {};
}

/** @return   a singleton color card
 **/
Card
StartWithLowColor::singleton_color_card()
{
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();
  Card best_card;
  for (auto card : hand.cards()) {
    if (   card.points() >= 10
        || card.istrump()
        || hand.count(card.color()) > 1)
      continue;

    if (   best_card.is_empty()
        || (cards_information.remaining_others(card.color())
            > cards_information.remaining_others(best_card.color()))
        || (cards_information.color_runs(best_card.color()) > 0
            && (    cards_information.color_runs(card.tcolor()) == 0
                || best_card.value() > card.value()) ) ) {
      best_card = card;
    }
  }

  if (!best_card) {
    rationale_.add(_("Heuristic::condition::no singleton color card with less then 10 points found"));
    return {};
  }

  rationale_.add(_("Heuristic::condition::lowest singleton color card: %s", _(best_card)));
  return best_card;
}

/** @return   smallest color cord from the longest color
 **/
Card
StartWithLowColor::smallest_color_card_from_longest_color()
{
  auto const& hand = this->hand();
  auto longest_color = this->longest_color();

  if (!longest_color) {
    rationale_.add(_("Heuristic::condition::no color card with less then 10 points found"));
    return {};
  }
  auto const card = hand.lowest_card(longest_color);
  rationale_.add(_("Heuristic::condition::lowest card of the longest color: %s", _(card)));
  return card;
}

/** @return   longest color with a card below 10 points
 **/
Card::Color
StartWithLowColor::longest_color()
{
  auto const& hand = this->hand();
  auto longest_color = Card::nocardcolor;
  unsigned longest_color_count = 0;
  for (auto const color : game().cards().colors()) {
    auto const color_count = hand.count(color);
    if (   color_count > longest_color_count
        && hand.lowest_card(color).points() < 10) {
      longest_color = color;
      longest_color_count = color_count;
    }
  }
  return longest_color;
}

} // namespace Heuristics
