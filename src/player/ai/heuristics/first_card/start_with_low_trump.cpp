/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "start_with_low_trump.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool StartWithLowTrump::is_valid(GameTypeGroup const game_type,
                                 PlayerTypeGroup const player_group)
{
  return is_with_partner(game_type, player_group);
}

/** constructor
 **/
StartWithLowTrump::StartWithLowTrump(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::start_with_low_trump)
{ }

/** destructor
 **/
StartWithLowTrump::~StartWithLowTrump() = default;

/** @return  whether the conditions are met
 **/
bool
StartWithLowTrump::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && !condition_partner_guessed());
}

/** @return   card to play
 **/
Card
StartWithLowTrump::card_to_play(Trick const& trick)
{
  auto const& hand = this->hand();

  auto const card = HandCard(hand, lowest_best_trump_card(trick));

  if (!card.is_jabbed_by(value(Aiconfig::Type::trumplimit_normal))) {
    rationale_.add(_("Heuristic::reject::trump too high"));
    return {};
  }
  if (card.points() >= 10) {
    rationale_.add(_("Heuristic::reject::selected trump has %i points", card.value()));
    return {};
  }

  rationale_.add(_("Heuristic::return::lowest best trump: %s", _(card)));
  return card;
}

} // namespace Heuristics
