/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_color_trick_high.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabColorTrickHigh::is_valid(GameTypeGroup const game_type,
                               PlayerTypeGroup const player_group)
{
  return is_with_trump_color(game_type);
}

/** constructor
 **/
JabColorTrickHigh::JabColorTrickHigh(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_color_trick_high)
{ }

/** destructor
 **/
JabColorTrickHigh::~JabColorTrickHigh() = default;

/** @return  whether the conditions are met
 **/
bool
JabColorTrickHigh::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && !condition_last_card(trick)
          && condition_color_trick(trick)
          && condition_can_jab_with_trump(trick)
          && condition_opposite_or_unknown_player_behind(trick)
          && !condition_first_color_run(trick)
          && condition_many_trick_points(trick)
          && !(   condition_winnerplayer_guessed_same_team(trick)
               && condition_high_winnercard(trick))
          );
}

/** @return   card to play
 **/
Card
JabColorTrickHigh::card_to_play(Trick const& trick)
{
  auto const card = result_high_trump_jabbing_card(trick);
  if (   game().type() == GameType::normal
      // ToDo: check the own gameplay for team informations
      && game().teaminfo().get(player()) == Team::unknown
      && team() == Team::re
      && hand().contains(Card::club_queen)
      && card.value() == Card::queen
      && card != Card::spade_queen) {
    if (card.color() == Card::spade) {
      rationale_.add(_("Heuristic::return::%s", _(Card::club_queen)));
      return Card::club_queen;
    }
    if (   condition_opposite_or_unknown_player_behind(trick)
        && (   trick.points() >= 8
            || trick.actcardno() == 1)) {
      rationale_.add(_("Heuristic::return::%s", _(Card::club_queen)));
      return Card::club_queen;
    }
  }
  // ToDo: only take a dulle/swine if the trick has many points. Else prefer a queen
  rationale_.add(_("Heuristic::return::jab with high trump %s", _(card)));
  return card;
}

/** @return   whether the trick has many points
 **/
bool
JabColorTrickHigh::condition_many_trick_points(Trick const& trick)
{
  if (trick.points() >= 10) {
    rationale_.add(_("Heuristic::condition::many points in the trick"));
    return true;
  }
  if (   trick.points() + 4 * trick.remainingcardno() >= 8 + 4 + 4
      && game().announcements().last(team()) != Announcement::noannouncement) {
    rationale_.add(_("Heuristic::condition::my team has made an announcement"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::not enough points"));
  return false;
}

} // namespace Heuristics
