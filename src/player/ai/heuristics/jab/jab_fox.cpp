/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_fox.h"

#include "../../ai.h"

namespace Heuristics {

auto JabFox::is_valid(GameTypeGroup const game_type,
                      PlayerTypeGroup const player_group) -> bool
{
  return is_with_trump_color(game_type);
}


JabFox::JabFox(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_fox)
{ }


JabFox::~JabFox() = default;


auto JabFox::conditions_met(Trick const& trick) -> bool
{
  return (   condition_trick_contains_fox(trick)
          && !condition_is_marriage_determination_trick(trick)
          && condition_can_jab_with_trump(trick)
          && !(   condition_winnerplayer_same_team(trick)
               && !condition_opposite_or_unknown_team_can_jab(trick))
          && !condition_partner_jabbed_with_high_trump(trick)
         );
}


auto JabFox::card_to_play(Trick const& trick) -> Card
{
  return result_high_trump_jabbing_card(trick);
}


auto JabFox::condition_trick_contains_fox(Trick const& trick) -> bool
{
  auto const& game = this->game();
  auto const fox = game.cards().fox();
  if (!fox) {
    rationale_.add(_("Heuristic::condition::there exists no fox in this game"));
    return false;
  }
  if (!trick.contains(fox)) {
    rationale_.add(_("Heuristic::condition::no fox in the trick"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::fox in the trick"));
  return true;
}

} // namespace Heuristics
