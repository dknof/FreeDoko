/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_second_color_run.h"

#include "../../ai.h"
#include "../../../cards_information.of_player.h"
#include "../../../team_information.h"

#include "../../../../party/rule.h"

namespace Heuristics {

auto JabSecondColorRun::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return is_with_trump_color(game_type);
}


JabSecondColorRun::JabSecondColorRun(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_second_color_run)
{ }


JabSecondColorRun::~JabSecondColorRun() = default;


auto JabSecondColorRun::conditions_met(Trick const& trick) -> bool
{
  return (   condition_second_color_run(trick)
          && !condition_partner_already_jabbed_high(trick)
          && condition_can_jab_with_trump(trick)
          && !condition_opposite_player_with_announcement_behind(trick)
          && condition_enough_remaining_lower_color_cards(trick)
          && condition_opposite_players_behind_have_color(trick, trick.startcard().tcolor())
         );
}

auto JabSecondColorRun::card_to_play(Trick const& trick) -> Card
{
  if (trick.islastcard()) {
    rationale_.add(_("Heuristic::condition::last card"));
    auto const card = jabbing_card_for_last_player(trick);
    if (trick.points() >= 20)
      return card;
    if (trick.points() >= 10 && !card.is_special())
      return card;
    if (below_trump_card_limit(card))
      return card;
    rationale_.add(_("Heuristic::condition::not enough trick points for jabbing with %s", _(card)));
    return {};
  }

  if (   trick.remainingcardno() == 2
      && guessed_same_team(trick.lastplayer())) {
    if (same_team(trick.lastplayer()))
      rationale_.add(_("Heuristic::condition::last player of same team"));
    else
      rationale_.add(_("Heuristic::condition::last player guessed of same team"));
    return jabbing_card_for_last_player(trick);
  }

  if (game().announcements().last(team()) >= Announcement::no90) {
    rationale_.add(_("Heuristic::condition::announcement made"));
    auto const card = get_high_trump(trick);
    if (card)
      return card;
  }
  if (   game().announcements().last(team()) != Announcement::noannouncement
      && trick.points() >= 10
      && value(Aiconfig::Type::cautious)) {
    rationale_.add(_("Heuristic::condition::my team has made an announcement"));
    rationale_.add(_("Heuristic::condition::the trick has at least %u points", 10u));
    auto const card = get_high_trump(trick);
    if (card)
      return card;
  }
  if (only_own_team_behind(trick)) {
    rationale_.add(_("Heuristic::condition::only own team behind"));
    auto const card = jabbing_card_for_last_player(trick);
    if (card)
      return card;
  }
  if (value(Aiconfig::Type::chancy)) {
    rationale_.add(_("Heuristic::condition::I am chancy"));
    auto const card = get_trump_at_most_fox(trick);
    if (card)
      return card;
  }
  auto const card = get_trump_above_fox(trick);
  if (   card == Card::spade_queen
      && team() == Team::re
      && !own_team_guessed()) {
    rationale_.add(_("Heuristic::jab::prefer club queen in order to show my team"));
    return Card::club_queen;
  }
  return card;
}


auto JabSecondColorRun::condition_enough_remaining_lower_color_cards(Trick const& trick) -> bool
{
  if (   trick.issecondlastcard()
      && guessed_same_team(trick.lastplayer())) {
    rationale_.add(_("Heuristic::condition::last player guessed of same team"));
    if (condition_guessed_own_team_can_jab(trick)) {
      rationale_.add(_("Heuristic::condition::own team can jab %s", _(trick.winnercard())));
      return false;
    }
    return true;
  }
  if (   trick.remainingcardno() == 3
      && guessed_same_team(trick.lastplayer())
      && guessed_same_team(trick.secondlastplayer())) {
    rationale_.add(_("Heuristic::condition::last players guessed of same team"));
    if (condition_guessed_own_team_can_jab(trick)) {
      rationale_.add(_("Heuristic::condition::own team can jab %s", _(trick.winnercard())));
      return false;
    }
    return true;
  }
  auto const color = trick.startcard().color();
  if (trick.issecondlastcard()) {
    if (guessed_hand(trick.lastplayer()).count(color) <= 1 - (ai().difficulty() == AiconfigDifficulty::chancy ? 1 : 0)) {
      rationale_.add(_("Heuristic::condition::not enough remaining cards of color %s for last player", _(color)));
      return false;
    }
  }
  if (cards_information().remaining(color) < trick.remainingcardno()) {
    rationale_.add(_("Heuristic::condition::not enough remaining cards of %s for all other players", _(color)));
    return false;
  }
  rationale_.add(_("Heuristic::condition::enough remaining cards of %s for all other players", _(color)));
  return true;
}


auto JabSecondColorRun::condition_partner_already_jabbed_high(Trick const& trick) -> bool
{
  if (!condition_winnerplayer_guessed_same_team(trick))
    return false;
  auto const& winnercard = trick.winnercard();
  if (!winnercard.istrump()) {
    rationale_.add(_("Heuristic::condition::winnercard is no trump"));
    if (winnercard.value() == Card::Value::ace) {
      rationale_.add(_("Heuristic::condition::winnercard is the ace"));
      return true;
    }
    if (   winnercard.value() == Card::Value::ten
        && cards_information().remaining(winnercard.color(), Card::Value::ace) == 0) {
      rationale_.add(_("Heuristic::condition::winnercard is the ten but no ace remaining"));
      return true;
    }
    return false;
  }
  if (   below_trump_card_limit(winnercard)
      && (   below_lowest_trump_card_limit(winnercard)
          || trick.points() > 6) ) {
    rationale_.add(_("Heuristic::condition::winnercard is no high trump"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::winnercard is a high trump"));
  return true;
}


auto JabSecondColorRun::condition_following_opposite_player_can_jab(Trick const& trick) -> bool
{
  auto const color = trick.startcard().color();
  for (auto const& player : trick.remaining_following_players()) {
    if (guessed_same_team(player))
      continue;
    if (cards_information(player).must_have(color))
      continue;
    if (  value(Aiconfig::Type::chancy)
        ? guessed_hand(player).contains(color)
        : guessed_hand(player).count(color) >= 2) {
      continue;
    }
    rationale_.add(_("Heuristic::condition::following opposite player can jab"));
    return true;
  }
  return false;
}


auto JabSecondColorRun::condition_second_color_run(Trick const& trick) -> bool
{
  if (trick.isstartcard()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  } else if (trick.startcard().istrump()) {
    rationale_.add(_("Heuristic::condition::trump trick"));
    return false;
  }
  auto const color = trick.startcard().tcolor();
  switch (cards_information().color_runs(color)) {
  case 0:
    rationale_.add(_("Heuristic::condition::first color run"));
    return false;
  case 1:
    rationale_.add(_("Heuristic::condition::second color run"));
    return true;
  default:
    rationale_.add(_("Heuristic::condition::%u color runs", cards_information().color_runs(color)));
    return false;
  }
}


auto JabSecondColorRun::get_trump_at_most_fox(Trick const& trick) -> Card
{
  for (auto const& card : hand().cards_at_max_fox()) {
    if (trick.isjabbed(card)) {
      rationale_.add(_("Heuristic::return::jab with %s", _(card)));
      return card;
    }
  }
  return {};
}


auto JabSecondColorRun::get_trump_above_fox(Trick const& trick) -> Card
{
  auto const& hand = this->hand();
  auto const card = hand.same_or_higher_card(   value(Aiconfig::Type::cautious)
                                             && trick.points() >= 10
                                             ? Card::diamond_queen
                                             : Card::diamond_jack);
  if (!trick.isjabbed(card)) {
    auto const card = hand.next_jabbing_card(trick.winnercard()); // cppcheck-suppress shadowVariable
    rationale_.add(_("Heuristic::condition::next jabbing card: %s", _(card)));
    return card;
  }
  if (!card) {
    rationale_.add(_("Heuristic::condition::no card found over the fox"));
    auto const card = hand.highest_trump(); // cppcheck-suppress shadowVariable
    rationale_.add(_("Heuristic::return::highest trump: %s", _(card)));
    return card;
  }
  rationale_.add(_("Heuristic::return::next trump over fox: %s", _(card)));
  return card;
}


auto JabSecondColorRun::get_high_trump(Trick const& trick) -> Card
{
  auto const& hand = this->hand();

  if (trick.points() >= 10) {
    auto const card = get_special_trump(trick);
    if (card) {
      rationale_.add(_("Heuristic::return::jab trick with many points with %s", _(card)));
      return card;
    }
  }

  auto card = hand.highest_card(Card::queen);
  if (!card)
    card = hand.highest_card(Card::jack);
  if (!card)
    return {};
  if (trick.isjabbed(card)) {
    rationale_.add(_("Heuristic::return::jab with high trump %s", _(card)));
    return optimized_jabbing_card(trick, card);
  }
  return {};
}


auto JabSecondColorRun::get_special_trump(Trick const& trick) -> Card
{
  if (rule()(Rule::Type::dullen)) {
    auto const dulle = Card::dulle;
    if (   hand().contains(dulle)
        && trick.isjabbed({hand(), dulle})
        && !opposite_player_behind_can_jab_dulle(trick)) {
      return dulle;
    }
  }
  if (   hand().has_swines()
      && trick.isjabbed({hand(), game().cards().swine()})
      && !opposite_player_behind_can_jab_swine(trick)) {
    return game().cards().swine();
  }
  if (hand().has_hyperswines()) {
    return game().cards().hyperswine();
  }
  return {};
}


auto JabSecondColorRun::opposite_player_behind_can_jab_dulle(Trick const& trick) -> bool
{
  if (   rule()(Rule::Type::dullen_second_over_first)
      && condition_opposite_players_behind_have(trick, Card::dulle))
    return true;
  if (   game().swines().swines_announced()
      && !guessed_same_team(game().swines().swines_owner())
      && !trick.has_played(game().swines().swines_owner()))
    return true;
  if (opposite_player_behind_can_jab_swine(trick))
    return true;
  return false;
}


auto JabSecondColorRun::opposite_player_behind_can_jab_swine(Trick const& trick) -> bool
{
  if (   game().swines().hyperswines_announced()
      && !guessed_same_team(game().swines().hyperswines_owner())
      && !trick.has_played(game().swines().hyperswines_owner()))
    return true;
  return false;
}

} // namespace Heuristics
