/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../../heuristic.h"

namespace Heuristics {

/** Jab the second color run with a small trump
 **/
class JabSecondColorRun : public Heuristic {
public:
static auto is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group) -> bool;

public:
explicit JabSecondColorRun(Ai const& ai);
JabSecondColorRun(JabSecondColorRun const&) = delete;
auto operator=(JabSecondColorRun const&)    = delete;

~JabSecondColorRun() override;

auto conditions_met(Trick const& trick) -> bool override;
auto card_to_play(Trick const& trick)   -> Card override;

private:
auto condition_second_color_run(Trick const& trick)                  -> bool;
auto condition_enough_remaining_lower_color_cards(Trick const& trick)-> bool;
auto condition_partner_already_jabbed_high(Trick const& trick)       -> bool;
auto condition_following_opposite_player_can_jab(Trick const& trick) -> bool;
auto get_trump_at_most_fox(Trick const& trick) -> Card;
auto get_trump_above_fox(Trick const& trick)   -> Card;
auto get_high_trump(Trick const& trick)        -> Card;
auto get_special_trump(Trick const& trick)     -> Card;
auto opposite_player_behind_can_jab_dulle(Trick const& trick) -> bool;
auto opposite_player_behind_can_jab_swine(Trick const& trick) -> bool;
};
} // namespace Heuristics
