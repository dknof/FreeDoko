/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_small_color_card.h"

#include "../../ai.h"

namespace Heuristics {

auto JabSmallColorCard::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return true;
}


JabSmallColorCard::JabSmallColorCard(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_small_color_card)
{ }


JabSmallColorCard::~JabSmallColorCard() = default;


auto JabSmallColorCard::conditions_met(Trick const& trick) -> bool
{
  return (   condition_winnercard_small_color_card(trick)
          && condition_can_jab_with_small_color_card(trick));
}


auto JabSmallColorCard::card_to_play(Trick const& trick) -> Card
{
  auto const card = hand().next_jabbing_card(trick.winnercard());
  rationale_.add(_("Heuristic::jab::next jabbing card: %s", _(card)));
  return card;
}


auto JabSmallColorCard::condition_winnercard_small_color_card(Trick const& trick) -> bool
{
  if (trick.isempty()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  if (trick.winnercard().istrump()) {
    rationale_.add(_("Heuristic::condition::winnecard is trump"));
    return false;
  }
  if (trick.winnercard().value() >= 10) {
    rationale_.add(_("Heuristic::condition::winnecard is a big color card"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::winnecard is a small color card"));
  return true;
}


auto JabSmallColorCard::condition_can_jab_with_small_color_card(Trick const& trick) -> bool
{
  if (!hand().can_jab(trick)) {
    rationale_.add(_("Heuristic::condition::cannot jab"));
    return false;
  }
  auto const card = hand().next_higher_card(trick.winnercard());
  if (card.istrump()) {
    rationale_.add(_("Heuristic::condition::next jabbing card is trump"));
    return false;
  }
  if (card.value() >= 10) {
    rationale_.add(_("Heuristic::condition::next jabbing card has at least 10 points"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::next jabbing card is small"));
  return true;
}

} // namespace Heuristics
