/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../../heuristic.h"

namespace Heuristics {

/** Jab a small color card with a not so big one
 **/
class JabSmallColorCard : public Heuristic {
public:
static auto is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group) -> bool;

public:
explicit JabSmallColorCard(Ai const& ai);
JabSmallColorCard(JabSmallColorCard const&) = delete;
auto operator=(JabSmallColorCard const&)    = delete;

~JabSmallColorCard() override;

auto conditions_met(Trick const& trick) -> bool override;
auto card_to_play(Trick const& trick)   -> Card override;

private:
auto condition_winnercard_small_color_card(Trick const& trick) -> bool;
auto condition_can_jab_with_small_color_card(Trick const& trick) -> bool;
};
} // namespace Heuristics
