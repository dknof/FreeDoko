/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_swine_with_hyperswine.h"

#include "../../ai.h"
#include "../../../../party/rule.h"

namespace Heuristics {

auto JabSwineWithHyperswine::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return is_with_trump_color(game_type);
}


JabSwineWithHyperswine::JabSwineWithHyperswine(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_swine_with_hyperswine)
{ }


JabSwineWithHyperswine::~JabSwineWithHyperswine() = default;


auto JabSwineWithHyperswine::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && condition_opposite_swine_in_trick(trick)
          && condition_can_jab_with_hyperswines(trick)
         );
}


auto JabSwineWithHyperswine::card_to_play(Trick const& trick) -> Card
{
  rationale_.add(_("Heuristic::return::jab with hyperswine"));
  return game().cards().hyperswine();
}


auto JabSwineWithHyperswine::condition_opposite_swine_in_trick(Trick const& trick) -> bool
{
  if (!game().rule()(Rule::Type::swines)) {
    return false;
  }
  if (condition_winnerplayer_surely_same_team(trick)) {
    return false;
  }
  if (trick.winnercard() != game().cards().swine()) {
    rationale_.add(_("Heuristic::condition::winnercard is no swine"));
    return false;
  }
    rationale_.add(_("Heuristic::condition::winnercard is a swine"));
  return true;
}


auto JabSwineWithHyperswine::condition_can_jab_with_hyperswines(Trick const& trick) -> bool
{
  if (!game().rule()(Rule::Type::hyperswines)) {
    return false;
  }
  if (!hand().has_hyperswines()) {
    rationale_.add(_("Heuristic::condition::have no hyperswine"));
    return false;
  }
  if (!condition_can_jab_with(trick, game().cards().hyperswine())) {
    return false;
  }
  rationale_.add(_("Heuristic::condition::have a hyperswine"));
  return true;
}

} // namespace Heuristics
