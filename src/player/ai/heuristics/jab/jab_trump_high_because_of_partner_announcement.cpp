/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_trump_high_because_of_partner_announcement.h"

#include "../../ai.h"

namespace Heuristics {

auto JabTrumpHighBecauseOfPartnerAnnouncement::is_valid(GameTypeGroup const game_type,
                                                        PlayerTypeGroup const player_group) -> bool
{
  return (   !is_solo(game_type)
          && is_with_partner(game_type, player_group));
}


JabTrumpHighBecauseOfPartnerAnnouncement::JabTrumpHighBecauseOfPartnerAnnouncement(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_trump_high_because_of_partner_announcement)
{ }


JabTrumpHighBecauseOfPartnerAnnouncement::~JabTrumpHighBecauseOfPartnerAnnouncement() = default;


auto JabTrumpHighBecauseOfPartnerAnnouncement::conditions_met(Trick const& trick) -> bool
{
  return (   condition_trump_trick(trick)
          && condition_partner_announcement()
          && !condition_last_card(trick)
          && !condition_guessed_partner_behind(trick)
          && !condition_high_winnercard(trick)
          && condition_can_jab(trick)
         );
}


auto JabTrumpHighBecauseOfPartnerAnnouncement::card_to_play(Trick const& trick) -> Card
{
  auto const card = HandCard(hand(), result_high_trump_jabbing_card(trick));

  if (   card.is_special()
      && condition_can_be_jabbed_by_opposite_team(trick, card)) {
    rationale_.add(_("Heuristic::reject::high jabbing card is special and can be jabbed"));
    auto const queen = hand().highest_card(Card::queen);
    if (trick.isjabbed(queen)) {
      rationale_.add(_("Heuristic::return::highest queen: %s", _(queen)));
      return queen;
    }
    return {};
  }

  rationale_.add(_("Heuristic::return::high jabbing card: %s", _(card)));
  return card;
}

} // namespace Heuristics
