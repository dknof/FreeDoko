/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_with_highest_trump.h"

#include "../../../player.h"
#include "../../../../card/hand.h"
#include "../../../../game/game.h"

namespace Heuristics {

auto JabWithHighestTrump::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return is_with_trump(game_type);
}


JabWithHighestTrump::JabWithHighestTrump(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_with_highest_trump)
{ }


JabWithHighestTrump::~JabWithHighestTrump() = default;


auto JabWithHighestTrump::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && !condition_last_card(trick)
          && !condition_marriage_decision_for_me(trick)
          && condition_can_jab_with_trump(trick)
          && (   condition_high_winnercard(trick)
              ? !condition_winnerplayer_guessed_same_team(trick)
              : !condition_winnerplayer_same_team(trick))
          && condition_opposite_or_unknown_player_behind(trick)
          && condition_many_points(trick)
          && condition_have_highest_trump(trick)
         );
}


auto JabWithHighestTrump::card_to_play(Trick const& trick) -> Card
{
  auto const card = jabbing_card(trick);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no suitable card found to jab with"));
    return card;
  }
  rationale_.add(_("Heuristic::return::jab with highest trump: %s", _(card)));
  return card;
}


auto JabWithHighestTrump::condition_many_points(Trick const& trick) -> bool
{
  unsigned const min_points = (game().type() == GameType::poverty
                               ? 14
                               : 10);
  if (trick.points() >= min_points) {
    rationale_.add(_("Heuristic::condition::the trick has at least %u points", min_points));
    return true;
  }
  rationale_.add(_("Heuristic::condition::the trick has less then %u points", min_points));
  return false;
}


auto JabWithHighestTrump::condition_have_highest_trump(Trick const& trick) -> bool
{
  auto const& trump = hand().highest_trump();
  if (!trump) {
    rationale_.add(_("Heuristic::condition::have no trump"));
    return false;
  }
  Trick trick2 = trick;
  trick2.add(trump);
  for (auto const& player : trick2.remaining_players()) {
    if (guessed_hand(player).can_jab(trick)) {
      rationale_.add(_("Heuristic::condition::%s can jab the highest trump", player.get().name()));
      return false;
    }
  }
  rationale_.add(_("Heuristic::condition::have the highest trump"));
  return true;
}


auto JabWithHighestTrump::jabbing_card(Trick const& trick) -> Card
{
  auto const card = hand().highest_trump();
  return optimized_jabbing_card(trick, card);
}

} // namespace Heuristics
