/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_with_lowest_card.h"

#include "../../ai.h"
#include "../../../../party/rule.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabWithLowestCard::is_valid(GameTypeGroup const game_type,
                                 PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
JabWithLowestCard::JabWithLowestCard(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_with_lowest_card)
{ }

/** destructor
 **/
JabWithLowestCard::~JabWithLowestCard() = default;

/** @return  whether the conditions are met
 **/
bool
JabWithLowestCard::conditions_met(Trick const& trick)
{
  return condition_can_jab(trick);
}

/** @return   card to play
 **/
Card
JabWithLowestCard::card_to_play(Trick const& trick)
{
  HandCard best_card;
  auto const& hand = this->hand();
  for (auto const& card : hand.validcards(trick)) {
    if (!trick.isjabbed(card))
      continue;

    if (   !best_card
        || card.is_jabbed_by(best_card)) {
      best_card = card;
    }
  }
  if (!best_card) {
    DEBUG_ASSERTION(false,
                    "Heuristic::JabWithLowestCard::get_card(trick)\n"
                    "  no valid card found.\n"
                    "Hand:\n"
                    << hand
                    << "Trick:\n"
                    << trick);
    return {};
  }
  // Spezifische Verbesserungen
  if (   best_card.isfox()
      && is_normal(game().type())) {
    for (auto const card : {
         Card::diamond_jack,  Card::heart_jack,  Card::spade_jack,  Card::club_jack,
         Card::diamond_queen, Card::heart_queen, Card::spade_queen, Card::club_queen}) {
      if (hand.contains(card)) {
        rationale_.add(_("Heuristic::condition::prefer %s to %s", _(card), _(best_card)));
        best_card = card;
        break;
      }
    }
  }
  if (   best_card.points() >= 10
      && best_card.istrump()
      && is_with_trump_color(game().type())) {
    for (auto const card : {
         Card::diamond_jack,  Card::heart_jack,  Card::spade_jack,  Card::club_jack,
         Card::diamond_queen}) {
      if (hand.contains(card)) {
        rationale_.add(_("Heuristic::condition::prefer %s to %s", _(card), _(best_card)));
        best_card = card;
        break;
      }
    }
  }
  if (   best_card.isdulle()
      && rule()(Rule::Type::dullen_second_over_first)
      && cards_information().remaining_others(Card::dulle)) {
    if (hand.has_swines()) {
      auto const card = game().cards().swine();
      rationale_.add(_("Heuristic::condition::prefer %s to %s", _(card), _(best_card)));
      best_card = card;
    } else if (hand.has_hyperswines()) {
      auto const card = game().cards().hyperswine();
      rationale_.add(_("Heuristic::condition::prefer %s to %s", _(card), _(best_card)));
      best_card = card;
    }
  }
  if (   !below_trump_card_limit(best_card)
      && game().type() == GameType::normal
      && team() == Team::re
      && !player().announcement()
      && best_card != Card::club_queen
      && hand.contains(Card::club_queen)
      && trick.isvalid(Card::club_queen, hand)
      && trick.isjabbed({hand, Card::club_queen})) {
    rationale_.add(_("Heuristic::return::play club queen to show the team"));
    return Card::club_queen;
  }
  rationale_.add(_("Heuristic::return::jab with lowest card: %s", _(best_card)));
  return best_card;
}

} // namespace Heuristics
