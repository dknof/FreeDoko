/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_with_lowest_card_below_trump_card_limit.h"

#include "../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabWithLowestCardBelowTrumpCardLimit::is_valid(GameTypeGroup const game_type,
                                                    PlayerTypeGroup const player_group)
{
  return is_with_trump(game_type);
}

/** constructor
 **/
JabWithLowestCardBelowTrumpCardLimit::JabWithLowestCardBelowTrumpCardLimit(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::jab_with_lowest_card_below_trump_card_limit)
{ }

/** destructor
 **/
JabWithLowestCardBelowTrumpCardLimit::~JabWithLowestCardBelowTrumpCardLimit() = default;

/** @return  whether the conditions are met
 **/
bool
JabWithLowestCardBelowTrumpCardLimit::conditions_met(Trick const& trick)
{
  return condition_can_jab(trick);
}

/** @return   card to play
 **/
Card
JabWithLowestCardBelowTrumpCardLimit::card_to_play(Trick const& trick)
{
  HandCard best_card;
  auto const& hand = this->hand();
  for (auto const& card : hand.validcards(trick)) {
    if (card.value() >= 10)
      continue;
    if (!trick.isjabbed(card))
      continue;

    if (   !best_card
        || card.is_jabbed_by(best_card)) {
      best_card = card;
    }
  }
  if (!best_card) {
    rationale_.add(_("Heuristic::reject::no small card for jabbing found"));
    return {};
  }
  if (   !below_trump_card_limit(best_card)
      || best_card.possible_hyperswine()) {
    rationale_.add(_("Heuristic::reject::lowest card %s is not below the trump card limit", _(best_card)));
    return {};
  }
  rationale_.add(_("Heuristic::return::jab with lowest card: %s", _(best_card)));
  return best_card;
}

} // namespace Heuristics
