/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "second_last_jab_for_following_pfund.h"

#include "../../../team_information.h"
#include "../../../player.h"
#include "../../../../card/hand.h"
#include "../../../../game/game.h"

namespace Heuristics {


auto SecondLastJabForFollowingPfund::is_valid(GameTypeGroup const game_type,
                                              PlayerTypeGroup const player_group) -> bool
{
  return is_with_partner(game_type, player_group);
}


SecondLastJabForFollowingPfund::SecondLastJabForFollowingPfund(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::second_last_jab_for_following_pfund)
{ }


SecondLastJabForFollowingPfund::~SecondLastJabForFollowingPfund() = default;


auto SecondLastJabForFollowingPfund::conditions_met(Trick const& trick) -> bool
{
  return (   condition_second_last_card(trick)
          && condition_winnerplayer_guessed_opposite_team(trick)
          && condition_last_player_guessed_same_team(trick)
          && condition_backhand_can_pfund(trick)
          && condition_can_jab(trick)
         );
}


auto SecondLastJabForFollowingPfund::card_to_play(Trick const& trick) -> Card
{
  auto const card = jabbing_card(trick);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no suitable card found to jab with"));
    return card;
  }
  if (card.points() >= 10 && guessed_hand(trick.lastplayer()).can_jab(trick + card)) {
    rationale_.add(_("Heuristic::reject::do not jab with card %s", _(card)));
    return {};
  }
  if (   is_with_trump_color(game().type())
      && card.value() == Card::nine) {
    for (auto const card : {Card::diamond_jack, Card::heart_jack}) { // cppcheck-suppress shadowVariable
      if (   hand().contains(card)
          && trick.isjabbed(card, hand())) {
        rationale_.add(_("Heuristic::return::jab with %s", _(card)));
        return card;
      }
    }
  }
  rationale_.add(_("Heuristic::return::jab with %s", _(card)));
  return card;
}


auto SecondLastJabForFollowingPfund::jabbing_card(Trick const& trick) -> HandCard
{
  if (   game().type() == GameType::normal
      && hand().contains(Card::club_queen)
      && trick.no() <= 6
      && game().announcements().last(Team::contra) == Announcement::noannouncement
      && !own_team_guessed()
      && trick.isvalid(Card::club_queen, hand())
      && trick.isjabbed(Card::club_queen, hand())
     ) {
    rationale_.add(_("Heuristic::return::jab with club queen to show the team"));
    return HandCard(hand(), Card::club_queen);
  }
  auto const card = hand().next_jabbing_card(trick.winnercard());
  if (!trick.isvalid(card))
    return {};
  if (   !card.is_special()
      && is_with_trump_color(game().type())) {
    auto const trumpcolor = game().cards().trumpcolor();
    auto const trump_ace  = HandCard(hand(), trumpcolor, Card::ace);
    auto const trump_ten  = HandCard(hand(), trumpcolor, Card::ten);
    auto const trump_king = HandCard(hand(), trumpcolor, Card::king);
    auto const trump_nine = HandCard(hand(), trumpcolor, Card::nine);
    if (   hand().contains(trump_ace)
        && !trump_ace.is_special()
        && (   card == trump_ten
            || card == trump_king
            || card == trump_nine)) {
      return trump_ace;
    }
    if (   hand().contains(trump_ten)
        && !trump_ten.is_special()
        && (   card == trump_king
            || card == trump_nine)) {
      return trump_ten;
    }
    if (   hand().contains(trump_king)
        && !trump_king.is_special()
        && card == trump_nine) {
      return trump_king;
    }
  }
  if (   game().type() == GameType::normal
      && (card == Card::heart_queen || card == Card::spade_queen)
      && hand().contains(Card::club_queen)
      && player().announcement() == Announcement::noannouncement
      && game().announcements().last(Team::contra) == Announcement::noannouncement
     ) {
    rationale_.add(_("Heuristic::return::jab with club queen to show the team"));
    return HandCard(hand(), Card::club_queen);
  }
  return card;
}

} // namespace Heuristics
