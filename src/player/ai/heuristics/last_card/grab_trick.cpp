/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "grab_trick.h"

#include "../../ai.h"
#include "../../../../game/game.h"


namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool GrabTrick::is_valid(GameTypeGroup const game_type,
                         PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
GrabTrick::GrabTrick(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::grab_trick)
{ }

/** destructor
 **/
GrabTrick::~GrabTrick() = default;

/** @return  whether the conditions are met
 **/
bool
GrabTrick::conditions_met(Trick const& trick)
{
  return (   condition_last_card(trick)
          && !condition_winnerplayer_same_team(trick)
          && condition_can_jab(trick)
         );
}

/** @return   card to play
 **/
Card
GrabTrick::card_to_play(Trick const& trick)
{
  auto const card = best_jabbing_card(trick);

  rationale_.add(_("Heuristic::return::jab the trick with %s", _(card)));
  return card;
}

} // namespace Heuristics
