/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_with_trump_ace_or_ten.h"

#include "../../../../game/game.h"
#include "../../../../card/hand.h"

namespace Heuristics {

/**
 * @return whether this heuristic is valid for this game type and player group
 */
bool LastPlayerJabWithTrumpAceOrTen::is_valid(GameTypeGroup const game_type,
                                              PlayerTypeGroup const player_group)
{
  return is_with_trump_color(game_type);
}


/** constructor
 */
LastPlayerJabWithTrumpAceOrTen::LastPlayerJabWithTrumpAceOrTen(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::last_player_jab_with_trump_ace_or_ten)
{ }


/**
 * @return whether the conditions met
 */
auto LastPlayerJabWithTrumpAceOrTen::conditions_met(Trick const& trick) -> bool
{
  auto const trumpcolor = trick.game().cards().trumpcolor();
  auto const ace = Card(trumpcolor, Card::ace);
  auto const ten = Card(trumpcolor, Card::ten);
  return (   condition_last_card(trick)
          && (   (   !hand().has_swines()
                  && condition_can_jab_with(trick, ace))
              || (ten != game().cards().dulle()
                  && condition_can_jab_with(trick, ten))
             ));
}


/**
 * @return which card to play
 */
auto LastPlayerJabWithTrumpAceOrTen::card_to_play(Trick const& trick) -> Card
{
  auto const trumpcolor = trick.game().cards().trumpcolor();

  if (!hand().has_swines()) {
    auto const ace = Card(trumpcolor, Card::ace);
    if (hand().contains(ace)) {
      rationale_.add(_("Heuristic::condition::jab trick with %s", _(ace)));
      return ace;
    }
  }
  auto const ten = Card(trumpcolor, Card::ten);
  if (ten != game().cards().dulle()) {
    if (hand().contains(ten)) {
      rationale_.add(_("Heuristic::condition::jab trick with %s", _(ten)));
      return ten;
    }
  }
  return {};
}

} // namespace Heuristics
