/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_to_marry.h"

#include "../../../../game/game.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool JabToMarry::is_valid(GameTypeGroup const game_type,
                          PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::marriage_undetermined
          && player_group == PlayerTypeGroup::contra);
}

/** constructor
 **/
JabToMarry::JabToMarry(Ai const& ai) :
  HeuristicMarriage(ai, Aiconfig::Heuristic::jab_to_marry)
{ }

/** destructor
 **/
JabToMarry::~JabToMarry() = default;

/** @return  whether the conditions are met
 **/
bool
JabToMarry::conditions_met(Trick const& trick)
{
  return (   condition_unresolved_marriage()
          && !condition(Team::re)
          && !condition_startcard(trick)
          && condition_is_marriage_determination_trick(trick)
          && condition_can_jab(trick));
}

/** @return   card to play
 **/
Card
JabToMarry::card_to_play(Trick const& trick)
{
  auto const& hand = this->hand();

  if (!trick.startcard().istrump()) {
    auto const color = trick.startcard().color();
    if (hand.contains(color, Card::ace)) {
      auto const card = Card(color, Card::ace);
      rationale_.add(_("Heuristic::return::jab with %s", _(card)));
      return card;
    }

    if (hand.contains(color)) {
      rationale_.add(_("Heuristic::reject::cannot jab the color trick"));
      return {};
    }

    if (cards_information().color_runs(color) == 0) {
      rationale_.add(_("Heuristic::info::first color run"));
      if (!trick.islastcard()) {
        auto const card = jabbing_trump_for_first_color_run(trick);
        rationale_.add(_("Heuristic::return::jab with %s", _(card)));
        return card;
      }
    }
  }

  if (trick.islastcard()) {
    rationale_.add(_("Heuristic::condition::last card"));
    auto const card = jabbing_trump_for_last_player(trick);
    rationale_.add(_("Heuristic::return::jab with %s", _(card)));
    return card;
  }

  if (   trick.remainingcardno() == 2
      && trick.lastplayer().team() == Team::re) {
    rationale_.add(_("Heuristic::condition::second last card and marriage player behind"));
    auto const card = simple_jabbing_card(trick);
    rationale_.add(_("Heuristic::return::jab with %s", _(card)));
    return card;
  }

  {
    auto const card = highest_trump_in_game();
    if (card) {
      rationale_.add(_("Heuristic::return::play highest trump in game: %s", _(card)));
      return card;
    }
  }

  auto const card = good_jabbing_card(trick);
  if (card) {
    rationale_.add(_("Heuristic::return::jab trick: %s", _(card)));
    return card;
  }

  return {};
}

Card
JabToMarry::simple_jabbing_card(Trick const& trick)
{
  if (   hand().contains(Card::diamond_jack)
      && trick.isjabbed({hand(), Card::diamond_jack})) {
    return Card::diamond_jack;
  }
  auto const card = hand().next_higher_card(Card::diamond_jack);
  if (!card)
    return {};
  if (trick.isjabbed(card))
    return card;
  return hand().next_higher_card(trick.winnercard());
}

/** @return the highest queen to jab
 **/
Card
JabToMarry::good_jabbing_card(Trick const& trick)
{
  for (auto const card : {Card::spade_queen, Card::heart_queen, Card::diamond_queen}) {
    if (   hand().contains(card)
        && trick.isjabbed({hand(), card}))
      return card;
  }
  return {};
}

/** @return   card to jab as last player
 **/
Card
JabToMarry::jabbing_trump_for_last_player(Trick const& trick)
{
  for (auto const& card_ : {Card::diamond_ace, Card::diamond_ten, Card::diamond_king, Card::diamond_jack, Card::diamond_nine}) {
    auto const card = HandCard(hand(), card_);
    if (   !card.is_special()
        && hand().contains(card)
        && trick.isjabbed(card)) {
      return card;
    }
  }

  return hand().next_jabbing_card(trick.winnercard());
}

/** @return   card to jab as first color run (not last player)
 **/
Card
JabToMarry::jabbing_trump_for_first_color_run(Trick const& trick)
{
  if (   hand().contains(Card::diamond_jack)
      && trick.isjabbed({hand(), Card::diamond_jack})) {
    return Card::diamond_jack;
  }

  //return hand.text_jabbing_card(Card::diamond_jack);
  return {};
}

} // namespace Heuristics
