/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "marriage.h"

namespace Heuristics {

/** Jab with a good card to marry
 **/
class JabToMarry : public HeuristicMarriage {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
explicit JabToMarry(Ai const& ai);
JabToMarry(JabToMarry const&) = delete;
JabToMarry& operator=(JabToMarry const&) = delete;

~JabToMarry() override;

bool conditions_met(Trick const& trick) override;
Card card_to_play(Trick const& trick) override;

private:
Card simple_jabbing_card(Trick const& trick);
Card good_jabbing_card(Trick const& trick);
Card jabbing_trump_for_last_player(Trick const& trick);
Card jabbing_trump_for_first_color_run(Trick const& trick);
};
} // namespace Heuristics
