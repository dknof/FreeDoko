/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "pfund_in_marriage_determination_trick.h"
#include "../pfund/pfund_for_sure.h"

namespace Heuristics {

auto PfundInMarriageDeterminationTrick::is_valid(GameTypeGroup const game_type,
                                                 PlayerTypeGroup const player_group) -> bool
{
  return (   game_type == GameTypeGroup::marriage_undetermined
          && player_group == PlayerTypeGroup::re);
}


PfundInMarriageDeterminationTrick::PfundInMarriageDeterminationTrick(Ai const& ai) :
  HeuristicMarriage(ai, Aiconfig::Heuristic::pfund_in_marriage_determination_trick)
{ }


PfundInMarriageDeterminationTrick::~PfundInMarriageDeterminationTrick() = default;


auto PfundInMarriageDeterminationTrick::conditions_met(Trick const& trick) -> bool
{
  return (   condition_unresolved_marriage()
          && condition(Team::re)
          && !condition_startcard(trick)
          && condition_is_marriage_determination_trick(trick)
         );
}


auto PfundInMarriageDeterminationTrick::card_to_play(Trick const& trick) -> Card
{
  auto pfund_heuristic = PfundForSure(ai());
  pfund_heuristic.set_player(player());
  auto const card = pfund_heuristic.card_to_play(trick);
  if (!card) {
    rationale_.add(_("Heuristic::reject::no card found to pfund"));
    return {};
  }
  if (trick.isjabbed(card, hand())) {
    rationale_.add(_("Heuristic::reject::card %s jabs the trick", _(card)));
    return {};
  }
  rationale_.add(_("Heuristic::return::pfund %s", _(card)));
  return card;
}

} // namespace Heuristics
