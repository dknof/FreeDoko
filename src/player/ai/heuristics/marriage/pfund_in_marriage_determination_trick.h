/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "marriage.h"
#include "../pfund/pfund.h"

namespace Heuristics {

/** Pfund in a marriage determination trick
 **/
class PfundInMarriageDeterminationTrick : public HeuristicMarriage {
public:
static auto is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group) -> bool;

public:
explicit PfundInMarriageDeterminationTrick(Ai const& ai);
PfundInMarriageDeterminationTrick(PfundInMarriageDeterminationTrick const&) = delete;
PfundInMarriageDeterminationTrick& operator=(PfundInMarriageDeterminationTrick const&) = delete;

~PfundInMarriageDeterminationTrick() override;

auto conditions_met(Trick const& trick) -> bool override;
auto card_to_play(Trick const& trick)   -> Card override;
};
} // namespace Heuristics
