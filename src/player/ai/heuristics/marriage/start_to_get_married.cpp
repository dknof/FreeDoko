/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "start_to_get_married.h"

#include "../../../../party/rule.h"
#include "../../../../game/game.h"

namespace Heuristics {

auto StartToGetMarried::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (   game_type == GameTypeGroup::marriage_undetermined
          && player_group == PlayerTypeGroup::re);
}


StartToGetMarried::StartToGetMarried(Ai const& ai) :
  HeuristicMarriage(ai, Aiconfig::Heuristic::start_to_get_married)
{ }


StartToGetMarried::~StartToGetMarried() = default;


auto StartToGetMarried::conditions_met(Trick const& trick) -> bool
{
  return (   condition_unresolved_marriage()
          && condition(Team::re)
          && condition_startcard(trick));
}

auto StartToGetMarried::card_to_play(Trick const& trick) -> Card
{
  switch (game().marriage().selector()) {
  case MarriageSelector::silent:
  case MarriageSelector::team_set:
    return {};

  case MarriageSelector::first_foreign:
    return get_best_card();

  case MarriageSelector::first_trump:
    return get_best_trump();

  case MarriageSelector::first_club:
    return get_best_card(Card::club);
  case MarriageSelector::first_spade:
    return get_best_card(Card::spade);
  case MarriageSelector::first_heart:
    return get_best_card(Card::heart);

  case MarriageSelector::first_color:
    return get_best_color_card();
  }

  return {};
}

auto StartToGetMarried::get_best_card() -> Card
{
  auto const& hand = this->hand();
  {
    auto const card = Card::diamond_ace;
    if (   hand.contains(card)
        && !hand.has_swines()) {
      rationale_.add(_("Heuristic::return::pfund %s", _(card)));
      return card;
    }
  }
  if (game().swines().hyperswines_announced() && !hand.has_hyperswines()) {
    auto const card = get_best_trump();
    rationale_.add(_("Heuristic::return::pfund %s to marry the hyperswines", _(card)));
    return card;
  }
  if (game().swines().swines_announced()      && !hand.has_swines()) {
    auto const card = get_best_trump();
    rationale_.add(_("Heuristic::return::pfund %s to marry the swines", _(card)));
    return card;
  }

  {
    auto const card = get_best_color_card();
    if (card) {
      if (card.value() < 10) {
        auto const card = Card::diamond_ten; // cppcheck-suppress shadowVariable
        if (hand.contains(card)) {
          rationale_.add(_("Heuristic::return::have no rich color card, take %s", _(card)));
          return card;
        }
      }
      return card;
    }
  }
  rationale_.add(_("Heuristic::info::have found no good color card to pfund"));
  auto const card = get_best_trump();
  rationale_.add(_("Heuristic::return::pfund %s", _(card)));
  return card;
}

auto StartToGetMarried::get_best_trump() -> Card
{
  auto const& hand = this->hand();
  {
    auto const card = Card::diamond_ace;
    if (   hand.contains(card)
        && !hand.has_swines()) {
      rationale_.add(_("Heuristic::return::pfund %s", _(card)));
      return card;
    }
  }
  {
    auto const card = Card::diamond_ten;
    if (hand.contains(card)) {
      rationale_.add(_("Heuristic::return::pfund %s", _(card)));
      return card;
    }
  }
  {
    auto const card = Card::diamond_king;
    if (hand.contains(card)) {
      rationale_.add(_("Heuristic::return::pfund %s", _(card)));
      return card;
    }
  }
  auto const card = hand.lowest_trump();
  rationale_.add(_("Heuristic::return::lowest trump card: %s", _(card)));
  return card;
}

auto StartToGetMarried::get_best_color_card() -> Card
{
  if (auto const card = get_double_color_card(Card::ten); card)
    return card;
  if (auto const card = get_solo_color_card(Card::ten); card)
    return card;
  if (auto const card = get_color_card(Card::ten); card)
    return card;
  if (auto const card = get_color_ace_to_be_jabbed(); card)
    return card;
  if (auto const card = get_color_card(Card::king); card)
    return card;
  if (auto const card = get_color_card(Card::nine); card)
    return card;
  if (auto const card = get_color_card(Card::ace); card)
    return card;
  return {};
}

auto StartToGetMarried::get_double_color_card(Card::Value const value) -> Card
{
  auto const& hand = this->hand();
  for (auto const color : {Card::club, Card::spade, Card::heart}) {
    auto const card = Card(color, value);
    if (card.istrump(game()))
      continue;
    if (   hand.count(color) == 2
        && hand.count(card) == 2) {
      rationale_.add(_("Heuristic::return::pure double %s", _(card)));
      return card;
    }
  }
  rationale_.add(_("Heuristic::info::have no pure double color %s", _(value)));
  return {};
}

auto StartToGetMarried::get_solo_color_card(Card::Value const value) -> Card
{
  auto const& hand = this->hand();
  for (auto const color : {Card::club, Card::spade, Card::heart}) {
    auto const card = Card(color, value);
    if (card.istrump(game()))
      continue;
    if (   hand.count(color) == 1
        && hand.count(card) == 1) {
      rationale_.add(_("Heuristic::return::solo %s", _(card)));
      return card;
    }
  }
  rationale_.add(_("Heuristic::info::have no solo color %s", _(value)));
  return {};
}

auto StartToGetMarried::get_color_card(Card::Value const value) -> Card
{
  auto const& hand = this->hand();
  auto const& trick = this->game().tricks().current();
  auto best_color = Card::nocardcolor;
  unsigned best_count = 0;
  for (auto const color : {Card::club, Card::spade, Card::heart}) {
    auto const card = Card(color, value);
    if (card.istrump(game()))
      continue;
    if (!hand.contains(card))
      continue;
    if (!condition_can_be_jabbed_by_opposite_team(trick, card))
      continue;
    if (   cards_information().remaining_others(color) >= 3
        && !HandCard(hand, card).is_jabbed_by({color, cards_information().highest_remaining_card_of_others(color).value()}) )
      continue;
    auto const count = hand.count(color);
    if (best_count < count) {
      best_count = count;
      best_color = color;
    }
  }
  if (best_color != Card::nocardcolor) {
    auto const card = Card(best_color, value);
    rationale_.add(_("Heuristic::return::%s of the color with the most cards: %s", _(value), _(card)));
    return card;
  }
  rationale_.add(_("Heuristic::info::have found no color %s", _(value)));
  return {};
}


auto StartToGetMarried::get_color_ace_to_be_jabbed() -> Card
{
  auto const& hand = this->hand();
  auto best_color = Card::nocardcolor;
  unsigned best_count = 0;
  for (auto const color : {Card::club, Card::spade, Card::heart}) {
    auto const ace = Card(color, Card::ace);
    if (!hand.contains(ace))
      continue;
    if (!can_be_jabbed_by_others(color))
      continue;
    auto const count = hand.count(color);
    if (best_count < count) {
      best_count = count;
      best_color = color;
    }
  }
  if (best_color != Card::nocardcolor) {
    auto const card = Card(best_color, Card::ace);
    rationale_.add(_("Heuristic::return::color ace which can be jabbed: %s", _(card)));
    return card;
  }
  rationale_.add(_("Heuristic::info::have found no color ace that can be jabbed"));
  return {};
}

auto StartToGetMarried::get_best_card(Card::Color const color) -> Card
{
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();

  if (!hand.contains(color)) {
    rationale_.add(_("Heuristic::reject::have no %s", _(color)));
    return {};
  }

  if (can_be_jabbed_by_others(color)) {
    auto const card = hand.highest_card(color);
    rationale_.add(_("Heuristic::return::%s can be jabbed, take highest card %s", _(color), _(card)));
    return card;
  }

  auto const ace = Card(color, Card::ace);
  auto const ten = Card(color, Card::ten);
  if (   hand.contains(ten)
      && !ten.istrump(game())) {
    if (cards_information.remaining_others(ace)) {
      rationale_.add(_("Heuristic::return::take %s to be jabbed by a remaining ace", _(ten)));
      return ten;
    }
    rationale_.add(_("Heuristic::info::no other players has a %s", _(ace)));
  }
  auto const king = Card(color, Card::king);
  if (hand.contains(king)) {
    if (   cards_information.remaining_others(ace)
        || (cards_information.remaining_others(ten) && !ten.istrump(game()))) {
      rationale_.add(_("Heuristic::return::take %s to be jabbed by a remaining ace or ten", _(king)));
      return king;
    }
    rationale_.add(_("Heuristic::info::no other players has a %s or %s", _(ace), _(ten)));
  }

  auto const nine = Card(color, Card::nine);
  if (hand.contains(nine)) {
    if (   cards_information.remaining_others(ace)
        || (cards_information.remaining_others(ten) && !ten.istrump(game()))
        || cards_information.remaining_others(king)) {
      rationale_.add(_("Heuristic::return::take %s to be jabbed by a remaining ace, ten or king", _(nine)));
      return {};
    }
    rationale_.add(_("Heuristic::info::no other players has a %s or %s or %s", _(ace), _(ten), _(king)));
  }

  auto const card = hand.highest_card(color);
  rationale_.add(_("Heuristic::return::highest card of %s: %s", _(color), _(card)));
  return card;
}

} // namespace Heuristics
