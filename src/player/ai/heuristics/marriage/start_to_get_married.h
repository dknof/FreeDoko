/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "marriage.h"

namespace Heuristics {

/** Start with a good card to marry
 **/
class StartToGetMarried : public HeuristicMarriage {
  friend class StartToGetMarriedLastChance; 
public:
  static auto is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group) -> bool;

public:
  explicit StartToGetMarried(Ai const& ai);
  StartToGetMarried(StartToGetMarried const&) = delete;
  auto operator=(StartToGetMarried const&)    = delete;

  ~StartToGetMarried() override;

  auto conditions_met(Trick const& trick) -> bool override;
  auto card_to_play(Trick const& trick) -> Card override;

private:
  auto get_best_card()                          -> Card;
  auto get_best_trump()                         -> Card;
  auto get_best_color_card()                    -> Card;
  auto get_double_color_card(Card::Value value) -> Card;
  auto get_solo_color_card(Card::Value value)   -> Card;
  auto get_color_card(Card::Value value)        -> Card;
  auto get_color_ace_to_be_jabbed()             -> Card;
  auto get_best_card(Card::Color color)         -> Card;
  auto can_be_jabbed(Card::Color color)         -> bool;
};
} // namespace Heuristics
