/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "start_to_get_married_last_chance.h"
#include "start_to_get_married.h"

#include "../../../../party/rule.h"
#include "../../../../game/game.h"

namespace Heuristics {

auto StartToGetMarriedLastChance::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (   game_type == GameTypeGroup::marriage_undetermined
          && player_group == PlayerTypeGroup::re);
}


StartToGetMarriedLastChance::StartToGetMarriedLastChance(Ai const& ai) :
  HeuristicMarriage(ai, Aiconfig::Heuristic::start_to_get_married_last_chance)
{ }


StartToGetMarriedLastChance::~StartToGetMarriedLastChance() = default;


auto StartToGetMarriedLastChance::conditions_met(Trick const& trick) -> bool
{
  return (   condition_unresolved_marriage()
          && condition(Team::re)
          && condition_startcard(trick)
          && condition_last_trick_for_marriage_determination());
}


auto StartToGetMarriedLastChance::card_to_play(Trick const& trick) -> Card
{
  auto heuristic_logic = Heuristics::StartToGetMarried(ai());
  heuristic_logic.set_player(player());
  return heuristic_logic.card_to_play(trick);
}


auto StartToGetMarriedLastChance::condition_last_trick_for_marriage_determination() -> bool
{
  auto const& game = this->game();
  auto const remaining_tricks = rule()(Rule::Type::marriage_determination) - (game.tricks().current_no() + 1);
  if (remaining_tricks == 0) {
    rationale_.add(_("Heuristic::condition::last trick do determinate the marriage"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::%u more tricks do determinate the marriage", remaining_tricks));
  return false;
}

} // namespace Heuristics
