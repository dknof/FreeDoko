/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../../heuristic.h"

namespace Heuristics {

/** Base class for pfund heuristics
 **/
class Pfund : public Heuristic {
public:
explicit Pfund(Ai const& ai, Aiconfig::Heuristic heuristic);
Pfund(Pfund const&)            = delete;
Pfund& operator=(Pfund const&) = delete;

~Pfund() override;

auto condition_have_rich_pfund(Trick const& trick) -> bool;

auto high_pfund(Trick const& trick)           -> HandCard;
auto high_arbitrary_pfund(Trick const& trick) -> HandCard;
auto high_trump_pfund()                       -> HandCard;
auto high_pfund(Trick const& trick, Card::TColor color) -> HandCard;
auto small_pfund(Trick const& trick)          -> HandCard;
auto small_arbitrary_pfund()                  -> HandCard;
auto small_trump_pfund()                      -> HandCard;
auto small_pfund(Card::TColor color)          -> HandCard;

private:
auto keep_color_ace_for_second_color_run(Trick const& trick) -> bool;
};
} // namespace Heuristics
