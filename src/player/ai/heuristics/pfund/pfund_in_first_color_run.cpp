/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "pfund_in_first_color_run.h"

#include "../../ai.h"
#include "../../../../game/game.h"
#include "../../../../party/rule.h"


namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PfundInFirstColorRun::is_valid(GameTypeGroup const game_type,
                                    PlayerTypeGroup const player_group)
{
  return is_with_partner(game_type, player_group);
}

/** constructor
 **/
PfundInFirstColorRun::PfundInFirstColorRun(Ai const& ai) :
  Pfund(ai, Aiconfig::Heuristic::pfund_in_first_color_run)
{ }

/** destructor
 **/
PfundInFirstColorRun::~PfundInFirstColorRun() = default;

/** @return  whether the conditions are met
 **/
bool
PfundInFirstColorRun::conditions_met(Trick const& trick)
{
  if (!(   !condition_startcard(trick)
        && condition_first_color_run(trick)))
    return false;

  if (condition_winnerplayer_guessed_same_team(trick)) {
    return (condition_winnercard_ace_or_trump(trick));
  } else {
    return (   !condition_last_card(trick)
            && !condition_winnercard_ace_or_trump(trick)
            && !condition_startcard_is(trick, Card(trick.startcard().color(), Card::ten))
            && condition_guessed_own_team_can_jab(trick)
           );
  }
}

/** @return   card to play
 **/
Card
PfundInFirstColorRun::card_to_play(Trick const& trick)
{
  {
    auto const card = choose_best_fehl(trick);
    auto const& hand = this->hand();
    auto const& cards_information = ai().cards_information();
    if (   card
        && hand.count(card.color()) == 1
        && cards_information.color_runs(card.color()) == 0) {
      rationale_.add(_("Heuristic::return::create fehl: %s", _(card)));
      return card;
    }
  }

  {
    auto const card = high_pfund(trick);
    if (card) {
      rationale_.add(_("Heuristic::return::pfund: %s", _(card)));
      return card;
    }
  }
  {
    auto const card = small_pfund(trick);
    if (card) {
      rationale_.add(_("Heuristic::return::pfund: %s", _(card)));
      return card;
    }
  }
  return {};
}

/** @return   whether the winnercard is a color ace or a trump
 **/
bool
PfundInFirstColorRun::condition_winnercard_ace_or_trump(Trick const& trick)
{
  if (trick.isstartcard()) {
    rationale_.add(_("Heuristic::condition::empty trick"));
    return false;
  }
  auto const& startcard = trick.startcard();
  if (startcard.istrump()) {
    rationale_.add(_("Heuristic::condition::trump trick"));
    return true;
  }
  auto const color = startcard.color();
  auto const& winnercard = trick.winnercard();
  auto const& cards_information = ai().cards_information();
  if (winnercard.istrump()) {
    rationale_.add(_("Heuristic::condition::winnercard is trump"));
    return true;
  }
  if (   winnercard.value() == Card::ace
      && winnercard.color() == color) {
    if (cards_information.played(color) > trick.cards().count(color)) {
      rationale_.add(_("Heuristic::condition::%s played before", _(color)));
      return false;
    }
    if (cards_information.remaining_others(color) + 2 <= trick.remainingcardno()) {
      rationale_.add(_("Heuristic::condition::not enough remaining cards of %s", _(color)));
      return false;
    }
    rationale_.add(_("Heuristic::condition::winnercard is %s", _(winnercard)));
    return true;
  }
  if (   !trick.islastcard()
      && same_team(trick.nextplayer()) ) {
    rationale_.add(_("Heuristic::condition::the next player %s is of the same team", trick.nextplayer().name()));
    if (guessed_hand(trick.nextplayer()).contains(color, Card::ace)) {
      rationale_.add(_("Heuristic::condition::player %s can have card %s", trick.nextplayer().name(), _(Card(color, Card::ace))));
      return false;
    }
    if (!guessed_hand(trick.nextplayer()).contains(color)) {
      rationale_.add(_("Heuristic::condition::player %s has no color %s", trick.nextplayer().name(), _(color)));
      return true;
    }
  }
  rationale_.add(_("Heuristic::condition::winnercard is neither trump nor %s", _(Card(color, Card::ace))));
  return false;
}

/** @return   low fehl card with many cards of the color remaining
 **/
Card
PfundInFirstColorRun::choose_best_fehl(Trick const& trick)
{
  Card best_card;
  unsigned best_remaining = game().playerno() - 1;
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();

  for (auto const& card : hand.validcards(trick)) {
    if (card.istrump())
      continue;
    if (card.points() > 4)
      continue;
    auto const remaining
      = cards_information.remaining_others(card.color());
    if(   remaining > best_remaining
       && hand.count(card.color()) <= 1) {
      best_card = card;
      best_remaining = remaining;
    }
  }

  return best_card;
}

} // namespace Heuristics
