/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "play_highest_color.h"

#include "../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PlayHighestColor::is_valid(GameTypeGroup const game_type,
                                PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
PlayHighestColor::PlayHighestColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::play_highest_color_card_in_game)
{ }

/** destructor
 **/
PlayHighestColor::~PlayHighestColor() = default;

/** @return  whether the conditions are met
 **/
bool
PlayHighestColor::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && condition_pure_color_trick(trick));
}

/** @return   card to play
 **/
Card
PlayHighestColor::card_to_play(Trick const& trick)
{
  auto const& hand = this->hand();
  auto const color = trick.startcard().tcolor();
  if (!(   condition_has(color)))
    return {};
  auto const card = hand.highest_card(color);
  if (!condition_can_jab_with(trick, card))
    return {};
  if (condition_can_be_jabbed_by_opposite_team(trick, card))
    return {};

  if (card.value() == Card::ace) {
    auto const ten = Card(color, Card::ten);
    if (   hand.contains(ten)
        && condition_can_jab_with(trick, ten)
        && !condition_can_be_jabbed_by_opposite_team(trick, ten)
        && (   condition_opposite_team_can_have(card)
            || condition_opposite_team_can_have(ten))) {
      rationale_.add(_("Heuristic::condition::jab with %s instead of the ace", _(ten)));
      return ten;
    }
  }
  rationale_.add(_("Heuristic::condition::jab trick with %s", _(card)));

  return card;
}

} // namespace Heuristics
