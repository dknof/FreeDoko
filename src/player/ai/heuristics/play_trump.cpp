/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "play_trump.h"

#include "../ai.h"
#include "../../team_information.h"
#include "../../../party/rule.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PlayTrump::is_valid(GameTypeGroup const game_type,
                         PlayerTypeGroup const player_group)
{
  return (game_type != GameTypeGroup::solo_meatless);
}

/** constructor
 **/
PlayTrump::PlayTrump(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::play_trump)
{ }

/** destructor
 **/
PlayTrump::~PlayTrump() = default;

/** @return  whether the conditions are met
 **/
bool
PlayTrump::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && !condition_soloplayer_behind(trick)
         );
}

/** @return   card to play
 **/
Card
PlayTrump::card_to_play(Trick const& trick)
{
  auto const& game = trick.game();
  if (game.type() == GameType::poverty) {
    return get_card_in_poverty(trick);
  }

  auto const& hand = this->hand();
  auto const& team = player().team();
  // @heuristic::condition   only trumps
  if (hand.hascolor()) {
    rationale_.add(_("Heuristic::reject::still color on the hand"));
    return Card::empty;
  }

  // @heuristic::action      if the trick goes to the own team: play low card
  if (   trick.winnerteam() == team
      && game.cards().less(trump_card_limit(), trick.winnercard())) {
    auto const card = lowest_best_trump_card(trick);
    rationale_.add(_("Heuristic::return::own team has already jabbed high: take low trump %s", _(card)));
    return card;
  }

  auto const card = best_jabbing_card(trick, true);

  // @heuristic::action      if the trick has enough points: jab it
  if (trick.points()
      > ((1.2 * game.remaining_points())
         / game.tricks().remaining_no()) ) {
    rationale_.add(_("Heuristic::return::many points in the trick: jab with %s", _(card)));
    return card;
  }
  // @heuristic::action      if the player has enough high cards: jab it
  if (   (trick.winnerteam() != team)
      && (hand.higher_cards_no(trick.winnercard())
          + hand.count(trick.winnercard())
          > hand.cardsnumber() / 2)) {
    rationale_.add(_("Heuristic::return::many high cards in the hand: jab with %s", _(card)));
    return card;
  }

  // special case: two fox on the hand, not many trumps and partner behind: try fox
  auto const guessed_partner = team_information().guessed_partner();
  if (   !game.is_solo()
      && hand.count(Card::trump, Card::ace) == 2
      && !rule()(Rule::Type::swines)
      && hand.count(Card::trump) <= 4
      && guessed_partner
      && !trick.has_played(*guessed_partner)
      && (   trick.startcard().istrump()
          || !guessed_hand(*guessed_partner).contains(trick.startcard().tcolor()) )
     ) {
    auto const card = Card(game.cards().trumpcolor(), Card::ace); // cppcheck-suppress shadowVariable
    rationale_.add(_("Heuristic::return::two fox on the hand, not many trumps, partner behind: try to save %s", _(card)));
    return card;
  }

  // @heuristic::action      else: play low card
  if (   HandCard(hand, trump_card_limit()).is_jabbed_by(trick.winnercard())
      && trick.points() + 3 * trick.remainingcardno() < 16) { // *Value*
    // ToDo: if there is no card greater then a fox in the game, play such
    auto const card = lowest_best_trump_card(trick); // cppcheck-suppress shadowVariable
    rationale_.add(_("Heuristic::return::few points, play low trump %s", _(card)));
    return card;
  }

  if (game.cards().less(trump_card_limit(), card)) {
    rationale_.add(_("Heuristic::reject::low card to jab selected, just let it pass"));
    return Card::empty;
  }

  // ToDo: perhaps always return no card, because the player should have enough high trumps
  rationale_.add(_("Heuristic::return::just jab with %s", _(card)));
  return card;
}

/** @return  card to play or empty card for a poverty
 **/
Card
PlayTrump::get_card_in_poverty(Trick const& trick)
{
  if (team() == Team::re)
    return get_card_in_poverty_re(trick);
  else if (team() == Team::contra)
    return get_card_in_poverty_re(trick);
  else
    return {};
}

/** @return  card to play or empty card for a poverty (re player)
 **/
Card
PlayTrump::get_card_in_poverty_re(Trick const& trick)
{
  if (only_own_team_behind(trick)) {
    auto const card = best_jabbing_card(trick, true);
    rationale_.add(_("Heuristic::return::poverty re and no opponent behind: take best jabbing card %s", _(card)));
    return card;
  }
  auto const color = trick.startcard().tcolor();
  if (color_runs(color) == 0) {
    auto const card = best_jabbing_card(trick);
    rationale_.add(_("Heuristic::return::poverty re and first color run: take best winning card %s", _(card)));
    return card;
  }
  rationale_.add(_("Heuristic::reject::poverty, but opposite player behind and %s was already played", _(color)));
  return {};
}

/** @return  card to play or empty card for a poverty (contra player)
 **/
Card
PlayTrump::get_card_in_poverty_contra(Trick const& trick)
{
  auto const& game = trick.game();
  auto const& hand = this->hand();
  if (hand.contains(trick.startcard().tcolor())) {
    rationale_.add(_("Heuristic::reject::color in the hand"));
    return {};
  }
  Player const& re_player = game.players().poverty_partner();
  if (trick.has_played(re_player)) {
    rationale_.add(_("Heuristic::reject::re player has already played"));
    return {};
  }
  auto const trumpcolor = game.cards().trumpcolor();
  if (hand.contains(Card(trumpcolor, Card::ten)) > 0 ) {
    auto const card = Card(trumpcolor, Card::ten );
    rationale_.add(_("Heuristic::return::contra and first color run: take %s", _(card)));
    return card;
  }

  auto const card = lowest_best_trump_card(trick);
  rationale_.add(_("Heuristic::return::contra and first color run: take lowest best trump card %s", _(card)));
  return card;
}

} // namespace Heuristics
