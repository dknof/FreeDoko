/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "leave_to_partner.h"

#include "../../../ai.h"

namespace Heuristics {

auto PovertyContraLeaveToPartner::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::contra);
}


PovertyContraLeaveToPartner::PovertyContraLeaveToPartner(Ai const& ai) :
  HeuristicPoverty(ai, Aiconfig::Heuristic::poverty_leave_to_partner)
{ }


PovertyContraLeaveToPartner::~PovertyContraLeaveToPartner() = default;


bool
PovertyContraLeaveToPartner::conditions_met(Trick const& trick)
{
  return (   condition_poverty()
          && condition_trump_trick(trick)
          && condition(Team::contra)
          && condition_winnerplayer_re(trick)
          && condition_winnercard_trump(trick)
          && condition_partner_behind(trick)
          && condition_partner_guessed_can_jab(trick)
         );
}


auto PovertyContraLeaveToPartner::card_to_play(Trick const& trick) -> Card
{
  auto card = pfund_card(trick);

  if (!card) {
    rationale_.add(_("Heuristic::reject::no card found to pfund"));
    return {};
  }

  rationale_.add(_("Heuristic::return::pfund %s", _(card)));
  return card;
}


auto PovertyContraLeaveToPartner::condition_winnerplayer_re(Trick const& trick) -> bool
{
  auto const& winnerplayer = trick.winnerplayer();
  if (winnerplayer == poverty_partner()) {
    rationale_.add(_("Heuristic::condition::the winnerplayer is the re player"));
    return true;
  }
  if (winnerplayer == poverty_player()) {
    rationale_.add(_("Heuristic::condition::the winnerplayer is the poverty player"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::the winnerplayer is my partner"));
  return false;
}


auto PovertyContraLeaveToPartner::condition_partner_guessed_can_jab(Trick const& trick) -> bool
{
  auto const& partner = contra_partner();
  auto const& hand_of_partner = guessed_hand(partner);

  if (!trick.startcard().istrump()) {
    rationale_.add(_("Heuristic::info::color trick"));
    if (hand_of_partner.contains(trick.startcard().tcolor())) {
      rationale_.add(_("Heuristic::condition::partner guessed has to serve"));
      return false;
    }
    rationale_.add(_("Heuristic::info::partner does not have %s", _(trick.startcard().tcolor())));
  }

  if (!trick.isjabbed(hand_of_partner.highest_trump())) {
    rationale_.add(_("Heuristic::condition::partner guessed cannot jab"));
    return false;
  }
  // better: check over trumplimit_normal, whether I know, my player has a higher card (p.e. club queen, swine)
  if (!trick.isjabbed({hand(), trump_card_limit()})) {
    rationale_.add(_("Heuristic::condition::winnercard is not less then trump limit"));
    return false;
  }

  rationale_.add(_("Heuristic::condition::partner is supposed to be able to jab"));
  return true;
}


auto PovertyContraLeaveToPartner::pfund_card(Trick const& trick) -> Card
{
  auto const& hand = this->hand();
  auto const& cards = game().cards();
  vector<Card> cards_to_play;

  if (!hand.has_swines())
    cards_to_play.push_back(Card::diamond_ace);
  cards_to_play.push_back(Card::diamond_ten);
  cards_to_play.push_back(Card::diamond_king);
  cards_to_play.push_back(Card::diamond_nine);
  // ToDo: Hand function 'next greater card'
  cards_to_play.push_back(Card::diamond_jack);
  cards_to_play.push_back(Card::heart_jack);
  cards_to_play.push_back(Card::spade_jack);
  cards_to_play.push_back(Card::club_jack);
  cards_to_play.push_back(Card::diamond_queen);
  cards_to_play.push_back(Card::heart_queen);
  cards_to_play.push_back(Card::spade_queen);
  cards_to_play.push_back(Card::club_queen);

  for (auto c : cards_to_play) {
    if (!hand.contains(c))
      continue;
    if (c == trick.winnercard()) {
      if (hand.contains(cards.next_higher_card(c)))
        return cards.next_higher_card(c);
      else if (hand.contains(cards.next_higher_card(c, 2)))
        return cards.next_higher_card(c, 2);
    }
    if (HandCard(hand, c).is_special())
      continue;
    return c;
  }
  return {};
}

} // namespace Heuristics
