/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../poverty.h"

namespace Heuristics {

/** Overjab the re player
 **/
class PovertyContraLeaveToPartner : public HeuristicPoverty {
public:
  static auto is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group) -> bool;

public:
  explicit PovertyContraLeaveToPartner(Ai const& ai);
  PovertyContraLeaveToPartner(PovertyContraLeaveToPartner const&) = delete;
  auto operator=(PovertyContraLeaveToPartner const&)              = delete;

  ~PovertyContraLeaveToPartner() override;

  auto conditions_met(Trick const& trick) -> bool override;
  auto card_to_play(Trick const& trick)   -> Card override;

private:
  auto condition_winnerplayer_re(Trick const& trick)         -> bool;
  auto condition_partner_guessed_can_jab(Trick const& trick) -> bool;
  auto pfund_card(Trick const& trick) -> Card;
};
} // namespace Heuristics
