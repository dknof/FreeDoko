/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "overjab_re.h"

#include "../../../../../game/game.h"

namespace Heuristics {

auto PovertyContraOverjabRe::is_valid(GameTypeGroup const game_type,
                                      PlayerTypeGroup const player_group) -> bool
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::contra);
}


PovertyContraOverjabRe::PovertyContraOverjabRe(Ai const& ai) :
  HeuristicPoverty(ai, Aiconfig::Heuristic::poverty_overjab_re)
{ }


PovertyContraOverjabRe::~PovertyContraOverjabRe() = default;


auto PovertyContraOverjabRe::conditions_met(Trick const& trick) -> bool
{
  return (   condition_poverty()
          && condition(Team::contra)
          && condition_winnerplayer_re(trick)
          && condition_winnercard_trump(trick)
          && condition_can_jab(trick)
         );
}


auto PovertyContraOverjabRe::card_to_play(Trick const& trick) -> Card
{
  auto card = hand().next_jabbing_card(trick.winnercard());

  if (!card) {
    rationale_.add(_("Heuristic::reject::cannot jab"));
    return {};
  }

  auto const& hand = this->hand();
  if (   card.is_special()
      && (condition_partner_behind(trick) || condition_last_card(trick))
      && (static_cast<int>(trick.points())
          < max(10, (20
                     - 3 * static_cast<int>(hand.count(Card::dulle))
                     - 3 * static_cast<int>(count_swines_and_hyperswines_of_own_team())
                     - (trick.issecondlastcard() ? 10 : 0)
                    )))) {
    rationale_.add(_("Heuristic::reject::not enough points in the trick to jab with %s", _(card)));
    return {};
  }

  rationale_.add(_("Heuristic::return::jab with next higher card %s", _(card)));
  return card;
}


auto PovertyContraOverjabRe::condition_winnerplayer_re(Trick const& trick) -> bool
{
  if (trick.winnerplayer() == poverty_partner()) {
    rationale_.add(_("Heuristic::condition::the winnerplayer is the re player"));
    return true;
  }
  if (trick.winnerplayer() == poverty_player()) {
    rationale_.add(_("Heuristic::condition::the winnerplayer is the poverty player"));
    return false;
  }
  rationale_.add(_("Heuristic::condition::the winnerplayer is my partner"));
  return false;
}

} // namespace Heuristics
