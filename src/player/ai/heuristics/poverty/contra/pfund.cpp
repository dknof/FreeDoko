/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "pfund.h"

#include "../../../ai.h"
#include "../../../../../card/trick.h"

namespace Heuristics {

PovertyContraPfund::PovertyContraPfund(Ai const& ai, Aiconfig::Heuristic heuristic) :
  Heuristic(ai, heuristic)
{ }


PovertyContraPfund::~PovertyContraPfund() = default;


auto PovertyContraPfund::condition_have_rich_pfund(Trick const& trick) -> bool
{
  auto const cards = hand().validcards(trick);
  if (cards.count(Card::ten) || cards.count(Card::ace)) {
    rationale_.add(_("Heuristic::condition::have a ten or an ace to pfund"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::have neither ten or ace to pfund"));
  return false;
}


auto PovertyContraPfund::high_pfund(Trick const& trick) -> HandCard
{
  if (trick.isempty())
    return high_arbitrary_pfund();
  if (!hand().contains(trick.startcard().tcolor()))
    return high_arbitrary_pfund();
  return high_pfund(trick, trick.startcard().tcolor());
}


auto PovertyContraPfund::high_arbitrary_pfund() -> HandCard
{
  auto const& hand = this->hand();
  {
    auto const card = high_trump_pfund();
    if (card.isfox()) {
      if (hand.count(Card::trump) < 6) {
        rationale_.add(_("Heuristic::condition::few trumps and have fox to pfund"));
        return card;
      }
      if (condition_opposite_team_announcement()) {
        return card;
      }
      if (   game().type() == GameType::marriage
          && team() == Team::contra) {
        rationale_.add(_("Heuristic::condition::contra team in a marriage"));
        return card;
      }
    }
  }

  auto const& cards_information = ai().cards_information();
  auto const colors = game().cards().colors();

  for (auto const color : colors) {
    auto const ten = HandCard(hand, color, Card::ten);
    if (   hand.contains(ten)
        && !ten.istrump()
        && hand.count(color) == 1
        && cards_information.remaining_others(color, Card::ace)) {
      rationale_.add(_("Heuristic::condition::color ten where another player still has the ace"));
      return ten;
    }
  }

  for (auto const color : colors) {
    auto const ten = HandCard(hand, color, Card::ten);
    if (   hand.contains(ten)
        && !ten.istrump()
        && cards_information.remaining_others(color, Card::ace)) {
      rationale_.add(_("Heuristic::condition::color ten where another player still has the ace"));
      return ten;
    }
  }

  for (auto const color : colors) {
    auto const ace = HandCard(hand, color, Card::ace);
    if (   hand.contains(ace)
        && !ace.istrump()
        && hand.count(color) == 1
        && cards_information.remaining_others(color) == 0) {
      rationale_.add(_("Heuristic::condition::single color ace and noone else has the color"));
      return ace;
    }
  }

  for (auto const color : colors) {
    auto const ten = HandCard(hand, color, Card::ten);
    if (  hand.contains(ten)
        && !ten.istrump()
        && hand.count(color) == 1
        && cards_information.remaining_others(color) > 0) {
      rationale_.add(_("Heuristic::condition::single color ten and noone else has the color"));
      return ten;
    }
  }

  for (auto const color : colors) {
    auto const ace = HandCard(hand, color, Card::ace);
    auto const ten = HandCard(hand, color, Card::ten);
    if (   hand.count(color) >= 2
        && hand.count(color) == (  (ace.istrump() ? 0 : hand.count(ace))
                                 + (ten.istrump() ? 0 : hand.count(ten))) ) {
      if (!ace.istrump() && hand.contains(ace)) {
        if (!ten.istrump() && hand.contains(ten)) {
          rationale_.add(_("Heuristic::condition::only ace and ten in %s", _(color)));
        } else {
          rationale_.add(_("Heuristic::condition::only ace in %s", _(color)));
        }
        return ace;
      }
      if (!ten.istrump() && hand.contains(ten)) {
        rationale_.add(_("Heuristic::condition::only ten in %s", _(color)));
        return ten;
      }
    }
  }

  for (auto const color : colors) {
    auto const ace = HandCard(hand, color, Card::ace);
    if (   hand.contains(ace)
        && !ace.istrump()
        && cards_information.remaining_others(color) == 0) {
      rationale_.add(_("Heuristic::condition::color ace and noone else has the color"));
      return ace;
    }
  }

  for (auto const color : colors) {
    auto const ten = HandCard(hand, color, Card::ten);
    if (   hand.contains(ten)
        && !ten.istrump()
        && cards_information.remaining_others(color) == 0) {
      rationale_.add(_("Heuristic::condition::color ten and noone else has the color"));
      return ten;
    }
  }

  for (auto const color : colors) {
    auto const ace = HandCard(hand, color, Card::ace);
    if (   hand.contains(ace)
        && !ace.istrump()
        && hand.count(ace) == 2) {
      rationale_.add(_("Heuristic::condition::double color ace"));
      return ace;
    }
  }

  for (auto const color : colors) {
    auto const ten = HandCard(hand, color, Card::ten);
    if (   hand.contains(ten)
        && !ten.istrump()) {
      rationale_.add(_("Heuristic::condition::color ten"));
      return ten;
    }
  }

  for (auto const color : colors) {
    auto const ace = HandCard(hand, color, Card::ace);
    if (   hand.contains(ace)
        && !ace.istrump()) {
      rationale_.add(_("Heuristic::condition::color ace"));
      return ace;
    }
  }

  {
    auto const card = high_trump_pfund();
    if (card.value() == Card::ten) {
      rationale_.add(_("Heuristic::condition::trump ten"));
      return card;
    }
  }

  return {};
}


auto PovertyContraPfund::high_trump_pfund() -> HandCard
{
  auto const& hand = this->hand();
  auto const ace = HandCard(hand, game().cards().trumpcolor(), Card::ace);
  auto const ten = HandCard(hand, game().cards().trumpcolor(), Card::ten);
  if (   hand.contains(ace)
      && !ace.is_special()) {
    return ace;
  }
  if (   hand.contains(ten)
      && !ten.is_special()) {
    return ten;
  }
  return {};
}


auto PovertyContraPfund::high_pfund(Trick const& trick, Card::TColor const color) -> HandCard
{
  if (color == Card::trump)
    return high_trump_pfund();
  auto const& hand = this->hand();
  if (!hand.contains(color))
    return {};
  auto const ace = HandCard(hand, color, Card::ace);
  auto const ten = HandCard(hand, color, Card::ten);
  auto const& cards_information = ai().cards_information();
  if (hand.contains(ace)) {
    if (trick.winnercard().istrump())
      return ace;
    if (cards_information.color_runs(color) > 0)
      return ace;
    if (cards_information.played(ten))
      return ace;
  }
  if (   hand.contains(ten)
      && !HandCard(hand, ten).istrump()) {
    return ten;
  }
  return {};
}


auto PovertyContraPfund::small_pfund(Trick const& trick) -> HandCard
{
  if (trick.isempty())
    return small_arbitrary_pfund();
  if (!hand().contains(trick.startcard().tcolor()))
    return small_arbitrary_pfund();
  return small_pfund(trick.startcard().tcolor());
}


auto PovertyContraPfund::small_arbitrary_pfund() -> HandCard
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    for (auto const value : {Card::king, Card::queen, Card::jack}) {
      auto const card = HandCard(hand, color, value);
      if (   !card.istrump()
          && hand.contains(card)) {
        return card;
      }
    }
  }
  return small_trump_pfund();
}


auto PovertyContraPfund::small_trump_pfund() -> HandCard
{
  auto const& hand = this->hand();
  if (is_picture_solo_or_meatless(game().type())) {
    return hand.lowest_trump();
  }
  for (auto const& card : {HandCard(hand, game().cards().trumpcolor(), Card::king),
       HandCard(hand, Card::diamond, Card::jack),
       HandCard(hand, Card::heart, Card::jack),
       HandCard(hand, Card::spade, Card::jack),
       HandCard(hand, Card::diamond, Card::queen)}) {
    if (   hand.contains(card)
        && !card.is_special())
      return card;
  }
  return {};
}


auto PovertyContraPfund::small_pfund(Card::TColor const color) -> HandCard
{
  if (color == Card::trump)
    return small_trump_pfund();
  auto const& hand = this->hand();
  for (auto const value : {Card::king, Card::queen, Card::jack}) {
    auto const card = HandCard(hand, color, value);
    if (   !card.istrump()
        && hand.contains(card)) {
      return card;
    }
  }
  return {};
}

} // namespace Heuristics
