/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "pfund_before_partner.h"

#include "../../../ai.h"

#include "../../../../../card/trick.h"
#include "../../../../../party/rule.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PovertyContraPfundBeforePartner::is_valid(GameTypeGroup const game_type,
                                               PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::contra);
}

/** constructor
 **/
PovertyContraPfundBeforePartner::PovertyContraPfundBeforePartner(Ai const& ai) :
  PovertyContraPfund(ai, Aiconfig::Heuristic::pfund_before_partner)
{ }

/** destructor
 **/
PovertyContraPfundBeforePartner::~PovertyContraPfundBeforePartner() = default;

/** @return  whether the conditions are met
 **/
bool
PovertyContraPfundBeforePartner::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && !condition_last_card(trick)
          && condition_have_rich_pfund(trick)
          && condition_guessed_partner_behind_can_win_trick(trick));
}

/** @return   card to play
 **/
Card
PovertyContraPfundBeforePartner::card_to_play(Trick const& trick)
{
  {
    auto const pfund = high_pfund(trick);
    if (pfund) {
      rationale_.add(_("Heuristic::return::high pfund: %s", _(pfund)));
      return pfund;
    }
  }
  rationale_.add(_("Heuristic::return::no pfund found"));
  return {};
}

/** @return   whether a partner behind can win the trick
 **/
bool
PovertyContraPfundBeforePartner::condition_guessed_partner_behind_can_win_trick(Trick const& trick)
{
  auto const& game = this->game();
  auto const& hand = this->hand();

  if (   game.swines().hyperswines_announced()
      && !trick.has_played(game.swines().hyperswines_owner())
      && !guessed_hand(game.swines().hyperswines_owner()).can_jab(trick)
      && cards_information().remaining_others(game.cards().hyperswine())) {
    rationale_.add(_("Heuristic::condition::player %s with hyperswines is behind", game.swines().hyperswines_owner().name()));
    if (same_team(game.swines().hyperswines_owner())) {
      rationale_.add(_("Heuristic::condition::player %s with hyperswines is a partner", game.swines().hyperswines_owner().name()));
      return true;
    } else if (guessed_same_team(game.swines().hyperswines_owner())) {
      rationale_.add(_("Heuristic::condition::player %s with hyperswines is guessed to be a partner", game.swines().hyperswines_owner().name()));
      return true;
    } else {
      return false;
    }
  }
  if (   game.swines().swines_announced()
      && !trick.has_played(game.swines().swines_owner())
      && !guessed_hand(game.swines().swines_owner()).can_jab(trick)
      && cards_information().remaining_others(game.cards().swine())) {
    rationale_.add(_("Heuristic::condition::player %s with swines is behind", game.swines().swines_owner().name()));
    if (same_team(game.swines().swines_owner())) {
      rationale_.add(_("Heuristic::condition::player %s with swines is a partner", game.swines().swines_owner().name()));
      return true;
    } else if (guessed_same_team(game.swines().swines_owner())) {
      rationale_.add(_("Heuristic::condition::player %s with swines is guessed to be a partner", game.swines().swines_owner().name()));
      return true;
    } else {
      return false;
    }
  }
  if (   (game.type() == GameType::normal || game.type() == GameType::marriage)
      && team() == Team::re
      && cards_information().remaining_others(Card::club_queen)
      && (   trick.startcard().istrump()
          || cards_information().remaining_others(trick.startcard().tcolor()) == 0)
      && !(   rule()(Rule::Type::dullen)
           && cards_information().remaining_others(Card::dulle))) {
    if (   (trick.actcardno() >= 1 ? guessed_team(trick.player_of_card(0)) == Team::contra : true)
        && (trick.actcardno() >= 2 ? guessed_team(trick.player_of_card(1)) == Team::contra : true)) {
      rationale_.add(_("Heuristic::condition::partner can jab with the club queen"));
      return true;
    }
  }

  if (   trick.actcardno() == 2) {
    return (   condition_last_player_guessed_same_team(trick)
            && condition_last_player_can_win_trick(trick));
  }
  if (   trick.actcardno() == 1) {
    if (   same_team(trick.player_of_card(2))
        && same_team(trick.player_of_card(3))
        && condition_guessed_own_team_can_jab(trick)) {
      rationale_.add(_("Heuristic::condition::partners behind are supposed to be able to jab"));
      if (below_trump_card_limit(trick.winnercard())) {
        rationale_.add(_("Heuristic::condition::winnercard %s is below trump card limit %s", _(trick.winnercard()), _(trump_card_limit())));
        return true;
      }
      rationale_.add(_("Heuristic::condition::winnercard %s is not below trump card limit %s", _(trick.winnercard()), _(trump_card_limit())));
      if (hand.count(Card::trump)
          <= (1
              + (trick.winnercard() == Card::diamond_queen ? 3 : 0)
              + (trick.winnercard() == Card::heart_queen   ? 2 : 0)
              + (trick.winnercard() == Card::spade_queen   ? 1 : 0)
              + 2 * hand.count(Card::diamond_ten)
              + 2 * hand.count(Card::diamond_ace)) ) {
        rationale_.add(_("Heuristic::condition::only few trumps"));
        return true;
      }
      return false;
    }
    return false;
  }

  return false;
}

/** @return   whether a partner behind can win the trick
 **/
bool
PovertyContraPfundBeforePartner::condition_last_player_guessed_same_team(Trick const& trick)
{
  auto const& lastplayer = trick.lastplayer();
  if (same_team(lastplayer)) {
    rationale_.add(_("Heuristic::condition::last player %s is of the same team", lastplayer.name()));
    return true;
  } else if (opposite_team(lastplayer)) {
    rationale_.add(_("Heuristic::condition::last player %s is of the opposite team", lastplayer.name()));
    return false;
  } else if (guessed_same_team(lastplayer)) {
    rationale_.add(_("Heuristic::condition::last player %s is guessed of the same team", lastplayer.name()));
    return true;
  } else if (guessed_opposite_team(lastplayer)) {
    rationale_.add(_("Heuristic::condition::last player %s is guessed of the opposite team", lastplayer.name()));
    return false;
  }
  rationale_.add(_("Heuristic::condition::the team of the last player %s is unknown", lastplayer.name()));
  return false;
}

/** @return   whether the last player can win the trick
 **/
bool
PovertyContraPfundBeforePartner::condition_last_player_can_win_trick(Trick const& trick)
{
  auto const& player = trick.lastplayer();
  auto const hand = cards_information().estimated_hand(player);
  if (!hand.can_jab(trick)) {
    rationale_.add(_("Heuristic::condition::last player %s cannot jab the trick", _(player.name())));
    return false;
  }
  if (!trick.startcard().istrump()) {
    auto const color = trick.startcard().tcolor();
    if (hand.contains(color)) {
      if (trick.winnercard() < hand.lowest_card(color)) {
        rationale_.add(_("Heuristic::condition::last player %s can jab the color %s", _(player.name()), _(color)));
        return true;
      } else {
        rationale_.add(_("Heuristic::condition::last player %s must serve the color %s", _(player.name()), _(color)));
        return false;
      }
    }
  }
  rationale_.add(_("Heuristic::condition::last player %s is supposed to be able to jab the trick", _(player.name())));
  if (is_solo(game().type())) {
    rationale_.add(_("Heuristic::condition::solo, so try to pfund"));
    return true;
  } else if (below_trump_card_limit(trick.winnercard())) {
    rationale_.add(_("Heuristic::condition::winnercard %s is below trump card limit", _(trick.winnercard())));
    return true;
  }
  if (hand.count(Card::ten) + hand.count(Card::ace) >= max(2u, (static_cast<unsigned>(hand.cardsnumber()) * 2 + 1) / 3)) {
    rationale_.add(_("Heuristic::condition::have many points, so try a pfund"));
    return true;
  }
  if (hand.count(Card::diamond_ten)
      + hand.count(Card::diamond_ace)
      >= max(2u, static_cast<unsigned>(hand.count(Card::trump)) * 2 / 3)) {
    rationale_.add(_("Heuristic::condition::have many points, so try a pfund"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::winnercard %s is not below trump card limit", _(trick.winnercard())));
  return false;
}

} // namespace Heuristics
