/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "pfund.h"

namespace Heuristics {

/** Pfund high in a trick that possibly goes to the own team
 **/
class PovertyContraPfundForUnsure : public PovertyContraPfund {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
explicit PovertyContraPfundForUnsure(Ai const& ai);
PovertyContraPfundForUnsure(PovertyContraPfundForUnsure const&)            = delete;
PovertyContraPfundForUnsure& operator=(PovertyContraPfundForUnsure const&) = delete;

~PovertyContraPfundForUnsure() override;

bool conditions_met(Trick const& trick) override;
Card card_to_play(Trick const& trick) override;

private:
bool condition_have_many_points();
};
} // namespace Heuristics
