/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "start_with_color.h"

#include "../../../../../game/game.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PovertyContraStartWithColor::is_valid(GameTypeGroup const game_type,
                                           PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::contra);
}

/** constructor
 **/
PovertyContraStartWithColor::PovertyContraStartWithColor(Ai const& ai) :
  HeuristicPoverty(ai, Aiconfig::Heuristic::poverty_contra_start_with_color)
{ }

/** destructor
 **/
PovertyContraStartWithColor::~PovertyContraStartWithColor() = default;

/** @return  whether the conditions are met
 **/
bool
PovertyContraStartWithColor::conditions_met(Trick const& trick)
{
  return (   condition_poverty()
          && condition(Team::contra)
          && condition_startcard(trick)
          && condition_has_color()
         );
}

/** @return   card to play
 **/
Card
PovertyContraStartWithColor::card_to_play(Trick const& trick)
{
  auto const& partner = contra_partner();
  auto const& re_player = poverty_partner();

  if (trick.playes_before(re_player, partner)) {
    rationale_.add(_("Heuristic::info::re player %s playes before partner %s", re_player.name(), partner.name()));
    auto const& card = highest_color_card_the_partner_can_jab();
    if (card) {
      rationale_.add(_("Heuristic::return::take highest card %s", _(card)));
      return card;
    }
  }

  {
    auto const& card = color_ace();
    if (card) {
      rationale_.add(_("Heuristic::return::take color ace %s", _(card)));
      return card;
    }
  }
  if (trick.playes_before(re_player, partner)) {
    auto const& card = lowest_color_card_of_partners_smallest_color();
    if (card) {
      rationale_.add(_("Heuristic::return::take lowest card %s", _(card)));
      return card;
    }
  }
  {
    auto const& card = lowest_color_card();
    if (card) {
      rationale_.add(_("Heuristic::return::take lowest card %s", _(card)));
      return card;
    }
  }
  rationale_.add(_("Heuristic::reject::no color card found"));
  return {};
}

/** @return   the highest card the partner does not have
 **/
Card
PovertyContraStartWithColor::highest_color_card_the_partner_can_jab()
{
  Card card;
  auto const& hand = this->hand();
  auto const& hand_of_partner = guessed_hand(contra_partner());
  for (auto const color: game().cards().colors()) {
    if (!hand.contains(color))
      continue;
    if (hand_of_partner.contains(color))
      continue;
    rationale_.add(_("Heuristic::info::the partner does not have %s", _(color)));
    auto const highest_card = hand.highest_card(color);
    if (!card) {
      rationale_.add(_("Heuristic::info::select highest card %s", _(highest_card)));
      card = highest_card;
      continue;
    }
    if (highest_card.value() > card.value()) {
      rationale_.add(_("Heuristic::info::prefer %s to %s (more points)", _(highest_card), _(card)));
      card = highest_card;
      continue;
    }
  }

  return card;
}

/** @return   the lowest card
 **/
Card
PovertyContraStartWithColor::color_ace()
{
  Card card;
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();
  for (auto const color: game().cards().colors()) {
    auto const ace = Card(color, Card::ace);
    if (!hand.contains(ace))
      continue;
    if (!card) {
      rationale_.add(_("Heuristic::info::select ace %s", _(ace)));
      card = ace;
      continue;
    }
    if (cards_information.remaining_others(color) > cards_information.remaining_others(card.color())) {
      rationale_.add(_("Heuristic::info::prefer %s to %s (more remaining cards)", _(ace), _(card)));
      card = ace;
      continue;
    }
  }

  return card;
}

/** @return   the lowest card
 **/
Card
PovertyContraStartWithColor::lowest_color_card_of_partners_smallest_color()
{
  Card card;
  auto const& hand = this->hand();
  auto const& hand_of_partner = guessed_hand(contra_partner());
  for (auto const color: game().cards().colors()) {
    if (!hand.contains(color))
      continue;
    auto const lowest_card = hand.lowest_card(color);
    if (!card) {
      rationale_.add(_("Heuristic::info::select lowest card %s", _(lowest_card)));
      card = lowest_card;
      continue;
    }
    if (hand_of_partner.count(color) < hand_of_partner.count(card.color())) {
      rationale_.add(_("Heuristic::info::prefer %s to %s (the partner is supposed to have fewer cards of)", _(lowest_card), _(card)));
      card = lowest_card;
      continue;
    }
    if (   hand_of_partner.count(color) == hand_of_partner.count(card.color())
        && lowest_card.value() < card.value()) {
      rationale_.add(_("Heuristic::info::prefer %s to %s (less points)", _(lowest_card), _(card)));
      card = lowest_card;
      continue;
    }
  }

  return card;
}

/** @return   the lowest card
 **/
Card
PovertyContraStartWithColor::lowest_color_card()
{
  Card card;
  auto const& hand = this->hand();
  auto const& hand_of_partner = guessed_hand(contra_partner());
  for (auto const color: game().cards().colors()) {
    if (!hand.contains(color))
      continue;
    auto const lowest_card = hand.lowest_card(color);
    if (!card) {
      rationale_.add(_("Heuristic::info::select lowest card %s", _(lowest_card)));
      card = lowest_card;
      continue;
    }
    if (lowest_card.value() < card.value()) {
      rationale_.add(_("Heuristic::info::prefer %s to %s (less points)", _(lowest_card), _(card)));
      card = lowest_card;
      continue;
    }
    if (   lowest_card.value() == card.value()
        && hand_of_partner.count(color) < hand_of_partner.count(card.color())) {
      rationale_.add(_("Heuristic::info::prefer %s to %s (the partner is supposed to have fewer cards of)", _(lowest_card), _(card)));
      card = lowest_card;
      continue;
    }
  }

  return card;
}

} // namespace Heuristics
