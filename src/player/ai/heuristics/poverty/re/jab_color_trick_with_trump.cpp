/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_color_trick_with_trump.h"

#include "../../../../../card/hand.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PovertyReJabColorTrickWithTrump::is_valid(GameTypeGroup const game_type,
                                               PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::re);
}

/** constructor
 **/
PovertyReJabColorTrickWithTrump::PovertyReJabColorTrickWithTrump(Ai const& ai) :
  HeuristicPoverty(ai, Aiconfig::Heuristic::poverty_re_jab_color_trick_with_trump)
{ }

/** destructor
 **/
PovertyReJabColorTrickWithTrump::~PovertyReJabColorTrickWithTrump() = default;

/** @return  whether the conditions are met
 **/
bool
PovertyReJabColorTrickWithTrump::conditions_met(Trick const& trick)
{
  return (   condition_poverty_partner()
          && condition_color_trick(trick)
          && !condition_must_serve(trick)
          && !condition_first_color_run(trick)
          && condition_opposite_or_unknown_player_behind(trick)
         );
}

/** @return   card to play
 **/
Card
PovertyReJabColorTrickWithTrump::card_to_play(Trick const& trick)
{
  auto const card = card_to_jab(trick);

  if (!card) {
    rationale_.add(_("Heuristic::reject::cannot jab with a jack or queen below club queen"));
    return {};
  }

  rationale_.add(_("Heuristic::return::jab with %s", _(card)));
  return card;
}

/** @return   high trump to jab the trick with
 **/
Card
PovertyReJabColorTrickWithTrump::card_to_jab(Trick const& trick)
{
  auto const& hand = this->hand();
  static auto const cards_to_play_big_trick = {
    Card::spade_queen,
    Card::heart_queen,
    Card::diamond_queen,
    Card::club_jack,
    Card::spade_jack,
    Card::heart_jack,
    Card::diamond_jack
  };
  static auto const cards_to_play_small_trick = {
    Card::heart_queen,
    Card::diamond_queen,
    Card::club_jack,
    Card::spade_jack,
    Card::spade_queen,
    Card::heart_jack,
    Card::diamond_jack,
  };

  for (auto const card : (trick.points() >= 10 ? cards_to_play_big_trick : cards_to_play_small_trick)) {
    if (   hand.contains(card)
        && trick.isjabbed({hand, card})) {
      return card;
    }
  }

  return Card::empty;
}

} // namespace Heuristics
