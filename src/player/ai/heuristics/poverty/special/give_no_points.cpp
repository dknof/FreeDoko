/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "give_no_points.h"

#include "../../../../../card/trick.h"
#include "../../../../../game/game.h"
#include "../../../../../game/players.h"
#include "../../../../../party/rule.h"

#include "../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PovertySpecialGiveNoPoints::is_valid(GameTypeGroup const game_type,
                                          PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::special);
}

/** constructor
 **/
PovertySpecialGiveNoPoints::PovertySpecialGiveNoPoints(Ai const& ai) :
  HeuristicPoverty(ai, Aiconfig::Heuristic::poverty_special_give_no_points)
{ }

/** destructor
 **/
PovertySpecialGiveNoPoints::~PovertySpecialGiveNoPoints() = default;

/** @return  whether the conditions are met
 **/
bool
PovertySpecialGiveNoPoints::conditions_met(Trick const& trick)
{
  return (   condition_poverty_player()
          && !condition_startcard(trick)
         );
}

/** @return   card to play
 **/
Card
PovertySpecialGiveNoPoints::card_to_play(Trick const& trick)
{
  if (condition_winnerplayer_same_team(trick)) {
    if (trick.islastcard()) {
      rationale_.add(_("Heuristic::reject::the partner gets the trick"));
      return {};
    }
    if (trick.startcard().istrump()) {
      if (!trick.isjabbed({hand(), value(Aiconfig::Type::trumplimit_normal)})) {
        rationale_.add(_("Heuristic::reject::the partner has played a high trump"));
        return {};
      }
    } else {
      auto const cards_information = this->cards_information();
      if (cards_information.color_runs(trick.startcard().tcolor()) == 0) {
        if (   trick.winnercard().value() == Card::ace
            && cards_information.remaining_others(trick.startcard().tcolor()) >= trick.remainingcardno() - 1) {
          rationale_.add(_("Heuristic::reject::the partner gets the trick with a color ace"));
          return {};
        }
        if (trick.winnercard().istrump()) {
          rationale_.add(_("Heuristic::reject::the partner has jabbed the trick with a trump"));
          return {};
        }
      }
    }
  } else {
    if (condition_can_jab(trick)) {
      return {};
    }
  }

  auto card = low_card(trick);
  rationale_.add(_("Heuristic::return::throw %s", _(card)));
  return card;
}

/** @return   card give to the opposite team
 **/
Card
PovertySpecialGiveNoPoints::low_card(Trick const& trick)
{
  auto const& hand = this->hand();
  auto const cards = hand.validcards(trick);

  Card best_card = cards[0];
  for (auto const& card : cards) {
    if (card.value() < best_card.value()) {
      rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(card), _(best_card)));
      best_card = card;
    }
  }

  return best_card;
}

} // namespace Heuristics
