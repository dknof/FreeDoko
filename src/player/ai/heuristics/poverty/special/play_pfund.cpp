/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "play_pfund.h"

#include "../../../../../card/trick.h"
#include "../../../../../game/game.h"
#include "../../../../../game/players.h"
#include "../../../../../party/rule.h"

#include "../../../ai.h"

namespace Heuristics {

auto PovertySpecialPlayPfund::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::special);
}


PovertySpecialPlayPfund::PovertySpecialPlayPfund(Ai const& ai) :
  HeuristicPoverty(ai, Aiconfig::Heuristic::poverty_special_play_pfund)
{ }


PovertySpecialPlayPfund::~PovertySpecialPlayPfund() = default;


auto PovertySpecialPlayPfund::conditions_met(Trick const& trick) -> bool
{
  return (   condition_poverty_player()
          && !condition_startcard(trick)
          && (    condition_partner_jabbed_with_high_trump(trick)
              || (   condition_winnerplayer_same_team(trick)
                  && !condition_opposite_or_unknown_team_can_jab(trick))
              || (   condition_last_player_same_team(trick)
                  && condition_guessed_own_team_can_jab(trick))
             ));
}


auto PovertySpecialPlayPfund::card_to_play(Trick const& trick) -> Card
{
  auto const card = pfund_card(trick);

  if (!card) {
    rationale_.add(_("Heuristic::reject::no card found to pfund"));
    return {};
  }
  rationale_.add(_("Heuristic::return::pfund %s", _(card)));
  return card;
}


auto PovertySpecialPlayPfund::pfund_card(Trick const& trick) -> Card
{
  auto const& hand = this->hand();
  auto const cards = hand.validcards(trick);
  auto const cards_information = this->cards_information();

  Card best_card;

  for (auto const& card : cards) {
    if (card.istrump() && card.is_special())
      continue;
    if (!best_card) {
      best_card = card;
      rationale_.add(_("Heuristic::condition::check card %s", _(best_card)));
    }
    if (card.value() < best_card.value())
      continue;
    if (   !card.istrump()
        && card.value() == Card::ace
        && cards_information.color_runs(card.color()) == 0
        && cards_information.remaining_others(card.color()) >= 2
        && guessed_hand(partner()).contains(card.color())
        && hand.count(card) == 1)
      continue;
    rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(card), _(best_card)));
    best_card = card;
  }

  return best_card;
}

} // namespace Heuristics
