/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "start_with_trump.h"

#include "../../../../../party/rule.h"
#include "../../../../../card/hand.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PovertySpecialStartWithTrump::is_valid(GameTypeGroup const game_type,
                                            PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::poverty
          && player_group == PlayerTypeGroup::special);
}

/** constructor
 **/
PovertySpecialStartWithTrump::PovertySpecialStartWithTrump(Ai const& ai) :
  HeuristicPoverty(ai, Aiconfig::Heuristic::poverty_special_start_with_trump)
{ }

/** destructor
 **/
PovertySpecialStartWithTrump::~PovertySpecialStartWithTrump() = default;

/** @return  whether the conditions are met
 **/
bool
PovertySpecialStartWithTrump::conditions_met(Trick const& trick)
{
  return (   condition_poverty_player()
          && condition_startcard(trick)
          && condition_has_trump()
         );
}

/** @return   card to play
 **/
Card
PovertySpecialStartWithTrump::card_to_play(Trick const& trick)
{
  auto const card = trump_to_start_with();

  if (!card) {
    rationale_.add(_("Heuristic::reject::no trump found to start with"));
    return {};
  }

  rationale_.add(_("Heuristic::return::start with %s", _(card)));
  return card;
}

/** @return   trump to start the trick with
 **/
Card
PovertySpecialStartWithTrump::trump_to_start_with()
{
  auto const& hand = this->hand();

  static auto const cards_to_play = {
    Card::diamond_ace,
    Card::diamond_ten,
    Card::diamond_king,
    Card::club_queen,
    Card::spade_queen,
  };

  for (auto const card : cards_to_play)
    if (hand.contains(card))
      return card;
  if (   rule()(Rule::Type::dullen)
      && !rule()(Rule::Type::dullen_second_over_first)) {
    if (hand.contains(Card::dulle))
      return Card::dulle;
  }
  return hand.lowest_trump();
}


} // namespace Heuristics
