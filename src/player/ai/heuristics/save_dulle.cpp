/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "save_dulle.h"

#include "../ai.h"
#include "../../../party/rule.h"

namespace Heuristics {

auto SaveDulle::is_valid(GameTypeGroup const game_type,
                         PlayerTypeGroup const player_group) -> bool
{
  return is_with_trump_color(game_type);
}


SaveDulle::SaveDulle(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::save_dulle)
{ }


SaveDulle::~SaveDulle() = default;


auto SaveDulle::conditions_met(Trick const& trick) -> bool
{
  return (   condition_has_dulle()
          && condition_opposite_dulle()
          && !condition_partner_swine_or_hyperswine()
          && condition_is_valid(trick, Card::dulle)
          && condition_have_few_trumps()
          && (   (   !rule()(Rule::Type::dullen_second_over_first)
                  && condition_startcard(trick))
              || condition_can_jab_with(trick, Card::dulle)
              || condition_winnerplayer_guessed_same_team(trick))
          && !condition_can_be_jabbed_by_opposite_team(trick, Card::dulle)
         );
}


auto SaveDulle::card_to_play(Trick const& trick) -> Card
{
  if (   trick.isempty()
      && !rule()(Rule::Type::dullen_second_over_first)) {
    rationale_.add(_("Heuristic::return::save the dulle"));
    return Card::dulle;
  }
  auto const& hand = this->hand();
  auto const other_trump = hand.lowest_trump();
  if (   trick.islastcard()
      && trick.isjabbed(other_trump)) {
    if (!rule()(Rule::Type::dullen_second_over_first)) {
      rationale_.add(_("Heuristic::return::jab with %s and start the next trick with the dulle", _(other_trump)));
      return other_trump;
    }
    if (   game().is_solo()
        && team() != Team::re
        && hand.count(Card::trump) == 3
        && hand.cardsnumber() > 3) {
      rationale_.add(_("Heuristic::return::jab with %s and start the next trick with a color card", _(other_trump)));
      return other_trump;
    }
  }
  rationale_.add(_("Heuristic::return::save the dulle"));
  return Card::dulle;
}


auto SaveDulle::condition_opposite_dulle() -> bool
{
  for (Player const& player: game().players()) {
    if (guessed_same_team(player))
      continue;
    if (!guessed_hand(player).contains(Card::dulle))
      continue;
    rationale_.add(_("Heuristic::condition::%s can have a dulle", player.name()));
    return true;
  }
  rationale_.add(_("Heuristic::condition::the other opposite players have no remaining dulle"));
  return false;
}


auto SaveDulle::condition_partner_swine_or_hyperswine() -> bool
{
  if (!rule()(Rule::Type::swines)) {
    rationale_.add(_("Heuristic::condition::no rule swine"));
    return false;
  }
  if (count_swines_and_hyperswines_of_own_team() > 0) {
    rationale_.add(_("Heuristic::condition::the own team has swines or hyperswines"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::the own team has no remaining swine or hyperswine"));
  return false;
}


auto SaveDulle::condition_have_few_trumps() -> bool
{
  unsigned const max_trumps = (game().is_solo()                               ? 2 
                               : rule()(Rule::Type::dullen_second_over_first) ? 3 : 2);
  if (hand().count(Card::trump) <= max_trumps) {
    rationale_.add(_("Heuristic::condition::have few trumps"));
    return true;
  }
  rationale_.add(_("Heuristic::condition::have still enough trumps"));
  return false;
}

} // namespace Heuristics
