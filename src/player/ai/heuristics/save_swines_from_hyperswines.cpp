/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "save_swines_from_hyperswines.h"

#include "../ai.h"

namespace Heuristics {

auto SaveSwinesFromHyperswines::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return is_with_trump_color(game_type);
}


SaveSwinesFromHyperswines::SaveSwinesFromHyperswines(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::save_swines_from_hyperswines)
{ }


SaveSwinesFromHyperswines::~SaveSwinesFromHyperswines() = default;


auto SaveSwinesFromHyperswines::conditions_met(Trick const& trick) -> bool
{
  auto const swine = game().cards().swine();
  return (   !condition_startcard(trick)
          && condition_has_swine()
          && condition_is_valid(trick, swine)
          && condition_opposite_hyperswine()
          && (   condition_can_jab_with(trick, swine)
              || condition_winnerplayer_guessed_same_team(trick))
          && !condition_can_be_jabbed_by_opposite_team(trick, swine)
         );
}


auto SaveSwinesFromHyperswines::card_to_play(Trick const& trick) -> Card
{
  auto const& game = trick.game();
  auto const& swines = game.swines();
  auto const& hand = this->hand();
  auto const trumpno = hand.count(Card::trump);
  auto const& cards_information = ai().cards_information();
  auto const& player_following = game.players().following(player());
  auto const& player_previous  = game.players().previous(player());
  int n = 1; // Wie viele Trumpf ich mindestens benötige, um die Dulle noch sicher nach Hause zu bekommen.
  n -= hand.count_color_cards() / 3;
  if (trick.startcard().istrump()) {
    rationale_.add(_("Heuristic::condition::trump trick"));
    n += 1;
  } else if (!hand.hascolor()) {
    rationale_.add(_("Heuristic::condition::have only trump"));
    n += 1;
  } else if (hand.can_jab(trick)) {
    rationale_.add(_("Heuristic::condition::can jab the trick"));
    n += 1;
  }
  if (condition_opposite_hyperswine()) {
    n += cards_information.remaining_others(game.cards().hyperswine());
  }
  if (   condition_opposite_hyperswine()
      && player_following == swines.hyperswines_owner()) {
    rationale_.add(_("Heuristic::condition::the following player has a hyperswine"));
    n += 1;
  } else if (   condition_opposite_hyperswine()
             && player_previous == swines.hyperswines_owner()) {
    rationale_.add(_("Heuristic::condition::the previous player has a hyperswine"));
    n -= 1;
  }
  if (trick.points() >= 15 && condition_opposite_or_unknown_player_behind(trick)) {
    rationale_.add(_("Heuristic::condition::at least 10 points in the trick"));
    n += 1;
  }
  if (static_cast<int>(trumpno - 1.5 * hand.count_rich_trumps()) < n) {
    rationale_.add(_("Heuristic::return::too few trumps, jab the trick with a swine"));
    return game.cards().swine();
  }

  rationale_.add(_("Heuristic::reject::still enough trumps on the hand"));
  return {};
}

} // namespace Heuristics
