/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "serve_color_trick.h"

#include "../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool ServeColorTrick::is_valid(GameTypeGroup const game_type,
                               PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
ServeColorTrick::ServeColorTrick(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::serve_color_trick)
{ }

/** destructor
 **/
ServeColorTrick::~ServeColorTrick() = default;

/** @return  whether the conditions are met
 **/
bool
ServeColorTrick::conditions_met(Trick const& trick)
{
  return (   condition_color_trick(trick)
          && condition_has(trick.startcard().tcolor())
          && !condition_can_jab(trick)
          && (   !condition_winnerplayer_guessed_same_team(trick)
              || condition_opposite_or_unknown_team_can_jab(trick))
          && !condition_guessed_own_team_can_jab(trick)
         );
}

/** @return   card to play
 **/
Card
ServeColorTrick::card_to_play(Trick const& trick)
{
  auto const color = trick.startcard().tcolor();
  auto const card = hand().lowest_card(color);
  rationale_.add(_("Heuristic::return::lowest color card: %s", _(card)));
  return card;
}

} // namespace Heuristics
