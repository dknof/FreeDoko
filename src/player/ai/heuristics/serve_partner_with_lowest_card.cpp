/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "serve_partner_with_lowest_card.h"

#include "../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool ServePartnerWithLowestCard::is_valid(GameTypeGroup const game_type,
                                          PlayerTypeGroup const player_group)
{
  return is_with_partner(game_type, player_group);
}

/** constructor
 **/
ServePartnerWithLowestCard::ServePartnerWithLowestCard(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::serve_partner_with_lowest_card)
{ }

/** destructor
 **/
ServePartnerWithLowestCard::~ServePartnerWithLowestCard() = default;

/** @return  whether the conditions are met
 **/
bool
ServePartnerWithLowestCard::conditions_met(Trick const& trick)
{
  if (!condition_winnerplayer_same_team(trick))
    return false;

  if (!condition_opposite_or_unknown_player_behind(trick))
    return true;

  if (is_with_trump_color(trick.game().type())
      && !condition_winnercard_ge(trick, Card::diamond_jack))
    return false;

  return true;
}

/** @return   card to play
 **/
Card
ServePartnerWithLowestCard::card_to_play(Trick const& trick)
{
  auto const& hand = this->hand();
  auto best_card = HandCard(hand);
  for (auto const& card : hand.validcards(trick)) {
    if (trick.isjabbed(card))
      continue;

    if (card.is_special())
      continue;
    if (!best_card) {
      best_card = card;
      continue;
    }
    if (   card.is_jabbed_by(best_card)
        && card.value() <= best_card.value() + 2) { // Ziehe einen Karo König einem Karo Buben vor, aber nicht 10 Punkte, das wäre Pfunden
      best_card = card;
      continue;
    }
    if (   best_card.istrump() == card.istrump()
        && card.value() < best_card.value()) {
      best_card = card;
      continue;
    }
  }
  if (best_card.istrump()) {
    while (best_card.points() >= 10) {
      auto const card = hand.next_higher_card(best_card);
      if (!card)
        break;
      best_card = card;
    }
  }
  if (best_card.is_special()) {
    rationale_.add(_("Heuristic::reject::only found special card: %s", _(best_card)));
    return best_card;
  }

  if (   !best_card
      || trick.isjabbed(best_card)) {
    rationale_.add(_("Heuristic::reject::cannot serve the trick"));
    return {};
  }
  rationale_.add(_("Heuristic::return::serve with lowest card: %s", _(best_card)));
  return best_card;
}

} // namespace Heuristics
