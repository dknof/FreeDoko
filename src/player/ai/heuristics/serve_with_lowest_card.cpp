/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "serve_with_lowest_card.h"

#include "../ai.h"

namespace Heuristics {

auto ServeWithLowestCard::is_valid(GameTypeGroup const game_type,
                                   PlayerTypeGroup const player_group) -> bool
{
  return true;
}

ServeWithLowestCard::ServeWithLowestCard(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::serve_with_lowest_card)
{ }

ServeWithLowestCard::~ServeWithLowestCard() = default;


auto ServeWithLowestCard::conditions_met(Trick const& trick) -> bool
{
  return true;
}


auto ServeWithLowestCard::card_to_play(Trick const& trick) -> Card
{
  auto const& hand = this->hand();
  HandCard best_card;
  for (auto const& card : hand.validcards(trick)) {
    if (trick.isjabbed(card))
      continue;

    if (card.points() >= 10)
      continue;

    if (!best_card) {
      best_card = card;
      continue;
    }
    if (best_card.is_jabbed_by(card))
      continue;
    if (card.points() < best_card.points()) {
      best_card = card;
      continue;
    }
    if (   best_card.istrump()
        && !card.istrump()
        && card.points() <= 4) {
      best_card = card;
      continue;
    }
    if (   is_with_trump_color(game().type())
        && best_card.istrump()
        && card.istrump()
        && card.points() <= 4) {
      best_card = card;
      continue;
    }
  }
  if (!best_card) {
    best_card = hand.next_jabbing_card(trick);
    if (best_card && best_card.points() < 10) {
      rationale_.add(_("Heuristic::return::jab with next higher card %s", _(best_card)));
      return best_card;
    }
    best_card = HandCard();

    for (auto const& card : hand.validcards(trick)) {
      if (trick.isjabbed(card))
        continue;

      if (card.points() >= 10)
        continue;

      if (!best_card) {
        best_card = card;
        continue;
      }
      if (best_card.is_jabbed_by(card))
        continue;

      best_card = card;
    }
  }

  if (!best_card) {
    rationale_.add(_("Heuristic::reject::cannot serve the trick"));
    return {};
  }
  if (   is_with_trump_color(game().type())
      && best_card.istrump()
      && best_card.points() >= 10) {
    if (hand.contains(Card::diamond_jack))
      best_card = Card::diamond_jack;
    else if (hand.contains(Card::heart_jack))
      best_card = Card::heart_jack;
    else if (hand.contains(Card::spade_jack))
      best_card = Card::spade_jack;
  }

  rationale_.add(_("Heuristic::return::serve with lowest card: %s", _(best_card)));
  return best_card;
}

} // namespace Heuristics
