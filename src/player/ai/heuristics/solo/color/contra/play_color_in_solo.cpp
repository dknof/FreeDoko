/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "play_color_in_solo.h"

#include "../../../../ai.h"
#include "../../../../../cards_information.of_player.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PlayColorInSolo::is_valid(GameTypeGroup const game_type,
                               PlayerTypeGroup const player_group)
{
  return (   is_color_solo(game_type)
          && is_with_partner(game_type, player_group));
}

/** constructor
 **/
PlayColorInSolo::PlayColorInSolo(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::play_color_in_solo)
{ }

/** destructor
 **/
PlayColorInSolo::~PlayColorInSolo() = default;

/** @return  whether the conditions are met
 **/
bool
PlayColorInSolo::conditions_met(Trick const& trick)
{
  return condition_startcard(trick);
}

/** @return  card to play
 **/
Card
PlayColorInSolo::card_to_play(Trick const& trick)
{
  auto const& cards_information = this->cards_information();
  auto const& hand = this->hand();
  auto const& game = this->game();
  auto const& soloplayer = game.players().soloplayer();

  auto color_valid = [&hand, &game](auto const color) {
    return (   hand.contains(color)
            && color != game.cards().trumpcolor());
  };
  // Farben im ersten Lauf vorziehen
  auto color_value = [&cards_information, &soloplayer](auto const color) {
    if (   cards_information.color_runs(color) == 0
        && cards_information.of_player(soloplayer).can_have(color)
        && cards_information.of_player(soloplayer).played(color) == 0)
      return cards_information.remaining_others(color);
    else
      return -cards_information.remaining_others(color);
  };
  auto const color = best_of_if(game.cards().colors(),
                                color_valid, color_value);

  if (color == Card::nocardcolor) {
    rationale_.add(_("Heuristic::reject::no suitable color found"));
    return {};
  }
  auto const card = hand.lowest_card(color);
  rationale_.add(_("Heuristic::return::lowest card of color %s: %s", _(color), _(card)));
  return card;
}

} // namespace Heuristics
