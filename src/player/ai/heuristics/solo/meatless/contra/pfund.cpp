/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "pfund.h"

#include "../../../../ai.h"
#include "../../../../../cards_information.of_player.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool MeatlessPfund::is_valid(GameTypeGroup const game_type,
                             PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::solo_meatless
          && player_group == PlayerTypeGroup::contra);
}

/** constructor
 **/
MeatlessPfund::MeatlessPfund(Ai const& ai, Aiconfig::Heuristic heuristic) :
  Heuristic(ai, heuristic)
{ }

/** destructor
 **/
MeatlessPfund::~MeatlessPfund() = default;

/** @return  card to play
 **/
Card
MeatlessPfund::card_to_play(Trick const& trick)
{
  auto const color = trick.startcard().color();
  return (hand().contains(color)
          ? get_pfund_card(color)
          : get_pfund_card());
}

/** @return    good high card of any color
 **/
Card
MeatlessPfund::get_pfund_card()
{
  vector<Card> pfunds;
  for (auto const color : game().cards().colors()) {
    auto const pfund = get_pfund_card(color);
    if (pfund) {
      rationale_.add(_("Heuristic::condition::potential pfund in color %s: %s", _(color), _(pfund)));
      pfunds.push_back(pfund);
    }
  }
  if (pfunds.empty())
    return {};
  if (pfunds.size() == 1)
    return pfunds[0];

  for (auto function : {
       &MeatlessPfund::pfund_soloplayer_has_only_greater_cards,
       &MeatlessPfund::pfund_soloplayer_has_too_many_greater_cards,
       &MeatlessPfund::pfund_soloplayer_guessed_has_not_color,
       &MeatlessPfund::pfund_soloplayer_has_not_color,
       &MeatlessPfund::pfund_with_highest_points,
       }) {
    auto const pfund = std::invoke(function, *this, pfunds);
    if (pfund) {
      rationale_.add(_("Heuristic::return::pfund %s", _(pfund)));
      return pfund;
    }
  }
  return {};
}

/** @return  best pfund the soloplayer has only greater cards of
 **/
Card
MeatlessPfund::pfund_soloplayer_has_only_greater_cards(vector<Card> const& pfunds)
{
  auto const hand_of_soloplayer = guessed_hand_of_soloplayer();
  Card best_pfund;
  for (auto const pfund : pfunds) {
    if (!hand_of_soloplayer.higher_card_exists(pfund))
      continue;
    if (hand_of_soloplayer.lower_card_exists(pfund))
      continue;
    auto const color = pfund.color();
    auto const ace = Card(color, Card::ace);
    if (   hand().contains(ace)
        && hand_of_soloplayer.contains(ace)
        && hand().count(color) <= 2) {
      rationale_.add(_("Heuristic::condition::keep card %s to protect the ace %s", _(pfund), _(ace)));
      continue;
    }
    rationale_.add(_("Heuristic::condition::the soloplayer has only cards greater then %s", _(pfund)));
    if (!best_pfund) {
      best_pfund = pfund;
    } else if (pfund.points() > best_pfund.points()) {
      rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(pfund), _(best_pfund)));
      best_pfund = pfund;
    }
  }
  return best_pfund;
}

/** @return   pfund the soloplayer has too many greater cards of
 **/
Card
MeatlessPfund::pfund_soloplayer_has_too_many_greater_cards(vector<Card> const& pfunds)
{
  auto const& hand_of_soloplayer = guessed_hand_of_soloplayer();
  Card best_pfund;
  for (auto const pfund : pfunds) {
    auto const color = pfund.color();
    auto const ace = Card(color, Card::ace);
    if (   hand().contains(ace)
        && hand_of_soloplayer.contains(ace)
        && hand().count(color) <= 2) {
      rationale_.add(_("Heuristic::condition::keep card %s to protect the ace %s", _(pfund), _(ace)));
      continue;
    }
    if (hand_of_soloplayer.count_ge_in_color(pfund) < hand().count(color))
      continue;
    rationale_.add(_("Heuristic::condition::the soloplayer has more cards greater then %s then I have in the color", _(pfund)));
    if (!best_pfund) {
      best_pfund = pfund;
    } else if (pfund.points() > best_pfund.points()) {
      rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(pfund), _(best_pfund)));
      best_pfund = pfund;
    }
  }
  return best_pfund;
}

/** @return   pfund the soloplayer has not the color of
 **/
Card
MeatlessPfund::pfund_soloplayer_guessed_has_not_color(vector<Card> const& pfunds)
{
  auto const& of_player = cards_information(game().players().soloplayer());
  Card best_pfund;
  for (auto const pfund : pfunds) {
    auto const color = pfund.color();
    if (of_player.can_have(color))
      continue;
    rationale_.add(_("Heuristic::condition::the soloplayer has not the color %s", _(color)));
    if (!best_pfund) {
      best_pfund = pfund;
    } else if (pfund.points() > best_pfund.points()) {
      rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(pfund), _(best_pfund)));
      best_pfund = pfund;
    }
  }
  return best_pfund;
}

/** @return   pfund the soloplayer has not the color of
 **/
Card
MeatlessPfund::pfund_soloplayer_has_not_color(vector<Card> const& pfunds)
{
  auto const& of_player = cards_information(game().players().soloplayer());
  Card best_pfund;
  for (auto const pfund : pfunds) {
    auto const color = pfund.color();
    if (of_player.must_have(color))
      continue;
    rationale_.add(_("Heuristic::condition::the soloplayer possible has not the color %s", _(color)));
    if (!best_pfund) {
      best_pfund = pfund;
    } else if (pfund.points() > best_pfund.points()) {
      rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(pfund), _(best_pfund)));
      best_pfund = pfund;
    }
  }
  return best_pfund;
}

/** @return  pfund with the highest points
 **/
Card
MeatlessPfund::pfund_with_highest_points(vector<Card> const& pfunds)
{
  Card best_pfund = pfunds[0];
  for (auto const pfund : pfunds) {
    if (pfund.points() > best_pfund.points()) {
      rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(pfund), _(best_pfund)));
      best_pfund = pfund;
    }
  }
  return best_pfund;
}

/** @return    pfund card of the color
 **/
Card
MeatlessPfund::get_pfund_card(Card::Color const color)
{
  auto const& hand = this->hand();
  if (!hand.contains(color))
    return {};

  auto const highest_card = get_highest_card(color);
  if (hand.count(color) == hand.count(highest_card))
    return highest_card;

  auto const& hand_of_soloplayer = guessed_hand_of_soloplayer();
  if (!hand_of_soloplayer.contains(color)) {
    rationale_.add(_("Heuristic::condition::the soloplayer does not have the color %s", _(color)));
    rationale_.add(_("Heuristic::return::highest card of color %s: %s", _(color), _(highest_card)));
    return highest_card;
  }
  rationale_.add(_("Heuristic::condition::the soloplayer does have the color %s", _(color)));

  auto const highest_card_of_soloplayer = hand_of_soloplayer.highest_card(color);
  rationale_.add(_("Heuristic::condition::highest card of color %s of the soloplayer: %s", _(color), _(highest_card_of_soloplayer)));
  {
    auto const card = hand.next_lower_card(highest_card_of_soloplayer);
    if (card) {
      rationale_.add(_("Heuristic::return::next lower card then the highest card of the soloplayer: %s", _(card)));
      return card;
    }
  }
  if (hand.contains(highest_card_of_soloplayer)) {
    rationale_.add(_("Heuristic::return::highest card of the soloplayer: %s", _(highest_card_of_soloplayer)));
    return highest_card_of_soloplayer;
  }

  rationale_.add(_("Heuristic::condition::have only higher cards then the soloplayer in the color %s", _(color)));
  rationale_.add(_("Heuristic::return::highest card of color %s: %s", _(color), _(highest_card)));
  return highest_card;
}

/** @return   highest card of 'color'
 **/
Card
MeatlessPfund::get_highest_card(Card::Color const color)
{
  auto const card = hand().highest_card(color);
  rationale_.add(_("Heuristic::return::highest card of color %s: %s", _(color), _(card)));
  return card;
}

} // namespace Heuristics
