/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "pfund_for_sure.h"

#include "../../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool MeatlessPfundForSure::is_valid(GameTypeGroup const game_type,
                                    PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::solo_meatless
          && player_group == PlayerTypeGroup::contra);
}

/** constructor
 **/
MeatlessPfundForSure::MeatlessPfundForSure(Ai const& ai) :
  MeatlessPfund(ai, Aiconfig::Heuristic::meatless_pfund_for_sure)
{ }

/** destructor
 **/
MeatlessPfundForSure::~MeatlessPfundForSure() = default;

/** @return  whether the conditions are met
 **/
bool
MeatlessPfundForSure::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && condition_winnerplayer_same_team(trick)
          && !condition_opposite_or_unknown_team_can_jab(trick)
         );
}

} // namespace Heuristics
