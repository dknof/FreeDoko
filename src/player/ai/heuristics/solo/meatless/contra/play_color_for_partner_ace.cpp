/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "play_color_for_partner_ace.h"

#include "../../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool MeatlessPlayColorForPartnerAce::is_valid(GameTypeGroup const game_type,
                                              PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::solo_meatless
          && is_with_partner(game_type, player_group));
}

/** constructor
 **/
MeatlessPlayColorForPartnerAce::MeatlessPlayColorForPartnerAce(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::meatless_play_color_for_partner_ace)
{ }

/** destructor
 **/
MeatlessPlayColorForPartnerAce::~MeatlessPlayColorForPartnerAce() = default;

/** @return  whether the conditions are met
 **/
bool
MeatlessPlayColorForPartnerAce::conditions_met(Trick const& trick)
{
  return (   condition_startcard(trick)
          && condition_color_exists_with_partner_highest_card());
}

/** @return   card to play
 **/
Card
MeatlessPlayColorForPartnerAce::card_to_play(Trick const& trick)
{
  auto const color = best_color_to_play();

  if (!color) {
    rationale_.add(_("Heuristic::reject::no suitable color found"));
    return {};
  }

  auto const& hand = this->hand();
  auto const card = hand.highest_card(color);
  rationale_.add(_("Heuristic::return::highest card from color %s: %s", _(color), _(card)));
  return card;
}

/** @return  whether there exists a color a partner has the highest card
 **/
bool
MeatlessPlayColorForPartnerAce::condition_color_exists_with_partner_highest_card()
{
  auto const& hand = this->hand();
  auto const& hand_of_soloplayer = guessed_hand_of_soloplayer();
  auto const& cards_information = this->cards_information();
  for (auto const color : game().cards().colors()) {
    if (!hand.contains(color))
      continue;
    auto const ace = Card(color, Card::ace);
    if (hand.contains(ace)) {
      rationale_.add(_("Heuristic::condition::I have the card %s", _(ace)));
      continue;
    }
    if (hand_of_soloplayer.contains(ace)) {
      rationale_.add(_("Heuristic::condition::the soloplayer has the card %s", _(ace)));
      continue;
    }
    rationale_.add(_("Heuristic::condition::the soloplayer does not have the card %s", _(ace)));
    if (cards_information.remaining(ace) > 0) {
      rationale_.add(_("Heuristic::condition::a partner has the card %s", _(ace)));
      return true;
    }
      rationale_.add(_("Heuristic::condition::no partner has the card %s", _(ace)));
  }
  return false;
}

/** @return  the best color to play: first run of the color, the partner can have the ace, shortest color
 **/
Card::Color
MeatlessPlayColorForPartnerAce::best_color_to_play()
{
  auto const& hand = this->hand();
  auto const& hand_of_soloplayer = guessed_hand_of_soloplayer();
  auto const& cards_information = this->cards_information();
  Card best_ace;
  for (auto const color : game().cards().colors()) {
    if (!hand.contains(color))
      continue;
    auto const ace = Card(color, Card::ace);
    if (hand.contains(ace))
      continue;
    if (cards_information.remaining(ace) == 0)
      continue;
    if (hand_of_soloplayer.contains(ace))
      continue;
    if (!best_ace) {
      best_ace = ace;
    } else if (cards_information.remaining(ace) > cards_information.remaining(best_ace)) {
      rationale_.add(_("Heuristic::condition::prefer color %s to %s", _(color), _(best_ace.color())));
      best_ace = ace;
    } else if (   cards_information.remaining(ace) == cards_information.remaining(best_ace)
               && hand.highest_card(color).points() > hand.highest_card(best_ace.color()).points()) {
      rationale_.add(_("Heuristic::condition::prefer color %s to %s", _(color), _(best_ace.color())));
      best_ace = ace;
    } else if (   cards_information.remaining(ace) == cards_information.remaining(best_ace)
               && hand.highest_card(color).points() == hand.highest_card(best_ace.color()).points()
               && cards_information.remaining_others(color) > cards_information.remaining_others(best_ace.color())) {
      rationale_.add(_("Heuristic::condition::prefer color %s to %s", _(color), _(best_ace.color())));
      best_ace = ace;
    }
  }
  return best_ace.color();
}

} // namespace Heuristics
