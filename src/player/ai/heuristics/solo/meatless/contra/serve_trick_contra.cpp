/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "serve_trick_contra.h"

#include "../../../../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool MeatlessServeTrickContra::is_valid(GameTypeGroup const game_type,
                                       PlayerTypeGroup const player_group)
{
  return (   game_type == GameTypeGroup::solo_meatless
          && player_group == PlayerTypeGroup::contra);
}

/** constructor
 **/
MeatlessServeTrickContra::MeatlessServeTrickContra(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::meatless_serve_trick_contra)
{ }

/** destructor
 **/
MeatlessServeTrickContra::~MeatlessServeTrickContra() = default;

/** @return  whether the conditions are met
 **/
bool
MeatlessServeTrickContra::conditions_met(Trick const& trick)
{
  return (   !condition_startcard(trick)
          && !condition_can_jab(trick)
          && (   !condition_winnerplayer_guessed_same_team(trick)
              || condition_opposite_or_unknown_team_can_jab(trick))
          && !condition_guessed_own_team_can_jab(trick)
         );
}

/** @return  card to play
 **/
Card
MeatlessServeTrickContra::card_to_play(Trick const& trick)
{
  if (hand().contains(trick.startcard().color()))
    return lowest_card(trick.startcard().color());

  vector<Card> cards;
  for (auto const color : game().cards().colors()) {
    auto const card = lowest_card(color);
    if (card)
      cards.push_back(card);
  }
  if (cards.empty())
    return {};
  if (cards.size() == 1)
    return cards[0];

  for (auto function : {
       &MeatlessServeTrickContra::single_low_card_the_soloplayer_has_color,
       &MeatlessServeTrickContra::single_low_card,
       &MeatlessServeTrickContra::low_card_of_color_soloplayer_has_not,
       }) {
    auto const card = std::invoke(function, *this, cards);
    if (card) {
      rationale_.add(_("Heuristic::return::low card %s", _(card)));
      return card;
    }
  }
  return {};
}

/** @return    smallest card of color
 **/
Card
MeatlessServeTrickContra::lowest_card(Card::Color color)
{
  auto const card = hand().lowest_card(color);
  rationale_.add(_("Heuristic::return::lowest card of color %s: %s", _(color), _(card)));
  return card;
}

/** @return   a single low card of a color the soloplayer does have
 **/
Card
MeatlessServeTrickContra::single_low_card_the_soloplayer_has_color(vector<Card> const& cards)
{
  auto const& hand = this->hand();
  auto const& hand_of_soloplayer = guessed_hand_of_soloplayer();
  Card best_card;
  for (auto const card : cards) {
    auto const color = card.color();
    if (card.points() >= 10)
      continue;
    if (hand_of_soloplayer.count_ge_in_color(card) > 0)
      continue;
    if (hand.count(color) > 1)
      continue;
    rationale_.add(_("Heuristic::condition::the soloplayer does have the color %s", _(color)));
    rationale_.add(_("Heuristic::condition::%s is the only card of color %s", _(card), _(color)));
    if (!best_card) {
      best_card = card;
    } else if (card.points() < best_card.points()) {
      rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(card), _(best_card)));
      best_card = card;
    }
  }
  return best_card;
}

/** @return   a single low card
 **/
Card
MeatlessServeTrickContra::single_low_card(vector<Card> const& cards)
{
  auto const& hand = this->hand();
  Card best_card;
  for (auto const card : cards) {
    auto const color = card.color();
    if (card.points() >= 10)
      continue;
    if (hand.count(color) > 1)
      continue;
    rationale_.add(_("Heuristic::condition::%s is the only card of color %s", _(card), _(color)));
    if (!best_card) {
      best_card = card;
    } else if (card.points() < best_card.points()) {
      rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(card), _(best_card)));
      best_card = card;
    }
  }
  return best_card;
}

/** @return   pfund the soloplayer has too many greater cards of
 **/
Card
MeatlessServeTrickContra::low_card_of_color_soloplayer_has_not(vector<Card> const& cards)
{
  auto const& hand_of_soloplayer = guessed_hand_of_soloplayer();
  Card best_card;
  for (auto const card : cards) {
    auto const color = card.color();
    if (hand_of_soloplayer.contains(color))
      continue;
    rationale_.add(_("Heuristic::condition::the soloplayer does not have the color %s", _(color)));
    if (!best_card) {
      best_card = card;
    } else if (card.points() < best_card.points()) {
      rationale_.add(_("Heuristic::condition::prefer card %s to %s", _(card), _(best_card)));
      best_card = card;
    }
  }
  return best_card;
}

} // namespace Heuristics
