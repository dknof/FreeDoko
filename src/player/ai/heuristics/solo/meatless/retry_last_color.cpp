/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "retry_last_color.h"

#include "../../../ai.h"

namespace Heuristics {

auto MeatlessRetryLastColor::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (game_type == GameTypeGroup::solo_meatless);
}


MeatlessRetryLastColor::MeatlessRetryLastColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::meatless_retry_last_color)
{ }


MeatlessRetryLastColor::~MeatlessRetryLastColor() = default;


auto MeatlessRetryLastColor::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && !condition_first_trick()
         );
}


auto MeatlessRetryLastColor::card_to_play(Trick const& trick) -> Card
{
  auto const color = last_color();
  auto const card = hand().highest_card(color);
  rationale_.add(_("Heuristic::return::highest card of %s: %s", _(color), _(card)));
  return card;
}


auto MeatlessRetryLastColor::last_color() -> Card::Color
{
  auto const& tricks = game().tricks();
  if (tricks.current_no() == 0)
    return Card::Color::nocardcolor;
  return tricks.trick(tricks.current_no() - 1).startcard().tcolor();
}

} // namespace Heuristics
