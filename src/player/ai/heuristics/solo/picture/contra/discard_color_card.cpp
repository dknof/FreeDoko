/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "discard_color_card.h"

#include "../../../../ai.h"

namespace Heuristics {

bool PictureContraDiscardColorCard::is_valid(GameTypeGroup const game_type,
                               PlayerTypeGroup const player_group)
{
  return is_picture_solo(game_type);
}


PictureContraDiscardColorCard::PictureContraDiscardColorCard(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_contra_discard_color_card)
{ }


PictureContraDiscardColorCard::~PictureContraDiscardColorCard() = default;


auto PictureContraDiscardColorCard::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && !condition_must_serve(trick));
}


auto PictureContraDiscardColorCard::card_to_play(Trick const& trick) -> Card
{
  auto const& game = this->game();
  auto const& hand = this->hand();
  auto const& soloplayer_hand = guessed_hand_of_soloplayer();
  
  // Einzelne kleine Karte, wenn der Solospieler eine höhere hat
  for (auto const value : {Card::nine, Card::jack, Card::queen, Card::king}) {
    if (!Card(Card::club, value).istrump(game)) {
      for (auto const color : game.cards().colors()) {
        auto const card = Card(color, value);
        if (   hand.contains(card)
            && hand.count(color) == 1
            && soloplayer_hand.count_ge_in_color(card)
           ) {
          rationale_.add(_("Heuristic::return::play single %s", _(card)));
          return card;
        }
      }
    }
  }

  // Kleine Karte, wenn man kein Ass und keine Zehn darin hat
  for (auto const value : {Card::nine, Card::jack, Card::queen, Card::king}) {
    if (!Card(Card::club, value).istrump(game)) {
      for (auto const color : game.cards().colors()) {
        auto const card = Card(color, value);
        if (   hand.contains(card)
            && !hand.contains(color, Card::ten)
            && !hand.contains(color, Card::ace)
            && cards_information().remaining(color, Card::ace) + cards_information().remaining(color, Card::ten) > 0
           ) {
          rationale_.add(_("Heuristic::return::play low %s of color %s without ace and ten", _(card), _(color)));
          return card;
        }
      }
    }
  }

#if 1
  // Kleine Karte, wenn der Solospieler eine höhere hat und man nicht sein Ass oder seine Zehn sichern muss
  for (auto const value : {Card::nine, Card::jack, Card::queen, Card::king}) {
    if (!Card(Card::club, value).istrump(game)) {
      for (auto const color : game.cards().colors()) {
        auto const card = Card(color, value);
        if (   hand.contains(card)
            && hand.count(color) > 1
            && !hand.contains(color, Card::ace)
            && !hand.contains(color, Card::ten)
            && soloplayer_hand.count_ge_in_color(card)
           ) {
          rationale_.add(_("Heuristic::return::play low %s, must not cover ace or ten", _(card)));
          return card;
        }
      }
    }
  }
#endif

#if 1
  // Kleinste Karte von einer Farbe, von der der Solospieler nur die höchste hat
  for (auto const value : {Card::nine, Card::jack, Card::queen, Card::king}) {
    if (!Card(Card::club, value).istrump(game)) {
      for (auto const color : game.cards().colors()) {
        auto const card = Card(color, value);
        if (   hand.contains(card)
            && soloplayer_hand.contains(color)
            && (   soloplayer_hand.count(color) == soloplayer_hand.count(color, Card::ace)
                || (   cards_information().remaining(color, Card::ace) == 0
                    && soloplayer_hand.count(color) == soloplayer_hand.count(color, Card::ten)))
           ) {
          rationale_.add(_("Heuristic::return::play low %s, the soloplayer does only have the highest card of color %s", _(card), _(color)));
          return card;
        }
      }
    }
  }
#endif

#if 1
  // Kleine Karte, wenn man noch das Ass hat und sonst keiner
  for (auto const value : {Card::nine, Card::jack, Card::queen, Card::king}) {
    if (!Card(Card::club, value).istrump(game)) {
      for (auto const color : game.cards().colors()) {
        auto const card = Card(color, value);
        if (   hand.contains(card)
            && hand.contains(color, Card::ace)
            && !cards_information().remaining_others(color, Card::ace)
           ) {
          rationale_.add(_("Heuristic::return::play low %s, have also the remaining ace in color %s", _(card), _(color)));
          return card;
        }
      }
    }
  }
#endif

#if 1
  // Kleinste Karte von einer Farbe, die der Solospieler nicht (mehr) hat
  for (auto const value : {Card::nine, Card::jack, Card::queen, Card::king}) {
    if (!Card(Card::club, value).istrump(game)) {
      for (auto const color : game.cards().colors()) {
        auto const card = Card(color, value);
        if (   hand.contains(card)
            && !soloplayer_hand.contains(color)
           ) {
          rationale_.add(_("Heuristic::return::play low %s, the soloplayer does not have color %s", _(card), _(color)));
          return card;
        }
      }
    }
  }
#endif

#if 1
  // Kleine Karte, wenn man noch mindestens zwei weitere Karten der Farbe hat.
  for (auto const value : {Card::nine, Card::jack, Card::queen, Card::king}) {
    if (!Card(Card::club, value).istrump(game)) {
      for (auto const color : game.cards().colors()) {
        auto const card = Card(color, value);
        if (   hand.contains(card)
            && hand.count(color) >= 3
           ) {
          rationale_.add(_("Heuristic::return::play low %s, have another %u cards of color %s", _(card), hand.count(color), _(color)));
          return card;
        }
      }
    }
  }
#endif

#if 1
  // Einzelne Zehn, wenn noch ein Ass unterwegs ist
  for (auto const color : game.cards().colors()) {
    auto const card = Card(color, Card::ten);
    if (   hand.contains(card)
        && hand.count(color) == 1
        && cards_information().remaining(color, Card::ace)
       ) {
      rationale_.add(_("Heuristic::return::play single %s", _(card)));
      return card;
    }
  }
#endif
#if 1
  // Zehn, wenn das zugehörige Ass noch draußen ist
  for (auto const color : game.cards().colors()) {
    auto const card = Card(color, Card::ten);
    if (   hand.contains(card)
        && hand.count(color) == hand.count(card)
        && cards_information().remaining(color, Card::ace)) {
      rationale_.add(_("Heuristic::return::play %s, it is single and there is %s remaining", _(card), _(Card(color, Card::ace))));
      return card;
    }
  }
#endif


#if 1
  // Solo Zehn
  for (auto const color : game.cards().colors()) {
    auto const card = Card(color, Card::ten);
    if (   hand.contains(card)
        && hand.count(color) == hand.count(card)
        && cards_information().remaining_others(color) == 0) {
      rationale_.add(_("Heuristic::return::play %s, no other card of color %s is remaining", _(card), _(color)));
      return card;
    }
  }
#endif

  // Kleine Karte, wenn beide Asse noch draußen sind
  for (auto const value : {Card::nine, Card::jack, Card::queen, Card::king}) {
    if (!Card(Card::club, value).istrump(game)) {
      for (auto const color : game.cards().colors()) {
        auto const card = Card(color, value);
        if (   hand.contains(card)
            && !hand.contains(color, Card::ace)
            && hand.count(color) <= cards_information().remaining(color, Card::ace)
           ) {
          rationale_.add(_("Heuristic::return::play low %s of color %s, have no more cards then remaining aces", _(card), _(color)));
          return card;
        }
      }
    }
  }

  { // Kleinste Karte eine Farbe, die der Solospieler nicht mehr hat
    Card card;
    for (auto const color : game.cards().colors()) {
      if (   hand.contains(color)
             && !soloplayer_hand.contains(color)) {
        auto const c = hand.lowest_card(color);
        if (!card || card.value() > c.value()) {
          card = c;
        }
      }
      if (card) {
        rationale_.add(_("Heuristic::return::play lowest %s of color %s, since the soloplayer does not have the color", _(card), _(color)));
        return card;
      }
    }
  }

  rationale_.add(_("Heuristic::reject::no color card found to discard"));
  return {};
}

} // namespace Heuristics
