/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_color_trick.h"

#include "../../../../ai.h"

namespace Heuristics {

auto PictureContraJabColorTrick::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (   is_picture_solo(game_type)
          && player_group == PlayerTypeGroup::contra);
}


PictureContraJabColorTrick::PictureContraJabColorTrick(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_contra_jab_color_trick)
{ }


PictureContraJabColorTrick::~PictureContraJabColorTrick() = default;


auto PictureContraJabColorTrick::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && condition_color_trick(trick)
          && condition_can_jab_with_trump(trick)
          && condition_soloplayer_can_jab_in_color(trick)
          // ToDo: save highest trump for further jabbing
         );
}


auto PictureContraJabColorTrick::card_to_play(Trick const& trick) -> Card
{
  auto const card = hand().lowest_trump();
  rationale_.add(_("Heuristic::return::jab with %s", _(card)));
  return card;
}


} // namespace Heuristics
