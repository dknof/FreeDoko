/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_trump_trick.h"

#include "../../../../ai.h"

namespace Heuristics {

bool PictureJabTrumpTrick::is_valid(GameTypeGroup const game_type,
                                           PlayerTypeGroup const player_group)
{
  return (   is_picture_solo(game_type)
          && player_group == PlayerTypeGroup::contra);
}


PictureJabTrumpTrick::PictureJabTrumpTrick(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_jab_trump_trick)
{ }


PictureJabTrumpTrick::~PictureJabTrumpTrick() = default;


auto PictureJabTrumpTrick::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && condition_trump_trick(trick)
          && condition_can_jab_with_trump(trick)
          && condition_winnerplayer_soloplayer(trick)
          // ToDo: save highest trump for further jabbing
         );
}


auto PictureJabTrumpTrick::card_to_play(Trick const& trick) -> Card
{
  auto const card = hand().next_jabbing_card(trick.winnercard());
  if (!card) {
    rationale_.add(_("Heuristic::reject::cannot jab, winnercard is too high"));
    return {};
  }
  rationale_.add(_("Heuristic::return::jab with %s", _(card)));
  return card;
}

} // namespace Heuristics
