/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_with_high_color_card.h"

#include "../../../../ai.h"

namespace Heuristics {

auto PictureContraJabWithHighColorCard::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (   is_picture_solo(game_type)
          && player_group == PlayerTypeGroup::contra);
}


PictureContraJabWithHighColorCard::PictureContraJabWithHighColorCard(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_contra_jab_with_high_color_card)
{ }


PictureContraJabWithHighColorCard::~PictureContraJabWithHighColorCard() = default;


auto PictureContraJabWithHighColorCard::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && condition_color_trick(trick)
          && !condition_winnercard_trump(trick)
          && condition_has(trick.startcolor())
          && condition_can_jab(trick)
          && (   condition_winnerplayer_soloplayer(trick)
              || condition_soloplayer_can_jab_in_color(trick))
         );
}


auto PictureContraJabWithHighColorCard::card_to_play(Trick const& trick) -> Card
{
  auto const color = trick.startcolor();
  auto const card = hand().highest_card(color);
  rationale_.add(_("Heuristic::return::jab with highest card in color %s: %s", _(color), _(card)));
  return card;
}

} // namespace Heuristics
