/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "pfund.h"

#include "../../../../ai.h"
#include "../../../../../../game/game.h"


namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool PicturePfund::is_valid(GameTypeGroup const game_type,
                            PlayerTypeGroup const player_group)
{
  return (   is_picture_solo(game_type)
          && is_with_partner(game_type, player_group));
}

/** constructor
 **/
PicturePfund::PicturePfund(Ai const& ai, Aiconfig::Heuristic heuristic) :
  Heuristic(ai, heuristic)
{ }

/** destructor
 **/
PicturePfund::~PicturePfund() = default;

/** @return   highest card of 'color'
 **/
Card
PicturePfund::get_highest_card(Card::Color const color)
{
  auto const card = hand().highest_card(color);
  auto const second_highest_card = hand().second_highest_card(color);
  auto const hand_soloplayer = guessed_hand_of_soloplayer();
  if (hand_soloplayer.contains(color)) {
    auto const highest_card_of_soloplayer = hand_soloplayer.highest_card(color);
    if (   highest_card_of_soloplayer.is_jabbed_by(card)
        && !highest_card_of_soloplayer.is_jabbed_by(second_highest_card)) {
      rationale_.add(_("Heuristic::condition::keep highest card of color %s: %s for later jab", _(color), _(card)));
      rationale_.add(_("Heuristic::return::second highest card of color %s: %s", _(color), _(card)));
      return second_highest_card;
    }
    if (   card.value() == Card::ace
        && hand().count(color) > 2) {
      rationale_.add(_("Heuristic::condition::keep %s for later jab", _(card)));
      rationale_.add(_("Heuristic::return::second highest card of color %s: %s", _(color), _(card)));
      return second_highest_card;
    }
  }
  rationale_.add(_("Heuristic::return::highest card of color %s: %s", _(color), _(card)));
  return card;
}

/** @return    a blank ten
 **/
Card
PicturePfund::get_blank_ten()
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    auto const ten = Card(color, Card::ten);
    if (   hand.count(color) == 1
        && hand.contains(ten)) {
      rationale_.add(_("Heuristic::return::blank ten: %s", _(ten)));
      return ten;
    }
  }
  rationale_.add(_("Heuristic::reject::have no blank ten"));
  return {};
}

/** @return    a single ten
 **/
Card
PicturePfund::get_single_ten()
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    auto const ten = Card(color, Card::ten);
    if (hand.count(ten) == 1) {
      rationale_.add(_("Heuristic::return::single ten: %s", _(ten)));
      return ten;
    }
  }
  rationale_.add(_("Heuristic::reject::have no single ten"));
  return {};
}

/** @return    a double ten (no other card of the color)
 **/
Card
PicturePfund::get_solo_double_ten()
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    auto const ten = Card(color, Card::ten);
    if (   hand.count(ten) == 2
        && hand.count(color) == 2) {
      rationale_.add(_("Heuristic::return::solo double ten: %s", _(ten)));
      return ten;
    }
  }
  rationale_.add(_("Heuristic::reject::have no solo double ten"));
  return {};
}

/** @return    a double ten
 **/
Card
PicturePfund::get_double_ten()
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    auto const ten = Card(color, Card::ten);
    if (hand.count(ten) == 2) {
      rationale_.add(_("Heuristic::return::double ten: %s", _(ten)));
      return ten;
    }
  }
  rationale_.add(_("Heuristic::reject::have no double ten"));
  return {};
}

/** @return    a ten
 **/
Card
PicturePfund::get_ten()
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    auto const ten = Card(color, Card::ten);
    if (hand.contains(ten)) {
      rationale_.add(_("Heuristic::return::ten: %s", _(ten)));
      return ten;
    }
  }
  rationale_.add(_("Heuristic::reject::have no ten"));
  return {};
}

/** @return    a blank ace
 **/
Card
PicturePfund::get_blank_ace()
{
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();
  auto const& hand_of_soloplayer = guessed_hand(game().players().soloplayer());
  for (auto const color : game().cards().colors()) {
    auto const ace = Card(color, Card::ace);
    if (   hand.count(color) == 1
        && hand.contains(ace)
        && cards_information.played(ace) == 1
        && !hand_of_soloplayer.contains(color)
       ) {
      rationale_.add(_("Heuristic::return::blank ace: %s", _(ace)));
      return ace;
    }
  }
  rationale_.add(_("Heuristic::reject::have no blank ace"));
  return {};
}

/** @return    a double ace
 **/
Card
PicturePfund::get_double_ace()
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    auto const ace = Card(color, Card::ace);
    if (hand.count(ace) == 2) {
      rationale_.add(_("Heuristic::return::double ace: %s", _(ace)));
      return ace;
    }
  }
  rationale_.add(_("Heuristic::reject::have no double ace"));
  return {};
}

/** @return    an ace
 **/
Card
PicturePfund::get_ace()
{
  auto const& hand = this->hand();
  for (auto const color : game().cards().colors()) {
    auto const ace = Card(color, Card::ace);
    if (hand.contains(ace)) {
      rationale_.add(_("Heuristic::return::ace: %s", _(ace)));
      return ace;
    }
  }
  rationale_.add(_("Heuristic::reject::have no ace"));
  return {};
}

/** @return   high card from a color the others do not have
 **/
Card
PicturePfund::get_high_card_from_solo_color()
{
  auto const& hand = this->hand();
  bool have_solo_color = false;
  for (auto const color : game().cards().colors()) {
    if (!is_solo_color(color))
      continue;
    auto const card = hand.highest_card(color);
    if (card.value() >= 10) {
      rationale_.add(_("Heuristic::return::highest card from solo color: %s", _(card)));
      return card;
    }
    rationale_.add(_("Heuristic::return::%s is solo color, but highest card has value < 10", _(color)));
    have_solo_color = true;
  }
  if (have_solo_color) {
    rationale_.add(_("Heuristic::reject::have no high card in a solo color"));
  } else {
    rationale_.add(_("Heuristic::reject::have no solo color"));
  }
  return {};
}

/** @return   highest card from a color the others do not have
 **/
Card
PicturePfund::get_highest_card_from_solo_color()
{
  auto const& hand = this->hand();
  Card highest_card;
  for (auto const color : game().cards().colors()) {
    if (!is_solo_color(color))
      continue;
    auto const card = hand.highest_card(color);
    if (highest_card.value() < card.value()) {
      highest_card = card;
    }
  }
  if (highest_card) {
    rationale_.add(_("Heuristic::return::highest card from solo color: %s", _(highest_card)));
    return highest_card;
  }
  rationale_.add(_("Heuristic::reject::have no solo color"));
  return {};
}

/** @return    a king/queen/jack
 **/
Card
PicturePfund::get_medium_card()
{
  auto const& hand = this->hand();
  auto const& game = this->game();
  for (auto const value : {Card::king, Card::queen, Card::jack}) {
    for (auto const color : game.cards().colors()) {
      auto const card = Card(color, value);
      if (   !card.istrump(game)
          && hand.contains(card)
          && !(   hand.contains(color, Card::ace)
               && hand.count(color) == 2)) {
        rationale_.add(_("Heuristic::return::%s", _(card)));
        return card;
      }
    }
  }
  rationale_.add(_("Heuristic::reject::have no suitable king/queen/jack"));
  return {};
}

/** @return   whether color is a solo color
 **/
bool
PicturePfund::is_solo_color(Card::Color const color) const
{
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();
  return (   hand.contains(color)
          && !cards_information.remaining_others(color));
}

} // namespace Heuristics
