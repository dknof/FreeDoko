/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "jab_color_trick_for_sure.h"

#include "../../../ai.h"
#include "../../../../cards_information.of_player.h"

namespace Heuristics {

auto PictureJabColorTrickForSure::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return is_picture_solo(game_type);
}


PictureJabColorTrickForSure::PictureJabColorTrickForSure(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_jab_color_trick_for_sure)
{ }


PictureJabColorTrickForSure::~PictureJabColorTrickForSure() = default;


auto PictureJabColorTrickForSure::conditions_met(Trick const& trick) -> bool
{
  return (   !condition_startcard(trick)
          && condition_color_trick(trick)
          && condition_can_jab_with_trump(trick)
          //&& !condition_opposite_high_trump_behind(trick) // ist in card_to_play integriert, ansonsten wäre zu viel Logik doppelt
         );
}


auto PictureJabColorTrickForSure::card_to_play(Trick const& trick) -> Card
{
  auto card = hand().next_jabbing_card(trick.winnercard());
  if (!card)
    return {};

  if (trick.islastcard()) {
    rationale_.add(_("Heuristic::condition::last card"));
    rationale_.add(_("Heuristic::return::jab with lowest jabbing trump %s", _(card)));
    return card;
  }
  for (auto const& p : game().players()) {
    if (trick.has_played(p))
      continue;
    if (guessed_same_team(p))
      continue;
    if (!guessed_hand(p).hastrump())
      continue;
    if (cards_information().of_player(p).must_have(trick.startcard().color()))
      continue;
    auto const trumpno = max(cards_information(p).must_have_trump(),
                             min(guessed_hand(p).count_trumps(),
                                 (cards_information().remaining_trumps_others() + 1) / 2)); 
    if (trumpno < player().hand().count_ge_trump(guessed_hand(p).highest_trump())) {
      if (guessed_hand(p).highest_trump().jabs(card)) {
        card = guessed_hand(p).highest_trump();
        card = hand().contains(card) ? card : hand().next_jabbing_card(card);
      }
      rationale_.add(_("Heuristic::condition::player %s behind still has a trump", p.name()));
      continue;
    }
    rationale_.add(_("Heuristic::reject::player %s behind still has a high trump", p.name()));
    return {};
  }
  if (!card) {
    rationale_.add(_("Heuristic::reject::cannot jab for sure"));
    return {};
  }
  rationale_.add(_("Heuristic::return::jab with trump %s", _(card)));
  return card;
}

} // namespace Heuristics
