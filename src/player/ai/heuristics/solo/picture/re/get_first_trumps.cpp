/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "get_first_trumps.h"

#include "../../../../ai.h"

namespace Heuristics {

auto PictureGetFirstTrumps::is_valid(GameTypeGroup const game_type,
                                     PlayerTypeGroup const player_group) -> bool
{
  return is_picture_solo(game_type);
}


PictureGetFirstTrumps::PictureGetFirstTrumps(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_get_first_trumps)
{ }


PictureGetFirstTrumps::~PictureGetFirstTrumps() = default;


auto PictureGetFirstTrumps::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && !condition_noone_else_has_trump()
          && condition_have_enough_high_trumps()
         );
}


auto PictureGetFirstTrumps::card_to_play(Trick const& trick) -> Card
{
  auto const highest_trump_of_others = cards_information().highest_remaining_trump_of_others();
  auto const trump = hand().same_or_higher_card(highest_trump_of_others);
  rationale_.add(_("Heuristic::return::play the highest trump: %s", _(trump)));
  return trump;
}


auto PictureGetFirstTrumps::condition_have_enough_high_trumps() -> bool
{
  auto const highest_trumps_no = count_highest_trumps();
  if (highest_trumps_no == 0) {
    rationale_.add(_("Heuristic::condition::have not the highest trump"));
    return false;
  }

  auto const remaining_trumps_others = cards_information().remaining_trumps_others();
  auto const highest_trump_of_others = cards_information().highest_remaining_trump_of_others();
  auto const higher_trump_no = hand().count_ge_trump(highest_trump_of_others);
  if (higher_trump_no >= remaining_trumps_others) {
    rationale_.add(_("Heuristic::condition::can get all remaining trump"));
    return true;
  }
  if (remaining_trumps_others <= 2) {
    rationale_.add(_("Heuristic::condition::only %u remaining trump", remaining_trumps_others));
    return true;
  }
  if (higher_trump_no >= 3) {
    rationale_.add(_("Heuristic::condition::have %u trump above the other players", higher_trump_no));
    return true;
  }
  if (   higher_trump_no >= 2 - (chancy() ? 1 : 0)
      && remaining_trumps_others <= 3) {
    rationale_.add(_("Heuristic::condition::only %u remaining trump of the other players but have %u high trump", remaining_trumps_others, higher_trump_no));
    return true;
  }
  if (   condition_first_trick()
      && higher_trump_no >= (  remaining_trumps_others == 1 ? 1
                             : remaining_trumps_others == 2 ? 3
                             : remaining_trumps_others == 3 ? 4
                             : (remaining_trumps_others + 4) / 2)
     ) {
    rationale_.add(_("Heuristic::condition::first trick and have %u high trump against %u remaining trump of the other players", higher_trump_no, remaining_trumps_others));
    return true;
  }

  auto const has_trump = [this](Player const& player) {
    return (   player.team() != team()
            && guessed_hand(player).hastrump());
  };
  unsigned const players_with_trump_no = count_if(game().players(), has_trump);

  if (players_with_trump_no == 1) {
    rationale_.add(_("Heuristic::condition::only one other player with trump"));
    return false;
  }

  return false;
}


auto PictureGetFirstTrumps::count_highest_trumps() -> unsigned
{
  auto const highest_trump_of_others = cards_information().highest_remaining_trump_of_others();
  if (!highest_trump_of_others) {
    return hand().count_trumps();
  }
  return hand().count_ge(highest_trump_of_others);
}

} // namespace Heuristics
