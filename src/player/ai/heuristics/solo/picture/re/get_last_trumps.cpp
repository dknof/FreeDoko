/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "get_last_trumps.h"

#include "../../../../ai.h"

namespace Heuristics {

auto PictureGetLastTrumps::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return is_picture_solo(game_type);
}


PictureGetLastTrumps::PictureGetLastTrumps(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_get_last_trumps)
{ }


PictureGetLastTrumps::~PictureGetLastTrumps() = default;


auto PictureGetLastTrumps::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_has_trump()
          && !condition_noone_else_has_trump()
          && can_get_all_trumps()
         );
}


auto PictureGetLastTrumps::card_to_play(Trick const& trick) -> Card
{
  if (is_single_picture_solo(game().type())) {
    auto const value = (  game().type() == GameType::solo_jack  ? Card::Value::jack
                        : game().type() == GameType::solo_queen ? Card::Value::queen
                        : game().type() == GameType::solo_king  ? Card::Value::king
                        : Card::Value::nocardvalue);
    if (   hand().count(Card::club, value) == 2
        && cards_information().remaining_trumps_others() >= 3) {
      // Erst später mit Pik Buben antäuschen
      auto const card = Card(Card::club, value);
      rationale_.add(_("Heuristic::return::play highest trump %s", _(card)));
      return card;
    }
  }
  auto const highest_opposite_trump = cards_information().highest_remaining_trump_of_others();
  auto const card = hand().same_or_higher_card(highest_opposite_trump);
  rationale_.add(_("Heuristic::return::play highest trump %s", _(card)));
  return card;
}


auto PictureGetLastTrumps::can_get_all_trumps() -> bool
{
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();
  auto const highest_opposite_trump = cards_information.highest_remaining_trump_of_others();
  auto const n = count_opponents_with_trump();
  auto const remaining_trumps_others = cards_information.remaining_trumps_others();
  auto const higher_trumps_count = hand.count_ge(highest_opposite_trump);
  if (n == 1) {
    if (higher_trumps_count >= remaining_trumps_others) {
      rationale_.add(_("Heuristic::condition::enough trumps of me are higher then the remaining ones"));
      return true;
    }
    rationale_.add(_("Heuristic::condition::not enough trumps of me are higher then the remaining ones"));
    return false;
  }
  if (   cards_information.trump_runs() == 0
      && remaining_trumps_others <= 4
      && higher_trumps_count > remaining_trumps_others / 2) {
    rationale_.add(_("Heuristic::condition::no trump run and have the highest trump"));
    return true;
  }
  if (   higher_trumps_count == 1
      && remaining_trumps_others == 2) {
    // entweder ich erwische beide oder verliere sowieso einen Stich
    rationale_.add(_("Heuristic::condition::I have enough higher trumps"));
    return true;
  }
  if (n == 2) {
    if (higher_trumps_count
        >= remaining_trumps_others - remaining_trumps_others / 3) {
      rationale_.add(_("Heuristic::condition::I have enough higher trumps"));
      return true;
    }
    rationale_.add(_("Heuristic::condition::I do not have enough higher trumps"));
    return false;
  }
  if (n == 3) {
    if (higher_trumps_count
        >= remaining_trumps_others - remaining_trumps_others / 2) {
      rationale_.add(_("Heuristic::condition::I have enough higher trumps"));
      return true;
    }
    rationale_.add(_("Heuristic::condition::I do not have enough higher trumps"));
    return false;
  }
  DEBUG_ASSERTION(n <= 3,
                  "Heuristic::PictureGetLastTrumps::can_get_all_trumps()\n"
                  "   More then 3 opponents still have trump");
  return false;
}


auto PictureGetLastTrumps::count_opponents_with_trump() const -> unsigned
{
  unsigned n = 0;
  for (auto const& p : game().players()) {
    if (!opposite_team(p))
      continue;

    if (guessed_hand(p).contains(Card::trump))
      n += 1;
  }
  return n;
}

} // namespace Heuristics
