/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "get_trumps_before_blank_color_ace.h"

#include "../../../../ai.h"

namespace Heuristics {

auto PictureGetTrumpsBeforeBlankColorAce::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return is_picture_solo(game_type);
}


PictureGetTrumpsBeforeBlankColorAce::PictureGetTrumpsBeforeBlankColorAce(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_get_trumps_before_blank_color_ace)
{ }


PictureGetTrumpsBeforeBlankColorAce::~PictureGetTrumpsBeforeBlankColorAce() = default;


auto PictureGetTrumpsBeforeBlankColorAce::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_has_highest_trump()
          && !condition_noone_else_has_trump()
          && condition_has_color_blank_ace()
         );
}


auto PictureGetTrumpsBeforeBlankColorAce::card_to_play(Trick const& trick) -> Card
{
  auto const highest_trump_of_others = cards_information().highest_remaining_trump_of_others();
  auto const trump = hand().same_or_higher_card(highest_trump_of_others);
  rationale_.add(_("Heuristic::return::play highest trump %s", _(trump)));
  return trump;
}


} // namespace Heuristics
