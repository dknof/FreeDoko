/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "start_with_loosing_solo_color.h"

#include "../../../../ai.h"

namespace Heuristics {

auto PictureStartWithLoosingSoloColor::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return (   is_picture_solo(game_type)
          && player_group == PlayerTypeGroup::re);
}


PictureStartWithLoosingSoloColor::PictureStartWithLoosingSoloColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_start_with_loosing_solo_color)
{ }


PictureStartWithLoosingSoloColor::~PictureStartWithLoosingSoloColor() = default;


auto PictureStartWithLoosingSoloColor::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_has_trump()
          && !condition_opposite_team_has_trump());
  // have single color card, other players can jab -> card_to_play()
}


auto PictureStartWithLoosingSoloColor::card_to_play(Trick const& trick) -> Card
{
  Card card;
  for (auto const color : game().cards().colors()) {
    if (!hand().contains(color))
      continue;
    if (!cards_information().remaining_others(color))
      continue;
    if (hand().count(color) > 1)
      continue;

    rationale_.add(_("Heuristic::condition::only one card in color %s", _(color)));

    if (cards_information().lowest_remaining_card_of_others(color).points() >= 10) {
      continue;
    }

    {
      auto const c = hand().highest_card(color);
      auto const c2 = cards_information().highest_remaining_card_of_others(color);

      if (!c.is_jabbed_by(c2)) {
        rationale_.add(_("Heuristic::condition::my card %s ist the highest in color %s", _(card), _(color)));
        continue;
      }
      rationale_.add(_("Heuristic::condition::card %s is jabbed by the other players", _(c)));
    }

    auto const c = hand().lowest_card(color);
    if (!card) {
      card = c;
    } else if (c.value() < card.value()) {
      rationale_.add(_("Heuristic::condition::prefer card %s to %s because it has fewer points", _(c), _(card)));
      card = c;
    }
  }

  if (!card) {
    rationale_.add(_("Heuristic::reject::no solo color card found that is jabbed by the other players"));
    return {};
  }

  rationale_.add(_("Heuristic::return::play %s", _(card)));
  return card;
}

} // namespace Heuristics
