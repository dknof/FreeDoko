/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "start_with_winning_card_before_bad_loosing_cards.h"
#include "../pull_down_color.h"

#include "../../../../ai.h"

namespace Heuristics {

auto PictureStartWithWinningCardBeforeBadLoosingCard::is_valid(GameTypeGroup const game_type, PlayerTypeGroup const player_group) -> bool
{
  return is_picture_solo(game_type);
}


PictureStartWithWinningCardBeforeBadLoosingCard::PictureStartWithWinningCardBeforeBadLoosingCard(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_start_with_winning_card_before_bad_loosing_cards)
{ }


PictureStartWithWinningCardBeforeBadLoosingCard::~PictureStartWithWinningCardBeforeBadLoosingCard() = default;


auto PictureStartWithWinningCardBeforeBadLoosingCard::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && condition_noone_else_has_trump()
          && condition_at_most_simple_loosing_colors()
         );
}


auto PictureStartWithWinningCardBeforeBadLoosingCard::card_to_play(Trick const& trick) -> Card
{
  auto const card = hand().lowest_trump();
  if (card) {
    rationale_.add(_("Heuristic::return::play lowest trump %s", _(card)));
    return card;
  }
  auto pull_down_color = PicturePullDownColor(ai());
  pull_down_color.set_player(player());
  return pull_down_color.card_to_play(trick);
}


auto PictureStartWithWinningCardBeforeBadLoosingCard::condition_at_most_simple_loosing_colors() -> bool
{
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();
  unsigned needed_trumps_count = 0;
  unsigned bad_color_cards_count = 0;

  for (auto const c : game().cards().colors()) {
    auto const n = hand.count(c);
    if (n == 0)
      continue;

    auto const n_others = cards_information.remaining_others(c);
    if (n_others == 0)
      continue;

    auto const num_ace = hand.count(c, Card::ace);
    if (n_others <= num_ace)
      continue;
    auto const num_ace_others = cards_information.remaining_others(c, Card::ace);

    if (n == num_ace)
      continue;

    if (num_ace_others) {
      if (num_ace && n == num_ace + 1) {
        needed_trumps_count += 1;
        continue;
      }
      if (cards_information.lowest_remaining_card_of_others(c).value() < 10) {
        //&& hand.lowest_card(c).value() < 10 // in mehreren Fällen schlechteres Ergebnis
        rationale_.add(_("Heuristic::condition::too many low cards in color %s", _(c)));
        return false;
      }
      needed_trumps_count += min(n - num_ace, n_others);
      bad_color_cards_count += n - num_ace;
      continue;
    }

    auto const num_ten = hand.count(c, Card::ten);
    if (n_others <= num_ace + num_ten)
      continue;

    if (n_others <= 1.5 * (num_ace + num_ten))
      continue;
    if (n - num_ace - num_ten <= 1)
      continue;

    needed_trumps_count += c - num_ace - num_ten;

    auto const num_ten_others = cards_information.remaining_others(c, Card::ten);
    auto const num_opposite_low = n_others - num_ace_others - num_ten_others;
    // Wenn ich das Ass ausspiele, dann haben anschließend die Gegner nur noch volle in der Farbe.
    // Siehe Situation oben
    if (num_opposite_low <= num_ace) {
      bad_color_cards_count += n - num_ace - num_ten;
      continue;
    }

    if (num_ten_others) {
      rationale_.add(_("Heuristic::condition::too many low cards in color %s", _(c)));
      return false;
    }

    // ToDo: king, queen, jack, nine

    rationale_.add(_("Heuristic::condition::too many low cards in color %s", _(c)));
    return false;
  }

  if (bad_color_cards_count <= 1) {
    // Nur eine schlechte Karte, die kann ich auch am Ende spielen
    // ToDo: eventuell doch nicht, vielleicht schmieren die Gegner früher weniger Punkte
    return true;
  }

  if (needed_trumps_count && std::max(2u, needed_trumps_count) > player().hand().count_trumps()) { // Ein Trumpf wird noch zum Ausspielen benoetigt
    rationale_.add(_("Heuristic::condition::too few trumps"));
    return false;
  }

  return true;
}

} // namespace Heuristics
