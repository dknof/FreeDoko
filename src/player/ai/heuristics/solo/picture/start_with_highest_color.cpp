/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "start_with_highest_color.h"

#include "../../../ai.h"

namespace Heuristics {

auto PictureStartWithHighestColor::is_valid(GameTypeGroup const game_type,
                                            PlayerTypeGroup const player_group) -> bool
{
  return is_picture_solo(game_type);
}


PictureStartWithHighestColor::PictureStartWithHighestColor(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::picture_start_with_highest_color)
{ }


PictureStartWithHighestColor::~PictureStartWithHighestColor() = default;


auto PictureStartWithHighestColor::conditions_met(Trick const& trick) -> bool
{
  return (   condition_startcard(trick)
          && (   condition_noone_else_has_trump()
              || !condition_has_trump())
         );
}


auto PictureStartWithHighestColor::card_to_play(Trick const& trick) -> Card
{
  auto const color = color_to_start();
  if (color == Card::nocardcolor) {
    rationale_.add(_("Heuristic::reject::no color found with the highest card"));
    return {};
  }

  auto const card = hand().highest_card(color);
  rationale_.add(_("Heuristic::return::start with color %s, highest card is %s", _(color), _(card)));
  return card;
}


auto PictureStartWithHighestColor::color_to_start() -> Card::Color
{
  auto const& hand = this->hand();

  for (auto const color : colors_starting_with(last_color())) {
    if (   hand.contains(color)
        && condition_have_highest_card(color)) {
      rationale_.add(_("Heuristic::reject::do not have the highest card of %s", _(color)));
      return color;
    }
  }
  return Card::nocardcolor;
}


auto PictureStartWithHighestColor::last_color() -> Card::Color
{
  auto const& tricks = game().tricks();
  if (tricks.current_no() == 0)
    return Card::Color::nocardcolor;
  return tricks.trick(tricks.current_no() - 1).startcard().tcolor();
}


auto PictureStartWithHighestColor::condition_have_highest_card(Card::Color const color) -> bool
{
  auto const& hand = this->hand();
  auto const& cards_information = this->cards_information();
  if (!cards_information.remaining_others(color)) {
    rationale_.add(_("Heuristic::condition::the other players do not have %s", _(color)));
    return true;
  }

  auto const lhs = hand.highest_card(color);
  auto const rhs = cards_information.highest_remaining_card_of_others(color);
  if (lhs.is_jabbed_by(rhs)) {
    rationale_.add(_("Heuristic::condition::have highest card of %s", _(color)));
    return false;
  }

  rationale_.add(_("Heuristic::condition::do not have the highest card of %s", _(color)));
  return true;
}

} // namespace Heuristics
