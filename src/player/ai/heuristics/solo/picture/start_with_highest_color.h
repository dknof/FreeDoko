/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../../../heuristic.h"

namespace Heuristics {

/** Play the highest color card in game if no player has trump and no player is higher then the card
 **/
class PictureStartWithHighestColor : public Heuristic {
public:
  static auto is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group) -> bool;

public:
  explicit PictureStartWithHighestColor(Ai const& ai);
  PictureStartWithHighestColor(PictureStartWithHighestColor const&) = delete;
  auto operator=(PictureStartWithHighestColor const&)               = delete;

  ~PictureStartWithHighestColor() override;

  auto conditions_met(Trick const& trick) -> bool override;
  auto card_to_play(Trick const& trick)   -> Card override;

private:
  Card::Color last_color();
  auto condition_have_highest_card(Card::Color color) -> bool;

  Card::Color color_to_start();
};
} // namespace Heuristics
