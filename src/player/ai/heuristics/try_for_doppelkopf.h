/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../heuristic.h"

namespace Heuristics {

/** try to get a doppelkopf for the own team
 **/
class TryForDoppelkopf : public Heuristic {
public:
static bool is_valid(GameTypeGroup game_type, PlayerTypeGroup player_group);

public:
explicit TryForDoppelkopf(Ai const& ai);
TryForDoppelkopf(TryForDoppelkopf const&) = delete;
TryForDoppelkopf& operator=(TryForDoppelkopf const&) = delete;

~TryForDoppelkopf() override;

bool conditions_met(Trick const& trick) override;
Card card_to_play(Trick const& trick) override;

private:
bool condition_last_player_has_10_points(Trick const& trick);
bool condition_last_player_must_play_10_points(Trick const& trick);
bool condition_can_announce_no_120();
}; // class Heuristic::TryForDoppelkopf : public Heuristic
} // namespace Heuristics
