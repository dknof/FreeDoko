/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "valid_card.h"

#include "../ai.h"

namespace Heuristics {

/** @return  whether this heuristic is valid for the gametype group and the player group
 **/
bool ValidCard::is_valid(GameTypeGroup const game_type,
                         PlayerTypeGroup const player_group)
{
  return true;
}

/** constructor
 **/
ValidCard::ValidCard(Ai const& ai) :
  Heuristic(ai, Aiconfig::Heuristic::valid_card)
{ }

/** destructor
 **/
ValidCard::~ValidCard() = default;

/** @return  whether the conditions are met
 **/
bool
ValidCard::conditions_met(Trick const& trick)
{
  return true;
}

/** @return   card to play
 **/
Card
ValidCard::card_to_play(Trick const& trick)
{
  auto const& hand = this->hand();
  for (auto const& card : hand) {
    if (trick.isvalid(card, hand)) {
      rationale_.add(_("Heuristic::return::first valid card: %s", _(card)));
      return card;
    }
  }
  DEBUG_ASSERTION(false,
                  "Heuristic::ValidCard::get_card(trick)\n"
                  "  no valid card found.\n"
                  "Hand:\n"
                  << hand
                  << "Trick:\n"
                  << trick);
  return {};
}

} // namespace Heuristics
