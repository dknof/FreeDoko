/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../../utils/container_algorithm.h"

class Rationale {
public:
  Rationale() = default;
  Rationale(string const text) : text_({text}) { }
  Rationale(Rationale const&) = default;
  auto operator=(Rationale const&) -> Rationale& = default;
  auto operator=(string text)      -> Rationale&
  { text_.emplace_back(text); return *this; }

  operator string() const
  { return text(); }

  auto empty() const -> bool
  { return this->text_.empty(); }

  explicit operator bool() const
  { return !this->empty(); }

  auto text() const -> string
  {
    string text;
    for (auto const& t : text_) {
      text += "– ";
      text += t;
      text += '\n';
    }
    return text;
  }

  void clear()
  { this->text_.clear(); }

  void add(string const text)
  {
    if (text.empty())
      return ;
    if (contains(text_, text))
      return ;
    text_.emplace_back(text);
  }
private:
  vector<string> text_;
};

inline auto operator<<(ostream& ostr, Rationale const& reason) -> ostream&
{ return (ostr << reason.text()); }
inline auto operator!=(Rationale const& lhs, Rationale const& rhs) -> bool
{ return lhs.text() != rhs.text(); }
