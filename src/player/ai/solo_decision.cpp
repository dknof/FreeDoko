/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "solo_decision.h"
#include "ai.h"
#include "../player.h"
#include "../../party/party.h"
#include "../../party/rule.h"

#include "solo_decision/meatless.h"
#include "solo_decision/single_picture.h"
#include "solo_decision/double_picture.h"
#include "solo_decision/koehler.h"
#include "solo_decision/color.h"


auto SoloDecision::create(GameType const game_type,
                          Ai const& ai) -> unique_ptr<SoloDecision>
{
  using namespace SoloDecisions;
  switch (game_type) {
  case GameType::normal:
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::thrown_richness:
  case GameType::fox_highest_trump:
  case GameType::redistribute:
  case GameType::poverty:
  case GameType::marriage:
  case GameType::marriage_solo:
  case GameType::marriage_silent:
    return {};
  case GameType::solo_meatless:
    return make_unique<Meatless>(ai);
  case GameType::solo_jack:
    return make_unique<SinglePicture>(ai, Card::jack);
  case GameType::solo_queen:
    return make_unique<SinglePicture>(ai, Card::queen);
  case GameType::solo_king:
    return make_unique<SinglePicture>(ai, Card::king);
  case GameType::solo_queen_jack:
    return make_unique<DoublePicture>(ai, Card::queen, Card::jack);
  case GameType::solo_king_jack:
    return make_unique<DoublePicture>(ai, Card::king, Card::jack);
  case GameType::solo_king_queen:
    return make_unique<DoublePicture>(ai, Card::king, Card::queen);
  case GameType::solo_koehler:
    return make_unique<Koehler>(ai);
  case GameType::solo_club:
    return make_unique<Color>(ai, Card::club);
  case GameType::solo_spade:
    return make_unique<Color>(ai, Card::spade);
  case GameType::solo_heart:
    return make_unique<Color>(ai, Card::heart);
  case GameType::solo_diamond:
    return make_unique<Color>(ai, Card::diamond);
  }
  return {};
}


SoloDecision::SoloDecision(Ai const& ai, GameType const game_type) :
  Decision(ai),
  game_type_(game_type)
{ }


SoloDecision::~SoloDecision() = default;


auto SoloDecision::game_type() const -> GameType
{
  return game_type_;
}


auto SoloDecision::is_duty_solo() const -> bool
{
  auto const game_type = this->game_type();
  auto const& party    = this->party(); // cppcheck-suppress shadowVariable
  auto const& player   = this->player();
  if (   is_color_solo(game_type)
      && party.remaining_duty_color_soli(player)) {
    return true;
  }

  if (   is_picture_solo(game_type)
      && party.remaining_duty_picture_soli(player)) {
    return true;
  }

  if (party.remaining_duty_free_soli(player)) {
    return true;
  }

  return false;
}


auto SoloDecision::is_startplayer() const -> bool
{
  if (player() == game().startplayer()) {
    //rationale_.add(_("SoloDecision::condition::startplayer"));
    return true;
  }

  if (is_duty_solo()) {
    //rationale_.add(_("SoloDecision::condition::duty solo, so startplayer"));
    return true;
  }

  if (game().rule(Rule::Type::lustsolo_player_leads)) {
    //rationale_.add(_("SoloDecision::condition::lust solo, so startplayer according to the rules"));
    return true;
  }

  //rationale_.add(_("SoloDecision::condition::no startplayer"));
  return false;
}
