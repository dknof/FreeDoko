/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "color.h"

#include "../../../game/game.h"
#include "../../../party/rule.h"

namespace SoloDecisions {

Points constexpr points_for_swine                      = 24;
Points constexpr points_for_hyperswine                 = 15;
Points constexpr points_for_dulle                      = 25;
Points constexpr points_for_queen                      = 18;
Points constexpr points_for_blank_colors_with_nines    = 16;
Points constexpr points_for_blank_colors_without_nines = 20;
Points constexpr points_for_color_ace_with_nines       = 20;
Points constexpr points_for_color_ace_without_nines    = 27;


Color::Color(Ai const& ai,
             Card::Color const color) :
  SoloDecision(ai, solo(color)),
  color_(color)
{ }


Color::~Color() = default;


auto Color::color() const -> Card::Color
{
  return color_;
}


auto Color::estimated_points() -> Points
{
  return (  estimated_points_for_special_cards()
          + estimated_points_for_queens()
          + estimated_points_for_color_aces()
          + estimated_points_for_blank_colors()
          + estimated_points_for_color_cards()
          + modifier_for_trumps());
}


auto Color::estimated_points_for_special_cards() -> Points
{
  Points points = 0;

  auto const& rule = this->rule();
  auto const color = this->color();
  auto const& hand = this->hand();

  if (rule(Rule::Type::swines_in_solo)) {
    auto const swine = Card(color, Card::ace);
    if (hand.count(swine) == 2) {
      Points p = 2 * points_for_swine;
      rationale_.add(_("SoloDecision::information::%s::swines: %i points", _(game_type()), p));
      points += p;
      if (rule(Rule::Type::hyperswines_in_solo)) {
        auto const hyperswine_value = (rule(Rule::Type::with_nines)
                                       ? Card::nine
                                       : Card::king);
        auto const hyperswine = Card(color, hyperswine_value);
        if (hand.count(hyperswine) == 2)  {
          Points const p = 2 * points_for_hyperswine; // cppcheck-suppress shadowVariable
          rationale_.add(_("SoloDecision::information::%s::hyperswines: %i points", _(game_type()), p));
          points += p;

        }
      }
    }
  }

  if (rule(Rule::Type::dullen)) {
    Points const p = (hand.count(Card::dulle) - (rule(Rule::Type::number_of_same_cards) - hand.count(Card::dulle))) * points_for_dulle ;
    rationale_.add(_("SoloDecision::information::%s::dullen: %i points", _(game_type()), p));
    points += p;
  }
  return points;
}


auto Color::estimated_points_for_queens() -> Points
{
  auto const& hand = this->hand();
  auto const& rule = this->rule();

  auto const queens = hand.count(Card::queen);
  int n = queens;
  if (rule(Rule::Type::dullen))
    n -= 2 - hand.count(Card::dulle);
  n -= 2 - hand.count(Card::club_queen);
  n -= 2 - hand.count(Card::spade_queen);
  Points points = n * points_for_queen;
  rationale_.add(_("SoloDecision::information::%s::%u queens: %i points", _(game_type()), queens, points));

  return points;
}


auto Color::estimated_points_for_blank_colors() -> Points
{
  int n = 0;
  auto const& hand = this->hand();
  auto const& rule = this->rule();
  for (auto const c : rule.card_colors()) {
    if (c == color())
      continue;

    if (!hand_contains(c))
      n += 1;
  }
  if (n == 0)
    return 0;

  unsigned const blank_colors = n;

  Points points = 0;
  if (rule(Rule::Type::with_nines)) {
    points += n * points_for_blank_colors_with_nines;
  } else {
    points += n * points_for_blank_colors_without_nines;
  }

  auto const trump_ace = Card(color(), Card::ace);
  auto const trump_ace_count = hand.count(trump_ace);
  if (!(   rule(Rule::Type::swines_in_solo)
        && hand.count(trump_ace) == 2)) {
    points += 11 * min(static_cast<unsigned>(n), trump_ace_count);
    n -= trump_ace_count;
  }

  if (n > 0) {
    auto const trump_ten = Card(color(), Card::ten);
    if (!(   rule(Rule::Type::dullen)
          && trump_ten == Card::dulle)) {
      auto const trump_ten_count = hand.count(trump_ten);
      points += 10 * min(static_cast<unsigned>(n), trump_ten_count);
      n -= trump_ten_count;
    }
  }
  (void)n;

  rationale_.add(_("SoloDecision::information::%s::%u blank colors: %i points", _(game_type()), blank_colors, points));
  return points;
}


auto Color::hand_contains(Card::Color const color) const -> bool
{
  auto const& hand = this->hand();
  for (auto const card : {Card(color, Card::ace),
       Card(color, Card::king),
       Card(color, Card::nine)}) {
    if (hand.contains(card))
      return true;
  }
  auto const ten = Card(color, Card::ten);
  if (   hand.contains(ten)
      && !(   rule()(Rule::Type::dullen)
           && ten == Card::dulle))
    return true;

  return false;
}


auto Color::estimated_points_for_color_aces() -> Points
{
  Points points = 0;
  for (auto const c : rule().card_colors()) {
    if (c == color())
      continue;

    points += (rule()(Rule::Type::with_nines)
               ? estimated_points_for_color_aces_with_nines(c)
               : estimated_points_for_color_aces_without_nines(c));
  }

  return points;
}


auto Color::estimated_points_for_color_aces_with_nines(Card::Color const color) -> Points
{
  auto const& hand = this->hand();

  if (!hand.contains(Card(color, Card::ace)))
    return 0;
  if (   hand.count(color, game_type()) == 1
      && !is_startplayer())
    return 0;
  if (hand.count(color, game_type()) > 3)
    return 0;

  Points points = points_for_color_ace_with_nines;
  rationale_.add(_("SoloDecision::information::%s::ace in %s: %i points", _(game_type()), _(color), points));
  return points;
}


auto Color::estimated_points_for_color_aces_without_nines(Card::Color const color) -> Points
{
  auto const& hand = this->hand();

  if (!hand.contains(Card(color, Card::ace)))
    return 0;
  if (   hand.count(color, game_type()) == 1
      && !is_startplayer())
    return 0;
  if (hand.count(color, game_type()) > 2)
    return 0;
  if (rule()(Rule::Type::dullen)
      && color == Card::heart
      && hand.count(color, game_type()) > 1)
    return 0;

  Points points = points_for_color_ace_without_nines;
  rationale_.add(_("SoloDecision::information::%s::ace in %s: %i points", _(game_type()), _(color), points));
  return points;
}


auto Color::estimated_points_for_color_cards() -> Points
{
  Points points = 0;
  for (auto const c : rule().card_colors()) {
    if (c == color())
      continue;

    points -= 20 * hand().count(c, game_type());
    int cards_of_others = (rule()(Rule::Type::with_nines) ? 6 : 4);
    cards_of_others -= hand().count(c, game_type());
    if (rule()(Rule::Type::dullen)
        && c == Card::heart) {
      cards_of_others -= 2;
    }
    if (hand().contains(c, Card::ace)
        && cards_of_others >= 4 + (cautious() ? 1 : 0) - (chancy() ? 1 : 0)) {
      points += 20;
    }
  }
  rationale_.add(_("SoloDecision::information::%s::%i points for color cards", _(game_type()), points));
  return points;
}


auto Color::modifier_for_trumps() -> Points
{
  auto const& hand = this->hand();
  Points points = hand.count(Card::queen);
  points += hand.count(Card::jack);
  points += hand.count(color()) - 2;
  if (   rule()(Rule::Type::dullen)
      && color() != Card::heart)
    points += hand.count(Card::dulle);

  return 2 * points;
}

} // namespace SoloDecisions
