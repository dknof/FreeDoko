/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "double_picture.h"

#include "../../../game/game.h"

namespace SoloDecisions {


DoublePicture::DoublePicture(Ai const& ai,
                             Card::Value const picture1,
                             Card::Value const picture2) :
  Picture(ai, picture1, picture2)
{ }


DoublePicture::~DoublePicture() = default;


auto DoublePicture::picture1() const -> Card::Value
{
  return picture_[0];
}


auto DoublePicture::picture2() const -> Card::Value
{
  return picture_[1];
}


auto DoublePicture::estimated_points() -> Points
{
  return heuristic_trumps_and_aces();
}


auto DoublePicture::heuristic_trumps_and_aces() -> Points
{
  auto const trumps = count_trumps();
  auto const aces = count_aces_and_tens() - (!is_startplayer() ? count_blank_aces() : 0);
  auto const tricks = game().tricks().max_size();
  if (trumps < 8) {
    rationale_.add(_("SoloDecision::information::%s::%u trumps, less then %u", _(game_type()), trumps, 8u));
    return 0;
  }
  if (trumps + aces < tricks - 2) {
    rationale_.add(_("SoloDecision::information::%s::%u trumps + %u aces, less then %u", _(game_type()), trumps, aces, tricks - 2));
    return 0;
  }
  if (!enough_good_trumps()) {
    rationale_.add(_("SoloDecision::information::%s::not enough good trumps", _(game_type())));
    return 0;
  }
  Points const points =  min(240u, 121 + (trumps + aces - (tricks - 3)) * 30);
  rationale_.add(_("SoloDecision::information::%s::%u trumps + %u aces: %i points", _(game_type()), trumps, aces, points));
  return points;
}


auto DoublePicture::enough_good_trumps() const -> bool
{
  auto const& hand = this->hand();
  auto const club = hand.count(Card::club, picture1());
  if (club == 2)
    return true;
  auto const spade = hand.count(Card::spade, picture1());
  if (  club + spade >= 3)
    return true;
  auto const tricks = game().tricks().max_size();
  if (   club + spade == 2
      && tricks - (count_trumps() + count_aces_and_tens()) <= 3)
    return true;
  return false;
}

} // namespace SoloDecisions
