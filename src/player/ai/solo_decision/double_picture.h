/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "picture.h"

namespace SoloDecisions {
/** Decision for double picture solo
 **/
class DoublePicture : public Picture {
public:
  DoublePicture(Ai const& ai, Card::Value picture1, Card::Value picture2);
  DoublePicture(DoublePicture const&) = delete;
  DoublePicture& operator=(DoublePicture const&) = delete;

  ~DoublePicture() override;

  auto picture1() const -> Card::Value;
  auto picture2() const -> Card::Value;

  auto estimated_points() -> Points override;

private: // Hilfsfunktionen
  auto heuristic_trumps_and_aces() -> Points;

  auto enough_good_trumps() const -> bool;
};
} // namespace SoloDecisions
