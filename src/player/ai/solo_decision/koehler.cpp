/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "koehler.h"

#include "../../../game/game.h"

namespace SoloDecisions {

Koehler::Koehler(Ai const& ai) :
  Picture(ai, Card::king, Card::queen, Card::jack)
{ }


Koehler::~Koehler() = default;


auto Koehler::estimated_points() -> Points
{
  return heuristic_trumps_and_aces();
}


auto Koehler::heuristic_trumps_and_aces() -> Points
{
  auto const trumps = count_trumps();
  auto const aces = count_aces() - (!is_startplayer() ? count_blank_aces() : 0);
  auto const tricks = game().tricks().max_size();
  if (trumps < tricks - 3) {
    rationale_.add(_("SoloDecision::information::%s::%u trumps, less then %u", _(game_type()), trumps, tricks - 3));
    return 0;
  }
  if (trumps + aces < tricks - 2) {
    rationale_.add(_("SoloDecision::information::%s::%u trumps + %u aces, less then %u", _(game_type()), trumps, aces, tricks - 2));
    return 0;
  }
  if (!enough_good_trumps()) {
    rationale_.add(_("SoloDecision::information::%s::not enough good trumps", _(game_type())));
    return 0;
  }
  Points const points =  min(240u, 121 + (trumps + aces - (tricks - 3)) * 30);
  rationale_.add(_("SoloDecision::information::%s::%u trumps + %u aces: %i points", _(game_type()), trumps, aces, points));
  return points;
}


auto Koehler::enough_good_trumps() const -> bool
{
  auto const tricks = game().tricks().max_size();
  auto const& hand  = this->hand();
  auto const trumps = hand.count(Card::trump);
  auto const club   = hand.count(Card::club_king);
  auto const spade  = hand.count(Card::spade_king);
  if (   club  == 2
      && spade >= 1)
    return true;
  if (   club == 2
      && tricks - trumps <= 3)
    return true;
  if (   club + spade >= 3
      && tricks - trumps <= 3)
    return true;
  if (   club + spade == 2
      && tricks - trumps <= 2)
    return true;
  return false;
}

} // namespace SoloDecisions
