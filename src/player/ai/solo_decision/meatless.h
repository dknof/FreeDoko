/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../solo_decision.h"

class HandCards;

namespace SoloDecisions {
/** Decision for meatless
 **/
class Meatless : public SoloDecision {
public:
  explicit Meatless(Ai const& ai);
  Meatless(Meatless const&) = delete;
  Meatless& operator=(Meatless const&) = delete;

  ~Meatless() override;

  auto estimated_points() -> Points override;

protected: // Hilfsfunktionen
  auto lower_points_bound() -> Points;

  auto can_jab_each_color()       -> bool;
  auto can_jab(Card::Color color) -> bool;
  auto loosing_cards(Card::Color color) -> HandCards;
  auto sum_lowest_points(Card::Color color, unsigned cardno) -> unsigned;
};
} // namespace SoloDecisions
