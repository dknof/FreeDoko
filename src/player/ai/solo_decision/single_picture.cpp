/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "single_picture.h"

#include "../../../party/rule.h"
#include "../../../game/game.h"

namespace SoloDecisions {

SinglePicture::SinglePicture(Ai const& ai,
                                           Card::Value const picture) :
  Picture(ai, picture)
{ }


SinglePicture::~SinglePicture() = default;


auto SinglePicture::picture() const -> Card::Value
{
  return picture_[0];
}


auto SinglePicture::estimated_points() -> Points
{
  if (!enough_trumps())
    return 0;
  return max({lower_points_bound(),
             heuristic_winning_tricks(),
             heuristic_loosing_tricks(),
             heuristic_trumps_and_aces()
             });
}


auto SinglePicture::enough_trumps() -> bool
{
  auto const trumps = count_trumps();
  auto const& hand = this->hand();
  if (trumps >= 5)
    return true;
  if (   trumps == 4
      && (   hand.contains(Card::club, picture())
          || hand.contains(Card::spade, picture()) ))
    return true;
  if (   trumps == 3
      && hand.contains(Card::club, picture())
      && hand.count(Card::club, picture()) + hand.count(Card::spade, picture()) >= 2)
    return true;
  return false;
}

auto SinglePicture::lower_points_bound() -> Points
{
  unsigned loosing_points = loosing_trump_tricks() * 24;
  for (auto const color : rule().card_colors()) {
    auto const n = loosing_tricks(color);
    rationale_.add(_("SoloDecision::information::%s::%u loosing tricks in %s", _(game_type()), n, _(color)));
    loosing_points += 25 * n + sum_lowest_points(color, n);
  }
  auto const trumps = count_trumps();
  if (trumps < 5) {
    rationale_.add(_("SoloDecision::information::%s::(5 - %u trumps) * 20", _(game_type()), trumps));
    loosing_points += (5 - trumps) * 20;
  }
  rationale_.add(_("SoloDecision::information::%s::%u loosing points", _(game_type()), loosing_points));
  return 240 - min(240u, loosing_points);
}


auto SinglePicture::heuristic_winning_tricks() -> Points
{
  auto const aces = (count_aces_and_tens()
                     - (is_startplayer()
                        ? 0
                        : count_blank_aces()));
  auto const trumps = count_trumps();
  if (trumps < 5) {
    rationale_.add(_("SoloDecision::information::%s::%u trumps, less then %u", _(game_type()), trumps, 5u));
    return 0;
  }
  Points const points = 3 * 3 * winning_trump_tricks() + 16 * aces;
  rationale_.add(_("SoloDecision::information::%s::%u winning trump tricks + %u aces - %u blank aces: %i points", _(game_type()), winning_trump_tricks(), count_aces_and_tens(), is_startplayer() ? 0 : count_blank_aces(), points));
  return points;
}


auto SinglePicture::heuristic_loosing_tricks() -> Points
{
  auto const trumps = count_trumps();
  auto const aces = (count_aces_and_tens()
                     - (is_startplayer()
                        ? 0
                        : count_blank_aces()));
  auto const tricks = game().tricks().max_size();
  auto tricks_won = aces;
  auto const& hand = this->hand();
  if (trumps >= 5) {
    tricks_won += trumps;
    if (trumps < 7)
      tricks_won -= 2 - hand.count(Card::club, picture());
  } else {
    tricks_won -= (8 - trumps) / 2;
  }
  Points const points = 240 - 30 * static_cast<int>(tricks - tricks_won);
  rationale_.add(_("SoloDecision::information::%s::%u trumps + %u aces - %u blank aces: %i points", _(game_type()), count_trumps(), count_aces_and_tens(), is_startplayer() ? 0 : count_blank_aces(), points));
  return points;
}


auto SinglePicture::heuristic_trumps_and_aces() -> Points
{
  auto const trumps = count_trumps();
  auto const aces = (count_aces_and_tens()
                     - (is_startplayer()
                        ? 0
                        : count_blank_aces()));
  auto const tricks = game().tricks().max_size();
  if (   (trumps >= 3 && trumps < 5)
      || trumps + aces < tricks - 4) {
    unsigned high_trumps = hand().count(Card::club, picture());
    if (high_trumps == 2)
      high_trumps += hand().count(Card::spade, picture());
    if (high_trumps == 4)
      high_trumps += hand().count(Card::heart, picture());
    if (high_trumps == 6)
      high_trumps += hand().count(Card::diamond, picture());
    Points const points =  min(240u, high_trumps * 6 + aces * 16);
    rationale_.add(_("SoloDecision::information::%s::%u high trumps * %u + %u aces * 16: %i points", _(game_type()), high_trumps, static_cast<unsigned>(3 * picture()), aces, points));
    return points;
  }
  if (!enough_good_trumps()) {
    rationale_.add(_("SoloDecision::information::%s::not enough good trumps", _(game_type())));
    return 0;
  }
  if (trumps < 4) {
    rationale_.add(_("SoloDecision::information::%s::not enough trumps", _(game_type())));
    return 0;
  }
  Points const points =  min(240u, 121 + (trumps + aces - (tricks - 3)) * 30);
  rationale_.add(_("SoloDecision::information::%s::%u trumps + %u aces: %i points", _(game_type()), trumps, aces, points));
  return points;
}


auto SinglePicture::enough_good_trumps() const -> bool
{
  auto const& hand = this->hand();
  auto const club = hand.count(Card::club, picture());
  if (club == 2)
    return true;
  auto const spade = hand.count(Card::spade, picture());
  if (  club + spade >= 3)
    return true;
  auto const tricks = game().tricks().max_size();
  if (   club + spade == 2
      && tricks - (count_trumps() + count_aces_and_tens()) <= 3)
    return true;
  return false;
}


auto SinglePicture::loosing_trump_tricks() const -> unsigned
{
  auto const& hand = this->hand();
  auto const trumps = hand.count(picture());
  auto const opposite_trumps = 8 - trumps;
  auto const clubs  = hand.count(Card::club,  picture());
  auto const spades = hand.count(Card::spade, picture());
  auto const hearts = hand.count(Card::heart, picture());
  if (opposite_trumps <= 1)
    return 0;
  if (clubs == 2 && opposite_trumps <= 2)
    return 0;
  if (clubs == 2 && spades == 2)
    return 0;
  if (clubs == 2 && spades == 1 && opposite_trumps <= 3)
    return 0;
  if (clubs == 2 && spades == 1)
    return 1;
  if (clubs == 2 && spades == 0 && opposite_trumps <= 3)
    return 0;
  if (clubs == 2 && spades == 0 && hearts == 2)
    return 1;
  if (clubs == 2 && spades == 0 && trumps >= opposite_trumps)
    return 1;
  if (clubs == 2 && spades == 0)
    return 2;
  DEBUG_ASSERTION(clubs < 2,
                  "SoloDecision::SinglePicture(" << picture() << ")::loosing_trump_tricks()\n"
                  "  case is missing: "
                  << "trumps = " << trumps << ", "
                  << "clubs  = " << clubs << ", "
                  << "spades = " << spades << ", "
                  << "hearts = " << hearts << "\n");

  if (clubs == 1 && spades == 2 && hearts >= 1)
    return 1;
  if (clubs == 1 && spades == 2)
    return 2;
  if (clubs == 1 && spades == 1 && hearts == 2)
    return 1;
  if (clubs == 1 && spades == 1 && trumps >= opposite_trumps)
    return 2;
  if (clubs == 1 && spades == 1 && opposite_trumps >= 6)
    return 3;
  if (clubs == 1 && spades == 1)
    return 2;
  if (clubs == 1 && spades == 0 && opposite_trumps >= 7)
    return 3;
  if (clubs == 1 && spades == 0 && opposite_trumps >= 5)
    return 2;
  if (clubs == 1 && spades == 0)
    return 1;
  DEBUG_ASSERTION(clubs < 1,
                  "SoloDecision::SinglePicture(" << picture() << ")::loosing_trump_tricks()\n"
                  "  case is missing: "
                  << "trumps = " << trumps << ", "
                  << "clubs  = " << clubs << ", "
                  << "spades = " << spades << ", "
                  << "hearts = " << hearts << "\n");

  if (clubs == 0 && spades == 0)
    return 4;
  if (clubs == 0 && spades == 2 && trumps >= 5)
    return 2;
  if (clubs == 0 && spades == 2 && trumps >= 4)
    return 3;
  if (clubs == 0 && spades == 2)
    return 4;
  if (clubs == 0 && spades == 1 && trumps >= 5)
    return 3;
  if (clubs == 0 && spades == 1)
    return 4;
  if (clubs == 0 && spades == 0)
    return 4;
  DEBUG_ASSERTION(false,
                  "SoloDecision::SinglePicture(" << picture() << ")::loosing_trump_tricks()\n"
                  "  case is missing: "
                  << "trumps = " << trumps << ", "
                  << "clubs  = " << clubs << ", "
                  << "spades = " << spades << ", "
                  << "hearts = " << hearts << "\n");

  return 5;
}


auto SinglePicture::winning_trump_tricks() const -> unsigned
{
  auto const& hand = this->hand();
  auto const trumps = hand.count(picture());
  auto const opposite_trumps = 8 - trumps;
  auto const clubs  = hand.count(Card::club,  picture());
  auto const spades = hand.count(Card::spade, picture());
  auto const hearts = hand.count(Card::heart, picture());
  if (opposite_trumps <= 1)
    return trumps;
  if (clubs == 2 && opposite_trumps <= 2)
    return trumps;
  if (clubs == 2 && trumps == 2)
    return trumps;
  if (clubs == 2 && spades == 2)
    return trumps;
  if (clubs == 2 && spades == 1 && opposite_trumps <= 3)
    return trumps;
  if (clubs == 2 && spades == 1 && trumps == 3)
    return trumps;
  if (clubs == 2 && spades == 1)
    return trumps - 1;
  if (clubs == 2 && spades == 0 && opposite_trumps <= 3)
    return trumps;
  if (clubs == 2 && spades == 0 && hearts == 2) // 4 gegnerische Trümpfe
    return trumps - 1;
  if (clubs == 2 && spades == 0 && opposite_trumps == 4)
    return 2;
  if (clubs == 2 && spades == 0 && opposite_trumps == 5)
    return 2;
  DEBUG_ASSERTION(clubs < 2,
                  "SoloDecision::SinglePicture(" << picture() << ")::winning_trump_tricks()\n"
                  "  case is missing: "
                  << "trumps = " << trumps << ", "
                  << "clubs  = " << clubs << ", "
                  << "spades = " << spades << ", "
                  << "hearts = " << hearts << "\n");

  if (clubs == 1 && trumps == 1)
    return trumps;
  if (clubs == 1 && spades == 2)
    return trumps - 1;
  if (clubs == 1 && spades == 1 && opposite_trumps <= 3)
    return trumps - 1; // optimistic
  if (clubs == 1 && spades == 1 && hearts == 2)
    return trumps - 1; // optimistic
  if (clubs == 1 && spades == 1 && trumps <= 3)
    return 1;
  if (clubs == 1 && spades == 1 && trumps == 4)
    return 2; // optimistic
  if (clubs == 1)
    return 1;
  DEBUG_ASSERTION(clubs < 1,
                  "SoloDecision::SinglePicture(" << picture() << ")::winning_trump_tricks()\n"
                  "  case is missing: "
                  << "trumps = " << trumps << ", "
                  << "clubs  = " << clubs << ", "
                  << "spades = " << spades << ", "
                  << "hearts = " << hearts << "\n");

  if (clubs == 0 && spades == 0)
    return 0;
  if (clubs == 0 && trumps <= 2)
    return 0;
  if (clubs == 0 && spades == 1)
    return trumps - 3;
  if (clubs == 0 && spades == 2 && hearts >= 1)
    return trumps - 2;
  if (clubs == 0 && spades == 2 && hearts == 0)
    return trumps - 3;
  DEBUG_ASSERTION(false,
                  "SoloDecision::SinglePicture(" << picture() << ")::winning_trump_tricks()\n"
                  "  case is missing: "
                  << "trumps = " << trumps << ", "
                  << "clubs  = " << clubs << ", "
                  << "spades = " << spades << ", "
                  << "hearts = " << hearts << "\n");
  return 0;
}


auto SinglePicture::loosing_tricks(Card::Color const color) const -> unsigned
{
  auto cards = hand().cards(color, game_type());
  unsigned loosing_tricks = 0;
  if (cards.empty())
    return 0;
  if (   cards.size() == 1
      && cards[0].value() == Card::ace)
    return (is_startplayer() ? 0 : 1);
  if (   cards.size() == 2
      && cards[0].value() == Card::ace
      && cards[1].value() != Card::ace)
    return 1;

  for (auto const card : all_single_cards(color)) {
    loosing_tricks += 2 - cards.count(card);
    cards.remove_all(card);
    if (loosing_tricks >= cards.size())
      break;
  }
  auto const n = min(2 * (rule()(Rule::Type::number_of_card_values) - 1)
                     - hand().count(color, game_type()),
                     loosing_tricks);
  return n;
}

} // namespace SoloDecisions
