/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "picture.h"

namespace SoloDecisions {
/** Decision for single picture solo
 **/
class SinglePicture : public Picture {
public:
  SinglePicture(Ai const& ai, Card::Value picture);
  SinglePicture(SinglePicture const&) = delete;
  SinglePicture& operator=(SinglePicture const&) = delete;

  ~SinglePicture() override;

  auto picture() const -> Card::Value;

  auto estimated_points() -> Points override;

private: // Hilfsfunktionen
  auto enough_trumps() -> bool;
  auto lower_points_bound() -> Points;
  auto heuristic_winning_tricks() -> Points;
  auto heuristic_loosing_tricks() -> Points;
  auto heuristic_trumps_and_aces() -> Points;

  auto enough_good_trumps() const -> bool;

  auto loosing_trump_tricks() const -> unsigned;
  auto winning_trump_tricks() const -> unsigned;
  auto loosing_tricks(Card::Color color) const -> unsigned;
};
} // namespace SoloDecisions
