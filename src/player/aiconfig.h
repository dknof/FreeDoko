/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "aiconfig_type.h"
#include "aiconfig_heuristic.h"
#include "../card/card.h"
#include "heuristicsmap.h"
#include "rating/rating.h"


#include <map>

class Aiconfig {
public:
  using Difficulty = AiconfigDifficulty;
  using Type = AiconfigType;

  using Heuristic = AiconfigHeuristic;
  static auto Heuristic_from_name(string const name) -> Heuristic
  { return AiconfigHeuristic_from_name(name); }

  struct HeuristicState {
    HeuristicState() = delete;
    // cppcheck-suppress passedByValue
    HeuristicState(Heuristic heuristic, bool active = true) :
      heuristic(heuristic), active(active) { }

    Heuristic heuristic = Heuristic::no_heuristic;
    bool active = false;
  }; // struct HeuristicState

public:
  static auto preset(Difficulty difficulty) -> Aiconfig const&;
  // returns all (interesting) keys for the heuristic configuration
  static auto keys() -> vector<HeuristicsMap::Key> const&;

public:
  friend class Ai;

public:
  Aiconfig();
  Aiconfig(Difficulty difficulty);
  Aiconfig(istream& istr);
  Aiconfig(istream& istr, Difficulty difficulty);
  Aiconfig(Aiconfig const& aiconfig);
  auto operator=(Aiconfig const& aiconfig) -> Aiconfig&;
  virtual ~Aiconfig();

  void reset_to_hardcoded(unsigned no = UINT_MAX);

  void set_to_difficulty(Difficulty difficulty);
  void rule_changed(int type, void const* old_value);

  auto difficulty() const -> Difficulty;
private:
  void update_difficulty();
public:
  // whether the aiconfig is in a (nested) update
  auto in_update() const -> bool;
  // enter a (nested) update
  void enter_update();
  // leave a (nested) update
  void leave_update();
  // leave a (nested) update and set the difficulty manual
  void leave_update(Difficulty difficulty);


  // whether the aiconfig is equal
  auto equal(Aiconfig const& aiconfig) const -> bool;

  auto rating() const -> Rating::Type;
  void set_rating(Rating::Type rating);

  // returns the heuristic states
  auto heuristic_states(HeuristicsMap::Key key) const -> vector<HeuristicState> const&;
  auto heuristic_states(HeuristicsMap::GameTypeGroup gametype_group,
                        HeuristicsMap::PlayerTypeGroup playertype_group) const -> vector<HeuristicState> const&;
private:
  auto heuristic_states(HeuristicsMap::Key key) -> vector<HeuristicState>&;
  auto heuristic_states(HeuristicsMap::GameTypeGroup gametype_group,
                        HeuristicsMap::PlayerTypeGroup playertype_group) -> vector<HeuristicState>&;
public:
  auto heuristics(HeuristicsMap::GameTypeGroup gametype_group,
                  HeuristicsMap::PlayerTypeGroup playertype_group) const -> vector<Heuristic>;
  auto heuristics(Player const& player) const -> vector<Heuristic>;

  // returns the value
  auto value(HeuristicsMap::Key key, Heuristic heuristic) const -> bool;
  auto value(HeuristicsMap::GameTypeGroup gametype_group,
             HeuristicsMap::PlayerTypeGroup playertype_group, Heuristic heuristic) const -> bool;
  auto value(Type::Bool type) const -> bool;
  auto value(Type::Int type)  const -> int;
  auto value(Type::Card type) const -> Card;

  // the minimum and maximum value
  auto min(Type::Int type) const -> int;
  auto max(Type::Int type) const -> int;

  // the valid cards for the type
  auto valid_cards(Type::Card type) const -> vector<Card> const&;

  // sets the value
  auto set(string type, string value) -> bool;

  void set(HeuristicsMap::GameTypeGroup gametype_group,
           HeuristicsMap::PlayerTypeGroup playertype_group,
           Heuristic heuristic,
           string value);
  void move(HeuristicsMap::Key key,
            Heuristic heuristic,
            unsigned pos);
  void set(Heuristic heuristic, string value);
  void set(Type::Bool type, string value);
  void set(Type::Int type, string value);
  void set(Type::Card type, string value);

  void set(HeuristicsMap::Key key,
           Heuristic heuristic,
           bool value);
  void set(HeuristicsMap::GameTypeGroup gametype_group,
           HeuristicsMap::PlayerTypeGroup playertype_group,
           Heuristic heuristic,
           bool value);
  void set_to_default(HeuristicsMap::Key key);
  void set(Heuristic heuristic, bool value);
  void set(Type::Bool type, bool value);
  void set(Type::Int type, int value);
  void set(Type::Card type, Card value);

  auto load(Path path) -> bool;
  auto read(istream& istr) -> bool;
  auto read_heuristics(istream& istr) -> bool;
  auto save(Path path) const -> bool;

  // output of the settings
  void write(ostream& ostr) const;
  void write_full(ostream& ostr) const;

private:
  // initializes the heuristic states
  void init_heuristic_states();
  // fills the heuristic states with all heuristics
  void fill_up_heuristic_states();

  auto difficulty_with_min_differences() const -> Difficulty;
  auto count_differences(Difficulty difficulty) const -> unsigned;
  void write_differences(ostream& ostr, Difficulty difficulty) const;

private:
  Difficulty difficulty_ = Difficulty::standard;
  Rating::Type rating_ = Rating::Type::max;

  map<HeuristicsMap::Key, vector<HeuristicState> > heuristic_states_;

  mutable map<Type::Bool, bool> bool_;
  mutable map<Type::Int, int> int_;
  mutable map<Type::Card, Card> card_;

  // the level of the internal update
  unsigned update_level_ = 0;
};

namespace HeuristicsMap {
using Map = std::map<Key, vector<Aiconfig::Heuristic>>;
} // namespace HeuristicsMap

auto operator==(Aiconfig const& lhs, Aiconfig const& rhs) -> bool;
auto operator!=(Aiconfig const& lhs, Aiconfig const& rhs) -> bool;

auto operator==(Aiconfig::HeuristicState lhs, Aiconfig::HeuristicState rhs) -> bool;

auto operator<<(ostream& ostr, Aiconfig const& aiconfig) -> ostream&;
