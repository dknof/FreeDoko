/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

enum class AiconfigDifficulty {
  custom,
  standard,
  cautious,
  chancy,
  unfair
};
static constexpr auto aiconfig_difficulty_list = array<AiconfigDifficulty, 5> {
  AiconfigDifficulty::custom,
  AiconfigDifficulty::standard,
  AiconfigDifficulty::cautious,
  AiconfigDifficulty::chancy,
  AiconfigDifficulty::unfair
};

struct AiconfigType {
  ENUM_WITH_STATIC_LIST(Bool, bool_list, 0,
                        trusting, // whether the ai is trusting

                        aggressive, // how much the ai will jab tricks

                        cautious,
                        chancy,

                        teams_known,   // whether all teams are known
                        hands_known,   // whether all hands are known
                       ); // enum Bool
  static auto constexpr bool_number = sizeof(bool_list) / sizeof(Bool);
  static auto constexpr bool_first = bool_list[0];
  static auto constexpr bool_last = bool_list[bool_number - 1];

  ENUM_WITH_STATIC_LIST(Int, int_list, hands_known + 1,
                        future_limit, // how many cards to calculate into the future
                        max_sec_wait_for_gametree, // how many seconds maximal wait for gametree

                        limit_throw_fehl, // maximum points for playing a fehl
                        limitqueen, // mininum points per trick for jabbing with a queen
                        limitdulle, // minimum points per trick for jabbing with dullen or higher

                        last_tricks_without_heuristics, // how many tricks at the end of the game no heuristics are used
                        first_trick_for_trump_points_optimization,

                        announcelimit, // points of hand for first announcement 
                        announcelimitdec, // points of hand you can lose between two announcements
                        announceconfig, // Parameter for Playerconfiguration of announcements
                        announcelimitreply, // points of hand for first Reply
                        announceconfigreply, // Decrement for each announcment made, when replying 

                        ); // enum Int
  static auto constexpr int_number = sizeof(int_list) / sizeof(Int);
  static auto constexpr int_first = int_list[0];
  static auto constexpr int_last = int_list[int_number - 1];

  ENUM_WITH_STATIC_LIST(Card, card_list, announceconfigreply + 1,
                        limitthrowing, // minimum team mate card for not over jabbing

                        trumplimit_solocolor,
                        trumplimit_solojack,
                        trumplimit_soloqueen,
                        trumplimit_soloking,
                        trumplimit_solojackking,
                        trumplimit_solojackqueen,
                        trumplimit_soloqueenking,
                        trumplimit_solokoehler,
                        trumplimit_meatless,
                        trumplimit_normal,

                        lowest_trumplimit_solocolor,
                        lowest_trumplimit_solojack,
                        lowest_trumplimit_soloqueen,
                        lowest_trumplimit_soloking,
                        lowest_trumplimit_solojackking,
                        lowest_trumplimit_solojackqueen,
                        lowest_trumplimit_soloqueenking,
                        lowest_trumplimit_solokoehler,
                        lowest_trumplimit_meatless,
                        lowest_trumplimit_normal,

                        limithigh, // minimum team mate card for throwing a ten or ace into this trick
                        ); // enum Card
  static auto constexpr card_number = sizeof(card_list) / sizeof(Card);
  static auto constexpr card_first = card_list[0];
  static auto constexpr card_last = card_list[card_number - 1];
}; // struct Type


auto to_string(AiconfigDifficulty difficulty) -> string;
auto gettext(AiconfigDifficulty difficulty)   -> string;
auto operator<<(ostream& ostr, AiconfigDifficulty difficulty) -> ostream&;
auto aiconfig_difficulty_from_string(string name) -> AiconfigDifficulty;

auto to_string(AiconfigType::Bool type) -> string;
auto gettext(AiconfigType::Bool type)   -> string;
auto operator<<(ostream& ostr, AiconfigType::Bool type) -> ostream&;

auto to_string(AiconfigType::Int type) -> string;
auto gettext(AiconfigType::Int type)   -> string;
auto operator<<(ostream& ostr, AiconfigType::Int type) -> ostream&;

auto to_string(AiconfigType::Card type) -> string;
auto gettext(AiconfigType::Card type)   -> string;
auto operator<<(ostream& ostr, AiconfigType::Card type) -> ostream&;
