/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "cards_information.h"

/**
 ** Information of the cards a specific player can have
 **
 ** @author   Diether Knof
 **
 ** @todo     join the vector<Card> and vector<Card> functions (by begin, end iterators?)
 ** @todo     open shifted fox
 **/
class CardsInformation::OfPlayer {
  friend bool operator==(OfPlayer const& lhs, OfPlayer const& rhs);
  friend class CardsInformation;

  public:
  OfPlayer(CardsInformation& cards_information, unsigned playerno);
  OfPlayer(OfPlayer const& of_player);
  ~OfPlayer();

  // reset all informations
  void reset();


  // writes the information in 'ostr'
  void write(ostream& ostr) const;

  CardsInformation const& cards_information() const;
  CardsInformation& cards_information();
  unsigned playerno() const;

  Game const& game() const;
  Player const& player() const;

  void set_cards_information(CardsInformation& cards_information);

  bool all_known() const;

  unsigned played(Card card) const;
  unsigned played(Card::TColor tcolor) const;
  unsigned must_have(Card card) const;
  unsigned must_have(Card::TColor tcolor) const;
  unsigned must_have_trump() const;
  unsigned can_have(Card card) const;
  unsigned can_have(Card::TColor tcolor) const;
  unsigned can_have_trump() const;
  bool cannot_have(Card card) const;
  bool does_not_have(Card::TColor tcolor) const;
  unsigned unknown(Card card) const;
  unsigned unknown(Card::TColor tcolor) const;

  int weighting(Card card, bool modify = true) const;


  unsigned played_cards_no() const;
  unsigned remaining_cards_no() const;
  Hand played_cards() const;
  Hand must_have_cards() const;
  Hand cannot_have_cards() const;
  Hand possible_hand() const;
  Hand estimated_hand() const;

  bool is_valid(Hand const& hand) const;
  int weighting(Hand const& hand) const;

  void game_start();
  void card_played(HandCard card, Trick const& trick);

  void weight_played_card(HandCard card, Trick const& trick);

  void add_must_have(Card card, unsigned no = 1);
  void add_must_have(vector<Card> const& cards);
  void add_must_have(Card::TColor tcolor, unsigned no);

  void add_can_have(Card card, unsigned no);
  void add_can_have(vector<Card> const& cards, unsigned no);
  void add_can_only_have(vector<Card> const& cards);
  void add_can_have(Card::TColor tcolor, unsigned no);
  void add_must_exactly_have(Card::TColor tcolor, unsigned no);

  void add_cannot_have(Card card);
  void add_cannot_have(vector<Card> const& cards);
  void add_cannot_have(Card::TColor tcolor);

  void update_information(Card card);
  void update_information(Card::TColor tcolor);
  void update_tcolor_information();
  void update_remaining_cards();

  private:
  // check whether the cards the player can have are the ones he has to have
  void check_can_is_must();
  // check whether the cards the player can have are the ones he has to have
  void check_can_is_must(Card::TColor tcolor);
  // check whether the cards the player has to have are the only ones he can have
  void check_must_is_can();
  bool self_check() const;

  private:
  CardsInformation* cards_information_;
  unsigned playerno_ = UINT_MAX;

  // number of played cards of the player
  CardCounter played_;
  CardCounter must_have_;
  CardCounter can_have_;

  TColorCounter tcolor_played_;
  TColorCounter tcolor_must_have_;
  TColorCounter tcolor_can_have_;

  // weighting for the cards for estimations
  // -100
  mutable map<Card, int> cards_weighting_;
}; // class CardsInformation::OfPlayer

ostream& operator<<(ostream& ostr, CardsInformation::OfPlayer const& of_player);
bool operator==(CardsInformation::OfPlayer const& lhs,
		CardsInformation::OfPlayer const& rhs);
bool operator!=(CardsInformation::OfPlayer const& lhs,
		CardsInformation::OfPlayer const& rhs);
