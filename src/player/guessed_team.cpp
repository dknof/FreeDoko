/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "guessed_team.h"

auto to_guessed(Team team) -> GuessedTeam
{
  switch (team) {
  case Team::noteam:
    return GuessedTeam::noteam;
  case Team::unknown:
    return GuessedTeam::unknown;
  case Team::re:
    return GuessedTeam::re;
  case Team::contra:
    return GuessedTeam::contra;
  }
  return GuessedTeam::unknown;
}

auto surely(Team team) -> GuessedTeam
{
  switch (team) {
  case Team::noteam:
    return GuessedTeam::noteam;
  case Team::unknown:
    return GuessedTeam::unknown;
  case Team::re:
    return GuessedTeam::surely_re;
  case Team::contra:
    return GuessedTeam::surely_contra;
  }
  return GuessedTeam::unknown;
}

auto maybe(Team team) -> GuessedTeam
{
  switch (team) {
  case Team::noteam:
    return GuessedTeam::noteam;
  case Team::re:
    return GuessedTeam::maybe_re;
  case Team::contra:
    return GuessedTeam::maybe_contra;
  case Team::unknown:
    return GuessedTeam::unknown;
  }
  return GuessedTeam::unknown;
}

auto to_real(GuessedTeam const guessed_team) -> Team
{
  switch (guessed_team) {
  case GuessedTeam::noteam:
    return Team::noteam;
  case GuessedTeam::unknown:
    return Team::unknown;
  case GuessedTeam::re:
    return Team::re;
  case GuessedTeam::contra:
    return Team::contra;
  case GuessedTeam::surely_re:
  case GuessedTeam::surely_contra:
  case GuessedTeam::maybe_re:
  case GuessedTeam::maybe_contra:
    return Team::unknown;
  }
  return Team::unknown;
}

auto maybe_to_real(GuessedTeam const guessed_team) -> Team
{
  switch (guessed_team) {
  case GuessedTeam::noteam:
    return Team::noteam;
  case GuessedTeam::unknown:
    return Team::unknown;
  case GuessedTeam::re:
  case GuessedTeam::surely_re:
  case GuessedTeam::maybe_re:
    return Team::re;
  case GuessedTeam::contra:
  case GuessedTeam::surely_contra:
  case GuessedTeam::maybe_contra:
    return Team::contra;
  }
  return Team::unknown;
}

auto to_string(GuessedTeam team) -> string
{
  switch(team) {
  case GuessedTeam::noteam:
    (void)_("Team::no team");
    return "no team";
  case GuessedTeam::unknown:
    (void)_("Team::unknown team");
    return "unknown team";
  case GuessedTeam::re:
    (void)_("Team::re");
    return "re";
  case GuessedTeam::contra:
    (void)_("Team::contra");
    return "contra";
  case GuessedTeam::surely_re:
    (void)_("Team::surely re");
    return "surely re";
  case GuessedTeam::surely_contra:
    (void)_("Team::surely contra");
    return "surely contra";
  case GuessedTeam::maybe_re:
    (void)_("Team::maybe re");
    return "maybe re";
  case GuessedTeam::maybe_contra:
    (void)_("Team::maybe contra");
    return "maybe contra";
  }
  return {};
}

auto short_string(GuessedTeam team) -> string
{
  switch(team) {
  case GuessedTeam::unknown:
    (void)_("Team::?");
    return "?";
  case GuessedTeam::noteam:
    (void)_("Team::-");
    return "-";
  case GuessedTeam::re:
    (void)_("Team::re");
    return "re";
  case GuessedTeam::contra:
    (void)_("Team::contra");
    return "contra";
  case GuessedTeam::surely_re:
    (void)_("Team::*re*");
    return "*re*";
  case GuessedTeam::surely_contra:
    (void)_("Team::*contra*");
    return "*contra*";
  case GuessedTeam::maybe_re:
    (void)_("Team::?re?");
    return "?re?";
  case GuessedTeam::maybe_contra:
    (void)_("Team::?contra?");
    return "?contra?";
  }
  return {};
}

auto gettext(GuessedTeam const team) -> string
{
  return gettext("Team::" + to_string(team));
}

auto operator<<(ostream& ostr, GuessedTeam const team) -> ostream&
{
  ostr << to_string(team);
  return ostr;
}

auto operator>=(GuessedTeam const lhs, GuessedTeam const rhs) -> bool
{
  switch (lhs) {
  case GuessedTeam::re:
    if (rhs == GuessedTeam::re)
      return true;
    [[fallthrough]];
  case GuessedTeam::surely_re:
    if (rhs == GuessedTeam::surely_re)
      return true;
    [[fallthrough]];
  case GuessedTeam::maybe_re:
    if (rhs == GuessedTeam::maybe_re)
      return true;
    return (rhs == GuessedTeam::unknown);
  case GuessedTeam::contra:
    if (rhs == GuessedTeam::contra)
      return true;
    [[fallthrough]];
  case GuessedTeam::surely_contra:
    if (rhs == GuessedTeam::surely_contra)
      return true;
    [[fallthrough]];
  case GuessedTeam::maybe_contra:
    if (rhs == GuessedTeam::maybe_contra)
      return true;
    return (rhs == GuessedTeam::unknown);
  case GuessedTeam::noteam:
  case GuessedTeam::unknown:
    return false;
  }
  return false;
}

auto operator==(Team lhs, GuessedTeam rhs) -> bool
{
  switch (lhs) {
  case Team::unknown:
    return rhs == GuessedTeam::unknown;
  case Team::noteam:
    return rhs == GuessedTeam::noteam;
  case Team::re:
    return rhs == GuessedTeam::re;
  case Team::contra:
    return rhs == GuessedTeam::contra;
  }
  return false;
}

auto operator&(GuessedTeam lhs, GuessedTeam rhs) -> bool
{
  switch (lhs) {
  case GuessedTeam::unknown:
    return rhs == GuessedTeam::unknown;
  case GuessedTeam::noteam:
    return rhs == GuessedTeam::noteam;
  case GuessedTeam::re:
  case GuessedTeam::surely_re:
  case GuessedTeam::maybe_re:
    return (   rhs == GuessedTeam::re
            || rhs == GuessedTeam::surely_re
            || rhs == GuessedTeam::maybe_re);
  case GuessedTeam::contra:
  case GuessedTeam::surely_contra:
  case GuessedTeam::maybe_contra:
    return (   rhs == GuessedTeam::contra
            || rhs == GuessedTeam::surely_contra
            || rhs == GuessedTeam::maybe_contra);
  }
  return false;
}

auto operator&(GuessedTeam lhs, Team rhs) -> bool
{
  switch (lhs) {
  case GuessedTeam::unknown:
    return rhs == Team::unknown;
  case GuessedTeam::noteam:
    return rhs == Team::noteam;
  case GuessedTeam::re:
  case GuessedTeam::surely_re:
  case GuessedTeam::maybe_re:
    return rhs == Team::re;
  case GuessedTeam::contra:
  case GuessedTeam::surely_contra:
  case GuessedTeam::maybe_contra:
    return rhs == Team::contra;
  }
  return false;
}

auto operator&(Team lhs, GuessedTeam rhs) -> bool
{
  return rhs == lhs;
}

auto operator==(GuessedTeam lhs, Team rhs) -> bool
{
  return rhs == lhs;
}

auto surely_equal(GuessedTeam lhs, GuessedTeam rhs) -> bool
{
  switch (lhs) {
  case GuessedTeam::re:
  case GuessedTeam::surely_re:
    if (   rhs == GuessedTeam::re
        || rhs == GuessedTeam::surely_re)
      return true;
    return false;
  case GuessedTeam::contra:
  case GuessedTeam::surely_contra:
    if (   rhs == GuessedTeam::contra
        || rhs == GuessedTeam::surely_contra)
      return true;
    return false;
  case GuessedTeam::maybe_re:
  case GuessedTeam::maybe_contra:
  case GuessedTeam::noteam:
  case GuessedTeam::unknown:
    return false;
  }
  return false;
}

auto maybe_equal(GuessedTeam lhs, GuessedTeam rhs) -> bool
{
  switch (lhs) {
  case GuessedTeam::re:
  case GuessedTeam::surely_re:
  case GuessedTeam::maybe_re:
    return (   rhs == GuessedTeam::re
            || rhs == GuessedTeam::surely_re
            || rhs == GuessedTeam::maybe_re);
  case GuessedTeam::contra:
  case GuessedTeam::surely_contra:
  case GuessedTeam::maybe_contra:
    return (   rhs == GuessedTeam::contra
            || rhs == GuessedTeam::surely_contra
            || rhs == GuessedTeam::maybe_contra);
  case GuessedTeam::noteam:
  case GuessedTeam::unknown:
    return false;
  }
  return false;
}

auto is_real(GuessedTeam const guessed_team) -> bool
{
  switch (guessed_team) {
  case GuessedTeam::re:
  case GuessedTeam::contra:
    return true;
  case GuessedTeam::noteam:
  case GuessedTeam::surely_re:
  case GuessedTeam::surely_contra:
  case GuessedTeam::maybe_re:
  case GuessedTeam::maybe_contra:
  case GuessedTeam::unknown:
    return false;
  }
  return false;
}

auto is_surely(GuessedTeam const guessed_team) -> bool
{
  switch (guessed_team) {
  case GuessedTeam::surely_re:
  case GuessedTeam::surely_contra:
    return true;
  case GuessedTeam::noteam:
  case GuessedTeam::unknown:
  case GuessedTeam::re:
  case GuessedTeam::contra:
  case GuessedTeam::maybe_re:
  case GuessedTeam::maybe_contra:
    return false;
  }
  return false;
}

auto is_maybe(GuessedTeam const guessed_team) -> bool
{
  switch (guessed_team) {
  case GuessedTeam::maybe_re:
  case GuessedTeam::maybe_contra:
    return true;
  case GuessedTeam::noteam:
  case GuessedTeam::unknown:
  case GuessedTeam::re:
  case GuessedTeam::contra:
  case GuessedTeam::surely_re:
  case GuessedTeam::surely_contra:
    return false;
  }
  return false;
}
