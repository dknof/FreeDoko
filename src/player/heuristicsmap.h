/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

class Game;
class Player;

namespace HeuristicsMap {
  enum class GameTypeGroup {
    normal,
    poverty,
    marriage_undetermined,
    marriage_silent,
    solo_meatless,
    soli_color,
    soli_single_picture,
    soli_double_picture,
    solo_koehler,
  }; // enum GameTypeGroup
  constexpr GameTypeGroup game_type_group_all[] = {
    GameTypeGroup::normal,
    GameTypeGroup::poverty,
    GameTypeGroup::marriage_undetermined,
    GameTypeGroup::marriage_silent,
    GameTypeGroup::solo_meatless,
    GameTypeGroup::soli_color,
    GameTypeGroup::soli_single_picture,
    GameTypeGroup::soli_double_picture,
    GameTypeGroup::solo_koehler
  };
  auto group(Game const& game)                       -> GameTypeGroup;
  auto GameTypeGroup_from_string(string const& name) -> GameTypeGroup;

  enum class PlayerTypeGroup {
    special,
    re,
    contra,
  }; // enum PlayerTypeGroup
  constexpr PlayerTypeGroup player_type_group_all[] = {
    PlayerTypeGroup::special,
    PlayerTypeGroup::re,
    PlayerTypeGroup::contra,
  };
  auto group(Player const& player)                     -> PlayerTypeGroup;
  auto PlayerTypeGroup_from_string(string const& name) -> PlayerTypeGroup;

  struct Key {
    constexpr Key() = default;
    explicit Key(Player const& player);
    constexpr Key(GameTypeGroup gametype_group,
                  PlayerTypeGroup playertype_group) :
      gametype_group(gametype_group), playertype_group(playertype_group)
    { }

    GameTypeGroup gametype_group     = GameTypeGroup::normal;
    PlayerTypeGroup playertype_group = PlayerTypeGroup::re;
  }; // class Key

  auto operator==(Key lhs, Key rhs) -> bool;
  auto operator!=(Key lhs, Key rhs) -> bool;
  auto operator< (Key lhs, Key rhs) -> bool;
} // namespace HeuristicsMap

using GameTypeGroup = HeuristicsMap::GameTypeGroup;
using PlayerTypeGroup = HeuristicsMap::PlayerTypeGroup;

auto is_solo(GameTypeGroup game_type)             -> bool;
auto is_picture_solo(GameTypeGroup game_type)     -> bool;
auto is_color_solo(GameTypeGroup game_type)       -> bool;
auto is_with_trump(GameTypeGroup game_type)       -> bool;
auto is_with_trump_color(GameTypeGroup game_type) -> bool;
auto is_with_partner(GameTypeGroup game_type, PlayerTypeGroup player_group) -> bool;
auto is_with_special_points(GameTypeGroup game_type) -> bool;

auto to_string(HeuristicsMap::GameTypeGroup group)   -> string;
auto to_string(HeuristicsMap::PlayerTypeGroup group) -> string;
auto operator<<(ostream& ostr, HeuristicsMap::GameTypeGroup group)   -> ostream&;
auto operator<<(ostream& ostr, HeuristicsMap::PlayerTypeGroup group) -> ostream&;
auto operator<<(ostream& ostr, HeuristicsMap::Key key) -> ostream&;

auto gettext(HeuristicsMap::GameTypeGroup gametype_group)     -> string;
auto gettext(HeuristicsMap::PlayerTypeGroup playertype_group) -> string;
