/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "human.h"
#ifdef POSTPHONED
#include "humanDb.h"
#endif

#include "../../basetypes/fast_play.h"

#include "../../party/rule.h"
#include "../../misc/preferences.h"
#include "../../ui/ui.h"
#include "../../os/bug_report_replay.h"
#include "../../game/gameplay_actions.h"

Human::Human()
{
  // *** HACK
  static_cast<Person&>(*this).set_type(Player::Type::human);
  static_cast<Person&>(*this).set_name("");
#ifdef POSTPHONED
  db_ = new HumanDb();
#endif
}


Human::Human(Name const name) :
  Ai(name)
{
  // *** HACK
  static_cast<Person&>(*this).set_type(Player::Type::human);
#ifdef POSTPHONED
  db_ = new HumanDb();
#endif
}


Human::Human(istream& istr) :
  Ai(istr)
{
  static_cast<Person&>(*this).set_type(Player::Type::human);
}


Human::Human(Player const& player) :
  Ai(player)
{
  static_cast<Person&>(*this).set_type(Player::Type::human);
}


Human::Human(Player const& player, Aiconfig const& aiconfig) :
  Ai(player, aiconfig)
{
  static_cast<Person&>(*this).set_type(Player::Type::human);
}


Human::Human(Ai const& ai) :
  Ai(static_cast<Ai const&>(ai))
{
  static_cast<Person&>(*this).set_type(Player::Type::human);
}


Human::Human(Human const& human) :
  Ai(static_cast<Ai const&>(human))
{
  static_cast<Person&>(*this).set_type(Player::Type::human);
}


auto Human::clone() const -> unique_ptr<Player>
{
  return make_unique<Human>(*this);
}


auto Human::get_reservation() -> Reservation const&
{
  DEBUG_ASSERTION(!game().isvirtual(),
		  "Human::reservation_get():\n"
		  "  Game is virtual");

#ifdef BUG_REPORT_REPLAY
  {
    if (   ::bug_report_replay
	&& ::bug_report_replay->auto_action()
	&& (::bug_report_replay->current_action().type()
	    == GameplayActions::Type::reservation)
	&& (dynamic_cast<GameplayActions::Reservation const&>(::bug_report_replay->current_action()).player
	    == no())
       ) {
      reservation() = dynamic_cast<GameplayActions::Reservation const&>(::bug_report_replay->current_action()).reservation;
      return reservation();
    }
  }
#endif

  if (::fast_play & FastPlay::player) {
    reservation() = Ai::get_reservation();
    return reservation();
  }

  reservation() = ::ui->get_reservation(*this);

  return reservation();
}


void Human::game_start()
{
  Ai::game_start();
  if (::preferences(Preferences::Type::announce_swines_automatically)) {
    if (game().rule(Rule::Type::swines_announcement_begin))
      if (game().swines().swines_announcement_valid(*this))
	game().swines().swines_announce(*this);

  }
}


auto Human::announcement_request() const -> Announcement
{
#ifdef BUG_REPORT_REPLAY
  { // bug report replay
    if (   ::bug_report_replay
	&& ::bug_report_replay->auto_action()
	&& (::bug_report_replay->current_action().type()
	    == GameplayActions::Type::announcement)
	&& (dynamic_cast<GameplayActions::Announcement const&>(::bug_report_replay->current_action()).player
	    == no())
       ) {
      return dynamic_cast<GameplayActions::Announcement const&>(::bug_report_replay->current_action()).announcement;
    }
  }
#endif

  return Announcement::noannouncement;
}


auto Human::card_get() -> HandCard
{
  DEBUG_ASSERTION(!game().isvirtual(),
		  "Human::card_get():\n"
		  "  Game is virtual");

#ifdef BUG_REPORT_REPLAY
  {
    if (   ::bug_report_replay
	&& ::bug_report_replay->auto_action()
       ) {
      if ( (::bug_report_replay->current_action().type()
	    == GameplayActions::Type::swines)
	  && (dynamic_cast<GameplayActions::Swines const&>(::bug_report_replay->current_action()).player
	    == no()))
	game().swines().swines_announce(*this);

      if ( (::bug_report_replay->current_action().type()
	    == GameplayActions::Type::hyperswines)
	  && (dynamic_cast<GameplayActions::Hyperswines const&>(::bug_report_replay->current_action()).player
	    == no()))
	game().swines().hyperswines_announce(*this);

      if ( (::bug_report_replay->current_action().type()
	    == GameplayActions::Type::card_played)
	  && (dynamic_cast<GameplayActions::CardPlayed const&>(::bug_report_replay->current_action()).player
              == no())) {
        set_last_heuristic_to_bug_report();
        return HandCard(hand(), dynamic_cast<GameplayActions::CardPlayed const&>(::bug_report_replay->current_action()).card);
      }
    }
  }
#endif

  if (::fast_play & FastPlay::player) {
    HandCard const card = Ai::card_suggestion();
    if (!card.is_empty()
        && card.isvalid(game().tricks().current())) {
      return card;
    }
  }

  set_last_heuristic_to_manual();
  auto const card = ::ui->get_card(*this);

  if (game().status() != Game::Status::play)
    return HandCard(hand(), Card());

  DEBUG_ASSERTION(!card.is_empty(),
                  "Human::card_get():\n"
                  "  result of 'ui->card_get()' is an empty card");

  if (::preferences(Preferences::Type::announce_swines_automatically)) {
    auto const& rule = game().rule();
    auto& swines = game().swines();
    if (card.possible_swine()
        && !rule(Rule::Type::swines_announcement_begin)
        && swines.swines_announcement_valid(*this)) {
      if (rule(Rule::Type::swine_only_second)) {
        if (hand().count(Card::trump, Card::ace) == 1)
          swines.swines_announce(*this);
      } else {
        swines.swines_announce(*this);
      }
    }
    if (card.possible_hyperswine()
        && !rule(Rule::Type::hyperswines_announcement_begin)
        && swines.hyperswines_announcement_valid(*this))
      swines.hyperswines_announce(*this);
  }

  return card;
}


void Human::teaminfo_update()
{
  Player::teaminfo_update();
  if (::preferences(Preferences::Type::show_ai_information_teams)
      && !game().isvirtual()) {
    for (auto const& p : game().players()) {
      ::ui->teaminfo_changed(p);
    }
  }
}


auto Human::poverty_shift() -> HandCards
{
#ifdef BUG_REPORT_REPLAY
  {
    if (   ::bug_report_replay
        && ::bug_report_replay->auto_action()
        && (::bug_report_replay->current_action().type()
            == GameplayActions::Type::poverty_shift)
        && (dynamic_cast<GameplayActions::PovertyShift const&>(::bug_report_replay->current_action()).player
            == no())
       ) {
      auto const cards = HandCards(hand(), dynamic_cast<GameplayActions::PovertyShift const&>(::bug_report_replay->current_action()).cards);
      hand().remove(cards);
      return cards;
    }
  }
#endif

  if (::fast_play & FastPlay::player)
    return Ai::poverty_shift();

  return ::ui->get_poverty_cards_to_shift(*this);
}


auto Human::poverty_take_accept(unsigned const cardno) -> bool
{
#ifdef BUG_REPORT_REPLAY
  {
    if (   ::bug_report_replay
        && ::bug_report_replay->auto_action()) {
      if (   (::bug_report_replay->current_action().type()
              == GameplayActions::Type::poverty_accepted)
          && (dynamic_cast<GameplayActions::PovertyAccepted const&>(::bug_report_replay->current_action()).player
              == no()) )
        return true;
      if (   (::bug_report_replay->current_action().type()
              == GameplayActions::Type::poverty_denied)
          && (dynamic_cast<GameplayActions::PovertyDenied const&>(::bug_report_replay->current_action()).player
              == no()) )
        return false;
    }
  }
#endif

  if (::fast_play & FastPlay::player)
    return Ai::poverty_take_accept(cardno);

  return ::ui->get_poverty_take_accept(*this, cardno);
}


auto Human::poverty_cards_change(vector<Card> const& cards) -> HandCards
{
#ifdef BUG_REPORT_REPLAY
  {
    if (   ::bug_report_replay
        && ::bug_report_replay->auto_action()) {
      if (::bug_report_replay->current_action().type()
          == GameplayActions::Type::poverty_returned) {
        hand().add(cards);
        auto const cards_returned = HandCards(hand(), dynamic_cast<GameplayActions::PovertyReturned const&>(::bug_report_replay->current_action()).cards);
        hand().remove(cards_returned);
#ifdef WORDAROUND
        // the ai has to update the cards
        set_hand(hand());
#endif
        return cards_returned;
      }
    }
  }
#endif

  if (::fast_play & FastPlay::player)
    return Ai::poverty_cards_change(cards);

#ifdef WORKAROUND
  // the ai has to update the cards
  HandCards const cards_returned
    = ::ui->get_poverty_exchanged_cards(*this, cards);
  Ai::set_hand(hand());

  return cards_returned;
#else
  return ::ui->poverty_cards_change(*this, cards);
#endif
}


void Human::poverty_cards_get_back(vector<Card> const& cards)
{
#ifdef BUG_REPORT_REPLAY
  {
    if (   ::bug_report_replay
        && ::bug_report_replay->auto_action()) {
      Ai::poverty_cards_get_back(cards);
      return ;
    }
  }
#endif

  if (::fast_play & FastPlay::player) {
    Ai::poverty_cards_get_back(cards);
    return ;
  }

  ::ui->poverty_cards_get_back(*this, cards);
#ifdef WORKAROUND
  // the ai has to update the cards
  Ai::set_hand(hand());
#endif
}


void Human::swines_announced(Player const& player)
{
  Ai::swines_announced(player);

  if (::preferences(Preferences::Type::announce_swines_automatically)) {
    if (game().swines().hyperswines_announcement_valid(*this)
        && game().rule(Rule::Type::hyperswines_announcement_begin))
      game().swines().hyperswines_announce(*this);

  }
}
