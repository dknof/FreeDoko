/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "player.h"
#include "playersDb.h"
#include "cards_information.h"
#include "team_information.h"

#include "human/human.h"
#include "ai/ai.h"
#include "ai.dummy/ai.h"
#include "ai.random/ai.h"

#include "../versions.h"
#include "../card/hand.h"
#include "../party/party.h"
#include "../party/rule.h"
#include "../game/game_summary.h"
#include "../card/trick.h"
#include "../misc/preferences.h"
#include "../ui/ui.h"

// whether to write the databasea
bool Player::write_database = true; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)


auto Player::create(Type const type, Name const name) -> unique_ptr<Player>
{
  switch(type) {
  case Player::Type::unset:
    DEBUG_ASSERTION(false,
                    "Player::create(type):\n"
                    "  type == UNSET");
    return nullptr;
  case Player::Type::human:
    return make_unique<Human>(name);
  case Player::Type::ai:
    return make_unique<Ai>(name);
  case Player::Type::ai_dummy:
    return make_unique<AiDummy>(name);
  case Player::Type::ai_random:
    return make_unique<AiRandom>(name);
  } // switch(type)

  return {};
}


auto Player::create(Type const type, unsigned const no) -> unique_ptr<Player>
{
  return create(type, Person::default_name(type, no));
}


auto Player::create(Type const type, Player const& player) -> unique_ptr<Player>
{
  unique_ptr<Player> player_new;

  switch(type) {
  case Player::Type::unset:
    DEBUG_ASSERTION(false,
                    "Player::create(type, player):\n"
                    "  type == unset");
    break;
  case Player::Type::human:
#ifdef WORKAROUND
    if (dynamic_cast<Ai const*>(&player))
      player_new = make_unique<Human>(dynamic_cast<Ai const&>(player));
    else if (dynamic_cast<Aiconfig const*>(&player))
      player_new = make_unique<Human>(player, dynamic_cast<Aiconfig const&>(player));
    else
      player_new = make_unique<Human>(player);
#else
    // see 'AI'
    player_new = make_unique<Human>(player);
#endif
    break;

  case Player::Type::ai:
#ifdef WORKAROUND
    if (dynamic_cast<Ai const*>(&player))
      player_new = make_unique<Ai>(dynamic_cast<Ai const&>(player));
    else if (dynamic_cast<Aiconfig const*>(&player))
      player_new = make_unique<Ai>(player, dynamic_cast<Aiconfig const&>(player));
    else
      player_new = make_unique<Ai>(player);
#else
    // if 'p' is of type 'Human', the GNU C++ compiler calls 'Ai(Player)' but
    // not 'Ai(Ai)' although 'Human' is a child of 'Ai'
    player_new = make_unique<Ai>(player);
#endif
    break;

  case Player::Type::ai_dummy:
    if (dynamic_cast<Aiconfig const*>(&player))
      player_new = make_unique<AiDummy>(player, dynamic_cast<Aiconfig const&>(player));
    else
      player_new = make_unique<AiDummy>(player);
    break;

  case Player::Type::ai_random:
    if (dynamic_cast<Aiconfig const*>(&player))
      player_new = make_unique<AiRandom>(player, dynamic_cast<Aiconfig const&>(player));
    else
      player_new = make_unique<AiRandom>(player);
    break;
  } // switch(type)

  return player_new;
}

#ifndef OUTDATED
// 0.7.21: Ersetzt mittels durchgehendes Speichern des gesamten Turniers in ~/.FreeDoko/party

auto Player::create(Path const path) -> unique_ptr<Player>
{
  ifstream istr(path);
  if (istr.fail())
    return {};

#ifdef WORKAROUND
  // test whether the player file is empty
  // Note: if the human player (player 0) cannot be loaded,
  // he is replaced by an ai.
  istr.peek();
  if (istr.eof()) {
    cerr << "could not load player from file '" << path << "', "
      << "creating new player" << endl;
    return Player::create(Type::ai, "");
  }
#endif

  return Player::create(istr);
}
#endif


auto Player::create(istream& istr) -> unique_ptr<Player>
{
  auto player = make_unique<Player>(Type::unset, "");

  Config config;
  istr >> config;

  int depth = 0;
  Type type_before = Player::Type::unset;

  if ((config.name == "{")
      && config.value.empty()) {
    depth += 1;
    istr >> config;
  }
  while (istr.good()) {

    if (istr.eof())
      break;

    if (istr.fail()) {
#ifndef RELEASE
      cerr << "reading player failed" << endl;
#endif
      return nullptr;
    }

    // finished with the config file
    if (config.empty())
      continue;

    if (   config.name == "{"
        && config.value.empty())
      depth += 1;

    if (   config.name == "}"
        && config.value.empty()) {
      depth -= 1;
      // finished with the player
      if (depth == -1)
        istr.putback('}');
      if (depth <= 0)
        break;
    }

    if (!player->read(config, istr))
      cerr << "Unknown player line '" << config.name << " = " << config.value
        << '\'' << endl;

    // if the type is set, change the underlying player
    if (player->type() != type_before) {
      unique_ptr<Player> player2;
      switch(player->type()) {
      case Type::unset:
        DEBUG_ASSERTION(false,
                        "Player::create(istr):\n"
                        "  player type is 'UNSET'");
        player2 = Player::create(Type::ai, *player);
        break;
      case Type::human:
        player2 = Player::create(Type::human, *player);
        break;
      case Type::ai:
        player2 = Player::create(Type::ai, *player);
        break;
      case Type::ai_dummy:
        player2 = Player::create(Type::ai_dummy, *player);
        break;
      case Type::ai_random:
        player2 = Player::create(Type::ai_random, *player);
        break;
      } // switch(player->type())
      player = std::move(player2);
      type_before = player->type();
    } // if (player->type() != UNSET)

    istr >> config;
  } // while (istr.good())

  return player;
}


Player::Player(Type const type, Name const name) :
  person_(make_unique<Person>(type, name)),
  trickpile_(*this),
  db_(make_unique<PlayersDb>())
{ }


Player::Player(Player const& player) :
  person_(player.person_->clone()),
  game_(player.game_),
  team_(player.team_),
  hand_(*this, player.hand_),
  reservation_(player.reservation_),
  trickpile_(player.trickpile_, *this),
  db_( (player.db_ == nullptr)
      ? nullptr
      : new PlayersDb(player.db()) ) // *** DK must use a copy!
{
  if (team_information_)
    team_information_->set_player(*this);
  if (cards_information_)
    cards_information_->set_player(*this);
}


Player::~Player() = default;


auto Player::person() noexcept -> Person&
{
  return *person_;
}


auto Player::person() const noexcept -> Person const&
{
  return *person_;
}


Player::operator Person&() noexcept
{
  return *person_;
}


Player::operator Person const&() const noexcept
{
  return *person_;
}


auto Player::self_check() const -> bool
{
  DEBUG_ASSERTION(db_,
                  "Player::self_check()\n"
                  "  no database");
  if (game_) {
    DEBUG_ASSERTION((game_->players().no(*this) != UINT_MAX),
                    "Player::self_check()\n"
                    "  player not in game: " << name());


    // ToDo
    // check the hand

    hand().self_check();
  } // if (game_)

  return true;
}


auto Player::clone() const -> unique_ptr<Player>
{
  return make_unique<Player>(*this);
}


auto Player::operator=(Player const& player) -> Player&
{
  if (this == &player)
    return *this;
  // copy the values
  *person_ = *player.person_;
  game_ = player.game_;
  team_ = player.team_;
  hand_ = player.hand_;
  hand_.set_player(*this);
  trickpile_ = player.trickpile_;
  db_        = (player.db_
                ? make_unique<PlayersDb>(*player.db_) // *** DK must use a copy!
                : nullptr);

  return *this;
}


auto operator==(Player const& lhs, Player const& rhs) -> bool
{
  //return player1.is_equal(player2);
  return (&lhs == &rhs);
}


auto operator!=(Player const& lhs, Player const& rhs) -> bool
{
  return !(lhs == rhs);
}

auto Player::team_information() const noexcept -> TeamInformation const&
{
  return *team_information_;
}

auto Player::team_information() noexcept -> TeamInformation&
{
  return *team_information_;
}

auto Player::cards_information() const noexcept -> CardsInformation const&
{
  return *cards_information_;
}

auto Player::cards_information() noexcept -> CardsInformation&
{
  return *cards_information_;
}

void Player::set_cards_information(CardsInformation const& cards_information)
{
  if (cards_information_.get() == &cards_information)
    return ;

  cards_information_ = make_unique<CardsInformation>(cards_information);
  cards_information_->set_player(*this);
}


auto Player::teaminfo(unsigned const playerno) const -> GuessedTeam
{
  if (!team_information_) {
    if (playerno == no())
      return to_guessed(team());
    return to_guessed(game().teaminfo().get(playerno));
  }
  return team_information().team(playerno);
}


auto Player::teaminfo(Player const& player) const -> GuessedTeam
{
  if (!team_information_) {
    if (player.no() == no())
      return to_guessed(team());
    return to_guessed(game().teaminfo().get(player));
  }
  return team_information().team(player);
}


void Player::teaminfo_update()
{
  if (team_information_)
    team_information().update_teams();
}



auto Player::save(Path const path) const -> bool
{
  auto path_tmp = path;
  path_tmp += ".tmp";
  ofstream ostr(path_tmp);
  if (!ostr.good()) {
    ::ui->information(_("Error::Player(%s)::save: Error opening temporary file %s. Aborting saving.",
                        name(), path_tmp.string()), InformationType::problem);
    return false;
  }

  ostr << "# FreeDoko player (" << *::version << ")\n"
    << '\n';
  write(ostr);

  if (!ostr.good()) {
    ::ui->information(_("Error::Player(%s)::save: Error saving in temporary file %s. Keeping temporary file (for bug tracking).",
                        name(), path_tmp.string()), InformationType::problem);
    return false;
  }
  ostr.close();

  try {
    if (is_regular_file(path))
      remove(path);
    rename(path_tmp, path);
  } catch (std::filesystem::filesystem_error const& error) {
    ::ui->information(_("Error::Player(%s)::save: Could not rename temporary file %s to requested file %s. Keeping temporary file.", name(), path_tmp.string(), path.string())
                      + "\n"
                      + _("Error message: %s", error.what()),
                      InformationType::problem);
    return false;
  } catch (...) {
    ::ui->information(_("Error::Player(%s)::save: Could not rename temporary file %s to requested file %s. Keeping temporary file.",
                        name(), path_tmp.string(), path.string()), InformationType::problem);
    return false;
  }

  return true;
}


auto Player::write(ostream& ostr) const -> ostream&
{
  ostr << "name = " << person_->name_value() << '\n';
  if (person_->name_value().empty())
    ostr << "# name = " << name() << '\n';
  ostr << "voice = " << voice() << '\n'
    << "type = " << type() << '\n'
    << "color = " << color() << '\n';
  if (Player::write_database) {
    ostr << '\n'
      << "database\n"
      << "{\n"
      << db()
      << "}\n";
  } // if (Player::write_database)

  return ostr;
}


auto operator<<(ostream& ostr, const Player& player) -> ostream&
{
  player.write(ostr);

  return ostr;
}


void Player::write_short(ostream& ostr) const
{
  ostr << person_->name_value() << " -- "
    << type();
}


auto Player::read(Config const& config, istream& istr) -> bool
{
  if (config.separator) {
    if (person_->read(config)) {
      return true;
    }
  } else { // if (config.separator)
#ifndef OUTDATED
    // 0.7.3
    if ( (config.name == "database")
        || (config.name == "Database") )
#else
      if (config.name == "Database")
#endif
      {
        db_->read(istr);
        return true;
      }
  } // if (config.separator)

  return false;
}


auto Player::type() const -> Person::Type
{
  return person_->type();
}


auto Player::name() const -> Person::Name
{
  return person_->name();
}


auto Player::voice() const -> Person::Voice
{
  return person_->voice();
}


auto Player::color() const -> Person::Color
{
  return person_->color();
}


auto Player::team() const -> Team
{
  return team_;
}


void Player::set_team(Team const team)
{
  team_ = team;
}


auto Player::game() -> Game&
{
  DEBUG_ASSERTION(game_,
                  "Player::game(): 'game_' is a NULL-pointer\n"
                  "  this = " << this);

  return *game_;
}


auto Player::game() const -> Game const&
{
  DEBUG_ASSERTION(game_,
                  "Player::game(): 'game_' is a NULL-pointer\n"
                  "  this = " << this);

  return *game_;
}


void Player::set_game(Game& game)
{
  game_ = &game;
}


auto Player::no() const -> unsigned
{
  if (!game_)
    return UINT_MAX;
  return game_->players().no(*this);
}


auto Player::db() const -> PlayersDb const&
{
  DEBUG_ASSERTION(db_,
                  "Playe::db():\n"
                  "  'db_' == NULL");

  return *db_;
}


auto Player::db() -> PlayersDb&
{
  DEBUG_ASSERTION(db_,
                  "Playe::db():\n"
                  "  'db_' == NULL");

  return *db_;
}


auto Player::is_remaining_duty_solo(GameType const game_type) const -> bool
{
  if (!game().rule(game_type))
    return false;
  if (!is_real_solo(game_type))
    return false;
  if (remaining_duty_free_soli())
    return true;
  if (   is_picture_solo(game_type)
      && remaining_duty_picture_soli())
    return true;
  if (   is_color_solo(game_type)
      && remaining_duty_color_soli())
    return true;
  return false;
}


auto Player::remaining_duty_soli() const -> unsigned
{
  DEBUG_ASSERTION(game_,
                  "Player::remaining_duty_soli()\n"
                  "not in game");
  return game().party().remaining_duty_soli(*this);
}


auto Player::remaining_duty_color_soli() const -> unsigned
{
  DEBUG_ASSERTION(game_,
                  "Player::remaining_duty_color_soli()\n"
                  "not in game");
  return game().party().remaining_duty_color_soli(*this);
}


auto Player::remaining_duty_picture_soli() const -> unsigned
{
  DEBUG_ASSERTION(game_,
                  "Player::remaining_duty_picture_soli()\n"
                  "not in game");
  return game().party().remaining_duty_picture_soli(*this);
}


auto Player::remaining_duty_free_soli() const -> unsigned
{
  DEBUG_ASSERTION(game_,
                  "Player::remaining_duty_free_soli()\n"
                  "not in game");
  return game().party().remaining_duty_free_soli(*this);
}


auto Player::hand() const -> Hand const&
{
  return hand_;
}


auto Player::hand() -> Hand&
{
  return hand_;
}


void Player::set_hand(Hand const& hand)
{
  hand_ = hand;
  hand_.set_player(*this);

  if (game().status() == Game::Status::init) {
    set_team(  (hand.count(Card::club_queen) > 0)
             ? Team::re
             : hand.contains(Card::unknown)
             ? Team::unknown
             : Team::contra );
  }
}


auto Player::isre() const -> bool
{
  return (team() == Team::re);
}


auto Player::has_swines() const -> bool
{
  if (   game().swines().swines_announced()
      && *this == game().swines().swines_owner()) {
    if (   game().status() == Game::Status::poverty_shift
        && *this == game().players().soloplayer())
      return true;
    // search a swine in all cards of the hand
    for (unsigned c = 0; c < hand().cardsnumber_all(); c++)
      if (hand().card_all(c).isswine())
        return true;
  }

  return false;
}


auto Player::has_hyperswines() const -> bool
{
  if (   game().swines().hyperswines_announced()
      && (*this == game().swines().hyperswines_owner())) {
    // search a hyperswine in all cards of the hand
    for (unsigned c = 0; c < hand().cardsnumber_all(); c++)
      if (hand().card_all(c).ishyperswine())
        return true;
  }

  return false;
}


auto Player::reservation() const -> Reservation const&
{
  return reservation_;
}


auto Player::reservation() -> Reservation&
{
  return reservation_;
}


auto Player::trickpile() const -> TrickPile const&
{
  return trickpile_;
}


auto Player::announcement() const -> Announcement
{
  return game().announcements().last(*this);
}


auto Player::next_announcement() const -> Announcement
{
  if (!::is_real(team()))
    return Announcement::noannouncement;

  Announcement const last_announcement
    = (game().rule(Rule::Type::knocking)
       ? game().announcements().last(*this).announcement
       : game().announcements().last(team()).announcement);

  Announcement next_announcement = Announcement::noannouncement;

  switch (last_announcement) {
  case Announcement::noannouncement:
    next_announcement = Announcement::no120;
    break;
  case Announcement::no120:
  case Announcement::no90:
  case Announcement::no60:
  case Announcement::no30:
    next_announcement = next(last_announcement);
    break;
  case Announcement::no0:
  default:
    next_announcement = Announcement::noannouncement;
    break;
  } // switch(last_announcement)

  if (game().announcements().is_valid(next_announcement, *this))
    return next_announcement;

  if (game().announcements().is_valid(Announcement::reply, *this))
    return Announcement::reply;

  return next_announcement;
}


auto Player::number_of_tricks() const -> unsigned
{
  DEBUG_ASSERTION((game_),
                  "Player::number_of_tricks():\n"
                  "  'game' == NULL");

  return game().tricks().number_of_tricks(*this);
}


auto Player::points_in_trickpile() const -> unsigned
{
  DEBUG_ASSERTION((game_),
                  "Player::points_in_trickpile():\n"
                  "  'game' == NULL");

  return game().points_of_player(*this);
}


auto Player::cards_to_play() const -> unsigned
{
  DEBUG_ASSERTION((game_),
                  "Player::cards_to_play():\n"
                  "  'game' == NULL");

  if (game().tricks().empty())
    return game().tricks().max_size();

  if (game().tricks().current().cardno_of_player(*this)
      < game().tricks().current().actcardno())
    return (game().tricks().max_size() - game().tricks().real_current_no() - 1);
  else
    return (game().tricks().max_size() - game().tricks().real_current_no());
}


void Player::rule_changed(int const type, void const* const old_value)
{ }


void Player::game_open(Game& game)
{
  game_ = &game;
  hand_ = Hand();
  team_ = Team::unknown;
  reservation_ = Reservation();
}


void Player::game_close(Game const& game)
{
  // check whether the player is playing in a game
  if (game_ == nullptr)
    return;

  DEBUG_ASSERTION(   game_ == &game
                  || game.party().status() == Party::Status::loaded,
                  "Player::game_close(game):\n"
                  "  wrong game is closed: " << game_ << " instead of " << &game);

  if (team_information_)
    team_information_->reset();
  if (cards_information_)
    cards_information_->reset();
  trickpile_.clear();
  game_ = nullptr;
}


auto Player::announcement_request() const -> Announcement
{
  return Announcement::noannouncement;
}


void Player::announcement_made(Announcement const announcement,
                               Player const& player)
{
  if (team_information_)
    team_information().announcement_made(announcement, player);
  if (cards_information_)
    cards_information().announcement_made(announcement, player);
  if (team_information_)
    team_information().update_teams();

  teaminfo_update();
}


void Player::poverty_shift(Player const& player, unsigned const cardno)
{  }


void Player::poverty_take_denied(Player const& player)
{  }


void Player::poverty_take_denied_by_all()
{ }


void Player::poverty_take_accepted(Player const& player,
                                   unsigned const cardno, unsigned const trumpno)
{  }


auto Player::poverty_shift() -> HandCards
{
  cerr << "call of Player::poverty shift()"
    << endl;

  return HandCards(hand());
}


auto Player::poverty_take_accept(unsigned const cardno) -> bool
{
  cerr << "call of Player::poverty_take_accept(cardno)"
    << endl;

  return false;
}


auto Player::poverty_cards_change(Game::Poverty::Cards const& cards) -> HandCards
{
  cerr << "call of Player::poverty_cards_change(cards)"
    << endl;

  return HandCards(hand());
}


void Player::poverty_cards_get_back(Game::Poverty::Cards const& cards)
{
  hand().add(cards);
}


void Player::marriage(Player const& bridegroom, Player const& bride)
{
  if (team_information_)
    team_information().marriage(bridegroom, bride);
  teaminfo_update();
}


auto Player::trump_card_limit() const -> HandCard
{
  switch (game().type()) {
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
    return HandCard(hand(), Card::diamond_queen);
  case GameType::solo_jack:
    return HandCard(hand(), Card::club_jack);
  case GameType::solo_queen:
    return HandCard(hand(), Card::club_queen);
  case GameType::solo_king:
    return HandCard(hand(), Card::club_king);
  case GameType::solo_queen_jack:
    return HandCard(hand(), Card::heart_queen);
  case GameType::solo_king_jack:
    return HandCard(hand(), Card::heart_king);
  case GameType::solo_king_queen:
    return HandCard(hand(), Card::heart_king);
  case GameType::solo_koehler:
    return HandCard(hand(), Card::spade_queen);
  case GameType::solo_meatless:
    return HandCard(hand(), Card::diamond_queen);
  default:
    return HandCard(hand(), Card::diamond_jack);
  }
  return {};
}


void Player::new_game(Game& game)
{
  game_ = &game;
  hand_ = Hand();
  team_information_  = make_unique<TeamInformation>(*this);
  cards_information_ = make_unique<CardsInformation>(*this);
  team_information().reset();
  cards_information().reset();
}


auto Player::get_reservation() -> Reservation const&
{
  reservation_ = get_default_reservation();
  return reservation_;
}


auto Player::get_default_reservation() const -> Reservation
{
  Reservation reservation;
  auto const& rule = game().rule();
  auto const& hand = this->hand();

  if (hand.count(Card::club_queen)
      == rule(Rule::Type::number_of_same_cards)) {
    reservation.game_type = GameType::marriage;
    if (rule(MarriageSelector::first_foreign))
      reservation.marriage_selector = MarriageSelector::first_foreign;
    else if (rule(MarriageSelector::first_color))
      reservation.marriage_selector = MarriageSelector::first_color;
    else if (rule(MarriageSelector::first_trump))
      reservation.marriage_selector = MarriageSelector::first_trump;
    else if (rule(MarriageSelector::first_club))
      reservation.marriage_selector = MarriageSelector::first_club;
    else if (rule(MarriageSelector::first_spade))
      reservation.marriage_selector = MarriageSelector::first_spade;
    else if (rule(MarriageSelector::first_heart))
      reservation.marriage_selector = MarriageSelector::first_heart;
    else
      DEBUG_ASSERTION(false,
                      "Player::reservation_get_default():\n"
                      "  no marriage selector valid.\n"
                      "  Ruleset:"
                      << rule);
  } // if (possible marriage)

  reservation.swines
    = (   rule(Rule::Type::swines)
       && rule(Rule::Type::swines_announcement_begin)
       && game().swines().swines_announcement_valid(*this));

  reservation.hyperswines
    = (   rule(Rule::Type::hyperswines)
       && rule(Rule::Type::hyperswines_announcement_begin)
       && game().swines().hyperswines_announcement_valid(*this));


  if (hand.has_poverty()) {
    reservation.game_type = GameType::poverty;
    if (hand.count(Card::trump)
        > rule(Rule::Type::max_number_of_poverty_trumps))
      reservation.swines = false;
  }

  if (rule(Rule::Type::throw_when_fox_highest_trump)) {
    auto const fox_highest_trump = [&hand]() {
      for (auto const& c : hand) {
        if (c.istrump() && !c.is_at_max_fox())
          return false;
      }
      return true;
    }();

    if (fox_highest_trump)
      reservation.game_type = GameType::fox_highest_trump;
  } // test fox highest trump

  if (rule(Rule::Type::throw_with_richness)
      && (hand.points()
          >= rule(Rule::Type::min_richness_for_throwing)))
    reservation.game_type = GameType::thrown_richness;

  if (rule(Rule::Type::throw_with_nines_and_kings)
      && (hand.count(Card::nine) + hand.count(Card::king)
          >= rule(Rule::Type::min_number_of_throwing_nines_and_kings)))
    reservation.game_type = GameType::thrown_nines_and_kings;

  if (rule(Rule::Type::throw_with_kings)
      && (hand.count(Card::king)
          >= rule(Rule::Type::min_number_of_throwing_kings)))
    reservation.game_type = GameType::thrown_kings;

  if (rule(Rule::Type::throw_with_nines)
      && (hand.count(Card::nine)
          >= rule(Rule::Type::min_number_of_throwing_nines)))
    reservation.game_type = GameType::thrown_nines;

  return reservation;
}


void Player::game_start()
{
  // last time for swines announcements

  { // update the team
    if (::is_real(game().teaminfo().get(*this))) {
      // all solo games:
      set_team(game().teaminfo().get(*this));
    } else if (game().type() == GameType::marriage) {
      // undetermined marriage
      set_team(Team::noteam);

    } else {
      // a normal game
      set_team((hand().count(Card::club_queen) > 0)
               ? Team::re
               : hand().contains(Card::unknown)
               ? Team::unknown
               : Team::contra);
    }
  } // update the team

  // Für Solo u.ä. muss zurückgesetzt werden wegen der Unterscheidung TColor::trump und ::diamond
  cards_information().reset();
}


void Player::trick_open(Trick const& trick)
{
  if (team_information_)
    team_information().update_teams();
}


auto Player::card_get() -> HandCard
{
  DEBUG_ASSERTION(false,
                  "Player::card_get():\n"
                  "  call of this function");

  return hand().card(0);
}


void Player::card_played(HandCard const card)
{
  if (cards_information_)
    cards_information().card_played(card);
  if (team_information_)
    team_information().card_played(card);
  if (team_information_)
    team_information().update_teams();

  teaminfo_update();
}


void Player::trick_full(Trick const& trick)
{
  DEBUG_ASSERTION(trick.isfull(),
                  "Player::trick_full()\n"
                  "  Trick is not full:\n" << trick);

  if (team_information_)
    team_information().trick_full(trick);
  if (cards_information_)
    cards_information().trick_full(trick);
  if (team_information_)
    team_information().update_teams();
}


void Player::trick_taken()
{  }


void Player::move_in_trickpile(Trick const& trick)
{
  DEBUG_ASSERTION((&trick.game() == game_),
                  "Player::move_in_trickpile(trick)"
                  "  trick.game != game");

  trickpile_.add(trick);
}


void Player::check_swines_announcement_at_game_start()
{  }


void Player::swines_announced(Player const& player)
{
  if (cards_information_)
    cards_information().swines_announced(player);
  if (team_information_)
    team_information().update_teams();
}


void Player::hyperswines_announced(Player const& player)
{
  if (cards_information_)
    cards_information().hyperswines_announced(player);
  if (team_information_)
    team_information().update_teams();
}


void Player::evaluate(GameSummary const& game_summary)
{
  db().evaluate(*this, game_summary);
}
