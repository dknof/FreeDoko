/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../basetypes/team.h"
#include "../basetypes/announcement.h"

#include "person.h"
#include "trickpile.h"
class CardsInformation;
class TeamInformation;
#include "guessed_team.h"

#include "../card/hand.h"
#include "../game/reservation.h"

class PlayersDb;
class GameSummary;
class Game;

/**
 **
 ** the base class for a player (human or ai)
 **
 ** @author	Borg Enders
 ** @author	Diether Knof
 **
 **/
class Player {
  friend class Party;
  friend class Game;
  public:

  using Type = Person::Type;
  using Name = Person::Name;

  public:
  // whether to write the database
  static bool write_database; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

  public:
  static auto create(Type type, Name name)            -> unique_ptr<Player>;
  static auto create(Type type, unsigned no)          -> unique_ptr<Player>;
  static auto create(Type type, Player const& player) -> unique_ptr<Player>;
#ifndef OUTDATED
  // 0.7.21: Ersetzt mittels durchgehendes Speichern des gesamten Turniers in ~/.FreeDoko/party
  static auto create(Path path) -> unique_ptr<Player>;
#endif
  static auto create(istream& istr) -> unique_ptr<Player>;

  public:
  Player() = delete;
  Player(Type type, Name name);
  Player(Player const& player);
  auto operator=(Player const& player) -> Player&;
  virtual auto clone() const -> unique_ptr<Player>;

  virtual ~Player();

  auto person()                  noexcept -> Person&;
  auto person()            const noexcept -> Person const&;
  operator Person&()             noexcept;
  operator Person const&() const noexcept;

  auto team_information()  const noexcept -> TeamInformation const&;
  auto team_information()        noexcept -> TeamInformation&;
  auto cards_information() const noexcept -> CardsInformation const&;
  auto cards_information()       noexcept -> CardsInformation&;

  void set_cards_information(CardsInformation const& cards_information);

  auto teaminfo(unsigned playerno   ) const -> GuessedTeam;
  auto teaminfo(Player const& player) const -> GuessedTeam;
  virtual void teaminfo_update();

  virtual auto self_check() const -> bool;

  auto save(Path path) const -> bool;
  virtual auto write(ostream& ostr) const -> ostream&;
  void write_short(ostream& ostr) const;

  virtual auto read(Config const& config, istream& istr) -> bool;

  auto type()  const -> Person::Type;
  auto name()  const -> Person::Name;
  auto voice() const -> Person::Voice;
  auto color() const -> Person::Color;

  auto team() const -> Team;
  void set_team(Team team);

  auto game() const -> Game const&;
  auto game()       -> Game&;
  virtual void set_game(Game& game);
  auto no() const -> unsigned;

  // database
  auto db() const -> PlayersDb const&;
  auto db()       -> PlayersDb&;

  // party information

  auto is_remaining_duty_solo(GameType game_type) const -> bool;
  auto remaining_duty_soli()         const -> unsigned;
  auto remaining_duty_color_soli()   const -> unsigned;
  auto remaining_duty_picture_soli() const -> unsigned;
  auto remaining_duty_free_soli()    const -> unsigned;

  // game information

  auto hand() const -> Hand const&;
  auto hand()       -> Hand&;
  auto has_swines()      const -> bool;
  auto has_hyperswines() const -> bool;

  auto reservation() const -> Reservation const&;
  auto reservation()       -> Reservation&;

  auto trickpile() const -> TrickPile const&;


  auto announcement()      const -> Announcement;
  auto next_announcement() const -> Announcement;


  auto number_of_tricks()    const -> unsigned;
  auto points_in_trickpile() const -> unsigned;

  // how many cards the player has still to play in the game
  auto cards_to_play() const -> unsigned;

  virtual void set_hand(Hand const& hand);

  auto isre() const -> bool; 


  // game information

  virtual void rule_changed(int type, void const* old_value);

  virtual void game_open(Game& game);
  void game_close(Game const& game);

  virtual auto announcement_request() const -> Announcement;
  virtual void announcement_made(Announcement announcement, Player const& player);

  virtual void poverty_shift(Player const& player, unsigned cardno);
  virtual void poverty_take_denied(Player const& player);
  virtual void poverty_take_denied_by_all();
  virtual void poverty_take_accepted(Player const& player, unsigned cardno, unsigned trumpno);
  virtual auto poverty_shift() -> HandCards;
  virtual auto poverty_take_accept(unsigned cardno) -> bool;
  virtual auto poverty_cards_change(vector<Card> const& cards) -> HandCards;
  virtual void poverty_cards_get_back(vector<Card> const& cards);

  virtual void marriage(Player const& bridegroom, Player const& bride);

  virtual auto trump_card_limit() const -> HandCard;

  virtual void new_game(Game& game);
  virtual auto get_reservation() -> Reservation const&;
  virtual auto get_default_reservation() const -> Reservation;
  virtual void game_start();
  virtual void trick_open(Trick const& trick);
  virtual auto card_get() -> HandCard;
  virtual void card_played(HandCard card);
  virtual void trick_full(Trick const& trick);
  virtual void trick_taken();
  virtual void move_in_trickpile(Trick const& trick);

  virtual void check_swines_announcement_at_game_start();
  virtual void swines_announced(Player const& player);
  virtual void hyperswines_announced(Player const& player);

  virtual void evaluate(GameSummary const& game_summary);

  private:
  unique_ptr<Person> person_;
  Game* game_ = nullptr;

  Team team_ = Team::noteam;
  Hand hand_;
  Reservation reservation_;
  TrickPile trickpile_;

  protected: // AiDummy und AiRandom sollen dies deaktivieren können, da sie es nicht brauchen
  unique_ptr<TeamInformation>  team_information_;
  unique_ptr<CardsInformation> cards_information_;

  protected:

  // statistics about the player
  unique_ptr<PlayersDb> db_;
}; // class Player

auto operator<<(ostream& ostr, Player const& player) -> ostream&;

auto operator==(Player const& lhs, Player const& rhs) -> bool;
auto operator!=(Player const& lhs, Player const& rhs) -> bool;
