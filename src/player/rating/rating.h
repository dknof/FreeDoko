/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"


/**
 ** base class for ratings
 ** A rating takes multiple values and returns a rated value.
 ** An example is the 'max' rater, that returns the maximum of the given values.
 **/
class Rating
{
  public:
    // rating types
    enum class Type {
      max,
      min,
      second_max,
      second_min,
      median,
      average,
      average_physical,
      linear,
      linear_reverse
    };
    constexpr static Type type_list[] = {
      Type::max,
      Type::min,
      Type::second_max,
      Type::second_min,
      Type::median,
      Type::average,
      Type::average_physical,
      Type::linear,
      Type::linear_reverse
    };
    static auto type_from_string(string const& name) -> Type;

    class Max;
    class Min;
    class SecondMax;
    class SecondMin;
    class Median;
    class Average;
    class AveragePhysical;
    class Linear;
    class LinearReverse;

    // returns a rating of the type 'type'
    static auto create(Type type) -> unique_ptr<Rating>;

  public:
    Rating() = delete;
    explicit Rating(Type type);
    virtual ~Rating() = default;

    void write(ostream& ostr) const;

    void add(int value);
    virtual auto value() const -> int = 0;
    void delete_worst(size_t n);

  protected:
    void sort() const;

  protected:
    mutable vector<int> values;

  private:
    Type type_;
}; // class Rating

auto operator<<(ostream& ostr, Rating const& rating) -> ostream&;

auto to_string(Rating::Type type) -> string;
auto gettext(Rating::Type type)   -> string;
auto operator<<(ostream& ostr, Rating::Type type) -> ostream&;
