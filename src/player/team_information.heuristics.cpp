/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "team_information.heuristics.h"
#include "team_information.h"
#include "cards_information.of_player.h"
#include "player.h"
#include "ai/ai.h"

#include "../card/trick.h"
#include "../party/rule.h"
#include "../game/game.h"

// For configuration values search for '*Value*'

namespace TeamInformationHeuristic {

/* Further ideas
 *
 * - if the last player has a known team and he pfunds, than the winnerplayer
 *   could be in the same team
 *   (this assumes, that the last player knows more then I do)
 *
 * - opposite of 'choose pfund for partner'
 *   if a player has played his fox as startcard:
 *   o teams unknown: the player is re
 *   o teams known: the partner is the one with the highest card
 *
 */

// Return: > 0 for re, < 0 for contra
using CardPlayedHeuristic = int(*)(HandCard, Trick const&, Player const&);
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CARD_PLAYED_HEURISTIC(name) \
int name(HandCard card, \
         Trick const& trick, \
         Player const& self)
// the startplayer in a trick
CARD_PLAYED_HEURISTIC(startplayer_color);
// the penultimate (second last) player has pfund
CARD_PLAYED_HEURISTIC(penultimate_pfund);
// the last player in a trick
CARD_PLAYED_HEURISTIC(last_player);

// overjabbed a player
CARD_PLAYED_HEURISTIC(overjabbed_player);

// the player does not have the color
CARD_PLAYED_HEURISTIC(does_not_have_color);
// the player has pfund in a color trick (with the same color)
CARD_PLAYED_HEURISTIC(pfund_in_color_trick);
// the player has jabbed the first color ace
CARD_PLAYED_HEURISTIC(jab_first_color_ace);

// the player has started with a low color card in the first run of the color
CARD_PLAYED_HEURISTIC(started_with_low_color_first_run);
// the player has started with a low trump in the first trump trick
CARD_PLAYED_HEURISTIC(started_with_low_trump_first_run);
// the player has not jabbed the first trump run with the club queen
CARD_PLAYED_HEURISTIC(first_trump_run_no_jabbing_with_club_queen);

#undef CARD_PLAYED_HEURISTIC


// the value according to the team
static int team_value(int value, Player const& player, Player const& self);

auto card_played(HandCard const played_card, Trick const& trick, Player const& self) -> int
{
  // if it is the only card allowed, there is no information to get
  if (   (trick.actcardno() > 1)
      && (trick.startcard().tcolor() == played_card.tcolor())
      && !self.cards_information().of_player(played_card.player()).possible_hand().contains(trick.startcard().tcolor()))
    return 0;

  static const vector<pair<CardPlayedHeuristic, string> > heuristic_list
    = { {pair<CardPlayedHeuristic, string>(startplayer_color, "startplayer color")},
      {pair<CardPlayedHeuristic, string>(penultimate_pfund, "penultimate pfund")},
      {pair<CardPlayedHeuristic, string>(last_player, "last player")},
      {pair<CardPlayedHeuristic, string>(overjabbed_player, "overjabbed player")},
      {pair<CardPlayedHeuristic, string>(does_not_have_color, "does not have color")},
      {pair<CardPlayedHeuristic, string>(pfund_in_color_trick, "pfund in color trick")},
      {pair<CardPlayedHeuristic, string>(jab_first_color_ace, "jab first color ace")},
      {pair<CardPlayedHeuristic, string>(started_with_low_color_first_run, "started with low color first run")},
      {pair<CardPlayedHeuristic, string>(started_with_low_trump_first_run, "started with low trump first run")},
      {pair<CardPlayedHeuristic, string>(first_trump_run_no_jabbing_with_club_queen, "first trump run no jabbing with club queen")}
    };

  int value = 0;

  for (auto const& f : heuristic_list) {
    int const v = (*f.first)(played_card, trick, self);
    if (   v != 0
        && (   debug("team information")
            || debug (self.name() + " team information"))) {
      cout << "team info(" << self.name() << ") " << played_card.player().no() << " - " << played_card << ": " << f.second << ": " << v << endl;
    }
    value += v;
  }

  return value;
}


auto team_value(int const value, Player const& player, Player const& self) -> int
{
  if (value == 0)
    return 0;

  auto const team = self.teaminfo(player);
  auto modifier = (  is_real(team)   ? 1.0
                   : is_surely(team) ? 0.9
                   : is_maybe(team)  ? 0.5
                   : 0);
  int team_value_limit = 20; // *Value*
  try {
    if (dynamic_cast<Ai const&>(self).value(Aiconfig::Type::trusting))
      team_value_limit = 10;
  } catch (std::bad_cast const&) {
  }

  if (   player == self
      && !::is_real(self.game().teaminfo().get(self))
      && (  self.team() == Team::re
          ? self.team_information().team_value(self) < team_value_limit
          : self.team_information().team_value(self) > -team_value_limit
         ) ) {
    modifier *= (0.2
                 + ((0.1 * player.game().tricks().current_no())
                    / player.game().tricks().max_size())
                 + ((0.1 * player.game().cards().count_played_trumps())
                    / player.game().cards().count(Card::trump))); // *Value*
  }

  switch (team) {
  case GuessedTeam::noteam:
  case GuessedTeam::unknown:
    return 0;
  case GuessedTeam::maybe_re:
  case GuessedTeam::surely_re:
    if (self.team_information().team_value(player) < 5) // *Value*
      return 0;
    [[fallthrough]];
  case GuessedTeam::re:
    return static_cast<int>(modifier * value);
  case GuessedTeam::surely_contra:
  case GuessedTeam::maybe_contra:
    if (self.team_information().team_value(player) > -5) // *Value*
      return 0;
    [[fallthrough]];
  case GuessedTeam::contra:
    return static_cast<int>(-modifier * value);
  }
  return 0;
}


auto startplayer_color(HandCard const card, Trick const& trick, Player const& self) -> int
{
  // Idea:
  // If a player starts with a color another player is known not to have,
  // it is likely the two players play in the same team.

  // startplayer
  if (trick.startplayer() != card.player())
    return 0;

  // color trick
  if (card.istrump())
    return 0;

  if (self.cards_information().color_runs(card.color()) == 0) {

    // first color run and no ace: add some points for the second player
    if (card.value() != Card::ace)
      return team_value(2, card.player().game().players().following(card.player()), self); // *Value*

    return 0;
  } else { // if !(first run)

    // * not first color run: add some points for the last player who has
    //   jabbed the color so far
    for (auto p = &card.player().game().players().previous(card.player());
         p != &card.player();
         p = &card.player().game().players().previous(*p))
      if (self.cards_information().of_player(*p).does_not_have(card.color()))
        return team_value(3, *p, self); // *Value*
  } // if !(first run)

  return 0;
}


auto penultimate_pfund(HandCard const card, Trick const& trick, Player const& self) -> int
{
  // Idea:
  // The second last player pfunds.
  // If the winnercard so far is low, the last player is in the same team.
  // If the winnercard so far is high, the winnerplayer is in the same team.

  // the player must have played the penultimate card
  if (trick.actcardno() != card.player().game().playerno() - 1)
    return 0;

  // the player must not be the winner
  if (trick.winnerplayer() == card.player())
    return 0;

  // the player must have pfund at least ten points
  if (card.points() < 10)
    return 0;

  // it must be a trump trick (but it need not be a trump pfund)
  // or it is a color trick but the player has not served the color
  if (!(   trick.startcard().istrump()
        || (card.tcolor() != trick.startcard().tcolor())))
    return 0;

  // if the winnercard is small assume that the last player jabs
  // else assume that the last player cannot jab
  // ToDo: check whether there are cards above the winner card
  // ToDo: check fox
  if (trick.isjabbed({card.hand(), Card::heart_queen})) // *Value*
    return team_value(10, card.player().game().players().following(card.player()), self); // *Value*
  else
    return team_value(10, trick.winnerplayer(), self); // *Value*

  return 0;
}


auto last_player(HandCard const card, Trick const& trick, Player const& self) -> int
{
  // Idea:
  // If the last player in a trick jabs the trick, he does play against
  // the winnerplayer so far,
  // else if he even makes a pfund, he plays with the winnerplayer.
  // Giving a card for a special point gives a much higher credit.

  // the player must have played the last card of the trick
  if (!trick.isfull())
    return 0;

  // it must not be a color trick
  // or the player must not have played the same color
  if (   (trick.startcard().tcolor() != Card::trump)
      && (card.tcolor() == trick.startcard().tcolor()) )
    return 0;

  if (trick.winnerplayer() == card.player()) {
    // the player has jabbed, so check who was the winnerplayer before
    auto const& winnercard = trick.winnercard_before();

    // ToDo: check whether the player has jabbed in order to play
    //       the ace of a color

    // the value depends on the heigh of the card
    if (winnercard.value() == Card::jack)
      return team_value(-15, winnercard.player(), self); // *Value*
    if (winnercard == Card::club_queen)
      return team_value(-50, winnercard.player(), self); // *Value*
    if (winnercard.value() == Card::queen)
      return team_value(-25, winnercard.player(), self); // *Value*
    if (   (winnercard == Card::dulle)
        || winnercard.isswine() )
      return team_value(-35, winnercard.player(), self); // *Value*

  } else { // if !(trick.winnerplayerno() == card.player().no())
    // the player has not jabbed
    // check whether the player could jab the trick
    // or if the player has even made a pfund
    // or, better, added a card for a special point

    // check whether the player has added a card for a special point
    // especially a fox is taken into account
    if (!trick.specialpoints().empty()) {
      int value = 0;
      auto const specialpoints = trick.specialpoints();
      for (auto const& sp : specialpoints) {
        if (sp.player_of_no == card.player().no())
          value += 100 * sp.value; // *Value*
        else if (sp.type == Specialpoint::Type::doppelkopf)
          value += 80; // *Value*
      }
      return team_value(value, trick.winnerplayer(), self);
    } // if (!trick.specialpoints().empty())

    // check whether the player has made a pfund
    if (card.points() >= 10) {
      if (card.isfox())
        return team_value(60, trick.winnerplayer(), self); // *Value*
      if (!trick.startcard().istrump()
          && (card.tcolor() != trick.startcard().tcolor())) {
        return team_value(3 * card.points() // *Value*
                          + static_cast<int>(trick.points()), // *Value*
                          trick.winnerplayer(), self);
      }
      return team_value(card.points() // *Value*
                        + static_cast<int>(trick.points()) / 4, // *Value*
                        trick.winnerplayer(), self);
    } // if (trick.card(card.player()).value() >= 10)

    // check whether the player could jab the trick
    auto const limit_card = HandCard(card.hand(), Card::heart_queen); // *Value*
    if (trick.isjabbed(limit_card))
      return team_value(static_cast<int>(trick.points()) / 3, // *Value*
                        trick.winnerplayer(), self);
  } // if !(trick.winnerplayerno() == card.player().no())

  // ToDo
  // * player jabs with card > club queen ==> contra

  return 0;
}


auto overjabbed_player(HandCard const card, Trick const& trick, Player const& self) -> int
{
  // Idea:
  // The player has overjabbed another player.
  // If the other player has played a high card, he is assumed to be of the opposite team.

  if (trick.actcardno() == 1)
    return 0;
  if (trick.isfull())
    return 0;
  // the player has overjabbed
  if (trick.winnerplayer() != card.player())
    return 0;
  // the winnercard but without the last player
  auto const& winnercard = trick.winnercard_before();

  // the player has overjabbed a high card
  if (winnercard.is_jabbed_by(self.trump_card_limit()))
    return 0;

  if (winnercard == Card::club_queen)
    return team_value(-70, winnercard.player(), self);
  return -team_value(trick.points() / 2 + card.value()
                     + (trick.remainingcardno() * 2), // *Value*
                     winnercard.player(), self);
}


auto does_not_have_color(HandCard const card, Trick const& trick, Player const& self) -> int
{
  // Idea:
  // A player does not have a color and the winnercard of the color is
  // an ace.
  // If he jabs, he does not play with the winnerplayer
  // (different behaviour for Fox, trump ten because the player may
  //  want to bring home the points)
  // If the playe does not jab, he plays with the winnerplayer.

  // not the first player
  if (   (trick.actcardno() != 1)
      // a color trick
      && (trick.startcard().tcolor() != Card::trump)
      // the player does not have the color
      && (card.tcolor() != trick.startcard().tcolor())
     ) {

    if (!is_real(trick.game().teaminfo().get(trick.winnerplayer())))
      return 0;

    // the winnercard but without the last player
    auto const& winnercard = trick.winnercard_before();

    // the winner card must be an ace
    if (winnercard.value() == Card::ace) {

      if (card.istrump()) {
        // ToDo
        // The player could have jabbed in order to play a color ace
        // so check whether all colors have already been run
        // and check in the next trick whether the player does play a color ace
        if (card.isfox())
          return team_value(-11, winnercard.player(), self);
        return team_value(trick.isfull()
                          ? -2 * (35 - card.value())
                          : (trick.actcardno() == 3)
                          ? -2 * (20 - card.value())
                          : -2 * (15 - card.value()),
                          winnercard.player(),
                          self); // *Value*
      } else {
        return team_value(trick.points() / 2 + card.value()
                          + (trick.remainingcardno() * 2), // *Value*
                          winnercard.player(), self);
      }
    } // if (conditions met)
  } // if (conditions met)

  return 0;
}


auto pfund_in_color_trick(HandCard const card, Trick const& trick, Player const& self) -> int
{
  // Idea:
  // In the first run of a color trick:
  // If the player pfunds (p.e. ten), he plays with the winnerplayer
  // (t.i. the one who has played the ace),
  // else (p.e. nine) he plays against the player.
  //
  // Note: In order to return something != 0 the team of the winnerplayer
  // must be known. So the normal case will be that a player has announced
  // 're' and played a color ace.

  // not the first player
  if (trick.actcardno() == 1)
    return 0;

  // a color trick
  if (trick.startcard().istrump())
    return 0;

  // first run of the color trick or second run with enough remaining cards
  if (   (self.cards_information().color_runs(trick.startcard().tcolor()) > 1)
      || (self.cards_information().remaining(trick.startcard().tcolor())
          < trick.remainingcardno()))
    return 0;

  // the player does not play a trump
  if (card.istrump())
    return 0;

  // search the winnercard but without the last player
  auto const& winnercard = trick.winnercard_before();

  // the winnercard must be an ace or a trump
  if (   (winnercard.value() != Card::ace)
      && !winnercard.istrump() )
    return 0;

  // ToDo: search whether a player behind does not have the color

  // there are negative values because a small card shows that the player
  // does not wish to pfund
  // ToDo: test with nines
  if (card.tcolor() == trick.startcard().tcolor()) {
    if (self.cards_information().of_player(card.player()).must_have_cards().lower_card_exists(card)) {
      static const map<Card::Value, int> value
        = { {Card::ace, 50},
          {Card::ten,   45},
          {Card::king,  40},
          {Card::nine,   0} }; // *Value*
      return team_value(value.find(card.value())->second,
                        winnercard.player(), self);
    } else {
      static const map<Card::Value, int> value
        = { {Card::ace, 9},
          {Card::ten,   7},
          {Card::king,  0},
          {Card::nine, -5} }; // *Value*
      return team_value(value.find(card.value())->second,
                        winnercard.player(), self);
    }
  } else { // if (other color)
    static const map<Card::Value, int> value
      = { {Card::ace, 50},
        {Card::ten,   45},
        {Card::king,  30},
        {Card::nine,  10} }; // *Value*
    return team_value(value.find(card.value())->second,
                      winnercard.player(), self);
  }

  return 0;
}


auto jab_first_color_ace(HandCard const card, Trick const& trick, Player const& self) -> int
{
  // color trick
  if (trick.startcard().istrump())
    return 0;

  auto const color = trick.startcard().color();

  // first run of the color
  if (self.cards_information().color_runs(color) != 0)
    return 0;

  // winnercard before is color ace
  if (trick.winnercard_before() != Card(color, Card::ace))
    return 0;

  // the player has jabbed the trick
  if (trick.winnerplayer() != card.player())
    return 0;

  // no player has thrown the color
  if (   !trick.isfull()
      && self.cards_information().played(color) > trick.actcardno() - 1)
    return 0;

  return team_value(-20, trick.winnerplayer_before(), self);
}


auto started_with_low_color_first_run(HandCard const card, Trick const& trick, Player const& self) -> int
{
  // Idea:
  // As startplayer: started with a non ace in a color trick, which has not run before.
  // This is normally for contra players.

  // the first player
  if (trick.actcardno() != 1)
    return 0;

  // color trick
  if (trick.startcard().istrump())
    return 0;

  // first run of the color
  if (self.cards_information().color_runs(trick.startcard().tcolor()) != 0)
    return 0;

  // not played the ace
  if (card.value() != Card::ace)
    return -10; // *Value*

  return 0;
}


auto started_with_low_trump_first_run(HandCard const card, Trick const& trick, Player const& self) -> int
{
  // Idea:
  // As startplayer: started with a small trump in a trump trick, which has not run before.
  // This is normally for contra players.

  // the first player
  if (trick.actcardno() != 1)
    return 0;

  // trump trick
  if (!trick.startcard().istrump())
    return 0;

  // first trump trick
  if (self.cards_information().trump_runs() > 0)
    return 0;

  { // not all color played so far
    auto c = self.game().rule().card_colors().begin();
    for (;
         c != self.game().rule().card_colors().end();
         ++c)
      if (self.cards_information().color_runs(*c) == 0)
        break;
    if (c == self.game().rule().card_colors().end())
      return 0;
  }

  // small trump
  if (card.is_jabbed_by(Card::spade_jack))
    return max(0, 10 - 2 * static_cast<int>(trick.game().tricks().current_no())); // *Value*

  return 0;
}


auto first_trump_run_no_jabbing_with_club_queen(HandCard const card, Trick const& trick, Player const& self) -> int
{
  // Idea:
  // As second/third player in the first trump run: The player has not jabbed with the club queen.
  // Assume, he has a color ace, he then would jab in order to play a color ace.

  // neither first nor last player
  if (   trick.actcardno() == 1
      || trick.isfull())
    return 0;

  // trump trick
  if (!trick.startcard().istrump())
    return 0;

  // first trump trick
  if (self.cards_information().trump_runs() > 0)
    return 0;

  // winnercard before is below club queen
  if (!trick.before_last_played_card().winnercard().is_jabbed_by({self.hand(), Card::club_queen}))
    return 0;
  // the player has not played the highest card in the game
  if (trick.game().cards().next_higher_card(trick.winnercard()) == Card::empty)
    return 0;

  { // not all color have run
    bool all_colors = true;
    for (auto c : self.game().rule().card_colors()) {
      if (   c != self.game().cards().trumpcolor()
          && self.cards_information().color_runs(c) == 0) {
        all_colors = false;
        break;
      }
    }
    if (all_colors)
      return 0;
  } // not all color have run

  // contra
  return min(0, -6 + 1 * static_cast<int>(trick.game().tricks().current_no())); // *Value*
}

} // namespace TeamInformationHeuristic
