/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "trickpile.h"
#include "player.h"
#include "../game/game.h"
#include "../party/rule.h"

TrickPile::TrickPile(Player& player) :
  player_(&player)
{ }


TrickPile::TrickPile(TrickPile const& trickpile, Player& player) :
  player_(&player),
  tricks_(trickpile.tricks_)
{ }


auto TrickPile::operator=(TrickPile const& trickpile) -> TrickPile&
{
  if (this == &trickpile)
    return *this;
  tricks_ = trickpile.tricks_;
  return *this;
}


auto TrickPile::begin() const -> const_iterator
{
  return TrickPile::const_iterator(*this, 0);
}


auto TrickPile::end() const -> const_iterator
{
  return TrickPile::const_iterator(*this, size());
}


auto TrickPile::empty() const -> bool
{
  return tricks_.empty();
}


auto TrickPile::size() const -> unsigned
{
  return tricks_.size();
}


void TrickPile::clear()
{
  tricks_.clear();
}


auto TrickPile::trick(unsigned const t) -> Trick&
{
  DEBUG_ASSERTION(t < tricks_.size(),
                  "TrickPile::trick(t):\n"
                  "'t' is not valid (" << t << ")");

  return player_->game().tricks().trick(tricks_[t]);
}


auto TrickPile::trick(unsigned const t) const -> Trick const&
{
  DEBUG_ASSERTION(t < tricks_.size(),
                  "TrickPile::trick(t):\n"
                  "'t' is not valid (" << t << ")");

  return player_->game().tricks().trick(tricks_[t]);
}


auto TrickPile::last_trick() const -> Trick const&
{
  DEBUG_ASSERTION(!tricks_.empty(),
                  "TrickPile::last_trick():\n"
                  "The trickpile is empty");

  return trick(size() - 1);
}


void TrickPile::add(Trick const& trick)
{
  tricks_.push_back(trick.no());
}
