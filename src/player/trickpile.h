/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "../card/trick.h"
#include "trickpile_iterator.h"
class Player;

/**
 ** The trickpile of a player
 **/
class TrickPile {
  friend class Player;
public:
  using const_iterator = TrickPileConstIterator;

  TrickPile() = delete;
  explicit TrickPile(Player& player);
  TrickPile(TrickPile const& trickpile, Player& player);
  TrickPile(TrickPile const& trickpile) = delete;
  auto operator=(TrickPile const& trickpile) -> TrickPile&;

  auto begin() const -> const_iterator;
  auto end()   const -> const_iterator;

  auto empty() const -> bool;
  auto size()  const -> unsigned;

  void clear();

  auto trick(unsigned p)       -> Trick&;
  auto trick(unsigned p) const -> Trick const&;
  auto last_trick()      const -> Trick const&;

  void add(Trick const& trick);

private:
  Player* const player_;
  vector<unsigned> tricks_;
}; // class TrickPile
