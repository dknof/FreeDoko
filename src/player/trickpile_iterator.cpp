/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "trickpile.h"

TrickPileIterator::TrickPileIterator(TrickPile& tricks,
                                     unsigned const t) :
  tricks_(&tricks),
  t_(t)
{ }


auto TrickPileIterator::operator*() -> Trick&
{
  return tricks_->trick(t_);
}

auto TrickPileIterator::operator*() const -> Trick const&
{
  return tricks_->trick(t_);
}


auto TrickPileIterator::operator->() -> Trick*
{
  return &(**this);
}


auto TrickPileIterator::operator->() const -> Trick const*
{
  return &(**this);
}


auto TrickPileIterator::operator++() -> TrickPileIterator&
{
  t_ += 1;
  return *this;
}


auto TrickPileIterator::operator!=(TrickPileIterator const& i) const -> bool
{
  return (   (t_ != i.t_)
          || (tricks_ != i.tricks_));
}


TrickPileConstIterator::TrickPileConstIterator(TrickPile const& tricks,
                                               unsigned const t) :
  tricks_(&tricks),
  t_(t)
{ }


auto TrickPileConstIterator::operator*() const -> Trick const&
{
  return tricks_->trick(t_);
}


auto TrickPileConstIterator::operator->() const -> Trick const*
{
  return &(**this);
}


auto TrickPileConstIterator::operator++() -> TrickPileConstIterator&
{
  t_ += 1;
  return *this;
}


auto TrickPileConstIterator::operator!=(TrickPileConstIterator const& i) const -> bool
{
  return (   (t_ != i.t_)
          || (tricks_ != i.tricks_));
}
