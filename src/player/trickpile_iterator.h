/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

// included by trickpile.h

#pragma once

#include "constants.h"

class TrickPile;

class TrickPileIterator {
  friend class TrickPile;
public:
  using iterator_category = std::forward_iterator_tag;
  using value_type      = Trick;
  using difference_type = std::ptrdiff_t;
  using pointer         = Trick*;
  using reference       = Trick&;

private:
  TrickPileIterator(TrickPile& tricks, unsigned t);

public:
  auto operator*()        -> Trick&;
  auto operator*()  const -> Trick const&;
  auto operator->()       -> Trick*;
  auto operator->() const -> Trick const*;
  auto operator++() -> TrickPileIterator&;
  auto operator!=(TrickPileIterator const& i) const -> bool;
private:
  TrickPile* const tricks_;
  size_t t_ = 0;
};

class TrickPileConstIterator {
  friend class TrickPile;
public:
  using iterator_category = std::forward_iterator_tag;
  using value_type      = Trick;
  using difference_type = std::ptrdiff_t;
  using pointer         = Trick*;
  using reference       = Trick&;

private:
  TrickPileConstIterator() = delete;
  TrickPileConstIterator(TrickPile const& tricks, unsigned t);

public:
  TrickPileConstIterator(TrickPileIterator i);
  auto operator*()  const -> Trick const&;
  auto operator->() const -> Trick const*;
  auto operator++() -> TrickPileConstIterator&;
  auto operator!=(TrickPileConstIterator const& i) const -> bool;
private:
  TrickPile const* const tricks_;
  size_t t_ = 0;
};
