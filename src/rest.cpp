/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef REST_SERVICE

#ifdef WINDOWS
#include <winsock2.h>
#endif

#include "constants.h"
#include "options.h"
#include "utils/string.h"

#include "game/status.h"
#include "party/party.h"
#include "party/rule.h"
#include "game/game.h"
#include "game/game_summary.h"
#include "game/gameplay_action.h"
#include "game/gameplay_actions/trick.h"
#include "player/player.h"
#include "player/ai/ai.h"
#include "misc/preferences.h"
#ifdef BUG_REPORT
#include "misc/bug_report.h"
#endif

#include <microhttpd.h>
#include <mutex>
#include <unistd.h>

namespace {
struct KeyValue {
  KeyValue() = default;
  KeyValue(string key_) :                key(key_)                { }
  KeyValue(string key_, string value_) : key(key_), value(value_) { }
  string line() const { return value.empty() ? key : key + "=" + value; }
  string key;
  string value;
};
using Query = vector<KeyValue>;

std::mutex rest_mutex;
bool quit_called = false;

auto handle_get(void *cls, struct MHD_Connection *connection,
               const char *url,
               const char *method, const char *version,
               const char *upload_data,
               size_t *upload_data_size, void **con_cls) -> enum MHD_Result;
auto append_query(void* query_, enum MHD_ValueKind, const char *key, const char *value) -> enum MHD_Result;

auto rest_help() -> string;
auto unknown_path(string path) -> string;

auto ai_result(unsigned playerno, Query const& query,
               std::function<string(Game&, Player&)> decision_function) -> string;
auto reservation(Game& game, Player& ai)  -> string;
auto card(Game& game, Player& ai)         -> string;
auto announcement(Game& game, Player& ai) -> string;
#ifdef BUG_REPORT
auto bugreport(Game& game, Player& ai)    -> string;
#endif

void parse_query(Party& party, unsigned const playerno, Query const& query);
void init_players(Party& party, unsigned playerno);
auto hand_from_string(Party const& party, string const& cards) -> Hand;
void set_game_type(Game& game, string const& query_data);
} // namespace

auto main(int argc, char* argv[]) -> int
{
  unsigned port = 54969;

  init_before_option_parsing();
  ::preferences.set(Preferences::Type::automatic_savings, false);
  ::preferences.set(Preferences::Type::additional_party_settings, true);

  // parse the options
  GetOpt::Option option;
  while ((option =
          GetOpt::getopt(argc, argv,
                         {
                         {"help",             '?',  GetOpt::Syntax::BOOL},
                         {"hilfe",            'h',  GetOpt::Syntax::BOOL},
                         {"version",          'v',  GetOpt::Syntax::BOOL},
                         {"license",          'L',  GetOpt::Syntax::BOOL},
                         {"defines",          '\0', GetOpt::Syntax::BOOL},
                         {"directories",      '\0', GetOpt::Syntax::BOOL},
                         {"debug",            '\0',  GetOpt::Syntax::BSTRING},
                         {"port",             'P',  GetOpt::Syntax::UNSIGNED},
#ifdef USE_THREADS
                         {"threads",          'T',  GetOpt::Syntax::BOOL},
#endif
                         })
         )) {

    if (option.fail()) {
      cout << usage_string(argv[0], option);
      std::exit(EXIT_FAILURE);
    } // if (option.fail())

    if (   (option.name() == "help")
        || (option.name() == "hilfe")) {
      cout << help_string();
      std::exit(EXIT_SUCCESS);
    } else if (option.name() == "version") {
      cout << version_string();
      std::exit(EXIT_SUCCESS);
    } else if (option.name() == "license") {
      cout << license_string();
      std::exit(EXIT_SUCCESS);
    } else if (option.name() == "defines") {
      cout << defines_string();
      std::exit(EXIT_SUCCESS);
    } else if (option.name() == "directories") {
      cout << directories_string();
      std::exit(EXIT_SUCCESS);
    } else if (option.name() == "debug") {
      add_debug(option.value_string());
    } else if (option.name() == "port") {
      port = option.value(GetOpt::Option::UNSIGNED);
    }
  }

  auto const daemon = MHD_start_daemon(MHD_USE_INTERNAL_POLLING_THREAD, port, nullptr, nullptr,
                                       &handle_get, nullptr,
                                       MHD_OPTION_END);
  if (!daemon) {
    cerr << "Failed starting daemon on port " << port << ".\n";
    return EXIT_FAILURE;
  }

  cout << "Starting REST-Service on port " << port << "." << endl;
  while (!quit_called) {
    sleep(1);
  }

  cout << "Stopping REST-Service on port " << port << "." << endl;
  MHD_stop_daemon(daemon);

  cout << "Finished\n";
  return EXIT_SUCCESS;
}

namespace {

auto handle_get(void *cls, struct MHD_Connection *connection,
               const char *url,
               const char *method, const char *version,
               const char *upload_data,
               size_t *upload_data_size, void **con_cls) -> enum MHD_Result
{
  // Ein GET-Request ist eingegangen.
  std::lock_guard<std::mutex> guard(rest_mutex);
  if (quit_called)
    return MHD_NO;
  try {
    string const path = url;
    Query query;
    MHD_get_connection_values(connection, MHD_GET_ARGUMENT_KIND, &append_query, &query);
    // Es wird aktuell nur ein laufendes Turnier zur Zeit unterstützt
    string reply;

    if (   path == "/"
        || path == "/help") {
      reply = rest_help();
    } else if (path == "/version") {
      reply = version_string();
    } else if (path == "/license") {
      reply = ("FreeDoko -- License:\n\n"s + gpl_txt);
    } else if (path == "/quit") {
      quit_called = true;
      reply = "quit";
    } else if (path == "/debug/Blatt") {
      ::debug.add("Blatt");
      reply = "Debug-Informationen Blatt aktiviert";
    } else if (path == "/debug/-Blatt") {
      ::debug.remove("Blatt");
      reply = "Debug-Informationen Blatt deaktiviert";
    } else if (path == "/debug/KI") {
      ::debug.add("KI");
      reply = "Debug-Informationen KI aktiviert";
    } else if (path == "/debug/-KI") {
      ::debug.remove("KI");
      reply = "Debug-Informationen KI deaktiviert";
    } else if (path == "/debug/game") {
      ::debug.add("game");
      reply = "Debug-Informationen game aktiviert";
    } else if (path == "/debug/-game") {
      ::debug.remove("game");
      reply = "Debug-Informationen game deaktiviert";
    } else if (regex_matchi(path, "^/[0123]/reservation$")) {
      reply = ai_result(stou(string(path, 1, 1)), query, &reservation);
    } else if (regex_matchi(path, "^/[0123]/card$")) {
      reply = ai_result(stou(string(path, 1, 1)), query, &card);
    } else if (regex_matchi(path, "^/[0123]/announcement$")) {
      reply = ai_result(stou(string(path, 1, 1)), query, &announcement);
#ifdef BUG_REPORT
    } else if (regex_matchi(path, "^/[0123]/bugreport$")) {
      reply = ai_result(stou(string(path, 1, 1)), query, &bugreport);
#endif
    } else {
      throw unknown_path(path);
    }

    auto const response = MHD_create_response_from_buffer(reply.size(),
                                                          const_cast<char*>(reply.data()),
                                                          MHD_RESPMEM_MUST_COPY);

    auto result = MHD_queue_response(connection, MHD_HTTP_OK, response);
    MHD_destroy_response(response);

    return result;
  } catch (string const& error) {
    auto const text = "error\n" + error;
    auto const response = MHD_create_response_from_buffer(text.size(),
                                                          const_cast<char*>(text.data()),
                                                          MHD_RESPMEM_MUST_COPY);
    auto result = MHD_queue_response(connection, MHD_HTTP_BAD_REQUEST, response);
    MHD_destroy_response(response);
    return result;
  } catch (...) {
    auto const text = "error\n" + "unknown error"s;
    auto const response = MHD_create_response_from_buffer(text.size(),
                                                          const_cast<char*>(text.data()),
                                                          MHD_RESPMEM_MUST_COPY);
    auto result = MHD_queue_response(connection, MHD_HTTP_INTERNAL_SERVER_ERROR, response);
    MHD_destroy_response(response);
    return result;
  }
  return MHD_NO;
}

auto append_query(void* query_, enum MHD_ValueKind, char const* key_, char const* value_) -> enum MHD_Result
{
  string key;
  if (key_) {
    while (*key_ && isspace(*key_))
      ++key_;
    key = key_;
    while (!key.empty() && isspace(key.back()))
      key.pop_back();
  }
  string value;
  if (value_) {
    while (*value_ && isspace(*value_))
      ++value_;
    value = value_;
    while (!value.empty() && isspace(value.back()))
      value.pop_back();
  }

  auto& query = *static_cast<Query*>(query_);
  query.emplace_back(key, value);
  return MHD_YES;
};


auto rest_help() -> string
{
  return "rest help";
}


auto unknown_path(string path) -> string
{
  return "unknown path: " + path;
}


auto ai_result(unsigned playerno, Query const& query,
               std::function<string(Game&, Player&)> const decision_function) -> string
{
  Party party;
  party.open();
  party.set_status(Party::Status::init);
  party.set_seed(0);
  try {
    parse_query(party, playerno, query);
    if (party.status() == Party::Status::init)
      return "Party status ist still 'init'. Have you forgotten to give more info?";
    auto& game = party.game();

    auto& ai = game.players().player(playerno);
    auto result = decision_function(game, ai);

    if (::debug("game"))
      result += "\n\n" + to_string(game);

    if (::debug("Blatt"))
      result += "\n\nBlatt:\n" + gettext(ai.hand());
    else if (::debug("hand"))
      result += "\n\nHand:\n" + to_string(ai.hand());

    game.close();
    party.close();

    return result;
  } catch (...) {
    party.close();
    throw;
  }
}


auto reservation(Game& game, Player& ai) -> string
{
  if (game.status() != Game::Status::reservation)
    throw "reservation requested, but game status = " + to_string(game.status()) + " != reservation";

  auto const reservation = ai.get_reservation();

  return to_rest_string(reservation);
}


auto card(Game& game, Player& ai) -> string
{
  if (game.status() == Game::Status::reservation) {
    set_game_type(game, "normal");
  }
  if (game.status() == Game::Status::start) {
    game.set_status(Game::Status::play);
    game.play(GameplayActions::TrickOpen());
  }
  if (game.status() != Game::Status::play)
    throw "card requested, but game status = " + to_string(game.status()) + " != play";

  if (game.tricks().current().isfull()) {
    game.play(GameplayActions::TrickFull(game.tricks().current()));
    game.play(GameplayActions::TrickOpen());
  }

  auto const& trick = game.tricks().current();
  if (trick.actplayer() != ai)
    throw "card requested, but the current player is not " + std::to_string(ai.no());
  if (ai.hand().empty())
    throw "card requested, but the hand is empty";

  auto const card = ai.card_get();

  if (!trick.isvalid(card)) {
    return to_string(ai.hand().validcards(trick)[0]);
  }

  if (::debug("KI")) {
    return to_string(card) + "\n\nKI:\n" + _(dynamic_cast<Ai&>(ai).last_heuristic_rationale());
  }

  return to_string(card);
}


auto announcement(Game& game, Player& ai) -> string
{
  if (game.status() == Game::Status::reservation) {
    set_game_type(game, "normal");
  }
  if (game.status() == Game::Status::start) {
    game.set_status(Game::Status::play);
    game.play(GameplayActions::TrickOpen());
  }
  if (game.status() != Game::Status::play)
    throw "announcement, but game status = " + to_string(game.status()) + " != play";

  auto const announcement = ai.announcement_request();

  if (::debug("KI")) {
    return to_string(announcement) + "\n\nKI:\n" + _(dynamic_cast<Ai&>(ai).last_announcement_rationale());
  }

  return to_string(announcement);
}


#ifdef BUG_REPORT
auto bugreport(Game& game, Player& ai) -> string
{
  // ToDo: Vernünftige Ausgabe, die auch direkt verwendet werden kann:
  // * player type als "ai" statt "dummy ai" ausgeben
  // * Vorbehalte mit ausgeben
  if (game.status() == Game::Status::reservation) {
    set_game_type(game, "normal");
  }
  if (game.status() == Game::Status::start) {
    game.set_status(Game::Status::play);
    game.play(GameplayActions::TrickOpen());
  }

  auto const path = BugReport::create_bug_report(game.party(), "Fehlerbericht vom Rest-Service: Spieler " + ai.name());

  return path;
}
#endif


void parse_query(Party& party, unsigned const playerno, Query const& query)
{
  auto& rule = party.rule();
  rule.reset_to_tournament();
  init_players(party, playerno);

  Game* game = nullptr;
  for (auto const& q : query) {
    if (rule.set(q.key, q.value)) {
      if (party.status() != Party::Status::init)
        throw "Rule given, but party status = " + to_string(party.status()) + " != init";
    } else if (q.key == "startplayer") {
      if (party.status() != Party::Status::init)
        throw "Startplayer given, but party status = " + to_string(party.status()) + " != init";
      auto const startplayer = stoul(q.value);
      if (!(startplayer < 4))
        throw "Startplayer must be between 0 and 3 but is " + q.value;
      party.set_startplayer(startplayer);
    } else if (q.key == "hand") {
      if (party.status() != Party::Status::init)
        throw "Hand given, but party status = " + to_string(party.status()) + " != init";
      party.set_status(Party::Status::play);
      game = &party.create_game();
      auto hands = Hands(rule(Rule::Type::number_of_players_in_game));
      game->init();
      game->set_startplayer(party.startplayer());
      game->players().set_current_player(game->startplayer());
      hands[playerno] = hand_from_string(party, q.value);
      game->distribute_cards(hands);
      game->set_status(Game::Status::reservation);
    } else if (q.key == "game type") {
      if (!game)
        throw "gamplay action given, but party status = " + to_string(party.status());
      set_game_type(*game, q.value);
    } else if (   q.key == "card played"
               || q.key == "announcement") {
      if (!game)
        throw "gamplay action given, but party status = " + to_string(party.status());
      if (game->status() == Game::Status::reservation) {
        set_game_type(*game, "normal");
      }
      if (game->status() == Game::Status::start) {
        game->set_status(Game::Status::play);
        game->play(GameplayActions::TrickOpen());
      }
      if (game->status() != Game::Status::start && game->status() != Game::Status::play)
        throw "gamplay action given, but game status = " + to_string(game->status()) + " != game start/play";
      auto action = GameplayAction::create(q.line());
      game->play(*action);
    } else {
      throw "unknown query: " + q.key + " = " + q.value;
    }
  }
}


void init_players(Party& party, unsigned const playerno)
{
  for (unsigned p = 0; p < party.players().size(); ++p) {
    party.players().set_type(p, (p == playerno
                                 ? Player::Type::ai
                                 : Player::Type::ai_dummy));
  }
}


auto hand_from_string(Party const& party, string const& cards) -> Hand
{
  Hand hand;
  auto const single_cards = String::split(cards, ',');
  for (auto const& card : single_cards)
    hand.add(Card(card));
  if (hand.cardsnumber() != party.rule()(Rule::Type::number_of_tricks_in_game))
    throw "The hand does not have exactly " + std::to_string(party.rule()(Rule::Type::number_of_tricks_in_game)) + " cards";
  return hand;
}


void set_game_type(Game& game, string const& query_data)
{
  if (game.status() != Game::Status::reservation)
    throw "Game type given, but game status = " + to_string(game.status()) + " != game reservation";

  auto const entries = String::split(query_data, ',');
  auto game_type = game_type_from_string(entries[0]);
  game.set_type(game_type);
  if (is_solo(game_type)) {
    if (entries.size() < 2)
      throw "solo game but no soloplayer given";
    auto const soloplayerno = std::stoul(entries[1]);
    game.players().set_soloplayer(game.players().player(soloplayerno));
  }
  if (game_type == GameType::marriage) {
    if (entries.size() < 2)
      throw "marriage but no marriage player given";
    auto const soloplayerno = std::stoul(entries[1]);
    game.players().set_soloplayer(game.players().player(soloplayerno));

    game.marriage().set_selector(entries.size() < 3
                                 ? MarriageSelector::first_foreign
                                 : marriage_selector_from_string(entries[2]));
    if (entries.size() >= 4)
      throw "unknown entry for the game type: " + query_data;
  }
  game.players().set_current_player(game.startplayer());
  game.set_status(Game::Status::start);
  game.teaminfo().set_at_gamestart();
  for (auto& player : game.players()) {
    player.game_start();
  }
}

} // namespace

#endif
