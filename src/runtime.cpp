/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef CHECK_RUNTIME

#include "runtime.h"

// the runtime
Runtime runtime;

Runtime::Runtime() :
  map<string, StopWatch>()
{
  (*this)["program"].start();
}

Runtime::~Runtime()
{
  (*this)["program"].stop();
  for (auto& i : *this) {
    if (i.second.running()) {
#ifdef DKNOF
      cerr << "Stop watch not finished: " << i.first << endl;
#endif
      i.second.stop();
    } // if (i->second.running())
  } // for (i : this)
  cout << *this;
}

auto operator<<(ostream& ostr, Runtime const& runtime) -> ostream&
{
  ostr << "runtime  calls  name\n";
  ostr << "------+-------+-------\n";
  for (auto const& sw : runtime)
    ostr << setw(6) << sw.second.msec() << ": "
    << setw(6) << sw.second.calls() << "  "
      << sw.first << '\n';
  return ostr;
}

#endif // #ifdef CHECK_RUNTIME
