/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include <map>
#include "utils/stop_watch.h"

class Runtime;
extern Runtime runtime;

/**
 ** class to test the runtime of the program
 ** just use '::runtime["marker"].start()' and '...stop()'
 **
 ** @todo     ui wrapper
 **/
class Runtime : public std::map<string, StopWatch> {
    Runtime();
    Runtime(Runtime const&) = delete;
    Runtime& operator=(Runtime const&) = delete;
    ~Runtime();
}; // class Runtime

auto operator<<(std::ostream& ostr, Runtime const& runtime) -> std::ostream&;
