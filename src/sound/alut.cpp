/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_SOUND_ALUT

#include "alut.h"
#include <cstring>

#include "../ui/ui.h"

#include "../utils/file.h"

// number of instances
unsigned Sound::Alut::instances_no = 0;

/** constructor
 **/
Sound::Alut::Alut()
{
  if (Alut::instances_no == 0) {
    // the only option is the name of the program
      int argc_alut = 1;
      auto argv_alut = new char*[2];
      argv_alut[0] = static_cast<char*>(calloc(sizeof(char), strlen(program_name) + 1)); // NOLINT
      strcpy(argv_alut[0], program_name); // NOLINT
      argv_alut[1] = NULL; // NOLINT

      alutInit(&argc_alut, argv_alut);
      if (int error = alutGetError()) // NOLINT
        cerr << "ALUT error: " << alutGetErrorString(error) << endl; // NOLINT
  } // if (Alut::instances_no == 0)

#if 0
  { // print the supported audio file types
    const char * supports = alutGetMIMETypes(ALUT_LOADER_BUFFER);
    cout << supports << endl;
  } // print the supported audio file types
#endif

  ::signal_update.connect(*this, &Alut::check_current_sound,
                          disconnector_);

  Alut::instances_no += 1;
} // Sound::Alut::Alut()

/** destructor
 **/
Sound::Alut::~Alut()
{
  Alut::instances_no -= 1;
  if (Alut::instances_no == 0) {
    alDeleteSources(1, &current_source_);
    alutExit();
#ifdef WORKAROUND
    // without the second call I get the following error message upon terminating the program:
    // AL lib: (EE) alc_cleanup: 1 device not closed
    alutExit();
#endif
  }
} // Sound::Alut::~Alut()

/** play the sound (global group)
 **
 ** @param    position   position of the sound
 ** @param    voice      voice (directory to take the sound from)
 ** @param    type       sound to play
 **/
void
Sound::Alut::play(Position const position,
                  string const& voice,
                  Type const type)
{
  file_queue_.push(Sound::path(position, voice, type));
} // void Sound::Alut::play(Position position, string voice, Type type)

/** @return   whether the sound is still playing
 **/
bool
Sound::Alut::playing() const
{
  ALint val;
  alGetSourcei(current_source_, AL_SOURCE_STATE, &val);
  return (val == AL_PLAYING);
} // bool Sound::Alut::playing() const

/** check the current sound
 **/
void
Sound::Alut::check_current_sound()
{
  if (current_source_) {
    ALint val;
    alGetSourcei(current_source_, AL_SOURCE_STATE, &val);
    if (val != AL_PLAYING) {
      alDeleteSources(1, &current_source_);
      current_source_ = 0;
    }
  } // if (current_source_)

  if (!current_source_)
    play_next_sound();
} // void Sound::Alut::check_current_sound()

/** play the next sound
 **/
void
Sound::Alut::play_next_sound()
{
  if (file_queue_.empty())
    return ;

  if (file_queue_.front().string() == "") {
    file_queue_.pop();
    play_next_sound();
    return ;
  }

  if (false)
  {
    ALuint helloBuffer, helloSource;
    helloBuffer = alutCreateBufferHelloWorld ();
    alGenSources (1, &helloSource);
    alSourcei (helloSource, AL_BUFFER, helloBuffer);
    alSourcePlay (helloSource);
    alutSleep (1);
    return ;
  }
  ALuint buffer, source;
  buffer = alutCreateBufferFromFile(file_queue_.front().string().c_str());
  if (int error = alutGetError()) {
    cerr << "ALUT error: " << alutGetErrorString(error) << endl;
    cerr << "Sound::Alut::play_next_sound()\n"
      << "  Sound file '" << file_queue_.front() << "' not found.\n"
      << "  Skipping it." << endl;
    file_queue_.pop();
    return ;
  }
  alGenSources(1, &source);
  alSourcei(source, AL_BUFFER, buffer);
  alSourcePlay(source);

  if (current_source_)
    alDeleteSources(1, &current_source_);
  current_source_ = source;

  file_queue_.pop();
} // void Sound::Alut::play_next_sound()

#endif // #ifdef USE_SOUND_ALUT
