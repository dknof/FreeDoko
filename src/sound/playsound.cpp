/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_SOUND_PLAYSOUND

#include "playsound.h"

/** Contructor
 **/
Sound::WinPlaySound::WinPlaySound()
{
  ::signal_update.connect(*this, &WinPlaySound::check_current_sound,
                          disconnector_);
} // WinPlaySound::WinPlaySound()

/** Destructor
 **/
Sound::WinPlaySound::~WinPlaySound() = default;

/** play the sound (global group)
 **
 ** @param    position   position of the sound
 ** @param    voice      voice (directory to take the sound from)
 ** @param    type       sound to play
 **/
void
Sound::WinPlaySound::play(Position const position,
                          string const& voice,
                          Type const type)
{
  auto const path = Sound::path(position, voice, type);
  if (!path.empty())
    file_queue_.push(Sound::path(position, voice, type));
} // void Sound::WinPlaySound::play(Position position, string voice, Type type)

/** @return   whether the sound is still playing
 **/
bool
Sound::WinPlaySound::playing() const
{
  return (PlaySound(TEXT(""), NULL,
                    SND_FILENAME | SND_ASYNC | SND_NODEFAULT | SND_NOSTOP)
          == FALSE);
} // bool Sound::WinPlaySound::playing() const

/** check the current sound
 **/
void
Sound::WinPlaySound::check_current_sound()
{
  if (!playing())
    play_next_sound();
} // void Sound::WinPlaySound::check_current_sound()

/** play the next sound
 **/
void
Sound::WinPlaySound::play_next_sound()
{
  if (file_queue_.empty())
    return ;

  PlaySound(TEXT(file_queue_.front().string().c_str()), NULL,
            SND_FILENAME | SND_ASYNC | SND_NODEFAULT | SND_NOWAIT);

  file_queue_.pop();
} // void Sound::WinPlaySound::play_next_sound()

#endif // #ifdef USE_SOUND_PLAYSOUND
