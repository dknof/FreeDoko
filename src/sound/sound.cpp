/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_SOUND

#include "sound.h"
#ifdef USE_SOUND_ALUT
#include "alut.h"
#endif
#ifdef USE_SOUND_COMMAND
#include "command.h"
#endif
#ifdef USE_SOUND_PLAYSOUND
#include "playsound.h"
#endif

#include "../party/party.h"
#include "../game/game.h"
#include "../player/player.h"
#include "../card/trick.h"

#include "../misc/preferences.h"

#include "../utils/string.h"
#include "../utils/file.h"


/** -> result
 **
 ** @param    announcement   the announcement
 **
 ** @return   the type for the announcement
 **/
Sound::Type
Sound::type(GameType const gametype)
{
  switch (gametype) {
  case GameType::normal:
    return Type::gametype_normal;
  case GameType::thrown_nines:
    return Type::gametype_thrown_nines;
  case GameType::thrown_kings:
    return Type::gametype_thrown_kings;
  case GameType::thrown_nines_and_kings:
    return Type::gametype_thrown_nines_and_kings;
  case GameType::thrown_richness:
    return Type::gametype_thrown_richness;
  case GameType::redistribute:
    return Type::gametype_redistribute;
  case GameType::fox_highest_trump:
    return Type::gametype_fox_highest_trump;
  case GameType::poverty:
    return Type::gametype_poverty;
  case GameType::marriage:
    return Type::gametype_marriage;
  case GameType::marriage_solo:
    return Type::gametype_marriage_solo;
  case GameType::marriage_silent:
    return Type::gametype_marriage_silent;
  case GameType::solo_meatless:
    return Type::gametype_solo_meatless;
  case GameType::solo_jack:
    return Type::gametype_solo_jack;
  case GameType::solo_queen:
    return Type::gametype_solo_queen;
  case GameType::solo_king:
    return Type::gametype_solo_king;
  case GameType::solo_queen_jack:
    return Type::gametype_solo_queen_jack;
  case GameType::solo_king_jack:
    return Type::gametype_solo_king_jack;
  case GameType::solo_king_queen:
    return Type::gametype_solo_king_queen;
  case GameType::solo_koehler:
    return Type::gametype_solo_koehler;
  case GameType::solo_club:
    return Type::gametype_solo_club;
  case GameType::solo_heart:
    return Type::gametype_solo_heart;
  case GameType::solo_spade:
    return Type::gametype_solo_spade;
  case GameType::solo_diamond:
    return Type::gametype_solo_diamond;
  } // switch (gametype)

  return Type::first;
} // static Sound::Type Sound::type(GameType gametype)

/** -> result
 **
 ** @param    marriage_selector   the marriage selector
 **
 ** @return   the type for the marriage selector
 **/
Sound::Type
Sound::type(MarriageSelector const marriage_selector)
{
  switch (marriage_selector) {
  case MarriageSelector::team_set:
    return Type::marriage_selector_team_set;
  case MarriageSelector::first_foreign:
    return Type::marriage_selector_first_foreign;
  case MarriageSelector::first_trump:
    return Type::marriage_selector_first_trump;
  case MarriageSelector::first_color:
    return Type::marriage_selector_first_color;
  case MarriageSelector::first_club:
    return Type::marriage_selector_first_club;
  case MarriageSelector::first_spade:
    return Type::marriage_selector_first_spade;
  case MarriageSelector::first_heart:
    return Type::marriage_selector_first_heart;
  case MarriageSelector::silent:
    return Type::marriage_selector_silent;
  } // switch (marriage_selector)

  return Type::first;
} // static Sound::Type Sound::type(MarriageSelector marriage_selector)

/** -> result
 **
 ** @param    announcement   the announcement
 **
 ** @return   the type for the announcement
 **/
Sound::Type
Sound::type(Announcement const announcement)
{
  switch (announcement) {
  case Announcement::noannouncement:
    return Type::announcement_noannouncement;
  case Announcement::no120:
    return Type::announcement_no120;
  case Announcement::no90:
    return Type::announcement_no90;
  case Announcement::no60:
    return Type::announcement_no60;
  case Announcement::no30:
    return Type::announcement_no30;
  case Announcement::no0:
    return Type::announcement_no0;
  case Announcement::reply:
    DEBUG_ASSERTION(false,
                    "Sound::type(" << announcement << "):\n"
                    "  cannot convert the announcement reply.\n");
  } // switch (announcement)

  return Type::first;
} // static Sound::Type Sound::type(Announcement announcement)


/** -> result
 **
 ** @param    type   the sound type
 **
 ** @return   the name of the sound type
 **/
string
Sound::name(Sound::Type const type)
{
  switch (type) {
  case Type::gametype_normal:
    return "gametype: normal";
  case Type::gametype_thrown_nines:
    return "gametype: thrown nines";
  case Type::gametype_thrown_kings:
    return "gametype: thrown kings";
  case Type::gametype_thrown_nines_and_kings:
    return "gametype: thrown nines and kings";
  case Type::gametype_thrown_richness:
    return "gametype: thrown richness";
  case Type::gametype_redistribute:
    return "gametype: redistribute";
  case Type::gametype_fox_highest_trump:
    return "gametype: fox highest trump";
  case Type::gametype_poverty:
    return "gametype: poverty";
  case Type::gametype_marriage:
    return "gametype: marriage";
  case Type::gametype_marriage_solo:
    return "gametype: marriage solo";
  case Type::gametype_marriage_silent:
    return "gametype: marriage silent";
  case Type::gametype_solo_meatless:
    return "gametype: solo meatless";
  case Type::gametype_solo_jack:
    return "gametype: solo jack";
  case Type::gametype_solo_queen:
    return "gametype: solo queen";
  case Type::gametype_solo_king:
    return "gametype: solo king";
  case Type::gametype_solo_queen_jack:
    return "gametype: solo queen jack";
  case Type::gametype_solo_king_jack:
    return "gametype: solo king jack";
  case Type::gametype_solo_king_queen:
    return "gametype: solo king queen";
  case Type::gametype_solo_koehler:
    return "gametype: solo koehler";
  case Type::gametype_solo_club:
    return "gametype: solo club";
  case Type::gametype_solo_heart:
    return "gametype: solo heart";
  case Type::gametype_solo_spade:
    return "gametype: solo spade";
  case Type::gametype_solo_diamond:
    return "gametype: solo diamond";

  case Type::marriage_selector_team_set:
    return "marriage selector: team set";
  case Type::marriage_selector_first_foreign:
    return "marriage selector: first foreign";
  case Type::marriage_selector_first_trump:
    return "marriage selector: first trump";
  case Type::marriage_selector_first_color:
    return "marriage selector: first color";
  case Type::marriage_selector_first_club:
    return "marriage selector: first club";
  case Type::marriage_selector_first_spade:
    return "marriage selector: first spade";
  case Type::marriage_selector_first_heart:
    return "marriage selector: first heart";
  case Type::marriage_selector_silent:
    return "marriage selector: silent";

  case Type::cards_distributed:
    return "cards distributed";
  case Type::trick_move_in_pile:
    return "trick move in pile";
  case Type::card_played:
    return "card played";

  case Type::poverty:
    return "poverty";
  case Type::poverty_denied:
    return "poverty denied";
  case Type::poverty_accepted:
    return "poverty accepted";

  case Type::marriage:
    return "marriage";

  case Type::announcement_noannouncement:
    return "announcement: no announcement";
  case Type::announcement_re:
    return "announcement: re";
  case Type::announcement_contra:
    return "announcement: contra";
  case Type::announcement_no120:
    return "announcement: no 120";
  case Type::announcement_no90:
    return "announcement: no 90";
  case Type::announcement_no60:
    return "announcement: no 60";
  case Type::announcement_no30:
    return "announcement: no 30";
  case Type::announcement_no0:
    return "announcement: no 0";

  case Type::swines:
    return "swines";
  case Type::hyperswines:
    return "hyperswines";

  case Type::no_winner:
    return "no winner";
  case Type::soloplayer_won:
    return "soloplayer won";
  case Type::soloplayer_lost:
    return "soloplayer lost";
  case Type::re_won:
    return "re won";
  case Type::contra_won:
    return "contra won";
  } // switch (type)

  return "";
} // static string Sound::name(Sound::Type type)

/** -> result
 **
 ** @param    type   the sound type
 **
 ** @return   the text for the sound type
 **/
string
Sound::text(Sound::Type const type)
{
  switch (type) {
  case Type::gametype_normal:
    return "normal";
  case Type::gametype_thrown_nines:
    return "thrown nines";
  case Type::gametype_thrown_kings:
    return "thrown kings";
  case Type::gametype_thrown_nines_and_kings:
    return "thrown nines and kings";
  case Type::gametype_thrown_richness:
    return "thrown richness";
  case Type::gametype_redistribute:
    return "redistribute";
  case Type::gametype_fox_highest_trump:
    return "fox highest trump";
  case Type::gametype_poverty:
    return "poverty";
  case Type::gametype_marriage:
    return "marriage";
  case Type::gametype_marriage_solo:
    return "marriage solo";
  case Type::gametype_marriage_silent:
    return "marriage silent";
  case Type::gametype_solo_meatless:
    return "solo meatless";
  case Type::gametype_solo_jack:
    return "solo jack";
  case Type::gametype_solo_queen:
    return "solo queen";
  case Type::gametype_solo_king:
    return "solo king";
  case Type::gametype_solo_queen_jack:
    return "solo queen jack";
  case Type::gametype_solo_king_jack:
    return "solo king jack";
  case Type::gametype_solo_king_queen:
    return "solo king queen";
  case Type::gametype_solo_koehler:
    return "solo koehler";
  case Type::gametype_solo_club:
    return "solo club";
  case Type::gametype_solo_heart:
    return "solo heart";
  case Type::gametype_solo_spade:
    return "solo spade";
  case Type::gametype_solo_diamond:
    return "solo diamond";

  case Type::marriage_selector_team_set:
    return ""; // "team set";
  case Type::marriage_selector_first_foreign:
    return "first foreign";
  case Type::marriage_selector_first_trump:
    return "first trump";
  case Type::marriage_selector_first_color:
    return "first color";
  case Type::marriage_selector_first_club:
    return "first club";
  case Type::marriage_selector_first_spade:
    return "first spade";
  case Type::marriage_selector_first_heart:
    return "first heart";
  case Type::marriage_selector_silent:
    return "silent";

  case Type::cards_distributed:
    return "cards distributed";
  case Type::trick_move_in_pile:
    return "trick move in pile";
  case Type::card_played:
    return "card played";

  case Type::poverty:
    return "poverty";
  case Type::poverty_denied:
    return "poverty denied";
  case Type::poverty_accepted:
    return "poverty accepted";

  case Type::marriage:
    return "marriage";

  case Type::announcement_noannouncement:
    return ""; // "no announcement";
  case Type::announcement_re:
    return "re";
  case Type::announcement_contra:
    return "contra";
  case Type::announcement_no120:
    return "no 120";
  case Type::announcement_no90:
    return "no 90";
  case Type::announcement_no60:
    return "no 60";
  case Type::announcement_no30:
    return "no 30";
  case Type::announcement_no0:
    return "no 0";

  case Type::swines:
    return "swines";
  case Type::hyperswines:
    return "hyperswines";

  case Type::no_winner:
    return "no winner";
  case Type::soloplayer_won:
    return "soloplayer won";
  case Type::soloplayer_lost:
    return "soloplayer lost";
  case Type::re_won:
    return "re won";
  case Type::contra_won:
    return "contra won";
  } // switch (type)

  return "";
} // static string Sound::text(Sound::Type type)

/** -> result
 **
 ** @param    type   the sound type
 **
 ** @return   the file name for the sound type
 **/
string
Sound::filename(Sound::Type const type)
{
  string filename = Sound::text(type);
  String::replace_all(filename, " ",  "_");
  return filename;
} // static string Sound::filename(Sound::Type type)

/** -> result
 **
 ** @param    position   position of the sound
 ** @param    voice      voice (directory to take the sound from)
 ** @param    type       sound to play
 **
 ** @return   the filename of the sound, empty if not found
 **/
Path
Sound::path(Position const position,
            string const& voice,
            Type const type)
{
  // search the sound file
  auto const basename = (Sound::filename(type) + ".wav");
  for (auto const& dir : ::preferences.data_directories()) {
    auto const path = (dir
                       / "sounds"
                       / ::preferences(Preferences::Type::language)
                       / voice
                       / basename);
    if (is_regular_file(path)) {
      return path;
    }
  }
  for (auto const& dir : ::preferences.data_directories()) {
    auto const path = (dir
                       / "sounds"
                       / ::preferences(Preferences::Type::language)
                       / "general"
                       / basename);
    if (is_regular_file(path)) {
      return path;
    }
  }

  return {};
}


/** create a sound class
 **/
unique_ptr<Sound>
Sound::create()
{
#ifdef USE_SOUND_ALUT
  return make_unique<Alut>();
#endif
#ifdef USE_SOUND_COMMAND
  return make_unique<Command>();
#endif
#ifdef USE_SOUND_PLAYSOUND
  return make_unique<WinPlaySound>();
#endif

  DEBUG_ASSERTION(false,
                  "Sound::create()"
                  "  no sound object created");

  return {};
} // static unique_ptr<Sound> Sound::create()

/** constructor
 **/
Sound::Sound()
{
  Game::signal_open.connect(*this, &Sound::game_open, disconnector_);
}

/** play the sound (global group)
 **
 ** @param    type   sound to play
 **/
void
Sound::play(Type const type)
{
  if (::preferences(Preferences::Type::sound))
    play(Position::center, "general", type);
} // virtual void Sound::play(Type type)

/** play the sound (group of the player)
 **
 ** @param    player   player who says it
 ** @param    type     sound to play
 **/
void
Sound::play(Player const& player, Type const type)
{
  if (::preferences(Preferences::Type::sound))
    play(::preferences.position(player.no()), player.voice(), type);
}

/** the game is opened
 **
 ** @param    game   game that is opened
 **/
void
Sound::game_open(Game& game)
{
  game.signal_cards_distributed                .connect(*this, &Sound::game_cards_distributed, disconnector_);
  game.tricks().signal_trick_move_in_pile      .connect(*this, &Sound::trick_move_in_pile, disconnector_);
  game.signal_card_played                      .connect(*this, &Sound::card_played, disconnector_);
  game.signal_start                            .connect(*this, &Sound::game_start, disconnector_);
  game.signal_finish                           .connect(*this, &Sound::game_finish, disconnector_);
  game.poverty().signal_shift                  .connect(*this, &Sound::poverty_shift, disconnector_);
  game.poverty().signal_take_denied            .connect(*this, &Sound::poverty_take_denied, disconnector_);
  game.poverty().signal_take_accepted          .connect(*this, &Sound::poverty_take_accepted, disconnector_);
  game.marriage().signal_marriage              .connect(*this, &Sound::marriage, disconnector_);
  game.announcements().signal_announcement_made.connect(*this, &Sound::announcement_made, disconnector_);
  game.swines().signal_swines_announced        .connect(*this, &Sound::swines_announced, disconnector_);
  game.swines().signal_hyperswines_announced   .connect(*this, &Sound::hyperswines_announced, disconnector_);
} // void Sound::game_open(Game game)

/** the cards are distributed
 **/
void
Sound::game_cards_distributed()
{
  play(Type::cards_distributed);
}

/** the trick is moved into a pile
 **
 ** @param    trick   trick
 **/
void
Sound::trick_move_in_pile(Trick const& trick)
{
  play(trick.winnerplayer(), Type::trick_move_in_pile);
}

/** the card is played
 **
 ** @param    card   card that is played
 **/
void
Sound::card_played(HandCard const card)
{
  play(card.player(), Type::card_played);
}

/** the game is started
 **
 ** @param    game  game that is started
 **/
void
Sound::game_start()
{
  auto const& game = ::party->game();
  switch (game.type()) {
  case GameType::marriage:
    play(game.players().soloplayer(), Sound::type(game.type()));
    play(game.players().soloplayer(), Sound::type(game.marriage().selector()));
    break;
  case GameType::marriage_solo:
  case GameType::marriage_silent:
  case GameType::solo_meatless:
  case GameType::solo_jack:
  case GameType::solo_queen:
  case GameType::solo_king:
  case GameType::solo_queen_jack:
  case GameType::solo_king_jack:
  case GameType::solo_king_queen:
  case GameType::solo_koehler:
  case GameType::solo_club:
  case GameType::solo_heart:
  case GameType::solo_spade:
  case GameType::solo_diamond:
  case GameType::thrown_nines:
  case GameType::thrown_kings:
  case GameType::thrown_nines_and_kings:
  case GameType::redistribute:
  case GameType::fox_highest_trump:
  case GameType::poverty:
    play(game.players().soloplayer(), Sound::type(game.type()));
    break;
  default:
    break;
  } // switch (game.type())
} // void Sound::game_start()

/** the game is finished
 **
 ** @param    game   game that is finished
 **/
void
Sound::game_finish()
{
  auto const& game = ::party->game();
  if (game.winnerteam() == Team::noteam) {
    play(Type::no_winner);
    return ;
  }

  if (is_solo(game.type())) {
    play(  (game.winnerteam() == Team::re)
               ? Type::soloplayer_won
               : Type::soloplayer_lost);
  } else { // if (GameType::is_solo(game.type()))
    play(  (game.winnerteam() == Team::re)
               ? Type::re_won
               : Type::contra_won);
  } // if !(GameType::is_solo(game.type()))
} // void Sound::game_finish()

/** the marriage partner has found a bride
 **
 ** @param    bridegroom   player with the marriage
 ** @param    bride        player who is married
 **/
void
Sound::marriage(Player const& bridegroom, Player const& bride)
{
  play(bridegroom, Type::marriage);
} // void Sound::marriage(Player bridegroom, Player bride)


// poverty

/** 'player' shifts 'cardno' cards
 **
 ** @param    player   player with the poverty
 ** @param    cardno   number of shifted cards
 **/
void
Sound::poverty_shift(Player const& player, unsigned const cardno)
{
#ifdef POSTPONED
  play(player, "poverty: "
             + String::to_string(cardno) + " cards");
#else
  play(player, Type::poverty);
#endif
} // void Sound::poverty_shift(Player player, unsigned cardno)

/** the player 'player' has denied the poverty trumps
 **
 ** @param    player   player who has denied the poverty
 **/
void
Sound::poverty_take_denied(Player const& player)
{
  play(player, Type::poverty_denied);
} // void Sound::poverty_take_denied(Player player)

/** and has returned 'cardno' cards with 'trumpno' trumps
 **
 ** @param    player    player who has accepted the poverty
 ** @param    cardno    number of reshifted cards
 ** @param    trumpno   number of reshifted trumps
 **/
void
Sound::poverty_take_accepted(Player const& player,
                             unsigned const cardno,
                             unsigned const trumpno)
{
#ifdef POSTPONED
  play(player, "poverty accepted: "
             + String::to_string(trumpno) + " trumps");
#else
  play(player, Type::poverty_accepted);
#endif
} // void Sound::poverty_take_accepted(Player player, unsigned cardno, unsigned trumpno)

/** an announcement is made
 **
 ** @param    announcement   announcement made
 ** @param    player         player who has announced
 **/
void
Sound::announcement_made(Announcement const announcement,
                         Player const& player)
{
  if (   announcement == Announcement::no120
      || announcement == Announcement::reply)
    play(player, ((player.team() == Team::re)
                        ? Type::announcement_re
                        : Type::announcement_contra));
  else
    play(player, Sound::type(announcement));
} // void Sound::announcement_made(Announcement announcement, Player player)

/** the player has swines
 **
 ** @param    player    player with the swines
 **/
void Sound::swines_announced(Player const& player)
{
  play(player, Type::swines);
} // void Sound::swines_announced(Player const& player)

/** the player has hyperswines
 **
 ** @param    player   player with the hyperswines
 **/
void Sound::hyperswines_announced(Player const& player)
{
  play(player, Type::hyperswines);
} // void Sound::hyperswines_announced(Player player)

#endif // #ifdef USE_SOUND
