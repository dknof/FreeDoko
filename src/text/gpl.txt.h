auto constexpr gpl_txt = "\
Copyright (C) 2001 - 2024 by Diether Knof and Borg Enders\n\
\n\
FreeDoko is free software: you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License as published by\n\
the Free Software Foundation, either version 3 of the License, or\n\
(at your option) any later version.\n\
\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n\
GNU General Public License for more details.\n\
\n\
You should have received a copy of the GNU General Public License\n\
along with this program.  If not, see <https://www.gnu.org/licenses/>.\n\
\n\
Contact:\n\
Diether Knof dknof@posteo.de\n\
\n\
You can find the whole GPL in the doc-directory \n\
(normally /usr/share/doc/FreeDoko/COPYING \n\
or C:\\Program Files\\FreeDoko\\doc\\COPYING).\n\
";
