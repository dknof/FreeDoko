arguments:
-h -?
--help
	print this help, quits

-v
--version
	print the version, quits

-L
--license
	print some info about the license (GPL), quits

--defines
	print the defines (compile time), quits

--directories
	print the directories FreeDoko searchs, quits

--po
	prints information of the translation, quits

--debug [string]
	output debug information (p.e. gameplay, heuristics, table)
	"heuristics": output for all heuristics (that support it)
	heuristic name: output for a specific heuristics
	"solo decision": only play solo games and write the winner
	"solo decision rationale": write the rationale for solo decisions
	"init ui": the ui initialization
	"table": display the layout of the table
	"mouse": mouse coordinates
	"screenshots": create screenshots for each card played (in the desktop)
	"pdf": create a pdf with one page per full trick (in the current directory)
	The following flags regulate additional information in each output
	"codeline": add information about the code position (default on)
	"time": add time information (default off)
	"clock": add processor time information (default off)
	"color": use color for the output (p.e. for terminals in linux)

-u
--ui [string]
	setting the ui type.
	til now, only 'gtkmm' is implemented

--settings [string]
	file with (extra) settings to load

-n
--name [string]
	the name of the player
-l
--language [string]
	the language (shortform or longform)

-C
--cardset [string]
	the cardset

-H
--cards-height [number]
	the height of the cards

--width [number]
	the width of the main window

--height [number]
	the height of the main window

--fullscreen
	set fullscreen mode

-s
--seed [number]
        set the starting seed
        this value will be overwritten by a loaded party, so use with '-'

-b
--bug-report [filename]
	replay the bug report

-r
--references [dirname]
	check the references in the directory

--auto-bug-report [condition]
        automatically create bug reports for the given condition
	- game:         game started
	- trick:        trick opened
	- card:         card played
	- no heuristic: no heuristic found
	- dulle lost
	- fox lost
	- swine lost
	- poverty lost
	- solo lost
	- bad announcement
	- no announcement

--auto-bug-reports
        automatically create bug reports for hardcoded situations (see above: *lost and *announcement)

--fast-play
        automate some behaviour
        bitwise settings:
          1  automatic party start
          2  ai players
          4  random ai
          8  skip pauses
         16  skip gameplay informations
         32  skip game finished dialog
         64  skip party finished dialog
        128  show all hands
        256  quit when finished
        512  seed info (see below)

--seed-info
        print the seeds and some information on stdout
        (overwrites the bit in 'fast-play')

--config-directory [path]
        set the directory for the configuration files

--no-automatic-savings
        do save nothing automatically

--threads [number]
	number of threads to use when calculating
	(thread support must have been enabled for compiling)

[filename] 	
	play the party in the file
	Per default the file 'current' in the 'parties' subdirectory is taken.
	If the file ends in .BugReport.FreeDoko or .Reference.FreeDoko the file is treated as a bug report.


-
	when used as first option:
	do load neither the rules, the players nor the current party


Report bugs to freedoko@users.sourceforge.net
