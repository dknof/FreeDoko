static auto const help_txt = "\
FreeDoko -- Version " + to_string(*::version) + "\n\
\n\
arguments:\n\
-h -?\n\
--help\n\
	print this help, quits\n\
\n\
-v\n\
--version\n\
	print the version, quits\n\
\n\
-L\n\
--license\n\
	print some info about the license (GPL), quits\n\
\n\
--defines\n\
	print the defines (compile time), quits\n\
\n\
--directories\n\
	print the directories FreeDoko searchs, quits\n\
\n\
--po\n\
	prints information of the translation, quits\n\
\n\
--debug [string]\n\
	output debug information (p.e. gameplay, heuristics, table)\n\
	\"heuristics\": output for all heuristics (that support it)\n\
	heuristic name: output for a specific heuristics\n\
	\"solo decision\": only play solo games and write the winner\n\
	\"solo decision rationale\": write the rationale for solo decisions\n\
	\"init ui\": the ui initialization\n\
	\"table\": display the layout of the table\n\
	\"mouse\": mouse coordinates\n\
	\"screenshots\": create screenshots for each card played (in the desktop)\n\
	\"pdf\": create a pdf with one page per full trick (in the current directory)\n\
	The following flags regulate additional information in each output\n\
	\"codeline\": add information about the code position (default on)\n\
	\"time\": add time information (default off)\n\
	\"clock\": add processor time information (default off)\n\
	\"color\": use color for the output (p.e. for terminals in linux)\n\
\n\
-u\n\
--ui [string]\n\
	setting the ui type.\n\
	til now, only 'gtkmm' is implemented\n\
\n\
--settings [string]\n\
	file with (extra) settings to load\n\
\n\
-n\n\
--name [string]\n\
	the name of the player\n\
-l\n\
--language [string]\n\
	the language (shortform or longform)\n\
\n\
-C\n\
--cardset [string]\n\
	the cardset\n\
\n\
-H\n\
--cards-height [number]\n\
	the height of the cards\n\
\n\
--width [number]\n\
	the width of the main window\n\
\n\
--height [number]\n\
	the height of the main window\n\
\n\
--fullscreen\n\
	set fullscreen mode\n\
\n\
-s\n\
--seed [number]\n\
        set the starting seed\n\
        this value will be overwritten by a loaded party, so use with '-'\n\
\n\
-b\n\
--bug-report [filename]\n\
	replay the bug report\n\
\n\
-r\n\
--references [dirname]\n\
	check the references in the directory\n\
\n\
--auto-bug-report [condition]\n\
        automatically create bug reports for the given condition\n\
	- game:         game started\n\
	- trick:        trick opened\n\
	- card:         card played\n\
	- no heuristic: no heuristic found\n\
	- dulle lost\n\
	- fox lost\n\
	- swine lost\n\
	- poverty lost\n\
	- solo lost\n\
	- bad announcement\n\
	- no announcement\n\
\n\
--auto-bug-reports\n\
        automatically create bug reports for hardcoded situations (see above: *lost and *announcement)\n\
\n\
--fast-play\n\
        automate some behaviour\n\
        bitwise settings:\n\
          1  automatic party start\n\
          2  ai players\n\
          4  random ai\n\
          8  skip pauses\n\
         16  skip gameplay informations\n\
         32  skip game finished dialog\n\
         64  skip party finished dialog\n\
        128  show all hands\n\
        256  quit when finished\n\
        512  seed info (see below)\n\
\n\
--seed-info\n\
        print the seeds and some information on stdout\n\
        (overwrites the bit in 'fast-play')\n\
\n\
--config-directory [path]\n\
        set the directory for the configuration files\n\
\n\
--no-automatic-savings\n\
        do save nothing automatically\n\
\n\
--threads [number]\n\
	number of threads to use when calculating\n\
	(thread support must have been enabled for compiling)\n\
\n\
[filename] 	\n\
	play the party in the file\n\
	Per default the file 'current' in the 'parties' subdirectory is taken.\n\
	If the file ends in .BugReport.FreeDoko or .Reference.FreeDoko the file is treated as a bug report.\n\
\n\
\n\
-\n\
	when used as first option:\n\
	do load neither the rules, the players nor the current party\n\
\n\
\n\
Report bugs to freedoko@users.sourceforge.net\n\
";
