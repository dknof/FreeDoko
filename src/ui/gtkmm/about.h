/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"
#include "widgets/sticky_dialog.h"
namespace Gtk {
class TextView;
} // namespace Gtk

namespace UI_GTKMM_NS {

/**
 ** the about dialog
 **/
class About : public Base, public Gtk::StickyDialog {
  public:
    About() = delete;
    explicit About(Base* parent);
    About(About const&) = delete;
    About& operator=(About const&) = delete;
    ~About() override;

  private:
    void init();

  private:
    Gtk::TextView* text = nullptr;
};

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
