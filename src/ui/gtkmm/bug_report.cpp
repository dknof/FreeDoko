/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "bug_report.h"

#include "ui.h"
#include "party_settings.h"
#include "table.h"

#include "../../misc/preferences.h"
#include "../../misc/bug_report.h"
#include "../../os/bug_report_replay.h"

#include "../../utils/file.h"
#include "../../utils/string.h"

#include <gtkmm/scrolledwindow.h>
#include <gtkmm/label.h>
#include <gtkmm/textview.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/checkbutton.h>

#include <cstring>

namespace UI_GTKMM_NS {

/// @todo   Mail the bug report directly:
///         ShellExecute(NULL,"open","mailto:adresse@bla.at?cc=adresse2@bla.at&subject=Überschrift&body=Text",NULL,NULL,NULL);
///         http://www.ianr.unl.edu/internet/mailto.html

/** constructor
 **
 ** @param    parent   the parent widget
 ** @param    message   the default message
 **/
BugReport::BugReport(Base* const parent, string const text) :
  Base(parent),
  StickyDialog(_("Window::BugReport"), false)
{
  ui->add_window(*this);

  message = Gtk::manage(new Gtk::TextView());
  message->get_buffer()->set_text(text);

  { // load file chooser
    load_file_chooser = make_unique<Gtk::FileChooserDialogWrapper>(_("load bug report"),
                                                                         Gtk::FILE_CHOOSER_ACTION_OPEN,
                                                                         sigc::mem_fun(*this, &BugReport::load));
#ifndef RELEASE
    // ToDo: add current directory
    if (is_directory(std::filesystem::current_path() / "../Bugreports"))
      load_file_chooser->set_current_folder((std::filesystem::current_path() / "../Bugreports").string());
    else
      load_file_chooser->set_current_folder(std::filesystem::current_path().string());
#else
    load_file_chooser->set_current_folder(std::filesystem::current_path().string());
#endif
#ifdef POSTPONED
    {
      auto filter = new Gtk::FileFilter; // memory bug?
      filter->add_pattern("*.BugReport.FreeDoko");
      load_file_chooser->add_filter(*filter);
    }
#endif
  } // load file chooser

  signal_realize().connect(sigc::mem_fun(*this, &BugReport::init));
  set_dnd_destination(*this);
}

/** destruktor
 **/
BugReport::~BugReport() = default;

/** create all subelements
 **/
void
BugReport::init()
{
  set_icon(ui->icon);

  description = Gtk::manage(new Gtk::TextView());
  description->get_buffer()->set_text(_("BugReport::save"));
  file_label = Gtk::manage(new Gtk::Label(_("file: %s", ::BugReport::default_filename(ui->party()))));
  save_on_desktop_button = Gtk::manage(new Gtk::CheckButton(_("Button::save on desktop")));
  dismiss_button = Gtk::manage(new Gtk::Button(_("Button::dismiss")));
  dismiss_button->set_image_from_icon_name("window-close");
  dismiss_button->set_always_show_image();
  add_action_widget(*dismiss_button, Gtk::RESPONSE_CLOSE);
  save_button = Gtk::manage(new Gtk::Button(_("Button::save")));
  save_button->set_image_from_icon_name("document-save");
  save_button->set_always_show_image();
  add_action_widget(*save_button, Gtk::RESPONSE_CLOSE);

  save_button->set_can_default();
  save_button->grab_default();

  set_default_size(25 EM, 40 EX);

  { // the description
    description->set_editable(false);
    description->set_wrap_mode(Gtk::WRAP_WORD);
    description->set_cursor_visible(false);
    description->set_margin_bottom(1 EX);

    get_content_area()->pack_start(*description, false, true);
  } // the description
  get_content_area()->pack_start(*file_label, false, true);
  get_content_area()->pack_start(*save_on_desktop_button, false, true);
  { // the message
    auto message_window = Gtk::manage(new Gtk::ScrolledWindow());
    message_window->add(*message);
    message_window->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    message->set_editable(true);
    message->set_wrap_mode(Gtk::WRAP_WORD);
    message->set_cursor_visible(true);
    message_window->set_margin_top(1 EX);

    get_content_area()->pack_end(*message_window, true, true);
  } // the message

  show_all_children();
#ifndef WINDOWS
#ifndef LINUX
  save_on_desktop_button->hide();
#endif
#endif

  // signals
  dismiss_button->signal_clicked().connect(sigc::mem_fun(*this, &Gtk::Widget::hide));
  save_button->signal_clicked().connect(sigc::mem_fun(*this, &BugReport::save));
  save_button->signal_clicked().connect(sigc::mem_fun(*this, &Gtk::Widget::hide));

  // events
  save_on_desktop_button->signal_toggled().connect(sigc::mem_fun(*this, &BugReport::save_on_desktop_toggled_event));
  auto update_button = [this](bool value) { save_on_desktop_button->set_active(value); };
  update_button(::preferences(::Preferences::Type::save_bug_reports_on_desktop));
  ::preferences.signal_changed(::Preferences::Type::save_bug_reports_on_desktop).connect_back(update_button, disconnector_);

} // void BugReport::init()

/** load the given bug report
 **
 ** @param    filename   bug report to load
 **
 ** @return   whether the loading was successfull
 **/
bool
BugReport::load(string const filename)
{
  // Note: '::bug_report_replay' is set automatically to the
  //       newly opened bug report.
  ::bug_report_replay
    = make_unique<OS_NS::BugReportReplay>(filename,
                                          OS_NS::BugReportReplay::verbose_all);
  // this is true, when the loading was successfull
  if (!bug_report_replay->loaded()) {
    ::bug_report_replay = {};
    return false;
  }

  if (   ui->party().status() == Party::Status::init
      || ui->party().status() == Party::Status::initial_loaded) {
    ui->party_settings->update();
  } else if (ui->party().status() >= Party::Status::loaded) {
    ::party->close();
    ui->thrower(Party::Status::loaded,
                      __FILE__, __LINE__);
  }
  return true;
} // bool BugReport::load(string const& filename)

/** creates a report
 **/
void
BugReport::create_report()
{
  realize();

  message->get_buffer()->set_text("");
  if (file_label->get_realized())
    file_label->set_label(_("file: %s", ::BugReport::default_filename(ui->party())));
  message->grab_focus();

  present();
} // void BugReport::create_report()

/** save the report
 **/
void
BugReport::save()
{
  auto const filename
    = ::BugReport::create_bug_report(ui->party(),
                                     message->get_buffer()->get_text()
                                     + "\n"
                                     + (ui->table->in_game_review()
                                        ? ("---\n"
                                           + ("Game review, trick: " + String::to_string(ui->table->game_review_trickno()) + "\n\n"
                                              + String::to_string(ui->table->game_review_trick())
                                              +"\n")
                                          )
                                        : string()));

  cerr << "Saved bug report in the file '" << filename << "'." << endl;
} // void BugReport::save()

/** the button 'save on desktop' has been toggled
 **/
void
BugReport::save_on_desktop_toggled_event()
{
  ::preferences.set(::Preferences::Type::save_bug_reports_on_desktop,
                    save_on_desktop_button->get_active());
} // void BugReport::save_on_desktop_toggled_event()

/** sets the widget as destination for the drop of a bug report
 **
 ** @param    widget   widget to set as destination
 **/
void
BugReport::set_dnd_destination(Gtk::Widget& widget)
{
  std::vector<Gtk::TargetEntry> list_targets;
  list_targets.emplace_back("text/plain");

  widget.drag_dest_set(list_targets);

  widget.signal_drag_data_received().connect(sigc::mem_fun(*this, &BugReport::on_bug_report_dropped));
}

/** drag-and-drop:
 ** - bug report
 ** - tournament
 **
 ** @param    context          ?
 ** @param    x                ?
 ** @param    y                ?
 ** @param    selection_data   ?
 ** @param    info             ?
 ** @param    time             ?
 **
 ** @todo   tournament
 **
 ** @bug   copied code from the gtkmm example, not really understood
 **/
void
BugReport::on_bug_report_dropped(Glib::RefPtr<Gdk::DragContext> const&
                                 context,
                                 int const x, int const y,
                                 Gtk::SelectionData const& selection_data,
                                 guint const info, guint const time)
{
  // what is 'format'?
  if (   (selection_data.get_length() >= 0)
      && (selection_data.get_format() == 8)) { // !!! what is the format?

    // if a file is dropped, 'data' is 'file:///path/to/file'
    // so check whether the data is of the form

    // !!! Bug?
    // 'selection_data.data' is 'guchar*', but there is no
    // constructor 'Glib::ustring(guchar*)'
    Glib::ustring type(selection_data.get_text());

    if (string(type, 0, strlen("file://")) != "file://")
      return ;

    // If more than one file is dropped each file is in one line.
    if (type.find('\n') != type.rfind('\n'))
      return ;

    // so we have here a file

    string path(type, strlen("file://"));

    // remove the EOL
    if (*(path.end() - 1) == '\n')
      path.erase(path.end() - 1);
    if (*(path.end() - 1) == '\r')
      path.erase(path.end() - 1);

#ifdef OUTDATED
    // ToDo: korrigieren

    // simple check for a bug report
    if (string(File::basename(path),
               0, strlen("FreeDoko.BugReport"))
        == "FreeDoko.BugReport") {

      if (ui->bug_report->load(path))
        context->drag_finish(false, false, time);
    } // a bug report
#endif

  } // if ()
} // void BugReport::on_bug_report_dropped(...)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
