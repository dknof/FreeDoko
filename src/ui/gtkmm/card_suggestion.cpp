/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "card_suggestion.h"
#include "table.h"
#include "ui.h"
#include "cards.h"

#include "../../party/party.h"
#include "../../game/game.h"
#include "../../player/player.h"
#include "../../player/ai/ai.h"
#include "../../os/bug_report_replay.h"

#include <gtkmm/box.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/textview.h>
#include <gtkmm/main.h>
#include <gdk/gdkkeysyms.h>

namespace UI_GTKMM_NS {

CardSuggestion::CardSuggestion(Table* const table) :
  Base(table),
  Gtk::StickyDialog("FreeDoko – " + _("Window::Card suggestion"))
{
  signal_realize().connect(sigc::mem_fun(*this, &CardSuggestion::init));
}


CardSuggestion::~CardSuggestion() = default;


void CardSuggestion::init()
{
  { // action area
    add_close_button(*this);

    play_button = Gtk::manage(new Gtk::Button(_("Button::play card")));
    play_button->set_image_from_icon_name("dialog-ok-apply");
    play_button->set_always_show_image();
    add_action_widget(*play_button, Gtk::RESPONSE_NONE);
    play_button->signal_clicked().connect(sigc::mem_fun(*this, &CardSuggestion::play_card));

    play_button->set_can_default();
    play_button->grab_default();
    play_button->grab_focus();
    play_button->add_accelerator("activate",
                                       get_accel_group(),
                                       GDK_KEY_t,
                                       Gdk::ModifierType(0),
                                       Gtk::AccelFlags(0));
  } // action area

  { // vbox
    get_content_area()->set_spacing(20);

    card_box = Gtk::manage(new Gtk::EventBox());
    card_box->signal_button_press_event().connect(sigc::mem_fun(*this, &CardSuggestion::play_card_signal));

    card_image = ui->cards->new_managed_image();
    card_box->add(*card_image);
    card_image->set_padding(2 EM, 2 EX);
    heuristics_text = Gtk::manage(new Gtk::Label(_("Thinking, please wait.")));
    heuristics_description = Gtk::manage(new Gtk::TextView);
    heuristics_description->set_editable(false);
    heuristics_description->set_wrap_mode(Gtk::WRAP_WORD);
    heuristics_description->get_buffer()->set_text(_("Thinking, please wait."));

    get_content_area()->pack_start(*card_box, Gtk::PACK_SHRINK);
    get_content_area()->pack_start(*heuristics_text, Gtk::PACK_SHRINK);
    get_content_area()->pack_start(*heuristics_description);
  } // vbox

  show_all_children();
}


auto CardSuggestion::player() const -> ::Player const&
{
  return ui->game().players().current_player();
}


void CardSuggestion::show_information(bool const show_window)
{
  if (!get_realized())
    realize();

  if (show_window)
    present();

  get_suggestion();
}


void CardSuggestion::play_card()
{
  if (suggested_card.is_empty())
    return ;

  const_cast<Player&>(player()).hand().request_card(suggested_card);
  ui->thrower(suggested_card, __FILE__, __LINE__);
}


void CardSuggestion::card_played()
{
  //hide();
  suggested_card = Card();
  ui->cards->change_managed(card_image, Card());
  heuristics_text->set_label("");
  heuristics_description->get_buffer()->set_text("");
  play_button->set_sensitive(false);
}


void CardSuggestion::get_suggestion()
{
  DEBUG_ASSERTION(dynamic_cast<Ai const*>(&player()),
                  "CardSuggestion::get_Suggestion():\n"
                  "  'player' cannot be cast to an ai.");

  if (thinking)
    return ;

  auto const& ai = dynamic_cast<::Ai const&>(player());
  if (suggested_card.is_empty()
      || (   ::bug_report_replay
          && (player().type() == Player::Type::human))) {
    thinking = true;

    heuristics_text->set_label(_("Thinking, please wait."));
    heuristics_description->get_buffer()->set_text("");
    ui->cards->change_managed(card_image, Card());

    play_button->set_sensitive(false);

    auto const thrower_depth = ui->thrower.depth();
    ui->thrower.set_depth(0);
    try {
      suggested_card = const_cast<Ai&>(ai).card_suggestion();
      if (ai.hand().contains(suggested_card))
        const_cast<Ai&>(ai).hand().request_card(suggested_card);
    } catch (Card const& card) {
      ui->thrower(card, __FILE__, __LINE__);
    } catch (Party::Status const status) {
      ui->thrower(status, __FILE__, __LINE__);
    } catch (Game::Status const status) {
      ui->thrower(status, __FILE__, __LINE__);
    } catch (...) {
      thinking = false;
      ui->thrower.set_depth(thrower_depth);
      throw;
    } // try
    ui->thrower.set_depth(thrower_depth);

    thinking = false;
  } // if (!suggested_card)

  dynamic_cast<Table*>(parent)->draw_all();

  ui->cards->change_managed(card_image, suggested_card);
  heuristics_text->set_label(_(ai.last_heuristic()));
  auto buffer = heuristics_description->get_buffer();
  buffer->set_text(_("AiConfig::Heuristic::Description::" + to_string(ai.last_heuristic())));
  auto const rationale_text = ai.last_heuristic_rationale().text();
  if (!rationale_text.empty()) {
    buffer->insert(buffer->end(),
                   "\n\n" + _("decision logic") + ":\n" + rationale_text);
  }

  play_button->set_sensitive(true);
}


auto CardSuggestion::play_card_signal(GdkEventButton* event) -> bool
{
  if (suggested_card.is_empty())
    return false;

  play_card();

  return true;
}


void CardSuggestion::mouse_click_on_table()
{ }

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
