/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "cards.h"

#include "ui.h"
#include "splash_screen.h"
#include "help.h"

#include "../status_message.h"

#include "../../party/party.h"
#include "../../misc/preferences.h"

#include "../../utils/file.h"

#include <gtkmm/image.h>
#include <gtkmm/messagedialog.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 **/
Cards::Cards(Base* const parent) :
  Base(parent)
{
  load_all();
  ::preferences.signal_changed(::Preferences::Type::cardset).connect([this](auto) {load_all();}, disconnector_);
  ::preferences.signal_changed(::Preferences::Type::cards_back).connect([this](auto) {load_back();}, disconnector_);
  ::preferences.signal_changed(::Preferences::Type::cards_height).connect([this](auto) {update_scaling();}, disconnector_);
  ::preferences.signal_changed(::Preferences::Type::original_cards_size).connect([this](auto) {::preferences.set(::Preferences::Type::cards_height, height_original());}, disconnector_);
}

/** destructor
 **/
Cards::~Cards() = default;

/** @return   the scaling
 **/
double
Cards::scaling() const
{ return scaling_; }

/** update the scaling
 **/
void
Cards::update_scaling()
{
  if (::preferences(::Preferences::Type::original_cards_size))
    scaling_ = 1;
  else
    scaling_ = (static_cast<double>(::preferences(::Preferences::Type::cards_height))
                      / height_original());

  // make that the width cannot get '0'
  if (static_cast<unsigned>(scaling_ * width_original())
      < ::preferences.min(::Preferences::Type::cards_height))
    scaling_
      = (static_cast<double>(::preferences.min(::Preferences::Type::cards_height))
         / width_original());


  // set the scaling of the 'ScaledPixbuf'
  back_.set_scaling(scaling());
  for (auto& c : card_) {
    for (auto& v : c) {
      v.set_scaling(scaling_);
    }
  }
} // void Cards::update_scaling()

/** @return   the original width of the cards
 **/
int
Cards::width_original() const
{
  DEBUG_ASSERTION(back_,
                  "Cards::width_original():\n"
                  "  'back_' is empty");

  return back_.get_orig_width();
}

/** @return   the original height of the cards
 **/
int
Cards::height_original() const
{
  DEBUG_ASSERTION(back_,
                  "Cards::height_original():\n"
                  "  'back_' is empty");

  return back_.get_orig_height();
}


/** @return   the width of the cards
 **/
int
Cards::width(Rotation const rotation) const
{
  switch (rotation) {
  case Rotation::up:
  case Rotation::down:
    return back_.get_width();
  case Rotation::left:
  case Rotation::right:
    return back_.get_height();
  }
  return 0;
}

/** @return   the height of the cards
 **/
int
Cards::height(Rotation const rotation) const
{
  switch (rotation) {
  case Rotation::up:
  case Rotation::down:
    return back_.get_height();
  case Rotation::left:
  case Rotation::right:
    return back_.get_width();
  }
  return 0;
}

/** @return   the back of the cards
 **/
Gdk::ScaledPixbuf&
Cards::back()
{
  return back_;
}

/** -> result
 **
 ** @param    card      the card
 **
 ** @return   the pixbuf of 'card'
 **/
Gdk::ScaledPixbuf&
Cards::card(Card const card)
{
  if (   (card == Card::empty)
      || (card == Card::unknown))
    return back();

  // a pointer to the vector with all cards with 'card.color'
  vector<Gdk::ScaledPixbuf>* card_color = nullptr;
  switch (card.color()) {
  case Card::club:
    card_color = &(card_[0]);
    break;
  case Card::spade:
    card_color = &(card_[1]);
    break;
  case Card::heart:
    card_color = &(card_[2]);
    break;
  case Card::diamond:
    card_color = &(card_[3]);
    break;
  default:
    DEBUG_ASSERTION(false,
                    "Cards::card(card):\n"
                    "  card.color '" << card.color() << "' not valid.");
    break;
  } // switch(card.color())

  // a pointer to the vector with all cards with 'card'
  Gdk::ScaledPixbuf* card_color_value = nullptr;
  switch (card.value()) {
  case Card::ace:
    card_color_value = &((*card_color)[0]);
    break;
  case Card::ten:
    card_color_value = &((*card_color)[1]);
    break;
  case Card::king:
    card_color_value = &((*card_color)[2]);
    break;
  case Card::queen:
    card_color_value = &((*card_color)[3]);
    break;
  case Card::jack:
    card_color_value = &((*card_color)[4]);
    break;
  case Card::nine:
    card_color_value = &((*card_color)[5]);
    break;
  default:
    DEBUG_ASSERTION(false,
                    "Cards::card(card):\n"
                    "  card.value '" << card.value() << "' not valid.");
    break;
  } // switch(card.color())

  return *card_color_value;
} // Gdk::ScaledPixbuf& Cards::card(Card card)

/** @return   the back of the cards (scaled)
 **/
Glib::RefPtr<Gdk::Pixbuf>
Cards::back_original()
{
  return back().orig_pixbuf();
}

/** -> result
 **
 ** @param    card      the card
 **
 ** @return   the pixbuf of 'card' (original)
 **/
Glib::RefPtr<Gdk::Pixbuf>
Cards::card_original(Card const card)
{
  DEBUG_ASSERTION(this->card(card),
                  "Cards::card_original(card):\n"
                  "  'card' is null");
  DEBUG_ASSERTION(this->card(card).orig_pixbuf(),
                  "Cards::card_original(card):\n"
                  "  'card.orig_pixbuf()' is null");

  return this->card(card).orig_pixbuf();
}

/** -> result
 **
 ** @param    card   card
 **           (default: Card())
 **
 ** @return   new image with the card 'card' that is managed
 **           (that is size and theme)
 **/
Gtk::Image*
Cards::new_managed_image(Card const card)
{
  auto image = Gtk::manage(new Gtk::Image(this->card(card)));
  image->set_data("card", new Card(card));

  image->signal_unrealize().connect(sigc::bind<Gtk::Image const* const>(sigc::mem_fun(*this, &Cards::remove_managed), image));

  managed_image_.push_back(image);

  return image;
}

/** changes the type of 'image'
 **
 ** @param    image   image to change the type of
 ** @param    card   new card
 **/
void
Cards::change_managed(Gtk::Image* const image, Card const card)
{
  DEBUG_ASSERTION(image->get_data("card"),
                  "Cards::change_managed:\n"
                  "  image has no data 'type'");
  // ToDo: Test whether the image is in the vector

  delete static_cast<Card*>(image->steal_data("card"));
  image->set_data("card", new Card(card));

  image->set(this->card(card));
}

/** remove 'image' from the managed image list
 **
 ** @param    image   image to remove
 **/
void
Cards::remove_managed(Gtk::Image const* const image)
{
  for (auto i = managed_image_.begin();
       i != managed_image_.end();
      )
    if (*i == image) {
      delete static_cast<Card*>((*i)->steal_data("card"));
      i = managed_image_.erase(i);
      break;
    } else {
      ++i;
    }
} // void Cards::remove_managed(Gtk::Image const* image)

/** update the managed images
 **/
void
Cards::update_managed()
{
  for (auto i : managed_image_) {
    auto const card = *(static_cast<Card*>(i->get_data("card")));
    i->set(this->card(card));
  }
}

/** load the cards and the back
 **/
void
Cards::load_all()
{
  load_back();
  load_cards();
}

/** load the cards back
 **/
void
Cards::load_back()
{
  ui->add_status_message(_("loading the cards (cards back)"));
  auto back_new
    = Gdk::ScaledPixbuf(::preferences.path(::Preferences::Type::cards_back).string());
  if (!back_new) {
    if (!back_) {
      auto const back = (::preferences.value(::Preferences::Type::cards_back).empty()
                         ? ::preferences(::Preferences::Type::cards_back)
                         : ::preferences.value(::Preferences::Type::cards_back));
      auto const cardset = (::preferences.value(::Preferences::Type::cardset).empty()
                            ? ::preferences(::Preferences::Type::cardset)
                            : ::preferences.value(::Preferences::Type::cardset));
      // gettext: %s = back, %s = cardset
      ui->error(_("Error::loading the card back %s (%s)",
                        back, cardset));
    }
    return ;
  } // if (!back_new)

  back_ = back_new;
  back_.set_scaling(scaling());

  if (::preferences(::Preferences::Type::original_cards_size) == true)
    ::preferences.set(::Preferences::Type::cards_height, height_original());

  update_scaling();

  ui->remove_status_message(_("loading the cards (cards back)"));
} // void Cards::load_back()

/** load the cards
 **/
void
Cards::load_cards()
{
  StatusMessages sm(*ui);
  if (   ::party
      && ::party->status() != Party::Status::programstart
      && ::party->status() != Party::Status::initial_loaded) {
    sm.add(_("loading the cards"));
  }

  // load the cards
  vector<Card::Color> const color = {Card::club, Card::spade, Card::heart, Card::diamond};
  vector<Card::Value> const value = {Card::ace, Card::ten, Card::king, Card::queen, Card::jack, Card::nine};

  vector<vector<Gdk::ScaledPixbuf>> card_new;
  for (auto c : color) {
    card_new.emplace_back();
    for (auto v : value) {
      auto card = Card(c, v);
      sm.add(_("loading the cards (%t)", _(card)));
      card_new.back().emplace_back(path(card));
      card_new.back().back().set_scaling(scaling());
      if (!card_new.back().back()) {
        auto const cardset = (::preferences.value(::Preferences::Type::cardset).empty()
                              ? ::preferences(::Preferences::Type::cardset)
                              : ::preferences.value(::Preferences::Type::cardset));
        // gettext: %t = cardn, %s = cardset
        ui->error(_("Error::loading the card %t (%s)",
                    _(card), cardset));
      }
      if (   !::party
          || ::party->status() == Party::Status::programstart
          || ::party->status() == Party::Status::initial_loaded)
        ui->add_progress(1.0 / (color.size() * value.size()));
    } // for (v \in value)
  } // for (c \in color)

  card_ = card_new;
} // void Cards::load_cards()

/** @return   the path to the card
 **/
string
Cards::path(Card const card)
{
  switch (::preferences.cardset_format()) {
  case CardsetFormat::freedoko:
    return (::preferences.path(::Preferences::Type::cardset)
            /"cards"/ (to_string(card.color()) + "_" + to_string(card.value()) + ".png")
           ).string();
  case CardsetFormat::pysol:
    return (::preferences.path(::Preferences::Type::cardset)
            / (  card == Card::club_ace      ? "01c.gif"
               : card == Card::spade_ace     ? "01s.gif"
               : card == Card::heart_ace     ? "01h.gif"
               : card == Card::diamond_ace   ? "01d.gif"
               : card == Card::club_ten      ? "10c.gif"
               : card == Card::spade_ten     ? "10s.gif"
               : card == Card::heart_ten     ? "10h.gif"
               : card == Card::diamond_ten   ? "10d.gif"
               : card == Card::club_king     ? "13c.gif"
               : card == Card::spade_king    ? "13s.gif"
               : card == Card::heart_king    ? "13h.gif"
               : card == Card::diamond_king  ? "13d.gif"
               : card == Card::club_queen    ? "12c.gif"
               : card == Card::spade_queen   ? "12s.gif"
               : card == Card::heart_queen   ? "12h.gif"
               : card == Card::diamond_queen ? "12d.gif"
               : card == Card::club_jack     ? "11c.gif"
               : card == Card::spade_jack    ? "11s.gif"
               : card == Card::heart_jack    ? "11h.gif"
               : card == Card::diamond_jack  ? "11d.gif"
               : card == Card::club_nine     ? "09c.gif"
               : card == Card::spade_nine    ? "09s.gif"
               : card == Card::heart_nine    ? "09h.gif"
               : card == Card::diamond_nine  ? "09d.gif"
               : "")).string();
  case CardsetFormat::kde:
    // ToDo
    return {};
  case CardsetFormat::gnome:
    // ToDo
    return {};
  case CardsetFormat::unknown:
    break;
  }
  return {};
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
