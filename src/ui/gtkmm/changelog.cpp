/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "changelog.h"

#include "ui.h"
#include "cards.h"

#include "../../versions.h"

#include <gtkmm/label.h>
#include <gtkmm/image.h>
#include <gtkmm/scrolledwindow.h>
#ifdef CHANGELOG_USE_TREEVIEW
#include <gtkmm/treeview.h>
#include <gtkmm/treestore.h>
#endif
#include <gtkmm/textview.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 **/
ChangeLog::ChangeLog(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::ChangeLog"), false)
{
  ui->add_window(*this);
  signal_realize().connect(sigc::mem_fun(*this,
                                               &ChangeLog::init));
} // ChangeLog::ChangeLog(Base* parent)

/** destructor
 **/
ChangeLog::~ChangeLog() = default;

/** create all subelements
 **/
void
ChangeLog::init()
{
  set_icon(ui->icon);

  set_skip_taskbar_hint();

  set_default_size((ui->logo->get_width() * 3) / 2,
                         (ui->logo->get_height() * 6) / 2);
#ifdef POSTPONED
  get_window()->set_decorations(Gdk::DECOR_BORDER
                                      | Gdk::DECOR_RESIZEH
                                      | Gdk::DECOR_TITLE
                                      | Gdk::DECOR_MENU);
#endif


  add_close_button(*this);

  { // the image
    auto image = Gtk::manage(new Gtk::Image(parent->ui->logo));
    get_content_area()->pack_start(*image, Gtk::PACK_SHRINK);
  } // the image
  { // title
    auto title_text = Gtk::manage(new Gtk::TextView());
    title_text->set_editable(false);
    title_text->set_cursor_visible(false);

    auto title_buffer = title_text->get_buffer();
    {
      auto tag_title = title_buffer->create_tag("title");
      //tag_title->set_property("weight", Pango::WEIGHT_BOLD);
      tag_title->set_property("scale", Pango::SCALE_X_LARGE);
      tag_title->set_property("justification", Gtk::JUSTIFY_CENTER);
    }
    title_buffer->insert_with_tag(title_buffer->begin(),
                                        "\n"
                                        // gettext: %s = version
                                        + _("ChangeLog::till %s", ::version->number_to_string())
                                        + "\n",
                                        "title");

    get_content_area()->pack_start(*title_text, false, true);
  } // title

  { // ChangeLog
#ifdef CHANGELOG_USE_TREEVIEW
    // the lines in the table do not wrap
    auto list = Gtk::TreeStore::create(*model);
    auto treeview = Gtk::manage(new Gtk::TreeView(list));
    treeview->set_headers_visible(false);

    treeview->append_column("asdf", model->text);

    for (auto v = ::all_versions.rbegin();
         v != ::all_versions.rend();
         ++v) {
      auto version_row = *list->append();
      version_row[model->text] = (*v)->number_to_string();
      text[**v] = *list->append(version_row.children());
      text[**v][model->text] = "changes";
    } // for (v : ::all_versions)

    { // the scrolled window
      auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow);
      scrolled_window->set_policy(Gtk::POLICY_AUTOMATIC,
                                  Gtk::POLICY_AUTOMATIC);
      scrolled_window->add(*treeview);

      get_content_area()->add(*scrolled_window);
    } // the scrolled window
#else // #ifdef POSTPONED
    auto text = Gtk::manage(new Gtk::TextView());
    text->set_wrap_mode(Gtk::WRAP_WORD);
    text->set_editable(false);
    auto text_buffer = text->get_buffer();
    {
      {
        auto tag_subtitle = text_buffer->create_tag("subtitle");
        tag_subtitle->set_property("weight", Pango::WEIGHT_BOLD);
      }

      text_buffer->begin_user_action();
      for (auto v = ::all_versions.rbegin();
           v != ::all_versions.rend();
           ++v) {
        if (v != ::all_versions.rbegin())
          text_buffer->insert_with_tag(text_buffer->end(),
                                       "\n\n", "subtitle");
        text_buffer->insert_with_tag(text_buffer->end(),
                                     v->number_to_string() + "\n",
                                     "subtitle");
        text_buffer->insert(text_buffer->end(),
                            _("ChangeLog::" + v->number_to_string()));
      } // for (v : ::all_versions)
      text_buffer->end_user_action();
    }

    { // the scrolled window
      auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow);
      scrolled_window->set_policy(Gtk::POLICY_AUTOMATIC,
                                  Gtk::POLICY_AUTOMATIC);
      scrolled_window->add(*text);

      get_content_area()->pack_start(*scrolled_window);
    } // the scrolled window
#endif // #ifdef POSTPONED
  } // ChangeLog

  show_all_children();
} // void ChangeLog::init()

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
