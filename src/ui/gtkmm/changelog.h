/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"
#include "widgets/sticky_dialog.h"

#ifdef POSTPONED
// postponed till I can wrap the elements of a treeview
#define CHANGELOG_USE_TREEVIEW
#endif

#ifdef CHANGELOG_USE_TREEVIEW
#include <gtkmm/treemodel.h>
#endif
#include <gtkmm/textbuffer.h>

namespace UI_GTKMM_NS {

/**
 ** the changelog dialog
 **
 ** @todo   use treeview for the different versions
 **/
class ChangeLog : public Base, public Gtk::StickyDialog {
#ifdef CHANGELOG_USE_TREEVIEW
  struct Model : public Gtk::TreeModel::ColumnRecord {
      Model() : text()
    { this->add(this->text); }

      Gtk::TreeModelColumn<Glib::ustring> text;
  }; // struct Model : public Gtk::TreeModel::ColumnRecord
#endif
  public:
  explicit ChangeLog(Base* parent);
  ~ChangeLog() override;

  private:
  void init();

  private: // unused
  ChangeLog() = delete;
  ChangeLog(ChangeLog const&) = delete;
  ChangeLog& operator=(ChangeLog const&) = delete;
}; // class ChangeLog : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
