/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include <gtkmm/messagedialog.h>
#include <gtkmm/textview.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/image.h>
#include "../../misc/preferences.h"

namespace UI_GTKMM_NS {

void show_console_output(string const& title, string const& text)
{
  auto app = Gtk::Application::create("freedoko");
  auto window = Gtk::Dialog("FreeDoko – " + _("Window::" + title));
  window.set_default_size(600, 600);
  {
    auto icon = Gdk::Pixbuf::create_from_file(::preferences.path(::Preferences::Path::icon_file).string());
    window.set_icon(icon);
  }

  {
    auto quit_button = Gtk::manage(new Gtk::Button(_("Button::quit")));
    quit_button->set_image_from_icon_name("application-exit");
    quit_button->signal_clicked().connect(sigc::mem_fun(window, &Gtk::Dialog::close));
    window.add_action_widget(*quit_button, Gtk::RESPONSE_CLOSE);
    quit_button->set_can_default();
    quit_button->grab_default();
    quit_button->grab_focus();
  }

  {
    auto logo = Gdk::Pixbuf::create_from_file(::preferences.path(::Preferences::Path::logo_file).string());
    auto image = Gtk::manage(new Gtk::Image(logo));
    window.get_content_area()->pack_start(*image, Gtk::PACK_SHRINK);
  }
  {
    auto text_window = Gtk::manage(new Gtk::ScrolledWindow());
    text_window->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

    {
      auto text_view = Gtk::manage(new Gtk::TextView());
      text_view->set_editable(false);
      text_view->set_wrap_mode(Gtk::WRAP_WORD);
      text_view->set_cursor_visible(false);
      text_view->get_buffer()->set_text(text);
      text_window->add(*text_view);
    }

    window.get_content_area()->pack_end(*text_window, Gtk::PACK_EXPAND_WIDGET);
  }

  window.show_all_children();
  app->run(window);
}

} // namespace UI_GTKMM_NS

#endif
