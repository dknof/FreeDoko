/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "error.h"

#include "bug_report.h"
#include "ui.h"

#include "../../basetypes/program_flow_exception.h"
#include "../../misc/preferences.h"

#include <gtkmm/button.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    message   message to display
 ** @param    backtrace  backtrace to show
 ** @param    parent    the parent object
 **/
Error::Error(string message_, string backtrace_, Base* const parent) :
  Base(parent),
  Gtk::MessageDialog("FreeDoko – " + _("Window::error"),
                     false,
                     Gtk::MESSAGE_ERROR, Gtk::BUTTONS_NONE,
#ifdef RELEASE
                     true
#else
                     false
#endif
                    ),
  message(std::move(message_)),
  backtrace(std::move(backtrace_))
  {
#ifdef GLIBMM_EXCEPTIONS_ENABLED
    try {
      set_icon(Gdk::Pixbuf::create_from_file(::preferences.path(::Preferences::Path::icon_file).string()));
    } catch (Glib::FileError const& file_error) {
    } catch (Gdk::PixbufError const& pixbuf_error) {
    } // try
#else
    std::auto_ptr<Glib::Error> error;
    set_icon(Gdk::Pixbuf::create_from_file(::preferences.path(::Preferences::Path::icon_file).string(), error));
#endif

    // gettext: %s = contact
    set_message(_("BugReport::bug found, create report, %s",
                        contact)
                      + "\n\n"
                      + _("BugReport::message") + ":"
                      + "\n"
                      + message);
    {
      auto bug_report_button = Gtk::manage(new Gtk::Button(_("BugReport::create")));
      bug_report_button->set_image_from_icon_name("document-new");
      bug_report_button->set_always_show_image();
      add_action_widget(*bug_report_button, Gtk::RESPONSE_CLOSE);
      bug_report_button->signal_clicked().connect(sigc::mem_fun(*this,
                                                                &Error::create_bug_report)
                                                 );
    }
    {
      auto quit_button = Gtk::manage(new Gtk::Button(_("quit")));
      quit_button->set_image_from_icon_name("application-exit");
      quit_button->set_always_show_image();
      add_action_widget(*quit_button, Gtk::RESPONSE_CLOSE);
      quit_button->signal_clicked().connect(sigc::mem_fun(*this,
                                                          &Error::quit)
                                           );
    }

    show_all_children();
  }

/** constructor
 **
 ** @param    message   message to display
 ** @param    parent    the parent object
 **/
Error::Error(string message_, Base* const parent) :
  Error(message_, "", parent)
{ }
 
/** destructor
 **/
Error::~Error() = default;

/** show the bug report window
 **/
void
  Error::create_bug_report()
  {
    BugReport bug_report(this, "\n\n\n---\n\n"s + message + "\n\nBacktrace:" + backtrace);

    bug_report.run();
    hide();
  }

/** quit the program
 **/
void
  Error::quit()
  {
    std::exit(EXIT_FAILURE);
  }

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
