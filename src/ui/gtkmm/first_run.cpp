/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "first_run.h"

#include "ui.h"

#include <gtkmm/image.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textview.h>
#include <gtkmm/texttag.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent    the parent object
 ** @param    message   the message for the first run
 **/
FirstRun::FirstRun(Base* const parent,
                   string  message) :
  Base(parent),
  Dialog("FreeDoko – " + _("Window::first run")),
  message_(std::move(message))
{
  ui->add_window(*this);
  signal_realize().connect(sigc::mem_fun(*this,
                                               &FirstRun::init));
  set_icon(ui->icon);

  set_default_size(static_cast<int>(ui->logo->get_width() * 1.5),
                         static_cast<int>(ui->logo->get_height() * 2.5));
#ifdef POSTPONED
  get_window()->set_decorations(Gdk::DECOR_BORDER
                                      | Gdk::DECOR_RESIZEH
                                      | Gdk::DECOR_TITLE
                                      | Gdk::DECOR_MENU);
#endif

} // FirstRun::FirstRun(Base* parent, string message)

/** destructor
 **/
FirstRun::~FirstRun() = default;

/** initializes the window
 **/
void
FirstRun::init()
{
  add_close_button(*this);

  { // the image
    auto image = Gtk::manage(new Gtk::Image(parent->ui->logo));
    get_content_area()->pack_start(*image, Gtk::PACK_SHRINK);
  } // the image
  { // the text
    auto text = Gtk::manage(new Gtk::TextView());
    auto text_buffer = text->get_buffer();
    {
      auto tag_title = text_buffer->create_tag("title");
      //tag_title->set_property("weight", Pango::WEIGHT_BOLD);
      tag_title->set_property("scale", Pango::SCALE_X_LARGE);
      tag_title->set_property("justification", Gtk::JUSTIFY_CENTER);

      auto tag_endline = text_buffer->create_tag("endline");
      tag_endline->set_property("style", Pango::STYLE_ITALIC);
      tag_endline->set_property("justification", Gtk::JUSTIFY_CENTER);

    }
    Glib::ustring message_text = message_;
    size_t pos = 0;
    { // title
      size_t const title_pos = message_text.find("\n\n") ;
      if (title_pos == message_text.find("\n")) {
        // found the title!

        text_buffer->insert_with_tag(text_buffer->begin(),
                                     "\n" +
                                     Glib::ustring(message_text,
                                                   0, title_pos + 2),
                                     "title");
        pos = title_pos + 2;
      } // if (title found)
    } // title
    { // end line
      auto const end_pos = message_text.rfind("\n\n") ;
      if (end_pos == message_text.rfind("\n") - 1) {
        // found the end line!

        text_buffer->insert(text_buffer->end(),
                            Glib::ustring(message_text,
                                          pos,
                                          end_pos - pos + 1));
        text_buffer->insert_with_tag(text_buffer->end(),
                                     Glib::ustring(message_text,
                                                   end_pos + 1),
                                     "endline");
      } else { // if !(end line found)
        text_buffer->insert(text_buffer->end(),
                            Glib::ustring(message_text, pos));
      } // if !(end line found)
    } // end line


    auto text_window = Gtk::manage(new Gtk::ScrolledWindow());
    text_window->add(*text);
    text_window->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    text->set_editable(false);
    text->set_wrap_mode(Gtk::WRAP_WORD);
    text->set_cursor_visible(false);

    get_content_area()->pack_start(*text_window);
  } // the text

  show_all_children();
} // FirstRun::FirstRun(Base* parent, string message)


} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
