/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"

#include <gtkmm/dialog.h>
#include <gtkmm/textbuffer.h>

namespace UI_GTKMM_NS {

/**
 ** a dialog only shown on the first run of the program
 **
 ** @todo   when the program first starts let it stay on top (works),
 **         but when the user selects another window do not raise it again
 **/
class FirstRun : public Base, public Gtk::Dialog {
  public:
    FirstRun(Base* parent, string  message);
    ~FirstRun() override;

  private:
    void init();

  private:
    string const message_;

  private: // unused
    FirstRun() = delete;
    FirstRun(FirstRun const&) = delete;
    FirstRun& operator=(FirstRun const&) = delete;
}; // class FirstRun : public Base, public Gtk::Dialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
