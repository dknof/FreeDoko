/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "game_debug.h"

#include "ui.h"
#include "cards.h"

#include "../../card/trick.h"
#include "../../player/team_information.h"
#include "../../player/ai/ai.h"
#include "../../utils/string.h"

#include <gtkmm/label.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/treestore.h>
#include <gtkmm/frame.h>
#include <gtkmm/grid.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textview.h>

namespace UI_GTKMM_NS {

/** constructor
 **/
GameDebug::PlayerInfoModel::PlayerInfoModel()
{
  add(name);
  add(team);
  add(swines);
  add(announcement);
  add(points);
  add(tricks);
} // GameDebug::PlayerInfoModel::PlayerInfoModel()

/** constructor
 **/
GameDebug::TeamInfoModel::TeamInfoModel()
{
  add(name);
  known_team.resize(::party->rule()(Rule::Type::number_of_players_in_game));
  for (auto& t : known_team)
    add(t);
} // GameDebug::TeamInfoModel::TeamInfoModel()

/** constructor
 **/
GameDebug::TricksModel::TricksModel()
{
  add(trickno);
  add(playerno);
  add(card);
  add(heuristic);
  add(heuristic_text);
  add(rationale);
  add(isvirtual);
} // GameDebug::TricksModel::TricksModel()

/** constructor
 **
 ** @param    parent   the parent widget
 **/
GameDebug::GameDebug(Base* const parent) :
  Base(parent),
  StickyDialog(_("Window::Game debug"), false)
{
  ui->add_window(*this);

  signal_realize().connect(sigc::mem_fun(*this, &GameDebug::init));

  Game::signal_open.connect_back(*this, &GameDebug::game_open, disconnector_);

#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_key_press_event().connect(sigc::mem_fun(*this, &GameDebug::on_key_press_event));
#endif
} // GameDebug::GameDebug(Base* parent)

/** destructor
 **/
GameDebug::~GameDebug() = default;

/** the game is opened
 **
 ** @param    game   game that is opened
 **/
void
GameDebug::game_open(Game& game)
{
  realize();

  tricks_rows.clear();
  tricks_store->clear();

  while (!ai_test_rows.empty())
    ai_test_rows.pop();

  for (auto const& p : game.players())
    name_changed(p);

  {
    game.signal_start                         .connect_back(*this, &GameDebug::update_info, disconnector_);
    game.tricks().signal_trick_open           .connect_back(*this, &GameDebug::trick_open,  disconnector_);
    game.signal_card_played                   .connect_back(*this, &GameDebug::card_played, disconnector_);
    game.announcements().signal_announcement_made.connect_back(*this, &GameDebug::announcement_made, disconnector_);
    game.swines().signal_swines_announced     .connect_back(*this, &GameDebug::swines_announced,      disconnector_);
    game.swines().signal_hyperswines_announced.connect_back(*this, &GameDebug::hyperswines_announced, disconnector_);
    game.marriage().signal_marriage.connect_back(*this, &GameDebug::marriage,            disconnector_);
    game.signal_ai_heuristic_failed.connect_back(*this, &GameDebug::ai_heuristic_failed, disconnector_);
    game.signal_ai_test_card       .connect_back(*this, &GameDebug::ai_test_card,        disconnector_);
    game.signal_ai_card_weighting  .connect_back(*this, &GameDebug::ai_card_weighting,   disconnector_);
    game.signal_ai_test_hands      .connect_back(*this, &GameDebug::ai_test_hands,       disconnector_);
    game.signal_ai_hands_weighting .connect_back(*this, &GameDebug::ai_hands_weighting,  disconnector_);
    game.signal_virtual_card_played.connect_back(*this, &GameDebug::virtual_card_played, disconnector_);
  }
} // void GameDebug::game_open(Game game)

/** a new trick has opened
 **
 ** @param    trick   new trick
 **/
void
GameDebug::trick_open(::Trick const& trick)
{
  if (!tricks_rows.empty())
    tricks_treeview->collapse_row(tricks_store->get_path(tricks_rows.back()));
  tricks_rows.push_back(*tricks_store->append());
  tricks_rows.back()[tricks_model.trickno]
    = std::to_string(trick.no());
} // void GameDebug::trick_open(Trick trick)

/** 'player' has played 'card'
 **
 ** @param    card   the played card
 **/
void
GameDebug::card_played(HandCard const card)
{
  Player const& player = card.player();

  Gtk::TreeModel::Row row;
  if (ai_test_rows.empty())
    row = *tricks_store->append(tricks_rows.back().children());
  else {
    row = ai_test_rows.top();
    tricks_treeview->collapse_row(tricks_store->get_path(row));
    ai_test_rows.pop();
    while (!ai_test_rows.empty())
      ai_test_rows.pop();
  }
  tricks_treeview->expand_row(tricks_store->get_path(row.parent()), false);
  row[tricks_model.playerno]
    = std::to_string(player.no());
  row[tricks_model.card] = _(card);
  if (dynamic_cast<Ai const*>(&player)) {
    auto const& ai = dynamic_cast<Ai const&>(player);
    row[tricks_model.heuristic] = ai.last_heuristic();
    row[tricks_model.heuristic_text] = _(ai.last_heuristic());
    row[tricks_model.rationale] = ai.last_heuristic_rationale();
  } else {
    row[tricks_model.rationale] = {};
  }

  update_info();
} // void GameDebug::card_played(HandCard card)

/** a heuristic of the ai has failed
 **/
void
GameDebug::ai_heuristic_failed(Aiconfig::Heuristic heuristic, Rationale rationale)
{
  if (ai_test_rows.empty()) {
    ai_test_rows.push(*tricks_store->append(tricks_rows.back().children()));
  }
  auto row_ptr = tricks_store->append(ai_test_rows.top().children()); // Workaround: Variable, um Warnung vom Compiler zu umgehen
  auto& row = *row_ptr;
  row[tricks_model.heuristic] = heuristic;
  row[tricks_model.heuristic_text] = _(heuristic);
  row[tricks_model.rationale] = rationale;
}

/** the ai tests the card 'card'
 **
 ** @param    card      the card that is tested
 ** @param    playerno   number of the player who plays the card
 **/
void
GameDebug::ai_test_card(Card const card, unsigned const playerno)
{
  auto const& ai = dynamic_cast<Ai const&>(ui->game().players().current_player());
  if (ai_test_rows.empty()) {
    ai_test_rows.push(*tricks_store->append(tricks_rows.back().children()));
    auto& row = ai_test_rows.top();
    row[tricks_model.heuristic] = Aiconfig::Heuristic::no_heuristic;
    row[tricks_model.heuristic_text] = _(Aiconfig::Heuristic::gametree);
    row[tricks_model.rationale] = ai.last_heuristic_rationale();
  }
  ai_test_rows.push(*tricks_store->append(ai_test_rows.top().children()));
  auto& row = ai_test_rows.top();
  if (ai_test_rows.size() == 2)
    tricks_treeview->expand_to_path(tricks_store->get_path(row.parent()));
  row[tricks_model.card] = _(card);
  row[tricks_model.playerno] = std::to_string(playerno);
} // void GameDebug::ai_test_card(Card card, unsigned playerno)

/** the ai has calculated a weighting for the tested card
 **
 ** @param    weighting   the weighting for the tested card
 **/
void
GameDebug::ai_card_weighting(int const weighting)
{
  if (ai_test_rows.empty()) {
    DEBUG_ASSERTION(false,
                    "GameDebug::ai_card_weighting():\n"
                    "  ai_test_rows is empty");
  }
  while (ai_test_rows.top()[tricks_model.isvirtual])
    ai_test_rows.pop();

  if (   weighting == INT_MIN
      || weighting == INT_MAX) {
    ai_test_rows.top()[tricks_model.heuristic_text] = "-";
  } else {
    ai_test_rows.top()[tricks_model.heuristic_text] = std::to_string(weighting);
  }

  ai_test_rows.top()[tricks_model.isvirtual] = false;

  tricks_treeview->collapse_row(tricks_store->get_path(ai_test_rows.top()));
  ai_test_rows.pop();
} // void GameDebug::ai_card_weighting(int weighting)

/** the ai tests the card 'card'
 **
 ** @param    card      the card that is tested
 ** @param    playerno   number of the player who plays the card
 **/
void
GameDebug::ai_test_hands(Hands const& hands, int const weighting, unsigned const number, unsigned const playerno)
{
  ai_test_rows.push(*tricks_store->append(ai_test_rows.top().children()));
  auto& row = ai_test_rows.top();
  if (ai_test_rows.size() == 2)
    tricks_treeview->expand_to_path(tricks_store->get_path(row.parent()));
  row[tricks_model.card] = _("hand %u", number + 1);
  row[tricks_model.playerno] = std::to_string(playerno);
  ostringstream ostr;
  ostr << _("weighting") << ": " << weighting << "\n\n";
  auto const& game = hands[0].player().game();
  for (auto const& player : game.players())
    ostr << "\t |  " << player.name();
  ostr << '\n';
  for (auto const& player : game.players()) {
    (void) player;
    ostr << "\t+-----------------";
  }
  ostr << '\n';
  auto text = _(hands);
  text = String::replaced_all("\t" + text, "\n", "\n\t");
  text = String::replaced_all( text, "\t\n", "\n");
  text = String::replaced_all(text, "\t", "\t |  ");
  if (!text.empty() && text.back() == '\n')
    text.pop_back();
  row[tricks_model.rationale] = ostr.str() + text;
}

/** the ai has calculated a weighting for the tested card
 **
 ** @param    weighting   the weighting for the tested card
 **/
void
GameDebug::ai_hands_weighting(int weighting)
{
  if (ai_test_rows.empty()) {
    DEBUG_ASSERTION(false,
                    "GameDebug::ai_hands_weighting():\n"
                    "  ai_test_rows is empty");
  }
  while (ai_test_rows.top()[tricks_model.isvirtual])
    ai_test_rows.pop();

  if (   weighting == INT_MIN
      || weighting == INT_MAX) {
    ai_test_rows.top()[tricks_model.heuristic_text] = "-";
  } else {
    ai_test_rows.top()[tricks_model.heuristic_text] = std::to_string(weighting);
  }

  ai_test_rows.top()[tricks_model.isvirtual] = false;

  tricks_treeview->collapse_row(tricks_store->get_path(ai_test_rows.top()));
  ai_test_rows.pop();
}

/** virtual game: card is played
 **
 ** @param    card   the card that is played
 **/
void
GameDebug::virtual_card_played(HandCard const card)
{
  auto const& player = card.player();

  ai_test_rows.push(*tricks_store->append(ai_test_rows.top().children()));
  auto row = ai_test_rows.top();

  row[tricks_model.card] = _(card);
  row[tricks_model.playerno] = std::to_string(player.no());
  if (dynamic_cast<Ai const*>(&player)) {
    auto const& ai = dynamic_cast<Ai const&>(player);
    row[tricks_model.heuristic] = ai.last_heuristic();
    if (ai.last_heuristic() == Aiconfig::Heuristic::gametree) {
      row[tricks_model.heuristic_text] = _(ai.last_heuristic_rationale());
      row[tricks_model.rationale] = {};
    } else {
      row[tricks_model.rationale] = ai.last_heuristic_rationale();
    }
  }
  row[tricks_model.isvirtual]
    = true;
} // void GameDebug::virtual_card_played(HandCard card)

/** the announcement 'announcement' has been made by player 'player'
 **
 ** @param    announcement   the announcement
 ** @param    player      the player, who has made the announcement
 **/
void
GameDebug::announcement_made(Announcement const announcement,
                             Player const& player)
{
  if (player.game().isvirtual())
    return;

  Gtk::TreeModel::Row row;
  row = *tricks_store->append(tricks_rows.back().children());
  tricks_treeview->expand_row(tricks_store->get_path(row.parent()), false);
  row[tricks_model.playerno] = std::to_string(player.no());
  row[tricks_model.card] = _(announcement);
  if (dynamic_cast<Ai const*>(&player)) {
    auto const& ai = dynamic_cast<Ai const&>(player);
    row[tricks_model.rationale] = ai.last_announcement_rationale();
  } else {
    row[tricks_model.rationale] = {};
  }

  update_info();
} // void GameDebug::announcement_made(Announcement announcement, Player player)

/** the player has swines
 **
 ** @param    player      the player with the swines
 **/
void
GameDebug::swines_announced(Player const& player)
{
  update_info();
} // void GameDebug::swines_announced(Player player)

/** the player has hyperswines
 **
 ** @param    player      the player with the swines
 **/
void
GameDebug::hyperswines_announced(Player const& player)
{
  update_info();
} // void GameDebug::hyperswines_announced(Player player)

/** marriage: Information of the new team
 **
 ** @param    bridegroom   the player with the marriage
 ** @param    bride      the bride
 **/
void
GameDebug::marriage(Player const& bridegroom, Player const& bride)
{
  update_info();
} // void GameDebug::marriage(Player bridegroom, Player bride)

/** create all subelements
 **/
void
GameDebug::init()
{
  set_icon(ui->icon);

  set_default_size(static_cast<int>(7 * ui->cards->height()),
                         static_cast<int>(4 * ui->cards->height()));

  add_close_button(*this);

  auto const& party = ui->party(); // cppcheck-suppress shadowVariable

  { // content
    auto content_box = Gtk::manage(new Gtk::Box());
    content_box->set_spacing(2 EM);
    content_box->set_border_width(1 EM);

    { // left box
      auto left_box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));
      left_box->set_spacing(1 EX);
      { // game info
        auto grid = Gtk::manage(new Gtk::Grid());
        grid->set_border_width(1 EX);

        { // seed
          auto label = Gtk::manage(new Gtk::Label(_("Label::seed") + ": "));
          grid->attach(*label, 0, 0);
          seed = Gtk::manage(new Gtk::Label(""));
          grid->attach(*seed, 1, 0);
        } // seed
        { // game type
          auto label = Gtk::manage(new Gtk::Label(_("Label::gametype") + ": "));
          grid->attach(*label, 0, 1);
          gametype = Gtk::manage(new Gtk::Label(""));
          grid->attach(*gametype, 1, 1);
        } // game type
        { // soloplayer
          auto label = Gtk::manage(new Gtk::Label(_("Label::soloplayer") + ": "));
          grid->attach(*label, 0, 2);
          soloplayer = Gtk::manage(new Gtk::Label(""));
          grid->attach(*soloplayer, 1, 2);
        } // seed

        { // startplayer
          auto label = Gtk::manage(new Gtk::Label(_("startplayer") + ": "));
          grid->attach(*label, 0, 3);
          startplayer = Gtk::manage(new Gtk::Label(""));
          grid->attach(*startplayer, 1, 3);
        } // seed
        grid->set_row_spacing(1 EX);
        grid->set_column_spacing(1 EM);
        for (auto child : grid->get_children())
          child->set_halign(Gtk::ALIGN_START);

        auto frame = Gtk::manage(new Gtk::Frame(_("Label::game info")));
        frame->add(*grid);
        left_box->pack_start(*frame, false, true);
      } // game info

      { // player_info
        player_info_list
          = Gtk::ListStore::create(player_info_model);
        player_info_treeview
          = Gtk::manage(new Gtk::TreeView(player_info_list));
        player_info_treeview->get_selection()->set_mode(Gtk::SELECTION_NONE);

        unsigned column = 0;
        player_info_treeview->append_column(_("name"),
                                                  player_info_model.name);
        column += 1;
        player_info_treeview->append_column(_("team"),
                                                  player_info_model.team);
        player_info_treeview->get_column_cell_renderer(column)->set_property("xalign", 0.5);
        column += 1;
        player_info_treeview->append_column(_("swines"),
                                                  player_info_model.swines);
        player_info_treeview->get_column_cell_renderer(column)->set_property("xalign", 0.5);
        column += 1;
        player_info_treeview->append_column(_("announcement"),
                                                  player_info_model.announcement);
        player_info_treeview->get_column_cell_renderer(column)->set_property("xalign", 0.5);
        column += 1;
        player_info_treeview->append_column(_("points"),
                                                  player_info_model.points);
        player_info_treeview->get_column_cell_renderer(column)->set_property("xalign", 0.5);
        column += 1;
        player_info_treeview->append_column(_("tricks"),
                                                  player_info_model.tricks);
        player_info_treeview->get_column_cell_renderer(column)->set_property("xalign", 0.5);
        column += 1;

        (void) column;

        for (unsigned p = 0;
             p < party.rule()(Rule::Type::number_of_players_in_game);
             ++p) {
          player_info_rows.push_back(*player_info_list->append());
          player_info_rows.back()[player_info_model.name]
            = party.players()[p].name();
        }

        auto frame = Gtk::manage(new Gtk::Frame(_("player info")));
        frame->add(*player_info_treeview);
        left_box->pack_start(*frame, false, true);
      } // player_info

      { // team_info
        team_info_list
          = Gtk::ListStore::create(team_info_model);
        team_info_treeview
          = Gtk::manage(new Gtk::TreeView(team_info_list));
        team_info_treeview->get_selection()->set_mode(Gtk::SELECTION_NONE);

        unsigned column = 0;
        team_info_treeview->append_column(_("name"),
                                                team_info_model.name);
        column += 1;
        for (unsigned p = 0;
             p < team_info_model.known_team.size();
             ++p) {
          team_info_treeview->append_column(party.players()[p].name(),
                                                  team_info_model.known_team[p]);
          team_info_treeview->get_column_cell_renderer(column)->set_property("xalign", 0.5);
          column += 1;
        }

        // real team
        team_info_rows.push_back(*team_info_list->append());
        // teaminfo of the game
        team_info_rows.push_back(*team_info_list->append());
        for (unsigned p = 0;
             p < team_info_model.known_team.size();
             ++p) {
          team_info_rows.push_back(*team_info_list->append());
          team_info_rows.back()[team_info_model.name]
            = party.players()[p].name();
        } // for (p)

        Gtk::Frame* frame = Gtk::manage(new Gtk::Frame(_("team info")));
        frame->add(*team_info_treeview);
        left_box->pack_start(*frame, false, true);
      } // team_info
      content_box->pack_start(*left_box, false, false);
    } // left box

    { // tricks
      auto right_box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0));
      { // tricks
        tricks_store = Gtk::TreeStore::create(tricks_model);
        tricks_treeview
          = Gtk::manage(new Gtk::TreeView(tricks_store));
        tricks_treeview->get_selection()->set_mode(Gtk::SELECTION_MULTIPLE);
        tricks_treeview->set_rules_hint(true);

        unsigned column = 0;
        tricks_treeview->append_column(_("card"),
                                             tricks_model.card);
        column += 1;
        tricks_treeview->append_column(_("player"),
                                             tricks_model.playerno);
        tricks_treeview->get_column_cell_renderer(column)->set_property("xalign", 0.5);
        column += 1;
        tricks_treeview->append_column(_("trick"),
                                             tricks_model.trickno);
        tricks_treeview->get_column_cell_renderer(column)->set_property("xalign", 0.5);
        column += 1;
        tricks_treeview->append_column(_("heuristic"),
                                             tricks_model.heuristic_text);
        column += 1;
        tricks_treeview->get_selection()->signal_changed().connect(sigc::mem_fun(*this, &GameDebug::tricks_row_selection_changed));

        (void) column;
        tricks_treeview->get_selection()->set_mode(Gtk::SELECTION_SINGLE);

        auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow);
        scrolled_window->set_policy(Gtk::POLICY_AUTOMATIC,
                                    Gtk::POLICY_AUTOMATIC);
        scrolled_window->set_hadjustment(tricks_treeview->get_hadjustment());
        scrolled_window->set_vadjustment(tricks_treeview->get_vadjustment());
        scrolled_window->add(*tricks_treeview);

        auto frame = Gtk::manage(new Gtk::Frame(_("tricks")));
        frame->add(*scrolled_window);

        right_box->pack_start(*frame, true, true);
      } // tricks
      { // rationale
        auto text = Gtk::manage(new Gtk::TextView());
        text->set_wrap_mode(Gtk::WRAP_WORD);
        text->set_editable(false);
        text->set_cursor_visible(false);
        {
          auto tabs = Pango::TabArray(pango_tab_array_new_with_positions(4, true, // NOLINT(hicpp-vararg)
                                                                         PANGO_TAB_LEFT, 1,
                                                                         PANGO_TAB_LEFT, 1 * 10 * 10,
                                                                         PANGO_TAB_LEFT, 2 * 10 * 10,
                                                                         PANGO_TAB_LEFT, 3 * 10 * 10));
          text->set_tabs(tabs);
        }
        heuristic_rationale = text->get_buffer();

        heuristic_rationale_frame = Gtk::manage(new Gtk::Frame(_("rationale")));
        heuristic_rationale_frame->set_margin_top(1 EX);
        heuristic_rationale_frame->add(*text);
        right_box->pack_start(*heuristic_rationale_frame, false, true);
      } // rationale
      content_box->pack_end(*right_box, true, true);
    }
    get_content_area()->pack_start(*content_box);
  } // content

  show_all_children();
  heuristic_rationale_frame->hide();
} // void GameDebug::init()

/** updates the information for the players
 **/
void
GameDebug::update_info()
{
  if (!ui->party().in_game())
    return ;

  Game const& game = ui->game();

  seed->set_label(std::to_string(game.seed()));
  gametype->set_label(_(game.type()));
  if (game.players().exists_soloplayer())
    soloplayer->set_label(game.players().soloplayer().name());
  else
    soloplayer->set_label("-");
  startplayer->set_label(game.startplayer().name());

  for (unsigned p = 0; p < game.playerno(); ++p) {
    ::Player const& player = game.player(p);

    { // player info
      auto& row = player_info_rows[p];

      row[player_info_model.team]
        = _(player.team());
      row[player_info_model.swines]
        = (player.has_swines() ? "S" : "");
      if (player.has_hyperswines())
        row[player_info_model.swines]
          = row[player_info_model.swines] + "H";

      row[player_info_model.announcement]
        = ( (player.announcement() == Announcement::noannouncement)
           ? ""s
           : _(player.announcement())
          );
      if (game.tricks().empty()) {
        row[player_info_model.points] = 0;
        row[player_info_model.tricks] = 0;
      } else {
        auto const& trick = game.tricks().current();
        row[player_info_model.tricks] = player.number_of_tricks();
        row[player_info_model.points] = player.points_in_trickpile();
        if (trick.isfull() && !trick.intrickpile() && trick.winnerplayer() == player) {
          row[player_info_model.tricks] = player.number_of_tricks() + 1;
          row[player_info_model.points] = player.points_in_trickpile() + trick.points();
        }
      }

    } // player info
    { // team info
      team_info_rows[0][team_info_model.known_team[p]]
        = _("Team::" + ::short_string(player.team()));
      team_info_rows[1][team_info_model.known_team[p]]
        = _("Team::" + ::short_string(player.game().teaminfo().get(player)));

      for (unsigned q = 0; q < game.playerno(); ++q) {
        Player const& player2 = player.game().player(q);
        if (dynamic_cast<Ai const*>(&player2)) {
          Ai const& ai2 = dynamic_cast<Ai const&>(player2);
          team_info_rows[2 + q][team_info_model.known_team[p]]
            = (_("Team::" + ::short_string(ai2.teaminfo(player)))
               + (is_real(ai2.teaminfo(player))
                  ? ""s
                  : " " + std::to_string(ai2.team_information().team_value(player))));
        } else {
          team_info_rows[2 + q][team_info_model.known_team[p]]
            = "";
        }
      } // for (q)
    } // team info
  } // for (p)
} // void GameDebug::update_info()

/** the name of 'player' has changed
 **
 ** @param    player   player whose name has changed
 **/
void
GameDebug::name_changed(Player const& player)
{
  if (!get_realized())
    return;

  if (!ui->party().in_game())
    return ;
  if (!ui->game().players().contains(player))
    return ;

  team_info_treeview->get_column(1 + player.no())->set_title(player.name());

  player_info_rows[player.no()][player_info_model.name]
    = player.name();

  team_info_rows[2 + player.no()][team_info_model.name]
    = player.name();
} // void GameDebug::name_changed(Player player)

/** the row selection has changed:
 ** show the belonging game summary
 **/
void
GameDebug::tricks_row_selection_changed()
{
  auto selection = tricks_treeview->get_selection();

  if (!selection->get_selected()) {
    heuristic_rationale_frame->hide();
    heuristic_rationale->set_text({});
    return ;
  }
  auto row = *selection->get_selected();
  Aiconfig::Heuristic const heuristic = row[tricks_model.heuristic];
  Rationale const& rationale = row[tricks_model.rationale];
  if (!!rationale) {
    heuristic_rationale_frame->show();
    heuristic_rationale_frame->set_label(_("decision logic"));
    auto text = String::remove_trailing_newlines(rationale.text());
    heuristic_rationale->set_text(text);
  } else if (heuristic != Aiconfig::Heuristic::no_heuristic) {
    heuristic_rationale_frame->show();
    heuristic_rationale_frame->set_label(_("description"));
    heuristic_rationale->set_text(_("AiConfig::Heuristic::Description::" + to_string(heuristic)));
  } else {
    heuristic_rationale_frame->hide();
  }
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
