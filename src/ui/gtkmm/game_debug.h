/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"
#include "widgets/sticky_dialog.h"

#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../player/ai/heuristic.h"

#include <stack>
using std::stack;

#include <gtkmm/treemodel.h>
namespace Gtk {
class Label;
class TreeView;
class ListStore;
class TreeStore;
class Frame;
} // namespace Gtk

class Trick;

namespace UI_GTKMM_NS {

/**
 ** the summary of a game
 **
 ** Note, this class gets the information directly over the OS,
 ** the functions are not called by UI_GTKMM.
 **/
class GameDebug : public Base, public Gtk::StickyDialog {

  /**
   ** the columns of the player info table
   **/
  struct PlayerInfoModel : public Gtk::TreeModel::ColumnRecord {
    PlayerInfoModel();

    Gtk::TreeModelColumn<Glib::ustring> name;
    Gtk::TreeModelColumn<Glib::ustring> team;
    Gtk::TreeModelColumn<Glib::ustring> swines;
    Gtk::TreeModelColumn<Glib::ustring> announcement;
    Gtk::TreeModelColumn<unsigned> points;
    Gtk::TreeModelColumn<unsigned> tricks;
  }; // struct PlayerInfoModel : public Gtk::TreeModel::ColumnRecord

  /**
   ** the columns of the team info table
   **/
  struct TeamInfoModel : public Gtk::TreeModel::ColumnRecord {
    TeamInfoModel();

    Gtk::TreeModelColumn<Glib::ustring> name;
    vector<Gtk::TreeModelColumn<Glib::ustring> > known_team;
  }; // struct TeamInfoModel : public Gtk::TreeModel::ColumnRecord

  /**
   ** the columns of the tricks table
   **/
  struct TricksModel : public Gtk::TreeModel::ColumnRecord {
    TricksModel();

    Gtk::TreeModelColumn<Glib::ustring> trickno;
    Gtk::TreeModelColumn<Glib::ustring> playerno;
    Gtk::TreeModelColumn<Glib::ustring> card;
    Gtk::TreeModelColumn<Aiconfig::Heuristic> heuristic;
    Gtk::TreeModelColumn<Glib::ustring> heuristic_text;
    Gtk::TreeModelColumn<Rationale> rationale;
    Gtk::TreeModelColumn<bool> isvirtual;
  };

  public:
  explicit GameDebug(Base* parent);
  GameDebug() = delete;
  GameDebug(GameDebug const&) = delete;
  GameDebug& operator=(GameDebug const&) = delete;
  ~GameDebug() override;

  // the game is opened
  void game_open(Game& game);

  // the trick is opened
  void trick_open(::Trick const& trick);
  // the card is played
  void card_played(HandCard card);

  void ai_heuristic_failed(Aiconfig::Heuristic heuristic, Rationale rationale);
  void ai_test_card(Card card, unsigned playerno);
  void ai_card_weighting(int weighting);
  void ai_test_hands(Hands const& hands, int weighting, unsigned number, unsigned playerno);
  void ai_hands_weighting(int weighting);
  void virtual_card_played(HandCard card);

  // an announcement is made
  void announcement_made(Announcement announcement,
                         Player const& player);
  // the player has swines
  void swines_announced(Player const& player);
  // the player has hyperswines
  void hyperswines_announced(Player const& player);

  // the marriage partner has found a bride
  void marriage(Player const& bridegroom, Player const& bride);

  private:
  // initialize the window
  void init();

  // update the information
  void update_info();

  public:
  // the name of the player has changed
  void name_changed(Player const& player);

  private:
  void tricks_row_selection_changed();

  private:
  AutoDisconnector disconnector_;

  Gtk::Label* seed        = nullptr;
  Gtk::Label* gametype    = nullptr;
  Gtk::Label* soloplayer  = nullptr;
  Gtk::Label* startplayer = nullptr;

  PlayerInfoModel player_info_model;
  Glib::RefPtr<Gtk::ListStore> player_info_list;
  Gtk::TreeView* player_info_treeview = nullptr;
  vector<Gtk::TreeModel::Row> player_info_rows;

  TeamInfoModel team_info_model;
  Glib::RefPtr<Gtk::ListStore> team_info_list;
  Gtk::TreeView* team_info_treeview = nullptr;
  vector<Gtk::TreeModel::Row> team_info_rows;

  TricksModel tricks_model;
  Glib::RefPtr<Gtk::TreeStore> tricks_store;
  Gtk::TreeView* tricks_treeview = nullptr;
  vector<Gtk::TreeModel::Row> tricks_rows;
  stack<Gtk::TreeModel::Row> ai_test_rows;

  Glib::RefPtr<Gtk::TextBuffer> heuristic_rationale;
  Gtk::Frame* heuristic_rationale_frame = nullptr;
}; // class GameDebug : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
