/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"
#include "ui.h"
#include "widgets/sticky_dialog.h"

class Player;
namespace Gtk {
class Button;
class ToggleButton;
} // namespace Gtk

namespace UI_GTKMM_NS {
class GameSummary;
class GameReview;

/**
 ** the game finished window
 **
 ** @todo   when selecting a special point, jump to the trick
 ** @todo   when the party is finished, rename the 'next game' button
 **/
class GameFinished : public Base, public Gtk::StickyDialog {
  public:
    explicit GameFinished(Base* parent);
    ~GameFinished() override;

    void game_finished();
    void update_bug_report_button();

    void name_changed(::Player const& player);

  private:
    void init();
    void on_hide() override;
    void toggle_game_review();
    void replay_game();

  public:
    std::unique_ptr<GameReview> game_review;

  private:
    AutoDisconnector disconnector_;
    Gtk::Button* create_bug_report_button = nullptr;
  public:
    GameSummary* game_summary = nullptr;
  private:
    Gtk::Button* show_party_points = nullptr;
    Gtk::ToggleButton* show_game_review = nullptr;

    Gtk::Button* replay_game_button = nullptr;
    Gtk::Button* next_game_button = nullptr;

  private: // unused
    GameFinished() = delete;
    GameFinished(GameFinished const&) = delete;
    GameFinished& operator=(GameFinished const&) = delete;
}; // class GameFinished : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
