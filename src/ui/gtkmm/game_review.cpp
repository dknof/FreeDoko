/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "game_review.h"
#include "game_finished.h"
#include "trick_summary.h"

#include "ui.h"
#include "table.h"

#include "../../game/game.h"
#include "../../card/trick.h"
#include "../../player/player.h"

#include <gdk/gdkkeysyms.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    game_finished   parent object
 **/
GameReview::GameReview(GameFinished* const game_finished) :
  Base(game_finished)
{ }

/** destructor
 **/
GameReview::~GameReview() = default;

/** @return   the tricknumber
 **/
unsigned
GameReview::trickno() const
{ return trickno_; }

/** @return   the current trick that is viewed
 **/
::Trick const&
GameReview::trick() const
{
  DEBUG_ASSERTION((trickno() != UINT_MAX),
                  "GameReview::trickno():\n"
                  "  'trickno()' == UINT_MAX");

  if (trickno() == 0)
    return ::Trick::empty;
  return ui->game().tricks().trick(trickno() - 1);
} // ::Trick const& GameReview::trick() const

/** @return   whether a trick is visible (else: starting hands or all tricks in trickpiles)
 **/
bool
GameReview::trick_visible() const
{
  return (   (trickno() > 0)
          && (trickno() <= ui->game().tricks().max_size()) );
} // bool GameReview::trick_visible() const

/** set the trickno
 **
 ** @param    trickno   new trickno
 **/
void
GameReview::set_trickno(unsigned const trickno)
{
  trickno_ = trickno;

  ui->table->force_redraw_all();
} // void GameReview::set_trickno(unsigned trickno)

/** show the previous trickno
 **/
void
GameReview::previous_trick()
{
  if (trickno() > 0)
    set_trickno(trickno() - 1);
} // void GameReview::previous_trick()

/** show the following trickno
 **/
void
GameReview::following_trick()
{
  if (trickno() <= ui->game().tricks().max_size())
    set_trickno(trickno() + 1);
} // void GameReview::following_trick()

/** a key has been pressed
 **
 ** @param    key   the key
 **
 ** @return   from 'ui->key_press(key)'
 **
 ** @bug   signal does not get here for '->', '<-' and '\<Esc\>'
 **/
bool
GameReview::key_press(GdkEventKey const* const key)
{
  // whether the key was accepted
  bool accepted = false;

  if ((key->state & GDK_SHIFT_MASK) == 0) {
    switch (key->keyval) {
    case GDK_KEY_Left: // show previous trick
    case GDK_KEY_Down: // show previous trick
    case GDK_KEY_BackSpace: // show previous trick
      previous_trick();
      accepted = true;
      break;
    case GDK_KEY_Right: // show following trick
    case GDK_KEY_Up: // show following trick
    case GDK_KEY_space: // show following trick
      following_trick();
      accepted = true;
      break;
    } // switch (key->keyval)
  } // if (key->state == 0)
  if (key->state & GDK_CONTROL_MASK) {
    switch (key->keyval) {
    case GDK_KEY_p: // create a screenshot of the table
      ui->table->save_screenshot();
      accepted = true;
      break;
    }
  }

  return accepted;
} // bool GameReview::key_press(GdkEventKey* key)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
