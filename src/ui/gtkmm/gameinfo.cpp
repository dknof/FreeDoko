/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "gameinfo.h"

#include "ui.h"
#include "icons.h"

#include <gtkmm/image.h>
#include <gtkmm/label.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 ** @param    title    the title
 **/
GameInfoDialog::Information::Information(Base* const parent,
                                         string  title) :
  Base(parent),
  Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX),
  title(std::move(title)),
  icon(ui->icons->new_managed_image()),
  label(Gtk::manage(new Gtk::Label("")))
{
  set_border_width(1 EX);
} // GameInfoDialog::Information::Information(Base* parent, Translation title)

/** destructor
 **/
GameInfoDialog::Information::~Information() = default;

/** initializes the information
 **/
void
GameInfoDialog::Information::init()
{
  if (icon)
    add(*icon);
  if (label)
    add(*label);

  show_all();

  update_texts();
} // virtual void GameInfoDialog::Information::init()

/** the name of 'player' has changed
 **
 ** @param    player   player whose name has changed
 **/
void
GameInfoDialog::Information::name_changed(Player const& player)
{
  update_texts();
} // void GameInfoDialog::Information::name_changed(Player player)

/** update the texts
 **/
void
GameInfoDialog::Information::update_texts()
{ }

/** @return   whether the information is blocking the gameplay
 **/
bool
GameInfoDialog::Information::is_blocking() const
{
  return true;
} // virtual bool GameInfoDialog::Information::is_blocking() const

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
