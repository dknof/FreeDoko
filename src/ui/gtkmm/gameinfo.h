/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "gameinfo_dialog.h"


class Player;

#include <gtkmm/box.h>
namespace Gtk {
class Image;
class Label;
}; // namespace Gtk

namespace UI_GTKMM_NS {
class Table;

/**
 ** base class for game informations
 **
 ** @todo   reply button
 **/
class GameInfoDialog::Information : public Base, public Gtk::Box {
  public:
    // child classes
    class GameType;
    class Marriage;
    class Announcement;
    class Swines;
    class Hyperswines;

  public:
    friend class GameInfoDialog;

  public:
    Information(Base* parent, string  title);
    ~Information() override;

    // the name of 'player' has changed
    virtual void name_changed(Player const& player);

  protected:
    // initialize the game information
    // to be called from the children
    virtual void init();

    // update the texts
    virtual void update_texts();

    // whether the information is blocking the gameplay
    virtual bool is_blocking() const;

  protected:
    // the title
    ::string title;
    // the icon
    Gtk::Image* icon = nullptr;
    // the text
    Gtk::Label* label = nullptr;

  private: // unused
    Information() = delete;
    Information(Information const&) = delete;
    Information& operator=(Information const&) = delete;
}; // class GameInfoDialog::Information : public Base, public Gkt::Box

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
