/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "announcement.h"

#include "../ui.h"
#include "../icons.h"

#include "../../../player/player.h"
#include "../../../game/game.h"
#include "../../../party/rule.h"

#include <gtkmm/image.h>
#include <gtkmm/label.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent         the parent object
 ** @param    player         the player
 ** @param    announcement   the announcement made
 **/
GameInfoDialog::Information::Announcement::Announcement(Base* const parent,
                                                        Player const& player,
                                                        ::Announcement const
                                                        announcement) :
  GameInfoDialog::Information(parent, _("Window::GameInfo::announcement")),
  player(&player),
  announcement(announcement)
{

  if (   !ui->game().rule(Rule::Type::knocking)
      && announcement == ::Announcement::no120) {
    ui->icons->change_managed(icon, player.team());
    title = _(player.team());
  } else if (announcement == ::Announcement::reply) {
    ui->icons->change_managed(icon, player.team());
    title = _(player.team());
  } else {
    ui->icons->change_managed(icon, player.announcement());
    title = _(announcement);
  }

  init();
} // GameInfoDialog::Information::Announcement::Announcement(Base parent, Game game)

/** destructor
 **/
GameInfoDialog::Information::Announcement::~Announcement() = default;

/** update all texts
 **/
void
GameInfoDialog::Information::Announcement::update_texts()
{
  if (   !ui->game().rule(Rule::Type::knocking)
      && announcement == ::Announcement::no120) {
    label->set_label(_("%s announces '%t'.",
                             player->name(),
                             _(player->team())
                            ));

  } else if (announcement == ::Announcement::reply) {
    label->set_label(_("%s announces '%t'.",
                             player->name(),
                             _(player->team())
                            ));
  } else {
    label->set_label(_("%s announces '%t'.",
                             player->name(),
                             _(announcement)
                            ));
  }
} // void GameInfoDialog::Information::Announcement::update_texts()

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
