/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "../gameinfo.h"

class Game;

namespace UI_GTKMM_NS {

/**
 ** hyperswines information
 **/
class GameInfoDialog::Information::Hyperswines : public GameInfoDialog::Information {
  public:
    Hyperswines(Base* parent, Player const& player);
    ~Hyperswines() override;

  private:
    // update the text
    void update_texts() override;

  private:
    // the player with the hyperswines
    Player const* const player = nullptr;

  private: // unused
    Hyperswines() = delete;
    Hyperswines(Hyperswines const&) = delete;
    Hyperswines& operator=(Hyperswines const&) = delete;
}; // class GameInfoDialog::Information::Hyperswines : public GameInfoDialog::Information

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
