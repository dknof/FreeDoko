/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "swines.h"

#include "../ui.h"
#include "../icons.h"

#include "../../../player/player.h"
#include "../../../game/game.h"
#include "../../../party/rule.h"

#include <gtkmm/label.h>

namespace UI_GTKMM_NS {

GameInfoDialog::Information::Swines::Swines(Base* const parent,
                                            Player const& player) :
  GameInfoDialog::Information(parent, _("Window::GameInfo::swines")),
  player(&player)
{
  ui->icons->change_managed_swines(icon, player.game().cards().trumpcolor());
  init();
}


GameInfoDialog::Information::Swines::~Swines() = default;


void GameInfoDialog::Information::Swines::update_texts()
{
  if (player->game().rule(Rule::Type::swine_only_second))
    label->set_label(_("%s has a swine.",
                             player->name()
                            ));
  else
    label->set_label(_("%s has swines.",
                             player->name()
                            ));
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
