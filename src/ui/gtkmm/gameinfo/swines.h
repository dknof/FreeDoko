/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "../gameinfo.h"

class Game;

namespace UI_GTKMM_NS {

/**
 ** swines information
 **/
class GameInfoDialog::Information::Swines
  : public GameInfoDialog::Information
{
  public:
    Swines(Base* parent, Player const& player);
    ~Swines() override;

  private:
    // update the text
    void update_texts() override;

  private:
    // the player with the swines
    Player const* const player = nullptr;

  private: // unused
    Swines() = delete;
    Swines(Swines const&) = delete;
    Swines& operator=(Swines const&) = delete;
}; // class GameInfoDialog::Information::Swines : public GameInfoDialog::Information

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
