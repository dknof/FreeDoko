/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "gameinfo_dialog.h"
#include "gameinfo.h"
#include "gameinfo/announcement.h"
#include "gameinfo/swines.h"
#include "table.h"
#include "ui.h"
#include "main_window.h"

#include "../../party/party.h"
#include "../../game/game.h"
#include "../../party/rule.h"
#include "../../player/human/human.h"

#include <gtkmm/separator.h>
#include <gtkmm/main.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    table   the table
 **/
GameInfoDialog::GameInfoDialog(Table* const table) :
  Base(table),
  Gtk::StickyDialog("FreeDoko – " + _("Window::game information"), *table->ui->main_window, false)
{
  ui->add_window(*this);

  signal_realize().connect(sigc::mem_fun(*this, &GameInfoDialog::init));
} // GameInfoDialog::GameInfoDialog(Table* table)

/** destruktor
 **/
GameInfoDialog::~GameInfoDialog()
{
  clear();
} // GameInfoDialog::~GameInfoDialog()

/** init the dialog
 **/
void
GameInfoDialog::init()
{
  set_icon(ui->icon);

  set_skip_taskbar_hint();

  { // action area
    { // hyperswines announce
      hyperswines_announce_button
        = Gtk::manage(new Gtk::Button(_("Menu::Item::hyperswines announcement")));
      add_action_widget(*hyperswines_announce_button,
                              Gtk::RESPONSE_NONE);

      hyperswines_announce_button->signal_clicked().connect(sigc::mem_fun(*this, &GameInfoDialog::announce_hyperswines));
      hyperswines_announce_button->signal_clicked().connect(sigc::mem_fun(*this, &GameInfoDialog::close));

      // the button is only shown when the announcement is valid, see add_info().
      hyperswines_announce_button->show_all_children();
    } // hyperswines announce
    { // announcement reply button
      announcement_reply_button
        = Gtk::manage(new Gtk::Button(""));
      add_action_widget(*announcement_reply_button, Gtk::RESPONSE_CLOSE);

      announcement_reply_button->set_can_default();
      announcement_reply_button->grab_default();

      announcement_reply_button->signal_clicked().connect(sigc::mem_fun(*this, &GameInfoDialog::announcement_reply));
      announcement_reply_button->signal_clicked().connect(sigc::mem_fun(*this, &GameInfoDialog::close));

      // the button is only shown when the reply is valid, see add_info().
      announcement_reply_button->show_all_children();
    } // announcement reply button
    add_close_button(*this);
  } // action area

  { // move to the top left corner of the table
    int x = 0, y = 0;
    table().get_window()->get_origin(x, y);
    move(x, y);
  } // move to the top left corner of the table
} // void GameInfoDialog::init()

/** add the information and manage it
 **
 ** @param    info   game information to add
 **/
void
GameInfoDialog::add_info(Information* const info)
{
  infos.push_back(info);

  if (infos.empty()) {
    set_title(info->title);
  } else {
    get_content_area()->add(*Gtk::manage(new Gtk::Separator()));
    set_title(_("Window::game information"));
  }

  get_content_area()->add(*info);
  info->show();

  auto const& game = ui->game();

  { // check for announcement reply
    if (   game.players().count_humans() == 1
        && !ui->game().rule(Rule::Type::knocking)
        && !announcement_reply_button->is_visible()
        && dynamic_cast<Information::Announcement*>(info) ) {
      auto const& announcement_info
        = dynamic_cast<Information::Announcement const&>(*info);
      auto const& human = game.players().human();
      if (   announcement_info.player->team() != human.team()
          && game.announcements().is_valid(Announcement::reply, human)) {
        announcement_reply_button->set_label(_(human.team()));
        announcement_reply_button->show();
      } // if (can reply)
    } // if (is announcement)
  } // check for announcement reply

  { // check for hyperswines announcement
    if (   (game.players().count_humans() == 1)
        && (!hyperswines_announce_button->is_visible())
        && (dynamic_cast<Information::Swines*>(info)) ) {
      if (game.swines().hyperswines_announcement_valid(game.players().human())) {
        hyperswines_announce_button->show();
      } // if (reply valid
    } // if (is announcement)
  } // check for hyperswines announcement
} // void GameInfoDialog::add_info(Information* info)

/** clear the window
 **/
void
GameInfoDialog::clear()
{
  for (auto& info : infos) {
    get_content_area()->remove(*info);
    delete info;
  }

  infos.clear();

  if (announcement_reply_button)
    announcement_reply_button->hide();

  if (hyperswines_announce_button)
    hyperswines_announce_button->hide();
} // void GameinfDialog::clear()

/** @return   the table
 **/
Table&
GameInfoDialog::table()
{
  return *dynamic_cast<Table*>(parent);
} // Table& GameinfDialog::table()

/** show the information and manage it
 **
 ** @param    info                  information to show
 ** @param    close_automatically   whether to close the window automatically
 ** @param    close_delay             the time (in tenth of seconds) to wait
 **                                  before closing the window
 **
 ** @todo      manage multiple delays
 **/
void
GameInfoDialog::show(Information* const info,
                     bool const close_automatically,
                     unsigned const close_delay)
{
  if (!get_realized())
    realize();

  add_info(info);

  present();

#ifdef WORKAROUND
  // @todo   manage multiple delays
  if (timeout_connection)
    timeout_connection.disconnect();

  if (close_automatically)
    timeout_connection
      = Glib::signal_timeout().connect(sigc::slot0<bool>(sigc::mem_fun(*this, &GameInfoDialog::on_timeout)),
                                       close_delay);
#endif

  if (is_blocking()) {
    while (   is_visible()
           && !ui->thrower) {
      ::ui->wait();
    }

    close();
  } // if (is_blocking)
} // void GameInfoDialog::show(Information* info, bool close_automatically, unsigned close_delay)

/** show the information and close it on a click
 **
 ** @param    info   information to show
 **/
void
GameInfoDialog::show(Information* const info)
{
  show(info, false, 0);
} // void GameInfoDialog::show(Information* info)

/** hide the window
 **/
void
GameInfoDialog::close()
{
  if (timeout_connection)
    timeout_connection.disconnect();

  hide();
  clear();
} // void GameInfoDialog::close()

/** update the announcement reply sensitivity
 **/
void
GameInfoDialog::update_announcement_reply()
{
  if (!announcement_reply_button)
    return ;

  auto const& game = ui->game();
  if (   announcement_reply_button->is_visible()
      && !game.announcements().is_valid(Announcement::reply,
                                        game.players().human()) ) {
    announcement_reply_button->hide();
  } // if (reply invalid)
} // void GameInfoDialog::update_announcement_reply()

/** the name of 'player' has changed
 **
 ** @param    player   player whose name has changed
 **/
void
GameInfoDialog::name_changed(Player const& player)
{
  for (auto& info : infos)
    info->name_changed(player);
} // void GameInfoDialog::name_changed(Player player)

/** the timeout event:
 ** close the window and exit the main loop
 **
 ** @return   false (to stop the timeout event)
 **/
bool
GameInfoDialog::on_timeout()
{
  close();

  return false;
} // bool GameInfoDialog::on_timeout()

/** reply to the last announcement
 **/
void
GameInfoDialog::announcement_reply()
{
  auto& game = ui->game();

  DEBUG_ASSERTION((game.players().count_humans() == 1),
                  "GameInfoDialog::announcement_reply()\n"
                  "  game has '" << game.players().count_humans() << "' humans, must be '1'");

  game.announcements().make_announcement(Announcement::reply, game.players().human());
} // void GameInfoDialog::announcement_reply()

/** announce the hyperswines
 **/
void
GameInfoDialog::announce_hyperswines()
{
  auto& game = ui->game();

  DEBUG_ASSERTION(game.players().count_humans() == 1,
                  "GameInfoDialog::announce_hyperswines()\n"
                  "  game has '" << game.players().count_humans() << "' humans, must be '1'");

  game.swines().hyperswines_announce(game.players().human());
} // void GameInfoDialog::announce_hyperswines()

/** @return   whether the dialog is blocking the gameplay
 **/
bool
GameInfoDialog::is_blocking() const
{
  for (auto& i : infos)
    if (i->is_blocking())
      return true;

  return false;
} // bool GameInfoDialog::is_blocking() const

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
