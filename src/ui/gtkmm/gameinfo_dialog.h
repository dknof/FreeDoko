/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"

class Player;

#include "widgets/sticky_dialog.h"
namespace Gtk {
class Button;
}; // namespace Gtk

namespace UI_GTKMM_NS {
class Table;
class GameInfo;

/**
 ** dialog that shows game informations
 **/
class GameInfoDialog : public Base, public Gtk::StickyDialog {
  public:
    class Information;

  public:
    explicit GameInfoDialog(Table* table);
    ~GameInfoDialog() override;

    // the corresponding table
    Table& table();

    // show the information (and manage it)
    void show(Information* game_info,
              bool close_automatically,
              unsigned close_delay);
    // show the information (and manage it)
    void show(Information* game_info);

    // close the window
    void close();

    // update the announcement reply sensitivity
    void update_announcement_reply();
    // the name of 'player' has changed
    void name_changed(Player const& player);

    // on timeout close the window
    bool on_timeout();

  private:
    // initialize the window
    void init();
    // reply to the last announcement
    void announcement_reply();
    // announce hyperswines
    void announce_hyperswines();

    // add the information
    void add_info(Information* info);
    // remove all informations
    void clear();

    // whether the dialog is blocking the gameplay
    bool is_blocking() const;

  protected:
    // announcement reply button
    Gtk::Button* announcement_reply_button = nullptr;
    // announce hyperswines
    Gtk::Button* hyperswines_announce_button = nullptr;
    // variable for the timeout signal
    sigc::connection timeout_connection;

    // the informations which are shown
    vector<Information*> infos;

  private: // unused
    GameInfoDialog() = delete;
    GameInfoDialog(GameInfoDialog const&) = delete;
    GameInfoDialog& operator=(GameInfoDialog const&) = delete;
}; // class GameInfoDialog : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
