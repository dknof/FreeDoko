/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "icons.h"

#include "ui.h"
#include "cards.h"
#include "splash_screen.h" // ToDo: remove it

#include "../status_message.h"

#include "../../basetypes/program_flow_exception.h"
#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../game/game.h"
#include "../../misc/preferences.h"

#include <gtkmm/image.h>
#include <cairomm/context.h>
#include <gdkmm/general.h>

namespace UI_GTKMM_NS {

/** -> result
 **
 ** @param    gametype   the gametype
 **
 ** @return   icon type for 'gametype'
 **/
Icons::Type
  Icons::type(GameType const gametype)
  {
    auto const& game = ::party->game();
    auto const& status = game.status();
    switch(gametype) {
    case GameType::normal:
      return Type::re;
    case GameType::thrown_nines:
      return Type::thrown_nines;
    case GameType::thrown_kings:
      return Type::thrown_kings;
    case GameType::thrown_nines_and_kings:
      return Type::thrown_nines_and_kings;
    case GameType::thrown_richness:
      return Type::thrown_richness;
    case GameType::redistribute:
      return Type::redistribute;
    case GameType::poverty:
      if (status == Game::Status::reservation) {
        auto const& player = game.players().current_player();
        if (player.type() == Player::Type::human) {
          switch (player.hand().count(Card::trump)) {
          case 0:
            return Type::poverty_trumps_0;
          case 1:
            return Type::poverty_trumps_1;
          case 2:
            return Type::poverty_trumps_2;
          case 3:
          case 4:
          case 5:
            return Type::poverty_trumps_3;
          case UINT_MAX:
            return Type::poverty;
          default:
            DEBUG_ASSERTION(false,
                            "Icons::icon(gametype):\n"
                            "  gametype: poverty\n"
                            "  number of poverty cards for player " << player.no() << ' ' << player.name() << " invalid: "
                            << player.hand().count(Card::trump));
            return Type::poverty;
          } // switch (game.poverty_trumpno_returned())
        } else {
          return Type::poverty;
        }
      }
      if (status == Game::Status::poverty_shift) {
        return Type::poverty;
      }

      switch (game.poverty().returned_trumpno()) {
      case 0:
        return Type::poverty_trumps_0;
      case 1:
        return Type::poverty_trumps_1;
      case 2:
        return Type::poverty_trumps_2;
      case 3:
      case 4:
      case 5:
        return Type::poverty_trumps_3;
      case UINT_MAX:
        return Type::poverty;
      default:
        DEBUG_ASSERTION(false,
                        "Icons::icon(gametype):\n"
                        "  gametype: poverty\n"
                        "  number of poverty cards invalid: "
                        << game.poverty().returned_trumpno());
        return Type::poverty;
      } // switch (game.poverty().returned_trumpno())
    case GameType::fox_highest_trump:
      return Type::fox_highest_trump;
    case GameType::marriage:
      return Type::marriage;
    case GameType::marriage_solo:
    case GameType::marriage_silent:
      return Type::marriage_solo;
    case GameType::solo_diamond:
      return Type::solo_diamond;
    case GameType::solo_jack:
      return Type::solo_jack;
    case GameType::solo_queen:
      return Type::solo_queen;
    case GameType::solo_king:
      return Type::solo_king;
    case GameType::solo_queen_jack:
      return Type::solo_queen_jack;
    case GameType::solo_king_jack:
      return Type::solo_king_jack;
    case GameType::solo_king_queen:
      return Type::solo_king_queen;
    case GameType::solo_koehler:
      return Type::solo_koehler;
    case GameType::solo_club:
      return Type::solo_club;
    case GameType::solo_heart:
      return Type::solo_heart;
    case GameType::solo_spade:
      return Type::solo_spade;
    case GameType::solo_meatless:
      return Type::solo_meatless;
    default:
      DEBUG_ASSERTION(false,
                      "Icons::type(gametype):\n"
                      "  unknown gametype '" << gametype << "'");
      return Type::none;
    } // switch(gametype)
  } // static Icons::Type Icons::type(GameType gametype)

/** -> result
 **
 ** @param    marriage_selector   the marriage selector
 **
 ** @return   icon type for 'marriage_selector'
 **/
Icons::Type
  Icons::type(MarriageSelector const marriage_selector)
  {
    switch(marriage_selector) {
    case MarriageSelector::silent:
      return Type::marriage_solo;
    case MarriageSelector::team_set:
      return Type::marriage;
    case MarriageSelector::first_foreign:
      return Type::marriage_foreign;
    case MarriageSelector::first_trump:
      return Type::marriage_trump;
    case MarriageSelector::first_color:
      return Type::marriage_color;
    case MarriageSelector::first_club:
      return Type::marriage_club;
    case MarriageSelector::first_spade:
      return Type::marriage_spade;
    case MarriageSelector::first_heart:
      return Type::marriage_heart;
    } // switch (marriage_selector)
    DEBUG_ASSERTION(false,
                    "Icons::type(marriage_selector):\n"
                    "  unknown marriage selecter '" << marriage_selector << "'");
    return Type::none;
  } // static Icons::Type Icons::type(MarriageSelector marriage_selector)

/** -> result
 **
 ** @param    team      the team
 **
 ** @return   icon type for 'team'
 **/
Icons::Type
  Icons::type(Team const team)
  {
    switch(team) {
    case Team::re:
      return Type::re;
    case Team::contra:
      return Type::contra;
    default:
      return Type::none;
    } // switch(team)
  } // static Icons::Type Icons::type(Team team)

/** -> result
 **
 ** @param    announcement   the announcement
 **
 ** @return   icon type for 'announcement'
 **/
Icons::Type
  Icons::type(Announcement const announcement)
  {
    switch(announcement) {
    case Announcement::noannouncement:
      DEBUG_ASSERTION(false,
                      "Icon::icon(announcement):\n"
                      "  invalid announcement 'noannouncement'");
      break;
    case Announcement::no120:
      return Type::no_120;
    case Announcement::no90:
      return Type::no_90;
    case Announcement::no60:
      return Type::no_60;
    case Announcement::no30:
      return Type::no_30;
    case Announcement::no0:
      return Type::no_0;
    default:
      DEBUG_ASSERTION(false,
                      "Icons::type(announcement):\n"
                      "  unknown announcement '" << announcement << "'");
      return Type::none;
    } // switch(announcement)
    return Type::none;
  } // static Icons::Type Icons::type(Announcement announcement)

/** -> result
 **
 ** @param    announcement   the announcement
 **
 ** @return   icon type for 'announcement'
 **/
Icons::Type
  Icons::type_reply(Announcement const announcement)
  {
    switch(announcement) {
    case Announcement::noannouncement:
      DEBUG_ASSERTION(false,
                      "Icon::icon(announcement):\n"
                      "  invalid announcement 'noannouncement'");
      break;
    case Announcement::no120:
      return Type::no_120_reply;
    case Announcement::no90:
      return Type::no_90_reply;
    case Announcement::no60:
      return Type::no_60_reply;
    case Announcement::no30:
      return Type::no_30_reply;
    case Announcement::no0:
      return Type::no_0_reply;
    default:
      DEBUG_ASSERTION(false,
                      "Icons::type(announcement):\n"
                      "  unknown announcement '" << announcement << "'");
      return Type::none;
    } // switch(announcement)
    return Type::none;
  }

/** -> result
 **
 ** @param    color      the color
 **
 ** @return   swines icon type for 'color'
 **/
Icons::Type
  Icons::type_swines(Card::Color const color)
  {
    switch(color) {
    case Card::club:
      return Type::swines_club;
    case Card::spade:
      return Type::swines_spade;
    case Card::heart:
      return Type::swines_heart;
    case Card::diamond:
      return Type::swines_diamond;
    default:
      DEBUG_ASSERTION(false,
                      "Icon::swines(color)\n"
                      "  Color is not valid\n");
      return Type::none;
    } // switch(color)
  } // static Icons::Type Icons::type_swines(Card::Color color)

/** -> result
 **
 ** @param    color      the color
 **
 ** @return   hyperswines icon type for 'color'
 **/
Icons::Type
  Icons::type_hyperswines(Card::Color const color)
  {
    switch(color) {
    case Card::club:
      return (ui->party().rule()(Rule::Type::with_nines)
              ? Type::hyperswines_club
              : Type::hyperswines_king_club);
    case Card::spade:
      return (ui->party().rule()(Rule::Type::with_nines)
              ? Type::hyperswines_spade
              : Type::hyperswines_king_spade);
    case Card::heart:
      return (ui->party().rule()(Rule::Type::with_nines)
              ? Type::hyperswines_heart
              : Type::hyperswines_king_heart);
    case Card::diamond:
      return (ui->party().rule()(Rule::Type::with_nines)
              ? Type::hyperswines_diamond
              : Type::hyperswines_king_diamond);
    default:
      DEBUG_ASSERTION(false,
                      "Icon::hyperswines(color)\n"
                      "  Color is not valid\n");
      return Type::none;
    } // switch(color)
  } // static Icons::Type Icons::type_hyperswines(Card::Color color)

/** -> result
 **
 ** @param    color      the color
 **
 ** @return   swines-hyperswines icon type for 'color'
 **/
Icons::Type
  Icons::type_swines_hyperswines(Card::Color const color)
  {
    switch(color) {
    case Card::club:
      return (ui->party().rule()(Rule::Type::with_nines)
              ? Type::swines_hyperswines_club
              : Type::swines_hyperswines_king_club);
    case Card::spade:
      return (ui->party().rule()(Rule::Type::with_nines)
              ? Type::swines_hyperswines_spade
              : Type::swines_hyperswines_king_spade);
    case Card::heart:
      return (ui->party().rule()(Rule::Type::with_nines)
              ? Type::swines_hyperswines_heart
              : Type::swines_hyperswines_king_heart);
    case Card::diamond:
      return (ui->party().rule()(Rule::Type::with_nines)
              ? Type::swines_hyperswines_diamond
              : Type::swines_hyperswines_king_diamond);
    default:
      DEBUG_ASSERTION(false,
                      "Icon::swines_hyperswines(color)\n"
                      "  Color is not valid\n");
      return Type::none;
    } // switch(color)
  } // static Icons::Type Icons::type_swines_hyperswines(Card::Color const color)

/** constructor
 **
 ** @param    parent   the parent object
 **/
Icons::Icons(Base* const parent) :
  Base(parent)
{
  load();
  ::preferences.signal_changed(::Preferences::Type::cardset).connect([this](auto) {load();}, disconnector_);
  ::preferences.signal_changed(::Preferences::Type::iconset).connect([this](auto) {load();}, disconnector_);
  ::preferences.signal_changed(::Preferences::Type::cards_height).connect([this](auto) {update_scaling();}, disconnector_);
}

/** destructor
 **/
Icons::~Icons() = default;

/** @return   the scaling
 **/
double
Icons::scaling() const
{
  return ui->cards->scaling();
} // double Icons::scaling() const

/** update the scaling
 **/
void
Icons::update_scaling()
{
  // the scaling value is already updated in 'Cards'

  // set the scaling of the 'ScaledPixbuf'
  for (auto& i : icon_)
    i.set_scaling(scaling());

  update_max_values();
} // void Icons::update_scaling()

/** update the maximal width and height
 **/
void
Icons::update_max_values()
{
  max_width_ = 0;
  for (auto const& i : icon_)
    if (i.get_width() > max_width_)
      max_width_ = i.get_width();

  max_height_ = 0;
  for (auto const& i : icon_)
    if (i.get_height() > max_height_)
      max_height_ = i.get_height();
} // void Icons::update_max_values()

// NOLINTNEXTLINE(misc-no-recursion)
auto Icons::max_width(Rotation const rotation) const -> int
{
  if (   (rotation == Rotation::left)
      || (rotation == Rotation::right))
    return max_height(clockwise(rotation));

  return max_width_;
}


// NOLINTNEXTLINE(misc-no-recursion)
auto Icons::max_height(Rotation const rotation) const -> int
{
  if (   (rotation == Rotation::left)
      || (rotation == Rotation::right))
    return max_width(clockwise(rotation));

  return max_height_;
}

// NOLINTNEXTLINE(misc-no-recursion)
auto Icons::max_width(vector<Glib::RefPtr<Gdk::Pixbuf>> const& icons,
                      Rotation const rotation) const -> int
{
  if (   (rotation == Rotation::left)
      || (rotation == Rotation::right))
    return max_height(icons, clockwise(rotation));

  int width = 0;

  for (auto const& i : icons)
    if (i->get_width() > width)
      width = i->get_width();

  return width;
} // int Icons::max_width(vector<Glib::RefPtr<Gdk::Pixbuf>> icons, Rotation rotation = up) const

// NOLINTNEXTLINE(misc-no-recursion)
auto Icons::max_height(vector<Glib::RefPtr<Gdk::Pixbuf>> const& icons,
                       Rotation const rotation) const -> int
{
  if (   (rotation == Rotation::left)
      || (rotation == Rotation::right))
    return max_width(icons, clockwise(rotation));

  int height = 0;

  for (auto const& i : icons)
    if (i->get_height() > height)
      height = i->get_height();

  return height;
} // int Icons::max_height(vector<Glib::RefPtr<Gdk::Pixbuf>> icons, Rotation rotation = up) const

/** -> result
 **
 ** @param    type      the type
 ** @param    rotation   the rotation
 **
 ** @return   the pixbuf of 'icon'
 **   (with rotation 'rotation')
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::icon(Type const type)
{
  return icon_[static_cast<size_t>(type)];
} // Glib::RefPtr<Gdk::Pixbuf> Icons::icon(Type type)

/** -> result
 **
 ** @param    type   icon type
 **   (default: none)
 ** @param    scaling   additional scaling
 **   (default: 1)
 **
 ** @return   new image with the icon 'type' that is managed
 **   (that is size and theme)
 **/
Gtk::Image*
Icons::new_managed_image(Type const type, double const scaling)
{
  auto icon = this->icon(type);
  auto image = Gtk::manage(new Gtk::Image(icon));
  image->set_data("type", new Type(type));
  image->set_data("scaling", new double(scaling));
  if (scaling != 1)
    image->set(icon->scale_simple(static_cast<int>(icon->get_width() * scaling),
                                  static_cast<int>(icon->get_height() * scaling),
                                  Gdk::INTERP_TILES));

  image->signal_unrealize().connect(sigc::bind<Gtk::Image const* const>(sigc::mem_fun(*this, &Icons::remove_managed), image));

  managed_image_.push_back(image);

  return image;
} // Gtk::Image* Icons::new_managed_image(Type type = none, double scaling = 1)

/** changes the type of 'image'
 **
 ** @param    image   image to change the type of
 ** @param    type   new type
 **/
void
Icons::change_managed(Gtk::Image* const image, Type const type)
{
  DEBUG_ASSERTION(image->get_data("type"),
                  "Icons::change_managed:\n"
                  "  image has no data 'type'");
  // ToDo: Test whether the image is in the vector

  delete static_cast<Type*>(image->steal_data("type"));
  image->set_data("type", new Type(type));

  if (type == Type::none)
    image->clear();
  else
    image->set(icon(type));
} // void Icons::change_managed(Gtk::Image* image, Type type)

/** changes the scaling of 'image'
 **
 ** @param    image   image to change the type of
 ** @param    scaling   new scaling
 **/
void
Icons::change_scaling(Gtk::Image* const image, double const scaling)
{
  DEBUG_ASSERTION(image->get_data("scaling"),
                  "Icons::change_scaling:\n"
                  "  image has no data 'scaling'");
  // ToDo: Test whether the image is in the vector

  delete static_cast<double*>(image->steal_data("scaling"));
  image->set_data("scaling", new double(scaling));

  auto const type = *(static_cast<Type*>(image->get_data("type")));
  if (type == Type::none)
    return ;

  if (scaling == 1)
    image->set(icon(type));
  else {
    auto icon = this->icon(type);
    image->set(icon->scale_simple(static_cast<int>(icon->get_width() * scaling),
                                  static_cast<int>(icon->get_height() * scaling),
                                  Gdk::INTERP_TILES));
  }
} // void Icons::change_scaling(Gtk::Image* image, double scaling)

/** remove 'image' from the managed image list
 **
 ** @param    image   image to remove
 **/
void
Icons::remove_managed(Gtk::Image const* const image)
{
  for (auto i = managed_image_.begin();
       i != managed_image_.end();
      )
    if (*i == image) {
      delete static_cast<Type*>((*i)->steal_data("type"));
      delete static_cast<double*>((*i)->steal_data("scaling"));
      i = managed_image_.erase(i);
      break;
    } else {
      ++i;
    }
} // void Icons::remove_managed(Gtk::Image const* image)

/** update the managed images
 **/
void
Icons::update_managed()
{
  for (auto& i : managed_image_) {
    auto const type = *(static_cast<Type*>(i->get_data("type")));
    if (type == Type::none)
      continue;

    auto const scaling = *(static_cast<double*>(i->get_data("scaling")));
    if ( (scaling == 1)
        || (scaling == 0) )
      i->set(icon(type));
    else {
      auto icon = this->icon(type);
      i->set(icon->scale_simple(static_cast<int>(icon->get_width() * scaling),
                                static_cast<int>(icon->get_height() * scaling),
                                Gdk::INTERP_TILES));
    }
  }
} // void Icons::update_managed()

/** load the icons
 **/
void
Icons::load()
{
  StatusMessages sm(*ui);
  if (   ::party
      && ::party->status() != Party::Status::programstart
      && ::party->status() != Party::Status::initial_loaded) {
    sm.add(_("loading the icons"));
  }

  {
    vector<Gdk::ScaledPixbuf> icon_new;
    auto constexpr number_of_types = icons_type_list.size();
    size_t i = 1;
    for (auto const type : icons_type_list) {
      sm.add(_("loading the icons (%u/%u)", i, static_cast<unsigned>(number_of_types)));
      icon_new.emplace_back(load(static_cast<Type>(type)));
      if (   !::party
          || ::party->status() == Party::Status::programstart
          || ::party->status() == Party::Status::initial_loaded)
        ui->add_progress(1.0 / number_of_types);
      i += 1;
    } // for (n \in names)

    icon_ = icon_new;
  }

  update_scaling();
  update_managed();
  update_max_values();
} // void Icons::load()

/** load or construct the icons
 **
 ** @param    type      the icon type
 **
 ** @return   loaded/constructed icon
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::load(Type const type)
{
  switch(type) {
  case Type::none:
    return {};
  case Type::re:
    return load_from_file_or_construct("re",
                                       {Card::club_queen});
  case Type::contra:
    return load_from_file_or_construct_striked("contra",
                                               {Card::club_queen});
  case Type::thrown_nines:
    return load_from_file_or_construct("thrown_nines",
                                       {Card::club_nine,
                                       Card::spade_nine,
                                       Card::heart_nine,
                                       Card::diamond_nine});
  case Type::thrown_kings:
    return load_from_file_or_construct("thrown_kings",
                                       {Card::club_king,
                                       Card::spade_king,
                                       Card::heart_king,
                                       Card::diamond_king});
  case Type::thrown_nines_and_kings:
    return load_from_file_or_construct("thrown_nines_and_kings",
                                       {Card::club_king,
                                       Card::spade_king,
                                       Card::heart_nine,
                                       Card::diamond_nine });
  case Type::thrown_richness:
    return load_from_file_or_construct("thrown_richness",
                                       {Card::club_ten,
                                       Card::spade_ten,
                                       Card::heart_ten,
                                       Card::diamond_ten});
  case Type::redistribute:
    //return load_from_file("redistribute");
    return load_from_file_or_construct("redistribute",
                                       {Card(), Card(), Card()});

  case Type::fox_highest_trump:
    return load_from_file_or_construct("fox_highest_trump",
                                       {Card::diamond_ace,
                                       Card::diamond_ten,
                                       Card::diamond_king,
                                       Card::diamond_nine});
  case Type::marriage:
    return load_from_file_or_construct("marriage",
                                       {Card::club_queen,
                                       Card::club_queen});
  case Type::marriage_solo:
    return load_from_file_or_construct({"marriage.solo"s, "marriage"s},
                                       {Card::club_queen,
                                       Card::club_queen});
  case Type::marriage_foreign:
    return load_from_file_or_construct({"marriage.foreign"s, "marriage"s},
                                       {Card::club_queen,
                                       Card::club_queen,
                                       Card::diamond_jack,
                                       Card::spade_ace});
  case Type::marriage_trump:
    return load_from_file_or_construct({"marriage.trump"s, "marriage"s},
                                       {Card::club_queen,
                                       Card::club_queen,
                                       Card::diamond_jack,
                                       Card::spade_ace});
  case Type::marriage_color:
    return load_from_file_or_construct({"marriage.color"s, "marriage"s},
                                       {Card::club_queen,
                                       Card::club_queen,
                                       Card::heart_ace,
                                       Card::spade_ace});
  case Type::marriage_club:
    return load_from_file_or_construct({"marriage.club"s, "marriage"s},
                                       {Card::club_queen,
                                       Card::club_queen,
                                       Card::club_ace,
                                       Card::club_ace});
  case Type::marriage_spade:
    return load_from_file_or_construct({"marriage.spade"s, "marriage"s},
                                       {Card::club_queen,
                                       Card::club_queen,
                                       Card::spade_ace,
                                       Card::spade_ace});
  case Type::marriage_heart:
    return load_from_file_or_construct({"marriage.heart"s, "marriage"s},
                                       {Card::club_queen,
                                       Card::club_queen,
                                       Card::heart_ace,
                                       Card::heart_ace});
  case Type::marriage_partner:
    return load_from_file_or_construct({"marriage.partner"s, "re"s},
                                       {Card::club_queen});
  case Type::poverty:
    return load_from_file_or_construct("poverty",
                                       {Card::heart_queen,
                                       Card::spade_jack,
                                       Card::diamond_ten});
  case Type::poverty_trumps_0:
    return load_from_file_or_construct({"poverty.0"s, "poverty"s},
                                       {Card::club_ace,
                                       Card::heart_king,
                                       Card::spade_ten});
  case Type::poverty_trumps_1:
    return load_from_file_or_construct({"poverty.1"s, "poverty"s},
                                       {Card::spade_queen,
                                       Card::heart_nine,
                                       Card::club_king});
  case Type::poverty_trumps_2:
    return load_from_file_or_construct({"poverty.2"s, "poverty"s},
                                       {Card::diamond_queen,
                                       Card::spade_jack,
                                       Card::club_ace});
  case Type::poverty_trumps_3:
    return load_from_file_or_construct({"poverty.3"s, "poverty"s},
                                       {Card::heart_queen,
                                       Card::club_jack,
                                       Card::diamond_king});
  case Type::poverty_partner: {
    return load_from_file_or_construct({"poverty.partner"s, "re"s},
                                       {Card::club_queen});
  }
  case Type::swines_club:
    return load_from_file_or_construct({"swines.club"s, "swines"s},
                                       {Card::club_ace,
                                       Card::club_ace});
  case Type::swines_spade:
    return load_from_file_or_construct({"swines.spade"s, "swines"s},
                                       {Card::spade_ace,
                                       Card::spade_ace});
  case Type::swines_heart:
    return load_from_file_or_construct({"swines.heart"s, "swines"s},
                                       {Card::heart_ace,
                                       Card::heart_ace});
  case Type::swines_diamond:
    return load_from_file_or_construct({"swines.diamond"s, "swines"s},
                                       {Card::diamond_ace,
                                       Card::diamond_ace});
  case Type::hyperswines_club:
    return load_from_file_or_construct({"hyperswines.club"s, "hyperswines"s},
                                       {Card::club_nine,
                                       Card::club_nine});
  case Type::hyperswines_spade:
    return load_from_file_or_construct({"hyperswines.spade"s, "hyperswines"s},
                                       {Card::spade_nine,
                                       Card::spade_nine});
  case Type::hyperswines_heart:
    return load_from_file_or_construct({"hyperswines.heart"s, "hyperswines"s},
                                       {Card::heart_nine,
                                       Card::heart_nine});
  case Type::hyperswines_diamond:
    return load_from_file_or_construct({"hyperswines.diamond"s, "hyperswines"s},
                                       {Card::diamond_nine,
                                       Card::diamond_nine});
  case Type::hyperswines_king_club:
    return load_from_file_or_construct({"hyperswines.king.club"s, "hyperswines"s},
                                       {Card::club_king,
                                       Card::club_king});
  case Type::hyperswines_king_spade:
    return load_from_file_or_construct({"hyperswines.king.spade"s, "hyperswines"s},
                                       {Card::spade_king,
                                       Card::spade_king});
  case Type::hyperswines_king_heart:
    return load_from_file_or_construct({"hyperswines.king.heart"s, "hyperswines"s},
                                       {Card::heart_king,
                                       Card::heart_king});
  case Type::hyperswines_king_diamond:
    return load_from_file_or_construct({"hyperswines.king.diamond"s, "hyperswines"s},
                                       {Card::diamond_king,
                                       Card::diamond_king});
  case Type::swines_hyperswines_club:
    return load_from_file_or_construct({"swines-hyperswines.club"s, "swines-hyperswines"s},
                                       {Card::club_ace,
                                       Card::club_nine});
  case Type::swines_hyperswines_spade:
    return load_from_file_or_construct({"swines-hyperswines.spade"s, "swines-hyperswines"s},
                                       {Card::spade_ace,
                                       Card::spade_nine});
  case Type::swines_hyperswines_heart:
    return load_from_file_or_construct({"swines-hyperswines.heart"s, "swines-hyperswines"s},
                                       {Card::heart_ace,
                                       Card::heart_nine});
  case Type::swines_hyperswines_diamond:
    return load_from_file_or_construct({"swines-hyperswines.diamond"s, "swines-hyperswines"s},
                                       {Card::diamond_ace,
                                       Card::diamond_nine});
  case Type::swines_hyperswines_king_club:
    return load_from_file_or_construct({"swines-hyperswines.king.club"s, "swines-hyperswines"s},
                                       {Card::club_ace,
                                       Card::club_king});
  case Type::swines_hyperswines_king_spade:
    return load_from_file_or_construct({"swines-hyperswines.king.spade"s, "swines-hyperswines"s},
                                       {Card::spade_ace,
                                       Card::spade_king});
  case Type::swines_hyperswines_king_heart:
    return load_from_file_or_construct({"swines-hyperswines.king.heart"s, "swines-hyperswines"s},
                                       {Card::heart_ace,
                                       Card::heart_king});
  case Type::swines_hyperswines_king_diamond:
    return load_from_file_or_construct({"swines-hyperswines.king.diamond"s, "swines-hyperswines"s},
                                       {Card::diamond_ace,
                                       Card::diamond_king});
  case Type::dullen:
    return load_from_file_or_construct("dullen",
                                       {Card::heart_ten,
                                       Card::heart_ten});

  case Type::doppelkopf:
    return load_from_file_or_construct("doppelkopf",
                                       {Card::club_ace,
                                       Card::spade_ten,
                                       Card::diamond_ace,
                                       Card::heart_ten});
  case Type::solo_club:
    return load_from_file_or_construct("solo.club",
                                       {Card::spade_queen,
                                       Card::heart_jack,
                                       Card::club_ace,
                                       Card::club_king});
  case Type::solo_spade:
    return load_from_file_or_construct("solo.spade",
                                       {Card::club_queen,
                                       Card::diamond_jack,
                                       Card::spade_ace,
                                       Card::spade_king});
  case Type::solo_heart:
    return load_from_file_or_construct("solo.heart",
                                       {Card::diamond_queen,
                                       Card::club_jack,
                                       Card::heart_ace,
                                       Card::heart_king});
  case Type::solo_diamond:
    return load_from_file_or_construct("solo.diamond",
                                       {Card::heart_queen,
                                       Card::spade_jack,
                                       Card::diamond_ace,
                                       Card::diamond_king});
  case Type::solo_jack:
    return load_from_file_or_construct("solo.jack",
                                       {Card::club_jack,
                                       Card::spade_jack,
                                       Card::heart_jack,
                                       Card::diamond_jack});
  case Type::solo_queen:
    return load_from_file_or_construct("solo.queen",
                                       {Card::club_queen,
                                       Card::spade_queen,
                                       Card::heart_queen,
                                       Card::diamond_queen});
  case Type::solo_king:
    return load_from_file_or_construct("solo.king",
                                       {Card::club_king,
                                       Card::spade_king,
                                       Card::heart_king,
                                       Card::diamond_king});
  case Type::solo_queen_jack:
    return load_from_file_or_construct("solo.queen-jack",
                                       {Card::club_queen,
                                       Card::spade_queen,
                                       Card::heart_jack,
                                       Card::diamond_jack});
  case Type::solo_king_jack:
    return load_from_file_or_construct("solo.king-jack",
                                       {Card::club_king,
                                       Card::spade_king,
                                       Card::heart_jack,
                                       Card::diamond_jack});
  case Type::solo_king_queen:
    return load_from_file_or_construct("solo.king-queen",
                                       {Card::club_king,
                                       Card::spade_king,
                                       Card::heart_queen,
                                       Card::diamond_queen});
  case Type::solo_koehler:
    return load_from_file_or_construct("solo.koehler",
                                       {Card::club_king,
                                       Card::spade_king,
                                       Card::heart_queen,
                                       Card::diamond_jack});
  case Type::solo_meatless:
    return load_from_file_or_construct("solo.meatless",
                                       {Card::club_ace,
                                       Card::spade_ace,
                                       Card::heart_ace,
                                       Card::diamond_ace});
  case Type::no_120:
    return load_from_file_or_construct_striked("no_120", 120);
  case Type::no_90:
    return load_from_file_or_construct_striked("no_90", 90);
  case Type::no_60:
    return load_from_file_or_construct_striked("no_60", 60);
  case Type::no_30:
    return load_from_file_or_construct_striked("no_30", 30);
  case Type::no_0:
    return load_from_file_or_construct_striked("no_0", 0);
  case Type::no_120_reply:
    return load_from_file_or_construct("no_120_reply", 120);
  case Type::no_90_reply:
    return load_from_file_or_construct("no_90_reply", 90);
  case Type::no_60_reply:
    return load_from_file_or_construct("no_60_reply", 60);
  case Type::no_30_reply:
    return load_from_file_or_construct("no_30_reply", 30);
  case Type::no_0_reply:
    return load_from_file_or_construct("no_0_reply", 0);
  } // switch(type)

  return {};
}

/** load the icon from the file
 **
 ** @param    file   name of the file
 **
 ** @return   loaded or empty icon
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::load_from_file(string const file)
{
  return load_from_file(vector<string>{file});
}

/** load the icon from the files
 **
 ** @param    files   name of the files
 **
 ** @return   loaded or empty icon
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::load_from_file(vector<string> files)
{
  auto const iconset = ::preferences(::Preferences::Type::iconset);
  auto directories = ::preferences.directories(::Preferences::Type::iconset);
  for (auto& d : directories)
    d = d / iconset;
  for (auto const& d : ::preferences.directories(::Preferences::Type::cardset))
    directories.push_back(d / "icons");
  for (auto const& path : directories) {
    for (auto const& file : files) {
      for (auto const& graphic_format : supported_graphic_formats) {
        auto const filename = path / (file + graphic_format);
        if (!is_regular_file(filename))
          continue;
        auto icon = Gdk::Pixbuf::create_from_file(filename.string());
        if (icon)
          return icon;
      }
    }
  }

  return {};
}

/** load the icon from the file or construct it
 **
 ** @param    name     name of the file
 ** @param    cards    cards to construct from
 **
 ** @return   loaded or empty icon
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::load_from_file_or_construct(vector<string> const files,
                                   vector<Card> const& cards)
{
  auto icon = load_from_file(files);
  if (icon)
    return icon;
  return construct(cards);
}

/** load the icon from the file or construct it
 **
 ** @param    name     name of the file
 ** @param    cards    cards to construct from
 **
 ** @return   loaded or empty icon
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::load_from_file_or_construct(string const file,
                                   vector<Card> const& cards)
{
  return load_from_file_or_construct(vector<string>{file}, cards);
}

/** load the icon from the file or construct it with strike through
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::load_from_file_or_construct_striked(string file,
                                           vector<Card> const& cards)
{
  auto icon = load_from_file(file);
  if (icon)
    return icon;
  return strike(construct(cards));
}

/** load the icon from the file or construct it with strike through
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::load_from_file_or_construct(string file,
                                   unsigned points)
{
  auto icon = load_from_file(file);
  if (icon)
    return icon;
  return construct(points);
}

/** load the icon from the file or construct it with strike through
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::load_from_file_or_construct_striked(string file,
                                           unsigned points)
{
  auto pixbuf = load_from_file_or_construct(file, points);
  return strike(pixbuf);
}

/** construct an icon from the cards
 **
 ** @param    cards   cards
 **
 ** @return   constructed icon
 **
 ** @bug   the transparent regions are copied, too
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::construct(vector<Card> const& cards)
{
  // take the half height and overlap with a fourth width

  int const single_width = ui->cards->width_original() * 0.75;
  double const scale = static_cast<double>(single_width) / ui->cards->width_original();
  int const step_x = ui->cards->width_original() / 4;
  int const width = single_width + (cards.size() - 1) * step_x;
  int const height = (ui->cards->height_original() / 2) * scale; // NOLINT(bugprone-integer-division)

  auto surface = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, width, height);
  auto cr = Cairo::Context::create(surface);
  cr->scale(scale, scale);
  //Gdk::Cairo::set_source_pixbuf(cr, pixbuf, 0, 0);

  for (unsigned i = 0; i < cards.size(); i++) {
    auto pixbuf = ui->cards->card(cards[i]);
    Gdk::Cairo::set_source_pixbuf(cr, pixbuf, i * step_x, 0);
    cr->paint();
  }


  return Gdk::Pixbuf::create(surface, 0, 0, width, height);
}

/** construct a pixbuf from the points
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::construct(unsigned points)
{
  static constexpr auto scale = 1.5;
  auto font = Pango::FontDescription(::preferences(::Preferences::Type::name_font));
  font.set_style(Pango::STYLE_NORMAL);
  font.set_weight(Pango::WEIGHT_HEAVY);
  font.set_size(scale * font.get_size());
  int width = 0, height = 0;
  {
    auto surface = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, 1000, 1000);
    auto cr = Cairo::Context::create(surface);
    auto layout = Pango::Layout::create(cr);
    layout->set_font_description(font);
    layout->set_text("3690");
    layout->get_pixel_size(width, height);
  }

  auto surface = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, width, height);
  auto cr = Cairo::Context::create(surface);
  auto layout = Pango::Layout::create(cr);
  layout->set_font_description(font);
  layout->set_text(std::to_string(points));
  {
    int width = 0, height = 0;
    layout->get_pixel_size(width, height);
    cr->translate(-width / 2, 0); // NOLINT(bugprone-integer-division)
  }
  cr->translate(width / 2, 0); // NOLINT(bugprone-integer-division)
  layout->show_in_cairo_context(cr);
  return Gdk::Pixbuf::create(surface, 0, 0, width, height);
}


/** strike the pixbuf through
 **/
Glib::RefPtr<Gdk::Pixbuf>
Icons::strike(Glib::RefPtr<Gdk::Pixbuf> pixbuf)
{
  auto const width = pixbuf->get_width();
  auto const height = pixbuf->get_height();
  auto surface = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, width, height);
  auto cr = Cairo::Context::create(surface);
  {
    Gdk::Cairo::set_source_pixbuf(cr, pixbuf, 0, 0);
    cr->paint();
  }
  {
    cr->set_source_rgba(1, 0, 0, 0.7);
    cr->set_line_width(height / 6); // NOLINT(bugprone-integer-division)
    cr->move_to(-cr->get_line_width() / 2, height - cr->get_line_width() / 2);
    cr->line_to(width + cr->get_line_width() / 2, cr->get_line_width() / 2);
    cr->stroke();
  }
  return Gdk::Pixbuf::create(surface, 0, 0, width, height);
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
