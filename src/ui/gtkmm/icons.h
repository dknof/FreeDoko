/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"

#include "icons_type.h"
#include "widgets/scaled_pixbuf.h"

#include "../../basetypes/rotation.h"
#include "../../basetypes/announcement.h"
#include "../../basetypes/game_type.h"
#include "../../basetypes/marriage_selector.h"
#include "../../basetypes/team.h"
#include "../../card/card.h"

class Player;

#include <glibmm/refptr.h>
namespace Gtk {
class Image;
}; // namespace Gtk

namespace UI_GTKMM_NS {

/**
 ** the icons
 **/
class Icons : public Base, public sigc::trackable {
  friend class Icongroup;
  public:
  using Type = IconsType;

  static Type type(GameType gametype);
  static Type type(MarriageSelector marriage_selector);
  static Type type(Team team);
  static Type type(Announcement announcement);
  static Type type_reply(Announcement announcement);
  static Type type_swines(Card::Color color);
  Type type_hyperswines(Card::Color color);
  Type type_swines_hyperswines(Card::Color color);

  public:
  explicit Icons(Base* base);
  Icons(Icons const&)            = delete;
  Icons& operator=(Icons const&) = delete;
  ~Icons() override;

  double scaling() const;
  void update_scaling();

  int max_width(Rotation rotation = Rotation::up) const;
  int max_height(Rotation rotation = Rotation::up) const;
  int max_width(vector<Glib::RefPtr<Gdk::Pixbuf>> const& icons,
                Rotation rotation = Rotation::up) const;
  int max_height(vector<Glib::RefPtr<Gdk::Pixbuf>> const& icons,
                 Rotation rotation = Rotation::up) const;

  // the specific icon
  Glib::RefPtr<Gdk::Pixbuf> icon(Type type) ;

  // the specific gametype icon
  Glib::RefPtr<Gdk::Pixbuf> icon(GameType gametype)
    { return this->icon(this->type(gametype)); }
  // the specific marriage selector icon
  Glib::RefPtr<Gdk::Pixbuf> icon(MarriageSelector marriage_selector)
    { return this->icon(this->type(marriage_selector)); }
  // the specific team icon
  Glib::RefPtr<Gdk::Pixbuf> icon(Team team)
    { return this->icon(this->type(team)); }
  // the specific announcement icon
  Glib::RefPtr<Gdk::Pixbuf> icon(Announcement announcement)
    { return this->icon(this->type(announcement)); }
  // the specific announcement reply icon
  Glib::RefPtr<Gdk::Pixbuf> icon_reply(Announcement announcement)
    { return this->icon(this->type_reply(announcement)); }
  // the specific swines icon
  Glib::RefPtr<Gdk::Pixbuf> swines(Card::Color color)
    { return this->icon(this->type_swines(color)); }
  // the specific hyperswines icon
  Glib::RefPtr<Gdk::Pixbuf> hyperswines(Card::Color color)
    { return this->icon(this->type_hyperswines(color)); }
  // the specific swines-hyperswines icon
  Glib::RefPtr<Gdk::Pixbuf> swines_hyperswines(Card::Color color)
    { return this->icon(this->type_swines_hyperswines(color)); }


  // managed images
  Gtk::Image* new_managed_image(Type type = Type::none, double scaling = 1);

  void change_managed(Gtk::Image* image, Type type);
  void change_managed(Gtk::Image* image,
                      GameType gametype)
  { this->change_managed(image, this->type(gametype)); }
  void change_managed(Gtk::Image* image,
                      MarriageSelector marriage_selector)
  { this->change_managed(image, this->type(marriage_selector)); }
  void change_managed(Gtk::Image* image,
                      Team team)
  { this->change_managed(image, this->type(team)); }
  void change_managed(Gtk::Image* image,
                      Announcement announcement)
  { this->change_managed(image, this->type(announcement)); }
  void change_managed_swines(Gtk::Image* image,
                             Card::Color color)
  { this->change_managed(image, this->type_swines(color)); }
  void change_managed_hyperswines(Gtk::Image* image,
                                  Card::Color color)
  { this->change_managed(image, this->type_hyperswines(color)); }
  void change_scaling(Gtk::Image* image, double scaling);

  void remove_managed(Gtk::Image const* image);

  void update_managed();

  // update the maximal width and height
  void update_max_values();

  void load();

  Glib::RefPtr<Gdk::Pixbuf> load(Type type);
  private:
  Glib::RefPtr<Gdk::Pixbuf> load_from_file(string file);
  Glib::RefPtr<Gdk::Pixbuf> load_from_file(vector<string> files);
  Glib::RefPtr<Gdk::Pixbuf> load_from_file_or_construct(string file, vector<Card> const& cards);
  Glib::RefPtr<Gdk::Pixbuf> load_from_file_or_construct(vector<string> files, vector<Card> const& cards);
  Glib::RefPtr<Gdk::Pixbuf> load_from_file_or_construct_striked(string file, vector<Card> const& cards);
  Glib::RefPtr<Gdk::Pixbuf> load_from_file_or_construct(string file, unsigned points);
  Glib::RefPtr<Gdk::Pixbuf> load_from_file_or_construct_striked(string file, unsigned points);
  Glib::RefPtr<Gdk::Pixbuf> construct(vector<Card> const& cards);
  Glib::RefPtr<Gdk::Pixbuf> construct(unsigned points);
  Glib::RefPtr<Gdk::Pixbuf> strike(Glib::RefPtr<Gdk::Pixbuf> pixbuf);

  private:
  AutoDisconnector disconnector_;
  // the icons
  vector<Gdk::ScaledPixbuf> icon_;
  // maximal width of the icons
  int max_width_ = 0;
  // maximal height of the icons
  int max_height_ = 0;

  // the managed images
  vector<Gtk::Image*> managed_image_;
}; // class Icons : public Base

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
