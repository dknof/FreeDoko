/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"

namespace UI_GTKMM_NS {

enum class IconsType {
  none,
  re,
  contra,
  thrown_nines,
  thrown_kings,
  thrown_nines_and_kings,
  thrown_richness,
  redistribute,
  fox_highest_trump,
  marriage,
  marriage_solo,
  marriage_foreign,
  marriage_trump,
  marriage_color,
  marriage_club,
  marriage_spade,
  marriage_heart,
  marriage_partner,
  poverty,
  poverty_trumps_0,
  poverty_trumps_1,
  poverty_trumps_2,
  poverty_trumps_3,
  poverty_partner,
  swines_club,
  swines_spade,
  swines_heart,
  swines_diamond,
  swines = swines_diamond,
  hyperswines_club,
  hyperswines_spade,
  hyperswines_heart,
  hyperswines_diamond,
  hyperswines = hyperswines_diamond,
  hyperswines_king_club,
  hyperswines_king_spade,
  hyperswines_king_heart,
  hyperswines_king_diamond,
  swines_hyperswines_club,
  swines_hyperswines_spade,
  swines_hyperswines_heart,
  swines_hyperswines_diamond,
  swines_hyperswines = swines_hyperswines_diamond,
  swines_hyperswines_king_club,
  swines_hyperswines_king_spade,
  swines_hyperswines_king_heart,
  swines_hyperswines_king_diamond,
  dullen,
  doppelkopf,
  solo_club,
  solo_spade,
  solo_heart,
  solo_diamond,
  solo_jack,
  solo_queen,
  solo_king,
  solo_queen_jack,
  solo_king_jack,
  solo_king_queen,
  solo_koehler,
  solo_meatless,
  no_120_reply,
  no_90_reply,
  no_60_reply,
  no_30_reply,
  no_0_reply,
  no_120,
  no_90,
  no_60,
  no_30,
  no_0
};

#ifdef WORKAROUND
constexpr std::array<IconsType, 69> icons_type_list = {
#else
  constexpr auto icons_type_list = std::make_array({
                                                   })
#endif
  IconsType::none,
    IconsType::re,
    IconsType::contra,
    IconsType::thrown_nines,
    IconsType::thrown_kings,
    IconsType::thrown_nines_and_kings,
    IconsType::thrown_richness,
    IconsType::redistribute,
    IconsType::fox_highest_trump,
    IconsType::marriage,
    IconsType::marriage_solo,
    IconsType::marriage_foreign,
    IconsType::marriage_trump,
    IconsType::marriage_color,
    IconsType::marriage_club,
    IconsType::marriage_spade,
    IconsType::marriage_heart,
    IconsType::marriage_partner,
    IconsType::poverty,
    IconsType::poverty_trumps_0,
    IconsType::poverty_trumps_1,
    IconsType::poverty_trumps_2,
    IconsType::poverty_trumps_3,
    IconsType::poverty_partner,
    IconsType::swines_club,
    IconsType::swines_spade,
    IconsType::swines_heart,
    IconsType::swines_diamond,
    IconsType::hyperswines_club,
    IconsType::hyperswines_spade,
    IconsType::hyperswines_heart,
    IconsType::hyperswines_diamond,
    IconsType::hyperswines_king_club,
    IconsType::hyperswines_king_spade,
    IconsType::hyperswines_king_heart,
    IconsType::hyperswines_king_diamond,
    IconsType::swines_hyperswines_club,
    IconsType::swines_hyperswines_spade,
    IconsType::swines_hyperswines_heart,
    IconsType::swines_hyperswines_diamond,
    IconsType::swines_hyperswines_king_club,
    IconsType::swines_hyperswines_king_spade,
    IconsType::swines_hyperswines_king_heart,
    IconsType::swines_hyperswines_king_diamond,
    IconsType::dullen,
    IconsType::doppelkopf,
    IconsType::solo_club,
    IconsType::solo_spade,
    IconsType::solo_heart,
    IconsType::solo_diamond,
    IconsType::solo_jack,
    IconsType::solo_queen,
    IconsType::solo_king,
    IconsType::solo_queen_jack,
    IconsType::solo_king_jack,
    IconsType::solo_king_queen,
    IconsType::solo_koehler,
    IconsType::solo_meatless,
    IconsType::no_120_reply,
    IconsType::no_90_reply,
    IconsType::no_60_reply,
    IconsType::no_30_reply,
    IconsType::no_0_reply,
    IconsType::no_120,
    IconsType::no_90,
    IconsType::no_60,
    IconsType::no_30,
    IconsType::no_0,
};

bool operator!(IconsType type);

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
