/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "icons_window.h"

#include "ui.h"

#include "../../misc/preferences.h"

#include <gtkmm/grid.h>
#include <gtkmm/image.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 **/
IconsWindow::IconsWindow(Base* const parent) : // NOLINT(cppcoreguidelines-pro-type-member-init,hicpp-member-init)
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::IconsWindow"), false)
{
  ui->add_window(*this);
  signal_realize().connect(sigc::mem_fun(*this,
                                               &IconsWindow::init));
}

/** destructor
 **/
IconsWindow::~IconsWindow() = default;

/** create all subelements
 **/
void
IconsWindow::init()
{
  set_icon(ui->icon);

  set_skip_taskbar_hint();

  add_close_button(*this);

  container = Gtk::manage(new Gtk::Grid());
  get_content_area()->pack_start(*container, Gtk::PACK_SHRINK);
  update_icons();

  show_all_children();

  for (auto const type : {::Preferences::Type::cardset,
       ::Preferences::Type::cards_back,
       ::Preferences::Type::iconset}) {
  ::preferences.signal_changed(type).connect_back([this](auto value){update_icons();}, disconnector_);
  }
}

/** update the icons
 **/
void
IconsWindow::update_icons()
{
  get_content_area()->remove(*container);
  container = Gtk::manage(new Gtk::Grid());
  container->set_row_spacing(1 EX);
  add_icons();
  get_content_area()->pack_start(*container, Gtk::PACK_SHRINK);
  container->show_all_children();
  container->show();
}

/** add the icons
 **/
void
IconsWindow::add_icons()
{
  int row = 0;
  add_icon(IconsType::re, 0, row);
  add_icon(IconsType::contra, 1, row);
  add_icon(IconsType::marriage_partner, 0, row);
  add_icon(IconsType::poverty_partner, 1, row);
  row += 1;
  add_icon(IconsType::thrown_nines, 0, row);
  add_icon(IconsType::thrown_kings, 1, row);
  add_icon(IconsType::thrown_nines_and_kings, 2, row);
  add_icon(IconsType::thrown_richness, 3, row);
  add_icon(IconsType::redistribute, 4, row);
  add_icon(IconsType::fox_highest_trump, 5, row);
  row += 1;
  add_icon(IconsType::marriage, 0, row);
  add_icon(IconsType::marriage_solo, 1, row);
  add_icon(IconsType::marriage_foreign, 2, row);
  add_icon(IconsType::marriage_trump, 3, row);
  add_icon(IconsType::marriage_color, 4, row);
  add_icon(IconsType::marriage_club, 5, row);
  add_icon(IconsType::marriage_spade, 6, row);
  add_icon(IconsType::marriage_heart, 7, row);
  row += 1;
  add_icon(IconsType::poverty, 0, row);
  add_icon(IconsType::poverty_trumps_0, 1, row);
  add_icon(IconsType::poverty_trumps_1, 2, row);
  add_icon(IconsType::poverty_trumps_2, 3, row);
  add_icon(IconsType::poverty_trumps_3, 4, row);
  row += 1;
  add_icon(IconsType::swines_club, 0, row);
  add_icon(IconsType::swines_spade, 1, row);
  add_icon(IconsType::swines_heart, 2, row);
  add_icon(IconsType::swines_diamond, 3, row);
  add_icon(IconsType::dullen, 4, row);
  add_icon(IconsType::doppelkopf, 5, row);
  row += 1;
  add_icon(IconsType::hyperswines_club, 0, row);
  add_icon(IconsType::hyperswines_spade, 1, row);
  add_icon(IconsType::hyperswines_heart, 2, row);
  add_icon(IconsType::hyperswines_diamond, 3, row);
  add_icon(IconsType::hyperswines_king_club, 4, row);
  add_icon(IconsType::hyperswines_king_spade, 5, row);
  add_icon(IconsType::hyperswines_king_heart, 6, row);
  add_icon(IconsType::hyperswines_king_diamond, 7, row);
  row += 1;
  add_icon(IconsType::swines_hyperswines_club, 0, row);
  add_icon(IconsType::swines_hyperswines_spade, 1, row);
  add_icon(IconsType::swines_hyperswines_heart, 2, row);
  add_icon(IconsType::swines_hyperswines_diamond, 3, row);
  add_icon(IconsType::swines_hyperswines_king_club, 4, row);
  add_icon(IconsType::swines_hyperswines_king_spade, 5, row);
  add_icon(IconsType::swines_hyperswines_king_heart, 6, row);
  add_icon(IconsType::swines_hyperswines_king_diamond, 7, row);
  row += 1;
  add_icon(IconsType::solo_club, 0, row);
  add_icon(IconsType::solo_spade, 1, row);
  add_icon(IconsType::solo_heart, 2, row);
  add_icon(IconsType::solo_diamond, 3, row);
  add_icon(IconsType::solo_meatless, 4, row);
  row += 1;
  add_icon(IconsType::solo_jack, 0, row);
  add_icon(IconsType::solo_queen, 1, row);
  add_icon(IconsType::solo_king, 2, row);
  add_icon(IconsType::solo_queen_jack, 3, row);
  add_icon(IconsType::solo_king_jack, 4, row);
  add_icon(IconsType::solo_king_queen, 5, row);
  add_icon(IconsType::solo_koehler, 6, row);
  row += 1;
  add_icon(IconsType::no_120, 0, row);
  add_icon(IconsType::no_90, 1, row);
  add_icon(IconsType::no_60, 2, row);
  add_icon(IconsType::no_30, 3, row);
  add_icon(IconsType::no_0, 4, row);
  row += 1;
  add_icon(IconsType::no_120_reply, 0, row);
  add_icon(IconsType::no_90_reply, 1, row);
  add_icon(IconsType::no_60_reply, 2, row);
  add_icon(IconsType::no_30_reply, 3, row);
  add_icon(IconsType::no_0_reply, 4, row);
}

/** add one icon
 **/
void
IconsWindow::add_icon(Icons::Type const type, int const left, int const top)
{
  auto image = Gtk::manage(new Gtk::Image(ui->icons->icon(type)));
  container->attach(*image, left, top, 1, 1);
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
