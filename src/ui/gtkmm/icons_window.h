/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"
#include "widgets/sticky_dialog.h"
#include "icons.h"

namespace Gtk {
class Grid;
} // namespace Gtk

namespace UI_GTKMM_NS {

/**
 ** the changelog dialog
 **
 ** @todo   use treeview for the different versions
 **/
class IconsWindow : public Base, public Gtk::StickyDialog {
  public:
  explicit IconsWindow(Base* parent);
  IconsWindow() = delete;
  IconsWindow(IconsWindow const&) = delete;
  IconsWindow& operator=(IconsWindow const&) = delete;

  ~IconsWindow() override;

  private:
  void init();
  void update_icons();
  void add_icons();
  void add_icon(Icons::Type type, int left, int top);

  private:
  AutoDisconnector disconnector_;
  Gtk::Grid* container;
};

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
