/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"
#include "widgets/sticky_dialog.h"
#include "trick_summary.h"

class Trick;
class Player;

namespace Gtk {
class Button;
class Box;
}; // namespace Gtk

namespace UI_GTKMM_NS {
class Table;

/**
 ** the last trick window
 **/
class LastTrick : public Base, public Gtk::StickyDialog {
  public:
    explicit LastTrick(Table* table);
    ~LastTrick() override;

    // open the window and show the last trick
    void show_last_trick(::Trick const& last_trick);
    // close the window
    void close();

    // the current tricknumber
    unsigned trickno() const;
    // set the current trick number
    void set_trickno(unsigned trickno);

    // the name of a player has changed
    void name_changed(::Player const& player);

    // a key has been pressed
    bool key_press(GdkEventKey* key);

  private:
    // initialize the elements
    void init();

    // show the previous trick
    void previous_trick();
    // show the next trick
    void following_trick();

    // on hide signal
    void on_hide() override;

  private:
    // the trick shown
    unsigned last_trick_no = UINT_MAX;

    Gtk::Box* navigation_container = nullptr;
    Gtk::Button* previous_trick_button = nullptr;
    Gtk::Button* following_trick_button = nullptr;

    unique_ptr<TrickSummary> trick_summary;

  private: // unused
    LastTrick() = delete;
    LastTrick(LastTrick const&) = delete;
    LastTrick& operator=(LastTrick const&) = delete;
}; // class LastTrick : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
