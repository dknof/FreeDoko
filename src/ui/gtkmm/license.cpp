/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "license.h"

#include "ui.h"

#include "../../misc/preferences.h"

#include <gtkmm/image.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textview.h>
#include <gtkmm/notebook.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 **/
License::License(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::license"), false)
{
  ui->add_window(*this);
  signal_realize().connect(sigc::mem_fun(*this,
                                               &License::init));
} // License::License(Base* parent)

/** destruktor
 **/
License::~License() = default;

/** create all subelements
 **/
void
License::init()
{
  set_icon(ui->icon);

  set_skip_taskbar_hint();

  program_text = Gtk::manage(new Gtk::TextView());
  program_text->get_buffer()->set_text(::gpl_txt);
  cardset_text = Gtk::manage(new Gtk::TextView());
  iconset_text = Gtk::manage(new Gtk::TextView());

  add_close_button(*this);

  set_default_size((ui->logo->get_width() * 3) / 2,
                         (ui->logo->get_height() * 5) / 2);
#ifdef POSTPONED
  get_window()->set_decorations(Gdk::DECOR_BORDER
                                      | Gdk::DECOR_RESIZEH
                                      | Gdk::DECOR_TITLE
                                      | Gdk::DECOR_MENU);
#endif

  { // the image
    Gtk::Image* image = Gtk::manage(new Gtk::Image(parent->ui->logo));
    get_content_area()->pack_start(*image, Gtk::PACK_SHRINK);
  } // the image
  { // a notebook whith the different licenses
    auto notebook = Gtk::manage(new Gtk::Notebook);
    //notebook->set_tab_pos(Gtk::POS_LEFT);
    { // the program text
      auto text_window = Gtk::manage(new Gtk::ScrolledWindow());
      text_window->add(*(program_text));
      text_window->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
      program_text->set_editable(false);
      program_text->set_wrap_mode(Gtk::WRAP_WORD);
      program_text->set_cursor_visible(false);

      Gtk::Label* label = Gtk::manage(new Gtk::Label(_("program license")));
      notebook->append_page(*text_window, *label);
    } // the program text
    { // the cardset text
      auto text_window = Gtk::manage(new Gtk::ScrolledWindow());
      text_window->add(*(cardset_text));
      text_window->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
      cardset_text->set_editable(false);
      cardset_text->set_wrap_mode(Gtk::WRAP_WORD);
      cardset_text->set_cursor_visible(false);

      auto label = Gtk::manage(new Gtk::Label(_("cardset license")));
      notebook->append_page(*text_window, *label);
    } // the cardset text
    { // the iconset text
      auto text_window = Gtk::manage(new Gtk::ScrolledWindow());
      text_window->add(*(iconset_text));
      text_window->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
      iconset_text->set_editable(false);
      iconset_text->set_wrap_mode(Gtk::WRAP_WORD);
      iconset_text->set_cursor_visible(false);

      auto label = Gtk::manage(new Gtk::Label(_("iconset license")));
      notebook->append_page(*text_window, *label);
    } // the iconset text
    get_content_area()->pack_start(*notebook);
  } // a notebook whith the different licenses

  license_update();
  show_all_children();
} // void License::init()

/** updates the license
 **/
void
License::license_update()
{
  if (!get_realized())
    return ;

  cardset_text->get_buffer()->set_text(::preferences(::Preferences::Type::cardset_license));
  iconset_text->get_buffer()->set_text(::preferences(::Preferences::Type::iconset_license));
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
