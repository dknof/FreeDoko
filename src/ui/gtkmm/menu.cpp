/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "menu.h"

#include "ui.h"
#include "main_window.h"
#include "party_settings.h"
#include "rules.h"
#include "players.h"
#include "players_db.h"
#include "game_debug.h"
#include "preferences.h"
#include "table.h"
#include "party_points.h"
#include "bug_report.h"
#include "help.h"
#include "license.h"
#include "changelog.h"
#include "support.h"
#include "about.h"

#include "../../party/rule.h"
#include "../../game/game.h"
#include "../../misc/preferences.h"
#include "../../utils/string.h"

#include <gtkmm/menu.h>
#include <gtkmm/menuitem.h>
#include <gtkmm/imagemenuitem.h>
#include <gtkmm/image.h>
#include <gtkmm/checkmenuitem.h>
#include <gtkmm/separatormenuitem.h>
#include <gtkmm/notebook.h>
#include <gtkmm/filechooserdialog.h>

namespace UI_GTKMM_NS {

/** Constructor
 **
 ** @param    main_window   the corresponding main window
 **/
Menu::Menu(MainWindow* const main_window) :
  Base(main_window),
  main_window(main_window)
{
  init();
} // Menu::Menu(MainWindow* main_window)

/** Destruktor
 **/
Menu::~Menu() = default;

/** creates all subelements
 **/
void
Menu::init()
{
  { // party
    party_menu = Gtk::manage(new Gtk::Menu());
    party      = create_menu_item(_("Menu::party"));
    new_party  = create_menu_item(_("Menu::Item::new party")       , "arrow-right");
    save_party = create_menu_item(_("Menu::Item::save party") + "…", "document-save");
    end_party  = create_menu_item(_("Menu::Item::end party")       , "games-highscores");
    quit       = create_menu_item(_("Menu::Item::quit"), "application-exit");
  } // party

  { // information
    information_menu  = Gtk::manage(new Gtk::Menu());
    information       = create_menu_item(_("Menu::information"));
    card_suggestion   = create_menu_item(_("Menu::Item::card suggestion")   + "…", "games-hint");
    last_trick        = create_menu_item(_("Menu::Item::last trick")        + "…", "zoom-previous");
    game_debug        = create_menu_item(_("Menu::Item::game debug")        + "…", "games-solve");
    rules             = create_menu_item(_("Menu::Item::rules")             + "…", "preferences-other");
    party_points      = create_menu_item(_("Menu::Item::party points")      + "…", "games-highscores");
    players           = create_menu_item(_("Menu::Item::players")           + "…", "system-users");
    players_db        = create_menu_item(_("Menu::Item::players database")  + "…", "view-statistics");
  } // information

  { // announcements
    announcements = create_menu_item(_("Menu::announcements"));
  } // announcements

  { // preferences
    preferences_menu = Gtk::manage(new Gtk::Menu());
    preferences = create_menu_item(_("Menu::preferences"));

    emphasize_valid_cards
      = Gtk::manage(new Gtk::CheckMenuItem(_(::Preferences::Type::emphasize_valid_cards)));

    show_all_hands
      = Gtk::manage(new Gtk::CheckMenuItem(_(::Preferences::Type::show_all_hands)));

    cards_order = create_menu_item(_(::Preferences::Type::cards_order) + "…", "applications-games");
    preferences_preferences = create_menu_item(_("Menu::Item::preferences") + "…", "preferences-system");
  } // preferences


  { // help
    help_menu      = Gtk::manage(new Gtk::Menu());
    help           = create_menu_item(_("Menu::help"));
    help_index     = create_menu_item(_("Menu::Item::help index")    + "…", "system-help");
    license        = create_menu_item(_("Menu::Item::license")       + "…", "help-about");
    changelog      = create_menu_item(_("Menu::Item::changelog")     + "…", "help-about");
    support        = create_menu_item(_("Menu::Item::support")       + "…", "help-about");
    about          = create_menu_item(_("Menu::Item::about")         + "…", "help-about");
    debug          = create_menu_item(_("Menu::Item::debug")         + "…", "tools-report-bug");
    generate_error = create_menu_item(_("Menu::Item::generate error")+ "…", "tools-report-bug");
  } // help

  { // bug report
    bug_report = create_menu_item(_("Menu::Item::bug report"), "tools-report-bug");
  } // bug report

  append(*party);
  append(*information);
  append(*announcements);
  append(*preferences);
  append(*help);
  append(*bug_report);

  // the party menu
  party->set_submenu(*party_menu);

  party_menu->append(*new_party);
  party_menu->append(*save_party);
  party_menu->append(*end_party);
  party_menu->append(*Gtk::manage(new Gtk::SeparatorMenuItem()));
  party_menu->append(*quit);
  // the information menu
  information->set_submenu(*information_menu);
  information_menu->append(*card_suggestion);
  information_menu->append(*last_trick);
  information_menu->append(*game_debug);
  information_menu->append(*Gtk::manage(new Gtk::SeparatorMenuItem()));
  information_menu->append(*rules);
  information_menu->append(*party_points);
  information_menu->append(*Gtk::manage(new Gtk::SeparatorMenuItem()));
  information_menu->append(*players);
  information_menu->append(*players_db);
  // the preferences menu
  preferences->set_submenu(*preferences_menu);
  preferences_menu->append(*emphasize_valid_cards);
  preferences_menu->append(*show_all_hands);
  preferences_menu->append(*cards_order);
  preferences_menu->append(*preferences_preferences);
  // the help menu
  help->set_submenu(*help_menu);
  help_menu->append(*help_index);
  help_menu->append(*license);
  help_menu->append(*changelog);
  help_menu->append(*support);
  help_menu->append(*about);
#ifndef RELEASE
  help_menu->append(*Gtk::manage(new Gtk::SeparatorMenuItem()));
  help_menu->append(*debug);
  help_menu->append(*generate_error);
#endif

  card_suggestion->set_sensitive(false);
  last_trick->set_sensitive(false);
  game_debug->set_sensitive(false);
  information->set_sensitive(false);

  emphasize_valid_cards->set_active(::preferences.value(::Preferences::Type::emphasize_valid_cards));
  show_all_hands->set_active(::preferences.value(::Preferences::Type::show_all_hands));


  { // save party file chooser
    save_party_file_chooser = make_unique<Gtk::FileChooserDialogWrapper>(_("save party"),
                                                                               Gtk::FILE_CHOOSER_ACTION_SAVE,
                                                                               sigc::mem_fun(*this, &Menu::save_party_event));
    auto const dir = ::preferences.config_directory();
    save_party_file_chooser->set_current_folder(dir.string());
  } // save party file chooser

  show_all();

  Party::signal_open.connect_back(*this, &Menu::party_open, disconnector_);
} // void Menu::init()

/** set the signals
 **/
void
Menu::set_signals()
{
  { // the signals
    new_party ->signal_activate().connect(sigc::mem_fun(*main_window, &MainWindow::start_new_party_event));
    save_party->signal_activate().connect(sigc::mem_fun0(*save_party_file_chooser, &Gtk::FileChooserDialogWrapper::present));
    end_party ->signal_activate().connect(sigc::mem_fun(*main_window, &MainWindow::end_party_event));
    quit      ->signal_activate().connect(sigc::mem_fun(*main_window, &MainWindow::quit_program_event));


    card_suggestion->signal_activate().connect(sigc::bind<bool const>(sigc::mem_fun(*ui->table, &Table::show_card_suggestion), true));
    last_trick     ->signal_activate().connect(sigc::mem_fun(*ui->table, &Table::show_last_trick));
    game_debug     ->signal_activate().connect(sigc::mem_fun0(*ui->game_debug, &Gtk::Window::present));
    rules          ->signal_activate().connect(sigc::mem_fun0(*ui->party_settings->rules,   &Gtk::Window::present));
    players        ->signal_activate().connect(sigc::mem_fun0(*ui->party_settings->players, &Gtk::Window::present));
    players_db     ->signal_activate().connect(sigc::mem_fun0(*ui->players_db,              &Gtk::Window::present));


    emphasize_valid_cards  ->signal_toggled() .connect(sigc::mem_fun(*this,             &Menu::emphasize_valid_cards_set));
    show_all_hands         ->signal_toggled() .connect(sigc::mem_fun(*this,             &Menu::show_all_hands_set));
    cards_order            ->signal_activate().connect(sigc::mem_fun(*ui->preferences,  &Preferences::open_cards_order));
    preferences_preferences->signal_activate().connect(sigc::mem_fun0(*ui->preferences, &Gtk::Window::present));

    bug_report->signal_activate().connect(sigc::mem_fun(*ui->bug_report, &BugReport::create_report));


    help_index    ->signal_activate().connect(sigc::mem_fun0(*ui->help,      &Help::show_manual));
    license       ->signal_activate().connect(sigc::mem_fun0(*ui->license,   &Gtk::Window::present));
    changelog     ->signal_activate().connect(sigc::mem_fun0(*ui->changelog, &Gtk::Window::present));
    support       ->signal_activate().connect(sigc::mem_fun0(*ui->support,   &Gtk::Window::present));
    about         ->signal_activate().connect(sigc::mem_fun0(*ui->about,     &Gtk::Window::present));
    generate_error->signal_activate().connect(sigc::mem_fun(*main_window,    &MainWindow::generate_error_event));
  } // the signals

  { // keys

    // I have here some notes, whenever I differ from the HIG ('GNOME Human Interface Guidelines (1.0)', see http://developer.gnome.org/projects/gup/hig/)

    new_party      ->add_accelerator("activate",
                                     main_window->get_accel_group(),
                                     GDK_KEY_n,
                                     Gdk::CONTROL_MASK,
                                     Gtk::ACCEL_VISIBLE);
    save_party     ->add_accelerator("activate",
                                     main_window->get_accel_group(),
                                     GDK_KEY_s,
                                     Gdk::CONTROL_MASK,
                                     Gtk::ACCEL_VISIBLE);
    quit           ->add_accelerator("activate",
                                     main_window->get_accel_group(),
                                     GDK_KEY_q,
                                     Gdk::CONTROL_MASK,
                                     Gtk::ACCEL_VISIBLE);
    card_suggestion->add_accelerator("activate",
                                     main_window->get_accel_group(),
                                     GDK_KEY_h,
                                     Gdk::ModifierType(0),
                                     Gtk::ACCEL_VISIBLE);
    last_trick     ->add_accelerator("activate",
                                     main_window->get_accel_group(),
                                     GDK_KEY_l,
                                     Gdk::ModifierType(0),
                                     Gtk::ACCEL_VISIBLE);
    rules          ->add_accelerator("activate",
                                     main_window->get_accel_group(),
                                     GDK_KEY_r,
                                     Gdk::ModifierType(0),
                                     Gtk::ACCEL_VISIBLE);
    game_debug     ->add_accelerator("activate",
                                     main_window->get_accel_group(),
                                     GDK_KEY_i,
                                     Gdk::ModifierType(0),
                                     Gtk::ACCEL_VISIBLE);
    party_points   ->add_accelerator("activate",
                                     main_window->get_accel_group(),
                                     GDK_KEY_p,
                                     Gdk::ModifierType(0),
                                     Gtk::ACCEL_VISIBLE);
    players        ->add_accelerator("activate",
                                     main_window->get_accel_group(),
                                     GDK_KEY_P,
                                     Gdk::SHIFT_MASK,
                                     Gtk::ACCEL_VISIBLE);
    // HIG: C-a should select all
    show_all_hands ->add_accelerator("activate",
                                     main_window->get_accel_group(),
                                     GDK_KEY_a,
                                     Gdk::CONTROL_MASK,
                                     Gtk::ACCEL_VISIBLE);
    // should toggle valid cards
    emphasize_valid_cards->add_accelerator("activate",
                                           main_window->get_accel_group(),
                                           GDK_KEY_period,
                                           Gdk::ModifierType(0),
                                           Gtk::ACCEL_VISIBLE);
    // HIG: C-o should open a new document / party
    cards_order          ->add_accelerator("activate",
                                           main_window->get_accel_group(),
                                           GDK_KEY_o,
                                           Gdk::CONTROL_MASK,
                                           Gtk::ACCEL_VISIBLE);

    // Note:
    // The HIG uses F2 for 'Rename' (p.e. a filename in the file manager).
    // Since I did not find some shortcut for preferences I use F2 because
    // it is next to F1 for help.
    // Later I will use S-F2 for context sensitive preferences
    // as S-F1 for context sensitive help.
    preferences_preferences->add_accelerator("activate",
                                             main_window->get_accel_group(),
                                             GDK_KEY_F2,
                                             Gdk::ModifierType(0),
                                             Gtk::ACCEL_VISIBLE);
    help_index             ->add_accelerator("activate",
                                             main_window->get_accel_group(),
                                             GDK_KEY_F1,
                                             Gdk::ModifierType(0),
                                             Gtk::ACCEL_VISIBLE);
#if 0
    // any suggestions?
    // Ctrl-l is used for loading a party.
    // Further I do not think that Ctrl-Shift-something is good usability (which program (but emacs) does use such shortcuts?).
    license->add_accelerator("activate",
                             main_window->get_accel_group(),
                             GDK_KEY_l,
                             Gdk::CONTROL_MASK,
                             Gtk::ACCEL_VISIBLE);
#endif
  } // keys
} // void Menu::set_signals()

/** sets the setting 'emphasize valid cards'
 **/
void
Menu::emphasize_valid_cards_set()
{
  ::preferences.set(::Preferences::Type::emphasize_valid_cards,
                    emphasize_valid_cards->get_active());
} // void Menu::emphasize_valid_cards_set()

/** sets the setting 'show all hands'
 **/
void
Menu::show_all_hands_set()
{
  ::preferences.set(::Preferences::Type::show_all_hands,
                    show_all_hands->get_active());
} // void Menu::show_all_hands_set()

/** event: make the announcement 'announcement' for player 'playerno'
 **
 ** @param    playerno   the number of the player who announces
 ** @param    announcement   the announcement to make
 **/
void
Menu::announcement_event(unsigned const playerno,
                         Announcement const announcement)
{
  auto& game = ui->game();
  // note, that here it is not the playernumber in the game but in the party
  auto const& player = game.player(playerno);
  game.announcements().make_announcement(announcement, player);
  if (   announcement == Announcement::no120
      && player.announcement() == Announcement::noannouncement) {
    game.announcements().make_announcement(Announcement::reply, player);
  }
} // void Menu::announcement_event(unsigned playerno, int announcement)

/** event: announce 'swines' for player 'playerno'
 **
 ** @param    playerno   the number of the player who announces swines
 **   if it is 'UINT_MAX' the player is determined
 **   automatically
 **/
void
Menu::swines_announcement_event(unsigned const playerno)
{
  auto& game = ui->game();
  auto& swines = game.swines();

  // note, that here it is not the playernumber in the game but in the party
  if (playerno == UINT_MAX) {
    for (auto& p : game.players()) {
      if (swines.swines_announcement_valid(p)) {
        swines.swines_announce(p);
        break;
      }
    } // for (p : ui->game().players())
  } else { // if !(playerno == UINT_MAX)
    swines.swines_announce(game.player(playerno));
  } // if !(playerno == UINT_MAX)
} // void Menu::swines_announcement_event(unsigned playerno)

/** event: announce 'hyperswines' for player 'playerno'
 **
 ** @param    playerno   the number of the player with hyperswines
 **   if it is 'UINT_MAX' the player is determined
 **   automatically
 **/
void
Menu::hyperswines_announcement_event(unsigned const playerno)
{
  auto& game = ui->game();
  auto& swines = game.swines();

  // note, that here it is not the playernumber in the game but in the party
  if (playerno == UINT_MAX) {
    for (auto& p : game.players()) {
      if (swines.hyperswines_announcement_valid(p)) {
        swines.hyperswines_announce(p);
        break;
      }
    } // for (p : ui->game().players())
  } else { // if !(playerno == UINT_MAX)
    swines.hyperswines_announce(ui->game().player(playerno));
  } // if !(playerno == UINT_MAX)
} // void Menu::hyperswines_announcement_event(unsigned playerno)

/** the party is opened
 ** some sensitivity is set
 **
 ** @param    party   party
 **/
void
Menu::party_open(Party& party)
{
  party_points_present_connection
    = party_points->signal_activate().connect(sigc::mem_fun0(*ui->table->party_points_,
                                                             &Gtk::Window::present));

  switch (party.status()) {

  case Party::Status::init:
    party_points->set_sensitive(false);
    end_party->set_sensitive(false);
    break;

  case Party::Status::initial_loaded:
  case Party::Status::loaded:
    party_points->set_sensitive(true);
    end_party->set_sensitive(true);
    break;

  default:
    DEBUG_ASSERTION(false,
                    "Menu::party_open():\n"
                    "  party status = '" << party.status() << "'");
  } // switch (party.status())

  information      ->set_sensitive(true);
  rules            ->set_sensitive(true);
  players          ->set_sensitive(true);
  players_db       ->set_sensitive(true);

  card_suggestion  ->set_sensitive(false);
  last_trick       ->set_sensitive(false);
  game_debug       ->set_sensitive(false);
  announcements    ->set_sensitive(false);
  announcements    ->unset_submenu();

  party.signal_start .connect_back(*this, &Menu::party_start,  disconnector_);
  party.signal_finish.connect_back(*this, &Menu::party_finish, disconnector_);
  party.signal_close .connect_back(*this, &Menu::party_close,  disconnector_);
} // void Menu::party_open(Party& party)

/** the party has started
 ** create the announcements menu
 ** make some information accessable
 **/
void
Menu::party_start()
{
  end_party->set_sensitive(true);

  party_points->set_sensitive(true);

  { // create the announcements menu
    announcements_player.clear();
    announcements_player_menu.clear();
    announcements_announcement.clear();
    swines_separator.clear();
    swines_announcement.clear();
    hyperswines_announcement.clear();

    Gtk::Menu* menu = nullptr;
    menu  = Gtk::manage(new Gtk::Menu());
    for (auto const& p : ui->party().players()) {
      announcements_player.push_back(Gtk::manage(new Gtk::MenuItem(p.name())));

      announcements_player_menu.push_back(Gtk::manage(new Gtk::Menu()));
      announcements_announcement.emplace_back(vector<Gtk::MenuItem*>());

      announcements_announcement.back().push_back(Gtk::manage(new Gtk::MenuItem(_(Announcement::reply))));
      for (auto const a : real_announcement_list) {
        announcements_announcement.back().push_back(Gtk::manage(new Gtk::MenuItem(_(a))));
        announcements_player_menu.back()->append(*announcements_announcement.back().back());

        announcements_announcement.back().back()->signal_activate().connect(sigc::bind<unsigned, Announcement>(sigc::mem_fun(*this, &Menu::announcement_event), ui->party().players().no(p), a));

      } // for (a : real_announcement_list)

      { // swines separator
        swines_separator.push_back(Gtk::manage(new Gtk::SeparatorMenuItem()));
        announcements_player_menu.back()->append(*swines_separator.back());
      } // swines separator
      { // swines
        swines_announcement.push_back(Gtk::manage(new Gtk::MenuItem(_("Menu::Item::swines"))));
        announcements_player_menu.back()->append(*swines_announcement.back());

        swines_announcement.back()->signal_activate().connect(sigc::bind<unsigned const>(sigc::mem_fun(*this, &Menu::swines_announcement_event), ui->party().players().no(p)));
      } // swines
      { // hyperswines
        hyperswines_announcement.push_back(Gtk::manage(new Gtk::MenuItem(_("Menu::Item::hyperswines"))));
        announcements_player_menu.back()->append(*hyperswines_announcement.back());

        hyperswines_announcement.back()->signal_activate().connect(sigc::bind<unsigned const>(sigc::mem_fun(*this, &Menu::hyperswines_announcement_event), ui->party().players().no(p)));
      } // hyperswines

      announcements_player_menu.back()->show_all_children();
      if ((ui->party().players().count_humans() == 1)
          && (p.type() == Player::Type::human))
        menu = announcements_player_menu.back();
      else
        announcements_player.back()->set_submenu(*announcements_player_menu.back());

      if ((ui->party().players().count_humans() != 1)
          && (p.type() == Player::Type::human))
        menu->append(*announcements_player.back());
    } // for (p < ui->party().playerno())
    announcements->set_submenu(*menu);
    menu->show_all_children();
  } // create the announcements menu
} // void Menu::party_start()

/** the party is finished
 **/
void
Menu::party_finish()
{
  end_party->set_sensitive(false);
} // void Menu::party_finish()

/** the party has closed
 **
 ** @todo   better signal disconnection
 **/
void
Menu::party_close()
{
  // ToDo: disconnection without extra variables
  party_points_present_connection.disconnect();

  party_points->set_sensitive(false);
  information->set_sensitive(false);
  last_trick->set_sensitive(false);
  game_debug->set_sensitive(false);
} // void Menu::party_close()

/** the game has started
 ** make the announcements menu accessable
 **/
void
Menu::game_start()
{
  announcements->set_sensitive(ui->game().players().count_humans() > 0);
  game_debug->set_sensitive(true);

  // update the announcements
  // to have 're'/'contra' instead of 'no 120'
  if (ui->game().status() >= Game::Status::init)
    for (unsigned p = 0; p < ui->game().playerno(); ++p) {
      for (auto const a : real_announcement_list) {
        // if the team is 're' or 'contra'
        // take 're'/'contra' instead of 'no 120'
        if (   a == Announcement::no120
            && !ui->game().rule(Rule::Type::knocking)
            && is_real(ui->game().player(p).team())) {
          dynamic_cast<Gtk::Label*>(announcements_announcement[p][static_cast<int>(a)]->get_child()
                                   )->set_text(_(ui->game().player(p).team()));
        } else if (a == Announcement::reply) {
          dynamic_cast<Gtk::Label*>(announcements_announcement[p][static_cast<int>(a)]->get_child()
                                   )->set_text(_(ui->game().player(p).team()));
        } else {
          dynamic_cast<Gtk::Label*>(announcements_announcement[p][static_cast<int>(a)]->get_child()
                                   )->set_text(_(Announcement(a)));
        }
      }
    }
} // void Menu::game_start()

/** a new trick is opened
 **/
void
Menu::trick_open()
{
  last_trick->set_sensitive(ui->game().tricks().real_current_no() >= 1);
  announcements_update();
} // void Menu::trick_open()

/** the (human) player has to play a card
 **/
void
Menu::card_get()
{
  card_suggestion->set_sensitive(true);
} // void Menu::card_get()

/** the (human) player has played a card
 **/
void
Menu::card_got()
{
  card_suggestion->set_sensitive(false);
} // void Menu::card_got()

/** the game is finished
 **/
void
Menu::game_finish()
{
  last_trick->set_sensitive(false);
} // void Menu::game_finish()

/** the game is closed
 **/
void
Menu::game_close()
{
  game_debug->set_sensitive(false);
} // void Menu::game_close()

/** updates the announcements, that is the sensitivity of them
 **/
void
Menu::announcements_update()
{
  auto const& game = ui->game();
  bool one_valid = false;
  for (unsigned p = 0; p < game.playerno(); p++) {
    auto const& player = game.player(p);
    bool valid = false;

    if (   !game.rule(Rule::Type::knocking)
        && is_real(player.team())) {
      dynamic_cast<Gtk::Label*>(announcements_announcement[p][static_cast<int>(Announcement::no120)]->get_child())->set_text(_(player.team()));
    }
    for (auto const a : real_announcement_list)  {
      auto const v = game.announcements().is_valid(a, player);
      announcements_announcement[p][static_cast<int>(a)]->set_sensitive(v);
      valid |= v;
    } // for (a \in Announcement)
    if (game.announcements().is_valid(Announcement::reply, player)) {
      valid |= true;
      announcements_announcement[p][static_cast<int>(Announcement::no120)]->set_sensitive(true);
    }
    { // swines
      valid |= (   game.swines().swines_announcement_valid(player)
                || game.swines().hyperswines_announcement_valid(player));
      if (game.rule(Rule::Type::swines)) {
        swines_separator[p]->show();
        swines_announcement[p]->show();
      } else {
        swines_separator[p]->hide();
        swines_announcement[p]->hide();
      }
      if (game.rule(Rule::Type::hyperswines))
        hyperswines_announcement[p]->show();
      else
        hyperswines_announcement[p]->hide();

      swines_announcement[p]->set_sensitive(game.swines().swines_announcement_valid(player));
      hyperswines_announcement[p]->set_sensitive(game.swines().hyperswines_announcement_valid(player));
    } // swines

    announcements_player[p]->set_sensitive(valid);
    valid &= ((player.type() == Player::Type::human));
    one_valid |= valid;
  } // for (p < game.playerno())
  announcements->set_sensitive(one_valid);
} // void Menu::announcements_update()

/** saves the party
 ** The filename is taken from 'save_party_file_chooser'.
 **
 ** @param    filename   file to save
 **
 ** @return   whether the saving was successfull
 **/
bool
Menu::save_party_event(string const filename)
{
  if (!ui->party().save(filename)) {
    ui->information(_("save party failed: %s",
                      filename),
                    InformationType::problem);
    return false;
  }
  return true;
} // bool Menu::save_party_event(string filename)


auto Menu::create_menu_item(string const& text, string const& icon_name) -> Gtk::MenuItem*
{
  //return Gtk::manage(new Gtk::MenuItem(_(text)));
  // Gtk unterstützt nicht mehr Bilder im Menü :-(

  if (icon_name.empty()) {
    return Gtk::manage(new Gtk::MenuItem(_(text)));
  } else {
    auto const image = ui->create_theme_icon_image(icon_name, 16);
    if (!image)
      return Gtk::manage(new Gtk::MenuItem(_(text)));
    return Gtk::manage(new Gtk::ImageMenuItem(*image, _(text)));
  }
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
