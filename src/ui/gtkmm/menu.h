/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"

#include "../../basetypes/announcement.h"

class Party;

#include "widgets/file_chooser_dialog_wrapper.h"
#include <gtkmm/menubar.h>
#include <sigc++/connection.h>
namespace Gtk {
class Menu;
class MenuItem;
class CheckMenuItem;
class Label;
} // namespace Gtk

namespace UI_GTKMM_NS {
class MainWindow;
class Preferences;

/**
 ** the menu
 **
 ** party
 **   +- new party              C-n
 **   +- save party             C-s
 **   +- end party
 **   `- quit                   C-q
 ** information
 **   +- card suggestion        h
 **   +- last trick             l
 **   +- rules                  r
 **   +- players                P
 **   `- party points           p
 ** announcement
 **   `-  ...                   n        m
 ** preferences
 **   +- show all hands         C-a
 **   +- show valid cards       .
 **   +- cards order            C-o
 **   `- preferences            F2
 ** bug report
 ** help
 **   +- help index             F1
 **   +- license
 **   +- cardset license
 **   +- changelog
 **   `- about
 ** 
 ** Note: the translation of the announcements are managed internally
 ** 
 **
 ** @todo   replace 'party_points_*_connection' (see sigc-doc)
 ** @todo   update the player names in the announcement
 **/
class Menu : public Base, public Gtk::MenuBar {
  friend class Preferences; // for changing the preferences menu items
  friend class UI_GTKMM; // for shortcuts

  public:
  explicit Menu(MainWindow* main_window);
  ~Menu() override;

  Menu() = delete;
  Menu(Menu const&) = delete;
  Menu& operator=(Menu const&) = delete;

  void set_signals();
  void show_all_hands_set();
  void emphasize_valid_cards_set();

  void party_open(Party& party);
  void party_start();
  void game_start();
  void trick_open();
  void card_get();
  void card_got();
  void game_finish();
  void game_close();
  void party_finish();
  void party_close();

  void announcements_update();
  private:
  void init();

  public:
  void announcement_event(unsigned playerno, Announcement announcement);
  void swines_announcement_event(unsigned playerno);
  void hyperswines_announcement_event(unsigned playerno);

  private:
  bool save_party_event(string filename);

  auto create_menu_item(string const& text, string const& icon_name = "") -> Gtk::MenuItem*;

  private:
  AutoDisconnector disconnector_;

  MainWindow* main_window = nullptr;

  Gtk::Menu* party_menu = nullptr;
  Gtk::MenuItem* party = nullptr;
  Gtk::MenuItem* new_party = nullptr;
  Gtk::MenuItem* save_party = nullptr;
  Gtk::MenuItem* end_party = nullptr;
  Gtk::MenuItem* quit = nullptr;

  Gtk::Menu* information_menu = nullptr;
  Gtk::MenuItem* information = nullptr;
  Gtk::MenuItem* card_suggestion = nullptr;
  Gtk::MenuItem* last_trick = nullptr;
  Gtk::MenuItem* game_debug = nullptr;
  Gtk::MenuItem* bug_report_replay = nullptr;
  Gtk::MenuItem* rules = nullptr;
  Gtk::MenuItem* party_points = nullptr;
  Gtk::MenuItem* players = nullptr;
  Gtk::MenuItem* players_db = nullptr;

  Gtk::MenuItem* announcements = nullptr;
  vector<Gtk::MenuItem*> announcements_player;
  vector<Gtk::Menu*> announcements_player_menu;
  vector<vector<Gtk::MenuItem*> > announcements_announcement;
  vector<Gtk::MenuItem*> swines_separator;
  vector<Gtk::MenuItem*> swines_announcement;
  vector<Gtk::MenuItem*> hyperswines_announcement;

  Gtk::Menu* preferences_menu = nullptr;
  Gtk::MenuItem* preferences = nullptr;
  protected:
  Gtk::CheckMenuItem* emphasize_valid_cards = nullptr;
  Gtk::CheckMenuItem* show_all_hands = nullptr;
  private:
  Gtk::MenuItem* cards_order = nullptr;
  Gtk::MenuItem* preferences_preferences = nullptr;

  Gtk::MenuItem* bug_report = nullptr;

  Gtk::Menu* help_menu = nullptr;
  Gtk::MenuItem* help = nullptr;
  Gtk::MenuItem* help_index = nullptr;
  Gtk::MenuItem* license = nullptr;
  Gtk::MenuItem* changelog = nullptr;
  Gtk::MenuItem* support = nullptr;
  Gtk::MenuItem* about = nullptr;
  Gtk::MenuItem* debug = nullptr;
  Gtk::MenuItem* generate_error = nullptr;

  sigc::connection party_points_present_connection;

  unique_ptr<Gtk::FileChooserDialogWrapper> load_party_file_chooser;
  unique_ptr<Gtk::FileChooserDialogWrapper> save_party_file_chooser;
}; // class Menu : public Base, public Gtk::MenuBar

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
