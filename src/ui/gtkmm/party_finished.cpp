/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "party_finished.h"

#include "ui.h"
#include "table.h"
#include "main_window.h"
#include "party_points.h"
#include "cards.h"

#include "../../basetypes/fast_play.h"
#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../misc/preferences.h"

#include "../../utils/string.h"

#include <gtkmm/main.h>
#include <gtkmm/drawingarea.h>

namespace UI_GTKMM_NS {

PartyFinished::PartyFinished(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::Party finished"),
               *parent->ui->main_window, false)
{
  ui->add_window(*this);

  { // set size
    Gdk::Geometry geometry;
    geometry.min_width = 6 * ui->cards->width();
    geometry.min_height = 3 * ui->cards->height();

    set_geometry_hints(*this, geometry, Gdk::HINT_MIN_SIZE);
  }

  signal_realize().connect(sigc::mem_fun(*this, &PartyFinished::init));
#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_show().connect(sigc::mem_fun(*this, &PartyFinished::on_show));
  signal_hide().connect(sigc::mem_fun(*this, &PartyFinished::on_hide));
#endif
}


PartyFinished::~PartyFinished() = default;


void PartyFinished::init()
{
  set_icon(ui->icon);

  set_position(Gtk::WIN_POS_CENTER_ON_PARENT);

  get_content_area()->set_spacing(1 EX);

  points_graph = Gtk::manage(new Gtk::DrawingArea());
  get_content_area()->pack_start(*points_graph, true, true);
  {
    auto show_party_points = Gtk::manage(new Gtk::Button(_("Button::show party points")));
    get_content_area()->pack_start(*show_party_points, false, true);
    show_party_points->signal_clicked().connect(sigc::mem_fun0(ui->table->party_points(), &Gtk::Window::present));
  }

  add_close_button(*this);

  show_all_children();

  // signals
  points_graph->signal_draw().connect(sigc::mem_fun(*this, &PartyFinished::on_draw_graph));
}


void PartyFinished::party_finished()
{
  if (::fast_play & FastPlay::party_finished)
    return ;

  present();

  while (!ui->thrower
         && is_visible())
    ::ui->wait();
  if (ui->thrower)
    return ;

  hide();
}


void PartyFinished::redraw_points_graph()
{
  /*
   *     Mara    Gerd    Erika   Sven
   *                           _________
   *   _________               |       |
   *   |       |________       |       |
   *   |       |       |       |       |
   *   |  75   |  50   |  -50  |  100  |
   *                   |       |       |
   *                   |_______|
   */

  if (!get_realized())
    return ;

  auto cr = points_graph->get_window()->create_cairo_context();

  auto const width = points_graph->get_width();
  auto const height = points_graph->get_height();

  cr->set_source_rgb(1, 1, 1);
  cr->rectangle(0, 0, width, height);
  cr->fill();

  int min_points = INT_MAX; // minimal points made by the players
  int max_points = INT_MIN; // maximal points made by the players

  Party const& party = ui->party();

  for (auto const& p : party.players()) {
    int const points = party.pointsum(p);
    if (points < min_points)
      min_points = points;
    if (points > max_points)
      max_points = points;
  } // for (p : party.players())
  if (max_points == min_points)
    max_points += 1;

  int max_layout_height = 0;
  {
    auto name_layout = create_pango_layout("");
    name_layout->set_font_description(Pango::FontDescription("Sans Bold 10"));
    for (auto const& p : party.players()) {
      name_layout->set_text(p.name());
      int layout_width = 0, layout_height = 0;
      name_layout->get_pixel_size(layout_width, layout_height);
      if (layout_height > max_layout_height)
        max_layout_height = layout_height;
    } // for (p : party.players())
  }

  int const points_difference = (max(max_points, 0)
                                 - min(min_points, 0));
  int const border_left = ui->cards->width() / 5;
  int const border_right = border_left;
  int const border_top = (2 * max_layout_height
                          + ((max_points >= 0)
                             ? max_layout_height
                             : 0));
  int const border_bottom = (max_layout_height / 2
                             + ((min_points < 0)
                                ? max_layout_height
                                : 0));
  int const total_width = width - border_left - border_right;
  int const total_height = height - border_top - border_bottom;

  int const baseline = (border_top
                        + (max(max_points, 0)
                           * total_height
                           / points_difference));
  int const single_width = total_width / party.players().size();
  int const distance = 4; // for the distance between the columns
  { // draw all
    Gdk::RGBA color_name(::preferences(::Preferences::Type::name_font_color));
    Gdk::RGBA color_winner_name(::preferences(::Preferences::Type::name_active_font_color));
    for (unsigned p = 0; p < party.players().size(); ++p) {
      Player const& player = party.players()[p];
      bool winner = (party.rang(player) == 0);
      int const points = party.pointsum(player);
      int const pos_x = border_left + p * single_width;
      int height = (points * total_height / points_difference);
      (void) height;
      (void) distance;
      (void) baseline;

      if (winner)
        cr->set_source_rgb(color_winner_name.get_red(), color_winner_name.get_green(), color_winner_name.get_blue());
      else
        cr->set_source_rgb(color_name.get_red(), color_name.get_green(), color_name.get_blue());
      { // draw name
        auto name_layout = create_pango_layout("");
        name_layout->set_font_description(Pango::FontDescription("Sans Bold 10"));
        name_layout->set_text(player.name());
        int layout_width = 0, layout_height = 0;
        name_layout->get_pixel_size(layout_width, layout_height);
        // bug: there seems to be a problem with italic characters
        layout_width += 2 * layout_height / 5; // NOLINT(bugprone-integer-division)
        cr->move_to(pos_x + (single_width - layout_width) / 2, // NOLINT(bugprone-integer-division)
                    max_layout_height / 2); // NOLINT(bugprone-integer-division)
        name_layout->show_in_cairo_context(cr);
      } // draw name
      { // draw points
        auto points_layout = create_pango_layout("");
        points_layout->set_font_description(Pango::FontDescription("Sans 10"));
        points_layout->set_text(std::to_string(points));
        int layout_width = 0, layout_height = 0;
        points_layout->get_pixel_size(layout_width, layout_height);
        // bug: there seems to be a problem with italic characters
        layout_width += 2 * layout_height / 5; // NOLINT(bugprone-integer-division)

        cr->move_to(pos_x + (single_width - layout_width) / 2, // NOLINT(bugprone-integer-division)
                    baseline - height
                    - ((height >= 0)
                       ? layout_height
                       : 0));
        points_layout->show_in_cairo_context(cr);
      } // draw points
      { // draw point column
        cr->rectangle(pos_x + single_width / distance, // NOLINT(bugprone-integer-division)
                      baseline,
                      single_width - 2 * single_width / distance, // NOLINT(bugprone-integer-division)
                      1);
        cr->fill();
        if (height >= 0)
          cr->rectangle(pos_x + single_width / distance, // NOLINT(bugprone-integer-division)
                        baseline - height,
                        single_width - 2 * single_width / distance, // NOLINT(bugprone-integer-division)
                        height);
        else
          cr->rectangle(pos_x + single_width / distance, // NOLINT(bugprone-integer-division)
                        baseline,
                        single_width - 2 * single_width / distance, // NOLINT(bugprone-integer-division)
                        -height);
        cr->fill();
      } // draw point column
    } // for (p)

    if (party.rule()(Rule::Type::counting) == Counting::plusminus) {
      // draw baseline
      cr->set_source_rgb(color_name.get_red(), color_name.get_green(), color_name.get_blue());
      cr->rectangle(border_left + single_width / distance, // NOLINT(bugprone-integer-division)
                    baseline,
                    total_width - 2 * (single_width / distance), // NOLINT(bugprone-integer-division)
                    1);
      cr->fill();
    } // if (draw baseline)
  } // draw all
}


void PartyFinished::name_changed(::Player const& player)
{
  if (!get_realized())
    return ;

  redraw_points_graph();
}


void PartyFinished::on_hide()
{
  ui->main_quit();
#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  Gtk::StickyDialog::on_hide();
#endif
}


auto PartyFinished::on_draw_graph(::Cairo::RefPtr<::Cairo::Context> const& cr) -> bool
{
  redraw_points_graph();

  return true;
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
