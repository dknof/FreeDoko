/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"

#include "party_points.h"

#include <gtkmm/drawingarea.h>

namespace UI_GTKMM_NS {
/**
 ** a curve showing the points of the players
 **/
class PartyPoints::Graph : public Base, public Gtk::DrawingArea {
public:
  explicit Graph(PartyPoints* party_points);
  Graph() = delete;
  Graph(Graph const&) = delete;
  Graph& operator=(Graph const&) = delete;
  ~Graph() override;

  void draw_all();

private:
  auto on_draw(::Cairo::RefPtr<::Cairo::Context> const& cr) -> bool override;
};

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
