/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"
#include "widgets/sticky_dialog.h"


class Party;
class GameSummary;

#include <gtkmm/treestore.h>
namespace Gtk {
class Notebook;
class TreeView;
class CellRenderer;
class CheckButton;
class RadioButton;
} // namespace Gtk
class Player;

namespace UI_GTKMM_NS {

/**
 ** the party points window
 **
 ** Note: the first row in each round
 ** either shows the current game (expanded round)
 ** or shows the point sum of the round (collapsed)
 ** The bottom row (sum) is a second table.
 **
 ** @todo   bold number for the startplayer
 ** @todo   lines between the columns
 **/
class PartyPoints : public Base, public Gtk::StickyDialog {
  class GameOverview;
  friend class GameOverview;
  class Graph;

  struct PartyPointsModel : public Gtk::TreeModel::ColumnRecord {
      PartyPointsModel(unsigned playerno);

      Gtk::TreeModelColumn<Glib::ustring> empty;
      Gtk::TreeModelColumn<unsigned> round;
      Gtk::TreeModelColumn<Glib::ustring> round_str;
      Gtk::TreeModelColumn<Glib::ustring> round_color;
      Gtk::TreeModelColumn<unsigned> gameno;
      Gtk::TreeModelColumn<Glib::ustring> gameno_str;
      vector<Gtk::TreeModelColumn<Glib::ustring> > playerpoints;
      Gtk::TreeModelColumn<int> gamepoints;
      Gtk::TreeModelColumn<Glib::ustring> gamepoints_str;
      Gtk::TreeModelColumn<int> bock_multiplier;
      Gtk::TreeModelColumn<Glib::ustring> bock_multiplier_str;
#ifdef POSTPONED
      Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf> > gametype;
#else
      Gtk::TreeModelColumn<Glib::ustring> gametype;
#endif
  }; // struct PartyPointsModel : public Gtk::TreeModel::ColumnRecord

  public:
  explicit PartyPoints(Base* parent);
  ~PartyPoints() override;

  PartyPoints() = delete;
  PartyPoints(PartyPoints const&) = delete;
  PartyPoints& operator=(PartyPoints const&) = delete;

  auto party() -> Party&;
  void party_open(Party& party);
  void party_start_round(unsigned round);
  void party_finish();
  void party_close();
  void game_start();
  void game_finished();

  void name_changed(Player const& player);
  void update();

  protected:
  void clear_selection();

  private:
  void init();
  void on_show() override;
  public:
  void recalc_all();
  void recreate_all();
  void recreate_all_preprocessing();
  void lazy_recreate_all_step();
  void recreate_all_postprocessing();
  void player_added(Player const& player);
  private:
  void add_game(unsigned gameno, bool update = true);
  void set_points(unsigned gameno);
  void update_round(unsigned round);
  void update_sum();
  void update_duty_soli();
  void update_player_columns_size();

  void add_future_bock_multipliers();
  void add_row(unsigned gameno);
  void row_selection_changed();
  void sum_columns_size_update();
  void show_game_overview();
  void collaps_setting_changed();

  void row_collapsed_or_expanded(Gtk::TreeModel::iterator const& iter,
                                 Gtk::TreeModel::Path const& path);
  void set_cell_color(Gtk::CellRenderer* cell_renderer,
                      Gtk::TreeModel::iterator const& iter,
                      unsigned column);
  static auto color(::GameSummary const& game_summary) -> string;

  bool on_key_press_event(GdkEventKey* key) override;

  private: // elements
  AutoDisconnector disconnector_;
  Party* party_ = nullptr;

  public:
  Gtk::Notebook* notebook = nullptr;
  private:
  bool lazy_recreate_all_ = false;
  //TrickpointsModel party_points_model;
  PartyPointsModel party_points_model;
  Glib::RefPtr<Gtk::TreeStore> party_points_list;
  Gtk::TreeView* party_points_treeview = nullptr;
  vector<Gtk::TreeModel::Row> round_rows;
  vector<Gtk::TreeModel::Row> game_rows;

  Gtk::TreeView* party_points_sum_treeview = nullptr;

  Glib::RefPtr<Gtk::TreeStore> duty_soli_list;
  Gtk::TreeModel::Row sum_row;
  Gtk::TreeModel::Row duty_free_soli_row;
  Gtk::TreeModel::Row duty_color_soli_row;
  Gtk::TreeModel::Row duty_picture_soli_row;
  Gtk::TreeView* duty_soli_treeview = nullptr;

  protected:
  Gtk::Button* show_game_overview_button = nullptr;
  private:
  Gtk::CheckButton* add_up_points = nullptr;

  Gtk::RadioButton* collapse_rounds = nullptr;
  Gtk::RadioButton* expand_rounds   = nullptr;

  std::unique_ptr<GameOverview> game_overview;
  Graph* graph = nullptr;

  Gtk::Label* remaining_rounds_label = nullptr;
  Gtk::Label* remaining_points_label = nullptr;
};

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
