/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "party_settings.h"
#include "players.h"
#include "rules.h"

#include "ui.h"
#include "main_window.h"
#include "bug_report.h"
#include "first_run.h"
#include "program_updated.h"

#include "../../basetypes/fast_play.h"
#include "../../party/party.h"
#include "../../player/player.h"
#include "../../player/aiconfig.h"
#include "../../os/bug_report_replay.h"
#include "../../misc/preferences.h"
#include "../../utils/random.h"

#include <gtkmm/radiobutton.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/frame.h>
#include <gtkmm/main.h>
#include <gtkmm/grid.h>
#include <gtkmm/filechooserdialog.h>
namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 **/
PartySettings::PartySettings(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::party settings"),
               *parent->ui->main_window, false)
{
  ui->add_window(*this);

  init();

  signal_show().connect(sigc::mem_fun(*this, &PartySettings::update));
  signal_key_press_event().connect(sigc::mem_fun(*this,
                                                       &PartySettings::key_press));

  ui->bug_report->set_dnd_destination(*this);
} // PartySettings::PartySettings(Base* parent)

/** destruktor
 **/
PartySettings::~PartySettings() = default;

/** create all subelements
 **/
void
PartySettings::init()
{
#ifdef POSTPONED
  auto& party = ui->party();
#else
  auto& party = *::party;
#endif

  set_icon(ui->icon);

  set_position(Gtk::WIN_POS_CENTER_ON_PARENT);

  players = make_unique<Players>(this);
  rules = make_unique<Rules>(this);

  load_bug_report_button
    = Gtk::manage(new Gtk::Button(_("Button::load bug report")));
  load_bug_report_button->set_image_from_icon_name("document-open");
  load_bug_report_button->set_always_show_image();
#ifndef RELEASE
  add_action_widget(*load_bug_report_button,
                          Gtk::RESPONSE_NONE);
#endif
  start_party_button
    = Gtk::manage(new Gtk::Button(_("Button::start party")));
  start_party_button->set_image_from_icon_name("arrow-right");
  add_action_widget(*start_party_button, Gtk::RESPONSE_CLOSE);
  start_party_button->set_can_default();

  close_button = add_close_button(*this);

  { // seed
    seed_value = Gtk::manage(new Gtk::SpinButton(1, 0));
    seed_value->set_value(0);
    seed_value->set_range(0, Random::max_seed());
    seed_value->set_increments(1, 1000);
    seed_random = Gtk::manage(new Gtk::CheckButton(_("Button::random")));
  } // seed
  { // duration
    rule_number_of_rounds_limited
      = Gtk::manage(new Gtk::CheckButton(_(Rule::Type::number_of_rounds) + ":"));
    rule_number_of_rounds
      = Gtk::manage(new Gtk::SpinButton());
    rule_number_of_rounds->set_increments(1, 10);
    rule_points_limited
      = Gtk::manage(new Gtk::CheckButton(_(Rule::Type::points) + ":"));
    rule_points
      = Gtk::manage(new Gtk::SpinButton());
    rule_points->set_increments(1, 10);
  } // duration
  { // startplayer
    startplayer_random = Gtk::manage(new Gtk::RadioButton(_("Button::random")));
  } // startplayer

  configure_players = Gtk::manage(new Gtk::Button(_("Button::configure players")));
  configure_players->set_image_from_icon_name("system-users");
  configure_players->set_always_show_image();
  configure_rules = Gtk::manage(new Gtk::Button(_("Button::configure rules")));
  configure_rules->set_image_from_icon_name("preferences-other");
  configure_rules->set_always_show_image();


  { // create the startplayer buttons
    auto startplayer_group
      = startplayer_random->get_group();
    for (auto const& p : party.players()) {
      startplayer.push_back(Gtk::manage(new Gtk::RadioButton(startplayer_group, p.name())));
    }
    { // the swap buttons
#ifdef WORKAROUND
      // MS-Windows does not show the arrows
#include "arrows/arrow_lu.xpm"
#include "arrows/arrow_ur.xpm"
#include "arrows/arrow_rd.xpm"
#include "arrows/arrow_dl.xpm"
      swap_players_buttons.push_back(Gtk::manage(new Gtk::Button()));
      swap_players_buttons.back()->add(*Gtk::manage(new Gtk::Image(Gdk::Pixbuf::create_from_xpm_data(arrow_lu_xpm))));
      swap_players_buttons.push_back(Gtk::manage(new Gtk::Button()));
      swap_players_buttons.back()->add(*Gtk::manage(new Gtk::Image(Gdk::Pixbuf::create_from_xpm_data(arrow_ur_xpm))));
      swap_players_buttons.push_back(Gtk::manage(new Gtk::Button()));
      swap_players_buttons.back()->add(*Gtk::manage(new Gtk::Image(Gdk::Pixbuf::create_from_xpm_data(arrow_rd_xpm))));
      swap_players_buttons.push_back(Gtk::manage(new Gtk::Button()));
      swap_players_buttons.back()->add(*Gtk::manage(new Gtk::Image(Gdk::Pixbuf::create_from_xpm_data(arrow_dl_xpm))));

      for (auto& b : swap_players_buttons)
        b->set_relief(Gtk::RELIEF_NONE);
#else
      swap_players_buttons.push_back(Gtk::manage(new Gtk::Button(Glib::ustring(1, static_cast<gunichar>(0x2196))))); // up left
      swap_players_buttons.push_back(Gtk::manage(new Gtk::Button(Glib::ustring(1, static_cast<gunichar>(0x2197))))); // up right
      swap_players_buttons.push_back(Gtk::manage(new Gtk::Button(Glib::ustring(1, static_cast<gunichar>(0x2199))))); // down right
      swap_players_buttons.push_back(Gtk::manage(new Gtk::Button(Glib::ustring(1, static_cast<gunichar>(0x2198))))); // down left
#endif
    } // the swap buttons
  } // create the startplayer buttons
  { // Layout
    auto hbox = Gtk::manage(new Gtk::Box());
    hbox->set_spacing(1 EM);
    hbox->set_border_width(1 EM);
    get_content_area()->add(*hbox);
    { // actions
      { // configure
        auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));
        vbox->set_homogeneous();
        vbox->set_halign(Gtk::ALIGN_CENTER);
        vbox->set_valign(Gtk::ALIGN_CENTER);

        vbox->add(*configure_rules);
        vbox->add(*configure_players);
        hbox->pack_end(*vbox, false, true);
      } // configure

    } // actions

    { // settings
      auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));
      vbox->set_halign(Gtk::ALIGN_CENTER);
      vbox->set_valign(Gtk::ALIGN_CENTER);

      { // seed
        seed_frame = Gtk::manage(new Gtk::Frame(_("card distribution")));
        auto grid = Gtk::manage(new Gtk::Grid());
        grid->set_column_homogeneous(true);

        grid->set_halign(Gtk::ALIGN_CENTER);
        grid->set_border_width(2 EX);
        grid->set_row_spacing(1 EX);
        grid->set_column_spacing(1 EM);

        auto label = Gtk::manage(new Gtk::Label(_("Label::seed") + ":"));
        grid->attach(*label, 0, 0, 1, 1);
        grid->attach(*seed_random, 1, 0, 1, 1);
        grid->attach(*seed_value, 0, 1, 2, 1);
        seed_frame->add(*grid);
        vbox->add(*seed_frame);
      } // seed

      { // duration
        auto duration_frame = Gtk::manage(new Gtk::Frame(_("party duration")));

        auto grid = Gtk::manage(new Gtk::Grid());
        grid->set_halign(Gtk::ALIGN_CENTER);
        grid->set_border_width(2 EX);
        grid->set_row_spacing(1 EX);
        grid->set_column_spacing(1 EM);

        grid->attach(*rule_number_of_rounds_limited, 0, 0, 1, 1);
        grid->attach(*rule_number_of_rounds, 1, 0, 1, 1);
        grid->attach(*rule_points_limited, 0, 1, 1, 1);
        grid->attach(*rule_points, 1, 1, 1, 1);
        duration_frame->add(*grid);
        vbox->add(*duration_frame);
      } // duration

      { // startplayer
        startplayer_frame
          = Gtk::manage(new Gtk::Frame(_("startplayer")));
        vbox->add(*startplayer_frame);

        auto grid = Gtk::manage(new Gtk::Grid());
        grid->set_halign(Gtk::ALIGN_CENTER);
        grid->set_row_homogeneous(true);
        grid->set_column_homogeneous(true);
        grid->set_border_width(2 EX);
        grid->set_row_spacing(1 EX);
        grid->set_column_spacing(1 EM);

        grid->attach(*startplayer_random, 1, 1, 1, 1);
        grid->attach(*startplayer[0], 1, 2, 1, 1);
        grid->attach(*startplayer[1], 0, 1, 1, 1);
        grid->attach(*startplayer[2], 1, 0, 1, 1);
        grid->attach(*startplayer[3], 2, 1, 1, 1);
        grid->attach(*swap_players_buttons[0], 0, 2, 1, 1);
        grid->attach(*swap_players_buttons[1], 0, 0, 1, 1);
        grid->attach(*swap_players_buttons[2], 2, 0, 1, 1);
        grid->attach(*swap_players_buttons[3], 2, 2, 1, 1);
        startplayer_frame->add(*grid);
      } // startplayer
      hbox->pack_start(*vbox, true, true);
    } // settings

  } // Layout

  { // signals
    load_bug_report_button->signal_clicked().connect(sigc::mem_fun(*this,
                                                                   &PartySettings::load_bug_report)
                                                    );
    start_party_button->signal_clicked().connect(sigc::mem_fun(*this,
                                                               &PartySettings::start_party_event)
                                                );

    seed_value->signal_value_changed().connect(sigc::mem_fun(*this, &PartySettings::seed_change_event));
    seed_random->signal_toggled().connect(sigc::mem_fun(*this, &PartySettings::seed_change_event));
    seed_random->signal_toggled().connect(sigc::mem_fun(*this,
                                                        &PartySettings::seed_value_sensitivity_update)
                                         );

    rule_number_of_rounds_limited->signal_toggled().connect(sigc::bind<int const>(sigc::mem_fun(*this, &PartySettings::rule_change), Rule::Type::number_of_rounds_limited));
    rule_number_of_rounds->signal_value_changed().connect(sigc::bind<int const>(sigc::mem_fun(*this, &PartySettings::rule_change), Rule::Type::number_of_rounds));
    rule_points_limited->signal_toggled().connect(sigc::bind<int const>(sigc::mem_fun(*this, &PartySettings::rule_change), Rule::Type::points_limited));
    rule_points->signal_value_changed().connect(sigc::bind<int const>(sigc::mem_fun(*this, &PartySettings::rule_change), Rule::Type::points));

    startplayer_random->signal_toggled().connect(sigc::mem_fun(*this, &PartySettings::startplayer_change_event));
    for (auto& p : startplayer)
      p->signal_toggled().connect(sigc::mem_fun(*this, &PartySettings::startplayer_change_event));

    for (unsigned p = 0; p < swap_players_buttons.size(); ++p)
      swap_players_buttons[p]->signal_clicked().connect(sigc::bind<unsigned const>(sigc::mem_fun(*this, &PartySettings::swap_players_event), p));

    configure_players->signal_clicked().connect(sigc::mem_fun0(*players,
                                                               &Gtk::Window::present)
                                               );
    configure_rules->signal_clicked().connect(sigc::mem_fun0(*rules,
                                                             &Gtk::Window::present)
                                             );
  } // signals

  show_all_children();
  sensitivity_update();
  update();

  party.signal_seed_changed.connect_back(*this, &PartySettings::update, disconnector_);
  party.signal_startplayer_changed.connect_back(*this, &PartySettings::update, disconnector_);
  party.rule().signal_changed.connect_back([this](auto type, auto value)
                                           { rules_update();
                                           rules->update(type); },
                                           disconnector_);
  ::preferences.signal_changed(::Preferences::Type::additional_party_settings).connect_back([this](auto) {update();}, disconnector_);
  ::preferences.signal_changed(::Preferences::Type::shuffle_without_seed).connect_back([this](auto) {update();}, disconnector_);

} // void PartySettings::init()

/** get the party settings
 **/
void
PartySettings::get()
{
  if (::fast_play & FastPlay::party_start) {
    //::fast_play.remove(FastPlay::party_start);
    return ;
  }

  auto& party = ui->party();
  update();

  players->create_backup();
  rules->create_backup();

  start_party_button->show();
  start_party_button->grab_default();
  start_party_button->grab_focus();

  present();
  start_party_button->grab_focus();

  while (   !ui->thrower
         && is_visible()
         && ui->party().status() == Party::Status::init) {
    ::ui->wait();
    if (::bug_report_replay
        && ::bug_report_replay->auto_start_party())
      break;
  }

  if (ui->party().status() == Party::Status::init) {
    // set the seed
    if (seed_random->get_active()) {
      party.set_random_seed();
    } else {
      party.set_seed(seed_value->get_value());
    }

    // set the startplayer
    if (startplayer_random->get_active())
      party.set_random_startplayer();
    else
      for (unsigned p = 0; p < party.players().size(); ++p)
        if (startplayer[p]->get_active()) {
          party.set_startplayer(p);
          break;
        }

    players->create_backup();
    rules->create_backup();
  } // if (ui->party().status() == Party::Status::init)

  hide();
} // void PartySettings::get()

/** show the window.
 ** Shows/hides the action buttons
 **/
void
PartySettings::on_show()
{
#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  StickyDialog::on_show();
#endif

  if (ui->first_run_window)
    ui->first_run_window->raise();
  if (ui->program_updated_window)
    ui->program_updated_window->raise();

  update();
} // void PartySettings::show()

/** load the bug report
 **/
void
PartySettings::load_bug_report()
{
  ui->bug_report->load_file_chooser->present();
} // void PartySettings::load_bug_report()

/** update the sensitivity of all elements
 **/
void
PartySettings::sensitivity_update()
{
  auto const sensitive = (ui->party().status() == Party::Status::init);
  if (sensitive)
    seed_value_sensitivity_update();
  else
    seed_value->set_sensitive(false);

  seed_random->set_sensitive(sensitive);

  rule_number_of_rounds_limited->set_sensitive(sensitive);
  rule_number_of_rounds->set_sensitive(sensitive);
  rule_points_limited->set_sensitive(sensitive);
  rule_points->set_sensitive(sensitive);

  for (auto& p : startplayer)
    p->set_sensitive(sensitive);
  startplayer_random->set_sensitive(sensitive);
  for (auto p : swap_players_buttons)
    p->set_sensitive(sensitive);

  players->sensitivity_update();
  rules->sensitivity_update();

  load_bug_report_button->set_sensitive(sensitive);
  start_party_button->set_sensitive(sensitive);

  if (::preferences(::Preferences::Type::additional_party_settings)) {
    if (::preferences(::Preferences::Type::shuffle_without_seed)) {
      seed_frame->hide();
    } else {
      seed_frame->show();
    }
    startplayer_frame->show();
  } else {
    seed_frame->hide();
    startplayer_frame->hide();
  }
} // void PartySettings::sensitivity_update()

/** update the sensitivity of 'seed_value'
 **/
void
PartySettings::seed_value_sensitivity_update()
{
  switch (ui->party().status()) {
  case Party::Status::init:
  case Party::Status::initial_loaded:
    seed_value->set_sensitive(!(seed_random->get_active()));
    break;
  default:
    seed_value->set_sensitive(false);
    break;
  } // switch (ui->party().status())
} // void PartySettings::seed_value_sensitivity_update()

/** update the widgets
 **/
void
PartySettings::update()
{
  if (!is_visible())
    return ;

  auto& party = ui->party();
  sensitivity_update();

  switch (ui->party().status()) {
  case Party::Status::init:
#ifndef RELEASE
    load_bug_report_button->show();
#endif
    start_party_button->show();
    close_button->hide();

    start_party_button->grab_default();
    break;
  default:
    load_bug_report_button->hide();
    start_party_button->hide();
    close_button->show();

    close_button->grab_default();
    break;
  } // switch (ui->party().status())

  auto const random_seed = party.random_seed();
  if (!random_seed)
    seed_value->set_value(party.seed());
  seed_random->set_active(random_seed);

  rules_update();

  if (party.random_startplayer())
    startplayer_random->set_active(true);
  else
    startplayer[party.startplayer()]->set_active(true);

  for (auto& p : party.players()) {
    name_update_local(p);
  }
  players->update();
  rules->update_all();
} // void PartySettings::update()

/** update the rules (value, sensitivity)
 **/
void
PartySettings::rules_update()
{
  auto const& rule = ui->party().rule();

  rule_number_of_rounds_limited->set_active(rule(Rule::Type::number_of_rounds_limited));
  rule_number_of_rounds_limited->set_sensitive(rule.dependencies(Rule::Type::number_of_rounds_limited));
  rule_number_of_rounds->set_value(rule(Rule::Type::number_of_rounds));
  rule_number_of_rounds->set_sensitive(rule(Rule::Type::number_of_rounds_limited));
  rule_number_of_rounds->set_value(rule(Rule::Type::number_of_rounds));
  {
    auto const value = rule(Rule::Type::number_of_rounds);
    rule_number_of_rounds->set_range(rule.min(Rule::Type::number_of_rounds),
                                     rule.max(Rule::Type::number_of_rounds));
    rule_number_of_rounds->set_value(value);
  }

  rule_points_limited->set_active(rule(Rule::Type::points_limited));
  rule_points_limited->set_sensitive(rule.dependencies(Rule::Type::points_limited));
  rule_points->set_sensitive(true);
  rule_points->set_value(rule(Rule::Type::points));
  rule_points->set_sensitive(rule(Rule::Type::points_limited));
  {
    auto const value = rule(Rule::Type::points);
    rule_points->set_range(rule.min(Rule::Type::points),
                           rule.max(Rule::Type::points));
    rule_points->set_value(value);
  }
} // void PartySettings::rules_update()

/** the rules have been changed
 **
 ** @param    type        the type of the rule that has changed
 **/
void
PartySettings::rule_change(int const type)
{
  auto& rule = ui->party().rule();
  switch(type) {
  case Rule::Type::number_of_rounds_limited:
    rule.set(Rule::Type::number_of_rounds_limited,
             rule_number_of_rounds_limited->get_active());
    break;
  case Rule::Type::number_of_rounds:
    rule.set(Rule::Type::number_of_rounds,
             rule_number_of_rounds->get_value_as_int());
    break;
  case Rule::Type::points_limited:
    rule.set(Rule::Type::points_limited,
             rule_points_limited->get_active());
    break;
  case Rule::Type::points:
    rule.set(Rule::Type::points,
             rule_points->get_value_as_int());
    break;
  default:
    break;
  } // switch(type)
} // void PartySettings::rule_change(int type)

/** the players 'player_a' and 'player_b' have been swapped
 **
 ** @param    player_a   first player
 ** @param    player_b   second player
 **/
void
PartySettings::players_swapped(Player const& player_a,
                               Player const& player_b)
{
  name_update_local(player_a);
  name_update_local(player_b);

  players->players_swapped(player_a, player_b);
} // void PartySettings::players_swapped(Player player_a, Player player_b)

/** update 'player'
 **
 ** @param    player   player to update
 **/
void
PartySettings::player_update(Player const& player)
{
  if (ui->party().status() <= Party::Status::init)
    return ;

  name_update_local(player);
  players->player_update(player);
} // void PartySettings::player_update(Player player)

/** update the name of 'player'
 **
 ** @param    player   player with the new name
 **/
void
PartySettings::name_update(Player const& player)
{
  name_update_local(player);
  players->name_update(player);
} // void PartySettings::name_update(Player const& player)

/** update the name of 'player'
 **
 ** @param    player   player with the new name
 **/
void
PartySettings::name_update_local(Player const& player)
{
  Pango::FontDescription fd;
  fd.set_weight(player.type() == Player::Type::human
                ? Pango::WEIGHT_BOLD
                : Pango::WEIGHT_NORMAL);
  auto const playerno = ui->party().players().no(player);
  DEBUG_ASSERTION((playerno < UINT_MAX),
                  "PlayerSettings::name_update_local(player)\n"
                  "  player '" << player.name() << "' not found in the party");
  if (playerno >= 4)
    return ;

  startplayer[playerno]->get_child()->override_font(fd);

  startplayer[playerno]->set_label(player.name());
} // void PartySettings::name_update_local(Player player)

/** update the voice of 'player'
 **
 ** @param    player   player with the new voice
 **/
void
PartySettings::voice_update(Player const& player)
{
  players->voice_update(player);
} // void PartySettings::voice_update(Player player)

/** update the aiconfig
 **
 ** @param    aiconfig   changed aiconfig
 **/
void
PartySettings::aiconfig_update(Aiconfig const& aiconfig)
{
  players->aiconfig_update(aiconfig);
} // void PartySettings::aiconfig_update(Aiconfig aiconfig)

/** a new party is started
 **/
void
PartySettings::start_party_event()
{
  hide();
  ui->party().set_status(Party::Status::init);
} // void PartySettings::start_party_event()

/** the seed has changed
 **/
void
PartySettings::seed_change_event()
{
  auto& party = ui->party();
  // set the seed
  if (seed_random->get_active())
    party.set_random_seed();
  else
    party.set_seed(seed_value->get_value());
} // void PartySettings::seed_change_event()

/** the startplayer has changed
 **/
void
PartySettings::startplayer_change_event()
{
  auto& party = ui->party();
  // set the startplayer
  if (startplayer_random->get_active()) {
    party.set_random_startplayer();
  } else {
    for (unsigned p = 0; p < party.players().size(); ++p)
      if (startplayer[p]->get_active()) {
        party.set_startplayer(p);
        break;
      }
  }
} // void PartySettings::startplayer_change_event()

/** players shall be swapped
 **
 ** @param    p   player to swap with the following
 **/
void
PartySettings::swap_players_event(unsigned const p)
{
  auto& party = ui->party();
  party.players().swap(p, (p + 1) % party.players().size());
} // void PartySettings::swap_players_event(unsigned p)

/** a key has been pressed
 **
 ** @param    key   the key
 **
 ** @return   whether the key was used
 **/
bool
PartySettings::key_press(GdkEventKey* const key)
{
  bool used = false;

  if ((key->state & ~GDK_SHIFT_MASK) == 0) {
    switch (key->keyval) {
    case GDK_KEY_p: // show the players
      players->present();
      used = true;
      break;
    case GDK_KEY_r: // show the rules
      rules->present();
      used = true;
      break;
    } // switch (key->keyval)
  } // if ((key->state & ~GDK_SHIFT_MASK) == 0)

  return (used || ui->key_press(key));
} // bool PartySettings::key_press(GdkEventKey* key)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
