/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"
#include "widgets/sticky_dialog.h"
namespace Gtk {
class Notebook;
class Button;
class Label;
class SpinButton;
class CheckButton;
class RadioButton;
class Frame;
} // namespace Gtk

class Setting;
class Player;
class Aiconfig;

namespace UI_GTKMM_NS {
class Players;
class Rules;

/**
 ** the party settings dialog
 **/
class PartySettings : public Base, public Gtk::StickyDialog {
  public:
    explicit PartySettings(Base* parent);
    ~PartySettings() override;

    PartySettings() = delete;
    PartySettings(PartySettings const&) = delete;
    PartySettings& operator=(PartySettings const&) = delete;

    // get the party settings
    void get();
    // the shown signal
    void on_show() override;

    // load a bug report
    void load_bug_report();

    // update the sensitivity
    void sensitivity_update();
    // update the seed value sensitivity
    void seed_value_sensitivity_update();

    // update all
    void update();

    // update the rules (value, sensitivity)
    void rules_update();
    // the rules have changed
    void rule_change(int type);

    // the players have been swapped
    void players_swapped(::Player const& player_a,
                         ::Player const& player_b);
    // update the player
    void player_update(::Player const& player);
    // update the name of the player
    void name_update(::Player const& player);
  private:
    // update the name and font of the player
    void name_update_local(::Player const& player);
  public:
    // update the font of the player
    void voice_update(::Player const& player);
    // update the aiconfig
    void aiconfig_update(::Aiconfig const& aiconfig);

  private:
    // initialize the window
    void init();

    // event: start the party
    void start_party_event();

    // event: the seed has changed
    void seed_change_event();
    // event: the startplayer has changed
    void startplayer_change_event();

    // event: players have changed
    void swap_players_event(unsigned p);

    // event: a key press
    bool key_press(GdkEventKey* key);

  public:
    unique_ptr<Players> players;
    unique_ptr<Rules> rules;

  private:
    AutoDisconnector disconnector_;

    Gtk::Button* load_bug_report_button = nullptr;
    Gtk::Button* start_party_button = nullptr;
    Gtk::Button* close_button = nullptr;

    Gtk::Frame* seed_frame = nullptr;
    Gtk::SpinButton* seed_value = nullptr;
    Gtk::CheckButton* seed_random = nullptr;

    Gtk::CheckButton* rule_number_of_rounds_limited = nullptr;
    Gtk::SpinButton* rule_number_of_rounds = nullptr;
    Gtk::CheckButton* rule_points_limited = nullptr;
    Gtk::SpinButton* rule_points = nullptr;

    Gtk::Frame* startplayer_frame = nullptr;
    vector<Gtk::RadioButton*> startplayer;
    Gtk::RadioButton* startplayer_random = nullptr;
    vector<Gtk::Button*> swap_players_buttons;

    Gtk::Button* configure_players = nullptr;
    Gtk::Button* configure_rules = nullptr;
}; // class PartySettings : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
