/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"
#include "players.h"
#include "../../player/player.h"
using PlayerType = ::Player::Type;

#include <sigc++/trackable.h>
namespace Gtk {
class Label;
class Button;
class Entry;
class RadioButton;
class FileMenu;
} // namespace Gtk

namespace UI_GTKMM_NS {

/**
 ** the configuration of one player (without aiconfig)
 **/
class Players::Player : public Base, public sigc::trackable {
  public:
    friend class Players;

  public:
    class AiConfig;

  public:
    Player(Players* players, unsigned playerno);
    ~Player() override;

    // the player
    ::Player const& player() const;
    // the player
    ::Player& player();
    // the player number
    unsigned playerno() const;

    // the aiconfig
    AiConfig& aiconfig();

    // create a backup
    void create_backup();
    // reset the player
    void reset();

    // event: the name has changed
    bool name_changed_event(GdkEventFocus* event);
    // set a random name
    void random_name();
    // select a voice
    void voice_selected(string const& voice);
    // change the type
    void type_change(PlayerType type);
    // update the aiconfig
    void aiconfig_update();

    // update the sensitivity
    void sensitivity_update();
    // update all
    void update();
    // update the name
    void name_update();
    // update the voice
    void voice_update();

  private:
    // initialize all elements
    void init();

  private:
    Players* const players = nullptr;

    std::unique_ptr<AiConfig> aiconfig_;

    unsigned const playerno_ = UINT_MAX;
    std::unique_ptr<::Player const> player_bak;

  protected:
    Gtk::Label* name_label = nullptr;
    Gtk::Entry* name_entry = nullptr;
    Gtk::Button* random_name_button = nullptr;
    Gtk::Button* voice_button = nullptr;
    Gtk::FileMenu* voice_menu = nullptr;
    vector<Gtk::RadioButton*> type_button;
    Gtk::Button* reset_button = nullptr;

  private: // unused
    Player() = delete;
    Player(Player const&) = delete;
    Player& operator=(Player const&) = delete;
}; // class Players::Player : public Base, public Gtk::Alignment

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
