/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "players.h"
#include "player.h"
#include "aiconfig.h"
#include "players_db.h"
#include "ui.h"

#include "../../party/party.h"
#include "../../player/ai/ai.h"
#include "../../player/aiconfig.h"

#include "widgets/labelspinbutton.h"
#include <gtkmm/grid.h>
#include <gtkmm/fixed.h>
#include <gtkmm/label.h>
#include <gtkmm/frame.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/entry.h>
#include <gtkmm/notebook.h>
#include <gtkmm/radiobutton.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 **/
Players::Players(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::Players"), false)
{
  ui->add_window(*this);

  signal_realize().connect(sigc::mem_fun(*this, &Players::init));
  signal_show().connect(sigc::mem_fun(*this, &Players::sensitivity_update));
#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_key_press_event().connect(sigc::mem_fun(*this, &Players::on_key_press_event));
#endif

  create_backup();
} // Players::Players(Base* parent)

/** destructor
 **/
Players::~Players() = default;

/** creates all subelements
 **
 ** @todo   support for more than four players
 **/
void
Players::init()
{
  set_icon(ui->icon);

  { // set default size
    Gdk::Geometry geometry;
    geometry.min_width = 70 EX;
    geometry.min_height = 10 EX;

    set_geometry_hints(*this, geometry, Gdk::HINT_MIN_SIZE);
  } // set default size

  for (unsigned p = 0; p < ui->party().players().size(); ++p)
    player_.push_back(std::make_unique<Player>(this, p));

  { // layout
    add_close_button(*this);

    { // players configurations
      players_notebook = Gtk::manage(new Gtk::Notebook());
      //players_notebook->set_tab_pos(Gtk::POS_LEFT);

      vector<Gtk::Notebook*> player_notebook;
      for (unsigned p = 0; p < ui->party().players().size(); ++p) {
        Players::Player& player = this->player(p);

        auto player_box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 3 EX / 2));

        players_notebook->append_page(*player_box,
                                            *player.name_label);

        { // buttons
          auto grid = Gtk::manage(new Gtk::Grid());
          grid->set_halign(Gtk::ALIGN_CENTER);
          grid->set_column_homogeneous(true);
          grid->set_margin_bottom(1 EX);
          { // reset button
            grid->add(*player.reset_button);
          } // reset button
          grid->add(*Gtk::manage(new Gtk::Fixed())); // empty space
          { // statistics button
            auto statistics_button
              = Gtk::manage(new Gtk::Button(_("Button::statistics")));
            statistics_button->set_image_from_icon_name("view-statistics");
            statistics_button->set_always_show_image();
            grid->add(*statistics_button);
            statistics_button->signal_clicked().connect(sigc::mem_fun0(*(ui->players_db), &Gtk::Window::present));
          } // statistics button
          player_box->pack_end(*grid, false, true);
        } // buttons


        player_notebook.push_back(Gtk::manage(new Gtk::Notebook()));
        player_box->pack_start(*player_notebook.back(), true, true);
        { // person page
          auto person_label = Gtk::manage(new Gtk::Label(_("Label::person")));

          auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 2 EX));
          vbox->set_halign(Gtk::ALIGN_CENTER);
          vbox->set_valign(Gtk::ALIGN_CENTER);
          vbox->set_border_width(1 EX);
          player_notebook.back()->append_page(*vbox, *person_label);

          { // the name widgets
            auto hbox = Gtk::manage(new Gtk::Box());
            hbox->set_spacing(1 EM);
            hbox->set_valign(Gtk::ALIGN_CENTER);
            auto name_label = Gtk::manage(new Gtk::Label(_("Label::name") + ":"));

            player.name_entry->set_width_chars(12);
            hbox->add(*name_label);
            hbox->add(*player.name_entry);
            hbox->add(*player.random_name_button);
            vbox->add(*hbox);
          } // the name widgets
          { // the voice widgets
            auto hbox = Gtk::manage(new Gtk::Box());
            hbox->set_spacing(1 EM);
            hbox->set_halign(Gtk::ALIGN_CENTER);

            auto voice_label = Gtk::manage(new Gtk::Label(_("Label::voice") + ":"));

            hbox->add(*voice_label);
            hbox->add(*player.voice_button);
            vbox->add(*hbox);
          } // the voice widgets
          { // the type widgets
            auto vbox2 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX / 2));
            vbox2->set_halign(Gtk::ALIGN_CENTER);
            for (auto& type : player.type_button)
              vbox2->add(*type);
            vbox->add(*vbox2);
          } // the type widgets
          { // the ai difficulty
            vbox->add(*player.aiconfig().difficulty_container);
            player.aiconfig().difficulty_container->set_halign(Gtk::ALIGN_CENTER);
          } // the ai difficulty
        } // person page
        { // aiconfig

          auto aiconfig_label = Gtk::manage(new Gtk::Label(_("Label::aiconfig")));

          player_notebook.back()->append_page(*player.aiconfig().container,
                                              *aiconfig_label);

        } // aiconfig
      }

      // set the signals for the notebook changes
#ifndef WINDOWS
      // crash on windows
      for (auto notebook1 : player_notebook) {
        for (auto notebook2 : player_notebook) {
          if (notebook1 == notebook2)
            continue;

          notebook1->signal_switch_page().connect(sigc::bind<Gtk::Notebook*>(sigc::mem_fun(*this, &Players::notebook_switch_page_signal), notebook2));
        }
      }

      for (auto& player1 : player_) {
        for (auto& player2 : player_) {
          if (player1 == player2)
            continue;

          player1->aiconfig().notebook->signal_switch_page().connect(sigc::bind<Gtk::Notebook*>(sigc::mem_fun(*this, &Players::notebook_switch_page_signal), player2->aiconfig().notebook));

          for (size_t i = 0;
               i < player1->aiconfig().notebooks.size();
               ++i)
            player1->aiconfig().notebooks[i]->signal_switch_page().connect(sigc::bind<Gtk::Notebook*>(sigc::mem_fun(*this, &Players::notebook_switch_page_signal), player2->aiconfig().notebooks[i]));
        } // for (player2)
      } // for (player1)
#endif
      get_content_area()->pack_start(*players_notebook);
    } // players configurations
  } // layout

  show_all_children();

  update();

  ui->party().signal_start.connect_back(*this, &Players::sensitivity_update, disconnector_);
} // void Players::init()

/** -> result
 **
 ** @param    playerno   the player number
 **
 ** @return   the player configuration of player 'playerno'
 **/
Players::Player&
Players::player(unsigned const playerno)
{
  DEBUG_ASSERTION((player_.size() > playerno),
                  "Players::player(playerno):\n"
                  "  playerno too great: "
                  << playerno << " >= " << player_.size());

  return *(player_[playerno]);
} // Players::Player& Players::player(unsigned playerno)

/** -> result
 **
 ** @param    player   the player
 **
 ** @return   the player configuration of 'player'
 **/
Players::Player&
Players::player(::Player const& player)
{
  auto const playerno = ui->party().players().no(player);
  DEBUG_ASSERTION((playerno < UINT_MAX),
                  "Players::player(player):\n"
                  "  player '" << player.name() << "' not found in the party");

  return this->player(playerno);
} // Players::Player& Players::player(::Player player)

/** creates a backup
 **/
void
Players::create_backup()
{
  for (auto& player : player_)
    player->create_backup();
} // void Players::create_backup()

/** resets the settings
 **
 ** @todo   set the elements
 **/
void
Players::reset()
{
  if (!is_visible())
    return ;

  for (auto& player : player_)
    player->reset();
  update();
} // void Players::reset()

/** update the sensitivity of all widgets
 **/
void
Players::sensitivity_update()
{
  if (!is_visible())
    return ;

  for (auto& player : player_)
    player->sensitivity_update();
} // void Players::sensitivity_update()

/** update all widgets
 **/
void
Players::update()
{
  if (!is_visible())
    return ;

  for (auto& player : player_)
    player->update();
} // void Players::update()

/** the players 'player_a' and 'player_b' have been swapped
 **
 ** @param    player_a   first player
 ** @param    player_b   second player
 **/
void
Players::players_swapped(::Player const& player_a,
                         ::Player const& player_b)
{
  if (!is_visible())
    return ;

#ifdef POSTPONED
  std::swap(player(player_a), player(player_a));
  // Problem: cannot swap notebook pages
#else
  player(player_a).update();
  player(player_b).update();
#endif
} // void Players::players_swapped(Player player_a, Player player_b)

/** update 'player'
 **
 ** @param    player   player to update
 **/
void
Players::player_update(::Player const& player)
{
  if (!is_visible())
    return ;

  this->player(player).update();
} // void Players::player_update(::Player player)

/** update the name of 'player'
 **
 ** @param    player   player with the new name
 **/
void
Players::name_update(::Player const& player)
{
  if (!is_visible())
    return ;

  this->player(player).name_update();
} // void Players::name_update(::Player player)

/** update the voice of 'player'
 **
 ** @param    player   player with the new voice
 **/
void
Players::voice_update(::Player const& player)
{
  if (!is_visible())
    return ;

  this->player(player).voice_update();
} // void Players::voice_update(::Player player)

/** update the aiconfig
 **
 ** @param    aiconfig   changed aiconfig
 **/
void
Players::aiconfig_update(::Aiconfig const& aiconfig)
{
  if (!is_visible())
    return ;

  for (auto& p : player_)
    p->aiconfig_update();
} // void Players::aiconfig_update(::Aiconfig aiconfig)

/** signal: the page of the notebook is changed
 **
 ** @param    page      current page
 ** @param    pageno    current page number
 ** @param    notebook   notebook to change the page to 'pageno'
 **/
void
Players::notebook_switch_page_signal(Widget* widget,
                                     guint pageno,
                                     Gtk::Notebook* notebook)
{
  // Note: the signal for this member is binded in 'Players'
  // because it is called by the other 'AiConfig's which have to be
  // created first.

#ifdef WORKAROUND
  // Das Signal wird beim Destruktor ausgelöst.
  if (ui->party().status() == Party::Status::quit)
    return ;
#endif

  if (notebook->get_current_page() < 0)
    return ;

  if (static_cast<unsigned>(notebook->get_current_page()) != pageno)
    notebook->set_current_page(pageno);
} // void Players::notebook_switch_page_signal(Widget*, guint pageno, Gtk::Notebook* notebook)

/** a key has been pressed
 ** C-o: output of the active player on 'stdout'
 **
 ** @param    key   the key
 **
 ** @return   whether the key was managed
 **/
bool
Players::on_key_press_event(GdkEventKey* key)
{
  bool managed = false;

  if ((key->state & ~GDK_SHIFT_MASK) == GDK_CONTROL_MASK) {
    switch (key->keyval) {
    case GDK_KEY_o: { // ouput of the player
      if (players_notebook->get_current_page() == -1)
        break;
      bool const write_database_bak = ::Player::write_database;
      ::Player::write_database = false;
      cout << player(players_notebook->get_current_page()).player();
      ::Player::write_database = write_database_bak;
      managed = true;
    } break;
    case GDK_KEY_u: // update the entries
      if (players_notebook->get_current_page() == -1)
        break;
      player(players_notebook->get_current_page()).update();
      managed = true;
      break;
    } // switch (key->keyval)
  } // if (key->state == GDK_CONTROL_MASK)

  return (managed
          || StickyDialog::on_key_press_event(key)
          || ui->key_press(key));
} // bool Players::on_key_press_event(GdkEventKey* key)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
