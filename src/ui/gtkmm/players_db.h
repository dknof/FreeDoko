/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"
#include "widgets/sticky_dialog.h"

#include "../../game/specialpoint.h"
#include "../../player/aiconfig_heuristic.h"
class Party;
class Player;
class PlayersDb;
class TLWcount;

#include <gtkmm/treemodel.h>
#include <gtkmm/treestore.h>
namespace Gtk {
class TreeView;
} // namespace Gtk
namespace UI_GTKMM_NS {
/**
 ** window with the player db
 **
 ** @todo   only update the heuristics at the end of a game,
 **         so that the human player can get no additional information
 **         (p.e. with 'only valid card')
 **/
class PlayersDB : public Base, public Gtk::StickyDialog {
  enum class Statistic {
    total,
    won,
    lost,
    percent_won
  }; // enum Statistic
  static constexpr Statistic statistic_all[] = {
    Statistic::total,
    Statistic::won, 
    Statistic::lost, 
    Statistic::percent_won, 
  };
  static string to_string(Statistic statistic);
  static string gettext(Statistic statistic);

  // model for the data
  struct PlayersDBModel : public Gtk::TreeModel::ColumnRecord {
      PlayersDBModel(unsigned playerno);

      Gtk::TreeModelColumn<Glib::ustring> type;
      vector<Gtk::TreeModelColumn<Glib::ustring> > statistic;
  };

  public:
  explicit PlayersDB(Base* parent);
  ~PlayersDB() override;

  PlayersDB() = delete;
  PlayersDB(PlayersDB const&) = delete;
  PlayersDB& operator=(PlayersDB const&) = delete;

  void party_close();
  void game_finished();

  void update_db();
  void name_changed(Player const& player);

  private:
  void init();
  void recreate_db();
  void set_statistic(Statistic statistic);

  void clear_db();

  // returns a string representing the data
  Glib::ustring statistic_data(TLWcount const& tlwcount) const;
  // returns a string representing the data
  Glib::ustring heuristic_statistic_data(unsigned total,
                                         unsigned num) const;

  // a key has been pressed
  bool on_key_press_event(GdkEventKey* key) override;

  private:
  AutoDisconnector disconnector_;

  Statistic statistic = Statistic::total;

  std::unique_ptr<PlayersDBModel> players_db_model;
  Glib::RefPtr<Gtk::TreeStore> players_db_list;
  Gtk::TreeView* players_db_treeview = nullptr;

  Gtk::TreeModel::Row ranking;
  Gtk::TreeModel::Row average_game_points;
  Gtk::TreeModel::Row average_game_trick_points;
  Gtk::TreeModel::Row games;
  Gtk::TreeModel::Row games_marriage;
  Gtk::TreeModel::Row games_poverty;
  Gtk::TreeModel::Row games_solo;
  Gtk::TreeModel::Row games_solo_color;
  Gtk::TreeModel::Row games_solo_picture;
  Gtk::TreeModel::Row games_solo_picture_single;
  Gtk::TreeModel::Row games_solo_picture_double;
  vector<Gtk::TreeModel::Row> game;
  Gtk::TreeModel::Row specialpoints;
  Gtk::TreeModel::Row specialpoints_winning;
  Gtk::TreeModel::Row specialpoints_announcement;
  std::map<Specialpoint::Type, Gtk::TreeModel::Row> specialpoint;
  Gtk::TreeModel::Row heuristics;
  Gtk::TreeModel::Row heuristics_general;
  Gtk::TreeModel::Row heuristics_marriage;
  Gtk::TreeModel::Row heuristics_poverty;
  Gtk::TreeModel::Row heuristics_solo;
  Gtk::TreeModel::Row heuristics_solo_color;
  Gtk::TreeModel::Row heuristics_solo_picture;
  Gtk::TreeModel::Row heuristics_solo_meatless;
  std::map<AiconfigHeuristic, Gtk::TreeModel::Row> heuristic;
}; // class PlayersDB : public Base, public Gtk::StickyDialog

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
