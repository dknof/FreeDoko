/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "preferences.h"
#include "preferences/cards_order.h"
#include "preferences/cardset.h"
#include "preferences/cards_back.h"
#include "preferences/iconset.h"
#include "preferences/background.h"

#include "ui.h"
#include "cards.h"
#include "main_window.h"
#include "menu.h"

#include "../../misc/preferences.h"
#ifdef WINDOWS
#include "../../utils/string.h"
#endif

#include "widgets/labelspinbutton.h"
#include "widgets/filemenu_extension.h"
#include "widgets/filemenu_file.h"
#include <gtkmm/icontheme.h>
#include <gtkmm/notebook.h>
#include <gtkmm/label.h>
#include <gtkmm/entry.h>
#include <gtkmm/grid.h>
#include <gtkmm/scale.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/checkmenuitem.h>
#include <gtkmm/separatormenuitem.h>
#include <gtkmm/fontchooserdialog.h>
#include <gtkmm/colorchooserdialog.h>
namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 **/
Preferences::Preferences(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::preferences"), false)
{
  ui->add_window(*this);

#ifdef WORKAROUND
  { // name: change to utf8
    auto const name = UI_GTKMM::to_utf8(::preferences(::Preferences::Type::name));
    ::preferences.set(::Preferences::Type::name, name);
  } // name: change to utf8
#endif

  group_notebook = Gtk::manage(new Gtk::Notebook());
  group_notebook->set_tab_pos(Gtk::POS_LEFT);

  create_backup();

  signal_realize().connect(sigc::mem_fun(*this, &Preferences::init));
  signal_show().connect(sigc::mem_fun(*this, &Preferences::update_all));

#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_key_press_event().connect(sigc::mem_fun(*this, &Preferences::on_key_press_event));
#endif
} // Preferences::Preferences(Base* parent)

/** destruktor
 **/
Preferences::~Preferences()
{
  for (auto widget : type_bool)
    delete static_cast<::Preferences::Type::Bool*>(widget.second->steal_data("type"));

  delete language_menu;
  for (auto selector : font_chooser) {
    delete static_cast<int*>(selector.second->steal_data("type"));
    delete selector.second;
  } // for (selector : font_chooser)
  for (auto selector : color_chooser) {
    delete static_cast<int*>(selector.second->steal_data("type"));
    delete selector.second;
  } // for (selector : color_chooser)
} // Preferences::~Preferences()

/** creates all subelements
 **/
void
Preferences::init()
{
  set_icon(ui->icon);

  reset_button = Gtk::manage(new Gtk::Button(_("Button::reset")));
  reset_button->set_image_from_icon_name("edit-undo");
  reset_button->set_always_show_image();
  add_action_widget(*reset_button, Gtk::RESPONSE_REJECT);
  reset_button->show();
  reset_button->signal_clicked().connect(sigc::mem_fun(*this,
                                                             &Preferences::reset)
                                              );

  auto close_button = add_close_button(*this);
  close_button->signal_clicked().connect(sigc::mem_fun(*this,
                                                       &Preferences::save)
                                        );



  language_menu = new Gtk::Menu();

  { // create the buttons
    for (auto t : ::Preferences::Type::bool_list) {
      type_bool[t] = Gtk::manage(new Gtk::CheckButton(_(t)));
      type_bool[t]->set_data("type", new ::Preferences::Type::Bool(t));
      type_bool[t]->signal_toggled().connect(sigc::bind<int>(sigc::mem_fun(*this, &Preferences::changed), t));
    }
    for (auto const type : ::Preferences::Type::unsigned_list) {
      switch (type) {
      case ::Preferences::Type::max_attempts_sec_for_distribution: {
        auto button = Gtk::manage(new Gtk::LabelSpinButton(_(type) + ":", _("seconds")));
        type_unsigned[type] = button;
        button->get_spin_button()->set_digits(0);
        button->get_spin_button()->set_increments(1, 1000);
        button->signal_value_changed().connect(sigc::bind<int>(sigc::mem_fun(*this, &Preferences::changed), type));
        break;
      }
      case ::Preferences::Type::min_trumps_to_distribute:
      case ::Preferences::Type::max_trumps_to_distribute:
      case ::Preferences::Type::min_queens_to_distribute:
      case ::Preferences::Type::max_queens_to_distribute:
      case ::Preferences::Type::min_jacks_to_distribute:
      case ::Preferences::Type::max_jacks_to_distribute:
      case ::Preferences::Type::min_club_queens_to_distribute:
      case ::Preferences::Type::max_club_queens_to_distribute:
      case ::Preferences::Type::min_heart_tens_to_distribute:
      case ::Preferences::Type::max_heart_tens_to_distribute:
      case ::Preferences::Type::min_diamond_aces_to_distribute:
      case ::Preferences::Type::max_diamond_aces_to_distribute:
      case ::Preferences::Type::min_color_aces_to_distribute:
      case ::Preferences::Type::max_color_aces_to_distribute:
      case ::Preferences::Type::min_blank_colors_to_distribute:
      case ::Preferences::Type::max_blank_colors_to_distribute: {
        auto scale = Gtk::manage(new Gtk::Scale());
        type_unsigned[type] = scale;
        scale->set_range(::preferences.min(type), ::preferences.max(type));
        scale->set_increments(1, 1);
        scale->set_digits(0);
        scale->set_value_pos(Gtk::PositionType::POS_BOTTOM);
        for (auto i = preferences.min(type); i <= preferences.max(type); ++i)
          scale->add_mark(i, Gtk::POS_BOTTOM, {});
        scale->signal_value_changed().connect(sigc::bind<int const>(sigc::mem_fun(*this, &Preferences::changed), type));
        break;
      }
      case ::Preferences::Type::card_play_delay:
      case ::Preferences::Type::full_trick_close_delay:
      case ::Preferences::Type::gametype_window_close_delay:
      case ::Preferences::Type::marriage_window_close_delay:
      case ::Preferences::Type::announcement_window_close_delay:
      case ::Preferences::Type::swines_window_close_delay: {
        auto button = Gtk::manage(new Gtk::LabelSpinButton(_(type), _("seconds")));
        type_unsigned[type] = button;
        // make the precision 1/1000 seconds
        button->get_spin_button()->set_digits(1);
        button->get_spin_button()->set_increments(0.1, 1);
        button->signal_value_changed().connect(sigc::bind<int>(sigc::mem_fun(*this, &Preferences::changed), type));
        break;
      }
      case ::Preferences::Type::table_rotation:
      case ::Preferences::Type::cards_height: {
        auto button = Gtk::manage(new Gtk::LabelSpinButton(_(type)));
        type_unsigned[type] = button;
        button->get_spin_button()->set_digits(0);
        button->get_spin_button()->set_increments(1, 10);
        button->signal_value_changed().connect(sigc::bind<int>(sigc::mem_fun(*this, &Preferences::changed), type));
        break;
      }
      case ::Preferences::Type::ui_main_window_pos_x:
      case ::Preferences::Type::ui_main_window_pos_y:
      case ::Preferences::Type::ui_main_window_width:
      case ::Preferences::Type::ui_main_window_height: {
        type_unsigned[type] = Gtk::manage(new Gtk::Label(""));;
        break;
      }
      } // switch(t)
      type_unsigned[type]->set_data("type", new ::Preferences::Type::Unsigned(type));
    }
    for (auto t : ::Preferences::Type::string_list) {
      type_string_label[t] = Gtk::manage(new Gtk::Label(_(t) + ":"));

      type_string_label[t]->set_data("type", new ::Preferences::Type::String(t));

      switch (t) {
      case ::Preferences::Type::name:
        type_string[t] = Gtk::manage(new Gtk::Entry());
        dynamic_cast<Gtk::Entry*>(type_string[t])->signal_focus_out_event().connect(sigc::bind<int>(sigc::mem_fun(*this, &Preferences::focus_out_event), t));
        break;

      case ::Preferences::Type::language:
        type_string[t] = Gtk::manage(new Gtk::Button(_(t)));
        dynamic_cast<Gtk::Button*>(type_string[t])->signal_clicked().connect(sigc::mem_fun(*language_menu, &Widget::show_all));
        //	  dynamic_cast<Gtk::Button*>(type_string[t])->signal_clicked().connect(sigc::mem_fun(*this, &Preferences::language_menu_create));
        dynamic_cast<Gtk::Button*>(type_string[t])->signal_clicked().connect(sigc::bind<bool>(sigc::mem_fun(*this, &Preferences::language_menu_create), true));
        // a direct signal for popping up the menu does not work
        //dynamic_cast<Gtk::Button*>(type_string[t])->signal_clicked().connect(sigc::bind<guint, guint32>(sigc::mem_fun(*language_menu, &Gtk::Menu::popup), 0, 0));
        break;

      case ::Preferences::Type::cardset:
      case ::Preferences::Type::cards_back:
      case ::Preferences::Type::iconset:
      case ::Preferences::Type::background:
        break;

      case ::Preferences::Type::name_font:
      case ::Preferences::Type::trickpile_points_font: {
        auto button = Gtk::manage(new Gtk::Button(_(t)));
        type_string[t] = button;
        font_chooser[t] = new Gtk::FontChooserDialog(_("font selector"));
        font_chooser[t]->set_data("type", new int(t));
        button->set_data("font selector", font_chooser[t]);
        button->signal_clicked().connect(sigc::mem_fun0(*font_chooser[t], &Gtk::Window::present));
        break;
      }

      case ::Preferences::Type::name_font_color:
      case ::Preferences::Type::name_active_font_color:
      case ::Preferences::Type::name_reservation_font_color:
      case ::Preferences::Type::trickpile_points_font_color:
      case ::Preferences::Type::poverty_shift_arrow_color: {
        auto button = Gtk::manage(new Gtk::Button(_(t)));
        type_string[t] = button;
        color_chooser[t] = new Gtk::ColorChooserDialog(_("color selector"));
        color_chooser[t]->set_data("type", new int(t));
        button->set_data("color selector", color_chooser[t]);
        button->signal_clicked().connect(sigc::mem_fun0(*color_chooser[t], &Gtk::Window::present));
        break;
      }

#ifdef USE_SOUND_COMMAND
      case ::Preferences::Type::play_sound_command:
#endif
      case ::Preferences::Type::browser_command:
        type_string[t] = Gtk::manage(new Gtk::Entry());
        dynamic_cast<Gtk::Entry*>(type_string[t])->signal_focus_out_event().connect(sigc::bind<int>(sigc::mem_fun(*this, &Preferences::focus_out_event), t));
        break;

      case ::Preferences::Type::cardset_license:
      case ::Preferences::Type::iconset_license:
        break;
      } // switch(t)
      if (type_string[t]) {
        type_string[t]->set_data("type", new ::Preferences::Type::String(t));
      }
    } // for (t : ::Preferences::Type::string_list)
  } // create the buttons

  { // set the pages of the notebook
    { // General
      auto vbox = add_group_box("general", "games-config-custom");
      (void)_("Preferences::Group::general");

      { // name
        auto hbox = Gtk::manage(new Gtk::Box());
        hbox->set_spacing(1 EX);
        hbox->set_halign(Gtk::ALIGN_CENTER);
        hbox->add(*type_string_label[::Preferences::Type::name]);
        hbox->add(*type_string[::Preferences::Type::name]);
        vbox->add(*hbox);
      } // name
      { // language
        auto hbox = Gtk::manage(new Gtk::Box());
        hbox->set_spacing(1 EX);
        hbox->set_halign(Gtk::ALIGN_CENTER);
        hbox->add(*type_string_label[::Preferences::Type::language]);
        hbox->add(*type_string[::Preferences::Type::language]);
        vbox->add(*hbox);
      } // language
      vbox->add(*type_bool[::Preferences::Type::sound]);
      vbox->add(*type_bool[::Preferences::Type::use_threads]);
      vbox->add(*type_bool[::Preferences::Type::show_bug_report_button_in_game_finished_window]);
      vbox->add(*type_bool[::Preferences::Type::save_bug_reports_on_desktop]);
      vbox->add(*type_bool[::Preferences::Type::save_party_changes]);
      vbox->add(*type_bool[::Preferences::Type::additional_party_settings]);
#ifdef USE_SOUND_COMMAND
      { // play sound command
        auto hbox = Gtk::manage(new Gtk::Box());
        hbox->set_spacing(1 EX);
        hbox->set_halign(Gtk::ALIGN_CENTER);
        hbox->add(*type_string_label[::Preferences::Type::play_sound_command]);
        hbox->add(*type_string[::Preferences::Type::play_sound_command]);
        vbox->add(*hbox);
      } // play sound command
#endif
      { // browser
        auto hbox = Gtk::manage(new Gtk::Box());
        hbox->set_spacing(1 EX);
        hbox->set_halign(Gtk::ALIGN_CENTER);
        hbox->add(*type_string_label[::Preferences::Type::browser_command]);
        hbox->add(*type_string[::Preferences::Type::browser_command]);
        vbox->add(*hbox);
      } // browser
    } // General
    { // Kartensortierung
      auto vbox = add_group_box("cards order", "applications-games");
      (void)_("Preferences::Group::cards order");
      cards_order = make_unique<CardsOrder>(*this);
      vbox->add(*cards_order);
    } // Kartensortierung
    { // Kartenverteilung
      auto distribution_box = add_group_box("cards distribution", "shuffle");
      (void)_("Preferences::Group::cards distribution");
      distribution_box->set_halign(Gtk::ALIGN_FILL);
      distribution_box->set_spacing(1 EX);
      auto grid = Gtk::manage(new Gtk::Grid());
      grid->set_halign(Gtk::ALIGN_FILL);
      grid->set_row_homogeneous(true);
      grid->set_column_homogeneous(true);
      auto create_label = [](string const text) {
        auto label = Gtk::manage(new Gtk::Label(text));
        label->set_halign(Gtk::ALIGN_START);
        return label;
      };
      grid->attach(*create_label(" " + _("minimum")), 7, 0, 13, 1);
      grid->attach(*create_label(" " + _("maximum")), 21, 0, 13, 1);
      grid->attach(*create_label(_("color aces") + ":"),   0, 1, 7, 1);
      grid->attach(*create_label(_("blank colors") + ":"), 0, 2, 7, 1);
      grid->attach(*create_label(_("trumps") + ":"),       0, 3, 7, 1);
      grid->attach(*create_label(_("queens") + ":"),       0, 4, 7, 1);
      grid->attach(*create_label(_("jacks") + ":"),        0, 5, 7, 1);
      grid->attach(*create_label(_("club queens") + ":"),  0, 6, 7, 1);
      grid->attach(*create_label(_("heart tens") + ":"),   0, 7, 7, 1);
      grid->attach(*create_label(_("diamond aces") + ":"), 0, 8, 7, 1);

      grid->attach(*type_unsigned[::Preferences::Type::min_color_aces_to_distribute],
                   7, 1,  7, 1);
      grid->attach(*type_unsigned[::Preferences::Type::max_color_aces_to_distribute],
                   21, 1, 7, 1);
      grid->attach(*type_unsigned[::Preferences::Type::min_blank_colors_to_distribute],
                   7, 2,  4, 1);
      grid->attach(*type_unsigned[::Preferences::Type::max_blank_colors_to_distribute],
                   21, 2, 4, 1);
      grid->attach(*type_unsigned[::Preferences::Type::min_trumps_to_distribute],
                   7, 3, 13, 1);
      grid->attach(*type_unsigned[::Preferences::Type::max_trumps_to_distribute],
                   21, 3, 13, 1);
      grid->attach(*type_unsigned[::Preferences::Type::min_queens_to_distribute],
                   7, 4, 9, 1);
      grid->attach(*type_unsigned[::Preferences::Type::max_queens_to_distribute],
                   21, 4, 9, 1);
      grid->attach(*type_unsigned[::Preferences::Type::min_jacks_to_distribute],
                   7, 5, 9, 1);
      grid->attach(*type_unsigned[::Preferences::Type::max_jacks_to_distribute],
                   21, 5, 9, 1);
      grid->attach(*type_unsigned[::Preferences::Type::min_club_queens_to_distribute],
                   7, 6,  3, 1);
      grid->attach(*type_unsigned[::Preferences::Type::max_club_queens_to_distribute],
                   21, 6,  3, 1);
      grid->attach(*type_unsigned[::Preferences::Type::min_heart_tens_to_distribute],
                   7, 7,  3, 1);
      grid->attach(*type_unsigned[::Preferences::Type::max_heart_tens_to_distribute],
                   21, 7,  3, 1);
      grid->attach(*type_unsigned[::Preferences::Type::min_diamond_aces_to_distribute],
                   7, 8,  3, 1);
      grid->attach(*type_unsigned[::Preferences::Type::max_diamond_aces_to_distribute],
                   21, 8,  3,  1);
      distribution_box->add(*grid);

      //type_unsigned[::Preferences::Type::max_attempts_sec_for_distribution]->set_margin_top(2 EX);
      type_unsigned[::Preferences::Type::max_attempts_sec_for_distribution]->set_halign(Gtk::ALIGN_CENTER);
      distribution_box->add(*type_unsigned[::Preferences::Type::max_attempts_sec_for_distribution]);

      type_bool[::Preferences::Type::shuffle_without_seed]->set_halign(Gtk::ALIGN_CENTER);
      distribution_box->add(*type_bool[::Preferences::Type::shuffle_without_seed]);
      type_bool[::Preferences::Type::replayed_game_with_random_distribution_of_players]->set_halign(Gtk::ALIGN_CENTER);
      distribution_box->add(*type_bool[::Preferences::Type::replayed_game_with_random_distribution_of_players]);
    } // Kartenverteilung

    { // Assistance
      auto vbox = add_group_box("assistance", "help-hint");
      (void)_("Preferences::Group::assistance");

      auto grid = Gtk::manage(new Gtk::Grid());
      grid->set_halign(Gtk::ALIGN_FILL);
      grid->set_row_homogeneous(true);
      grid->set_column_homogeneous(true);
      grid->set_row_spacing(1 EX);
      grid->set_column_spacing(2 EM);

      grid->attach(*type_bool[::Preferences::Type::show_soloplayer_in_game],       0, 0, 1, 1);
      grid->attach(*type_bool[::Preferences::Type::announce_swines_automatically], 0, 1, 1, 1);
      grid->attach(*type_bool[::Preferences::Type::show_if_valid],                 0, 2, 1, 1);
      grid->attach(*type_bool[::Preferences::Type::emphasize_valid_cards],         0, 3, 1, 1);
      grid->attach(*type_bool[::Preferences::Type::automatic_card_suggestion],     0, 4, 1, 1);

      grid->attach(*type_bool[::Preferences::Type::show_all_hands],                1, 0, 1, 1);
      grid->attach(*type_bool[::Preferences::Type::show_ai_information_hands],     1, 1, 1, 1);
      grid->attach(*type_bool[::Preferences::Type::show_known_teams_in_game],      1, 2, 1, 1);
      grid->attach(*type_bool[::Preferences::Type::show_ai_information_teams],     1, 3, 1, 1);
      grid->attach(*type_bool[::Preferences::Type::show_trickpiles_points],        1, 4, 1, 1);
      grid->attach(*type_bool[::Preferences::Type::show_all_tricks],               1, 5, 1, 1);

      vbox->add(*grid);
    } // Assistance
    { // handling
      auto subnotebook
        = add_group_notebook("handling", "games-config-options");
      (void)_("Preferences::Group::handling");

      { // playing area
        auto vbox = add_subgroup_box(*subnotebook, "playing area");
        (void)_("Preferences::Subgroup::playing area");

        vbox->add(*type_bool[::Preferences::Type::announce_in_table]);
        vbox->add(*type_unsigned[::Preferences::Type::card_play_delay]);
      } // playing area
      { // gametype window
        auto vbox = add_subgroup_box(*subnotebook,
                                           "gametype");
        (void)_("Preferences::Subgroup::gametype");

        vbox->add(*type_bool[::Preferences::Type::show_gametype_window]);
        vbox->add(*type_bool[::Preferences::Type::close_gametype_window_automatically]);
        vbox->add(*type_unsigned[::Preferences::Type::gametype_window_close_delay]);
      } // gametype window
      { // full trick window
        auto vbox = add_subgroup_box(*subnotebook,
                                           "full trick");
        (void)_("Preferences::Subgroup::full trick");

        vbox->add(*type_bool[::Preferences::Type::show_full_trick_window]);
        vbox->add(*type_bool[::Preferences::Type::show_full_trick_window_if_special_points]);
        vbox->add(*type_bool[::Preferences::Type::close_full_trick_automatically]);
        vbox->add(*type_unsigned[::Preferences::Type::full_trick_close_delay]);
      } // full trick window
      { // marriage finding window
        auto vbox = add_subgroup_box(*subnotebook,
                                           "marriage");
        (void)_("Preferences::Subgroup::marriage");

        vbox->add(*type_bool[::Preferences::Type::show_marriage_window]);
        vbox->add(*type_bool[::Preferences::Type::close_marriage_window_automatically]);
        vbox->add(*type_unsigned[::Preferences::Type::marriage_window_close_delay]);
      } // marriage finding window
      { // announcement window
        auto vbox = add_subgroup_box(*subnotebook,
                                           "announcement");
        (void)_("Preferences::Subgroup::announcement");

        vbox->add(*type_bool[::Preferences::Type::show_announcement_window]);
        vbox->add(*type_bool[::Preferences::Type::close_announcement_window_automatically]);
        vbox->add(*type_unsigned[::Preferences::Type::announcement_window_close_delay]);
      } // announcement window
      { // swines window
        auto vbox = add_subgroup_box(*subnotebook,
                                           "swines");
        (void)_("Preferences::Subgroup::swines");

        vbox->add(*type_bool[::Preferences::Type::show_swines_window]);
        vbox->add(*type_bool[::Preferences::Type::close_swines_window_automatically]);
        vbox->add(*type_unsigned[::Preferences::Type::swines_window_close_delay]);
      } // swines window
    } // handling
    { // Appearance
      auto subnotebook = add_group_notebook("appearance", "games-config-theme");
      (void)_("Preferences::Group::appearance");

      { // cardset
        auto vbox = add_subgroup_box(*subnotebook,
                                           "cardset");
        (void)_("Preferences::Subgroup::cardset");
        cardset = make_unique<Cardset>(*this);
        vbox->set_valign(Gtk::ALIGN_FILL);
        vbox->set_halign(Gtk::ALIGN_FILL);
        vbox->pack_start(*cardset);
      } // cardset
      { // cards back
        auto vbox = add_subgroup_box(*subnotebook,
                                           "cards back");
        (void)_("Preferences::Subgroup::cards back");
        cards_back = make_unique<CardsBack>(*this);
        vbox->set_valign(Gtk::ALIGN_FILL);
        vbox->set_halign(Gtk::ALIGN_FILL);
        vbox->pack_start(*cards_back);
      } // cards back
      { // iconset
        auto vbox = add_subgroup_box(*subnotebook,
                                           "iconset");
        (void)_("Preferences::Subgroup::iconset");
        iconset = make_unique<Iconset>(*this);
        vbox->set_valign(Gtk::ALIGN_FILL);
        vbox->set_halign(Gtk::ALIGN_FILL);
        vbox->pack_start(*iconset);
      } // iconset
      { // background
        auto vbox = add_subgroup_box(*subnotebook,
                                           "background");
        (void)_("Preferences::Subgroup::background");
        background = make_unique<Background>(*this);
        vbox->set_valign(Gtk::ALIGN_FILL);
        vbox->set_halign(Gtk::ALIGN_FILL);
        vbox->pack_start(*background);
      } // background
      { // Fonts and colors
        auto vbox = add_subgroup_box(*subnotebook,
                                           "fonts and colors");
        (void)_("Preferences::Subgroup::fonts and colors");

        { // the name
          auto vbox2 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
          vbox->set_halign(Gtk::ALIGN_CENTER);

          vbox2->add(*type_string[::Preferences::Type::name_font]);
          vbox2->add(*type_string[::Preferences::Type::name_font_color]);
          vbox2->add(*type_string[::Preferences::Type::name_active_font_color]);
          vbox2->add(*type_string[::Preferences::Type::name_reservation_font_color]);
          vbox->add(*vbox2);
        } // the name

        { // the trickpile points
          auto vbox2 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
          vbox->set_halign(Gtk::ALIGN_CENTER);


          vbox2->add(*type_string[::Preferences::Type::trickpile_points_font]);
          vbox2->add(*type_string[::Preferences::Type::trickpile_points_font_color]);

          vbox->add(*vbox2);
        } // the trickpile points

        { // poverty arrow
          vbox->add(*type_string[::Preferences::Type::poverty_shift_arrow_color]);
        } // poverty arrow
      } // Fonts and colors
      { // Splash screen
        auto vbox = add_subgroup_box(*subnotebook,
                                           "splash screen");
        (void)_("Preferences::Subgroup::splash screen");

        vbox->add(*type_bool[::Preferences::Type::show_splash_screen]);
        vbox->add(*type_bool[::Preferences::Type::splash_screen_transparent]);
      } // Splash screen
      { // table
        auto vbox = add_subgroup_box(*subnotebook,
                                           "table");
        (void)_("Preferences::Subgroup::table");

        vbox->add(*type_bool[::Preferences::Type::own_hand_on_table_bottom]);
        vbox->add(*type_bool[::Preferences::Type::static_hands]);
        vbox->add(*type_unsigned[::Preferences::Type::table_rotation]);
        vbox->add(*type_bool[::Preferences::Type::rotate_trick_cards]);
        vbox->add(*type_bool[::Preferences::Type::original_cards_size]);
        vbox->add(*type_unsigned[::Preferences::Type::cards_height]);
      } // table
    } // Appearance
  } // set the pages of the notebook

#ifndef RELEASE
#ifdef DKNOF
  { // test, whether all preferences-buttons are packed in a container
    for (auto t : ::Preferences::Type::bool_list) {
      if (!type_bool[t]->get_parent()
          && (t != ::Preferences::Type::automatic_savings)
          && (t != ::Preferences::Type::ui_main_window_fullscreen)
         ) {
        cerr << "UI_GTKMM::Preferences::Type::Preferences():\n"
          << "  preference '" << to_string(t) << "' not packed\n";
      }
    }
    for (auto t : ::Preferences::Type::unsigned_list) {
      if (!type_unsigned[t]->get_parent()
          && (t != ::Preferences::Type::ui_main_window_pos_x)
          && (t != ::Preferences::Type::ui_main_window_pos_y)
          && (t != ::Preferences::Type::ui_main_window_width)
          && (t != ::Preferences::Type::ui_main_window_height)
         ) {
        cerr << "UI_GTKMM::Preferences::Type::Preferences():\n"
          << "  preference '" << to_string(t)
          << "' not packed\n";
      }
    }
  } // test, whether all preference-buttons are packed in a container
#endif
#endif

  get_content_area()->pack_start(*group_notebook);


  { // signals
    for (auto i : font_chooser) {
      auto selector = i.second;
      selector->signal_response().connect(sigc::bind<Gtk::FontChooserDialog*>(sigc::mem_fun(*this, &Preferences::font_chooser_response), selector));
    } // for (selector : font_chooser)

    for (auto i : color_chooser) {
      auto selector = i.second;
      auto const type
        = *static_cast<::Preferences::Type::String*>(selector->get_data("type"));

      selector->signal_response().connect(sigc::bind<Gtk::ColorChooserDialog*>(sigc::mem_fun(*this, &Preferences::color_chooser_response), selector));
#ifdef TODO
      selector->signal_color_changed().connect(sigc::bind<int>(sigc::mem_fun(*this, &Preferences::changed), type));
#endif
      (void)type;
    } // for (selector : color_chooser)
  } // signals

  update_all();

  show_all_children();

#ifdef RELEASE
#ifndef WINDOWS
#ifndef LINUX
  type_bool[::Preferences::Type::save_bug_reports_on_desktop]->hide();
#endif
#endif
#endif // #ifdef RELEASE
  ::preferences.signal_changed().connect_back([this](auto type, auto value) {update(type);}, disconnector_);
} // void Preferences::init()

/** adds to the group notebook a page with a vbox for group 'name'.
 **
 ** @param    name   name of the group
 **
 ** @return   created vbox for the preferences of the group
 **/
Gtk::Box*
Preferences::add_group_box(string const& name, string const& icon_name)
{
  auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));
  vbox->set_halign(Gtk::ALIGN_CENTER);
  vbox->set_valign(Gtk::ALIGN_CENTER);
  vbox->set_border_width(1 EM);

  add_group(name, icon_name, *vbox);

  return vbox;
} // Gtk::Box* Preferences::add_group_box(string name)

/** adds to the group notebook a subnotebook for group 'name'.
 **
 ** @param    name   name of the group
 **
 ** @return   created notebook for the subgroups
 **/
Gtk::Notebook*
Preferences::add_group_notebook(string const& name, string const& icon_name)
{
  auto notebook = Gtk::manage(new Gtk::Notebook());
  add_group(name, icon_name, *notebook);
  notebook->set_tab_pos(Gtk::POS_LEFT);

  return notebook;
} // Gtk::Notebook* Preferences::add_group_notebook(string name)

/** adds to the group notebook a page of 'widget' for group 'name'.
 **
 ** @param    name     name of the group
 ** @param    widget   widget contianing the group
 **/
void
Preferences::add_group(string const& name, string const& icon_name, Gtk::Widget& widget)
{
  widget.show();

  auto bookmark = Gtk::manage(new Gtk::Box());
  bookmark->set_spacing(1 EM / 2);
  if (!icon_name.empty()) {
    auto const image = ui->create_theme_icon_image(icon_name, 48);
    if (image) {
      bookmark->add(*image);
    }
  }
  auto label = Gtk::manage(new Gtk::Label(_("Preferences::Group::" + name)));
  bookmark->add(*label);
  bookmark->show_all_children();

  group_notebook->append_page(widget, *bookmark);
}

/** adds to the subgroup notebook a page with a vbox for group 'name'.
 **
 ** @param    subnotebook   notebook to add the group
 ** @param    name      name of the group
 **
 ** @return   created vbox for the preferences of the group
 **/
Gtk::Box*
Preferences::add_subgroup_box(Gtk::Notebook& subnotebook,
                              string const& name)
{
  auto label = Gtk::manage(new Gtk::Label(_("Preferences::Subgroup::" + name)));
  label->show();

  auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));
  vbox->set_halign(Gtk::ALIGN_CENTER);
  vbox->set_valign(Gtk::ALIGN_CENTER);
  vbox->set_border_width(1 EX);

  subnotebook.append_page(*vbox, *label);
  group_notebook->child_property_tab_expand(*vbox).set_value(true);
  return vbox;
} // Gtk::Box* Preferences::add_subgroup_box(Gtk::Notebook& subnotebook, string name)

/** creates a backup
 **/
void
Preferences::create_backup()
{
  backup_ = make_unique<::Preferences>(::preferences);
} // void Preferences::create_backup()

/** @return   the backup
 **/
::Preferences const&
Preferences::backup() const
{
  DEBUG_ASSERTION(backup_,
                  "Preferences::Type::backup():\n"
                  "  'backup_' == nullptr");

  return *backup_;
} // Preferences const& Preferences::backup() const

/** save the preferences
 **/
void
Preferences::save()
{
  if (::preferences(::Preferences::Type::automatic_savings))
    ::preferences.save();
} // void Preferences::save()

/** resets the preferences
 **/
void
Preferences::reset()
{
  if (backup_)
    ::preferences = *backup_;
} // void Preferences::reset()

/** update the sensitivity of all widgets
 **/
void
Preferences::sensitivity_update()
{
  for (auto i : type_bool) {
    auto widget = i.second;
    widget->set_sensitive(::preferences.dependencies(*static_cast<::Preferences::Type::Bool*>(widget->get_data("type"))));
  }
  for (auto i : type_unsigned) {
    auto widget = i.second;
    auto const type
      = *static_cast<::Preferences::Type::Unsigned*>(widget->get_data("type"));

    widget->set_sensitive(::preferences.dependencies(type));
    if (type == ::Preferences::Type::cards_height) {
      // make such a limit, so that a right-click (maximum) does not lead
      // to a limit problem
      dynamic_cast<Gtk::LabelSpinButton*>(widget)->set_range(::preferences.min(type),
                                                             10 * ui->cards->height_original());
    } else {
      if (dynamic_cast<Gtk::LabelSpinButton*>(widget)) {
        auto button = dynamic_cast<Gtk::LabelSpinButton*>(widget);
        button->set_range(::preferences.min(type), ::preferences.max(type));
      } else if (dynamic_cast<Gtk::Range*>(widget)) {
        auto range = dynamic_cast<Gtk::Range*>(widget);
        range->set_range(::preferences.min(type), ::preferences.max(type));
      }
    }
  } // for (widget : type_unsigned)
} // void Preferences::sensitivity_update()

/** update all
 **/
void
Preferences::update_all()
{
  for (auto t : ::Preferences::Type::bool_list)
    update(t, false);
  for (auto t : ::Preferences::Type::unsigned_list)
    update(t, false);
  for (auto t : ::Preferences::Type::string_list)
    update(t, false);
  update(::Preferences::Type::cards_order, false);

  sensitivity_update();

  reset_button->set_sensitive(backup() != ::preferences);
} // void Preferences::update_all()

/** update the preference 'type'
 **
 ** @param    type      the type of the preference
 ** @param    update_sensitivity   whether the sensitivity shall be updated
 **    (default: true)
 **
 ** @todo   style of the font and color buttons
 **/
void
Preferences::update(int const typex, bool const update_sensitivity)
{
  if (!is_visible())
    return ;

  if (update_sensitivity)
    sensitivity_update();

  if (contains(::Preferences::Type::bool_list, typex)) {
    auto const type = static_cast<::Preferences::Type::Bool>(typex);
    type_bool[type]->set_active(::preferences.value(type));
    switch (type) {
    case ::Preferences::Type::emphasize_valid_cards:
      ui->main_window->menu->emphasize_valid_cards->set_active(::preferences.value(type));
      break;
    case ::Preferences::Type::show_all_hands:
      ui->main_window->menu->show_all_hands->set_active(::preferences.value(type));
      break;
    default:
      break;
    } // switch(type)
  } else if (contains(::Preferences::Type::unsigned_list, typex)) {
    auto const type = static_cast<::Preferences::Type::Unsigned>(typex);
    switch (type) {
    case ::Preferences::Type::max_attempts_sec_for_distribution: {
      auto button = dynamic_cast<Gtk::LabelSpinButton*>(type_unsigned[type]);
      button->set_value(::preferences.value(type));
      break;
    }
    case ::Preferences::Type::min_trumps_to_distribute:
    case ::Preferences::Type::max_trumps_to_distribute:
    case ::Preferences::Type::min_queens_to_distribute:
    case ::Preferences::Type::max_queens_to_distribute:
    case ::Preferences::Type::min_jacks_to_distribute:
    case ::Preferences::Type::max_jacks_to_distribute:
    case ::Preferences::Type::min_club_queens_to_distribute:
    case ::Preferences::Type::max_club_queens_to_distribute:
    case ::Preferences::Type::min_heart_tens_to_distribute:
    case ::Preferences::Type::max_heart_tens_to_distribute:
    case ::Preferences::Type::min_diamond_aces_to_distribute:
    case ::Preferences::Type::max_diamond_aces_to_distribute:
    case ::Preferences::Type::min_color_aces_to_distribute:
    case ::Preferences::Type::max_color_aces_to_distribute:
    case ::Preferences::Type::min_blank_colors_to_distribute:
    case ::Preferences::Type::max_blank_colors_to_distribute: {
      auto scale = dynamic_cast<Gtk::Scale*>(type_unsigned[type]);
      scale->set_value(::preferences.value(type));
      break;
    }
    case ::Preferences::Type::card_play_delay:
    case ::Preferences::Type::full_trick_close_delay:
    case ::Preferences::Type::gametype_window_close_delay:
    case ::Preferences::Type::marriage_window_close_delay:
    case ::Preferences::Type::announcement_window_close_delay:
    case ::Preferences::Type::swines_window_close_delay: {
      auto button = dynamic_cast<Gtk::LabelSpinButton*>(type_unsigned[type]);
      // make one second to correspond with '1.0'
      button->set_value((1.0 / 1000) * ::preferences.value(type));
      break;
    }
    case ::Preferences::Type::table_rotation:
    case ::Preferences::Type::cards_height: {
      auto button = dynamic_cast<Gtk::LabelSpinButton*>(type_unsigned[type]);
      button->set_value(::preferences.value(type));
      break;
    }
    case ::Preferences::Type::ui_main_window_pos_x:
    case ::Preferences::Type::ui_main_window_pos_y:
    case ::Preferences::Type::ui_main_window_width:
    case ::Preferences::Type::ui_main_window_height:
      break;
    } // switch(type)
  } else if (contains(::Preferences::Type::string_list, typex)) {
    auto const type = static_cast<::Preferences::Type::String>(typex);
    switch (type) {
    case ::Preferences::Type::name:
      dynamic_cast<Gtk::Entry*>(type_string[type])->set_text(::preferences(type));
      break;
    case ::Preferences::Type::language: {
      language_menu_create(false);
      auto button = dynamic_cast<Gtk::Button*>(type_string[type]);
      if (::preferences.value(type).empty()) {
        button->set_label(_("default"));
      } else if (::preferences.value(type) == "de") {
        button->set_label("deutsch");
      } else if (::preferences.value(type) == "de-alt") {
        button->set_label("deutsch – alt");
      } else if (::preferences.value(type) == "en") {
        button->set_label("english");
      } else  {
        button->set_label(::preferences.value(type));
      }
      break;
    }
    case ::Preferences::Type::cardset:
    case ::Preferences::Type::cards_back:
    case ::Preferences::Type::iconset:
    case ::Preferences::Type::background:
      break;
    case ::Preferences::Type::name_font:
    case ::Preferences::Type::trickpile_points_font:
      static_cast<Gtk::FontChooserDialog*>(type_string[type]->get_data("font selector"))->set_font(::preferences(type));
#ifdef POSTPONED
      // ToDo
      // don't work :-(
      { // change the style
        auto style = type_string[type]->get_style();

        style->set_font(Pango::FontDescription(::preferences(type)));

        type_string[type]->set_style(style);
      } // change the style
#endif // #ifdef POSTPONED
      break;

    case ::Preferences::Type::name_font_color:
    case ::Preferences::Type::name_active_font_color:
    case ::Preferences::Type::name_reservation_font_color:
    case ::Preferences::Type::trickpile_points_font_color:
    case ::Preferences::Type::poverty_shift_arrow_color:
      static_cast<Gtk::ColorChooserDialog*>(type_string[type]->get_data("color selector"))->set_rgba(Gdk::RGBA(::preferences(type)));
#ifdef POSTPONED
      // change the color of the text in the buttons
      auto style
        = type_string[type]->get_style();
      // = static_cast<Gtk::Button*>(type_string[type])->get_child()->get_style();

      Gdk::RGBA color(::preferences(type));
      ui->colormap->alloc_color(color);
      style->set_fg(Gtk::STATE_normal, color);
      style->set_text(Gtk::STATE_normal, color);
      style->set_text_aa(Gtk::STATE_normal, color);
      style->set_base(Gtk::STATE_normal, color);

      type_string[type]->set_style(style);
      static_cast<Gtk::Button*>(type_string[type])->get_child()->set_style(style);
#endif // #ifdef POSTPONED
      break;

#ifdef USE_SOUND_COMMAND
    case ::Preferences::Type::play_sound_command:
#endif
    case ::Preferences::Type::browser_command:
      dynamic_cast<Gtk::Entry*>(type_string[type])->set_text(::preferences(type));
      break;
    case ::Preferences::Type::cardset_license:
    case ::Preferences::Type::iconset_license:
      break;
    } // switch(type)

  } else if (typex == ::Preferences::Type::cards_order) {
    cards_order->update();
  } else {
    DEBUG_ASSERTION(false,
                    "Preferences::update(type, update_sensitivity):\n"
                    "  type '" << typex << "' unknown.");
  }

  if (update_sensitivity)
    reset_button->set_sensitive(backup() != ::preferences);
} // void Preferences::update(int type, bool update_sensitivity = true)

/** a preference has been changed by the user
 **
 ** @param    type   the type of the preference
 **/
void
Preferences::changed(int const typex)
{
  if (contains(::Preferences::Type::bool_list, typex)) {
    auto const type = static_cast<::Preferences::Type::Bool>(typex);
    ::preferences.set(type,
                      type_bool[type]->get_active());
  } else if (contains(::Preferences::Type::unsigned_list, typex)) {
    auto const type = static_cast<::Preferences::Type::Unsigned>(typex);
    if (type_unsigned[type]->get_realized()) {
      switch (type) {
      case ::Preferences::Type::max_attempts_sec_for_distribution: {
        auto button = dynamic_cast<Gtk::LabelSpinButton*>(type_unsigned[type]);
        ::preferences.set(type, button->get_value_as_int());
        break;
      }
      case ::Preferences::Type::min_trumps_to_distribute:
      case ::Preferences::Type::max_trumps_to_distribute:
      case ::Preferences::Type::min_queens_to_distribute:
      case ::Preferences::Type::max_queens_to_distribute:
      case ::Preferences::Type::min_jacks_to_distribute:
      case ::Preferences::Type::max_jacks_to_distribute:
      case ::Preferences::Type::min_club_queens_to_distribute:
      case ::Preferences::Type::max_club_queens_to_distribute:
      case ::Preferences::Type::min_heart_tens_to_distribute:
      case ::Preferences::Type::max_heart_tens_to_distribute:
      case ::Preferences::Type::min_diamond_aces_to_distribute:
      case ::Preferences::Type::max_diamond_aces_to_distribute:
      case ::Preferences::Type::min_color_aces_to_distribute:
      case ::Preferences::Type::max_color_aces_to_distribute:
      case ::Preferences::Type::min_blank_colors_to_distribute:
      case ::Preferences::Type::max_blank_colors_to_distribute: {
        auto scale = dynamic_cast<Gtk::Scale*>(type_unsigned[type]);
        auto const value = static_cast<unsigned>(scale->get_value());
        ::preferences.set(type, value);

        if (   type == ::Preferences::Type::min_trumps_to_distribute
            && value > preferences(::Preferences::Type::max_trumps_to_distribute))
          preferences.set(::Preferences::Type::max_trumps_to_distribute, value);
        if (   type == ::Preferences::Type::max_trumps_to_distribute
            && value < preferences(::Preferences::Type::min_trumps_to_distribute))
          preferences.set(::Preferences::Type::min_trumps_to_distribute, value);
        if (   type == ::Preferences::Type::min_queens_to_distribute
            && value > preferences(::Preferences::Type::max_queens_to_distribute))
          preferences.set(::Preferences::Type::max_queens_to_distribute, value);
        if (   type == ::Preferences::Type::max_queens_to_distribute
            && value < preferences(::Preferences::Type::min_queens_to_distribute))
          preferences.set(::Preferences::Type::min_queens_to_distribute, value);
        if (   type == ::Preferences::Type::min_jacks_to_distribute
            && value > preferences(::Preferences::Type::max_jacks_to_distribute))
          preferences.set(::Preferences::Type::max_jacks_to_distribute, value);
        if (   type == ::Preferences::Type::max_jacks_to_distribute
            && value < preferences(::Preferences::Type::min_jacks_to_distribute))
          preferences.set(::Preferences::Type::min_jacks_to_distribute, value);
        if (   type == ::Preferences::Type::min_club_queens_to_distribute
            && value > preferences(::Preferences::Type::max_club_queens_to_distribute))
          preferences.set(::Preferences::Type::max_club_queens_to_distribute, value);
        if (   type == ::Preferences::Type::min_club_queens_to_distribute
            && value > preferences(::Preferences::Type::max_club_queens_to_distribute))
          preferences.set(::Preferences::Type::max_club_queens_to_distribute, value);
        if (   type == ::Preferences::Type::max_club_queens_to_distribute
            && value < preferences(::Preferences::Type::min_club_queens_to_distribute))
          preferences.set(::Preferences::Type::min_club_queens_to_distribute, value);
        if (   type == ::Preferences::Type::min_heart_tens_to_distribute
            && value > preferences(::Preferences::Type::max_heart_tens_to_distribute))
          preferences.set(::Preferences::Type::max_heart_tens_to_distribute, value);
        if (   type == ::Preferences::Type::max_heart_tens_to_distribute
            && value < preferences(::Preferences::Type::min_heart_tens_to_distribute))
          preferences.set(::Preferences::Type::min_heart_tens_to_distribute, value);
        if (   type == ::Preferences::Type::min_diamond_aces_to_distribute
            && value > preferences(::Preferences::Type::max_diamond_aces_to_distribute))
          preferences.set(::Preferences::Type::max_diamond_aces_to_distribute, value);
        if (   type == ::Preferences::Type::max_diamond_aces_to_distribute
            && value < preferences(::Preferences::Type::min_diamond_aces_to_distribute))
          preferences.set(::Preferences::Type::min_diamond_aces_to_distribute, value);
        if (   type == ::Preferences::Type::min_color_aces_to_distribute
            && value > preferences(::Preferences::Type::max_color_aces_to_distribute))
          preferences.set(::Preferences::Type::max_color_aces_to_distribute, value);
        if (   type == ::Preferences::Type::max_color_aces_to_distribute
            && value < preferences(::Preferences::Type::min_color_aces_to_distribute))
          preferences.set(::Preferences::Type::min_color_aces_to_distribute, value);
        if (   type == ::Preferences::Type::min_blank_colors_to_distribute
            && value > preferences(::Preferences::Type::max_blank_colors_to_distribute))
          preferences.set(::Preferences::Type::max_blank_colors_to_distribute, value);
        if (   type == ::Preferences::Type::max_blank_colors_to_distribute
            && value < preferences(::Preferences::Type::min_blank_colors_to_distribute))
          preferences.set(::Preferences::Type::min_blank_colors_to_distribute, value);
        break;
      }

      case ::Preferences::Type::card_play_delay:
      case ::Preferences::Type::full_trick_close_delay:
      case ::Preferences::Type::gametype_window_close_delay:
      case ::Preferences::Type::marriage_window_close_delay:
      case ::Preferences::Type::announcement_window_close_delay:
      case ::Preferences::Type::swines_window_close_delay: {
        auto button = dynamic_cast<Gtk::LabelSpinButton*>(type_unsigned[type]);
        // make one second to correspond with '1.0'
        ::preferences.set(type, static_cast<int>(1000 * button->get_value()));
        break;
      }
      case ::Preferences::Type::table_rotation:
      case ::Preferences::Type::cards_height: {
        auto button = dynamic_cast<Gtk::LabelSpinButton*>(type_unsigned[type]);
        ::preferences.set(type, button->get_value_as_int());
        break;
      }
      case ::Preferences::Type::ui_main_window_pos_x:
      case ::Preferences::Type::ui_main_window_pos_y:
      case ::Preferences::Type::ui_main_window_width:
      case ::Preferences::Type::ui_main_window_height:
        DEBUG_ASSERTION(false,
                        "Preferences type '" + to_string(type) + " cannot be changed manually\n");
        break;
      } // switch(type)
    } // if (type is realized)
  } else if (contains(::Preferences::Type::string_list, typex)) {
    auto const type = static_cast<::Preferences::Type::String>(typex);
    switch (type) {
    case ::Preferences::Type::name:
      ::preferences.set(type,
                        dynamic_cast<Gtk::Entry*>(type_string[type])->get_text());
      break;
    case ::Preferences::Type::language:
      break;
    case ::Preferences::Type::cardset:
      // -> Cardset
      break;
    case ::Preferences::Type::cards_back:
      // -> CardsBack
      break;
    case ::Preferences::Type::iconset:
      // -> Iconset
      break;
    case ::Preferences::Type::background:
      // -> Background
      break;
    case ::Preferences::Type::name_font:
    case ::Preferences::Type::trickpile_points_font:
      ::preferences.set(type,
                        static_cast<Gtk::FontChooserDialog*>(type_string[type]->get_data("font selector"))->get_font());
      break;
    case ::Preferences::Type::name_font_color:
    case ::Preferences::Type::name_active_font_color:
    case ::Preferences::Type::name_reservation_font_color:
    case ::Preferences::Type::trickpile_points_font_color:
    case ::Preferences::Type::poverty_shift_arrow_color:
      ::preferences.set(type,
                        colorname(static_cast<Gtk::ColorChooserDialog*>(type_string[type]->get_data("color selector"))->get_rgba()));
      break;

#ifdef USE_SOUND_COMMAND
    case ::Preferences::Type::play_sound_command:
#endif
    case ::Preferences::Type::browser_command:
      ::preferences.set(type,
                        dynamic_cast<Gtk::Entry*>(type_string[type])->get_text());
      break;
    case ::Preferences::Type::cardset_license:
    case ::Preferences::Type::iconset_license:
      break;
    } // switch(type)
  } // if (type == ())
  // cards order is managed in 'preferences/cards_order.cpp'
} // void Preferences::changed(int type)

/** a preference has been changed by the user
 **
 ** @param    focus   ignored
 ** @param    type    the type of the preference
 **/
bool
Preferences::focus_out_event(GdkEventFocus* event, int const type)
{
  changed(type);

  return true;
} // bool Preferences::focus_out_event(GdkEventFocus* event, int type)

/** open the cards order
 **/
void
Preferences::open_cards_order()
{
  realize();
  group_notebook->set_current_page(1);
  present();
} // void Preferences::open_cards_order()

/** create and show the language menu
 **
 ** @param    popup   whether to popup the menu (default: true)
 **/
void
Preferences::language_menu_create(bool const popup)
{
  { // remove all children
    auto children = language_menu->get_children();

    if (!children.empty()) {
      for (auto child : children)
        language_menu->remove(*child);
    } // if (!children.empty())
  } // remove all children

  // the language menu consists of the default button, a separator and
  // the languages (taken from '::language'
  Gtk::MenuItem* item = nullptr;

  // memory leak: the translation created in 'Ui::->translations->add' is not removed
  item = Gtk::manage(new Gtk::MenuItem(_("default")));
  item->signal_activate().connect(sigc::bind<string>(sigc::mem_fun(*this, &Preferences::language_selected), ""));
  language_menu->add(*item);
  item = Gtk::manage(new Gtk::SeparatorMenuItem());
  language_menu->add(*item);

  vector<std::pair<string, string>> const names
    = {{"deutsch", "de"},
      {"deutsch - alt", "de-alt"},
      {"english", "en"}};
  for (auto const& name : names) {
    item = Gtk::manage(new Gtk::MenuItem(name.first));
    item->signal_activate().connect(sigc::bind<string>(sigc::mem_fun(*this, &Preferences::language_selected), name.second));
    language_menu->add(*item);
  } // for (name : names)

  if (popup)
    language_menu->popup(0, 0);

  language_menu->show_all();
} // void Preferences::language_menu_create(bool popup = true)

/** change of the language
 **
 ** @param    language   new language
 **/
void
Preferences::language_selected(string const language)
{
  ::preferences.set(::Preferences::Type::language, language);
} // void Preferences::language_selected(string language)

/** change of the cardset
 **
 ** @param    cardset   new cardset
 **/
void
Preferences::cardset_selected(string const cardset)
{
  ::preferences.set(::Preferences::Type::cardset, cardset);
} // void Preferences::cardset_selected(string cardset)

/** the font chooser has emmited a response
 **
 ** @param    response_id     id of the response
 ** @param    font_chooser   the font selector
 **/
void
Preferences::font_chooser_response(int const response_id,
                                   Gtk::FontChooserDialog* const font_chooser)
{
  if (response_id == Gtk::RESPONSE_OK) {
    auto const type
      = *static_cast<::Preferences::Type::String*>(font_chooser->get_data("type"));
    font_chooser->hide();
    changed(type);
  } else {
    font_chooser->hide();
  }
} // void Preferences::font_chooser_response(int response_id, Gtk::FontChooserDialog* font_chooser)

/** the color of the preference 'type' has emmited a response
 **
 ** @param    response_id     id of the response
 ** @param    color_chooser   the color selector
 **/
void
Preferences::color_chooser_response(int const response_id,
                                    Gtk::ColorChooserDialog* const color_chooser)
{
  if (response_id == Gtk::RESPONSE_OK) {
    auto const type
      = *static_cast<::Preferences::Type::String*>(color_chooser->get_data("type"));
    //color_chooser->set_rgba(Gdk::RGBA(::preferences(type)));
    //::preferences.set(type, color_chooser->get_rgba().to_string());
    color_chooser->hide();
    changed(type);
  } else {
    color_chooser->hide();
  }
} // void Preferences::color_chooser_response(int response_id, Gtk::ColorChooserDialog* color_chooser)

/** a key has been pressed
 ** C-o: output of the preferences on 'stdout'
 **
 ** @param    key   the key
 **
 ** @return   whether the key was managed
 **/
bool
Preferences::on_key_press_event(GdkEventKey* key)
{
  bool managed = false;

  if ((key->state & ~GDK_SHIFT_MASK) == GDK_CONTROL_MASK) {
    switch (key->keyval) {
    case GDK_KEY_o: // ouput of the preferences
      cout << ::preferences;
      managed = true;
      break;
    } // switch (key->keyval)
  } // if (key->state == GDK_CONTROL_MASK)

  return (managed
          || StickyDialog::on_key_press_event(key)
          || ui->key_press(key));
} // bool Preferences::on_key_press_event(GdkEventKey* key)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
