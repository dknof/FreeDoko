/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "cards_order.h"

#include "../ui.h"
#include "../cards.h"

#include <gtkmm/radiobutton.h>
#include <gtkmm/grid.h>
#include <gtkmm/box.h>
#include <gtkmm/arrow.h>
#include <gtkmm/image.h>

namespace UI_GTKMM_NS {
using Direction = ::Preferences::CardsOrder::Direction;

/** constructor
 **/
Preferences::CardsOrder::CardsOrder(Preferences& preferences) :
  Base(&preferences),
  preferences_(preferences)
{
  init();
}

/** destructor
 **/
Preferences::CardsOrder::~CardsOrder()
  = default;

/** use the cards order as a widget
 **/
Preferences::CardsOrder::operator Gtk::Widget&()
{
  return *container;
}

/** creates all subelements for the cardsorder
 **/
void
Preferences::CardsOrder::init()
{
  auto const& cards_order = ::preferences(::Preferences::Type::cards_order);
  container = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 1 EX));
  { // sorted/unsorted
    auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
    vbox->set_halign(Gtk::ALIGN_CENTER);

    Gtk::RadioButton::Group group;

    sorted_button = Gtk::manage(new Gtk::RadioButton(group, _("Button::sorted")));
    vbox->add(*sorted_button);

    unsorted_button = Gtk::manage(new Gtk::RadioButton(group, _("Button::unsorted")));
    vbox->add(*unsorted_button);

    if (cards_order.mixed())
      unsorted_button->set_active();
    else
      sorted_button->set_active();

    sorted_button->signal_toggled().connect(sigc::mem_fun(*this, &CardsOrder::set_sorted));
    unsorted_button->signal_toggled().connect(sigc::mem_fun(*this, &CardsOrder::set_sorted));
    container->add(*vbox);
  } // sorted/unsorted

  { // the cards
    cards_container = Gtk::manage(new Gtk::Grid());
    auto grid = cards_container;
    container->add(*grid);
    grid->set_row_spacing(1 EX);
    grid->set_column_spacing(1 EM);

    for (unsigned p = 0; p < Card::number_of_tcolors; ++p) {
      { // left/right button
        auto hbox = Gtk::manage(new Gtk::Box());
        hbox->set_homogeneous();
        grid->attach(*hbox, p, 0, 1, 1);

        auto left_button = Gtk::manage(new Gtk::Button());
        left_button->add(*Gtk::manage(new Gtk::Arrow(Gtk::ARROW_LEFT,
                                                     Gtk::SHADOW_OUT)));
        hbox->add(*left_button);
        left_button->signal_clicked().connect(sigc::bind<unsigned const>(sigc::mem_fun(*this, &CardsOrder::pos_to_left), p));

        auto right_button = Gtk::manage(new Gtk::Button());
        right_button->add(*Gtk::manage(new Gtk::Arrow(Gtk::ARROW_RIGHT,
                                                      Gtk::SHADOW_OUT)));
        hbox->add(*right_button);
        right_button->signal_clicked().connect(sigc::bind<unsigned const>(sigc::mem_fun(*this, &CardsOrder::pos_to_right), p));
      } // left/right button
      { // the image
        if (cards_order.tcolor(p) == Card::trump) {
          sorting_tcolor_image.push_back(ui->cards->new_managed_image(Card::club_queen));
        } else {
          sorting_tcolor_image.push_back(ui->cards->new_managed_image(Card(cards_order.tcolor(p), Card::ace)));
        }
        grid->attach(*sorting_tcolor_image.back(), p, 1, 1, 1);
      } // the image
      { // up/down buttons
        auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
        grid->attach(*vbox, p, 2, 1, 1);

        Gtk::RadioButton::Group group;
        sorting_up_button.push_back(Gtk::manage(new Gtk::RadioButton(group, _(Direction::up))));
        vbox->add(*(sorting_up_button.back()));

        sorting_down_button.push_back(Gtk::manage(new Gtk::RadioButton(group, _(Direction::down))));
        vbox->add(*(sorting_down_button.back()));

        // events
        sorting_up_button.back()->set_active(cards_order.direction(p) == Direction::up);
        sorting_down_button.back()->set_active(cards_order.direction(p) == Direction::down);
        sorting_up_button.back()->signal_toggled().connect(sigc::bind<unsigned const>(sigc::mem_fun(*this, &CardsOrder::set_direction), p));
        sorting_down_button.back()->signal_toggled().connect(sigc::bind<unsigned const>(sigc::mem_fun(*this, &CardsOrder::set_direction), p));
      } // up/down buttons
    } // for (c < Card::number_of_tcolors)
  } // the cards
}

/** update the cards order
 **/
void
Preferences::CardsOrder::update()
{
  auto& cards_order = ::preferences(::Preferences::Type::cards_order);
  if (cards_order.mixed()) {
    unsorted_button->set_active();
    cards_container->set_sensitive(false);
  } else {
    sorted_button->set_active();
    cards_container->set_sensitive(true);

    for (unsigned p = 0; p < Card::number_of_tcolors; ++p) {
      if (cards_order.tcolor(p) == Card::trump)
        ui->cards->change_managed(sorting_tcolor_image[p],
                                        Card::club_queen);
      else
        ui->cards->change_managed(sorting_tcolor_image[p],
                                        Card(cards_order.tcolor(p), Card::ace));

      if (cards_order.direction(p)
          == Direction::up)
        sorting_up_button[p]->set_active();
      else
        sorting_down_button[p]->set_active();

    } // for (n < Card::number_of_tcolors)
  } // if !(::preferences(::Preferences::Type::cards_order).mixed())
}

/** event: set the sorting direction
 **
 ** @param    pos   the position which is changed
 **/
void
Preferences::CardsOrder::set_direction(unsigned const pos)
{
  auto& cards_order = ::preferences(::Preferences::Type::cards_order);
  cards_order.sorted_set(true);
  if (sorting_up_button[pos]->get_active())
    cards_order.direction_set(pos, Direction::up);
  else
    cards_order.direction_set(pos, Direction::down);
}

/** event: shift the type at 'pos' one position to the left
 **
 ** @param    pos   the position which is shifted
 **/
void
Preferences::CardsOrder::pos_to_left(unsigned const pos)
{
  auto& cards_order = ::preferences(::Preferences::Type::cards_order);
  cards_order.sorted_set(true);
  cards_order.pos_to_left(pos);
}

/** event: shift the type at 'pos' one position to the right
 **
 ** @param    pos   the position which is shifted
 **/
void
Preferences::CardsOrder::pos_to_right(unsigned const pos)
{
  auto& cards_order = ::preferences(::Preferences::Type::cards_order);
  cards_order.sorted_set(true);
  cards_order.pos_to_right(pos);
}

/** event: set the sorted state
 **/
void
Preferences::CardsOrder::set_sorted()
{
  auto& cards_order = ::preferences(::Preferences::Type::cards_order);
  cards_order.sorted_set(sorted_button->get_active());
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
