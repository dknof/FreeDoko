/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "../preferences.h"

namespace UI_GTKMM_NS {

/** cards order preferences
 **/
class Preferences::CardsOrder : public Base {
  public:
    explicit CardsOrder(Preferences& preferences);
    ~CardsOrder() override;

    operator Gtk::Widget&();

    void sensitivity_update();
    void update();
    void has_changed();

    void set_direction(::Preferences::CardsOrder::Position pos);
    void pos_to_left(::Preferences::CardsOrder::Position pos);
    void pos_to_right(::Preferences::CardsOrder::Position pos);
    void set_sorted();

  private:
    void init();

  private:
    Preferences& preferences_;
    Gtk::Box* container = nullptr;
    Gtk::Grid* cards_container = nullptr;
    vector<Gtk::Image*> sorting_tcolor_image;
    vector<Gtk::RadioButton*> sorting_up_button;
    vector<Gtk::RadioButton*> sorting_down_button;
    Gtk::RadioButton* sorted_button = nullptr;
    Gtk::RadioButton* unsorted_button = nullptr;
};

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
