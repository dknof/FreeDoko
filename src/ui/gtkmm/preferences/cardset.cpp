/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "cardset.h"

#include "../ui.h"
#include "../cards.h"

#include "../../../utils/file.h"

#include <gtkmm/scrolledwindow.h>
#include <gtkmm/flowbox.h>
#include <gtkmm/image.h>
#include <gtkmm/box.h>
#include <regex>

namespace UI_GTKMM_NS {

namespace {
vector<string> const cards_list_freedoko = {
  "cards/club_ace.png",
  "cards/heart_king.png",
  "cards/spade_queen.png",
  "cards/diamond_jack.png",
  "cards/club_ten.png",
  "cards/heart_nine.png",
};
vector<string> const cards_list_pysol = {
  "01c.gif", // club ace
  "13h.gif", // heart king
  "12s.gif", // spade queen
  "11d.gif", // diamond jack
  "10c.gif", // club ten
  "09h.gif", // heart nine
};
} // namespace

/** constructor
 **/
Preferences::Cardset::Cardset(Preferences& preferences) :
  Selection(preferences, ::Preferences::Type::cardset)
{ }

/** destructor
 **/
Preferences::Cardset::~Cardset()
  = default;

/** creates all subelements for the cardsorder
 **/
void
Preferences::Cardset::init()
{
  Selection::init();
  container->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
  box->set_max_children_per_line(1);
}

/** @return   vector of entries
 **/
auto
Preferences::Cardset::entries_list() const
-> vector<Entry>
{
  auto entries = Selection::entries_list();
  auto const entries_pysol = entries_pysol_list();
  entries.insert(entries.end(), entries_pysol.begin(), entries_pysol.end());
  return entries;
}
/** @return   list of all backs (graphic files) in the (backs)-directory
 **
 ** @todo   Unterverzeichnisse unterstützen
 **/
auto
Preferences::Cardset::entries_pysol_list() const
-> vector<Entry>
{
  vector<Entry> entries_list;
  for (auto const& directory : ::preferences.directories(::Preferences::Type::cardset)) {
    if (!std::filesystem::is_directory(directory))
      continue;
    for (auto const& entry : std::filesystem::directory_iterator(directory)) {
      if (!std::regex_search(entry.path().string(), std::regex("/cardset-[^/]*")))
        continue;
      if (!std::filesystem::is_regular_file(entry.path() / "12c.gif"))
        continue;
      auto const cardset = string(entry.path().filename().string(), strlen("cardset-"));
      entries_list.emplace_back(cardset, entry.path().string());
    }
  }
  return entries_list;
}

/** @return   whether the path is a valid path for a cardset
 **/
bool
Preferences::Cardset::path_valid(std::filesystem::path const path) const
{
  return (   ::preferences.cardset_format(path) != CardsetFormat::unknown
          && !::preferences.cardset_license_expired(path));
}

/** add the images to the box
 **/
Gtk::Widget*
Preferences::Cardset::create_entry(Entry entry)
{
  auto const format = ::preferences.cardset_format(entry.path);
  if (format == CardsetFormat::unknown)
    return {};

  auto const cards_list
    = (format == CardsetFormat::freedoko
       ? vector<string>(cards_list_freedoko)
       : format == CardsetFormat::pysol
       ? vector<string>(cards_list_pysol)
       : vector<string>{});

  auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
  vbox->set_margin_top(1 EX);
  vbox->set_margin_bottom(1 EX);
  {
    auto hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL));
    hbox->set_spacing(1 EM);
    for (auto const& card : cards_list) {
      try {
        auto pixbuf = Gdk::Pixbuf::create_from_file((entry.path / card).string());
        auto image = Gtk::manage(new Gtk::Image(pixbuf));
        hbox->add(*image);
      } catch (...) {
      }
    }
    vbox->add(*hbox);
  }
  {
    auto label = Gtk::manage(new Gtk::Label(entry.name));
    label->set_halign(Gtk::ALIGN_START);
    label->set_margin_top(0.5 EX);
    vbox->add(*label);
  }
  vbox->show_all_children();
  return vbox;
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
