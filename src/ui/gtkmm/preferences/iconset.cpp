/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "iconset.h"

#include "../ui.h"
#include "../cards.h"

#include <gtkmm/scrolledwindow.h>
#include <gtkmm/flowbox.h>
#include <gtkmm/image.h>
#include <gtkmm/box.h>

namespace UI_GTKMM_NS {

namespace {
std::string const icons_list[] = {
  "re.png"s,
  "contra.png"s,
  "marriage.png"s,
  "poverty.png"s,
  "swines.png"s,
};
} // namespace

/** constructor
 **/
Preferences::Iconset::Iconset(Preferences& preferences) :
  Selection(preferences, ::Preferences::Type::iconset)
{ }

/** destructor
 **/
Preferences::Iconset::~Iconset()
  = default;

/** creates all subelements for the cardsorder
 **/
void
Preferences::Iconset::init()
{
  Selection::init();
  container->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
  box->set_max_children_per_line(1);
}

/** @return   list of all iconsets
 **/
bool
Preferences::Iconset::path_valid(std::filesystem::path path) const
{
  return (   is_directory(path)
          && is_regular_file(path / "License.txt"));
}

/** add the images to the box
 **/
Gtk::Widget*
Preferences::Iconset::create_entry(Entry entry)
{
  if (!path_valid(entry.path))
    return {};

  auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
  vbox->set_margin_top(1 EX);
  vbox->set_margin_bottom(1 EX);
  auto hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL));
  for (auto const& icon : icons_list) {
    try {
      auto pixbuf = Gdk::Pixbuf::create_from_file((entry.path / icon).string());
      auto image = Gtk::manage(new Gtk::Image(pixbuf));
      hbox->add(*image);
    } catch (...) {
    }
  }
  if (hbox->get_children().empty()) {
    auto label = Gtk::manage(new Gtk::Label(entry.name));
    label->set_markup(R"(<span weight="bold" size="xx-large">)" + entry.name + "</span>");
    label->set_halign(Gtk::ALIGN_START);
    label->set_margin_top(1 EX);
    label->set_margin_bottom(1 EX);
    vbox->add(*label);
  } else {
    vbox->add(*hbox);
    auto label = Gtk::manage(new Gtk::Label(entry.name));
    label->set_halign(Gtk::ALIGN_START);
    label->set_margin_top(0.5 EX);
    vbox->add(*label);
  }
  vbox->show_all_children();
  return vbox;
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
