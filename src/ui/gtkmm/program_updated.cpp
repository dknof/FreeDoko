/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "program_updated.h"

#include "ui.h"

#include "../../versions.h"
#include "../../party/party.h"
#include "../../player/player.h"
#include "../../player/aiconfig.h"

#include <gtkmm/image.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textview.h>
#include <gtkmm/texttag.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent        the parent object
 ** @param    old_version   last version
 **/
ProgramUpdated::ProgramUpdated(Base* const parent,
                               Version const old_version) :
  Base(parent),
  Dialog("FreeDoko – " + _("Window::program updated")),
  old_version(old_version)
{
  ui->add_window(*this);

  set_icon(ui->icon);

  set_default_size(static_cast<int>(ui->logo->get_width() * 1.5),
                         static_cast<int>(ui->logo->get_height() * 2.5));
#ifdef POSTPONED
  get_window()->set_decorations(Gdk::DECOR_BORDER
                                      | Gdk::DECOR_RESIZEH
                                      | Gdk::DECOR_TITLE
                                      | Gdk::DECOR_MENU);
#endif

  { // action area
    { // reset ais button
      auto reset_ais_button = Gtk::manage(new Gtk::Button(_("Button::reset ais")));
      add_action_widget(*reset_ais_button, Gtk::RESPONSE_NONE);

      // signals
      reset_ais_button->signal_clicked().connect(sigc::mem_fun(*this, &ProgramUpdated::reset_ais));
    } // reset ais button
    add_close_button(*this);
  } // action area


  { // the image
    auto image = Gtk::manage(new Gtk::Image(parent->ui->logo));
    get_content_area()->pack_start(*image, Gtk::PACK_SHRINK);
  } // the image
  { // the text
    auto const text = Gtk::manage(new Gtk::TextView());
    text_buffer = text->get_buffer();
    {
      auto tag_title = text_buffer->create_tag("title");
      //tag_title->set_property("weight", Pango::WEIGHT_BOLD);
      tag_title->set_property("scale", Pango::SCALE_X_LARGE);
      tag_title->set_property("justification", Gtk::JUSTIFY_CENTER);

      auto tag_subtitle = text_buffer->create_tag("subtitle");
      tag_title->set_property("weight", Pango::WEIGHT_BOLD);
    }
    { // title
      text_buffer->insert_with_tag(text_buffer->begin(),
                                         "\n"
                                         // gettext: %s = version
                                         + _("Greeting::%s", ::version->number_to_string()),
                                         "title");
    } // title
    for (auto v = ::all_versions.rbegin();
         ( (v != ::all_versions.rend())
          && (old_version < *v) );
         ++v) {
      text_buffer->insert_with_tag(text_buffer->end(),
                                         "\n\n"
                                         // gettext: %s = version
                                         + _("ChangeLog::for %s", v->number_to_string()),
                                         "subtitle");
      text_buffer->insert(text_buffer->end(),
                                "\n"
                                + _("ChangeLog::" + v->number_to_string()));
    } // for (v \in ::all_versions)


    auto text_window = Gtk::manage(new Gtk::ScrolledWindow());
    text_window->add(*text);
    text_window->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    text->set_editable(false);
    text->set_wrap_mode(Gtk::WRAP_WORD);
    text->set_cursor_visible(false);

    get_content_area()->pack_end(*text_window);
  } // the text

  show_all_children();
} // ProgramUpdated::ProgramUpdated(Base* parent, Version old_version)

/** destructor
 **/
ProgramUpdated::~ProgramUpdated() = default;

/** reset the ais
 **/
void
ProgramUpdated::reset_ais()
{
  unsigned n = 0;
  for (auto& p : ui->party().players())
    if (dynamic_cast<Aiconfig*>(&p))
      dynamic_cast<Aiconfig&>(p).reset_to_hardcoded(n);

  hide();
} // void ProgramUpdated::reset_ais()

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
