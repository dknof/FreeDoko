/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "splash_screen.h"

#include "ui.h"

#include "../../misc/preferences.h"

#include <gtkmm/progressbar.h>
#include <gtkmm/box.h>
#include <gtkmm/image.h>
namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 **/
SplashScreen::SplashScreen(Base* const parent) :
  Base(parent),
  Window(Gtk::WINDOW_POPUP)
{
  set_type_hint(Gdk::WINDOW_TYPE_HINT_SPLASHSCREEN);
  signal_realize().connect(sigc::mem_fun(*this,
                                               &SplashScreen::init));
} // SplashScreen::SplashScreen(Base* parent)

/** destructor
 **/
SplashScreen::~SplashScreen() = default;

/** create all subelements
 **/
void
SplashScreen::init()
{
  Window::realize();

  set_title(_("Window::splash screen"));

  image = Gtk::manage(new Gtk::Image(ui->logo));
  progress_bar = Gtk::manage(new Gtk::ProgressBar());

  if (::preferences(::Preferences::Type::splash_screen_transparent)) {
    add(*(image));
    image->show();
  } else { // if !(::preferences(::Preferences::Type::splash_screen_transparent))
    auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
    add(*vbox);

    vbox->pack_start(*image, Gtk::PACK_SHRINK);
    vbox->pack_end(*progress_bar, Gtk::PACK_SHRINK);
    vbox->show_all();
  } // if !(::preferences(::Preferences::Type::splash_screen_transparent))

  set_position(Gtk::WIN_POS_CENTER);
} // void SplashScreen::init()

/** the status message has changed
 **
 ** @param    status_message   the new status_message
 **/
void
SplashScreen::status_message_changed(string const& status_message)
{
  if (!is_visible())
    return ;

  if (!::preferences(::Preferences::Type::splash_screen_transparent))
    progress_bar->set_text(status_message);

  ui->update();
} // void SplashScreen::status_message_changed(string status_message)

/** the progress has changed
 **
 ** @param    progress   the actual progress
 **/
void
SplashScreen::progress_changed(double const progress)
{
  if (!is_visible())
    return ;

  if (::preferences(::Preferences::Type::splash_screen_transparent)) {
#ifdef POSTPONED // gtkmm 3
    logo_pixmap->draw_rectangle(gc,
                                      true,
                                      55, 191,
                                      static_cast<int>((259 - 55)
                                                       * ui->progress()),
                                      226 - 191);

    image->set(logo_pixmap, logo_bitmap);
#endif
  } else { //  (::preferences(::Preferences::Type::splash_screen_transparent))
    progress_bar->set_fraction(progress);
  } // if (!::preferences(::Preferences::Type::splash_screen_transparent))

  ui->update();
} // void SplashScreen::progress_changed(double progress)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
