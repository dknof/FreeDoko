/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "support.h"

#include "ui.h"

#include "../../versions.h"

#include <gtkmm/image.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textview.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    parent   the parent object
 **/
Support::Support(Base* const parent) :
  Base(parent),
  StickyDialog("FreeDoko – " + _("Window::support"), false)
{
  ui->add_window(*this);
  signal_realize().connect(sigc::mem_fun(*this, &Support::init));
} // Support::Support(Base* parent)

/** destruktor
 **/
Support::~Support() = default;

/** create all subelements
 **/
void
Support::init()
{
  set_icon(ui->icon);

  set_skip_taskbar_hint();

  text = Gtk::manage(new Gtk::TextView());

  text->get_buffer()->set_text(_("Version: %s",
                                       to_string(*::version))
                                     + "\n\n"
                                     + _("Support::support"));

  add_close_button(*this);

  set_default_size((ui->logo->get_width() * 3) / 2,
                         (ui->logo->get_height() * 5) / 2);
#ifdef POSTPONED
  get_window()->set_decorations(Gdk::DECOR_BORDER
                                      | Gdk::DECOR_RESIZEH
                                      | Gdk::DECOR_TITLE
                                      | Gdk::DECOR_MENU);
#endif

  { // the image
    auto image = Gtk::manage(new Gtk::Image(parent->ui->logo));
    get_content_area()->pack_start(*image, Gtk::PACK_SHRINK);
  } // the image
  { // the text
    auto text_window = Gtk::manage(new Gtk::ScrolledWindow());
    text_window->add(*text);
    text_window->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    text->set_editable(false);
    text->set_wrap_mode(Gtk::WRAP_WORD);
    text->set_cursor_visible(false);

    get_content_area()->pack_start(*text_window);
  } // the text

  show_all_children();
} // void Support::init()

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
