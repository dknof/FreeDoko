/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "table.h"
#include "party_points.h"
#include "party_finished.h"
#include "reservation.h"
#include "last_trick.h"
#include "table/cards_distribution.h"
#include "table/poverty.h"
#include "table/trick.h"
#include "table/hand.h"
#include "table/trickpile.h"
#include "table/icongroup.h"
#include "table/name.h"

#include "ui.h"
#include "cards.h"
#include "gameinfo_dialog.h"
#include "full_trick.h"
#include "game_finished.h"

#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../game/game.h"
#include "../../player/player.h"
#include "../../card/trick.h"
#include "../../misc/preferences.h"
#include "../../utils/string.h"

#include <gtkmm/drawingarea.h>
#include <gdkmm/general.h>
#include <cmath>

namespace UI_GTKMM_NS {

/** updates the table from the buffer (the surface)
 **/
void
Table::update_from_buffer()
{
  auto cr = get_window()->create_cairo_context();
  cr->set_source(surface_, 0, 0);
  cr->paint();
} // void Table::update_from_buffer()


// NOLINTNEXTLINE(misc-no-recursion)
void Table::draw_all()
{
  if (!get_realized())
    return ;

  auto cr = create_cairo_context();

  draw_background(cr);

  if (ui->party().in_game()) {
    for (auto& p : part_) {
      auto const outline = p->outline();
      cr->set_source(p->surface(), outline.x, outline.y);
      cr->paint();
    }
  } else {
    draw_logo(cr);
  }

  update_from_buffer();

#ifdef WINDOWS
#ifdef WORKAROUND
  // Sometimes the window is not updated
  queue_draw();
#endif
#endif

  mouse_cursor_update();
} // void Table::draw_all()

/** Force the redrawing of all elements
 **/
void
Table::force_redraw_all()
{
  if (ui->party().in_game())
    layout_.recalc(*this);

  for (auto p : part_)
    p->force_redraw();
  draw_all();
} // void Table::force_redraw_all()

/** draw the background
 ** According to the size of the background as pattern or as image
 **/
void
Table::draw_background(Cairo::RefPtr<::Cairo::Context> cr)
{
  cr->save();
  if (   width() < 2 * background_pixbuf->get_width()
      && height() < 2  * background_pixbuf->get_height()) {
    cr->scale(static_cast<double>(width()) / background_pixbuf->get_width(),
              static_cast<double>(height()) / background_pixbuf->get_height());
    Gdk::Cairo::set_source_pixbuf(cr, background_pixbuf, 0, 0);
  } else {
    cr->set_source(background_pattern);
  }
  cr->paint();
  cr->restore();
}

/** draw the logo
 **
 ** @param    cr     drawing context
 **
 ** @bug   clip mask does not work (GTK-bug?)
 **/
void
Table::draw_logo(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!get_realized())
    return ;
  if (!ui->logo)
    return ;

  draw_pixbuf(cr, ui->logo,
              (width() - ui->logo->get_width()) / 2,
              (height() - ui->logo->get_height()) / 2);
  { // draw a border with the cards
    auto const card_width = ui->cards->width();
    auto const card_height = ui->cards->height();

    auto surface_top = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32,
                                                   width(),
                                                   height());
    auto surface_right = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32,
                                                     width(),
                                                     height());
    auto surface_bottom = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32,
                                                      width(),
                                                      height());
    auto surface_left = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32,
                                                    width(),
                                                    height());

    { // top
      auto cr = Cairo::Context::create(surface_top);
      vector<Card> const border_card =  {
        {Card::heart_ten},
        {Card::club_queen},
        {Card::spade_queen},
        {Card::heart_queen},
        {Card::diamond_queen},
        {Card::club_jack},
        {Card::spade_jack}};

      auto const border_top_x
        = (card_width
           + ( (static_cast<int>(border_card.size()) * card_height
                + card_width >= width())
              ? 0
              : ((width() - (border_card.size() * card_height
                                   + card_width)
                 ) / border_card.size())
             )
          );

      for (size_t n = 0; n < border_card.size(); n++) {
        draw_pixbuf(cr,
                    ui->cards->card(border_card[n]),
                    border_top_x +
                    n * (width() - 2 * border_top_x
                         - card_height)
                    / (border_card.size() - 1),
                    card_width / 2,
                    Rotation::left);
      } // for (n < border_card.size())
    } // top
    { // right
      auto cr = Cairo::Context::create(surface_right);
      vector<Card> const border_card = {
        {Card::heart_jack},
        {Card::diamond_jack},
        {Card::diamond_ace},
        {Card::diamond_ten},
        {Card::diamond_king}};

      auto const border_right_y
        = (card_width
           + ( (static_cast<int>(border_card.size()) * card_height
                + card_width >= height())
              ? 0
              : ((height() - (border_card.size() * card_height
                                    + card_width)
                 ) / border_card.size())
             )
          );

      for (size_t n = 0; n < border_card.size(); n++) {
        draw_pixbuf(cr,
                    ui->cards->card(border_card[n]),
                    width() - 3 * card_width / 2,
                    border_right_y +
                    n * (height() - 2 * border_right_y
                         - card_height)
                    / (border_card.size() - 1),
                    Rotation::up);
      }
    } // right
    { // bottom
      auto cr = Cairo::Context::create(surface_bottom);
      vector<Card> const border_card = {
        {Card::diamond_nine},
        {Card::club_ace},
        {Card::club_ten},
        {Card::club_king},
        {Card::club_nine},
        {Card::heart_ace},
        {Card::heart_king}};

      auto const border_bottom_x
        = (card_width
           + ( (static_cast<int>(border_card.size()) * card_height
                + card_width >= width())
              ? 0
              : ((width() - (border_card.size() * card_height
                                   + card_width)
                 ) / border_card.size())
             )
          );

      for (size_t n = 0; n < border_card.size(); n++) {
        draw_pixbuf(cr,
                    ui->cards->card(border_card[n]),
                    border_bottom_x +
                    (border_card.size() - 1 - n)
                    * (width() - 2 * border_bottom_x
                       - card_height)
                    / (border_card.size() - 1),
                    height()
                    - 3 * card_width / 2,
                    Rotation::right);
      }
    } // bottom
    int border_left_y = 0;
    { // left
      auto cr = Cairo::Context::create(surface_left);
      vector<Card> const border_card = {
        {Card::heart_nine},
        {Card::spade_ace},
        {Card::spade_ten},
        {Card::spade_king},
        {Card::spade_nine}};

      border_left_y
        = (card_width
           + ( (static_cast<int>(border_card.size()) * card_height
                + card_width >= height())
              ? 0
              : ((height() - (border_card.size() * card_height
                                    + card_width)
                 ) / border_card.size())
             )
          );

      for (size_t n = 0; n < border_card.size(); n++) {
        int const pos_x = card_width / 2;
        int const pos_y = (border_left_y +
                           (border_card.size() - 1 - n)
                           * (height() - 2 * border_left_y
                              - card_height)
                           / (border_card.size() - 1));

        draw_pixbuf(cr,
                    ui->cards->card(border_card[n]),
                    pos_x, pos_y, Rotation::down);
      }
    } // left

    cr->set_source(surface_top, 0, 0);
    cr->paint();
    cr->set_source(surface_right, 0, 0);
    cr->paint();
    cr->set_source(surface_bottom, 0, 0);
    cr->paint();
    cr->set_source(surface_left, 0, 0);
    cr->paint();
    {
      cr->save();
      cr->rectangle(0, 0,
                    border_left_y + max(ui->cards->width(), ui->cards->height()),
                    height());
      cr->clip();
      cr->set_source(surface_top, 0, 0);
      cr->paint();
      cr->restore();
    }
  } // draw a border with the cards

  update_from_buffer();
} // void Table::draw_logo(Cairo::RefPtr<::Cairo::Context> cr)

/** update the cards distribution
 **/
void
Table::update_cards_distribution()
{
  cards_distribution_->force_redraw();
}

/** update all hands
 **/
void
Table::update_hands()
{
  for (auto& h : hand_)
    h.second->force_redraw();
}

/** update all cards
 **/
void
Table::update_cards()
{
  for (auto& h : hand_)
    h.second->force_redraw();
  for (auto& t : trickpile_)
    t.second->force_redraw();
  trick_->force_redraw();
  poverty_->force_redraw();
}

/** update all cards back
 **/
void
Table::update_cards_back()
{
  for (auto& h : hand_)
    h.second->force_redraw();
  for (auto& t : trickpile_)
    t.second->force_redraw();
} // void Table::update_cards_back()

/** update all icons
 **/
void
Table::update_icons()
{
  for (auto& i : icongroup_)
    i.second->force_redraw();
} // void Table::update_icons()

/** loads the background
 **/
void
Table::load_background()
{
  try {
    auto back_new = Gdk::Pixbuf::create_from_file(::preferences.path(::Preferences::Type::background).string());
    if (!back_new)
      throw Glib::FileError(Glib::FileError::FAILED,
                            "error loading pixmap from '"
                            + ::preferences.path(::Preferences::Type::background).string() + "'");
    background_pixbuf = back_new;

    auto surface = Cairo::ImageSurface::create(Cairo::FORMAT_RGB24,
                                               back_new->get_width(),
                                               back_new->get_height());
    auto cr = Cairo::Context::create(surface);
    Gdk::Cairo::set_source_pixbuf(cr, back_new, 0, 0);
    cr->paint();
    background_pattern = Cairo::SurfacePattern::create(surface);
    background_pattern->set_extend(Cairo::EXTEND_REPEAT);

    draw_all();
  } catch (Glib::FileError const& file_error) {
    ui->error(_("Error::loading the background %s",
                      ::preferences.value(::Preferences::Type::background)
                     ));
  } // try
} // void Table::load_background()

/** the name of 'player' has changed
 **
 ** @param    player   the player with the changed name
 **/
void
Table::name_changed(Player const& player)
{
  if (party_points_)
    party_points_->name_changed(player);

  if (party_finished_)
    party_finished_->name_changed(player);

  if (!ui->party().in_game())
    return ;
  force_redraw_all();
  force_redraw_all(); // two times, for widescreen layout to get all positions right

  gameinfo_->name_changed(player);

  if (last_trick_)
    last_trick_->name_changed(player);

  if (game_finished_)
    game_finished_->name_changed(player);

  if (!name_.empty())
    name(player).force_redraw();
} // void Table::name_changed(Player const& player)

/** updates the font
 **
 ** @param    fontname   the name of the new font
 ** @param    type        the type of the font
 **/
void
Table::new_font(string const& fontname, ::Preferences::Type::String const type)
{
  switch (type) {
  case ::Preferences::Type::name_font:
    for (auto& n : name_)
      n.second->force_redraw();
    draw_all();
    break;

  case ::Preferences::Type::trickpile_points_font:
    for (auto& t : trickpile_)
      t.second->force_redraw();
    draw_all();
    break;
  default:
    break;
  } // switch (type)
} // void Table::new_font(string fontname, ::Preferences::Type::String type)

/** updates the  color
 **
 ** @param    colorname   the name of the new color
 ** @param    type      the type of the font
 **
 ** @todo   using 'set_foreground'
 **/
void
Table::new_color(string const& colorname, ::Preferences::Type::String const type)
{
  auto cr = create_cairo_context();
  cr->push_group();
  switch (type) {
  case ::Preferences::Type::name_font_color:
  case ::Preferences::Type::name_active_font_color:
  case ::Preferences::Type::name_reservation_font_color: {

    for (auto& n : name_)
      n.second->force_redraw();
    draw_all();

  } break;

  case ::Preferences::Type::trickpile_points_font_color:
    for (auto& t : trickpile_)
      t.second->force_redraw();
    draw_all();
    break;

  case ::Preferences::Type::poverty_shift_arrow_color:
    if (   ui->party().in_game()
        && ui->game().status()  == Game::Status::poverty_shift
        && poverty_)
      poverty().force_redraw();
    draw_all();

    break;

  default:
    break;
  } // switch(type)
} // void Table::new_color(string colorname, ::Preferences::Type::String type)

/** the setting has changed
 **
 ** @param    type   the type that has changed
 **/
void
Table::preference_changed(int const type)
{
  switch(type) {
  case ::Preferences::Type::show_all_hands:
    force_redraw_all();
    break;
  case ::Preferences::Type::show_all_tricks:
    if (last_trick_ && last_trick_->is_visible()) {
      // Aktualisiere die Sichtbarkeit der Navigationsschaltflächen
      last_trick_->set_trickno(last_trick_->trickno());
    }
    break;
  case ::Preferences::Type::emphasize_valid_cards:
    if (   ui->party().in_game()
        && ui->game().status() == Game::Status::play
        && ui->game().players().current_player().type() == Player::Type::human) {
      force_redraw_all();
    }
    break ;
  case ::Preferences::Type::automatic_card_suggestion:
    if (::preferences(::Preferences::Type::automatic_card_suggestion))
      if (   ui->party().in_game()
          && ui->game().status() == Game::Status::play
          && !ui->game().tricks().current().isfull()
          && ui->game().players().current_player().type() == Player::Type::human)
        show_card_suggestion(false);
    break;
  case ::Preferences::Type::show_trickpiles_points:
    for (auto& t : trickpile_)
      t.second->force_redraw();
    draw_all();
    break;
  case ::Preferences::Type::announce_swines_automatically:
    if (   ui->party().in_game()
        && ui->game().status() == Game::Status::reservation) {
      for (auto& r : reservation_)
        r.second->sensitivity_update();
    } // if (game.status() == Game::Status::reservation)
    break;
  case ::Preferences::Type::background:
    load_background();
    break;
  case ::Preferences::Type::rotate_trick_cards:
    trick_->force_redraw();
    draw_all();
    break;
  case ::Preferences::Type::name_font:
  case ::Preferences::Type::trickpile_points_font:
    new_font(::preferences(static_cast<::Preferences::Type::String>(type)),
             static_cast<::Preferences::Type::String>(type));
    break;
  case ::Preferences::Type::name_font_color:
  case ::Preferences::Type::name_active_font_color:
  case ::Preferences::Type::trickpile_points_font_color:
  case ::Preferences::Type::poverty_shift_arrow_color:
    new_color(::preferences(static_cast<::Preferences::Type::String>(type)),
              static_cast<::Preferences::Type::String>(type));
    break;
  case ::Preferences::Type::own_hand_on_table_bottom:
  case ::Preferences::Type::table_rotation:
    draw_all();
    break;

  case ::Preferences::Type::cardset:
  case ::Preferences::Type::cards_back:
  case ::Preferences::Type::iconset:
  case ::Preferences::Type::cards_height:
    force_redraw_all();
    break;

  case ::Preferences::Type::cards_order:
    update_cards_distribution();
    update_hands();
    draw_all();
    break;

  default:
    break;
  } // switch(type)
}


// NOLINTNEXTLINE(misc-no-recursion)
void Table::mouse_cursor_update()
{
  if (!get_realized())
    return ;
  if (!ui->party().in_game())
    return ;

  auto const& game = ui->game();

  int x = 0, y = 0;
  get_pointer(x, y);

  CursorType new_cursor_type = CursorType::standard;
  if (ui->busy())
    new_cursor_type = CursorType::busy;

  switch (game.status()) {
  case Game::Status::poverty_shift:
    DEBUG_ASSERTION(poverty_,
                    "Table::mouse_cursor_update():\n"
                    "  'poverty_' == nullptr");
    switch (poverty().possible_action(x, y)) {
    case Poverty::Action::none:
      break;
    case Poverty::Action::shift_cards:
      new_cursor_type = CursorType::poverty_shift;
      break;
    case Poverty::Action::accept_cards:
      new_cursor_type = CursorType::poverty_accept;
      break;
    case Poverty::Action::take_card:
      new_cursor_type = CursorType::poverty_get_card;
      break;
    case Poverty::Action::put_card:
      new_cursor_type = CursorType::poverty_put_card;
      break;
    case Poverty::Action::fill_up:
      new_cursor_type = CursorType::poverty_fill_up;
      break;
    case Poverty::Action::return_cards:
      new_cursor_type = CursorType::poverty_shift_back;
      break;
    case Poverty::Action::get_cards_back:
      new_cursor_type = CursorType::poverty_getting_back;
      break;

    } // switch (poverty().possible_action(x, y))
    break;

  case Game::Status::play: {
    // the human player has to play a card
    Player& player = ui->game().players().current_player();
    if (get_card()) {
      DEBUG_ASSERTION((player.type() == Player::Type::human),
                      "UI_GTKMM::Table::mouse_cursor_update():\n"
                      "  a card should be get, but the player '"
                      << player.no() << "' is not human but '"
                      << player.type() << "'");
      auto const card = hand(player).card_at_position(x, y);
      if (!card.is_empty()) {
        if (::preferences(::Preferences::Type::show_if_valid)) {
          if (player.game().tricks().current().isvalid(card, player.hand()))
            new_cursor_type = CursorType::card_valid;
          else
            new_cursor_type = CursorType::card_invalid;
        } else { // if !(show_if_valid)
          new_cursor_type = CursorType::play_card;
        } // if !(show_if_valid)

      } // if (card)
    } // if (player.type() == Player::Type::human)
    break;
  } // case Game::Status::play:
  default:
    if (   game.status() >= Game::Status::play
        && game.tricks().current().isfull()
        && full_trick_
        && trick().mouse_over_cards()) {
      new_cursor_type = CursorType::close_full_trick;
    } // if (::game_status == ...)
    break;
  } // switch(::game_type)

  if (game.in_running_game()) {
    for (auto const& p : game.players()) {
      auto const trick = trickpile(p).over_trick(x, y);
      if (trick
          && (   ::preferences(::Preferences::Type::show_all_tricks)
              || trick->no() == trick->game().tricks().real_current_no() - 1)) {
        new_cursor_type = CursorType::show_last_trick;
      }
      if (p.type() == Player::Type::human) {
        if (icongroup(p).mouse_over_next_announcement())
          new_cursor_type = CursorType::next_announcement;
      } // if (p->type() == Player::Type::human)
    } // for (p : game.player)
  } // if (::game_status in game)


  if (in_game_review())
    new_cursor_type = CursorType::game_review;

  if (cursor_type != new_cursor_type) {
    if (   (cursor_type == CursorType::next_announcement)
        || (new_cursor_type == CursorType::next_announcement) ) {
      cursor_type = new_cursor_type;
      draw_all();
    }
    cursor_type = new_cursor_type;
    get_window()->set_cursor(Table::cursor(cursor_type));
  } // if (cursor_type != new_cursor_type)
} // void Table::mouse_cursor_update()

/** event: draw the table
 **
 ** @param    c   the graphic context
 **
 ** @return   true
 **/
bool
Table::on_draw(::Cairo::RefPtr<::Cairo::Context> const& c)
{
  c->set_source(surface_, 0, 0);
  c->paint();
  return true;
} // bool Table::on_draw(::Cairo::RefPtr<::Cairo::Context> c)

/** event: the size has changed
 **
 ** @param    event   the event
 **
 ** @return   true
 **/
bool
Table::on_configure_event(GdkEventConfigure* const event)
{
  if (   surface_
      && get_width()  == surface_->get_width()
      && get_height() == surface_->get_height()) {
    return true;
  }

  surface_ = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32,
                                         get_width(),
                                         get_height());

  force_redraw_all();
  //update_from_buffer() wird von on_draw() aufgerufen

  return true;
} // bool Table::on_configure_event(GdkEventConfigure* event)

/** event: mouse motion
 **
 ** @param    event   the event
 **
 ** @return   whether there was a reaction
 **/
bool
Table::on_motion_notify_event(GdkEventMotion* const event)
{
  mouse_cursor_update();

  return false;
} // bool Table::on_motion_notify_event(GdkEventMotion* event)

/** -> result
 **
 ** @param    type   the cursor type
 **
 ** @return   the cursor for 'type'
 **/
Glib::RefPtr<Gdk::Cursor>
Table::cursor(CursorType const type)
{
  switch (type) {
  case CursorType::none:
    DEBUG_ASSERTION(false,
                    "Table::cursor(type):\n"
                    "  'type' == NONE");
    break;
  case CursorType::standard: {
    static auto const cursor = Gdk::Cursor::create(get_display(), "default");
    return cursor;
  }
    break;
  case CursorType::busy: {
#ifdef POSTPONED
    Glib::RefPtr<Gdk::Pixbuf> pixbuf = Gdk::Pixbuf::create_from_file("ui/gtkmm/cursor/busy.png");
    {
      GdkCursor* cursor = gdk_cursor_new_from_pixbuf(Gdk::Cursor(Gdk::X_CURSOR).get_display()->gobj(), pixbuf->gobj(), 0, 0);
      //Gdk::Cursor(Gdk::Pixbuf::create_from_file("ui/gtkmm/cursor/busy.png"),);
      return cursor;
    }
#endif
  }
    {
      static auto const cursor = Gdk::Cursor::create(Gdk::WATCH);
      return cursor;
    }
    //return Gdk::Cursor(Gdk::EXCHANGE);
    //return Gdk::Cursor(Gdk::CLOCK);
  case CursorType::poverty_shift: {
    static auto const cursor_left = Gdk::Cursor::create(Gdk::SB_LEFT_ARROW);
    static auto const cursor_right = Gdk::Cursor::create(Gdk::SB_RIGHT_ARROW);
    static auto const cursor_up = Gdk::Cursor::create(Gdk::SB_UP_ARROW);
    static auto const cursor_down = Gdk::Cursor::create(Gdk::SB_DOWN_ARROW);
    switch (position(ui->game().players().current_player())) {
    case Position::south:
      return cursor_left;
    case Position::north:
      return cursor_right;
    case Position::west:
      return cursor_up;
    case Position::east:
      return cursor_down;
    default:
      return cursor_left;
    }
  }
  case CursorType::poverty_shift_back: {
    static auto const cursor_left = Gdk::Cursor::create(Gdk::SB_LEFT_ARROW);
    static auto const cursor_right = Gdk::Cursor::create(Gdk::SB_RIGHT_ARROW);
    static auto const cursor_up = Gdk::Cursor::create(Gdk::SB_UP_ARROW);
    static auto const cursor_down = Gdk::Cursor::create(Gdk::SB_DOWN_ARROW);
    switch (position(ui->game().players().current_player())) {
    case Position::south:
      return cursor_right;
    case Position::north:
      return cursor_left;
    case Position::west:
      return cursor_down;
    case Position::east:
      return cursor_up;
    default:
      return cursor_right;
    }
  }
  case CursorType::poverty_get_card:
  case CursorType::poverty_put_card:
  case CursorType::poverty_fill_up: {
    static auto const cursor = Gdk::Cursor::create(Gdk::HAND1);
    return cursor;
  }
  case CursorType::poverty_accept:
  case CursorType::poverty_getting_back: {
    static auto const cursor = Gdk::Cursor::create(Gdk::HAND2);
    return cursor;
  }
  case CursorType::next_announcement: {
    static auto const cursor = Gdk::Cursor::create(Gdk::HAND2);
    return cursor;
  }
  case CursorType::play_card: {
    static auto const cursor = Gdk::Cursor::create(Gdk::HAND1);
    return cursor;
  }
  case CursorType::card_valid: {
    static auto const cursor = Gdk::Cursor::create(Gdk::HAND1);
    return cursor;
  }
  case CursorType::card_invalid: {
    static auto const cursor = Gdk::Cursor::create(Gdk::PIRATE);
    return cursor;
  }
  case CursorType::close_full_trick: {
    static auto const cursor = Gdk::Cursor::create(Gdk::HAND1);
    return cursor;
  }
  case CursorType::show_last_trick: {
    static auto const cursor = Gdk::Cursor::create(Gdk::HAND2);
    return cursor;
  }
  case CursorType::game_review: {
    static auto const cursor = Gdk::Cursor::create(Gdk::SB_H_DOUBLE_ARROW);
    return cursor;
  }
  } // switch (type)

  return {};
} // static Gdk::Cursor const& Table::cursor(CursorType type)

/** the progress has changed -- redraw the name of the active player
 **
 ** @param    progress   new progress
 **/
void
Table::progress_changed(double const progress)
{
  draw_all();
} // void Table::progress_changed(double progress)

/** the progress has finished -- redraw the name of the active player
 **/
void
Table::progress_finished()
{
  draw_all();
} // void Table::progress_finished()

/** draw a pixbuf
 **
 ** @param    cr       drawing context
 ** @param    pixbuf   pixbuf to draw
 ** @param    x        x position
 ** @param    y        y position
 **/
void
draw_pixbuf(Cairo::RefPtr<::Cairo::Context> cr,
            Glib::RefPtr<Gdk::Pixbuf> pixbuf,
            int const x, int const y,
            Rotation const rotation)
{
  if (!pixbuf)
    return ;
  cr->save();
  cr->translate(x, y);
  switch (rotation) {
  case Rotation::up:
    break ;
  case Rotation::down:
    cr->translate(pixbuf->get_width(), pixbuf->get_height());
    cr->rotate_degrees(180);
    break ;
  case Rotation::left:
    cr->translate(0, pixbuf->get_width());
    cr->rotate_degrees(-90);
    break ;
  case Rotation::right:
    cr->translate(pixbuf->get_height(), 0);
    cr->rotate_degrees(90);
    break ;
  }
  Gdk::Cairo::set_source_pixbuf(cr, pixbuf, 0, 0);
  cr->paint();
  cr->restore();
}

/** draw a pixbuf
 **
 ** @param    cr       drawing context
 ** @param    pixbuf   pixbuf to draw
 ** @param    pos      position
 **/
void
draw_pixbuf(Cairo::RefPtr<::Cairo::Context> cr,
            Glib::RefPtr<Gdk::Pixbuf> pixbuf,
            Gdk::Point const& pos,
            Rotation const rotation)
{
  draw_pixbuf(cr, pixbuf, pos.get_x(), pos.get_y(), rotation);
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
