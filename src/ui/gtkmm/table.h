/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"

#include "table/layout.h"

#include "../../basetypes/rotation.h"
#include "../../basetypes/announcement.h"
#include "../../game/reservation.h"
#include "../../game/gameplay_action.h"
#include "../../misc/preferences.h"
class Player;
class Trick;

#include <map>

#include <gtkmm/drawingarea.h>
#include <gdkmm/pixbuf.h>
#include <gdkmm/cursor.h>

namespace UI_GTKMM_NS {
class CardsDistribution;
class Reservation;
class Poverty;
class Trick;
class Hand;
class TrickPile;
class Icongroup;
class Name;
class GameFinished;
class PartyPoints;
class PartyFinished;

class GameInfoDialog;
class FullTrick;
class LastTrick;
class CardSuggestion;

/**
 ** the table
 **
 ** @todo   keep 'game_finished' window and 'poverty' over the games
 ** @todo   close full trick with click
 ** @todo   use 'Gdk::Point' and 'Gdk::Rectangle'
 **/
class Table : public Base, public Gtk::DrawingArea {
  public:
    enum class CursorType {
      none,
      standard,
      busy,
      poverty_shift,
      poverty_accept,
      poverty_get_card,
      poverty_put_card,
      poverty_fill_up,
      poverty_shift_back,
      poverty_getting_back,
      next_announcement,
      play_card,
      card_valid,
      card_invalid,
      close_full_trick,
      show_last_trick,
      game_review
    }; // enum CursorType

    using LayoutType = TableLayoutType;
    using Layout     = TableLayout;

  public:
    class Part;
    class HTIN;

    friend class Part;
    friend class HTIN;
    friend class Hand;
    friend class UI_GTKMM;
    friend class Menu;
  public:
    explicit Table(Base* parent);
    ~Table() override;

    Table() = delete;
    Table(Table const&) = delete;
    Table& operator=(Table const&) = delete;

    auto party_points() -> PartyPoints&;

    auto cards_distribution()                    -> CardsDistribution&;
    auto cards_distribution()              const -> CardsDistribution const&;
    auto poverty()                               -> Poverty&;
    auto poverty()                         const -> Poverty const&;
    auto trick()                                 -> Trick&;
    auto trick()                           const -> Trick const&;
    auto hand(Player const& player)              -> Hand&;
    auto hand(Player const& player)        const -> Hand const&;
    auto hand(Position position)                 -> Hand&;
    auto hand(Position position)           const -> Hand const&;
    auto trickpile(Player const& player)         -> TrickPile&;
    auto trickpile(Player const& player)   const -> TrickPile const&;
    auto trickpile(Position position)            -> TrickPile&;
    auto trickpile(Position position)      const -> TrickPile const&;
    auto icongroup(Player const& player)         -> Icongroup&;
    auto icongroup(Player const& player)   const -> Icongroup const&;
    auto icongroup(Position position)            -> Icongroup&;
    auto icongroup(Position position)      const -> Icongroup const&;
    auto name(Player const& player)              -> Name&;
    auto name(Player const& player)        const -> Name const&;
    auto name(Position position)                 -> Name&;
    auto name(Position position)           const -> Name const&;
    auto reservation(Player const& player)       -> Reservation&;
    auto reservation(Player const& player) const -> Reservation const&;
    auto reservation(Position position)          -> Reservation&;
    auto reservation(Position position)    const -> Reservation const&;

    bool get_card() const;

    Player const& player(Position position) const;
    Position position(Player const& player) const;
    Rotation rotation(Player const& player) const;
    Rotation rotation(Position position) const;

    Cairo::RefPtr<Cairo::Context> create_cairo_context();

    int width()  const;
    int height() const;

    auto layout() const -> Layout const&;

    void party_finish();

    // a gameplay action
    void gameplay_action(GameplayAction const& action);

    void game_open();
    void game_distribute_cards_manually();
    void game_cards_distributed();
    void game_redistribute();
    void reservation_ask(Player const& player);
    ::Reservation reservation_get(Player const& player);
    void game_start();
    void card_get(Player const& player);
    void card_got();
    void card_played(Player const& player);
    void trick_open();
    void trick_full();
    void trick_move_in_pile();
    void show_last_trick();
    void show_trick(::Trick const& trick);
    void show_card_suggestion(bool show_window = true);
    void game_finished();
    void game_close();

    bool in_game_review() const;
    unsigned game_review_trickno() const;
    ::Trick const& game_review_trick() const;
    bool game_review_trick_visible() const;

    void announcement_made(Announcement announcement,
                           Player const& player);
    void swines_announced(Player const& player);
    void hyperswines_announced(Player const& player);

    void marriage(Player const& bridegroom, Player const& bride);

    void update_human_teaminfo();

    void update_from_buffer();
    void draw_all();
    void force_redraw_all();

    void draw_background(Cairo::RefPtr<::Cairo::Context> cr);
    void draw_logo(Cairo::RefPtr<::Cairo::Context> cr);
    void draw_party_summary();
    void update_cards_distribution();
    void update_hands();
    void update_cards();
    void update_cards_back();
    void update_icons();

    void load_background();

    void save_screenshot() const;

    void open_pdf();
    void save_trick_in_pdf();
    void close_pdf();

    void name_changed(Player const& player);

    void new_font(string const& fontname, ::Preferences::Type::String type);
    void new_color(string const& colorname, ::Preferences::Type::String type);

    void preference_changed(int type);
    void mouse_cursor_update();

    void progress_changed(double progress);
    void progress_finished();

  private:
    bool on_draw(::Cairo::RefPtr<::Cairo::Context> const& cr) override;
    bool on_configure_event(GdkEventConfigure* event) override;
    bool on_button_press_event(GdkEventButton* event) override;
    bool on_scroll_event(GdkEventScroll* event) override;
    bool on_motion_notify_event(GdkEventMotion* event) override;

  private:
    AutoDisconnector disconnector_;

    Glib::RefPtr<Gdk::Cursor> cursor(CursorType type);

  public:
    CursorType cursor_type = CursorType::none;

    Glib::RefPtr<Gdk::Pixbuf> background_pixbuf;
    Cairo::RefPtr<Cairo::SurfacePattern> background_pattern;

  private:
    unique_ptr<CardsDistribution> cards_distribution_;
    std::map<Position, unique_ptr<Reservation>> reservation_;
    unique_ptr<Poverty> poverty_;
    unique_ptr<GameFinished> game_finished_;
    unique_ptr<PartyPoints> party_points_;
    unique_ptr<PartyFinished> party_finished_;
    unique_ptr<FullTrick> full_trick_;
    unique_ptr<LastTrick> last_trick_;
    unique_ptr<CardSuggestion> card_suggestion_;

    unique_ptr<GameInfoDialog> gameinfo_;

    unique_ptr<Trick> trick_;
    std::map<Position, unique_ptr<Hand>> hand_;
    std::map<Position, unique_ptr<TrickPile>> trickpile_;
    std::map<Position, unique_ptr<Icongroup>> icongroup_;
    std::map<Position, unique_ptr<Name>> name_;

    vector<Part*> part_;

    bool get_card_ = false;

    Cairo::RefPtr<Cairo::ImageSurface> surface_;
    Cairo::RefPtr<Cairo::PdfSurface> pdf_surface_;

    Layout layout_;
}; // class Table : public Base, protected Gtk::DrawingArea

void draw_pixbuf(Cairo::RefPtr<::Cairo::Context> cr,
                 Glib::RefPtr<Gdk::Pixbuf> pixbuf, Gdk::Point const& pos,
                 Rotation rotation = Rotation::up);
void draw_pixbuf(Cairo::RefPtr<::Cairo::Context> cr,
                 Glib::RefPtr<Gdk::Pixbuf> pixbuf, int x, int y,
                 Rotation rotation = Rotation::up);
} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
