/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "cards_distribution.h"
#include "hand.h"
#include "icongroup.h"
#include "trickpile.h"
#include "name.h"

#include "../table.h"
#include "../ui.h"

#include "../cards.h"

#include "../../../game/game.h"
#include "../../../party/rule.h"
#include "../../../misc/preferences.h"

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    table   the table
 ** @param    position   the corresponding position
 **/
CardsDistribution::CardsDistribution(Table& table) :
  Table::Part(table),
  last_status_{::Hand()}
{ }

/** destructor
 **/
CardsDistribution::~CardsDistribution() = default;

/** reset all values
 **/
void
CardsDistribution::reset()
{
  finished_ = false;
  cards_ = ::Hand(game().players().player(0));
  auto const& rule = game().rule();
  auto const number_of_same_cards = rule(Rule::Type::number_of_same_cards);
  for (auto const c : game().cards().cards()) {
    cards_.add(c, number_of_same_cards);
  }
  set_hand_of_human_player();
}

/** @return   whether the distribution is finished
 **/
bool
CardsDistribution::finished() const
{
  return finished_;
}

/** @return   the corresponding sorted hand
 **/
SortedHand
CardsDistribution::cards()
{
  return cards_.sorted();
}

/** @return   the corresponding sorted hand
 **/
SortedHandConst
CardsDistribution::cards() const
{
  return cards_.sorted();
}

/**
 **/
bool
CardsDistribution::changed() const
{
  if (!surface_)
    return true;
  if (game().status() == Game::Status::manual_cards_distribution)
    return (last_status_ != current_status());
  return true;
}

/** draws the hand
 **
 ** @param    cr       drawing context
 **/
void
CardsDistribution::draw(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (game().status() != Game::Status::manual_cards_distribution)
    return ;

  auto const& rule = game().rule();
  auto const number_of_same_cards = rule(Rule::Type::number_of_same_cards);

  cr->save();
  cr->push_group();

  last_status_ = current_status();

  auto const geometry = this->geometry();
  geometry.transform(cr);

  // draw the cards
  for (unsigned c = 0; c < cards().cardsnumber_all(); c++) {
    if (cards().played(c))
      continue;

    auto card = ui->cards->card(cards().card_all(c)).pixbuf();
    int const n = c / number_of_same_cards;
    int const i = c % number_of_same_cards;
    int const x = n % geometry.columns;
    int const y = n / geometry.columns;
    int const pos_x = (geometry.cards_step_x * x
                       + geometry.cards_small_step_x * i);
    int const pos_y = -geometry.cards_step_y * (geometry.rows - 1 - y);

    draw_pixbuf(cr, card, pos_x, pos_y - card->get_height());
  }
  cr->pop_group_to_source();
  cr->paint();
  cr->restore();
}

/** @return   geometry of the cards
 **/
auto CardsDistribution::geometry() const -> Geometry
{
  auto const& rule = game().rule();
  auto const cards_width  = ui->cards->width();
  auto const cards_height = ui->cards->height();

  auto const rows = [&]() {
    switch (table().layout().type) {
    case Table::LayoutType::standard:
      return 4;
    case Table::LayoutType::widescreen:
      return 3;
    default:
      return 1;
    }
  }();

  auto const columns = [&](){
    auto const number_of_cards      = rule(Rule::Type::number_of_cards);
    auto const number_of_same_cards = rule(Rule::Type::number_of_same_cards);
    auto const number_of_different_cards = number_of_cards / number_of_same_cards;
    return (number_of_different_cards / rows
            + (number_of_different_cards % rows == 0 ? 0 : 1));
  }();

  auto const cards_step_x = [&]() {
    auto const outline_west = table().icongroup(Position::west).outline();
    auto const x_west       = outline_west.x + outline_west.width + table().layout().border_x;
    auto const outline_east = table().icongroup(Position::east).outline();
    auto const x_east       = outline_east.x - table().layout().border_x;
    auto const max_width_in_center = x_east - x_west;
    auto const min_card_step_x = cards_width * 3 / 4;
    auto const max_card_step_x = cards_width * 7 / 5;
    auto const max_width = max(2 * ui->cards->width(),
                               max_width_in_center);
    return min(max_card_step_x,
               max(min_card_step_x,
                   cards_width
                   + static_cast<int>(  (max_width - columns * cards_width)
                                      / (columns - 1 + 1.0/3))
                  ) - 1
              );
  }();
  auto const cards_small_step_x = max(cards_width / 5,
                                      min(cards_width / 3,
                                          (cards_step_x - cards_width) / 3));

  auto const width = (cards_width
                      + (columns - 1) * cards_step_x
                      + cards_small_step_x);
  auto const cards_step_y = [&]() {
    auto const outline_south = table().icongroup(Position::south).outline();
    auto const y_south       = outline_south.y - outline_south.height - table().layout().border_y;
    auto const outline_north = table().icongroup(Position::north).outline();
    auto const y_north       = outline_north.y + table().layout().border_y;
    auto const max_height = max(2 * cards_height,
                                min(y_south - y_north,
                                    static_cast<int>((rows - 1)
                                                     * cards_height * 1.25
                                                     + cards_height)
                                   ));
    return (cards_height
            + ( (max_height
                 - rows * cards_height)
               / (rows - 1))
           ) - 1;

  }();
  auto const height = (cards_height
                       + (rows - 1) * cards_step_y);

  auto const x = [&]() {
    auto const outline_west = table().icongroup(Position::west).outline();
    auto const x_west       = outline_west.x + outline_west.width + table().layout().border_x;
    auto const outline_east = table().icongroup(Position::east).outline();
    auto const x_east       = outline_east.x - table().layout().border_x;
    return (x_east + x_west - width) / 2;
  }();
  auto const y = [&]() {
    auto const outline_south = table().icongroup(Position::south).outline();
    auto const y_south       = outline_south.y - table().layout().border_y;
    auto const outline_north = table().icongroup(Position::north).outline();
    auto const y_north       = outline_north.y + outline_north.height + table().layout().border_y;
    if (y_south - y_north <= height)
      return y_south;
    else
      return (y_north + y_south + height) / 2;
  }();

  Geometry geometry;
  geometry.x = x;
  geometry.y = y;
  geometry.width  = width;
  geometry.height = height;
  geometry.rows    = rows;
  geometry.columns = columns;
  geometry.cards_step_x       = cards_step_x;
  geometry.cards_small_step_x = cards_small_step_x;
  geometry.cards_step_y       = cards_step_y;

  return geometry;
}

/** @return   the outline of the hand
 **/
CardsDistribution::Outline
CardsDistribution::outline() const
{
  return geometry().outline();
}

/** -> result
 **
 ** @param    x   x position
 ** @param    y   y position
 **
 ** @return   the number of the card at the given position,
 **      UINT_MAX, if there is no card
 **
 ** @todo   check for transparency
 **/
optional<unsigned>
CardsDistribution::cardno_at_position(int x, int y) const
{
  // note: the position of the hand is complex to calculate
  // (see CardsDistribution::draw()), so since it does not take much time to get
  // through all cards I just check all cards.
  auto const geometry = this->geometry();
  geometry.retransform(x, y);
  auto const cards = this->cards();
  auto const& rule = game().rule();
  auto const number_of_same_cards = rule(Rule::Type::number_of_same_cards);
  auto const rows               = geometry.rows;
  auto const columns            = geometry.columns;
  auto const cards_step_x       = geometry.cards_step_x;
  auto const cards_small_step_x = geometry.cards_small_step_x;
  auto const cards_step_y       = geometry.cards_step_y;
  auto const card_width  = ui->cards->width();
  auto const card_height = ui->cards->height();
  for (auto cb = cards.cardsnumber_all();
       cb > 0;
       --cb) {
    unsigned const c = cb - 1;
    if (cards.played(c))
      continue;

    int const n = c / number_of_same_cards;
    int const i = c % number_of_same_cards;
    int const column = n % columns;
    int const row = n / columns;
    int const pos_x = (cards_step_x * column
                       + cards_small_step_x * i);
    int const pos_y = -cards_step_y * (rows - 1 - row);

    if (   x >= pos_x
        && x < pos_x + card_width
        && y < pos_y
        && y >= pos_y - card_height
       ) {
      // the position is over the card
      // ToDo: check for transparency
      return c;
    }
  } // for (cb)

  return {};
} // optional<unsigned> CardsDistribution::cardno_at_position(int x, int y) const

/** -> result
 **
 ** @param    x   x position
 ** @param    y   y position
 **
 ** @return   the card at the given position
 **      Card(), if there is no card
 **/
Card
CardsDistribution::card_at_position(int const x, int const y) const
{
  auto const cardnox = cardno_at_position(x, y);

  if (!cardnox)
    return Card();

  auto const& cardno = *cardnox;

  return cards().card_all(cardno);
}

/** move the card from the distribution in the hand of the current player
 **
 ** @param   c   number of the card
 **/
void
CardsDistribution::move_card_in_hand(unsigned c)
{
  auto hand = const_cast<Player&>(game().players().current_player()).hand().sorted();
  if (!hand.contains_unknown_or_played())
    return ;
  auto cards = this->cards();
  cards.request_position_all(c);
  auto const& card = cards.requested_card();
  cards.playcard(card);
  hand.replace_unknown_or_played_with(card);
  if (!hand.contains_unknown_or_played()) {
    rotate_to_next_hand() ;
  }
}

/** move the card from the distribution in the hand of the current player
 **
 ** @param   card   card
 **/
void
CardsDistribution::move_card_in_hand(Card const card)
{
  auto hand = const_cast<Player&>(game().players().current_player()).hand().sorted();
  if (!hand.contains_unknown_or_played())
    return ;
  auto cards = this->cards();
  cards.playcard(card);
  hand.replace_unknown_or_played_with(card);
  if (!hand.contains_unknown_or_played()) {
    rotate_to_next_hand() ;
  }
}

/** move the card from the hand of the current player into the cards distribution
 **
 ** @param   c   numeber of the card in the hand
 **/
void
CardsDistribution::remove_card_from_hand(Player& player, unsigned c)
{
  auto hand = player.hand().sorted();
  if (hand.played(c))
    return ;
  auto const card = hand.card_all(c);
  if (card == Card::unknown)
    return ;
  hand.playcard_all(c);
  auto cards = this->cards();
  cards.unplaycard(card);
  auto& game = const_cast<Game&>(this->game());
  auto& players = game.players();
  players.set_current_player(player);
  table().draw_all();
}

/** remove all cards from the hand of the player
 **/
void
CardsDistribution::remove_all_cards_from_hand(Player& player)
{
  auto hand = player.hand().sorted();
  for (unsigned i = 0; i < hand.cardsnumber_all(); ++i) {
    auto const card = hand.card_all(i);
    if (   !card
        || card == Card::unknown
        || hand.played(i)) {
      continue;
    }
    hand.playcard_all(i);
    cards().unplaycard(card);
  }

  auto& game = const_cast<Game&>(this->game());
  auto& players = game.players();
  players.set_current_player(player);
  table().draw_all();
}

/** save the hand of the human player in order to set it as default
 **/
void
CardsDistribution::save_hand_of_human_player(::Hand const& hand)
{
  saved_hand_of_human_player_ = hand;
}

/** set the hand of the human player to the saved one
 **/
void
CardsDistribution::set_hand_of_human_player()
{
  auto& game = const_cast<Game&>(this->game());
  auto& players = game.players();
  auto const& human = players.player(0);
  if (human.type() != Player::Type::human)
    return ;

  players.set_current_player(human);
  for (auto const& card : saved_hand_of_human_player_) {
    move_card_in_hand(card);
  }
}

/** rotate the table to the next hand with unknown cards
 **/
void
CardsDistribution::rotate_to_next_hand()
{
  auto& game = const_cast<Game&>(this->game());
  auto& players = game.players();
  auto const p = players.current_player().no();
  do {
    players.next_current_player();
    if (p == players.current_player().no()) {
      players.set_current_player(game.startplayer());
      break;
    }
  } while (!players.current_player().hand().contains_unknown_or_played()) ;
  if (check_all_cards_distributed())
    finish();
}

/** @return   whether all cards are distributed (or remaining go to one player)
 **/
bool
CardsDistribution::check_all_cards_distributed()
{
  auto const& players = game().players();
  auto n = 0;
  for (auto const& p : players) {
    if (p.hand().contains_unknown_or_played())
      n += 1;
  }
  return n <= 1;
}

/** finish the cards distribution
 **/
void
CardsDistribution::finish()
{
  finished_ = true;
}

/** button press event
 **
 ** @button_event   the button
 **
 ** @return   ob der Mausknopf angenommen wurde
 **/
bool
CardsDistribution::on_button_press_event(GdkEventButton* button_event)
{
  if (game().status() != Game::Status::manual_cards_distribution)
    return false;

  int const x = static_cast<int>(button_event->x);
  int const y = static_cast<int>(button_event->y);
  {
    auto const cardno = cardno_at_position(x, y);
    if (cardno) {
      if (button_event->button == 1) {
        move_card_in_hand(*cardno);
        table().draw_all();
        return true;
      } else if (button_event->button == 3) {
        finished_ = true;
        return true;
      }
    }
  }
  for (auto& player : ui->game().players()) {
    auto const& hand = table().hand(player);
    auto const cardno = hand.cardno_at_position(x, y);
    auto const card = hand.card_at_position(x, y);
    if (cardno) {
      if (button_event->button == 1) {
        if (card != Card::unknown) {
          remove_card_from_hand(player, *cardno);
          return true;
        } else {
          const_cast<Game::Players&>(ui->game().players()).set_current_player(player);
          table().draw_all();
          return true;
        }
      } else if (button_event->button == 3) {
        if (card != Card::unknown) {
          remove_all_cards_from_hand(player);
          return true;
        }
        const_cast<Game::Players&>(ui->game().players()).set_current_player(player);
        table().draw_all();
        return true;
      }
    }
  }
  return false;
}

/** @return   the current status
 **/
CardsDistribution::Status
CardsDistribution::current_status() const
{
  return {cards_};
}

/** comparison operator
 **/
bool
operator!=(CardsDistribution::Status const& lhs, CardsDistribution::Status const& rhs)
{
  return (lhs.hand != rhs.hand);
}

/** write the status in ostr
 **
 ** @param    ostr     output stream
 ** @param    status   status to write
 **
 ** @return   output stream
 **/
ostream&
operator<<(ostream& ostr, CardsDistribution::Status const& status)
{
  ostr << "hand = " << status.hand.cardsnumber();
  return ostr;
}

/** @return   the outline of the hand
 **/
CardsDistribution::Outline
CardsDistribution::Geometry::outline() const
{
  return transform({0, -height,
                   width, height});
}

/** write the geometry in ostr
 **
 ** @param    ostr     output stream
 ** @param    geometry   geometry to write
 **
 ** @return   output stream
 **/
ostream&
operator<<(ostream& ostr, CardsDistribution::Geometry const& geometry)
{
  ostr << "geometry: " << geometry.width
    << 'x' << geometry.height
    << '+' << geometry.x << '+' << geometry.y
    << '\n';
  ostr << "rows x columns: " << geometry.rows << 'x' << geometry.columns << '\n';
  ostr << "steps: " << geometry.cards_step_x
    << '/' << geometry.cards_small_step_x
    << '+' << geometry.cards_step_y
    << '\n';
  return ostr;
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
