/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "hand.h"
#include "icongroup.h"
#include "trickpile.h"
#include "name.h"

#include "../table.h"
#include "../card_suggestion.h"
#include "../ui.h"

#include "../cards.h"

#include "../../../party/party.h"
#include "../../../game/game.h"
#include "../../../party/rule.h"
#include "../../../player/player.h"
#include "../../../player/human/human.h"
#include "../../../player/cards_information.of_player.h"
#include "../../../card/trick.h"
#include "../../../misc/preferences.h"

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    table   the table
 ** @param    position   the corresponding position
 **/
Hand::Hand(Table& table, Position const position) :
  HTIN(table, position),
  last_status_{false, false, false, nullptr, {}}
{ }

/** destructor
 **/
Hand::~Hand() = default;

/**
 **/
bool
Hand::changed() const
{
  if (!surface_)
    return true;
  if (ui->party().in_game())
    return (last_status_ != current_status());
  return true;
} // bool Hand::changed() const

/** draws the hand
 **
 ** @param    cr       drawing context
 **/
void
Hand::draw(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!ui->party().in_game())
    return ;
  if (!(   game().status() >= Game::Status::init
        && game().status() <= Game::Status::finished) )
    return ;

  if (   game().status() == Game::Status::finished
      && !table().in_game_review()) {
    draw_tricks(cr);
    return ;
  }

  Game const& game = this->game();

  cr->save();
  cr->push_group();

  last_status_ = current_status();
  auto const& show = last_status_.show_hand;
  auto const& show_ai_information_hand = last_status_.show_ai_information_hand;
  auto const& ai = last_status_.ai;
  auto const sorted_hand = (!show_ai_information_hand
                            ? this->sorted_hand()
                            : SortedHand(last_status_.hand) );
  auto const geometry = this->geometry();
  geometry.transform(cr);

  // for showing the 'must have' cards
  Card last_card;
  unsigned last_card_no = 0;

  // draw the cards
  unsigned no = 0;
  for (unsigned c = 0; c < sorted_hand.cardsnumber_all(); c++) {
    if (table().in_game_review()) {
      if (sorted_hand.played_trick(c) < table().game_review_trickno()) {
        if (::preferences(::Preferences::Type::static_hands))
          no += 1;
        continue;
      }
    }

    if (!( (show || show_ai_information_hand
          ? !sorted_hand.played(c)
          : !sorted_hand.hand().played(c) )
          || game.status() == Game::Status::manual_cards_distribution
          || game.status() == Game::Status::finished)) {
      if (::preferences(::Preferences::Type::static_hands))
        no += 1;
      continue;
    }

    auto card = (show || show_ai_information_hand
                 ? ui->cards->card(sorted_hand.card_all(c))
                 : ui->cards->back()
                ).pixbuf();
    if (   game.status() == Game::Status::manual_cards_distribution
        && sorted_hand.played(c)) {
      card = ui->cards->back().pixbuf();
    }

    int pos_x = geometry.cards_step_x * no;
    int pos_y = 0;
    if (last_card != sorted_hand.card_all(c)) {
      last_card_no = 0;
      last_card = sorted_hand.card_all(c);
    }
    if (show_ai_information_hand
        && !(last_card_no < ai->cards_information().of_player(player()).must_have(sorted_hand.card_all(c))) ) {
      Glib::RefPtr<Gdk::Pixbuf> copy = card->copy();
      card->saturate_and_pixelate(copy, 5, true);
      card = copy;
    } else {
      last_card_no += 1;
    }

    // emphasize the valid cards
    if (   ::preferences(::Preferences::Type::emphasize_valid_cards)
        && game.status() == Game::Status::play
        && game.players().current_player() == player()
        && player().type() == Player::Type::human) {
      if (!game.tricks().current().isvalid(sorted_hand.card_all(c),
                                           sorted_hand.hand())) {
        Glib::RefPtr<Gdk::Pixbuf> copy = card->copy();
        card->saturate_and_pixelate(copy, 0, true);
        card = copy;
      } // if (!card valid)
    } // if (emphasize valid cards)
    if (   table().card_suggestion_
        && (player() == game.players().current_player())
        && (c == sorted_hand.requested_position_all()) ) {
      pos_y -= geometry.skip_y;
    } // if (suggested card)

    draw_pixbuf(cr, card, pos_x, pos_y - card->get_height());
    no += 1;
  } // for (c < sorted_hand.cardsnumber_all()
  cr->pop_group_to_source();
  cr->paint();
  cr->restore();
} // void Hand::draw(Cairo::RefPtr<::Cairo::Context> cr)

/** draw the tricks the player has got
 **
 ** @param    cr       cairo context
 **/
void
Hand::draw_tricks(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!ui->party().in_game())
    return ;
  if (!(   game().status() >= Game::Status::init
        && game().status() <= Game::Status::finished))
    return ;

  auto const geometry = this->geometry();
  auto width = geometry.width;
  auto const& game = this->game();
  auto const& trickpile = player().trickpile();

  // gap between two tricks
  int constexpr trick_gap = 2;
  // number of gaps
  int const gapsno = (trickpile.size() * (game.playerno() - 1 + trick_gap) - trick_gap);

  cr->save();
  cr->push_group();
  geometry.transform(cr);

  int const card_step_x
    = (  (gapsno == 0)
       ? 0
       : min(ui->cards->width() / 4,
             (width - ui->cards->width()) / gapsno)
      );
  int const trick_step_x
    = (  (trickpile.size() <= 1)
       ? 0
       : min(ui->cards->width() + trick_gap * card_step_x,
             static_cast<int>((width - ui->cards->width()
                               - trickpile.size() * (game.playerno() - 1) * card_step_x
                              ) / (trickpile.size() - 1)))
      );
  for (size_t t = 0; t < trickpile.size(); ++t) {
    auto const& trick = trickpile.trick(t);
    for (size_t c = 0; c < trick.actcardno(); ++c) {
      int pos_x = (t * trick_step_x
                   + (t * (game.playerno() - 1) + c) * card_step_x);
      int pos_y = 0;
      if (trick.is_specialpoints_card(c)) {
        pos_y -= geometry.skip_y;
      } // if (trick.is_specialpoints_card(c))

      Glib::RefPtr<Gdk::Pixbuf>
        card = ui->cards->card(trick.card(c));
      if (c != trick.winnercardno()) {
        Glib::RefPtr<Gdk::Pixbuf> copy = card->copy();
        card->saturate_and_pixelate(copy, 0, true);
        card = copy;
      } // if (c != trick.winnercardno())

      draw_pixbuf(cr, card, pos_x, pos_y - card->get_height());
    } // for (c < trick.actcardno())
  } // for (t < trickpile.size())
  cr->pop_group_to_source();
  cr->paint();
  cr->restore();
} // void Hand::draw_tricks(Cairo::RefPtr<::Cairo::Context> cr)

auto Hand::geometry() const -> Geometry
{
  Geometry geometry = table().layout_.hand.find(position())->second;
  geometry.skip_y = geometry.margin_y * 2 / 3;
  geometry.cards_step_x = [=](){
    auto const n = (!last_status_.show_ai_information_hand
                    ? sorted_hand().cardsnumber_all()
                    : last_status_.hand.cardsnumber_all() );
    return (geometry.width - ui->cards->width()) / (n - 1.0);
  }();
  return geometry;
}

auto Hand::outline() const -> Outline
{
  return geometry().outline();
}


auto Hand::sorted_hand() const -> SortedHand
{
  auto& hand = const_cast<::Hand&>(player().hand());
  return hand.sorted();
}

/** -> result
 **
 ** @param    x   x position
 ** @param    y   y position
 **
 ** @return   the number of the card at the given position,
 **      UINT_MAX, if there is no card
 **
 ** @todo   check for transparency
 **/
optional<unsigned>
Hand::cardno_at_position(int x, int y) const
{
  // note: the position of the hand is complex to calculate
  // (see Hand::draw()), so since it does not take much time to get
  // through all cards I just check all cards.
  auto const geometry = this->geometry();
  geometry.retransform(x, y);
  auto const hand = sorted_hand();
  unsigned cb = hand.cardsnumber_all();
  if (!::preferences(::Preferences::Type::static_hands)
      && game().status() != Game::Status::manual_cards_distribution)
  {
    if (table().in_game_review()) {
      cb -= table().game_review_trickno();
    } else {
      cb = hand.cardsnumber();
    }
  }
  for (; cb > 0; --cb) {
    unsigned const c = (   (::preferences(::Preferences::Type::static_hands)
                            || game().status() == Game::Status::manual_cards_distribution)
                        ? cb - 1
                        : hand.pos_to_pos_all(cb - 1));
    if (   hand.played(c)
        && game().status() != Game::Status::manual_cards_distribution)
    {
      continue;
    }

    int pos_x = geometry.cards_step_x * (cb - 1);
    int pos_y = 0;

    // emphasize suggested card
    if (   table().card_suggestion_
        && player() == game().players().current_player()
        && c == hand.requested_position() ) {
      pos_y -= geometry.skip_y;
    } // if (suggested card

    if (   x >= pos_x
        && x < pos_x + ui->cards->width()
        && y < pos_y
        && y >= pos_y - ui->cards->height()
       ) {
      // the position is over the card
      // ToDo: check for transparency
      return c;
    }
  } // for (cb)

  return {};
} // optional<unsigned> Hand::cardno_at_position(int x, int y) const

/** -> result
 **
 ** @param    x   x position
 ** @param    y   y position
 **
 ** @return   the card at the given position
 **      Card(), if there is no card
 **/
Card
Hand::card_at_position(int const x, int const y) const
{
  auto const cardnox = cardno_at_position(x, y);

  if (!cardnox)
    return Card();

  auto const& cardno = *cardnox;

  return sorted_hand().card_all(cardno);
} // Card Hand::card_at_position(int x, int y) const

/** @return   the current status
 **/
Hand::Status
Hand::current_status() const
{
  auto const& game   = this->game();
  auto const& player = this->player();

  bool const show_tricks = (  game.status() == Game::Status::finished
                            && !table().in_game_review());
  bool show_hand = ::preferences(::Preferences::Type::show_all_hands);
  if (game.status() == Game::Status::finished) {
    show_hand = true;
  }
  if (game.status() == Game::Status::manual_cards_distribution) {
    show_hand = true;
  }

  if (   game.status() == Game::Status::redistribute
      && game.players().exists_soloplayer()
      && player == game.players().soloplayer() ) {
    show_hand = true;
  }
  if (player.type() == Player::Type::human) {
    if (game.players().count_humans() == 1) {
      show_hand = true;
    } else {
      show_hand = (player == game.players().current_player());
    }
  }
#if !defined(RELEASE) && defined(DKNOF)
  if (   game.status() != Game::Status::poverty_shift
      && position() == Position::south) {
    show_hand = true;
  }
#endif

  // the ai to show the cards information of
  Ai const* const ai
    = (  (game.players().count_humans() == 1)
#if !defined(RELEASE) && defined(DKNOF)
       && false
#endif
       ? dynamic_cast<Ai const*>(&game.players().human())
       : dynamic_cast<Ai const*>(&hand(Position::south).player()));
  bool const show_ai_information_hand
    = (   !show_hand
       && game.status() >= Game::Status::play
       && ::preferences(::Preferences::Type::show_ai_information_hands)
       && ai != nullptr);

  // the hand to draw
  auto const hand(!show_ai_information_hand
                  ? player.hand()
                  : ai->cards_information().estimated_hand(player));

  return {show_tricks, show_hand, show_ai_information_hand, ai, hand};
} // Hand::Status Hand::current_status() const

/** comparison operator
 **/
bool
operator!=(Hand::Status const& lhs, Hand::Status const& rhs)
{
  if (lhs.show_tricks != rhs.show_tricks)
    return true;
  if (lhs.show_tricks)
    return false;
  if (lhs.show_hand != rhs.show_hand)
    return true;
  if (lhs.show_hand == true) {
    if (lhs.show_ai_information_hand != rhs.show_ai_information_hand)
      return true;
  }
  return (lhs.hand != rhs.hand);
} // bool operator!=(Hand::Status lhs, Hand::Status rhs)

/** write the status in ostr
 **
 ** @param    ostr     output stream
 ** @param    status   status to write
 **
 ** @return   output stream
 **/
ostream&
operator<<(ostream& ostr, Hand::Status const& status)
{
  ostr << "show_tricks = " << status.show_tricks << ", "
    << "show_hand = " << status.show_hand << ", "
    << "show_ai_information_hand = " << status.show_ai_information_hand << ", "
    << "ai = " << (status.ai ? status.ai->name() : "--"s) << ", "
    << "hand = " << status.hand.cardsnumber();
  return ostr;
} // ostream& operator<<(ostream& ostr, Hand::Status const& status)

auto Hand::Geometry::outline() const -> Outline
{
  return transform({0, -skip_y - height,
                   width, height + skip_y});
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
