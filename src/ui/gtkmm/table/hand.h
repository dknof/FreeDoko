/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "htin.h"

#include "../../../card/card.h"
#include "../../../card/hand.h"
#include "../../../card/sorted_hand.h"

class Ai;

namespace UI_GTKMM_NS {

/**
 ** the graphic representation of a sorted hand
 **
 ** @todo   use of 'Gdk::Point' and 'Gdk::Rectangle'
 **/
class Hand : public HTIN {
public:
  /**
   ** The status for checking changes (for redrawing)
   ** @todo: card suggestion
   **/
  struct Status {
    bool show_tricks = true;
    bool show_hand = false;
    bool show_ai_information_hand = false;
    Ai const* ai = nullptr;
    ::Hand hand;
  }; // struct Status

  struct Geometry : public TableLayoutGeometry {
    int skip_y = 0;
    int cards_step_x = 0;

    Geometry(TableLayoutGeometry const& geometry) : TableLayoutGeometry(geometry) { }

    auto outline() const -> Outline final;
  };

public:
  explicit Hand(Table& table, Position position);
  ~Hand() override;

  Hand(Hand const&) = delete;
  Hand& operator=(Hand const&) = delete;

  bool changed() const override;
  auto geometry() const -> Geometry;
  auto outline()  const -> Outline override;

  void draw(Cairo::RefPtr<::Cairo::Context> cr) override;
  void draw_tricks(Cairo::RefPtr<::Cairo::Context> cr);

  SortedHand sorted_hand() const;

  optional<unsigned> cardno_at_position(int x, int y) const;
  Card card_at_position(int x, int y) const;

public:
  //private:
  Status current_status() const;
  Status last_status_;
}; // class Hand : public HTIN

bool operator!=(Hand::Status const& lhs, Hand::Status const& rhs);
ostream& operator<<(ostream& ostr, Hand::Status const& status);

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
