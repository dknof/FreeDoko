/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "htin.h"
#include "../table.h"
#include "../ui.h"

#include "../../../game/game.h"
#include "../../../player/player.h"

#include <gdkmm/general.h>

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    table      the table
 ** @param    position   the position of the element
 **/
HTIN::HTIN(Table& table, Position const position) :
  Table::Part{table},
  position_{position}
{ }

/** destructor
 **/
HTIN::~HTIN()
= default;

/** @return   the position of the object
 **/
Position
HTIN::position() const
{
  return position_;
} // Rotation HTIN::rotation() const

/** @return   the rotation of the object
 **/
Rotation
HTIN::rotation() const
{
  return table().rotation(player());
} // Rotation HTIN::rotation() const

/** -> result
 **
 ** @param    position   the position
 **
 ** @return   the rotation of the object at 'position'
 **/
Rotation
HTIN::rotation(Position const position) const
{
  return table().rotation(position);
} // Rotation HTIN::rotation(Position position) const

/** @return   the corresponding player
 **/
Player const&
HTIN::player() const
{
  return table().player(position());
} // Player const& HTIN::player() const

/** @return   the corresponding hand
 **/
Hand&
HTIN::hand()
{
  return table().hand(player());
} // Hand& HTIN::hand()

/** @return   the corresponding hand
 **/
Hand const&
HTIN::hand() const
{
  return table().hand(position());
} // Hand const& HTIN::hand() const

/** -> result
 **
 ** @param    position   the position
 **
 ** @return   the hand at 'position'
 **/
Hand&
HTIN::hand(Position const position)
{
  return table().hand(position);
} // Hand& HTIN::hand(Position position)

/** -> result
 **
 ** @param    position   the position
 **
 ** @return   the hand at 'position'
 **/
Hand const&
HTIN::hand(Position const position) const
{
  return table().hand(position);
} // Hand const& HTIN::hand(Position position)

/** @return   the corresponding trickpile
 **/
TrickPile&
HTIN::trickpile()
{
  return table().trickpile(position());
} // TrickPile& HTIN::trickpile()

/** @return   the corresponding trickpile
 **/
TrickPile const&
HTIN::trickpile() const
{
  return table().trickpile(position());
} // TrickPile const& HTIN::trickpile() const

/** -> result
 **
 ** @param    position   the position
 **
 ** @return   the trickpile at 'position'
 **/
TrickPile&
HTIN::trickpile(Position const position)
{
  return table().trickpile(position);
} // TrickPile& HTIN::trickpile(Position position)

/** -> result
 **
 ** @param    position   the position
 **
 ** @return   the trickpile at 'position'
 **/
TrickPile const&
HTIN::trickpile(Position const position) const
{
  return table().trickpile(position);
} // TrickPile const& HTIN::trickpile(Position position) const

/** @return   the corresponding icongroup
 **/
Icongroup&
HTIN::icongroup()
{
  return table().icongroup(position());
} // Icongroup& HTIN::icongroup()

/** @return   the corresponding icongroup
 **/
Icongroup const&
HTIN::icongroup() const
{
  return table().icongroup(position());
} // Icongroup const& HTIN::icongroup() const

/** -> result
 **
 ** @param    position   the position
 **
 ** @return   the icongroup at 'position'
 **/
Icongroup&
HTIN::icongroup(Position const position)
{
  return table().icongroup(position);
} // Icongroup& HTIN::icongroup(Position position)

/** -> result
 **
 ** @param    position   the position
 **
 ** @return   the icongroup at 'position'
 **/
Icongroup const&
HTIN::icongroup(Position const position) const
{
  return table().icongroup(position);
} // Icongroup const& HTIN::icongroup(Position position) const

/** @return   the corresponding name
 **/
Name&
HTIN::name()
{
  return table().name(position());
} // Name& HTIN::name()

/** @return   the corresponding name
 **/
Name const&
HTIN::name() const
{
  return table().name(position());
} // Name const& HTIN::name() const

/** -> result
 **
 ** @param    position   the position
 **
 ** @return   the name at 'position'
 **/
Name&
HTIN::name(Position const position)
{
  return table().name(position);
} // Name& HTIN::name(Position position)

/** -> result
 **
 ** @param    position   the position
 **
 ** @return   the name at 'position'
 **/
Name const&
HTIN::name(Position const position) const
{
  return table().name(position);
} // Name const& HTIN::name(Position position) const

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
