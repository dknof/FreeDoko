/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "icongroup.h"
#include "hand.h"
#include "name.h"

#include "../table.h"
#include "../ui.h"

#include "../cards.h"
#include "../icons.h"

#include "../../../party/party.h"
#include "../../../game/game.h"
#include "../../../party/rule.h"
#include "../../../player/player.h"
#include "../../../player/human/human.h"
#include "../../../misc/preferences.h"

namespace UI_GTKMM_NS {

Icongroup::Icongroup(Table& table, Position const position) :
  HTIN(table, position)
{ }

Icongroup::~Icongroup()
= default;

auto Icongroup::changed() const -> bool
{
  if (!surface_)
    return true;
  if (ui->party().in_game())
    return (last_status_ != current_status());
  return true;
}

void Icongroup::draw(Cairo::RefPtr<::Cairo::Context> cr)
{
  cr->save();
  cr->push_group();
  last_status_ = current_status();
  draw_team(cr);
  draw_swines(cr);
  draw_announcement(cr);
  cr->pop_group_to_source();
  cr->paint();
  cr->restore();
}

void Icongroup::draw_team(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!ui->party().in_game())
    return ;

  if (   game().status() < Game::Status::reservation
      && !(   game().players().exists_soloplayer()
           && player() == game().players().soloplayer()))
    return ;
  if (   game().status() > Game::Status::reservation
      && is_solo(game().type())
      && player() != game().players().soloplayer())
    return ;
  if (!last_status_.team_icon)
    return ;

  auto icon = ui->icons->icon(last_status_.team_icon);
  if (!icon)
    return;

  if (last_status_.team_guessed) {
    Glib::RefPtr<Gdk::Pixbuf> copy = icon->copy();
    icon->saturate_and_pixelate(copy, 5, true);
    icon = copy;
  }

  cr->save();
  cr->push_group();
  auto geometry = this->geometry();
  geometry.transform(cr);
  draw_pixbuf(cr, icon,
              (  geometry.flip_x
               ? geometry.width - icon->get_width()
               : 0),
              -icon->get_height());
  cr->pop_group_to_source();
  cr->paint();
  cr->restore();
} // void Icongroup::draw_team(Cairo::RefPtr<::Cairo::Context> cr)

void Icongroup::draw_announcement(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!ui->party().in_game())
    return ;
  if (game().status() <= Game::Status::reservation)
    return ;

  auto const announcement = last_status_.announcement;
  if (announcement == Announcement::noannouncement)
    return ;

  auto icon = (announcement == Announcement::reply
               ? ui->icons->icon_reply(game().announcements().last(opposite(player().team())))
               : ui->icons->icon(announcement));
  if (last_status_.next_announcement) {
    Glib::RefPtr<Gdk::Pixbuf> copy = icon->copy();
    icon->saturate_and_pixelate(copy, 4, true);
    icon = copy;
  }

  cr->save();
  cr->push_group();
  auto const geometry = this->geometry();
  geometry.transform(cr);
  draw_pixbuf(cr, icon,
              (geometry.width - icon->get_width()) / 2,
              -icon->get_height());
  cr->pop_group_to_source();
  cr->paint();
  cr->restore();
} // void Icongroup::draw_announcement(Cairo::RefPtr<::Cairo::Context> cr)

void Icongroup::draw_swines(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!ui->party().in_game())
    return ;

  Glib::RefPtr<Gdk::Pixbuf> icon;

  auto const swines      = last_status_.swines;
  auto const hyperswines = last_status_.hyperswines;
  auto const trumpcolor  = last_status_.trumpcolor;
  if (swines && hyperswines) {
    icon = ui->icons->swines_hyperswines(trumpcolor);
  } else if (swines) {
    icon = ui->icons->swines(trumpcolor);
  } else if (hyperswines) {
    icon = ui->icons->hyperswines(trumpcolor);
  } else {
    return ;
  }

  auto const geometry = this->geometry();
  cr->save();
  cr->push_group();
  geometry.transform(cr);
  draw_pixbuf(cr, icon,
              (  geometry.flip_x
               ? 0
               : geometry.width - icon->get_width()),
              -icon->get_height());

  cr->pop_group_to_source();
  cr->paint();
  cr->restore();
} // void Icongroup::draw_swines(Cairo::RefPtr<::Cairo::Context> cr)

auto Icongroup::geometry() const -> Geometry
{
  Geometry geometry = table().layout().icongroup.find(position())->second;
  if (position() == Position::north)
    geometry.flip_x = true;
  return geometry;
}

auto Icongroup::outline() const -> Outline
{
  return geometry().outline();
}

auto Icongroup::mouse_over_next_announcement() const -> bool
{
  if (!::preferences(::Preferences::Type::announce_in_table))
    return false;

  if (!::is_real(player().team()))
    return false;

  auto const announcement = player().next_announcement();

  if (announcement == Announcement::noannouncement)
    return false;

  int x = 0, y = 0;
  table().get_pointer(x, y);

  auto icon = (announcement == Announcement::reply
               ? ui->icons->icon_reply(game().announcements().last(opposite(player().team())))
               : ui->icons->icon(announcement));
  auto const geometry = this->geometry();
  geometry.retransform(x, y);
  return (   x >= (geometry.width - icon->get_width()) / 2
          && x <  (geometry.width + icon->get_width()) / 2
          && y < 0
          && y >= -icon->get_height());
} // bool Icongroup::mouse_over_next_announcement() const

auto Icongroup::current_status() const -> Icongroup::Status
{
#ifdef TEST_LAYOUT
  return {Icons::Type::thrown_nines_and_kings, false,
    Announcement::no120, false,
    Card::Color::club, true, true};
#endif
  auto const& game = this->game();
  Team const team = game.teaminfo().get_human_teaminfo(player());
  auto const* const soloplayer
    = (game.players().exists_soloplayer()
       ? &game.players().soloplayer()
       : ((game.type() == GameType::normal)
          ? nullptr
          : &game.players().current_player()));

  auto team_icon = Icons::Type::none;
  auto team_guessed = false;
  if (game.status() == Game::Status::reservation) {
    if (player().type() == Player::Type::human) {
      ::Reservation const& reservation = player().reservation();
      switch (reservation.game_type) {
      case GameType::normal:
        team_icon = Icons::type(player().hand().contains(Card::club_queen)
                                ? Team::re : Team::contra);
        break;
      case GameType::poverty:
        {
          switch (player().hand().count(Card::trump)) {
          case 0:
            team_icon = Icons::Type::poverty_trumps_0;
            break;
          case 1:
            team_icon = Icons::Type::poverty_trumps_1;
            break;
          case 2:
            team_icon = Icons::Type::poverty_trumps_2;
            break;
          case 3:
          case 4:
          case 5:
            team_icon = Icons::Type::poverty_trumps_3;
            break;
          case UINT_MAX:
            team_icon = Icons::Type::poverty;
            break;
          default:
            DEBUG_ASSERTION(false,
                            "IconGroup::draw_team():\n"
                            "  gametype: poverty\n"
                            "  number of poverty cards for player " << player().no() << ' ' << player().name() << " invalid: "
                            << player().hand().count(Card::trump) << '\n'
                            << "reservation:\n"
                            << player().reservation() << '\n'
                            << "game status: " << game.status());
            team_icon = Icons::Type::poverty;
            break;
          } // switch (player().hand().count(Card::trump))
        }
        break;
      case GameType::marriage:
        team_icon = Icons::type(reservation.marriage_selector);
        break;
      case GameType::marriage_solo:
      case GameType::marriage_silent:
      case GameType::thrown_nines:
      case GameType::thrown_kings:
      case GameType::thrown_nines_and_kings:
      case GameType::thrown_richness:
      case GameType::fox_highest_trump:
      case GameType::redistribute:
      case GameType::solo_diamond:
      case GameType::solo_jack:
      case GameType::solo_queen:
      case GameType::solo_king:
      case GameType::solo_queen_jack:
      case GameType::solo_king_jack:
      case GameType::solo_king_queen:
      case GameType::solo_koehler:
      case GameType::solo_club:
      case GameType::solo_heart:
      case GameType::solo_spade:
      case GameType::solo_meatless:
        team_icon = Icons::type(reservation.game_type);
        break;
      } // switch (player().reservation().game_type())

    } // if (player().type() == Player::Type::human)

  } else if (   game.is_finished()
             || ::preferences(::Preferences::Type::show_soloplayer_in_game)) {
    switch (game.type()) {
    case GameType::normal:
      break;
    case GameType::poverty:
      if (player() == *soloplayer) {
        team_icon = Icons::type(game.type());
      } else if (   game.status() >= Game::Status::start
                 && player().team() == Team::re) {
        team_icon = Icons::Type::poverty_partner;
      }
      break;
    case GameType::marriage:
    case GameType::marriage_solo:
      if (player() == *soloplayer) {
        team_icon = Icons::type(game.marriage().selector());
      } else if (game.marriage().selector() == MarriageSelector::team_set) {
        if (team == Team::re) {
          team_icon = Icons::Type::marriage_partner;
        } else {
          team_icon = Icons::type(team);
        }
      } // if !(player == soloplayer)
      break;
    case GameType::marriage_silent:
      if (player() == *soloplayer) {
        team_icon = Icons::type(game.type());
      } else {
        team_icon = Icons::Type::none;
      }
      break;
    case GameType::thrown_nines:
    case GameType::thrown_kings:
    case GameType::thrown_nines_and_kings:
    case GameType::thrown_richness:
    case GameType::fox_highest_trump:
    case GameType::redistribute:
    case GameType::solo_diamond:
    case GameType::solo_jack:
    case GameType::solo_queen:
    case GameType::solo_king:
    case GameType::solo_queen_jack:
    case GameType::solo_king_jack:
    case GameType::solo_king_queen:
    case GameType::solo_koehler:
    case GameType::solo_club:
    case GameType::solo_heart:
    case GameType::solo_spade:
    case GameType::solo_meatless:
      if (player() == *soloplayer)
        team_icon = Icons::type(game.type());
      else
        team_icon = Icons::Type::none;
      break;
    } // switch(game.type())
  } // if (icon shall be shown for soloplayer)

  // special case: silent marriage
  if (   !team_icon
      && (game.players().count_humans() == 1)
      && (player().type() == Player::Type::human)
      && (   (   (game.type() == GameType::marriage_silent)
              && (player() == game.players().soloplayer()) )
          || (   (game.type() == GameType::normal)
              && (player().hand().count_all(Card::club_queen)
                  == game.rule(Rule::Type::number_of_same_cards)))
         )
     ) {
    team_icon = Icons::type(GameType::marriage_silent);
  }

  if (!team_icon) {
    if (   game.is_finished()
        || ::preferences(::Preferences::Type::show_known_teams_in_game)) {
      team_icon = Icons::type(team);
    } // if (show teams)
  } // if (!team_icon)

  if (   !team_icon
      && ::preferences(::Preferences::Type::show_ai_information_teams)
      && game.players().count_humans() == 1
      && game.players().human().teaminfo(player()) != GuessedTeam::unknown
      && game.type() == GameType::normal
     ) {
    team_icon = Icons::type(::to_real(game.players().human().teaminfo(player())));
    if (!!team_icon
        && !::is_real(game.players().human().teaminfo(player()))) {
      team_guessed = true;
    } // if (team is maybe)
  } // if (team_icon)


  bool next_announcement = false;
  auto announcement = player().announcement();
  if (   ::preferences(::Preferences::Type::announce_in_table)
      && player().type() == Player::Type::human
      && (mouse_over_next_announcement()
          || (   player().announcement()      == Announcement::noannouncement
              && player().next_announcement() != Announcement::noannouncement))) {
    auto announcement2 = player().next_announcement();
    if (!announcement2) {
      announcement2 = Announcement::no120;
    }
    if (game.announcements().is_valid(announcement2, player())) {
      next_announcement = true;
      announcement = announcement2;
    }
  }

  if (table().in_game_review()) {
    announcement = Announcement::noannouncement;
    for (auto a = game.announcements().all(player()).rbegin();
         a != game.announcements().all(player()).rend();
         ++a)
      if (a->trickno < table().game_review_trickno()) {
        announcement = a->announcement;
        break;
      }
  } // if (table().in_game_review())

  // Swines

  bool swines      = player().has_swines();
  bool hyperswines = player().has_hyperswines();
  if (   (game.status() == Game::Status::reservation && player().type() == Player::Type::human)
      || game.status() == Game::Status::start) {
    swines = swines || (   player().reservation().swines
                        && game.swines().swines_announcement_valid(player()));
    hyperswines = hyperswines || (   player().reservation().hyperswines
                                  && game.swines().hyperswines_announcement_valid(player()));
  }

  return {team_icon, team_guessed,
    announcement, next_announcement,
    game.cards().trumpcolor(), swines, hyperswines};
} // Icongroup::Status Icongroup::current_status() const

bool operator!=(Icongroup::Status const& lhs, Icongroup::Status const& rhs)
{
  return (   lhs.team_icon         != rhs.team_icon
          || lhs.team_guessed      != rhs.team_guessed
          || lhs.announcement      != rhs.announcement
          || lhs.next_announcement != rhs.next_announcement
          || lhs.trumpcolor        != rhs.trumpcolor
          || lhs.swines            != rhs.swines
          || lhs.hyperswines       != rhs.hyperswines);
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
