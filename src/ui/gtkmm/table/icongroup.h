/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "htin.h"
#include "../icons.h"
#include "../../../card/card.h"

namespace UI_GTKMM_NS {

/**
 ** the graphic representation of a hand
 **
 ** @todo   test overlapping of icons
 ** @todo   use of 'Gdk::Point' and 'Gdk::Rectangle'
 **/
class Icongroup : public HTIN {
  public:
    /**
     ** The status for checking changes (for redrawing)
     **/
    struct Status {
      Icons::Type team_icon;
      bool team_guessed;
      Announcement announcement;
      bool next_announcement;
      Card::Color trumpcolor;
      bool swines;
      bool hyperswines;
    }; // struct Status

    struct Geometry : public TableLayoutGeometry {
      bool flip_x = false;

      Geometry(TableLayoutGeometry const& geometry) : TableLayoutGeometry(geometry) { }
    };

  public:
    Icongroup(Table& table, Position position);
    Icongroup(Icongroup const&)            = delete;
    Icongroup& operator=(Icongroup const&) = delete;
    ~Icongroup() override;

    bool changed() const override;
    auto geometry() const -> Geometry;
    auto outline()  const -> Outline  override;

    void draw(Cairo::RefPtr<::Cairo::Context> cr) override;
    void draw_team(Cairo::RefPtr<::Cairo::Context> cr);
    void draw_announcement(Cairo::RefPtr<::Cairo::Context> cr);
    void draw_swines(Cairo::RefPtr<::Cairo::Context> cr);

    bool mouse_over_next_announcement() const;

    Status current_status() const;
  private:
    Status last_status_ = {};
}; // class Icongroup : public HTIN

bool operator!=(Icongroup::Status const& lhs, Icongroup::Status const& rhs);

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
