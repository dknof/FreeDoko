/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "name.h"
#include "hand.h"
#include "icongroup.h"

#include "../table.h"
#include "../ui.h"
#include "../cards.h"

#include "../../../player/player.h"
#include "../../../party/party.h"
#include "../../../party/rule.h"
#include "../../../game/game.h"
#include "../../../card/trick.h"

namespace UI_GTKMM_NS {

/** constructor
 **
 ** @param    table      the table
 ** @param    position   the position of the name
 **/
Name::Name(Table& table, Position const position) :
  HTIN(table, position),
  last_status_{"", false, "", 0}
{ }

/** destructor
 **/
Name::~Name() = default;

/** returns the surface
 ** If the surface is outdated, redraw it.
 ** Update width and height of the name
 **
 ** @return   surface with the drawing in it
 **/
Cairo::RefPtr<Cairo::ImageSurface>
Name::surface()
{
  HTIN::surface();

  auto cr = Cairo::Context::create(surface_);
  auto layout = Pango::Layout::create(cr);
  layout->set_font_description(Pango::FontDescription(::preferences(::Preferences::Type::name_font)));
  layout->set_text(player().name());
#ifdef WORKAROUND
  // Bei kursiven Schriften wird bei den Grenzen von layout->get_pixel_size() rechts etwas abgeschnitten
  auto r = layout->get_pixel_ink_extents();
  width_  = max(0, r.get_x()) + r.get_width();
  height_ = max(0, r.get_y()) + r.get_height();
#else
  layout->get_pixel_size(width_, height_);
#endif
  if (   width_  != surface_->get_width()
      || height_ != surface_->get_height() )
    surface_.clear();

  return HTIN::surface();
} // Cairo::RefPtr<Cairo::ImageSurface> Name::surface()


/** @return   whether the status has changed;
 **/
bool
Name::changed() const
{
  if (!surface_)
    return true;
  if (ui->party().in_game()) {
    if (last_status_ != current_status())
      return (last_status_ != current_status());
  }
  return true;
} // bool Name::changed() const

auto Name::geometry() const -> Geometry
{
  Geometry geometry = table().layout().name.find(position())->second;
  return geometry;
}

auto Name::outline() const -> Outline
{
  return geometry().outline();
}

int Name::width()  const
{
  return width_;
}

int Name::height() const
{
  return height_;
}

/** draws the name
 **
 ** @param    cr       cairo context
 **/
void
Name::draw(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!ui->party().in_game())
    return ;

  last_status_ = current_status();
  auto const is_active = last_status_.is_active;
  auto const color = Gdk::RGBA(last_status_.color);
  auto const progress = last_status_.progress;

  cr->save();
  cr->push_group();
  {
    cr->set_source_rgb(color.get_red(), color.get_green(), color.get_blue());
    auto layout = Pango::Layout::create(cr);
    layout->set_font_description(Pango::FontDescription(::preferences(::Preferences::Type::name_font)));
    layout->set_text(player().name());
    auto const width_bak  = width_;
    auto const height_bak = height_;
#ifdef WORKAROUND
    // Bei kursiven Schriften wird bei den Grenzen von layout->get_pixel_size() rechts etwas abgeschnitten
    auto r = layout->get_pixel_ink_extents();
    width_  = max(0, r.get_x()) + r.get_width();
    height_ = max(0, r.get_y()) + r.get_height();
#else
    layout->get_pixel_size(width_, height_);
#endif
    if (width_bak < width_ || height_bak < height_) {
      // Das Layout hängt von der Namensgröße ab!
      table().force_redraw_all();
    }
    auto const geometry = this->geometry();
    geometry.transform(cr);

    cr->translate(0, -geometry.height);
    layout->show_in_cairo_context(cr);
    cr->save();
    if (   is_active
        && (progress != 1) ) {
      {
        cr->rectangle(0, 0,
                      geometry.width * ui->progress(),
                      geometry.height);
        cr->clip();
        auto color = Gdk::RGBA(::preferences(::Preferences::Type::name_reservation_font_color));
        cr->set_source_rgb(color.get_red(), color.get_green(), color.get_blue());
        layout->show_in_cairo_context(cr);
      }
    } // if !(progress and active player)
    cr->restore();
  }

  cr->pop_group_to_source();
  cr->paint();
  cr->restore();
} // void Name::draw(Cairo::RefPtr<::Cairo::Context> cr)

/** @return   the current status
 **/
Name::Status
Name::current_status() const
{
  // test whether it is the active player
  bool is_active = false;
  switch (game().status()) {
  case Game::Status::manual_cards_distribution:
    is_active = (player()
                 == game().players().current_player());
    break;
  case Game::Status::reservation:
    // in the reservation show the current player
    if (game().players().current_player().type() == Player::Type::human) {
      // for the human show the startplayer
      if (   is_solo(game().type())
          && (game().rule(Rule::Type::lustsolo_player_leads)))
        is_active = (player()
                     == game().players().current_player());
      else
        is_active = (player()
                     == game().startplayer());
    } else {
      // for all other players show the player to wait for
      is_active = (player()
                   == game().players().current_player());
    }
    break;
  case Game::Status::start:
  case Game::Status::init:
    is_active = (player()
                 == game().players().current_player());
    break;
  case Game::Status::poverty_shift:
    if (game().players().current_player().type() == Player::Type::human)
      is_active = (player()
                   == game().startplayer());
    else
      is_active = (player()
                   == game().players().current_player());

    break;
  case Game::Status::play:
  case Game::Status::full_trick:
  case Game::Status::trick_taken:
    is_active = (player() == game().players().current_player());
    break;
  case Game::Status::finished:
    if (table().in_game_review())
      is_active = (   table().game_review_trick_visible()
                   && (player()
                       == table().game_review_trick().winnerplayer()));
    else
      is_active = (   (player().team()
                       == game().winnerteam())
                   || (   (game().winnerteam()
                           == Team::noteam)
                       && (player().team() == Team::re)) );
#ifdef POSTPONED
    if (gui().playfield().table().last_trick().trickno()
        == UINT_MAX) {
      is_active = (player().team()
                   == player().game().winnerteam());
    } else { // if (show a specific trick)
      is_active = (player()
                   == player().game().tricks().trick(gui().playfield().table().last_trick().trickno()).winnerplayer());
    } // if (show a specific trick)
#endif
    break;

  default:
    break;
  } // switch (game().status())

  auto color = ::preferences(is_active
                             ? ::Preferences::Type::name_active_font_color
                             : (   game().status() == Game::Status::reservation
                                && game().has_made_reservation(player())
                                && (game().reservation(player()).game_type
                                    != GameType::normal) )
                             ? ::Preferences::Type::name_reservation_font_color
                             : ::Preferences::Type::name_font_color);

  return {player().name(),
    is_active,
    color,
    ui->progress()};
} // Name::Status Name::current_status() const

/** comparison operator
 **/
bool
operator!=(Name::Status const& lhs, Name::Status const& rhs)
{
  return (   lhs.name      != rhs.name
          || lhs.is_active != rhs.is_active
          || lhs.color     != rhs.color
          || lhs.progress  != rhs.progress);
} // bool operator!=(Name::Status lhs, Name::Status rhs)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
