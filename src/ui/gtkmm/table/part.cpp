/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "part.h"
#include "../ui.h"

namespace UI_GTKMM_NS {

Table::Part::Part(Table& table) :
  Base{&table}
{ }

Table::Part::~Part() = default;

auto Table::Part::table() -> Table&
{
  return *dynamic_cast<Table*>(parent);
} // Table& Part::table()

auto Table::Part::table() const -> Table const&
{
  return *dynamic_cast<Table*>(parent);
}

auto Table::Part::game() const -> Game const&
{
  return ui->game();
}

/** returns the surface
 ** If the surface is outdated, redraw it.
 **
 ** @return   surface with the drawing in it
 **/
auto Table::Part::surface() -> Cairo::RefPtr<Cairo::ImageSurface>
{
  if (   surface_
      && !changed())
    return surface_;

  auto const outline = this->outline();
  surface_ = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32,
                                               outline.width,
                                               outline.height);
  auto cr = Cairo::Context::create(surface_);
  if (::debug("table")) {
    cr->save();
    cr->set_source_rgba(0, 0, 1, 0.2);
    cr->rectangle(0, 0, outline.width, outline.height);
    cr->fill();
    cr->restore();
  }
  //cr->set_source(surface_, outline.x(), outline.y());
  cr->set_source(surface_, 0, 0);
  cr->translate(-outline.x, -outline.y);
  draw(cr);

  return surface_;
} // Cairo::RefPtr<Cairo::ImageSurface> Table::Part::surface()

void Table::Part::force_redraw()
{
  surface_.clear();
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
