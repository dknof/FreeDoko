/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "../table.h"

namespace UI_GTKMM_NS {
/**
 ** a part of a table
 **/
class Table::Part : public Base {
public:
  using Geometry = TableLayoutGeometry;
  using Outline  = TableLayoutOutline;

public:
  explicit Part(Table& table);
  ~Part() override;

  Table& table();
  Table const& table() const;
  Game const& game() const;

  virtual Cairo::RefPtr<Cairo::ImageSurface> surface();

  // whether there was a change in the element
  virtual bool changed() const = 0;
  virtual auto outline() const -> Outline = 0;

  virtual void draw(Cairo::RefPtr<::Cairo::Context> cr) = 0;
  void force_redraw();

protected:
  Cairo::RefPtr<Cairo::ImageSurface> surface_;

private:
  Part() = delete;
  Part(Part const&) = delete;
  Part& operator=(Part const&) = delete;
}; // class Table::Part : public Base

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
