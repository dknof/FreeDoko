/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "poverty.h"
#include "hand.h"
#include "icongroup.h"
#include "name.h"

#include "../table.h"
#include "../ui.h"
#include "../cards.h"

#include "../../../basetypes/fast_play.h"
#include "../../../game/game.h"
#include "../../../party/rule.h"
#include "../../../player/player.h"
#include "../../../card/hand.h"
#include "../../../os/bug_report_replay.h"

#include <gtkmm/main.h>
#include <gdkmm/general.h>

namespace Gdk {
ostream& operator<<(ostream& ostr, Rectangle const& rectangle);
ostream& operator<<(ostream& ostr, Point const& point);
} // namespace Gdk

namespace UI_GTKMM_NS {
/** constructor
 **
 ** @param    table   parent object
 **/
Poverty::Poverty(Table& table) :
  Table::Part(table)
{ }

/** Destructor
 **/
Poverty::~Poverty() = default;

/** @return   the position of the poverty
 **/
Position
Poverty::position() const
{
  if (!player)
    return Position::center;
  return table().position(*player);
} // Position Poverty::position() const

/** @return   the corresponding hand
 **/
Hand const&
Poverty::hand() const
{
  return table().hand(position());
} // Hand Poverty::hand() const

/** @return   the corresponding hand
 **/
Hand&
Poverty::hand()
{
  return table().hand(position());
} // Hand& Poverty::hand()

/** @return   the corresponding icongroup
 **/
Icongroup const&
Poverty::icongroup() const
{
  return table().icongroup(position());
} // Icongroup Poverty::icongroup() const

/** @return   the corresponding name
 **/
Name const&
Poverty::name() const
{
  return table().name(position());
} // Icongroup Poverty::name() const

/** @return   whether the active player can shift
 **/
bool
Poverty::middle_full() const
{
  return !contains(cards, Card());
} // bool Poverty::middle_full() const

/** @return   whether the active player can shift
 **/
bool
Poverty::shifting_valid() const
{
  if (player->type() != Player::Type::human)
    return false;

  switch (status) {
  case Status::invalid:
    return false;
  case Status::asking:
    return true;
  case Status::shifting:
  case Status::shifting_back:
    return middle_full();
  case Status::getting_back:
    return false;
  case Status::accepted:
  case Status::denied_by_all:
  case Status::finished:
    return false;
  } // switch (status)

  return false;
} // bool Poverty::shifting_valid() const

/** 'player' shifts 'cardno' cards
 **
 ** @param    player   the player who shifts the cards
 ** @param    cardno   the number of cards that are shifted
 **/
void
Poverty::shift(Player const& player, unsigned const cardno)
{
  status = Status::shifting;

  if (   player.type() != Player::Type::human
      //&& (player.game().humanno() > 0)
      && !(::fast_play & FastPlay::pause)
      && !(::bug_report_replay && ::bug_report_replay->auto_action()) ) {
    shift_cards = false;
    cards = player.game().poverty().shifted_cards();
    this->player = &player;

    table().draw_all();

    while (!ui->thrower
           && game().status() == Game::Status::poverty_shift
           && shift_cards == false)
      ::ui->wait();
    if (ui->thrower)
      return ;
  } // if (player.type() != Player::Type::human)

  status = Status::asking;
}

/** 'player' denied to take the shifted cards
 **
 ** @param    player   the player who has denied to take the cards
 **/
void
Poverty::take_denied(Player const& player)
{ }

/** all players have denied to take the cards
 **/
void
Poverty::take_denied_by_all()
{
  player = &player->game().players().soloplayer();
  status = Status::denied_by_all;
} // void Poverty::take_denied_by_all()

/** 'player' accepts to take the shifted cards
 ** and returns 'cardno' cards with 'trumpno' trumps
 ** show the cards and wait for a click
 **
 ** @param    player   the player who has denied to take the cards
 ** @param    cardno   number of cards that are given back
 ** @param    trumpno   number of trumps of the cards
 **/
void
Poverty::take_accepted(Player const& player,
                       unsigned const cardno, unsigned const trumpno)
{
  status = Status::accepted;
  force_redraw();
  table().draw_all();
} // void Poverty::take_accepted(Player player, unsigned cardno, unsigned trumpno)

/** returns which cards the player shifts
 **
 ** @param    player   the player who shifts the cards (the soloplayer)
 **
 ** @return   the cards that are to be shifted
 **/
HandCards
Poverty::shift(Player& player)
{
  status = Status::shifting;

  cards = vector<Card>(player.hand().count_poverty_cards());
  this->player = &player;
  auto hand = player.hand().sorted();

  shift_cards = false;

  // put all trump in the middle
  unsigned j = 0;
  for (unsigned i = 0; i < hand.cardsnumber_all(); i++)
    if (hand.card_all(i).istrump()) {
      cards[j] = hand.card_all(i);
      hand.playcard(i - j);
      j += 1;
    }

  table().draw_all();

  while (!ui->thrower
         && game().status() == Game::Status::poverty_shift
         && shift_cards == false)
    ::ui->wait();
  if (ui->thrower)
    return HandCards(hand);

  if (game().status() != Game::Status::poverty_shift)
    return HandCards(hand, cards);

  hand.remove(HandCards(hand, cards));

  return HandCards(hand, cards);
}

/** ask 'player' whether to accept the poverty
 **
 ** @param    player   the player who is asked
 ** @param    cardno   the number of shifted cards
 **/
void
Poverty::ask(Player const& player, unsigned const cardno)
{
  //cards = HandCards(cardno);
  cards = player.game().poverty().shifted_cards();
  this->player = &player;

  table().draw_all();
} // void Poverty::ask(Player player, unsigned cardno)

/** returns whether 'player' accepts the shifted cards
 **
 ** @param    player   the player who shifts the cards
 ** @param    cardno   the number of shifted cards
 **
 ** @return   whether to accept the cards
 **/
bool
Poverty::take_accept(Player const& player, unsigned const cardno)
{
  status = Status::asking;

  //cards = HandCards(cardno);
  cards = player.game().poverty().shifted_cards();
  this->player = &player;

  accept_cards = false;
  shift_cards = false;

  table().draw_all();

  while (!ui->thrower
         && game().status() == Game::Status::poverty_shift
         && accept_cards == false
         && shift_cards  == false)
    ::ui->wait();
  if (ui->thrower)
    return false;

  return accept_cards;
} // bool Poverty::take_accept(Player const& player, unsigned cardno)

/** changes the cards from the poverty-player
 **
 ** @param    player          the player who has accepted the cards
 ** @param    cards_shifted   the cards that are given to the player
 **
 ** @return   the cards that are returned to the poverty-player
 **/
HandCards
Poverty::cards_change(Player& player, vector<Card> const& cards_shifted)
{
  status = Status::shifting_back;

  this->player = &player;
  auto hand = player.hand().sorted();

  accept_cards = false;
  shift_cards = false;

  // add the cards to the hand, but mark all cards in the middle as played
  hand.add(cards_shifted);
  for (auto const&  c : cards_shifted)
    player.hand().playcard(c);
  cards.clear();
  for (unsigned c = 0; c < hand.cardsnumber_all(); ++c)
    if (hand.played(c))
      cards.push_back(hand.card_all(c));

  table().draw_all();

  while (!ui->thrower
         && game().status() == Game::Status::poverty_shift
         && shift_cards == false)
    ::ui->wait();
  if (ui->thrower)
    return HandCards(hand);

  // remove the cards to shift from the hand of the player
  hand.remove(HandCards(hand, cards));

  return HandCards(hand, cards);
} // HandCards Poverty::cards_change(Player& player, HandCards cards_shifted)

/** changes the cards from the poverty-player
 **
 ** @param    player           the player who has accepted the cards
 ** @param    cards_returned   the cards that are given to the player
 **
 ** @return   the cards that are returned to the poverty-player
 **/
void
Poverty::cards_get_back(Player const& player_,
                        vector<Card> const& cards_returned)
{
  Player& player = ui->game().player(player_.no());
  auto hand = player.hand().sorted();
  if (player.type() != Player::Type::human)
    return ;

  status = Status::getting_back;

  this->player = &player;

  accept_cards = false;

  // add the cards to the hand, but mark all cards in the middle as played
  hand.add(cards_returned);
  for (auto const& c : cards_returned)
    player.hand().playcard(c);
  cards.clear();
  for (unsigned c = 0; c < hand.cardsnumber_all(); ++c)
    if (hand.played(c))
      cards.push_back(hand.card_all(c));

  table().draw_all();

  while (!ui->thrower
         && game().status() == Game::Status::poverty_shift
         && accept_cards == false)
    ::ui->wait();
  if (ui->thrower)
    return ;

  for (auto const& c : cards)
    hand.unplaycard(c);

  status = Status::finished;
} // void Poverty::cards_get_back(Player& player, HandCards cards_returned)

/** -> result
 **
 ** @param    cardno   number of card
 **
 ** @return   position of the cards (relative)
 **/
vector<Gdk::Point>
Poverty::cards_pos() const
{
  if (!player)
    return {};
  int const card_width = ui->cards->width();
  int const card_height = ui->cards->height();
  int const width = geometry_cards().width;
  int const height = geometry_cards().height;
  int const max_pos_y = -max(0, height - card_height);
  int const cardsnumber = cards.size();

  auto const gap = min(card_width / 2,
                       (width - cardsnumber * card_width)
                       / max(1, cardsnumber - 1));
  switch (cardsnumber) {
  case 0:
  case 1:
    return {{-card_width / 2, 0}};
  case 2:
    return {{-card_width - gap / 2, 0},
      {gap / 2, 0},
    };
  case 3:
    return {{-card_width - gap - card_width / 2, 0},
      {-card_width / 2, max(-card_height / 4, max_pos_y)},
      {card_width / 2 + gap, 0},
    };
  case 4:
    return {{-card_width - gap - card_width - gap / 2, 0},
      {-card_width - gap / 2, max(-card_height / 4, max_pos_y)},
      {gap / 2, max(-card_height / 4, max_pos_y)},
      {gap / 2 + card_width + gap, 0},
    };
  case 5:
    return {{-card_width - gap - card_width - gap - card_width / 2, 0},
      {-card_width - gap - card_width / 2, max(-card_height / 4, max_pos_y)},
      {-card_width / 2, max(-card_height / 3, max_pos_y)},
      {card_width / 2 + gap, max(-card_height / 4, max_pos_y)},
      {card_width / 2 + gap + card_width + gap, 0},
    };
  default:
    DEBUG_ASSERTION(cardsnumber <= 5,
                    "Povrty::cards_pos():\n"
                    "  the cardsnumber " << cardsnumber << " is greater then 5");
  }
  return {};
}

/** @return   whether the drawing has changed
 **/
bool
Poverty::changed() const
{
  return true;
} // bool Poverty::changed() const

/** draws the poverty elements in the table
 **
 ** @param    cr       cairo context
 **/
void
Poverty::draw(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!is_drawn())
    return ;

  draw_arrow(cr);
  draw_cards(cr);
} // void Poverty::draw(Cairo::RefPtr<::Cairo::Context> cr)

/** Draw the cards
 **
 ** @param    cr   graphic context
 **/
void
Poverty::draw_cards(Cairo::RefPtr<::Cairo::Context> cr)
{
  auto const cardno = cards.size();
  auto const positions = cards_pos();

  cr->push_group();
  geometry_cards().transform(cr);
  cr->translate(0, -ui->cards->height());

  if (cardno == 0) {
    auto card = ui->cards->back();
    draw_pixbuf(cr, card, positions[0]);
  } else { // if !(cardno == 0)
    DEBUG_ASSERTION(cardno == positions.size(),
                    "Poverty::draw()\n"
                    "  cardno = " << cardno << " != " << positions.size() << " = position.size()");
    for (unsigned i = 0; i < cardno; i++) {
      auto card = ui->cards->card(Card());
      if (   (   (status != Status::asking)
              && !(   (status == Status::shifting)
                   && (player->type() != Player::Type::human)) )
          || ::preferences(::Preferences::Type::show_all_hands)
          || (   game().rule(Rule::Type::poverty_fox_shift_open)
#ifdef WORKAROUND
              // Bug:
              // 'isfox' needs a corresponding hand,
              // but 'cards' does not belong anymore to the hand of
              // the poverty player.
              // The only problem exists, if the card is a swine, but
              // then the swines are already announced
              && cards[i] == ui->game().cards().fox()
              && !game().swines().swines_announced()
#else
              && HandCard(player->hand(), cards[i]).isfox()
#endif
             ) )
        card = ui->cards->card(cards[i]);
      draw_pixbuf(cr, card, positions[i]);
    } // for (i < cardno)
  } // if !(cardno == 0)
  cr->pop_group_to_source();
  cr->paint();
} // void Poverty::draw_cards(Cairo::RefPtr<::Cairo::Context> cr)

/** Draw the arrow
 **
 ** @param    cr   graphic context
 **/
void
Poverty::draw_arrow(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (player->type() != Player::Type::human)
    return ;
  if (!shifting_valid())
    return ;

  cr->save();
  auto const geometry = geometry_arrow();
  geometry.transform(cr);

  bool const mirror = (status == Status::shifting_back);

  cr->scale(geometry.width, geometry.height / 2); // NOLINT(bugprone-integer-division)
  if (mirror) {
    cr->scale(-1, 1);
  }
  cr->translate(-0.5, 0);
  cr->move_to(0, -1);
  cr->scale(1, 1 / 1.25);
  cr->rel_line_to(1.0 / 6, -1.25); // Obere Ecke
  cr->rel_line_to(0, 0.75);
  cr->rel_line_to(5.0 / 6, 0);
  cr->rel_line_to(0, 1);
  cr->rel_line_to(-5.0 / 6, 0);
  cr->rel_line_to(0, 0.75);
  cr->close_path();
  cr->set_source_rgb(0, 0, 0);
  cr->fill();
  cr->restore();
} // void Poverty::draw_arrow(Cairo::RefPtr<::Cairo::Context> cr)

/** @return   the outline of the trick
 **/
Poverty::Outline
Poverty::outline() const
{
  if (!is_drawn())
    return {0, 0, 1, 1};

  return (geometry_arrow().outline()
          + geometry_cards().outline());
} // Outline Poverty::outline() const

auto Poverty::geometry_arrow() const -> GeometryArrow
{
  Geometry geometry = table().layout().poverty_arrow.find(position())->second;
  return geometry;
}

auto Poverty::GeometryArrow::outline() const -> Outline
{
  return transform({-width / 2, -height,
                   width, height});
}

auto Poverty::geometry_cards() const -> GeometryCards
{
  Geometry geometry = table().layout().poverty_cards.find(position())->second;
  return geometry;
}

auto Poverty::GeometryCards::outline() const -> Outline
{
  return transform({-width / 2, -height,
                   width, height});
}

/** if the arrow is clicked, shift the cards
 ** if a card in the hand is clicked, move it in the cards-to-be-shifted
 ** if a card in the middle is clicked, move it in the hand
 ** no release: if the right button is clicked on an empty card, add cards
 **             according to the bug report
 **
 ** @param    button_event   data of the button event
 **
 ** @return   whether the click has been accepted
 **/
bool
Poverty::button_press_event(GdkEventButton* const button_event)
{
  // whether the mouse click leads to an action
  bool accepted = false;

  switch (possible_action(static_cast<int>(button_event->x),
                          static_cast<int>(button_event->y))) {
  case Action::none:
    accepted = false;
    break;
  case Action::shift_cards:
    switch (button_event->button) {
    case 1: // left mouse button
      shift_cards = true;
      accepted = true;
      break;
    default:
      break;
    } // switch (button_event->button)
    break;
  case Action::accept_cards:
    switch (button_event->button) {
    case 1: // left mouse button
      accept_cards = true;
      accepted = true;
      break;
    default:
      break;
    } // switch (button_event->button)
    break;
  case Action::take_card:
    {
      auto& player = const_cast<Player&>(*this->player);
      auto hand = player.hand().sorted();

      // test whether a card in the middle is clicked
      auto const cardno
        = cardno_at_position(static_cast<int>(button_event->x), static_cast<int>(button_event->y));
      DEBUG_ASSERTION(cardno,
                      "Poverty::button_press_event():\n"
                      "  no card under the mouse");

      // move a card in the hand
      switch (button_event->button) {
      case 1: // left mouse button
        if (cards[*cardno].is_empty())
          break;
        hand.unplaycard(cards[*cardno]);
        cards[*cardno] = Card();
        force_redraw();
        table().draw_all();
        accepted = true;
        break;
      case 3: // right mouse button

        // put all cards in the hand
        for (auto& c : cards) {
          if (!c.is_empty()) {
            if (   status != Status::shifting
                || !c.istrump(game())) {
              hand.unplaycard(c);
              c = Card();
            } // if (card not trump when soloplayer)
          }
        }
        force_redraw();
        table().draw_all();
        accepted = true;
      default:
        break;
      } // switch (button_event->button)
    }
    break;

  case Action::put_card:
    switch (button_event->button) {
    case 1: // left mouse button
      {
        auto const cardno
          = hand().cardno_at_position(static_cast<int>(button_event->x), static_cast<int>(button_event->y));
        DEBUG_ASSERTION(cardno,
                        "Poverty::button_press_event():\n"
                        "  no card under the mouse");
        add_card_to_shift(*cardno);
        force_redraw();
        table().draw_all();
        accepted = true;
      }
      break;
    default:
      break;
    } // switch (button_event->button)
    break;

  case Action::fill_up:
    // for a bug report: fill up according to the bug report
    switch (button_event->button) {
    case 3: { // right mouse button
      auto& player = const_cast<Player&>(*this->player);
      auto hand = player.hand().sorted();

      // test whether a card in the middle is clicked
      auto const cardno
        = cardno_at_position(static_cast<int>(button_event->x), static_cast<int>(button_event->y));
      if (!cardno)
        break;

      if (!(   ::bug_report_replay
            && cards[*cardno].is_empty()) )
        break;

      accepted = true;
      // fill up according to the bug report
      for (auto& c : cards) {
        if (!c.is_empty()) {
          hand.unplaycard(c);
          c = Card();
        }
      }

      switch (status) {
      case Status::shifting:
        DEBUG_ASSERTION(!::bug_report_replay->poverty_cards_shifted().empty(),
                        "UI_GTKMM:Poverty::on_mouse_click_event()\n"
                        "  the bug report does not contain shifted cards");
        cards.clear();
        for (auto const& c: ::bug_report_replay->poverty_cards_shifted())
          cards.push_back(c);
        break;

      case Status::shifting_back:
        DEBUG_ASSERTION(!::bug_report_replay->poverty_cards_returned().empty(),
                        "UI_GTKMM:Poverty::on_mouse_click_event()\n"
                        "  the bug report does not contain returned cards");
        cards.clear();
        for (auto const& c : ::bug_report_replay->poverty_cards_returned())
          cards.push_back(c);
        break;
      default:
        break;
      } // switch(status)

      for (auto const& c : cards)
        hand.playcard(c);

      force_redraw();
      table().draw_all();
      break;
    }

    default:
      break;
    } // switch (button_event->button)
    break;

  case Action::return_cards:
    switch (button_event->button) {
    case 1: // left mouse button
      shift_cards = true;
      accepted = true;
      break;
    default:
      break;
    } // switch (button_event->button)
    break;
  case Action::get_cards_back:
    switch (button_event->button) {
    case 1: // left mouse button
      accept_cards = true;
      accepted = true;
      break;
    default:
      break;
    } // switch (button_event->button)
    break;
  }; // switch (possible_actions(button_event->x, button_event->y)) {

  return accepted;
} // bool Poverty::button_press_event(GdkEventButton* button_event)

/** -> result
 **
 ** @param    x   x position
 ** @param    y   y position
 **
 ** @return   what action a can be made at the position (for the mouse)
 **/
Poverty::Action
Poverty::possible_action(int const x, int const y) const
{
  if (   (status == Status::invalid)
      || (status == Status::finished))
    return Action::none;

  if (!player)
    return Action::none;

  if (   (player->type() != Player::Type::human)
      && (status == Status::shifting))
    return Action::shift_cards;

  if (player->type() != Player::Type::human)
    return Action::none;

  if (shifting_valid()) {
    // test whether mouse is over the arrow
    auto const outline = geometry_arrow().outline();
    if (   x >= outline.x
        && x <= outline.x + outline.width
        && y >= outline.y
        && y <= outline.y + outline.height
       ) {
      switch(status) {
      case Status::invalid:
        return Action::none;
      case Status::shifting:
        return Action::shift_cards;
      case Status::shifting_back:
        return Action::return_cards;
      case Status::asking:
        return Action::shift_cards;
      case Status::getting_back:
      case Status::accepted:
      case Status::denied_by_all:
      case Status::finished:
        return Action::none;
      } // switch(status)
    } // if (arrow clicked)
  } // test whether the mouse is over the arrow

  { // test whether the mouse is over a card in the middle
    auto const cardno = cardno_at_position(x, y);

    if (cardno) {
      auto const& card = (cards.empty()
                          ? HandCard::empty
                          : cards[*cardno]);
      // special case: fill up the cards in the middle
      if (   ::bug_report_replay
          && (   (status == Status::shifting)
              || (status == Status::shifting_back) )
          && !cards.empty()
          && card.is_empty() ) {
        return Action::fill_up;
      }

      switch(status) {
      case Status::invalid:
        DEBUG_ASSERTION(false,
                        "Poverty::possible_action():\n"
                        "  'status' == invalid");
        break;
      case Status::shifting:
        // move a non-trump card in the hand
        if (card.is_empty())
          return Action::none;

        if (card.istrump(game()))
          return Action::none;

        return Action::take_card;

      case Status::asking:
        return Action::accept_cards;

      case Status::shifting_back:
        // move a card in the hand
        if (card.is_empty())
          return Action::none;

        return Action::take_card;

      case Status::getting_back:
        return Action::get_cards_back;

      case Status::accepted:
      case Status::denied_by_all:
      case Status::finished:
        break;
      } // switch(status)
    } // if (cardno)
  } // test whether the mouse is over a card in the middle

  { // test whether the player can move a card from the hand into the middle
    if (   status == Status::shifting
        || status == Status::shifting_back) {
      if (!middle_full()) {
        auto const cardno
          = hand().cardno_at_position(x, y);
        if (cardno)
          return Action::put_card;
      } // if (middle has a free place)
    }

    // look, whether there is space
  } // test whether the player can move a card from the hand

  return Action::none;
} // Poverty::Action Poverty::possible_action(int x, int y) const

/** moves the card at 'pos' in the middle
 **
 ** @param    cardno   number of cards to shift
 **/
void
Poverty::add_card_to_shift(unsigned const cardno)
{
  auto& player = const_cast<Player&>(*this->player);
  auto hand = player.hand().sorted();

  // search the first free card in the middle
  auto c = find(cards, Card());

  if (c == cards.end())
    // no free card found - ignore the taking of the card
    return ;

  DEBUG_ASSERTION((hand.played(cardno) == false),
                  "Poverty::add_card_to_shift(cardno):\n"
                  "  card number '" << cardno << "' is already played:\n"
                  "Hand:\n"
                  << hand);

  // move the card in the middle
  *c = hand.card_all(cardno);
  hand.playcard(hand.pos_all_to_pos(cardno));

#ifdef WORKAROUND
  { // just testing
    unsigned selected_cards = 0;
    for (auto const& c : cards)
      if (!c.is_empty())
        selected_cards += 1;

    DEBUG_ASSERTION((hand.cardsnumber() + selected_cards
                     == hand.cardsnumber_all()),
                    "Poverty::add_card_to_shift(cardno = " << cardno
                    << ")\n"
                    "  'hand.cardsnumber() + cards.size()' "
                    " = " << hand.cardsnumber() + cards.size()
                    << " != " << hand.cardsnumber_all()
                    << " = hand.cardsnumber_all()\n"
                    << "  shifted cards: " << cards << '\n'
                    << " Hand: " << hand);
  }
#endif // #ifdef WORKAROUND
} // void Poverty::add_card_to_shift(unsigned cardno)

/** -> result
 **
 ** @param    x   x position
 ** @param    y   y position
 **
 ** @return   the number of the card at the position
 **/
optional<unsigned>
Poverty::cardno_at_position(int x, int y) const
{
  geometry_cards().retransform(x, y);
  y += ui->cards->height();
  auto const positions = cards_pos();
  for (int i = max(0, static_cast<int>(cards.size()) - 1); i >= 0; --i) {
    auto const& card = (cards.empty()
                        ? ui->cards->back()
                        : ui->cards->card(cards[i]));
    auto const& pos = positions[i];
    if (   (x >= pos.get_x())
        && (x < (pos.get_x() + card.get_width()))
        && (y >= pos.get_y())
        && (y < (pos.get_y() + card.get_height()))

        // ToDo: check for transparency
#ifdef POSTPONED
        && !(ui->cards->card(cards_drawn[i])
             .is_transparent(unsigned(x - pos.get_x()),
                             unsigned(y - pos.get_y())))
#endif
       ) {
      return i;
    }
  } // for (i)

  return {};
} // optional<unsigned> Poverty::cardno_at_position(int x, int y) const

/** @return   whether the cards are drawn
 **/
bool
Poverty::is_drawn() const
{
  if (!ui->party().in_game())
    return false;
  if (   game().status() != Game::Status::poverty_shift
      && game().status() != Game::Status::redistribute)
    return false;

  if (!player)
    return false;

  if (   (status == Status::invalid)
      || (status == Status::accepted)
      || (status == Status::finished))
    return false;

  return true;
} // bool Poverty::is_drawn() const

/** write the status name into the stream
 **
 ** @param    ostr     output stream
 ** @param    status   status to write
 **
 ** @return   output stream
 **/
ostream&
operator<<(ostream& ostr, Poverty::Status const status)
{
  switch (status) {
  case Poverty::Status::invalid:
    return (ostr << "invalid");
  case Poverty::Status::shifting:
    return (ostr << "shifting");
  case Poverty::Status::asking:
    return (ostr << "asking");
  case Poverty::Status::shifting_back:
    return (ostr << "shifting back");
  case Poverty::Status::getting_back:
    return (ostr << "getting back");
  case Poverty::Status::accepted:
    return (ostr << "accepted");
  case Poverty::Status::denied_by_all:
    return (ostr << "denied by all");
  case Poverty::Status::finished:
    return (ostr << "finished");
  } // switch (status)
  return ostr;
} // ostream& operator<<(ostream& ostr, Poverty::Status status);

/** write the action name into the stream
 **
 ** @param    ostr     output stream
 ** @param    action   action to write
 **
 ** @return   output stream
 **/
ostream&
operator<<(ostream& ostr, Poverty::Action const action)
{
  switch (action) {
  case Poverty::Action::none:
    return (ostr << "none");
  case Poverty::Action::shift_cards:
    return (ostr << "shift cards");
  case Poverty::Action::accept_cards:
    return (ostr << "accept cards");
  case Poverty::Action::take_card:
    return (ostr << "take cards");
  case Poverty::Action::put_card:
    return (ostr << "put card");
  case Poverty::Action::fill_up:
    return (ostr << "fill up");
  case Poverty::Action::return_cards:
    return (ostr << "return cards");
  case Poverty::Action::get_cards_back:
    return (ostr << "get cards back");
  } // switch (action)
  return ostr;
} // ostream& operator<<(ostream& ostr, Poverty::Action action)

} // namespace UI_GTKMM_NS

namespace Gdk {
std::ostream&
  operator<<(std::ostream& ostr, Rectangle const& rectangle)
  {
    ostr << rectangle.get_width() << 'x' << rectangle.get_height()
      << '+' << rectangle.get_x() << '+' << rectangle.get_y();
    return ostr;
  }
std::ostream&
  operator<<(std::ostream& ostr, Point const& point)
  {
    ostr << '+' << point.get_x() << '+' << point.get_y();
    return ostr;
  }
} // namespace Gdk

#endif // #ifdef USE_UI_GTKMM
