/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "trickpile.h"
#include "hand.h"
#include "icongroup.h"
#include "name.h"

#include "../table.h"
#include "../ui.h"
#include "../cards.h"

#include "../../../player/player.h"
#include "../../../party/party.h"
#include "../../../game/game.h"
#include "../../../party/rule.h"
#include "../../../card/trick.h"
#include "../../../misc/preferences.h"

#include "../../../utils/string.h"

namespace UI_GTKMM_NS {

/** Constructor
 **
 ** @param    table   the table
 ** @param    position   the position of the trickpile
 **/
TrickPile::TrickPile(Table& table, Position const position) :
  HTIN(table, position)
{ }

/** destructor
 **/
TrickPile::~TrickPile() = default;

/**
 **/
bool
TrickPile::changed() const
{
  if (!surface_)
    return true;
  if (ui->party().in_game())
    return (last_status_ != current_status());
  return true;
} // bool TrickPile::changed() const

auto TrickPile::geometry() const -> Geometry
{
  auto const cards_width  = ui->cards->width();
  auto const cards_height = ui->cards->height();
  Geometry geometry = table().layout().trickpile.find(position())->second;
#ifdef TEST_LAYOUT
  geometry.number_of_tricks = 12;
#else
  geometry.number_of_tricks = player().number_of_tricks();
#endif
  geometry.tricks_step_x = cards_height / 15;
  geometry.tricks_step_y = -cards_height / 30;
  geometry.cards_step_x = 0;
  geometry.cards_step_y = -cards_height / 20;
  if (   table().layout().type == Table::Layout::Type::widescreen
      && (   position() == Position::west
          || position() == Position::east)) {
    geometry.tricks_step_x *= -1;
  }
  geometry.width = (max(cards_height, cards_width)
                    + ((geometry.number_of_tricks  - 1) * std::abs(geometry.tricks_step_x))
                    + ((player().game().playerno() - 1) * std::abs(geometry.cards_step_x))
                   );
  geometry.height = (max(cards_height, cards_width)
                     + ((geometry.number_of_tricks  - 1) * std::abs(geometry.tricks_step_y))
                     + ((player().game().playerno() - 1) * std::abs(geometry.cards_step_y))
                    );
  return geometry;
}

auto TrickPile::outline() const -> Outline
{
  auto const geometry = this->geometry();
  return geometry.transform({0 + min(0, geometry.tricks_step_x
                                     * (static_cast<int>(geometry.number_of_tricks) - 1)),
                            -geometry.height,
                            geometry.width, geometry.height});
}

/** draws the trickpile
 **
 ** @param    cr       cairo context
 **/
void
TrickPile::draw(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!ui->party().in_game())
    return ;

  if (!(   game().status() >= Game::Status::init
        && game().status() <= Game::Status::finished))
    return ;

  last_status_ = current_status();

  draw_tricks(cr);
  draw_points(cr);
} // void TrickPile::draw(Cairo::RefPtr<::Cairo::Context> cr)

/** draws the tricks in the trickpile
 **
 ** @param    cr    cairo context
 **
 ** @todo   skip the cards, the human player(s) do know not to make a special points (because of further team informations)
 **/
void
TrickPile::draw_tricks(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!ui->party().in_game())
    return ;

  if (   game().status() == Game::Status::finished
      && !table().in_game_review())
    return ;

  cr->save();
  cr->push_group();
  auto const geometry = this->geometry();
  geometry.transform(cr);

  unsigned trickno = geometry.number_of_tricks;

  if (table().in_game_review()) {
    trickno = 0;
    for (auto const& t : player().trickpile()) {
      if (t.no() + 1 < table().game_review_trickno())
        trickno += 1;
      else
        break;
    } // for (auto t : player.trickpile())
  } // if (table().in_game_review())

  for (unsigned t = 0; t < trickno; ++t) {
#ifdef TEST_LAYOUT
    vector<HandCard> cards;
#else
    auto cards = player().trickpile().trick(t).specialpoints_cards();
#endif
    auto const specialpoints_no = cards.size();
    // ToDo: skip the cards, the human player(s) do know not to make a special points (because of further team informations)

    while (cards.size() < game().playerno())
      cards.emplace_back();
    for (unsigned c = 0; c < cards.size(); ++c) {
      auto const& card = cards[c];
      int x = (  geometry.tricks_step_x * t
               + geometry.cards_step_x  * c);
      int y = (  geometry.tricks_step_y * t
               + geometry.cards_step_y  * c);
      if (card.is_empty()) {
        x -= (specialpoints_no * geometry.cards_step_x);
        y -= (specialpoints_no * geometry.cards_step_y);
      } // if !(card)
      if (card.is_empty()) {
        draw_pixbuf(cr, ui->cards->card(card),
                    x, y - ui->cards->width(), Rotation::left);
      } else {
        draw_pixbuf(cr, ui->cards->card(card),
                    x, y - ui->cards->height());
      }
    } // for (c < cards.size())
  } // for (t < player().number_of_tricks())
  cr->pop_group_to_source();
  cr->paint();
  cr->restore();
} // void TrickPile::draw_tricks(Cairo::RefPtr<::Cairo::Context> cr))

/** draws the points in the trickpile
 **
 ** @param    cr       cairo context
 **/
void
TrickPile::draw_points(Cairo::RefPtr<::Cairo::Context> cr)
{
  if (!ui->party().in_game())
    return ;
  if (!(   ::preferences(::Preferences::Type::show_trickpiles_points)
        || game().status() == Game::Status::finished))
    return ;

  auto const geometry = this->geometry();

  auto number_of_tricks = player().number_of_tricks();
  if (number_of_tricks == 0)
    return ;

  auto points_layout = Pango::Layout::create(cr);

  if (game().status() == Game::Status::finished) {
    auto points = player().points_in_trickpile();
    if (table().in_game_review()) {
      number_of_tricks = 0;
      points = 0;

      for (auto const& t : player().trickpile()) {
        if (t.no() + 1 < table().game_review_trickno()) {
          points += t.points();
          number_of_tricks += 1;
        } else {
          break;
        }
      } // for (auto t : player().trickpile())
      if (number_of_tricks == 0)
        return ;
    } // if (table().in_game_review())

    points_layout->set_text(String::to_string(points));
  } else { // if !(game().status() == Game::Status::finished)
    points_layout->set_text(String::to_string(player().points_in_trickpile()));
  } // if !(game().status() == Game::Status::finished)
  points_layout->set_font_description(Pango::FontDescription(::preferences(::Preferences::Type::trickpile_points_font)));

  auto number_of_cards
    = max(1,
          static_cast<int>(game().playerno())
          - static_cast<int>(player().trickpile().last_trick().specialpoints_cards().size()));

  int layout_width = 0, layout_height = 0;
  points_layout->get_pixel_size(layout_width, layout_height);
  int x = (  geometry.tricks_step_x * (number_of_tricks - 1)
           + geometry.cards_step_x  * (number_of_cards - 1)
           + ui->cards->height() / 2
          );
  int y = (  geometry.tricks_step_y * (number_of_tricks - 1)
           + geometry.cards_step_y  * (number_of_cards - 1)
           - ui->cards->width() / 2
          );

  Gdk::RGBA color;
  if (   game().status() == Game::Status::finished
      && !table().in_game_review()
     ) {
    if (   (player().team()
            == game().winnerteam())
        || (   (game().winnerteam() == Team::noteam)
            && (player().team() == Team::re)) )
      color = Gdk::RGBA(::preferences(::Preferences::Type::name_font_color));
    else
      color = Gdk::RGBA(::preferences(::Preferences::Type::name_active_font_color));
#if 0
    Gdk::Rectangle r(x - layout_width / 2,
                     y - layout_height / 2,
                     2 * layout_width, 2 * layout_height);
    gc->set_clip_rectangle(r);
#endif
  } else {
    color = Gdk::RGBA(::preferences(::Preferences::Type::trickpile_points_font_color));
  } // if ()

  cr->save();
  cr->push_group();
  geometry.transform(cr);
  cr->move_to(x, y);
  cr->set_identity_matrix();
  cr->rel_move_to(-layout_width / 2, -layout_height / 2); // NOLINT(bugprone-integer-division)
  cr->set_source_rgb(color.get_red(), color.get_green(), color.get_blue());
  points_layout->show_in_cairo_context(cr);
  cr->pop_group_to_source();
  cr->paint();
  cr->restore();
} // void TrickPile::draw_points(Cairo::RefPtr<::Cairo::Context> cr)

/** -> result
 **
 ** @param    x   x position
 ** @param    y   y position
 **
 ** @return   the pointer to the trick under the mouse curser
 **           nullptr if the pointer is over no trick
 **           or the trick cannot be shown (-> rules)
 **
 ** @todo     transparency
 **/
::Trick const*
TrickPile::over_trick(int x, int y) const
{
  if (!ui->party().in_game())
    return {};
  if (   game().status() != Game::Status::play
      && game().status() != Game::Status::full_trick
      && game().status() != Game::Status::trick_taken)
    return {};

  auto const& trickpile = player().trickpile();

  int const c = game().playerno() - 1;
  auto geometry = this->geometry();
  geometry.retransform(x, y);
  if (   x < 0
      || y > 0
      || x >= geometry.width
      || y <= -geometry.height)
    return {};

  for (int t = trickpile.size() - 1; t >= 0; --t) {
    int const pos_x = (  t * geometry.tricks_step_x
                       + c * geometry.cards_step_x);

    int const pos_y = (  t * geometry.tricks_step_y
                       + c * geometry.cards_step_y);

    // ToDo: check special points (open cards)
    if (   x >= pos_x
        && x < (pos_x + ui->cards->height())
        && y <= pos_y
        && y > (pos_y - ui->cards->width())
       ) {
      return &trickpile.trick(t);
    }
  } // for (t)

  return {};
} // ::Trick const* TrickPile::over_trick(int x, int y)

/** @return   the current status
 **/
TrickPile::Status
TrickPile::current_status() const
{
  return {player().number_of_tricks(),
    game().teaminfo().get_all()};
} // TrickPile::Status TrickPile::current_status() const

/** comparison operator
 **/
bool
operator!=(TrickPile::Status const& lhs, TrickPile::Status const& rhs)
{
  if (lhs.size != rhs.size)
    return true;
  if (lhs.teaminfo.size() != rhs.teaminfo.size())
    return true;
  for (size_t i = 0; i < lhs.teaminfo.size(); ++i)
    if (lhs.teaminfo[i] != rhs.teaminfo[i])
      return true;
  return false;
} // bool operator!=(TrickPile::Status lhs, TrickPile::Status rhs)

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
