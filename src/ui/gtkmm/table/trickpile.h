/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "htin.h"
#include "../../../basetypes/team.h"
class Trick;

namespace UI_GTKMM_NS {

/**
 ** the graphic representation of a trickpile with the points
 **
 ** @todo   use of 'Gdk::Point' and 'Gdk::Rectangle'
 **/
class TrickPile : public HTIN {
  public:
    /**
     ** The status for checking changes (for redrawing)
     **/
    struct Status {
      unsigned size = 0;
      vector<Team> teaminfo;
    };

    struct Geometry : public TableLayoutGeometry {
      unsigned number_of_tricks = 1;
      int tricks_step_x = 0;
      int tricks_step_y = 0;
      int cards_step_x = 0;
      int cards_step_y = 0;

      Geometry(TableLayoutGeometry const& geometry) : TableLayoutGeometry(geometry) { }
    };

  public:
    TrickPile(Table& table, Position position);
    ~TrickPile() override;

    bool changed() const override;
    auto geometry() const -> Geometry;
    auto outline()  const -> Outline  override;

    void draw(Cairo::RefPtr<::Cairo::Context> cr) override;
    void draw_tricks(Cairo::RefPtr<::Cairo::Context> cr);
    void draw_points(Cairo::RefPtr<::Cairo::Context> cr);

    ::Trick const* over_trick(int x, int y) const;

  private:
    Status current_status() const;
    Status last_status_;

  private: // unused
    TrickPile() = delete;
    TrickPile(TrickPile const&) = delete;
    TrickPile& operator=(TrickPile const&) = delete;
}; // class TrickPile : public HTIN

bool operator!=(TrickPile::Status const& lhs, TrickPile::Status const& rhs);

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
