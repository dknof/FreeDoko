/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "thrower.h"

#include "../../party/party.h"
#include "../../game/game.h"
#include "../../card/card.h"
#include "../../basetypes/program_flow_exception.h"

#ifdef DKNOF
//#define THROWER_INFO
#endif

namespace UI_GTKMM_NS {

Thrower::Thrower() = default;


auto Thrower::exception() const -> Exception
{
  return exception_;
}


auto Thrower::exception_type() const -> Type
{
  return exception_type_;
}


Thrower::operator bool() const
{
  return will_throw();
}


auto Thrower::depth() const -> unsigned
{
  return depth_;
}


auto Thrower::will_throw() const -> bool
{
  return (exception_type() != Type::none);
}


void Thrower::throw_it()
{
  if (exception_type() == Type::none)
    return ;

#ifdef THROWER_INFO
  cout << "thrower: throw it" << endl;
#endif

  // Since this function is left by throwing an exception,
  // I have to delete the type before the 'switch' (or in each 'case')
  // and I cannot use 'clear()' because then I do not have the
  // information afterwards.
  auto const type = exception_type();

  exception_type_ = Type::none;

  switch (type) {
  case Type::none:
    break;
  case Type::party_status:
    if (exception_.party_status == Party::Status::quit)
      throw ProgramFlowException::Quit();
    throw (exception_.party_status);
    break;
  case Type::game_status:
    throw (exception_.game_status);
    break;
  case Type::card:
    {
      auto const card = *exception_.card;
      delete exception_.card;
      exception_.card = nullptr;

      throw (card);
    }
    break;
  }; // switch(type)
}


void Thrower::clear()
{
#ifdef THROWER_INFO
  cout << "thrower: clear" << endl;
#endif
  switch (exception_type()) {
  case Type::none:
  case Type::party_status:
  case Type::game_status:
    break;
  case Type::card:
    delete exception_.card;
    break;
  } // switch (exception_type())

  exception_type_ = Type::none;
}


void Thrower::inc_depth()
{
  depth_ += 1;
}


void Thrower::dec_depth()
{
  DEBUG_ASSERTION((depth() > 0),
                  "Thrower::dec_depth()\n"
                  "  depth is '0'");
  DEBUG_ASSERTION((depth() < 10000),
                  "Thrower::dec_depth()\n"
                  "  depth is too big: " << depth());

  depth_ -= 1;

  if (depth() == 0) {
    throw_it();
  }
} // void Thrower::dec_depth()


void Thrower::set_depth(unsigned const depth)
{
  depth_ = depth;
}


void Thrower::operator()(Party::Status const party_status,
                         string const& file,
                         unsigned const line)
{
#ifdef THROWER_INFO
  cout << "thrower: throw party status '" << party_status << "'" << endl;
#endif

  exception_type_ = Type::party_status;
  exception_.party_status = party_status;

  this->file = file;
  this->line = line;
}


void Thrower::operator()(Game::Status const game_status,
                         string const& file,
                         unsigned const line)
{
#ifdef THROWER_INFO
  cout << "thrower: throw game status '" << game_status << "'" << endl;
#endif

  exception_type_ = Type::game_status;
  exception_.game_status = game_status;

  this->file = file;
  this->line = line;
}


void Thrower::operator()(Card const card,
                         string const& file,
                         unsigned const line)
{
  // double throw (by clicking to fast)
  if (exception_type_ == Type::card)
    return ;

#ifdef THROWER_INFO
  cout << "thrower: throw card '" << card << "'" << endl;
#endif
  DEBUG_ASSERTION(!will_throw(),
                  "Thrower::operator()(Card):\n"
                  "  will already throw '"
                  << static_cast<int>(exception_type()) << "'\n"
                  "  first throw: " << file << "::" << line
                  << '\n'
                  << "  new throw: " << file << "::" << line << '\n');

  exception_type_ = Type::card;
  exception_.card = new Card(card);

  this->file = file;
  this->line = line;
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
