/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "trick_drawing.h"
#include "ui.h"
#include "table.h"
#include "cards.h"

#include "../../game/game.h"
#include "../../card/trick.h"
#include "../../misc/preferences.h"

#include <gdkmm/general.h>

namespace UI_GTKMM_NS {

TrickDrawing::TrickDrawing(Base& base) :
  Base{&base}
{ }


TrickDrawing::~TrickDrawing() = default;


bool TrickDrawing::trick_set() const
{
  return (trickno != UINT_MAX);
}


auto TrickDrawing::trick() const -> ::Trick const&
{
  DEBUG_ASSERTION(trick_set(),
                  "TrickDrawing::trick():\n"
                  "  trick not set");
  return ui->game().tricks().trick(trickno);
}


void TrickDrawing::set_center(Gdk::Point const& center)
{
  this->center.set_x(center.get_x());
  this->center.set_y(center.get_y());
  center_set = true;
}


void TrickDrawing::set_trick(::Trick const& trick)
{
  if (&trick == &::Trick::empty)
    remove_trick();
  else
    trickno = trick.no();
}


void TrickDrawing::remove_trick()
{
  trickno = UINT_MAX;
}


void TrickDrawing::draw(Cairo::RefPtr<Cairo::Context> const& cr)
{
  for (size_t c = 0; c < trick().actcardno(); c++) {
    auto pos_x = (center_set ? center.get_x() : 0);
    auto pos_y = (center_set ? center.get_y() : 0);
    auto const& player = trick().player_of_card(c);

    switch (ui->table->position(player)) {
    case Position::north:
      pos_x -= ui->cards->width() / 2;
      pos_y -= ui->cards->height();

      pos_x -= overlap_x();
      pos_y += overlap_y();
      break;
    case Position::south:
      pos_x -= ui->cards->width() / 2;

      pos_x += overlap_x();
      pos_y -= overlap_y();
      break;
    case Position::west:
      if (::preferences(::Preferences::Type::rotate_trick_cards)) {
        pos_x -= ui->cards->width(ui->table->rotation(player));
        pos_y -= (ui->cards->height(ui->table->rotation(player))
                  / 2);
      } else { // if (::preferences(::Preferences::Type::rotate_trick_cards))
        pos_x -= ui->cards->width();
        pos_y -= ui->cards->height() / 2;
      } // if (::preferences(::Preferences::Type::rotate_trick_cards))
      pos_x += overlap_x();
      pos_y += overlap_y();
      break;
    case Position::east:
      if (::preferences(::Preferences::Type::rotate_trick_cards)) {
        pos_y -= (ui->cards->height(ui->table->rotation(player))
                  / 2);
      } else { // if (::preferences(::Preferences::Type::rotate_trick_cards))
        pos_y -= ui->cards->height() / 2;
      } // if (::preferences(::Preferences::Type::rotate_trick_cards))
      pos_x -= overlap_x();
      pos_y -= overlap_y();
      break;
    case Position::center:
      DEBUG_ASSERTION(false,
                      "TrickDrawgin::draw(drawable)\n"
                      "  wrong position 'center' "
                      << "(" << ui->table->position(player) << ")");
      break;
    } // switch (ui->table->position(player))
    auto trick_pixbuf = ui->cards->card(trick().card(c)).pixbuf();
    // check whether the cards shall be drawn greyed
    // The player has taken the trick but not all other players.
    if (   ui->game().status() == Game::Status::trick_taken
        && !trick().intrickpile()) {
      auto copy = trick_pixbuf->copy();
      trick_pixbuf->saturate_and_pixelate(copy, 0, true);
      trick_pixbuf = copy;
    } // if (draw cards grayed)
    Rotation const rotation = (::preferences(::Preferences::Type::rotate_trick_cards)
                               ? ui->table->rotation(player)
                               : Rotation::up);
    draw_pixbuf(cr, trick_pixbuf, pos_x, pos_y, rotation);
  } // for (c < trick.actcardno())
} // void TrickDrawing::draw(Cairo::RefPtr<Cairo::Context> cr)


bool TrickDrawing::mouse_over_cards(int const mouse_x, int const mouse_y) const
{
  DEBUG_ASSERTION(center_set,
                  "TrickDrawing::mouse_over_cards(x, y)\n"
                  "  center_set is false");

  for (unsigned c = 0; c < trick().actcardno(); c++) {
    Player const& player = trick().player_of_card(c);
    auto const rotation = (::preferences(::Preferences::Type::rotate_trick_cards)
                           ? ui->table->rotation(player)
                           : Rotation::up);
    auto const cards_width = ui->cards->width(rotation);
    auto const cards_height = ui->cards->height(rotation);
    int pos_x = center.get_x();
    int pos_y = center.get_y();

    switch (ui->table->position(player)) {
    case Position::north:
      pos_x -= cards_width / 2;
      pos_y -= cards_height;

      pos_x -= overlap_x();
      pos_y += overlap_y();
      break;
    case Position::south:
      pos_x -= cards_width / 2;

      pos_x += overlap_x();
      pos_y -= overlap_y();
      break;
    case Position::west:
      pos_x -= cards_width;
      pos_y -= cards_height / 2;
      pos_x += overlap_x();
      pos_y += overlap_y();
      break;
    case Position::east:
      pos_y -= cards_height / 2;
      pos_x -= overlap_x();
      pos_y -= overlap_y();
      break;
    case Position::center:
      DEBUG_ASSERTION(false,
                      "TrickDrawing::mouse_over_cards()\n"
                      "  wrong position 'center' "
                      << "(" << ui->table->position(player) << ")");
      break;
    } // switch (ui->table->position(player))

    if (   (mouse_x >= pos_x)
        && (mouse_y >= pos_y)
        && (mouse_x <  pos_x + cards_width)
        && (mouse_y <  pos_y + cards_height)
       ) {
      // ToDo: check for transparency
      return true;
    } // if (mouse over card)
  } // for (c < trick.actcardno())

  return false;
}


auto TrickDrawing::start() const -> Gdk::Point
{
  return Gdk::Point{center.get_x() - (width() / 2),
    center.get_y() - (height() / 2)};
}


auto TrickDrawing::outline() const -> Gdk::Rectangle
{
  return Gdk::Rectangle{start().get_x(),
    start().get_y(),
    width(),
    height()};
}


int TrickDrawing::width() const
{
  return (2 * ((::preferences(::Preferences::Type::rotate_trick_cards)
                ? ui->cards->height()
                : ui->cards->width()
               ) - overlap_x()));
}


int TrickDrawing::height() const
{
  return (2 * (ui->cards->height() - overlap_y()));
}


int TrickDrawing::overlap_x() const
{
  return ((::preferences(::Preferences::Type::rotate_trick_cards)
           ? ui->cards->height()
           : ui->cards->width()
          ) / 10);
}


int TrickDrawing::overlap_y() const
{
  return (ui->cards->height() / 8);
}

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
