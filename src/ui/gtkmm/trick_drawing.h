/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "base.h"

#include <gdkmm/window.h>

class Trick;

namespace UI_GTKMM_NS {

/**
 ** drawing a trick on a Gdk::Drawable
 **
 ** @todo   use 'Gdk::Point' and 'Gdk::Rectangle'
 **/
class TrickDrawing : public Base {
  public:
    explicit TrickDrawing(Base& base);
    ~TrickDrawing() override;

    bool trick_set() const;
    ::Trick const& trick() const;

    void set_center(Gdk::Point const& center);
    void set_trick(::Trick const& trick);
    void remove_trick();

    void draw(Cairo::RefPtr<Cairo::Context> const& cr);
    bool mouse_over_cards(int mouse_x, int mouse_y) const;

    Gdk::Point start() const;
    Gdk::Rectangle outline() const;
    int width() const;
    int height() const;
    int overlap_x() const;
    int overlap_y() const;

  private:
    Gdk::Point center;
    bool center_set = false;
  protected:
    unsigned trickno = UINT_MAX;

  private: // unused
    TrickDrawing() = delete;
    TrickDrawing(TrickDrawing const&) = delete;
    TrickDrawing& operator=(TrickDrawing const&) = delete;
}; // class TrickDrawing : public Base 

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
