/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "ui.h"

#include "main_window.h"
#include "menu.h"
#include "table.h"
#include "table/name.h"
#include "table/hand.h"
#include "table/trick.h"
#include "table/poverty.h"
#include "reservation.h"
#include "players_db.h"

#include "../../party/party.h"
#include "../../party/rule.h"
#include "../../game/game.h"
#include "../../game/gameplay_actions.h"
#include "../../card/trick.h"
#include "../../player/player.h"
#include "../../os/bug_report_replay.h"

namespace UI_GTKMM_NS {

/** the game is opened
 **
 ** @param    game   the game that is opened
 **/
void
  UI_GTKMM::game_open(Game& game)
  {
    UI::game_open(game);
    table->game_open();
  }

/** distribute the cards manually
 **/
void
  UI_GTKMM::game_distribute_cards_manually()
  {
    thrower.inc_depth();
    table->game_distribute_cards_manually();
    thrower.dec_depth();
  }

/** the cards are distributed
 **/
void
  UI_GTKMM::game_cards_distributed()
  {
    table->game_cards_distributed();
  }

/** ask 'player' for a reservation
 **
 ** @param    player   player who is asked
 **/
void
  UI_GTKMM::reservation_ask(Player const& player)
  {
    table->reservation_ask(player);
  }

/** Gets the reservation of the given player:
 ** pops up a window
 ** This is the first time, the hands can (should) be shown.
 **
 ** @param    player   player, whose reservation schould be get
 **
 ** @return   reservation of the player
 **/
::Reservation
  UI_GTKMM::get_reservation(Player const& player)
  {
    thrower.inc_depth();
    auto const result = table->reservation_get(player);
    thrower.dec_depth();
    return result;
  }

/** 'player' has a poverty and shifts cards
 **
 ** @param    player   the player who has the poverty
 **/
void
  UI_GTKMM::poverty_shifting(Player const& player)
  {
    table->draw_all();
  }

/** 'player' shifts 'cardno' cards
 **
 ** @param    player   the player who shifts the cards
 ** @param    cardno   the number of cards that are shifted
 **/
void
  UI_GTKMM::poverty_shift(Player const& player, unsigned const cardno)
  {
    table->poverty().shift(player, cardno);
  }

/** ask 'player' whether to accept the poverty
 **
 ** @param    player   the player who is asked
 ** @param    cardno   the number of cards that are shifted
 **/
void
  UI_GTKMM::poverty_ask(Player const& player, unsigned const cardno)
  {
    table->poverty().ask(player, cardno);
  }

/** 'player' denied to take the shifted cards
 **
 ** @param    player   the player who has denied to take the cards
 **/
void
  UI_GTKMM::poverty_take_denied(Player const& player)
  {
    table->poverty().take_denied(player);
  }

/** all players have denied to take the cards
 **
 ** @todo   show a window
 **/
void
  UI_GTKMM::poverty_take_denied_by_all()
  {
    table->poverty().take_denied_by_all();
  }

/** 'player' accepts to take the shifted cards
 ** and returns 'cardno' cards with 'trumpno' trumps
 **
 ** @param    player   the player who has denied to take the cards
 ** @param    cardno   number of cards that are given back
 ** @param    trumpno   number of trumps of the cards
 **/
void
  UI_GTKMM::poverty_take_accepted(Player const& player,
                                  unsigned const cardno,
                                  unsigned const trumpno)
  {
    table->poverty().take_accepted(player, cardno, trumpno);
  }

/** returns which cards the player shifts
 **
 ** @param    player   the player who shifts the cards
 **
 ** @return   the cards that are to be shifted
 **/
HandCards
  UI_GTKMM::get_poverty_cards_to_shift(Player& player)
  {
    auto const result = table->poverty().shift(player);
    return result;
  }

/** returns whether 'player' accepts the shifted cards
 **
 ** @param    player   the player who shifts the cards
 ** @param    cardno   the number of shifted cards
 **
 ** @return   whether to accept the cards
 **/
bool
  UI_GTKMM::get_poverty_take_accept(Player const& player, unsigned cardno)
  {
    auto const result = table->poverty().take_accept(player, cardno);
    return result;
  }

/** changes the cards from the poverty-player
 **
 ** @param    player   the player who has accepted the cards
 ** @param    cards   the cards that are given to the player
 **
 ** @return   the cards that are returned to the poverty-player
 **/
HandCards
  UI_GTKMM::get_poverty_exchanged_cards(Player& player,
                                        vector<Card> const& cards)
  {
    auto const result = table->poverty().cards_change(player, cards);
    return result;
  }

/** the poverty player 'player' gets 'cards'
 **
 ** @param    player   the player gets the cards
 ** @param    cards   the cards that are given to the player
 **/
void
  UI_GTKMM::poverty_cards_get_back(Player const& player,
                                   vector<Card> const& cards)
  {
    table->poverty().cards_get_back(player, cards);
  }

/** the game is redistributed
 **/
void
  UI_GTKMM::game_redistribute()
  {
    table->game_redistribute();
  }

/** the game is started:
 ** the gametype and announcements of the other players are shown
 **/
void
  UI_GTKMM::game_start()
  {
    main_window->menu->game_start();
    table->game_start();
  }

/** a new trick is opened
 **
 ** @param    trick   trick that is opened
 **/
void
  UI_GTKMM::trick_open(::Trick const& trick)
  {
    UI::trick_open(trick);
    main_window->menu->trick_open();
    table->trick_open();
  }

/** the trick is full.
 ** show the 'full trick'-window or wait
 **/
void
  UI_GTKMM::trick_full()
  {
    main_window->menu->announcements_update();
    table->trick_full();
  }

/** move the trick in the pile of the winnerplayer
 **/
void
  UI_GTKMM::trick_move_in_pile()
  {
    table->trick_move_in_pile();
  }

/** the trick is closed
 **/
void
  UI_GTKMM::trick_close()
  {
    UI::trick_close();
  }

/** a gameplay action
 **
 ** @param    action   the action
 **/
void
  UI_GTKMM::gameplay_action(GameplayAction const& action)
  {
    thrower.inc_depth();

    table->gameplay_action(action);

    switch (action.type()) {
    case GameplayActions::Type::announcement: {
      auto const& announcement
        = dynamic_cast<GameplayActions::Announcement const&>(action).announcement;
      auto const& player
        = game().player(dynamic_cast<GameplayActions::Announcement const&>(action).player);
      main_window->menu->announcements_update();
      table->announcement_made(announcement, player);
      break;
    }

    case GameplayActions::Type::swines: {
      auto const& player
        = game().player(dynamic_cast<GameplayActions::Swines const&>(action).player);
      main_window->menu->announcements_update();
      table->swines_announced(player);
    }
      break;

    case GameplayActions::Type::hyperswines: {
      auto const& player
        = game().player(dynamic_cast<GameplayActions::Hyperswines const&>(action).player);
      main_window->menu->announcements_update();
      table->hyperswines_announced(player);
      break;
    }

    case GameplayActions::Type::marriage: {
      auto const& bridegroom
        = game().player(dynamic_cast<GameplayActions::Marriage const&>(action).player);
      auto const& bride
        = game().player(dynamic_cast<GameplayActions::Marriage const&>(action).bride);
      main_window->menu->announcements_update();
      table->marriage(bridegroom, bride);
      break;
    }

    case GameplayActions::Type::reservation:
      // nothing to do
      break;
    case GameplayActions::Type::card_played:
      // much to do, see card_played(HandCard card)
      break;

    case GameplayActions::Type::poverty_shift:
    case GameplayActions::Type::poverty_accepted:
    case GameplayActions::Type::poverty_returned:
    case GameplayActions::Type::poverty_denied:
    case GameplayActions::Type::poverty_denied_by_all:
    case GameplayActions::Type::trick_open:
    case GameplayActions::Type::trick_full:
    case GameplayActions::Type::trick_taken:
    case GameplayActions::Type::check:
    case GameplayActions::Type::print:
    case GameplayActions::Type::quit:
      // ToDo
      break;
    } // switch (action.type())

    try {
      thrower.dec_depth();
    } catch (Game::Status const status) {
      if (status == Game::Status::trick_taken) {
        game().set_trick_taken();
      } else {
        throw;
      }
    }
  }

/** let the user select a card for the given player
 **
 ** @param    player   the player who is to play a card
 **
 ** @return   the selected card
 **/
HandCard
  UI_GTKMM::get_card(Player const& player)
  {
    table->card_get(player);

    main_window->menu->card_get();
    const_cast<Player&>(player).hand().forget_request();

    try {
      if (   ::preferences(::Preferences::Type::automatic_card_suggestion)
          || (   ::bug_report_replay
              && (player.type() == Player::Type::human) )
         )
        table->show_card_suggestion(false);

      do {
        try {
          while (   !ui->thrower
                 && game().status() == Game::Status::play)
            ::ui->wait();
          if (   thrower.depth() == 0
              && thrower.will_throw())
            thrower.throw_it();
        } catch (Card const& card) {
          DEBUG_ASSERTION((card == player.hand().requested_card()),
                          "UI_GTKMM::card_get(player):\n"
                          "  did not catch the requested card:\n"
                          "  " << card << " != " << player.hand().requested_card()
                         );
        }

        if (player.hand().requested_card().is_empty()) {
          const_cast<Player&>(player).hand().forget_request();
          continue;
        }
        // the player cannot play an invalid card
        if (!player.game().tricks().current().isvalid(player.hand().requested_card(),
                                                      player.hand())) {
          const_cast<Player&>(player).hand().forget_request();
          continue;
        }

      } while (player.hand().requested_position() == UINT_MAX);
    } catch (...) {
      table->card_got();
      main_window->menu->card_got();
      throw;
    } // catch(...)

    table->card_got();
    main_window->menu->card_got();
    return player.hand().requested_card();
  }

/** the given player has played 'card.
 ** remove the card from the hand
 **
 ** @param    card   the played card
 **/
void
  UI_GTKMM::card_played(HandCard const card)
  {
    auto const& player = card.player();

    // for the sensitivity of the swines button
    main_window->menu->announcements_update();

    // for updating the heuristics
    players_db->update_db();

    table->card_played(player);

    update();
  }

/** the game is finished:
 ** the winner and the points of the game are shown.
 **/
void
  UI_GTKMM::game_finish()
  {
    players_db->update_db();
    main_window->menu->game_finish();
    table->game_finished();
  }

/** the game is closed
 **/
void
  UI_GTKMM::game_close()
  {
    UI::game_close();

    main_window->menu->game_close();
    table->game_close();
  }

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
