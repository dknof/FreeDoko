/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "../ui.h"
#include "base.h"
#include "thrower.h"

namespace Gtk {
class Main;
class Label;
class Button;
class Window;
class TextView;
class Widget;
class IconTheme;
class Image;
} // namespace Gtk
#include <glibmm/refptr.h>
#include <gdkmm/pixbuf.h>

namespace UI_GTKMM_NS {
class UI_GTKMM_Wrap;
class Cards;
class Icons;
class DebugLog;
class FirstRun;
class ProgramUpdated;
class SplashScreen;
class MainWindow;
class PartySummary;
class PartySettings;
class PlayersDB;
class GameDebug;
class Chatter;
class Help;
class License;
class CardsetLicense;
class ChangeLog;
class Support;
class About;
class Table;
class Preferences;
class BugReport;
class BugReportReplay;

/**
 ** the UI, created with gtkmm
 **
 ** @todo   replay game
 ** @todo   help
 ** @todo   messages (status line)
 ** @todo   drag'n drop (cardset, cards back, background)
 **/
class UI_GTKMM : public UI, public Base {
  friend class Base;

  public:
  // detect whether the string is an utf8 text
  static bool is_utf8(string const& text);
  // converts the text in utf8 format
  static Glib::ustring to_utf8(string const& text);

  public:
  UI_GTKMM();
  ~UI_GTKMM() override;

  UI_GTKMM(UI_GTKMM const&) = delete;
  UI_GTKMM& operator=(UI_GTKMM const&) = delete;

  // initialize the ui
  void init(int& argc, char**& argv) override;

  // updates the UI
  void update() override;
  // sleeps 'sleep_msec' thousands of seconds
  // (< 0 for infinity)
  void sleep(unsigned sleep_msec) override;

  void main_quit();
  static bool stop_sleeping();

  bool key_press(GdkEventKey const* key);

  void party_loaded() override;
  void start_new_party();
  void end_party();
  void quit_program();

  // the parts of a party
  void party_get_settings() override;

  // a gameplay action
  void gameplay_action(GameplayAction const& action) override;

  // the parts of a game
  void game_open(Game& game) override;
  void game_distribute_cards_manually() override;
  void game_cards_distributed() override;
  void game_redistribute() override;
  void reservation_ask(Player const& player) override;
  ::Reservation get_reservation(Player const& player) override;
  void game_start() override;
  void game_finish() override;
  void game_close() override;

  // the parts of a trick
  void trick_open(::Trick const& trick) override;
  void trick_full() override;
  void trick_move_in_pile() override;
  void trick_close() override;
  // get a card
  HandCard get_card(Player const& player) override;
  // the card is played
  void card_played(HandCard card) override;

  // the parts of a poverty
  void poverty_shifting(Player const& player) override;
  void poverty_shift(Player const& player, unsigned cardno) override;
  void poverty_ask(Player const& player, unsigned cardno) override;
  void poverty_take_denied(Player const& player) override;
  void poverty_take_denied_by_all() override;
  void poverty_take_accepted(Player const& player,
                             unsigned cardno,
                             unsigned trumpno) override;
  HandCards get_poverty_cards_to_shift(Player& player) override;
  bool get_poverty_take_accept(Player const& player, unsigned cardno) override;
  HandCards get_poverty_exchanged_cards(Player& player, vector<Card> const& cards) override;

  void poverty_cards_get_back(Player const& player, vector<Card> const& cards) override;

  // redrawing
  void redraw_all() override;
  void gametype_changed() override;
  void players_switched(Player const& player_a,
                        Player const& player_b) override;
  void player_changed(Player const& player) override;
  void player_added(Player const& player) override;
  void name_changed(Person const& player) override;
  void voice_changed(Person const& player) override;
  void hand_changed(Player const& player) override;
  void teaminfo_changed(Player const& player) override;
  void aiconfig_changed(Aiconfig const& aiconfig) override;

  // change of the rules
  void rule_changed(int type, void const* old_value);
  // changes of the settings
  void setting_changed(int type, void const* old_value);

  // load the help page
  void help_load(string const& page) override;


  // whether the ui is busy
  void set_busy() override;
  void set_not_busy() override;

  // the status message has changed
  void status_message_changed(string const& status_message) override;

  // the progress has changed
  void progress_changed(double progress_max) override;
  // the progress is finished
  void progress_finished() override;


  // the first run of the program
  void first_run(string const& message) override;
  // an update of the version
  void program_updated(Version const& old_version) override;
  // information for the user
  void information(string const& message,
                   InformationType type,
                   bool force_show = false) override;
  // show the help
  void show_help(string const& location);

  // an error has occured
  void error(string const& message) override;
  void error(string const& message, string const& backtrace) override;


  // generate an error
  void generate_error(string error_message);


  // internal functions

  // add a window to the window list
  void add_window(Gtk::Window& window);

  // iconifies all windows
  void iconify_all_windows();
  // deiconifies all windows
  void deiconify_all_windows();



  auto create_theme_icon_image(string const& icon_name, size_t size) -> Gtk::Image*;

  public: // the gtkmm part
  AutoDisconnector disconnector_;

  Thrower thrower;

  size_t base_objects_number = 0;

  unique_ptr<Gtk::Main> kit;
  Glib::RefPtr<Gtk::IconTheme> icon_theme;

  Glib::RefPtr<Gdk::Pixbuf> logo;
  Glib::RefPtr<Gdk::Pixbuf> icon;

  unique_ptr<Cards> cards;
  unique_ptr<Icons> icons;

  unique_ptr<FirstRun> first_run_window;
  unique_ptr<ProgramUpdated> program_updated_window;
  unique_ptr<SplashScreen> splash_screen;

  unique_ptr<MainWindow> main_window;

  unique_ptr<Table> table;

  unique_ptr<PartySummary> party_summary;
  unique_ptr<PartySettings> party_settings;

  unique_ptr<PlayersDB> players_db;

  unique_ptr<GameDebug> game_debug;

  unique_ptr<Help> help;
  unique_ptr<License> license;
  unique_ptr<ChangeLog> changelog;
  unique_ptr<Support> support;
  unique_ptr<About> about;

  unique_ptr<Preferences> preferences;

  unique_ptr<BugReport> bug_report;
  unique_ptr<BugReportReplay> bug_report_replay;

  static bool sleeping; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

  private:
  std::vector<Gtk::Window*> windows;
}; // class UI_GTKMM : public UI, public Base

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
