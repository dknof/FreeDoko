/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "ui.h"
#include "splash_screen.h"
#include "cards.h"
#include "icons.h"
#include "party_summary.h"
#include "party_settings.h"
#include "players_db.h"
#include "game_debug.h"
#include "rules.h"
#include "table.h"
#include "game_finished.h"
#include "game_summary.h"
#include "party_points.h"
#include "party_finished.h"
#include "bug_report_replay.h"
#include "license.h"
#include "main_window.h"

#include "../../party/party.h"
#include "../../misc/preferences.h"

#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/textview.h>

namespace UI_GTKMM_NS {

/** redraw all (of the playfield)
 **/
void
  UI_GTKMM::redraw_all()
  {
    if (table)
      table->force_redraw_all();
  } // void UI_GTKMM::redraw_all()

/** update the gametype icon
 **/
void
  UI_GTKMM::gametype_changed()
  {
    if (table)
      table->draw_all();
  } // void UI_GTKMM::gametype_changed()

/** the players 'player_a' and 'player_b' have been switched
 **
 ** @param    player_a   first player
 ** @param    player_b   second player
 **/
void
  UI_GTKMM::players_switched(Player const& player_a, Player const& player_b)
  {
    if (party_settings)
      party_settings->players_swapped(player_a, player_b);
  } // void UI_GTKMM::players_switched(Player player_a, Player player_b)

/** update the 'player'
 **
 ** @param    player   the player
 **/
void
  UI_GTKMM::player_changed(Player const& player)
  {
    if (party_settings)
      party_settings->player_update(player);
    name_changed(player);
    voice_changed(player);
  } // void UI_GTKMM::player_changed(Player player)

/** add the 'player'
 **
 ** @param    player   the player
 **/
void
  UI_GTKMM::player_added(Player const& player)
  {
    if (table)
      table->party_points_->player_added(player);
    name_changed(player);
    voice_changed(player);
  } // void UI_GTKMM::player_added(Player player)

/** update the name of 'player'
 **
 ** @param    player   the player
 **/
void
  UI_GTKMM::name_changed(Person const& person)
  {
    auto const& player = party().players().player(person);
#ifdef WORKAROUND
    { // convert latin to utf8
      auto const name = UI_GTKMM::to_utf8(player.name());
      if (name != player.name()) {
        const_cast<Player&>(player).person().set_name(name);
        return ;
      }
    } // name: change to utf8
#endif

    if (players_db)
      players_db->name_changed(player);
    if (party_settings)
      party_settings->name_update(player);
    if (party_summary)
      party_summary->name_update(player);
    if (table) {
      table->name_changed(player);
      table->mouse_cursor_update();
    }
    if (game_debug)
      game_debug->name_changed(player);
    if (bug_report_replay)
      bug_report_replay->name_changed(player);
  } // void UI_GTKMM::name_changed(Player player)

/** update the voice of 'player'
 **
 ** @param    player   the player
 **/
void
  UI_GTKMM::voice_changed(Person const& person)
  {
    auto const& player = party().players().player(person);
    if (party_settings)
      party_settings->voice_update(player);
  }

/** update the hand of 'player'
 **
 ** @param    player   the player
 **/
void
  UI_GTKMM::hand_changed(Player const& player)
  {
    if (table)
      table->force_redraw_all();
  } // void UI_GTKMM::hand_changed(Player player)

/** update the team icon of 'player'
 **
 ** @param    player   the player
 **/
void
  UI_GTKMM::teaminfo_changed(Player const& player)
  {
    if (table)
      table->draw_all();
  } // void UI_GTKMM::teaminfo_changed(Player player)

/** update the 'aiconfig'
 **
 ** @param    aiconfig   the aiconfig
 **/
void
  UI_GTKMM::aiconfig_changed(Aiconfig const& aiconfig)
  {
    if (party_settings)
      party_settings->aiconfig_update(aiconfig);
  } // void UI_GTKMM::aiconfig_changed(Aiconfig aiconfig)

/** update the rule
 **
 ** @param    type      the rule, that has changed
 ** @param    old_value   old value
 **/
void
  UI_GTKMM::rule_changed(int const type, void const* const old_value)
  {
    switch (type) {
    case Rule::Type::counting: {
      if (table
          && table->game_finished_
          && table->game_finished_->game_summary)
        table->game_finished_->game_summary->update();

      if (table
          && table->party_points_)
        table->party_points_->recalc_all();

      if (table
          && table->party_finished_)
        table->party_finished_->redraw_points_graph();

      if (table
          && party().status() == Party::Status::finished)
        table->draw_all();
    }
      break;

    case Rule::Type::with_nines: {
      if (party_settings)
        party_settings->update();
    }
      break;

    case Rule::Type::number_of_duty_soli:
    case Rule::Type::number_of_duty_color_soli:
    case Rule::Type::number_of_duty_picture_soli:
      if (table
          && table->party_points_)
        table->party_points_->recreate_all();

    default:
      break;
    } // switch (type)

#ifndef RELEASE
    table->force_redraw_all();
#endif
  } // void UI_GTKMM::rule_changed(int type, void const* old_value)

/** update the setting
 **
 ** @param    type      the setting, that has changed
 ** @param    old_value   old value
 **/
void
  UI_GTKMM::setting_changed(int const type, void const* const old_value)
  {
    switch(type) {
    case ::Preferences::Type::show_ai_information_hands:
    case ::Preferences::Type::show_ai_information_teams:
    case ::Preferences::Type::announce_in_table:
    case ::Preferences::Type::show_known_teams_in_game:
    case ::Preferences::Type::show_soloplayer_in_game:
    case ::Preferences::Type::static_hands:
      redraw_all();
      break;

    case ::Preferences::Type::name:
#ifdef WORKAROUND
      { // convert latin to utf8
        auto const name = UI_GTKMM::to_utf8(::preferences(::Preferences::Type::name));
        if (name != ::preferences(::Preferences::Type::name)) {
          ::preferences.set(::Preferences::Type::name, name);
          return ;
        }
      } // convert latin to utf8
#endif
      for (auto const& p : ::party->players())
        name_changed(p);
      break;

    default:
      break;
    } // switch(type)
  } // void UI_GTKMM::setting_changed(int type, void const* old_value)

/** the status message has changed
 **
 ** @param    status_message   the status message
 **/
void
  UI_GTKMM::status_message_changed(string const& status_message)
  {
    if (splash_screen)
      splash_screen->status_message_changed(status_message);
  } // void UI_GTKMM::status_message_changed(string status_message)

/** the program is busy
 **/
void
  UI_GTKMM::set_busy()
  {
    UI::set_busy();
    table->mouse_cursor_update();
  } // void UI_GTKMM::set_busy()

/** the program is not busy any more
 **/
void
  UI_GTKMM::set_not_busy()
  {
    UI::set_not_busy();
    table->mouse_cursor_update();
  } // void UI_GTKMM::set_not_busy()

/** the progress has changed
 **
 ** @param    progress   the progress
 **/
void
  UI_GTKMM::progress_changed(double const progress)
  {
    if (splash_screen)
      splash_screen->progress_changed(progress);

    if (table)
      table->progress_changed(progress);
  } // void UI_GTKMM::progress_changed(double progress)

/** the progress is finished
 **/
void
  UI_GTKMM::progress_finished()
  {
    if (table)
      table->progress_finished();
  } // void UI_GTKMM::progress_finished()

} // namespace UI_GTKMM_NS

#endif // #ifdef USE_UI_GTKMM
