/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "file_chooser_dialog_wrapper.h"

namespace Gtk {

/** constructor
 **
 ** @param    title    the title of the window
 ** @param    action   save or open
 ** @param    slot     slot for the save/load signal
 **/
FileChooserDialogWrapper::FileChooserDialogWrapper(Glib::ustring const title,
                                                   Gtk::FileChooserAction action,
                                                   sigc::slot1<bool, string> const slot) :
  title_(title),
  action_(action),
  slot_(slot)
{ }

/** set the current directory
 **
 ** @param    dir   the current directory
 **/
void
FileChooserDialogWrapper::set_current_folder(string const dir)
{
  current_folder_ = dir;
  if (file_chooser_dialog_)
    file_chooser_dialog_->set_current_folder(dir);
} // set_current_folder(string dir)

/** sets the title
 **
 ** @param    title   new title of the dialog
 **/
void
FileChooserDialogWrapper::set_title(string const title)
{
  title_ = title;
  if (file_chooser_dialog_)
    file_chooser_dialog_->set_title(title);
} // set_title(string title)

/** show the window
 **/
void
FileChooserDialogWrapper::present()
{
  if (!file_chooser_dialog_)
    init();

  file_chooser_dialog_->present();
} // void present()

/** event: the window is shown
 ** move the window to the remembered position
 **/
void
FileChooserDialogWrapper::init()
{
  file_chooser_dialog_ = make_unique<FileChooserDialog>(title_, action_);
  auto cancel_button = file_chooser_dialog_->add_button(_("Button::cancel"), Gtk::RESPONSE_CANCEL);
  cancel_button->set_image_from_icon_name("window-close");
  cancel_button->set_always_show_image();
  auto ok_button = file_chooser_dialog_->add_button((action_ == Gtk::FILE_CHOOSER_ACTION_SAVE)
                                                          ? _("Button::save")
                                                          : _("Button::load"),
                                                          Gtk::RESPONSE_OK);
  ok_button->set_image_from_icon_name((action_ == Gtk::FILE_CHOOSER_ACTION_SAVE)
                                      ? "document-save"
                                      : "document-open");
  ok_button->set_always_show_image();

  { // signals
    file_chooser_dialog_->signal_file_activated().connect(sigc::mem_fun(*this, &FileChooserDialogWrapper::signal_ok));
    file_chooser_dialog_->signal_file_activated().connect(sigc::mem_fun(*file_chooser_dialog_, &Widget::hide));

    ok_button->signal_clicked().connect(sigc::mem_fun(*this, &FileChooserDialogWrapper::signal_ok));
    ok_button->signal_clicked().connect(sigc::mem_fun(*file_chooser_dialog_, &Widget::hide));
    cancel_button->signal_clicked().connect(sigc::mem_fun(*file_chooser_dialog_, &Widget::hide));
  } // signals

  file_chooser_dialog_->set_current_folder(current_folder_);
} // void init()

/** the OK button was clicked
 **/
void
FileChooserDialogWrapper::signal_ok()
{
  slot_(file_chooser_dialog_->get_filename());
} // void signal_ok()

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
