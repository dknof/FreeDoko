/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include <gtkmm/filechooserdialog.h>

namespace Gtk {
/**
 ** a dialog which remembers its place and forwards the key presses
 ** (workaround)
 **/
class FileChooserDialogWrapper : public sigc::trackable {
  public:
    FileChooserDialogWrapper(Glib::ustring title,
                             Gtk::FileChooserAction action,
                             sigc::slot1<bool, string> slot);
    FileChooserDialogWrapper() = delete;
    FileChooserDialogWrapper(FileChooserDialogWrapper const&) = delete;
    FileChooserDialogWrapper& operator=(FileChooserDialogWrapper const&) = delete;

    void set_current_folder(string dir);
    void set_title(string title);
    void present();

  protected:
    void init();
    void signal_ok();

  private:
    unique_ptr<FileChooserDialog> file_chooser_dialog_;
    Glib::ustring title_;
    Gtk::FileChooserAction const action_;
    sigc::slot1<bool, string> slot_;
    string current_folder_ = ".";
}; // class FileChooserDialogWrapper : public sigc::trackable

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
