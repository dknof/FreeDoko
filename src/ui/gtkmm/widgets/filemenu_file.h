/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "filemenu.h"

namespace Gtk {
/**
 ** a file menu, only with directories with the file 'file'
 **/
class FileMenuFilterFile : public FileMenu {
  public:
    FileMenuFilterFile(sigc::slot1<void, string> signal_slot,
                       string file);
    FileMenuFilterFile() = delete;
    FileMenuFilterFile(FileMenuFilterFile const&) = delete;
    FileMenuFilterFile& operator=(FileMenuFilterFile const&) = delete;
    ~FileMenuFilterFile() override;

    // create a new file menu
    FileMenu* create_new(Directory directory) const override;

    // whether the filename is accepted
    bool accept(Path filename) const override;

  private:
    string file;
}; // class FileMenuFilterFile : public Menu

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
