/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "type_selector.h"

#include <gtkmm/box.h>
#include <gtkmm/label.h>

namespace Gtk {
/**
 ** a selector for types with a label
 **/
template<typename Type>
  class LabelTypeSelector : public Gtk::Box {
    public:
      LabelTypeSelector();
      explicit LabelTypeSelector(Glib::ustring const& label);
      LabelTypeSelector(LabelTypeSelector const&) = delete;
      LabelTypeSelector& operator=(LabelTypeSelector const&) = delete;
      ~LabelTypeSelector() override = default;

      // the label
      Label* get_label()
      { return this->label_; }
      // the type selector
      TypeSelector<Type>* get_selector()
      { return this->selector_; }

      using Gtk::Container::add;
      // adds the type to the selector
      void add(Type const& type)
      { this->selector_->add(type); }

      // returns the selected type
      Type type()
      { return this->selector_->type(); }
      // set the selected type
      void set_type(Type const& type)
      { this->selector_->set_type(type); }

      Glib::SignalProxy0<void> signal_value_changed()
      { return this->selector_->signal_changed(); }

    private:
      // inits the label type selector
      void init()
      {
        this->set_spacing(1 EX);

        this->label_->set_halign(Gtk::ALIGN_START);
        this->label_->set_valign(Gtk::ALIGN_CENTER);
        this->pack_start(*this->label_, true, true);
        this->pack_end(*this->selector_, false, true);
      }
    private:
      // the label with the description
      Label* label_;
      // the menu with the types
      TypeSelector<Type>* selector_;
  }; // class LabelTypeSelector : public Box


template<typename Type>
  LabelTypeSelector<Type>::LabelTypeSelector() :
    label_(Gtk::manage(new Gtk::Label())),
    selector_(Gtk::manage(new TypeSelector<Type>()))
{ this->pack_end(*this->selector_); }

template<typename Type>
LabelTypeSelector<Type>::LabelTypeSelector(Glib::ustring const& label) :
  label_(Gtk::manage(new Gtk::Label(label))),
  selector_(Gtk::manage(new TypeSelector<Type>()))
{ this->init(); }

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
