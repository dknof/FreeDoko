/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "labelscalespinbutton.h"

namespace Gtk {

/** constructor
 **/
LabelScaleSpinButton::LabelScaleSpinButton() :
  LabelScaleSpinButton("")
{  }

/** constructor
 **
 ** @param    label    name for the label
 **/
LabelScaleSpinButton::LabelScaleSpinButton(Glib::ustring const& label) :
  LabelSpinButton(label),
  scale_(Gtk::manage(new Gtk::Scale(Gtk::ORIENTATION_HORIZONTAL)))
{
  scale_->set_adjustment(spin_button_->get_adjustment());
  scale_->set_digits(0);
  scale_->set_draw_value(false);
  pack_end(*scale_, false, true);
} // LabelScaleSpinButton::LabelScaleSpinButton(Glib::ustring label)

/** @return   the scale
 **/
Scale*
LabelScaleSpinButton::get_scale()
{
  return scale_;
}

/** set the range
 **
 ** @param    min    minimum value
 ** @param    max    maximum value
 **/
void
LabelScaleSpinButton::set_range(double const min, double const max)
{
  scale_->set_range(min, max);
  scale_->set_size_request(static_cast<int>((max - min) * 10));
  LabelSpinButton::set_range(min, max);
} // void LabelScaleSpinButton::set_range(double min, double max)

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
