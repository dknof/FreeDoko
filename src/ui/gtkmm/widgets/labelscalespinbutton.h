/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include "labelspinbutton.h"
#include <gtkmm/scale.h>

namespace Gtk {
/**
 ** a spin button with a label
 **/
class LabelScaleSpinButton : public LabelSpinButton {
  public:
    LabelScaleSpinButton();
    explicit LabelScaleSpinButton(Glib::ustring const& label);
    LabelScaleSpinButton(LabelScaleSpinButton const&) = delete;
    LabelScaleSpinButton& operator=(LabelScaleSpinButton const&) = delete;

    ~LabelScaleSpinButton() override = default;

    Scale* get_scale();

    void set_range(double min, double max) override;

  private:
    Scale* scale_ = nullptr;
}; // class LabelScaleSpinButton : public LabelSpinButton

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
