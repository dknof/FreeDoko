/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "labelspinbutton.h"

namespace Gtk {

/** constructor
 **/
LabelSpinButton::LabelSpinButton() :
LabelSpinButton("")
{  }

/** constructor
 **
 ** @param    label    name for the label
 **/
LabelSpinButton::LabelSpinButton(Glib::ustring const& label) :
  label_(Gtk::manage(new Gtk::Label(label))),
  spin_button_(Gtk::manage(new Gtk::SpinButton()))
{
  spin_button_->set_numeric(true);
  set_spacing(10);
  spin_button_->set_increments(1, 10);

  label_->set_halign(Gtk::ALIGN_START);
  pack_start(*label_,       Gtk::PACK_EXPAND_PADDING);
  pack_start(*spin_button_, Gtk::PACK_SHRINK);
}

/** constructor
 **
 ** @param    label          name for the label
 ** @param    label_right    name for the label
 **/
LabelSpinButton::LabelSpinButton(Glib::ustring const& label, Glib::ustring const& label_right) :
  label_(Gtk::manage(new Gtk::Label(label))),
  spin_button_(Gtk::manage(new Gtk::SpinButton())),
  label_right_(Gtk::manage(new Gtk::Label(label_right)))
{
  spin_button_->set_numeric(true);
  set_spacing(10);
  spin_button_->set_increments(1, 10);

  label_->set_halign(Gtk::ALIGN_START);
  label_right_->set_halign(Gtk::ALIGN_START);

  pack_start(*label_,       Gtk::PACK_EXPAND_PADDING);
  pack_start(*spin_button_, Gtk::PACK_SHRINK);
  pack_start(*label_right_, Gtk::PACK_SHRINK);
}

/** destructor
 **/
LabelSpinButton::~LabelSpinButton()
  = default;

/** @return   the label
 **/
Label*
LabelSpinButton::get_label()
{
  return label_;
}

/** set the label
 **
 ** @param    str   the string for the label
 **/
void
LabelSpinButton::set_label(Glib::ustring const& str)
{
  label_->set_text(str);
}

/** @return   the label
 **/
SpinButton*
LabelSpinButton::get_spin_button()
{
  return spin_button_;
}

/** set the range
 **
 ** @param    min    minimum value
 ** @param    max    maximum value
 **/
void
LabelSpinButton::set_range(double const min, double const max)
{
  spin_button_->set_range(min, max);
}

/** set the increments
 **/
void
LabelSpinButton::set_increments(double const step, double const page)
{
  spin_button_->set_increments(step, page);
}

/** @return   the value
 **/
double
LabelSpinButton::get_value()
{
  return spin_button_->get_value();
}

/** @return   the value
 **/
int
LabelSpinButton::get_value_as_int()
{
  return spin_button_->get_value_as_int();
}

/** set the value
 **
 ** @param    value    new value
 **/
void
LabelSpinButton::set_value(double const value)
{
  return spin_button_->set_value(value);
}

/** @return   the signal for value changes
 **/
Glib::SignalProxy0<void>
LabelSpinButton::signal_value_changed()
{
  return spin_button_->signal_value_changed();
}

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
