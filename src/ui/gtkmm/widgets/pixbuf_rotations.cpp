/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "pixbuf_rotations.h"

namespace Gdk {

/** constructor
 **
 ** @param    filename   the file to load the pixbuf from
 **/
PixbufRotations::PixbufRotations(string const& filename) :
  pixbuf(4)
{
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try {
    static_cast<Glib::RefPtr<Pixbuf>&>(*this)
      = Pixbuf::create_from_file(filename);
  } // try
  catch (Glib::FileError const& file_error) { }
  catch (Gdk::PixbufError const& pixbuf_error) { }
#else
  std::auto_ptr<Glib::Error> error;
  static_cast<Glib::RefPtr<Pixbuf>&>(*this) = Pixbuf::create_from_file(filename, error);
#endif

  pixbuf[0] = *this;
} // PixbufRotations::PixbufRotations(string filename)

/** constructor
 **
 ** @param    pixbuf   the pixbuf for the 'up' rotation
 **/
PixbufRotations::PixbufRotations(Glib::RefPtr<Pixbuf> const pixbuf) :
  Glib::RefPtr<Pixbuf>(pixbuf),
  pixbuf(4)
{
  this->pixbuf[0] = *this;
}

/** destruktor
 **/
PixbufRotations::~PixbufRotations() = default;

/** -> result
 ** the rotation is automatically created if it does not yet exists
 **
 ** @param    rotation   rotation
 **
 ** @return   the pixbuf with 'rotation'
 **/
Glib::RefPtr<Pixbuf>
PixbufRotations::operator[](Rotation const rotation)
{
  if (!*this)
    return {};
  return (!pixbuf[static_cast<int>(rotation)]
          ? create_rotation(rotation)
          : pixbuf[static_cast<int>(rotation)]);
} // Glib::RefPtr<Pixbuf> PixbufRotations::operator[](Rotation rotation)

/** -> result
 **
 ** @param    rotation   rotation (default: 'up')
 **
 ** @return   the width of the pixbuf with 'rotation'
 **/
int
PixbufRotations::width(Rotation const rotation) const
{
  switch(rotation) {
  case Rotation::up:
  case Rotation::down:
    return (*this)->get_width();
  case Rotation::left:
  case Rotation::right:
    return (*this)->get_height();
  } // switch(rotation)

  return 0;
} // int PixbufRotations::width(Rotation rotation) const

/** -> result
 **
 ** @param    rotation   rotation (default: 'up')
 **
 ** @return   the height of the pixbuf with 'rotation'
 **/
int
PixbufRotations::height(Rotation const rotation) const
{
  switch(rotation) {
  case Rotation::up:
  case Rotation::down:
    return (*this)->get_height();
  case Rotation::left:
  case Rotation::right:
    return (*this)->get_width();
  } // switch(rotation)

  return 0;
} // int PixbufRotations::height(Rotation rotation) const

/** creates the rotation 'rotation' of '*this'
 **
 ** @param    rotation   the rotation
 **
 ** @return   rotated pixbuf
 **/
Glib::RefPtr<Pixbuf>
PixbufRotations::create_rotation(Rotation const rotation)
{
  switch(rotation) {
  case Rotation::up:
    // cannot happen
    return *this;
  case Rotation::down:
    return (pixbuf[static_cast<int>(rotation)] = pixbuf[0]->rotate_simple(Gdk::PIXBUF_ROTATE_UPSIDEDOWN));
  case Rotation::left:
    return (pixbuf[static_cast<int>(rotation)] = pixbuf[0]->rotate_simple(Gdk::PIXBUF_ROTATE_COUNTERCLOCKWISE));
  case Rotation::right:
    return (pixbuf[static_cast<int>(rotation)] = pixbuf[0]->rotate_simple(Gdk::PIXBUF_ROTATE_CLOCKWISE));
  } // switch(rotation)

  return *this;
} // Glib::RefPtr<Pixbuf> PixbufRotations::create_rotation(Rotation rotation)

} // namespace Gdk

#endif // #ifdef USE_UI_GTKMM
