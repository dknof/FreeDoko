/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "scaled_pixbuf.h"
#ifdef WORKAROUND
#include "../../../utils/file.h"
#endif

namespace Gdk {

/** Constructor
 **
 ** @param    filename   the file to load the pixbuf from
 **/
ScaledPixbuf::ScaledPixbuf(Path const filename)
{
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try {
#ifdef WORKAROUND
    if (!is_regular_file(filename))
      throw Glib::FileError(Glib::FileError::NO_SUCH_ENTITY,
                            ("file " + filename.string() + "not found").c_str());
#endif
    orig_pixbuf_ = Pixbuf::create_from_file(filename.string());
  } // try
  catch (Glib::FileError const& file_error) { }
  catch (Gdk::PixbufError const& pixbuf_error) { }
#else
  std::auto_ptr<Glib::Error> error;
  orig_pixbuf_ = Pixbuf::create_from_file(filename, error);
#endif
} // ScaledPixbuf::ScaledPixbuf(string filename)

/** Constructor
 **
 ** @param    pixbuf     the pixbuf for the 'up' rotation
 ** @param    scaling   the scaling, default: 1
 **/
ScaledPixbuf::ScaledPixbuf(Glib::RefPtr<Pixbuf> const pixbuf,
                           double const scaling) :
  scaling_(scaling),
  orig_pixbuf_(pixbuf ? pixbuf->copy() : pixbuf)
{ }

/** Constructor
 **
 ** @param    scaled_pixbuf   scaled_pixbuf to copy
 **/
ScaledPixbuf::ScaledPixbuf(ScaledPixbuf const& scaled_pixbuf) :
  ScaledPixbuf(scaled_pixbuf.orig_pixbuf_, scaled_pixbuf.scaling_)
{ }

/** copy operator
 **
 ** @param    scaled_pixbuf   scaled_pixbuf to copy
 **
 ** @return   copied element
 **/
ScaledPixbuf&
ScaledPixbuf::operator=(ScaledPixbuf const& scaled_pixbuf)
{
  if (this == &scaled_pixbuf)
    return *this;
  scaling_ = scaled_pixbuf.scaling_;
  if (scaled_pixbuf.orig_pixbuf_)
    orig_pixbuf_ = scaled_pixbuf.orig_pixbuf_->copy();
  else
    orig_pixbuf_.reset();
  scaled_pixbuf_.reset();

  return *this;
} // ScaledPixbuf& ScaledPixbuf::operator=(ScaledPixbuf scaled_pixbuf) :

/** @return   the scaled pixbuf
 **/
ScaledPixbuf::operator Glib::RefPtr<Pixbuf> const&()
{
  return pixbuf();
} // ScaledPixbuf::operator Glib::RefPtr<Pixbuf>()

/** -> result
 ** the scaling is automatically created if it does not yet exists
 **
 ** @return   the scaled pixbuf
 **/
Glib::RefPtr<Pixbuf> const&
ScaledPixbuf::pixbuf()
{
  if (!scaled_pixbuf_)
    scale();

  return scaled_pixbuf_;
} // Glib::RefPtr<Pixbuf> const& ScaledPixbuf::pixbuf()

/** @return   the original pixbuf
 **/
Glib::RefPtr<Pixbuf> const&
ScaledPixbuf::orig_pixbuf() const
{
  return orig_pixbuf_;
} // Glib::RefPtr<Pixbuf> const& ScaledPixbuf::orig_pixbuf() const

/** @return   whether there is a pixbuf
 **/
ScaledPixbuf::operator bool() const
{
  return static_cast<bool>(orig_pixbuf_);
} // ScaledPixbuf::operator bool() const

/** -> result
 **
 ** @param    rotation   rotation
 **
 ** @return   a rotated pixbuf
 **/
ScaledPixbuf
ScaledPixbuf::rotate(Gdk::PixbufRotation const rotation) const
{
  if (!orig_pixbuf())
    return *this;
  return ScaledPixbuf(orig_pixbuf()->rotate_simple(rotation),
                      get_scaling());
} // ScaledPixbuf rotate(Gdk::PixbufRotation rotation) const

/** @return   the scaling
 **/
double
ScaledPixbuf::get_scaling() const
{
  return scaling_;
} // double ScaledPixbuf::get_scaling() const

/** set the scaling
 **
 ** @param    scaling      the new scaling of the pixbuf
 **/
void
ScaledPixbuf::set_scaling(double const scaling)
{
  scaling_ = scaling;
  scaled_pixbuf_.clear();
} // void ScaledPixbuf::set_scaling(double scaling)

/** set the scaling such that at max width x height place is needed
 **
 ** @param    width    maximum width
 ** @param    height   maximum height
 **/
void
ScaledPixbuf::set_to_max_size(int const width, int const height)
{
  if (   (get_orig_width() == 0)
      || (get_orig_height() == 0))
    return;
  set_scaling(min(1.0,
                        min(width / static_cast<double>(get_orig_width()),
                            height / static_cast<double>(get_orig_height())
                           )));
} // void ScaledPixbuf::set_to_max_size(int width, int height)

/** @return   the width of the scaled pixbuf
 **/
int
ScaledPixbuf::get_width() const
{
  return (!scaled_pixbuf_
          ? static_cast<int>(get_orig_width() * get_scaling())
          : scaled_pixbuf_->get_width());
} // int ScaledPixbuf::get_width() const

/** @return   the width of the scaled pixbuf
 **/
int
ScaledPixbuf::get_height() const
{
  return (!scaled_pixbuf_
          ? static_cast<int>(get_orig_height() * scaling_)
          : scaled_pixbuf_->get_height());
} // int ScaledPixbuf::get_height() const

/** @return   the width of the original pixbuf
 **/
int
ScaledPixbuf::get_orig_width() const
{
  return (orig_pixbuf_ ? orig_pixbuf_->get_width() : 0);
} // int ScaledPixbuf::get_orig_width() const

/** @return   the height of the original pixbuf
 **/
int
ScaledPixbuf::get_orig_height() const
{
  return (orig_pixbuf_ ? orig_pixbuf_->get_height() : 0);
} // int ScaledPixbuf::get_orig_height() const

/** make the scaling
 **/
void
ScaledPixbuf::scale()
{
  if (!orig_pixbuf_)
    return;

  if (scaling_ == 1)
    scaled_pixbuf_ = orig_pixbuf_->copy();
  else
    scaled_pixbuf_
      = orig_pixbuf_->scale_simple(static_cast<int>(get_orig_width()
                                                          * scaling_),
                                         static_cast<int>(get_orig_height()
                                                          * scaling_),
                                         Gdk::INTERP_TILES);
  // If you do not like the scaled images, try the following:
  //                 Gdk::INTERP_NEAREST);
  //                 Gdk::INTERP_BILINEAR);
  //                 Gdk::INTERP_HYPER);
} // void ScaledPixbuf::scale()

} // namespace Gdk

#endif // #ifdef USE_UI_GTKMM
