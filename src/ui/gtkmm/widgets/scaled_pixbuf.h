/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include <gdkmm/pixbuf.h>

namespace Gdk {
/**
 ** @brief	a scaled pixmap
 **		scaled is only when needed
 **
 ** @author	Diether Knof
 **/
class ScaledPixbuf {
  public:
    ScaledPixbuf(Path filename);
    ScaledPixbuf(Glib::RefPtr<Pixbuf> pixbuf, double scaling = 1);
    ScaledPixbuf() = default;
    ScaledPixbuf(ScaledPixbuf const& pixbuf_rotations);
    ScaledPixbuf& operator=(ScaledPixbuf const& pixbuf_rotations);

    ~ScaledPixbuf() = default;

    operator Glib::RefPtr<Pixbuf> const&();
    Glib::RefPtr<Pixbuf> const& pixbuf();
    Glib::RefPtr<Pixbuf> const& orig_pixbuf() const;

    operator bool() const;

    ScaledPixbuf rotate(Gdk::PixbufRotation rotation) const;

    double get_scaling() const;
    void set_scaling(double scaling);
    void set_to_max_size(int width, int height);

    int get_width() const;
    int get_height() const;
    int get_orig_width() const;
    int get_orig_height() const;

  private:
    void scale();

  private:
    double scaling_ = 1;
    Glib::RefPtr<Pixbuf> orig_pixbuf_;
    Glib::RefPtr<Pixbuf> scaled_pixbuf_;
}; // class ScaledPixbuf

} // namespace Gdk

#endif // #ifdef USE_UI_GTKMM
