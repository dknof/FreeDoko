/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#ifdef USE_UI_GTKMM

#include "sticky_dialog.h"
#include "../main_window.h"
#include "../ui.h"

#include <gdk/gdkkeysyms.h>

namespace Gtk {

/** constructor
 **
 ** @param    title   the title of the window
 ** @param    modal   whether the window is modal
 **   (default: false)
 **/
StickyDialog::StickyDialog(Glib::ustring const& title,
                           bool const modal) :
  Dialog(title, modal)
{
#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  signal_show().connect(sigc::mem_fun(*this, &StickyDialog::on_show));
  signal_hide().connect(sigc::mem_fun(*this, &StickyDialog::on_hide));
  signal_key_press_event().connect(sigc::mem_fun(*this, &StickyDialog::on_key_press_event));
#endif
} // StickyDialog::StickyDialog(Glib::ustring title, bool modal) :

/** constructor
 **
 ** @param    title   the title of the window
 ** @param    main_window   the main window (transient)
 ** @param    modal   whether the window is modal
 **   (default: false)
 **/
StickyDialog::StickyDialog(Glib::ustring const& title,
                           UI_GTKMM_NS::MainWindow& main_window,
                           bool const modal) :
  Dialog(title, main_window, modal),
  main_window(&main_window)
{
  set_transient_for(main_window);
} // StickyDialog::StickyDialog(Glib::ustring title, UI_GTKMM_NS::MainWindow& main_window, bool modal) :

/** event: the window is hidden
 ** remember the position of the window
 **/
void
StickyDialog::on_hide()
{
  get_position(x, y);
#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  Dialog::on_hide();
#endif
} // void on_hide()

/** event: the window is shown
 ** move the window to the remembered position
 **/
void
StickyDialog::on_show()
{
  if (x != INT_MAX)
    move(x, y);
#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  Dialog::on_show();
#endif
} // void on_show()

/** event: a key has been pressed
 **
 ** @param    key   pressed key
 **
 ** @return   whether the key was accepted
 **/
bool
StickyDialog::on_key_press_event(GdkEventKey* key)
{
  // key commands
  //   Escape: close the window

  if (key->state == 0) {
    switch (key->keyval) {
    case GDK_KEY_Escape:
      hide();
      return true;
    } // switch (key->keyval)
  } // if (key->state == 0)

#ifdef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
  return (   Gtk::Dialog::on_key_press_event(key)
          || (main_window
              ? main_window->on_key_press_event(key)
              : false) );
#else
  return (main_window
          ? main_window->on_key_press_event(key)
          : false);
#endif
} // bool StickyDialog::on_key_press_event(GdkEventKey* key)

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
