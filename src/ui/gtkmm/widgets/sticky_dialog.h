/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include <gtkmm/dialog.h>

namespace UI_GTKMM_NS {
  class MainWindow;
} // namespace UI_GTKMM_NS
namespace Gtk {
/**
 ** a dialog which remembers its place and forwards the key presses
 ** (workaround)
 **/
class StickyDialog : public Dialog {
  public:
    StickyDialog(Glib::ustring const& title,
                 bool modal = false);
    StickyDialog(Glib::ustring const& title,
                 UI_GTKMM_NS::MainWindow& main_window,
                 bool modal = false);
    StickyDialog() = delete;
    StickyDialog(StickyDialog const&) = delete;
    StickyDialog& operator=(StickyDialog const&) = delete;

    ~StickyDialog() override = default;

  protected:
    bool on_key_press_event(GdkEventKey* key) override;
    void on_hide() override;
    void on_show() override;

  private:
    int x = INT_MAX;
    int y = INT_MAX;
    UI_GTKMM_NS::MainWindow* const main_window = nullptr;
}; // class StickyDialog : public Dialog

} // namespace Gtk

#endif // #ifdef USE_UI_GTKMM
