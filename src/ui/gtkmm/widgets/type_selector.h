/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef USE_UI_GTKMM

#pragma once

#include "constants.h"

#include <gtkmm/comboboxtext.h>

namespace Gtk {
/**
 ** @brief    a selector for types (p.e. 'Card')
 **
 ** @author   Diether Knof
 **/
template<typename Type>
  class TypeSelector : public ComboBoxText {
    public:
      TypeSelector() = default;
      TypeSelector(TypeSelector const&) = delete;
      TypeSelector& operator=(TypeSelector const&) = delete;
      ~TypeSelector() override = default;

      using Gtk::Container::add;
      // adds the type to the selector
      void add(Type const& type);

      // returns the selected type
      Type type();
      // set the selected type
      void set_type(Type const& type);

    private:
      // the types
      vector<Type> types;
  }; // class TypeSelector : public ComboBoxText

} // namespace Gtk

#include "type_selector.hpp"

#endif // #ifdef USE_UI_GTKMM
