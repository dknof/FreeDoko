/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "help.h"

#include "ui.h"
#include "../utils/windows.h"
#include "../utils/string.h"
#include "../utils/file.h"

/** show the help page of the given location
 **
 ** @param    location   location of the page to show
 **/
void
Help::show(string const& location)
{
#ifdef WINDOWS
  STARTUPINFO si = {sizeof(si)};
  PROCESS_INFORMATION pi;
  CreateProcess(NULL,
                const_cast<CHAR*>((::preferences(Preferences::Type::browser_command) + " " + location).c_str()),
                NULL, NULL, FALSE, 0, NULL, NULL,
                &si, &pi);
#else
  int const a =
    ::system((::preferences(Preferences::Type::browser_command) + " " + location // NOLINT(cert-env33-c)
              + " &").c_str());
  (void) a;
#endif
} // void Help::show(string const location)

/** @return   the location of the homepage
 **/
string
Help::homepage_location()
{
  vector<string> const languages = {"en", "de"};

  if (!contains(languages, ::preferences(Preferences::Type::language)))
    return ("http://free-doko.sourceforge.net/en/");

  return ("http://free-doko.sourceforge.net/"
          + ::preferences(Preferences::Type::language) + "/");
} // string Help::homepage_location()

/** @return   the location of the sourceforge page to download cardsets
 **/
string
Help::cardsets_download_location()
{
  return "http://sourceforge.net/project/showfiles.php?group_id=38051&package_id=30877";
} // string Help::cardsets_download_location()

/** -> result
 **
 ** @param    page   help page
 **                   default: index
 **
 ** @return   the location with the manual help in page 'page'
 **
 ** @bug       using the function 'realpath'
 **/
string
Help::manual_location(string const& page)
{
  // Search through a list of local directories, if that does not help,
  // return the internet address of the manual.

  // the directories to search
  vector<Directory> directories;

  { // fill the directories list
    if (getenv("FREEDOKO_MANUAL_DIRECTORY")) {
      directories.emplace_back(getenv("FREEDOKO_MANUAL_DIRECTORY"));
      directories.emplace_back(getenv("FREEDOKO_MANUAL_DIRECTORY")
                               + ("/" + ::preferences(Preferences::Type::language)));
    }
#ifdef WINDOWS
    {
      string dir = Reg_read(HKEY_LOCAL_MACHINE,
                            "Software\\FreeDoko",
                            "Help Directory");
      if (!dir.empty())
        directories.emplace_back(dir + "/" + ::preferences(Preferences::Type::language));
    }
#endif
    if (::preferences(Preferences::Path::manual_directory).is_absolute()) {
      directories.emplace_back(::preferences(Preferences::Path::manual_directory)
                               / ::preferences(Preferences::Type::language));
    } else {
      for (auto const& d : ::preferences.data_directories()) {
        directories.emplace_back(d / ::preferences(Preferences::Path::manual_directory)
                                 / ::preferences(Preferences::Type::language));
      }
    }
      for (auto const& d : ::preferences.data_directories()) {
        directories.emplace_back(d / "manual" / ::preferences(Preferences::Type::language));
        directories.emplace_back(d / "doc"/"manual" / ::preferences(Preferences::Type::language));
      }
  } // fill the directories list


  { // search the manual page
    for (auto const& d : directories) {
      if (is_regular_file(d / page)) {
        return ("file://" + absolute(d / page).string());
      }
    }
  } // search the manual page

  return ("http://free-doko.sourceforge.net/doc/manual/"
          + ::preferences(Preferences::Type::language) + "/" + page);
} // string Help::manual_location(string page)

/** -> result
 **
 ** @param    page   help page
 **
 ** @return   the location with the operation help in page 'page'
 **/
string
Help::operation_location(string const& page)
{
  return manual_location("operation/" + page);
} // string Help::operation_location(string page)

/** -> result
 **
 ** @param    type   rule type to show
 **
 ** @return   the location of the help for the rule 'type'
 **/
string
Help::location(Rule::Type::Bool const type)
{
  return operation_location("rules_config.html"
                            + String::replaced_all(to_string(type),
                                                   " ", "_"));
} // string Help::location(Rule::Type::Bool type)

/** -> result
 **
 ** @param    type   rule type to show
 **
 ** @return   the location of the help for the rule 'type'
 **/
string
Help::location(Rule::Type::Unsigned const type)
{
  return operation_location("rules_config.html"
                            + String::replaced_all(to_string(type), " ", "_"));
} // string Help::location(Rule::Type::Unsigned type)

/** -> result
 **
 ** @param    type   preferences type to show
 **
 ** @return   the location of the help for the preferences 'type'
 **/
string
Help::location(Preferences::Type::Bool const type)
{
  return operation_location("preferences.html"
                            + String::replaced_all(to_string(type), " ", "_"));
} // string Help::location(Preferences::Type::Bool type)

/** -> result
 **
 ** @param    type   preferences type to show
 **
 ** @return   the location of the help for the preferences 'type'
 **/
string
Help::location(Preferences::Type::Unsigned const type)
{
  return operation_location("preferences.html"
                            + String::replaced_all(to_string(type), " ", "_"));
} // string Help::location(Preferences::Type::Unsigned type)

/** -> result
 **
 ** @param    type   preferences type to show
 **
 ** @return   the location of the help for the preferences 'type'
 **/
string
Help::location(Preferences::Type::String const type)
{
  return operation_location("preferences.html"
                            + String::replaced_all(to_string(type), " ", "_"));
} // string Help::location(Preferences::Type::String type)

/** -> result
 **
 ** @param    type   preferences type to show
 **
 ** @return   the location of the cards order help
 **/
string
Help::location(Preferences::Type::CardsOrder const type)
{
  return operation_location("preferences.html"
                            + String::replaced_all(to_string(type), " ", "_"));
} // string Help::location(Preferences::Type::CardsOrder type)
