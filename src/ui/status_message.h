/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"


class UI;

#include <stack>

/**
 ** status message
 ** adds a status message as long as the object exists
 **/
class StatusMessage {
  public:
    StatusMessage() = delete;
    StatusMessage(UI& ui, string translation);
    ~StatusMessage();
  private:
    UI* const ui;
    string const t;
}; // class StatusMessage

/**
 ** list of status messages
 **/
class StatusMessages : private std::stack<unique_ptr<StatusMessage>> {
  public:
    StatusMessages() = delete;
    explicit StatusMessages(UI& ui);
    ~StatusMessages() = default;

    // add a status massage
    void add(string const& translation);
  private:
    UI* const ui;
}; // class StatusMessages
