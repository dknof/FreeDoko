/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"

#include "ui.dummy.h"

#include "../game/game.h"
#include "../player/player.h"
#include "../misc/bug_report.h"
#include "../os/bug_report_replay.h"

UI_Dummy::UI_Dummy() :
  UI(UIType::dummy)
{ }


void UI_Dummy::party_close()
{
  UI::party_close();
}


auto UI_Dummy::get_reservation(Player const& player) -> Reservation
{
  return player.get_default_reservation();
}


auto UI_Dummy::get_card(Player const& player) -> HandCard
{
  return player.hand().validcards(game().tricks().current()).highest_card();
}


auto UI_Dummy::get_poverty_cards_to_shift(Player& player) -> HandCards
{
  return HandCards(player.hand());
}

auto UI_Dummy::get_poverty_take_accept(Player const& player, unsigned const cardno) -> bool
{
  return false;
}


auto UI_Dummy::get_poverty_exchanged_cards(Player& player, vector<Card> const& cards) -> HandCards
{
  return HandCards(player.hand());
}


void UI_Dummy::information(string const& message, InformationType const type, bool const force_show)
{
  UI::information(message, type, force_show);
  cout << message << endl;
}


void UI_Dummy::error(string const& message, string const& backtrace)
{
  cerr << message << '\n';
  cerr << "\nBacktrace:\n" << backtrace << '\n';
#ifdef BUG_REPORT_REPLAY
  if (::bug_report_replay != nullptr)
    cerr << "--\n"
      << _("replayed bug report: %s", ::bug_report_replay->file().string()) << '\n';
#endif

#ifdef BUG_REPORT
  {
    auto const path = BugReport::create_bug_report(party(), message + "\nBacktrace:\n" + backtrace);

    // gettext: %s: filename, %s: contact
    cerr << _("BugReport::automatic creation, %s, %s", path.string(), contact)
      << endl;
  }
#endif

#ifdef ASSERTION_GENERATES_SEGFAULT
  {
    cerr << _("Creating segmentation fault.") << endl;
    SEGFAULT;
  }
#endif // #ifdef ASSERTION_GENERATES_SEGFAULT

  std::exit(EXIT_FAILURE);
}
