/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

#include "ui.h"

/**
 ** a dummy ui class (no user interaction)
 **/
class UI_Dummy : public UI {
public:
  UI_Dummy();
  UI_Dummy(UI_Dummy const&) = delete;
  UI_Dummy& operator=(UI_Dummy const&) = delete;
  ~UI_Dummy() override = default;

  void party_close() override;

  auto get_reservation(Player const& player) -> Reservation override;
  auto get_card(Player const& player)        -> HandCard override;

  auto get_poverty_cards_to_shift(Player& player) -> HandCards override;
  auto get_poverty_exchanged_cards(Player& player, vector<Card> const& cards) -> HandCards override;
  auto get_poverty_take_accept(Player const& player, unsigned cardno) -> bool override;

  void information(string const& message, InformationType type, bool force_show = false) override;
  void error(string const& message, string const& backtrace) override;
};
