/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "utils.h"

#include <sys/stat.h>
#ifdef __linux__
#include <execinfo.h>
#include <cxxabi.h>
#include <cstring>
#endif
#include <regex>
#include <ctime>
#include <chrono>
#include <thread>

// all things written into this stream are ignored
NullOStr null_ostr; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

auto read_line(std::istream& istr) -> string
{
  string line;
  do {
    std::getline(istr, line);
    if ( !line.empty() && (*(line.end() - 1) == '\r') )
      line.erase(line.end() - 1);
  } while (line.empty() && istr.good()) ;
  return line;
}

auto regex_match(string const& text, string const& expression) -> bool
{
  try {
    return std::regex_match(text, std::regex(expression, std::regex::extended));
  } catch (...) {
    return false;
  }
  return false;
}

auto regex_matchi(string const& text, string const& expression) -> bool
{
  try {
    return std::regex_match(text, std::regex(expression, std::regex::extended | std::regex::icase));
  } catch (...) {
    return false;
  }
  return false;
}

auto regex_replacei(string const& text, string const& expression, string const& replacement) -> string
{
  try {
    return std::regex_replace(text, std::regex(expression, std::regex::extended | std::regex::icase), replacement);
  } catch (...) {
    return text;
  }
  return text;
}

void generate_error(string const error_message)
{
  DEBUG_ASSERTION(false, error_message);
}

auto backtrace_string(unsigned const skip) -> string
{
#ifdef __linux__
  int constexpr buffer_size = 256;
  static void* buffer[buffer_size];
  auto const size = backtrace(buffer, buffer_size);
  auto const function_symbols = backtrace_symbols(buffer, size);
  string text;

  size_t funcnamesize = 256;
  auto funcname = new char[funcnamesize];

  //int status;
  for (int i = static_cast<int>(skip); i < size; ++i) {
    // demangeling code taken from http://panthema.net/2008/0901-stacktrace-demangled/ (see also https://stackoverflow.com/questions/77005/how-to-automatically-generate-a-stacktrace-when-my-program-crashes)
    auto const& function_symbol = function_symbols[i];
    auto begin_name   = strchr(function_symbol, '(');
    auto begin_offset = strchr(function_symbol, '+');

    text += "#" + std::to_string(i-1) + "  ";
    if (   !begin_name
        || begin_name[1] == '+'
        || !begin_offset
        || begin_name >= begin_offset) {
      // couldn't parse the line? Take the unmangled text
      text += function_symbol + "\n"s;
      continue;
    }

    *begin_name   = '\0';
    begin_name   += 1;
    *begin_offset = '\0';

    // Mangled name is now in [begin_name, begin_offset) and caller offset in [begin_offset, end_offset).
    // Now apply __cxa_demangle():
    int status = 0;
    auto const ptr = abi::__cxa_demangle(begin_name, funcname, &funcnamesize, &status);
    if (status == 0) {
      funcname = ptr; // use possibly realloc()-ed string
      text += funcname + "\n"s;
    } else {
      // demangling failed. Output function name as a C function with no arguments.
      text += begin_name + "()\n"s;
    }
  }
  return text;
#else
  return {};
#endif
}

Unsigned::Unsigned(string const& s)
{
  istringstream istr(s);
  istr >> *this;
}

auto operator<<(std::ostream& ostr, Unsigned const& u) -> std::ostream&
{
  if (u == UINT_MAX)
    ostr << '-';
  else
    ostr << static_cast<unsigned>(u);
  return ostr;
}

auto operator>>(std::istream& istr, Unsigned& u) -> std::istream&
{
  // ein '-' zählt als UINT_MAX
  while (istr.good()
         && std::isspace(istr.peek()))
    istr.get();
  if (istr.peek() == '-') {
    u = UINT_MAX;
    istr.get();
  } else {
    istr >> static_cast<unsigned&>(u);
  }
  return istr;
}

auto operator<<(std::ostream& ostr, optional<unsigned> const& u) -> std::ostream&
{
  if (!u)
    ostr << '-';
  else
    ostr << *u;
  return ostr;
}

auto operator>>(std::istream& istr, optional<unsigned>& u) -> std::istream&
{
  // ein '-' zählt als nicht gesetzt
  while (istr.good()
         && std::isspace(istr.peek()))
    istr.get();
  if (istr.peek() == '-') {
    u.reset();
    istr.get();
  } else {
    unsigned u2 = 0;
    istr >> u2;
    if (istr)
      *u = u2;
  }
  return istr;
}


auto time_now_string() -> string
{
  auto time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  string text = std::asctime(localtime(&time)); // cppcheck-suppress asctimeCalled
  text.pop_back();
  return text;
}


void sleep_msec(int msec)
{
  std::this_thread::sleep_for(std::chrono::milliseconds(msec));;
}
