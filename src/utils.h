/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "constants.h"

// some utils
// D.K.

#include "class/readconfig/readconfig.h"
#include <chrono>
#include <sstream>


#undef VOID
#define VOID

#ifdef MS_VISUAL_C
template<typename T>
inline auto max(T const& a, T const& b) -> T const&
{
  return ((a > b) ? a : b);
}
template<typename T>
inline auto min(T const& a, T const& b) -> T const&
{
  return ((a < b) ? a : b);
}
#endif

template<typename T>
constexpr auto factorial(T const n) -> T
{
  T result = 1;
  for (T i = 1; i <= n; ++i)
    result *= i;
  return result;
}

constexpr auto quad(unsigned const i) -> unsigned
{
  return i * i;
}

// file-utils

// all things written into this stream are ignored
class NullOStr : public std::ostream {
  public:
    NullOStr() : std::ostream(nullptr) {}
    NullOStr(NullOStr const&)       = delete;
    NullOStr(NullOStr&&)            = delete;
    auto operator=(NullOStr const&) = delete;
    auto operator=(NullOStr&&)      = delete;
    ~NullOStr() override = default;

    template <class T>
      auto operator<<(T const& t) const -> NullOStr const&
      { (void) t; return *this; }
};
extern NullOStr null_ostr; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

auto read_line(std::istream& istr) -> string;

auto regex_match(string const& text, string const& expression) -> bool;
auto regex_matchi(string const& text, string const& expression) -> bool;
auto regex_replacei(string const& text, string const& expression, string const& replacement) -> string;

void generate_error(string error_message = "generated error");

auto backtrace_string(unsigned skip = 1) -> string;



// this class is an unsigned
// the difference is, that 'UINT_MAX' written in /read from a stream is a '-'
class Unsigned {
  public:
    Unsigned() = default;
    Unsigned(unsigned u) : u(u)   { }
    explicit Unsigned(string const& s);
    operator unsigned&() { return u; }
    operator unsigned const&() const { return u; }
    Unsigned(Unsigned const&) = default;
    Unsigned(Unsigned&&) = default;
    auto operator=(Unsigned const&) -> Unsigned& = default;
    auto operator=(Unsigned&&)      -> Unsigned& = default;
    ~Unsigned() = default;
  private:
    unsigned u = 0;
};
auto operator<<(std::ostream& ostr, Unsigned const& u) -> std::ostream&;
auto operator>>(std::istream& istr, Unsigned& u)       -> std::istream&;

// write and read -- no value is a '-'
auto operator<<(std::ostream& ostr, optional<unsigned> const& u) -> std::ostream&;
auto operator>>(std::istream& istr, optional<unsigned>& u)       -> std::istream&;

auto time_now_string() -> string;

void sleep_msec(int msec);

// output of the name
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define WRITE_NAME(Type) \
  inline ostream& operator<<(ostream& ostr, Type const& t) \
{ return (ostr << ::name(t)); }


template<typename T>
auto ostr_to_string(T const& t) -> string
{
  std::ostringstream ostr;
  ostr << t;
  return ostr.str();
}
