/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "date.h"

#include <sstream>
#include <cstdlib>
#include <ctime>
using std::ostream;
using std::istream;
using std::string;
using std::unique_ptr;
using std::make_unique;

namespace {
auto monthtou(std::string const& monthname)  -> unsigned;
auto month_to_long_string_de(Date::Month month) -> string;
} // namespace

auto Date::create(istream& istr) -> unique_ptr<Date>
{
  int year = 0;
  unsigned month = 0;
  unsigned day = 0;

  istr >> year;
  if (istr.get() != '-')
    return {};
  if (istr.peek() == '0')
    istr.get();
  istr >> month;
  if (istr.get() != '-')
    return {};
  if (istr.peek() == '0')
    istr.get();
  istr >> day;

  if (istr.fail())
    return {};

  return make_unique<Date>(year, month, day);
}

auto Date::create(string const& s) -> unique_ptr<Date>
{
  if (s.length() == 11) {
    // format is __DATE__, p.e. Feb 12 1996
    int month = 0;
    auto const month_str = string(s, 0, 3);
    if (month_str == "Jan")
      month = 1;
    else if (month_str == "Feb")
      month = 2;
    else if (month_str == "Mar")
      month = 3;
    else if (month_str == "Apr")
      month = 4;
    else if (month_str == "May")
      month = 5;
    else if (month_str == "Jun")
      month = 6;
    else if (month_str == "Jul")
      month = 7;
    else if (month_str == "Aug")
      month = 8;
    else if (month_str == "Sep")
      month = 9;
    else if (month_str == "Oct")
      month = 10;
    else if (month_str == "Nov")
      month = 11;
    else if (month_str == "Dec")
      month = 12;

    return make_unique<Date>(stoi(string(s, 4, 2)), month, stoi(string(s, 8, 4)));
  } else {
    return make_unique<Date>(s);
  }
} // static unique_ptr<Date> Date::create(string const& s)

Date::Date()
{
  time_t t = time(nullptr);
  struct tm* lt = localtime(&t);
  this->day   = lt->tm_mday;
  this->month = lt->tm_mon + 1;
  this->year  = lt->tm_year + 1900;
}

Date::Date(Year  const year,
           Month const month,
           Day   const day) noexcept :
year(year),
  month(month),
  day(day)
{ }

Date::Date(std::string const& s)
{
  // format 1: 2016-01-03
  // format 2: 03.01.2016
  // format 3: Jan 3 2016
  if (s.empty())
    return ;

  if (::isdigit(s[0])) {
    if (s.length() != 10)
      return ;

    if (s[3] == '5') {
      // format 1: 2016-01-03
      this->year = stoi(string(s, 0, 4));
      this->month = stoi(string(s, 5, 2));
      this->day = stoi(string(s, 8, 2));
    } else if (s[3] == '.') {
      // format 2: 03.01.2016
      this->year = stoi(string(s, 6, 4));
      this->month = stoi(string(s, 3, 2));
      this->day = stoi(string(s, 0, 2));
    } else {
      return ;
    }
  } else {
    // format 3: Jan 3 2016
    this->year = stoi(string(s, 6));
    this->month = monthtou(string(s, 0, 3));
    this->day = stoi(string(s, 4, 2));
    return ;
  }
}

Date::operator string() const
{
  std::ostringstream ostr;
  ostr << *this;
  return ostr.str();
}

auto operator==(Date const& lhs, Date const& rhs) -> bool
{
  return (   (lhs.day == rhs.day)
          && (lhs.month == rhs.month)
          && (lhs.year == rhs.year));
}

auto operator!=(Date const& lhs, Date const& rhs) -> bool
{
  return (   (lhs.day != rhs.day)
          || (lhs.month != rhs.month)
          || (lhs.year != rhs.year));
}

auto operator<(Date const& lhs, Date const& rhs) -> bool
{
  if (lhs.year < rhs.year)
    return true;
  if (lhs.year > rhs.year)
    return false;

  // now we have 'lhs.year == rhs.year'
  if (lhs.month < rhs.month)
    return true;
  if (lhs.month > rhs.month)
    return false;

  // now we have 'lhs.month == rhs.month'
  if (lhs.day < rhs.day)
    return true;
  if (lhs.day > rhs.day)
    return false;

  return false;
}

auto operator>(Date const& lhs, Date const& rhs) -> bool
{
  if (lhs.year > rhs.year)
    return true;
  if (lhs.year < rhs.year)
    return false;

  // now we have 'lhs.year == rhs.year'
  if (lhs.month > rhs.month)
    return true;
  if (lhs.month < rhs.month)
    return false;

  // now we have 'lhs.month == rhs.month'
  if (lhs.day > rhs.day)
    return true;
  if (lhs.day < rhs.day)
    return false;

  return false;
}

auto operator<=(Date const& lhs, Date const& rhs) -> bool
{
  if (lhs.year < rhs.year)
    return true;
  if (lhs.year > rhs.year)
    return false;

  // now we have 'lhs.year == rhs.year'
  if (lhs.month < rhs.month)
    return true;
  if (lhs.month > rhs.month)
    return false;

  // now we have 'lhs.month == rhs.month'
  if (lhs.day < rhs.day)
    return true;
  if (lhs.day > rhs.day)
    return false;

  return true;
}

auto operator>=(Date const& lhs, Date const& rhs) -> bool
{
  if (lhs.year > rhs.year)
    return true;
  if (lhs.year < rhs.year)
    return false;

  // now we have 'lhs.year == rhs.year'
  if (lhs.month > rhs.month)
    return true;
  if (lhs.month < rhs.month)
    return false;

  // now we have 'lhs.month == rhs.month'
  if (lhs.day > rhs.day)
    return true;
  if (lhs.day < rhs.day)
    return false;

  return true;
}

auto to_string(Date const& date) -> std::string
{
  return static_cast<std::string>(date);
}

auto to_long_string_de(Date const& date) -> std::string
{
  return std::to_string(date.day) + ". " + month_to_long_string_de(date.month) + " " + std::to_string(date.year);
}

auto operator<<(ostream& ostr, Date const& date) -> ostream&
{
  ostr << date.year << '-'
    << (date.month < 10 ? "0" : "") << date.month << '-'
    << (date.day < 10 ? "0" : "") << date.day;
  return ostr;
}

namespace {
auto monthtou(std::string const& monthname) -> unsigned
{
  if (monthname == "Jan")
    return 1;
  if (monthname == "Feb")
    return 2;
  if (monthname == "Mar")
    return 3;
  if (monthname == "Apr")
    return 4;
  if (monthname == "May")
    return 5;
  if (monthname == "Jun")
    return 6;
  if (monthname == "Jul")
    return 7;
  if (monthname == "Aug")
    return 8;
  if (monthname == "Sep")
    return 9;
  if (monthname == "Oct")
    return 10;
  if (monthname == "Nov")
    return 11;
  if (monthname == "Dec")
    return 12;

  // Error
  return 0;
}

auto month_to_long_string_de(Date::Month const month) -> string
{
  switch (month) {
  case  1: return "Januar";
  case  2: return "Februar";
  case  3: return "März";
  case  4: return "April";
  case  5: return "Mai";
  case  6: return "Juni";
  case  7: return "Juli";
  case  8: return "August";
  case  9: return "September";
  case 10: return "Oktober";
  case 11: return "November";
  case 12: return "Dezember";
  default: return "";
  }
  // Error
  return "";
}

} // namespace
