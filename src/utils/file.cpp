/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */


#include "file.h"
#include "string.h" // NOLINT(modernize-deprecated-headers,hicpp-deprecated-headers) false positive
#include "windows.h"

#include <algorithm>
#include <sys/stat.h>
#if defined(LINUX) || defined(HPUX)
// for getpwuid
#include <pwd.h>
#include <unistd.h>
// for MAXPATHLEN
#include <sys/param.h>
#endif
#ifdef WINDOWS
// for SHGetFolderPath
#include <shlobj.h>
#endif
#include <cstdlib>

#ifndef RELEASE
#include <iostream>
#endif

using std::string;

namespace File {

Directory executable_directory = {}; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

/** initializes the executable directory
 **/
void
  initialize_executable_directory(string const argv0)
  {
    executable_directory = std::filesystem::path(argv0).parent_path();
  }

/** returns the home directory of the user
 **
 ** @return   the home directory
 **/
Directory
  home_directory()
  {
#ifndef WINDOWS
    if (getenv("HOME") != nullptr)
      return getenv("HOME");

    auto const pwd = getpwuid(getuid());
    if (pwd)
      return pwd->pw_dir;

#else  // !#ifndef WINDOWS
    {
      // shlobj.h
      // see http://msdn.microsoft.com/en-us/library/bb762181(VS.85).aspx
      // deprecated, but we use it since we also support Windows XP
      TCHAR szPath[MAX_PATH];

      if(SUCCEEDED(SHGetFolderPath(nullptr,
                                   CSIDL_APPDATA|CSIDL_FLAG_CREATE,
                                   nullptr,
                                   0,
                                   szPath)))
      {
        return szPath;
      }
    }
#endif // !#ifndef WINDOWS
    return {};
  } // string home_directory()

/** returns the desktop directory of the user
 **
 ** @return   the desktop directory
 **    empty string, if the directory does not exists
 **/
Directory
  desktop_directory()
  {
#ifdef WINDOWS
    return Reg_read(HKEY_CURRENT_USER,
                    "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders",
                    "Desktop");
#endif
#ifdef LINUX
    if (is_directory(home_directory() / "Desktop"))
      return (home_directory() / "Desktop");
#endif

    return {};
  }

/** Linux: replayce '~/' with $HOME
 ** Windows: replace '~/' with HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVerson\Explorer\Shell Folders\AppData
 **/
Path
  expand_home(Path const path)
  {
    auto const filename = path.string();
    if (filename.size() >= 2) {
      if (   (filename[0] == '~')
          && (filename[1] == '/')) {
        return home_directory() / string(filename, 2, string::npos);
      }
    }

    return path;
  }

/** @return   the absolute path with relative paths interpreted according to the executable directory
 **/
Path
  absolute_path_from_executable_directory(Path const path)
  {
    if (path.is_absolute())
      return path;
    if (   !path.string().empty()
        && path.string()[0] == '~')
      return absolute(expand_home(path));
    return absolute(executable_directory / path);
  } // string absolute_path(string path)

/** @return   all paths as absolute paths, relative is interpreted to the executable directory
 **/
std::vector<Directory>
absolute(std::vector<Directory> dirs)
{
  for (auto& dir : dirs) {
    if (dir.is_absolute())
      dir = canonical(dir);
    else
      dir = canonical(File::executable_directory / dir);
  }
  return dirs;
}

/** @return   all directories that exists
 **/
std::vector<Directory>
without_nonexisting(std::vector<Directory> dirs)
{
  auto i = dirs.begin();
  while (i != dirs.end()) {
    if (is_directory(i->is_absolute() ? *i : executable_directory / *i)) {
      ++i;
    } else {
      i = dirs.erase(i);
    }
  }
  return dirs;
}

/** @return   each path only one times
 **/
std::vector<Directory>
without_duplicates(std::vector<Directory> dirs)
{
  auto i = dirs.begin();
  while (i != dirs.end()) {
    if (std::find(dirs.begin(), i, *i) == i) {
      ++i;
    } else {
      i = dirs.erase(i);
    }
  }
  return dirs;
}

} // namespace File
