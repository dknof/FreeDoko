/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include <string>
#include <vector>

#include <filesystem>
using Path = std::filesystem::path;
using Directory = std::filesystem::path;

namespace File {
extern Directory executable_directory; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

void initialize_executable_directory(std::string argv0);

// returns the home directory of the current user
auto home_directory() -> Directory;
// returns the desktop directory of the current user
auto desktop_directory() -> Directory;

// expands the filename: replaces '~/' by '$HOME/'
auto expand_home(Path path) -> Path;

auto absolute_path_from_executable_directory(Path path) -> Path;

auto absolute(std::vector<Directory> dirs)            -> std::vector<Directory>;
auto without_nonexisting(std::vector<Directory> dirs) -> std::vector<Directory>;
auto without_duplicates(std::vector<Directory> dirs)  -> std::vector<Directory>;

} // namespace File
