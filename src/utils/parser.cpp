/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "parser.h"
#include "string.h" // NOLINT(modernize-deprecated-headers,hicpp-deprecated-headers) false positive

namespace Parser {

/** Gibt zurück, ob text leer ist
 ** Abschlussroutine für die parse-Templates
 **/
bool
parse(string& text)
{
  return text.empty();
} // bool parse(string& text)

/** Liest n aus text, entfernt den gelesenen Text und die folgenden Leerzeichen
 ** Ändert text nur, wenn er mit einem unsigned beginnt.
 **
 ** @return   true, wenn text mit einem unsigned beginnt, sonst false
 **/
bool
parse_(string& text, unsigned& n)
{
  try {
    n = std::stou(text);
    while (!text.empty() && ::isdigit(text.front()))
      text.erase(0, 1);
    String::remove_leading_blanks(text);
    return true;
  } catch (...) {
    return false;
  }
} // bool parse_(string& text, unsigned& n)

/** Speichert den Text in remaining_text
 **
 ** @return   true, wenn text nicht leer ist
 **/
bool
parse_(string& text, string& remaining_text)
{
  if (text.empty())
    return false;
  remaining_text = text;
  text.clear();
  return true;
} // bool parse_(string& text, string& remaining_text)

/** Liest fixtext aus text, entfernt ihn und die folgenden Leerzeichen
 ** Ändert text nur, wenn er mit fixtext beginnt.
 **
 ** @return   true, wenn text mit fixtext beginnt, sonst false
 **/
bool
parse_(string& text, char const* fixtext)
{
  auto const n = strlen(fixtext);
  if (text.compare(0, n, fixtext))
    return false;
  text.erase(0, n);
  String::remove_leading_blanks(text);
  return true;
} // bool parse_(string& text, char const* fixtext)

}; // namespace Parser
