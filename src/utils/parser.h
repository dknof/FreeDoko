/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include <string>
using std::string;

namespace Parser {
// Parsen von mehreren verschiedenen Argumenten (Rekursion)
template<typename... Args>	
  bool parse(string const& text, char const* fixtext, Args&... args);
template<typename T, typename... Args>	
  bool parse(string const& text, T& param, Args&... args);
bool parse(string& text);
// Hilfsmethoden
bool parse_(string& text, unsigned& n);
bool parse_(string& text, string& remaining_text);
bool parse_(string& text, char const* fixtext);

}; // namespace Parser

/** Prüft, ob text mit fixtext beginnt. Wenn ja, wird weiter geparst, ansonesten nicht.
 ** Gibt zurück, ob die angegebenen Argumente gepasst haben.
 ** Wenn alle Argumente passen, werden die Parameter auf den entsprechenden Wert gesetzt.
 **/
template<typename... Args>
bool
Parser::parse(string const& text, char const* fixtext, Args&... args)
{
  auto text2 = text;
  if (!parse_(text2, fixtext)) 
    return false;
  if (!parse(text2, args...))
    return false;
  return true;
} // template<typename... Args> bool Eingabe::parse(string text, char const* fixtext, Args... args)

/** Prüft, ob text mit Typ T beginnt. Wenn ja, wird weiter geparst, ansonesten nicht.
 ** Gibt zurück, ob die angegebenen Argumente gepasst haben.
 ** Wenn alle Argumente passen, werden die Parameter auf den entsprechenden Wert gesetzt.
 **/
template<typename T, typename... Args>
bool
Parser::parse(string const& text, T& param, Args&... args)
{
  auto text2 = text;
  T p;
  if (!parse_(text2, p)) 
    return false;
  if (!parse(text2, args...))
    return false;
  param = std::move(p);
  return true;
} // template<typename T, typename... Args> bool Eingabe::parse(string text, T& param, Args... args)
