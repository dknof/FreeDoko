/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "date.h"

#include <memory>
#include <random>

/**
 ** Creates an integer random value
 **/
class RandomBase {
  public:
    using Type = std::uint_fast32_t;

    static void test();

  public:
    RandomBase() noexcept = default;
    virtual ~RandomBase() = default;

    virtual void set_random_seed()                 = 0;
    virtual void set_seed(Type seed)               = 0;
    virtual auto seed()              const -> Type = 0;

    // returns the next random number
    virtual auto get() -> Type = 0;
    virtual auto operator()(int end) -> int = 0;
};

/**
 ** current random number generator (since 0.7.17)
 **/
class RandomMT19937 : public RandomBase {
  public:
  RandomMT19937();
  explicit RandomMT19937(Type seed) noexcept;
  ~RandomMT19937() override;

  static auto max_seed() -> Type;

  void set_random_seed()           override;
  void set_seed(Type seed)         override;
  auto seed() const        -> Type override;

  // returns the next random number
  auto get()               -> Type override;
  auto operator()(int end) -> int  override;

  private:
  std::mt19937 engine_;
  Type seed_ = 0;
};

/**
 ** old random number generator (pre 0.7.17)
 **/
class RandomPre_0_7_17 : public RandomBase {
  static constexpr int a_ = 7200;
  static constexpr int c_ = 100;
  static constexpr int m_ = 279841;

  public:
  RandomPre_0_7_17() noexcept;
  explicit RandomPre_0_7_17(Type seed) noexcept;
  ~RandomPre_0_7_17() override;

  static auto max_seed() -> Type;

  void set_random_seed()                 override;
  void set_seed(Type seed)               override;
  auto seed()              const -> Type override;

  // returns the next random number
  auto get()               -> Type override;
  auto operator()(int end) -> int  override;

  private:
  Type seed_ = 0;
};

using Random = RandomMT19937;

extern Random random_value; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)


template<typename Container>
auto random_element(Container const& container) {
  auto it = container.begin();
  std::advance(it, random_value(container.size()));
  return *it;
}
