/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "stop_watch.h"

#include <iostream>
using namespace std;

/** constructor
 ** starts the stop watch
 **/
StopWatch::StartStopProxy::StartStopProxy(StopWatch& stop_watch) :
  stop_watch(&stop_watch)
{ this->stop_watch->start(); }

/** destructor
 ** stops the stop watch
 **/
StopWatch::StartStopProxy::~StartStopProxy()
{ this->stop_watch->stop(); }

/** -> result
 **
 ** @param    start_time   start time
 **
 ** @return   difference from 'start_time' to 'now'
 **            '0' on error
 **/
MSecCounter
StopWatch::diff_to_now(clock_t const start_time) {
  clock_t const stop_time = clock();
  if (stop_time == static_cast<clock_t>(-1)) {
    cerr << "StopWatch::diff_to_now()\n"
      << "  could not get time"
      << endl;
    return 0;
  }

  if (start_time > stop_time) {
    cerr << "StopWatch::diff_to_now()\n"
      << "  overflow: "
      << "start time = " << start_time
      << " > " << stop_time << " = stop time"
      << endl;
    return 0;
  }

  // under Microsoft Windows 'CLOCKS_PER_SEC' is 1000,
  // under GNU/Linux (Posix) 1000000,
  // under FreeBSD < 1000
  // So in order to do integer calculation we have to split the code

  // The variable is needed because 'CLOCKS_PER_SEC' need not to be a
  // constant (so no '#if CLOCKS_PER_SEC < 1000' can be used).
  // But with a constant in FreeBSD 'CLOCKS_PER_SEC / 1000' equals 0,
  // this leads to the warning division with zero (g++-3.4.4).
  clock_t clocks_per_sec = CLOCKS_PER_SEC;
  if (clocks_per_sec < 1000) {
    return ((stop_time - start_time) * (1000 / clocks_per_sec));
  } else {
    return ((stop_time - start_time) / (clocks_per_sec / 1000));
  }
} // static MSecCounter StopWatch::diff_to_now(clock_t start_time)


/** constructor
 **/
StopWatch::StopWatch() :
  start_time(static_cast<clock_t>(-1))
{ }

/** destructor
 **/
StopWatch::~StopWatch()
{
#ifdef DKNOF
  if (this->depth > 0) {
    cerr << "StopWatch::~StopWatch()\n"
      << "  depth is not 0: " << this->depth
      << endl;
#ifndef RELEASE
    std::abort();
#endif
  }
#endif
} // StopWatch::~StopWatch()

/** @return   a start stop proxy
 **/
std::unique_ptr<StopWatch::StartStopProxy>
StopWatch::start_stop_proxy()
{ return std::make_unique<StartStopProxy>(*this); }

/** start the time counter
 **/
void
StopWatch::start()
{
  if (this->depth == 0)
    this->start_time = clock();

  if (this->start_time == static_cast<clock_t>(-1)) {
    cerr << "StopWatch::start()\n"
      << "  clock could not be started"
      << endl;
    return ;
  }

  this->depth += 1;
  this->calls_ += 1;
} // void StopWatch::start()

/** stop the time counter
 **/
void
StopWatch::stop()
{
  if (   (this->start_time == static_cast<clock_t>(-1))
      || (this->depth == 0) ) {
#ifdef DKNOF
    cerr << "StopWatch::stop()\n"
      << "clock not started"
      << endl;
#ifndef RELEASE
    //  (*reinterpret_cast<int*>(NULL) = 0);
#endif
#endif
    return ;
  }

  this->depth -= 1;

  if (this->depth == 0) {
    this->msec_ += StopWatch::diff_to_now(this->start_time);

    this->start_time = static_cast<clock_t>(-1);
  }
} // void StopWatch::stop()

/** reset the counter
 **/
void
StopWatch::reset()
{
  this->start_time = static_cast<clock_t>(-1);
  this->msec_ = 0;
  this->depth = 0;
} // void StopWatch::reset()

/** @return   whether the stop watch is running
 **/
bool
StopWatch::running() const
{
  return (this->start_time != static_cast<clock_t>(-1));
} // bool StopWatch::running() const

/** @return   the stopped time in miliseconds
 **/
MSecCounter
StopWatch::msec() const
{
  if (this->start_time != static_cast<clock_t>(-1))
    return this->msec_ + StopWatch::diff_to_now(this->start_time);
  else
    return this->msec_;
} // MSecCounter StopWatch::msec() const

/** @return   how many calls (of start) has been made
 **/
int
StopWatch::calls() const
{
  return this->calls_;
} // int StopWatch::calls() const
