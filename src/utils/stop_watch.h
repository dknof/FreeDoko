/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include <ctime>
#include <memory>

using MSecCounter = long unsigned;

/**
 ** a class to check the runtime
 ** uses 'clock()'
 **
 ** @todo     thread savety (start/stop: either save the thread id or do not use)
 **/
class StopWatch {
  public:
    /** A proxy for simplier measuring
     ** Starts at construct, stop at destruct
     **/
    class StartStopProxy {
      public:
        StartStopProxy() = delete;
        StartStopProxy(StopWatch& stop_watch);
        ~StartStopProxy();
      private:
        StopWatch* const stop_watch;
    }; // class StartStopProxy

  private:
    // returns the time betwenn 'start_time' and 'now'
    static MSecCounter diff_to_now(clock_t start_time);

  public:
    StopWatch();
    StopWatch(StopWatch const& stop_watch) = default;
    StopWatch& operator=(StopWatch const&) = delete;
    ~StopWatch();

    // create a start stop proxy
    std::unique_ptr<StartStopProxy> start_stop_proxy();

    // start the time counter (the time is added)
    void start();
    // stop the time counter
    void stop();
    // reset the time counter
    void reset();

    // whether the stop watch is running
    bool running() const;

    // elapsed time in mili seconds
    unsigned long msec() const;
    // how many calls have been made
    int calls() const;

  private:
    // the time when 'add' was called
    clock_t start_time;
    // elapsed time in mili seconds
    MSecCounter msec_ = 0;
    // how many 'start's still remain
    int depth = 0;

    // how many calls (of start) have been made
    int calls_ = 0;
}; // class StopWatch
