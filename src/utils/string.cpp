/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include <cctype>

#include "string.h" // NOLINT(modernize-deprecated-headers,hicpp-deprecated-headers) false positive

using std::string;

#include <istream>
#include <fstream>
#include <algorithm>
using std::istream;

namespace String {

bool contains(std::string const& s, char c)
{
  return std::find(s.begin(), s.end(), c) != s.end();
}

/** removes all leading blanks
 **/
string&
remove_leading_blanks(string& text)
{
  while (!text.empty() && ::isspace(text.front()))
    text.erase(0, 1);
  return text;
} // string& remove_leading_blanks(string& text)

/** -> result
 **
 ** @param    s   the string
 **
 ** @return   the string without blanks at the start and end
 **/
string
  remove_blanks(string const& s)
  {
    string s2 = s;
    return remove_blanks(s2);
  } // string remove_blanks(string s)

/** removes the blanks from the start and the end of the string
 **
 ** @param    s   the string which shall be changed
 **
 ** @return   the changed string
 **/
string&
  remove_blanks(string& s)
  {
    if (s.empty())
      return s;

    // perhaps work with a substring
    string::size_type pos_start = 0;
    string::size_type pos_end = 0;

    for (pos_start = 0; pos_start < s.length(); pos_start++)
      if (!isspace(s[pos_start]))
        break;
    for (pos_end = s.length() - 1; pos_end > 0; --pos_end)
      if (!isspace(s[pos_end]))
        break;

    return (s = string(s, pos_start, pos_end - pos_start + 1));
  } // string& remove_blanks(string& s)

/** remove all trailing newlines
 **
 ** @param    s   the string which shall be changed
 **
 ** @return   the changed string
 **/
string
  remove_trailing_newlines(string s)
  {
    while (   (s[s.size() - 1] == '\n')
           || (s[s.size() - 1] == '\r') )
      s.resize(s.size() - 1);
    return s;
  }

/** replace double newlines with one
 **
 ** @param    s   the string which shall be changed
 **
 ** @return   the changed string
 **/
string
  remove_double_newlines(string s)
  {
    for (int i = s.size() - 2; i >= 0; --i) {
      if (   (s[i] == '\n')
          && (s[i+1] == '\n') ) {
        s.erase(i, 1);
      }
    } // for (i)
    for (int i = s.size() - 4; i >= 0; --i) {
      if (   (s[i] == '\n')
          && (s[i+1] == '\r')
          && (s[i+2] == '\n')
          && (s[i+3] == '\r') ) {
        s.erase(i, 2);
      }
    } // for (i)
    return s;
  }


/** returns the first word
 **
 ** @param    text   the text
 **
 ** @return   the first word
 **/
string
  word_first(string const& s)
  {
    // ignore first blanks
    auto b = s.begin(); // start of the word
    for (; b != s.end(); ++b)
      if (!isspace(*b))
        break;

    // search the end of the word
    auto e = b; // end of the word
    for (; e != s.end(); ++e)
      if (isspace(*e))
        break;

    return string(b, e);
  } // string word_first(string const& text)

/** removes the first word
 **
 ** @param    text   the text
 **
 ** @return   the remaining text
 **/
string&
  word_first_remove(string& s)
  {
    auto c = s.begin();

    // ignore first blanks
    for (; c != s.end(); ++c)
      if (!isspace(*c))
        break;

    // search the end of the word
    for ( ; c != s.end(); ++c)
      if (isspace(*c))
        break;

    s.erase(s.begin(), c);

    return s;
  } // string& word_first_remove(string& text)

/** removes the first word with the following blanks
 **
 ** @param    text   the text
 **
 ** @return   the remaining text
 **/
string&
  word_first_remove_with_blanks(string& s)
  {
    word_first_remove(s);

    auto c = s.begin();

    // search the next non blank character
    for (; c != s.end(); ++c)
      if (!isspace(*c))
        break;

    s.erase(s.begin(), c);

    return s;
  } // string& word_first_remove_with_blanks(string& text)

/** replaces all 's' with 'replacement' in 'text'
 **
 ** @param    text   the string which shall be changed
 ** @param    s   the string which shall be replaced
 ** @param    replacement   the replacement for 's'
 **
 ** @return   the changed string
 **/
string&
  replace_all(string& text, string const& s, string const& replacement)
  {
    string::size_type pos = 0;

    while (pos != string::npos) {
      pos = text.find(s, pos);
      if (pos == string::npos)
        // no more 's' in '*this'
        break;

      text.replace(pos, s.length(), replacement);
      pos += replacement.length();
    } // while (pos < s.length())

    return text;
  } // string& replace_all(string& text, string const& s, string const& replacement)

/** return a new string with s replaced
 **
 ** @param    text   the string which shall be changed
 ** @param    s   the string which shall be replaced
 ** @param    replacement   the replacement for 's'
 **
 ** @return   the changed string
 **/
string
  replaced_all(string text, string const& s, string const& replacement)
  { return replace_all(text, s, replacement); }

/** gets a line and replaces the escape-sequences
 **
 ** @param    istr   the input stream
 **
 ** @return   the line
 **/
string
  get_till_eof(istream& istr)
  {
    string text;
    std::getline(istr, text, '\0');

    return text;
  } // string get_till_eof(istream& istr)

/** gets a line and replaces the escape-sequences
 **
 ** @param    istr   the input stream
 **
 ** @return   the line
 **/
string
  getfile(std::filesystem::path const& path)
  {
    std::ifstream istr(path);

    if (istr.fail())
      return "";

    return get_till_eof(istr);
  }

/** split 'line' according to 'delemiter'
 **
 ** @param    line        line to split
 ** @param    separator   separator of the parts
 **
 ** @return   list with the separated parts,
 **            without leading or trailing blanks
 **/
std::vector<std::string> split(std::string const& line,
                               char const separator)
{
  std::vector<std::string> parts;

  std::stringstream str(line);
  std::string text;

  while (getline(str, text, separator)) {
    while (   !text.empty()
           && isspace(text.front())) {
      text.erase(0, 1);
    }
    parts.push_back(text);
  }

  return parts;
}

/** converts from latin1 to utf8
 **
 ** @param    text   text in latin1 encoding
 **
 ** @return   text in utf8 encoding
 **/
std::string
latin1_to_utf8(std::string text)
{
  for (auto c = text.begin(); c != text.end(); ++c) {
    if(static_cast<unsigned char>(*c) >= 0x80) {
      // thanks to http://stackoverflow.com/questions/5586214/how-to-convert-char-from-iso-8859-1-to-utf-8-in-c-multiplatformly for the code
      c = text.insert(c, 0xc0 | (*c & 0xc0) >> 6);// first byte, simplified since our range is only 8-bits
      ++c;
      *c = (0x80 | (*c & 0x3f));
    }

  } // for (c)
  return text;
} // std::string latin1_to_utf8(std::string text)

/** converts all ascii characters (A-Z) to lowercase
 *  @param text string to convert
 *  @return converted string
 */

std::string tolower(std::string text)
{
  for (char& c : text){
    c = std::tolower(c);
  }
  return text;
}

} // namespace String
