/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "time.h" // NOLINT(modernize-deprecated-headers,hicpp-deprecated-headers) false positive
#ifndef WINDOWS
#include <sys/time.h>
#endif
#include <iostream>
#include <iomanip>

auto Time::current() -> Time
{
  {
    timespec tp; // NOLINT cppcoreguidelines-pro-type-member-init
    if (clock_gettime(CLOCK_REALTIME, &tp) == 0)
      return Time(tp.tv_sec, tp.tv_nsec / 1000 / 1000);
  }

  auto const time_clock = clock();
  if (time_clock == static_cast<clock_t>(-1))
    return Time();

  // under Microsoft Windows 'CLOCKS_PER_SEC' is 1000,
  // under GNU/Linux (Posix) 1000000,
  // under FreeBSD < 1000
  // So in order to do integer calculation we have to split the code

  // The variable is needed because 'CLOCKS_PER_SEC' need not to be a
  // constant (so no '#if CLOCKS_PER_SEC < 1000' can be used).
  // But with a constant in FreeBSD 'CLOCKS_PER_SEC / 1000' equals 0,
  // this leads to the warning division with zero (g++-3.4.4).
  auto clocks_per_sec = CLOCKS_PER_SEC;
  if (clocks_per_sec < 1000)
    return Time(time_clock / clocks_per_sec,
                (time_clock * (1000 / clocks_per_sec)) % 1000);
  else
    return Time(time_clock / clocks_per_sec,
                (time_clock / (clocks_per_sec / 1000)) % 1000);
}


Time::Time(time_t const sec, unsigned const msec) :
  sec(sec),
  msec(msec)
{ }


auto operator<<(ostream& ostr, Time const& time) -> ostream&
{
  auto const fill_bak = ostr.fill('0');
  auto sec = time.sec;
  sec %= 24 * 60 * 60;
  if (sec >= 60 * 60) {
    ostr << sec / (60 * 60) << ':' << std::setw(2);
    sec %= 60 * 60;
  }
  if (sec >= 60) {
    ostr << sec / 60 << ':' << std::setw(2);
    sec %= 60;
  }
  ostr << sec;
  ostr << '.' << std::setw(3) << time.msec;
  ostr.fill(fill_bak);
  return ostr;
}
