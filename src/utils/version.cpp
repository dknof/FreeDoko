/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "version.h"

#include <fstream>
#include <sstream>
using std::ostream;
using std::istream;
using std::ifstream;
using std::string;
using std::istringstream;
using std::ostringstream;
using std::unique_ptr;
using std::make_unique;

  auto Version::create_from_file(std::filesystem::path const path) -> unique_ptr<Version>
  {
    ifstream istr(path);
    if (istr.fail())
      return {};

    return Version::create(istr);
  }

auto Version::create(istream& istr) -> unique_ptr<Version>
{
  unsigned major = 0;
  unsigned minor = 0;
  unsigned micro = 0;
  char patch = '\0';
  auto date = Date{__DATE__};
  bool release = false;
  unsigned rc = 0;


  // read the date from the stream.
  // Format: 'major.minor.micro  (date)'
  // Example: '0.6.1  (2003-12-21)'
  istr >> major;
  if (istr.get() != '.')
    return {};
  istr >> minor;
  if (istr.get() != '.')
    return {};
  istr >> micro;
  if (istr.peek() != ' ')
    patch = istr.get();

  while (istr.peek() == ' ') {
    istr.get();
  }


  { // Datum in Klammern
    if (istr.peek() != '(') {
      return make_unique<Version>(major, minor, micro, patch, date, release);
    }
    istr.get();

    auto date2 = Date::create(istr);
    if (!date2) {
      return make_unique<Version>(major, minor, micro, patch, date, release);
    }
    date = *date2;

    if (istr.get() != ')') {
      return {};
    }
  } // Datum in Klammern

  while (istr.peek() == ' ') {
    istr.get();
  }

  { // Versionsbeschreibung in Klammern
    if (istr.peek() != '(') {
      return make_unique<Version>(major, minor, micro, patch, date, release);
    }
    istr.get();

    string release_str;
    while (   (istr.peek() != ')')
           && istr.good())
      release_str += static_cast<char>(istr.get());

    if (   (release_str != "release")
        && (release_str != "developer version")) {
      return make_unique<Version>(major, minor, micro, patch, date, release);
    }

    if (release_str == "release") {
      release = true;
    } else if (string(release_str, 0, 2) == "rc") {
      release = true;
      rc = stoul(string(release_str, 2, string::npos));
    }
  } // Versionsbeschreibung in Klammern

  while (istr.peek() == ' ') {
    istr.get();
  }

  { // Beschreibung
    if (istr.peek() != '(') {
      if (rc != 0)
        return make_unique<Version>(major, minor, micro, date, release, rc);
      else
        return make_unique<Version>(major, minor, micro, patch, date, release);
    }
    istr.get();

    string description;
    while (   (istr.peek() != ')')
           && istr.good()) {
      description += static_cast<char>(istr.get());
    }
    if (rc != 0)
      return make_unique<Version>(major, minor, micro, date, release, rc, description);
    else
      return make_unique<Version>(major, minor, micro, patch, date, release, description);
  } // Beschreibung
} // static unique_ptr<Version> Version::create(string filename)

auto Version::create(string const& s) -> unique_ptr<Version>
{
  istringstream istr(s);

  return Version::create(istr);
}

Version::Version(unsigned const major,
                 unsigned const minor, unsigned const micro,
                 char const patch) :
  major_(major),
  minor_(minor),
  micro_(micro),
  patch_(patch)
{ }

Version::Version(unsigned const major,
                 unsigned const minor, unsigned const micro,
                 Date const& date) :
  major_(major),
  minor_(minor),
  micro_(micro),
  date_(date)
{ }

Version::Version(unsigned const major,
                 unsigned const minor, unsigned const micro,
                 char const patch,
                 Date const& date) :
  major_(major),
  minor_(minor),
  micro_(micro),
  patch_(patch),
  date_(date)
{ }

Version::Version(unsigned const major,
                 unsigned const minor, unsigned const micro,
                 Date const& date,
                 bool const release,
                 std::string const description) :
  major_(major),
  minor_(minor),
  micro_(micro),
  date_(date),
  release_(release),
  description_(description)
{ }

Version::Version(unsigned const major,
                 unsigned const minor, unsigned const micro,
                 Date const& date,
                 bool const release,
                 unsigned const rc,
                 std::string const description) :
  major_(major),
  minor_(minor),
  micro_(micro),
  date_(date),
  release_(release),
  rc_(rc),
  description_(description)
{ }

Version::Version(unsigned const major,
                 unsigned const minor, unsigned const micro,
                 char const patch,
                 Date const& date,
                 bool const release,
                 std::string const description) :
  major_(major),
  minor_(minor),
  micro_(micro),
  patch_(patch),
  date_(date),
  release_(release),
  description_(description)
{ }

Version::Version(unsigned const major,
                 unsigned const minor, unsigned const micro,
                 std::string const description) :
  major_(major),
  minor_(minor),
  micro_(micro),
  description_(description)
{ }

auto Version::number_to_string() const -> string
{
  ostringstream ostr;

  ostr << this->major_
    << "."
    << this->minor_
    << "."
    << this->micro_;

  if (this->patch_ != '\0')
    ostr << this->patch_;

  return ostr.str();
} // string Version::number_to_string() const

auto Version::to_string() const -> string
{
  ostringstream ostr;
  
  ostr << this->number_to_string();

  ostr << "  (" << this->date_ << ')';

  if (this->release_) {
    if (this->rc_ != 0)
      ostr << " (rc" << this->rc_ << ")";
    else
      ostr << " (release)";
  } else {
    ostr << " (developer version)";
  }

  if (!this->description_.empty())
    ostr << " (" << this->description_ << ")";

  return ostr.str();
}

auto to_string(Version const& version) -> std::string
{
  return version.to_string();
}

auto operator<<(ostream& ostr, Version const& version) -> ostream&
{
  ostr << to_string(version);

  return ostr;
} // ostream& operator<<(ostream& ostr, Version const& version)

auto operator==(Version const& lhs, Version const& rhs) -> bool
{
  return (   lhs.micro_ == rhs.micro_
          && lhs.minor_ == rhs.minor_
          && lhs.major_ == rhs.major_
          && lhs.patch_ == rhs.patch_
          && lhs.date_ == rhs.date_
          && lhs.release_ == rhs.release_
          && lhs.rc_ == rhs.rc_
          && lhs.description_ == rhs.description_);
}

auto operator<(Version const& lhs, Version const& rhs) -> bool
{

  if (lhs.major_ < rhs.major_)
    return true;
  if (lhs.major_ > rhs.major_)
    return false;

  // now we have 'lhs.major == rhs.major'
  if (lhs.minor_ < rhs.minor_)
    return true;
  if (lhs.minor_ > rhs.minor_)
    return false;

  // now we have 'lhs.minor == rhs.minor'
  if (lhs.micro_ < rhs.micro_)
    return true;
  if (lhs.micro_ > rhs.micro_)
    return false;

  // now we have 'lhs.micro == rhs.micro'
  if (lhs.patch_ < rhs.patch_)
    return true;
  if (lhs.patch_ > rhs.patch_)
    return false;

  // now we have 'lhs.patch == rhs.patch'
  if (lhs.release_ && rhs.release_) {
    if (lhs.rc_ == 0)
      return false;
    if (rhs.rc_ == 0)
      return true;
    if (lhs.rc_ < rhs.rc_)
      return true;
    if (lhs.rc_ > rhs.rc_)
      return false;
  }

  // now the version numbers are equal
  return (lhs.date_ < rhs.date_);
}

auto operator<=(Version const& lhs, Version const& rhs) -> bool
{
  return ((lhs < rhs) || (lhs == rhs));
}

auto operator>(Version const& lhs, Version const& rhs) -> bool
{
  return (rhs < lhs);
}

auto operator>=(Version const& lhs, Version const& rhs) -> bool
{
  return (rhs <= lhs);
}
