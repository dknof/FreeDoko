/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#pragma once

#include "date.h"

#include <iosfwd>
#include <string>
#include <memory>
#include <filesystem>

class Version {
  public:
    static auto create_from_file(std::filesystem::path path) -> std::unique_ptr<Version>;
    static auto create(std::istream& istr)                   -> std::unique_ptr<Version>;
    static auto create(std::string const& s)                 -> std::unique_ptr<Version>;

  public:
    friend auto operator==(Version const& lhs, Version const& rhs) -> bool;
    friend auto operator< (Version const& lhs, Version const& rhs) -> bool;
  public:
    Version() = delete;
    Version(unsigned major, unsigned minor,
            unsigned micro, char patch = '\0');
    Version(unsigned major, unsigned minor,
            unsigned micro,
            Date const& date);
    Version(unsigned major, unsigned minor,
            unsigned micro, char patch,
            Date const& date);
    Version(unsigned major, unsigned minor,
            unsigned micro,
            Date const& date,
            bool release,
            std::string description = "");
    Version(unsigned major, unsigned minor,
            unsigned micro,
            Date const& date,
            bool release,
            unsigned rc,
            std::string description = "");
    Version(unsigned major, unsigned minor,
            unsigned micro, char patch,
            Date const& date,
            bool release,
            std::string description = "");
    Version(unsigned major, unsigned minor,
            unsigned micro, char patch,
            Date const& date,
            std::string description = "");
    Version(unsigned major, unsigned minor,
            unsigned micro,
            std::string description);
    Version(Version const& version) = default;
    Version(Version&& version)      = default;
    auto operator=(Version const&)  = delete;
    auto operator=(Version&&)       = delete;
    ~Version() = default;

    auto to_string()        const -> std::string;
    auto number_to_string() const -> std::string;

  private:
    unsigned const major_;
    unsigned const minor_;
    unsigned const micro_ = 0;
    char const patch_ = '\0';
    Date const date_;
    bool const release_ = false;
    unsigned const rc_ = 0;
    std::string const description_; // a (short) description
}; // class Version

auto operator==(Version const& lhs, Version const& rhs) -> bool;
auto operator< (Version const& lhs, Version const& rhs) -> bool;
auto operator<=(Version const& lhs, Version const& rhs) -> bool;
auto operator> (Version const& lhs, Version const& rhs) -> bool;
auto operator>=(Version const& lhs, Version const& rhs) -> bool;

auto to_string(Version const& version) -> std::string;
auto operator<<(std::ostream& ostr, Version const& version) -> std::ostream&;
