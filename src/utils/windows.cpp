/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#ifdef WINDOWS

#include "windows.h"

#include <iostream>

using std::string;
using std::cerr;
using std::endl;

/** Windows: the registry entry
 **
 ** @param    hkey   hkey of the registry entry
 ** @param    key   key name of the registry entry
 ** @param    name   name of the registry entry
 **
 ** @return   string value of the registry
 **/
string
Reg_read(HKEY hkey, string const& key, string const& name)
{
  // reading the registry
  long winapi;
  HKEY handle;

  winapi = RegOpenKeyEx(hkey, key.c_str(), 0, KEY_QUERY_VALUE, &handle);
  if (winapi != 0) {
    return "";
  }

  DWORD size = 0;
  DWORD type = 0;
  winapi = RegQueryValueEx(handle, name.c_str(), NULL, &type, NULL, &size);
  // (HKEY,LPCSTR,PDWORD,PDWORD,LPBYTE,PDWORD);

  if ((winapi != 0)
      || (size == 0)) {
    RegCloseKey(hkey);
    return "";
  }

#ifndef RELEASE
  if (type != REG_SZ) {
    cerr << "type '" << type << "' is not 'REG_SZ'." << endl;
  }
#endif


  BYTE* value = new BYTE[size];
  winapi = RegQueryValueEx(handle, name.c_str(), NULL, NULL, value, &size);
  if (winapi != 0) {
    RegCloseKey(hkey);
    return "";
  }

  RegCloseKey(hkey);

  string result = (char*)value;
  delete[] value;

  return result;
} // string Reg_read(HKEY& hkey, string const& key, string const& name)

#endif // #ifdef WINDOWS
