/*  This file is part of FreeDoko.
    FreeDoko is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version (see <http://www.gnu.org/licenses/>).

    Diese Datei ist Teil von FreeDoko.
    FreeDoko ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren (siehe <https://www.gnu.org/licenses/>).
    */

#include "constants.h"
#include "versions.h"

vector<Version> const all_versions = {
  {0,6,3,     Date(2004,  5,  5), true},
  {0,6,4,     Date(2004,  6,  1), true},
  {0,6,5,     Date(2004,  6, 24), true},
  {0,6,5,'b', Date(2004,  6, 28), true},
  {0,6,6,     Date(2004,  9, 25), true},
  {0,6,7,     Date(2004, 12, 16), true},
  {0,6,7,'b', Date(2004, 12, 20), true},
  {0,6,8,     Date(2005,  4, 11), true},
  {0,6,9,     Date(2005,  6, 30), true},
  {0,7,0,     Date(2005,  8, 22), true},
  {0,7,1,     Date(2005, 11, 12), true},
  {0,7,2,     Date(2006,  1, 23), true},
  {0,7,2,'b', Date(2006,  2,  9), true},
  {0,7,3,     Date(2006,  8, 10), true},
  {0,7,4,     Date(2007,  1, 18), true},
  {0,7,5,     Date(2008, 10, 26), true},
  {0,7,6,     Date(2009,  8,  1), true},
  {0,7,6,'b', Date(2009,  8, 16), true},
  {0,7,7,     Date(2009,  9, 27), true},
  {0,7,8,     Date(2010,  3, 13), true},
  {0,7,9,     Date(2011,  3,  6), true},
  {0,7,10,    Date(2011,  8, 14), true},
  {0,7,11,    Date(2011, 12, 18), true},
  {0,7,12,    Date(2013, 12,  7), true},
  {0,7,13,    Date(2014, 12, 29), true},
  {0,7,14,    Date(2016,  1, 16), true},
  {0,7,15,    Date(2016, 12, 30), true},
  {0,7,16,    Date(2017,  1, 10), true},
  {0,7,17,    Date(2018,  1,  4), true},
  {0,7,18,    Date(2018,  7, 13), true},
  {0,7,19,    Date(2018, 12, 31), true},
  {0,7,20,    Date(2019,  4,  5), true},
  {0,7,21,    Date(2019, 11, 24), true},
  {0,7,22,    Date(2020,  4, 19), true},
  {0,7,23,    Date(2020, 12, 31), true},
  {0,7,23,'b',Date(2021,  1,  2), true},
  {0,7,23,'c',Date(2021,  1, 28), true},
  {0,7,24,    Date(2021, 12, 30), true},
  {0,7,25,    Date(2022,  1, 22), true},
  {0,7,26,    Date(2022, 12,  9), true},
  {0,8,0,     Date(2023, 12, 16), true},
  {0,8,1,     Date(2024, 12, 28), true},
  {0,8,2,     Date(__DATE__), false
#ifdef VERSION_DESCRIPTION
    , VERSION_DESCRIPTION
#endif
  }
};

Version const* const version = &::all_versions.back();
